import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/address_model/address_model.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class GetAllAddresses implements UseCase<List<AddressModel>, bool> {
  @override
  Future<List<AddressModel>> call(bool params) async {
    var data = await getIt<ProfileRepository>().getAllAddress(params);
    return data.fold((l) => [], (r) => r);
  }
}
