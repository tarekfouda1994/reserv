import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/services_favourit_model/services_favourite_model.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';

class GetFavouriteServices implements UseCase<List<ServicesFavouriteModel>, TopBusinessParams> {
  @override
  Future<List<ServicesFavouriteModel>> call(TopBusinessParams params) async {
    var data = await getIt<ProfileRepository>().getFavouriteServices(params);
    return data.fold((l) => [], (r) => r);
  }
}
