import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

import '../../../../core/helpers/di.dart';
class GetTerms implements UseCase<String,bool>{
  @override
  Future<String> call(bool params) async{
    var data = await getIt<ProfileRepository>().getTerms(params);
    return data.fold((l) => "", (r) => r);
  }
}