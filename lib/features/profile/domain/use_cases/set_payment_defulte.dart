import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class SetPaymentDefault implements UseCase<bool, String> {
  @override
  Future<bool> call(String params) async {
    var result = await getIt<ProfileRepository>().setPaymentDefault(params);
    return result.fold((l) => false, (r) => r);
  }
}
