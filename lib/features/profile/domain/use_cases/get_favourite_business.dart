import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/buisiness_total_favourite/business_total_favourite_model.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';

class GetFavouriteBusiness
    implements UseCase<BusinessTotalFavouriteModel?, TopBusinessParams> {
  @override
  Future<BusinessTotalFavouriteModel?> call(TopBusinessParams params) async {
    var data = await getIt<ProfileRepository>().getFavouriteBusiness(params);
    return data.fold((l) => null, (r) => r);
  }
}
