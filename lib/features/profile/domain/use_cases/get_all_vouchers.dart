import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_model/voucher_model.dart';
import 'package:flutter_tdd/features/profile/domain/entities/voucher_entity.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

import '../../../../core/usecases/use_case.dart';

class GetAllVouchers implements UseCase<List<VoucherModel>, VoucherParams> {
  @override
  Future<List<VoucherModel>> call(VoucherParams params) async {
    var data = await getIt<ProfileRepository>().getAllVoucher(params);
    return data.fold((l) => [], (r) => r);
  }
}
