import '../../../../core/helpers/di.dart';
import '../../../../core/usecases/use_case.dart';
import '../repositories/profile_repository.dart';

class DeleteAddress implements UseCase<bool, String> {
  @override
  Future<bool> call(String params) async {
    var result = await getIt<ProfileRepository>().deleteAddressCard(params);
    return result.fold((l) => false, (r) => r);
  }
}
