import '../../../../core/helpers/di.dart';
import '../../../../core/usecases/use_case.dart';
import '../repositories/profile_repository.dart';

class SetAddressDefault implements UseCase<bool, String> {
  @override
  Future<bool> call(String params) async {
    var result = await getIt<ProfileRepository>().setAddressDefault(params);
    return result.fold((l) => false, (r) => r);
  }
}
