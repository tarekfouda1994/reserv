import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class GetAllPayment implements UseCase<List<PaymentModel>, bool> {
  @override
  Future<List<PaymentModel>> call(bool params) async {
    var data = await getIt<ProfileRepository>().getAllPayment(params);
    return data.fold((l) => [], (r) => r);
  }
}
