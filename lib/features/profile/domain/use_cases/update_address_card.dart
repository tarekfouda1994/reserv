import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class UpdateAddressCard implements UseCase<bool, AddressPrams> {
  @override
  Future<bool> call(AddressPrams params) async {
    var result = await getIt<ProfileRepository>().updateAddress(params);
    return result.fold((l) => false, (r) => r);
  }
}
