import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/geo_code_address_model/geo_code_address_model.dart';
import 'package:flutter_tdd/features/profile/domain/entities/geo_code_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class GetGeoCodeAddress implements UseCase<GeoCodeAddressModel?, GeoCodeAddressEntity> {
  @override
  Future<GeoCodeAddressModel?> call(GeoCodeAddressEntity params) async {
    var data = await getIt<ProfileRepository>().getAddress(params);
    return data.fold((l) => null, (r) => r);
  }
}
