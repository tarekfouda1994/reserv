import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class UpdateWishBusiness implements UseCase<bool, String> {
  @override
  Future<bool> call(String params) async {
    var data = await getIt<ProfileRepository>().updateWishBusiness(params);
    return data.fold((l) => false, (r) => r);
  }
}
