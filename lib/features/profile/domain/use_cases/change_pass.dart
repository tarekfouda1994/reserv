import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/entities/cahnge_pass_entity.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class ChangePass implements UseCase<bool, ChangePassParams> {
  @override
  Future<bool> call(ChangePassParams params) async {
    var result = await getIt<ProfileRepository>().changePass(params);
    return result.fold((l) => false, (r) => r);
  }
}
