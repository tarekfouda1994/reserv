import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

import '../../../../core/models/user_model/user_model.dart';

class GetUserData implements UseCase<UserModel?, String> {
  @override
  Future<UserModel?> call(String params) async {
    var data = await getIt<ProfileRepository>().getUserData(params);
    return data.fold((l) => null, (r) => r);
  }
}
