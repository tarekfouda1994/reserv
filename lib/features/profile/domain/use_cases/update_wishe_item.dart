import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

import '../entities/wish_entity.dart';

class UpdateWishService implements UseCase<bool, WishParams> {
  @override
  Future<bool> call(WishParams params) async {
    var data = await getIt<ProfileRepository>().updateWishes(params);
    return data.fold((l) => false, (r) => r);
  }
}
