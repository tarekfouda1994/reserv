import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_stats_model/voucher_stats.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class GetVoucherStats implements UseCase<List<VoucherStats>, bool> {
  @override
  Future<List<VoucherStats>> call(bool params) async {
    var data = await getIt<ProfileRepository>().getVoucherStats();
    return data.fold((l) => [], (r) => r);
  }
}
