import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/question_model/question_model.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

import '../../../../core/helpers/di.dart';

class GetFrequentlyAskedQuestions implements UseCase<List<QuestionModel>, bool> {
  @override
  Future<List<QuestionModel>> call(bool params) async {
    var data = await getIt<ProfileRepository>().getQuestion(params);
    return data.fold((l) => [], (r) => r);
  }
}
