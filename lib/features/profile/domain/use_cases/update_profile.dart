import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_profile_entity.dart';

import '../repositories/profile_repository.dart';

class UpdateProfile implements UseCase<String?, UpdateProfileParams> {
  @override
  Future<String?> call(UpdateProfileParams params) async {
    var data = await getIt<ProfileRepository>().updateProfile(params);
    return data.fold((l) => null, (r) => r);
  }
}
