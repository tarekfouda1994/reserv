import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/domain/entities/payment_entity.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class CreateUpdatePayment implements UseCase<bool, PaymentPrams> {
  @override
  Future<bool> call(PaymentPrams params) async {
    var result = await getIt<ProfileRepository>().createPayment(params);
    return result.fold((l) => false, (r) => r);
  }
}
