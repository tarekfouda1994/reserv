import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_model/contact_us_model.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';

class GetContactUsData implements UseCase<ContactUsModel?, bool> {
  @override
  Future<ContactUsModel?> call(bool params) async {
    var data = await getIt<ProfileRepository>().getContactUs(params);
    return data.fold((l) => null, (r) => r);
  }
}
