import '../../../../core/helpers/di.dart';
import '../../../../core/usecases/use_case.dart';
import '../entities/wish_entity.dart';
import '../repositories/profile_repository.dart';

class RemoveWishService implements UseCase<bool, WishParams> {
  @override
  Future<bool> call(WishParams params) async {
    var data = await getIt<ProfileRepository>().removeWishService(params);
    return data.fold((l) => false, (r) => r);
  }
}
