class BusinessFavouriteParams {
  bool? hasWishItem;
  bool refresh;
  int pageNumber;
  int pageSize;
  bool? isImageRequired;

  BusinessFavouriteParams({
    this.hasWishItem,
    required this.refresh,
    required this.pageNumber,
    required this.pageSize,
    this.isImageRequired,
  });

  Map<String, dynamic> toJson() => {
        "hasWishItem": true,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "isImageRequired": true,
      };
}
