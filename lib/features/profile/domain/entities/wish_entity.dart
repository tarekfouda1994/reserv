class WishParams {
  String businessServiceID;
  String serviceID;

  WishParams({required this.businessServiceID, required this.serviceID});

  Map<String, dynamic> toJson() => {
        "businessServiceID": businessServiceID,
        "serviceID": serviceID,
      };
}
