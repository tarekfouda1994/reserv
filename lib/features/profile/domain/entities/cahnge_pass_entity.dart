class ChangePassParams {
  String email;
  String oldPassword;
  String newPassword;

  ChangePassParams({required this.email, required this.oldPassword, required this.newPassword});

  Map<String, dynamic> toJson() => {
        "userEmail": email,
        "oldPassword": oldPassword,
        "newPassword": newPassword,
      };
}
