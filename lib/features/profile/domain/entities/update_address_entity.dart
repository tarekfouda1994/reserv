class AddressPrams {
  String? id;
  String? addressType;
  String? locationDetail;
  double? longitude;
  double? latitude;
  bool? isDefault;

  AddressPrams(
      {this.id,
      this.longitude,
      this.locationDetail,
      this.latitude,
      this.addressType,
      this.isDefault});

  Map<String, dynamic> toJson() => {
        if (id != null) "id": id,
        "addressType": addressType,
        "locationDetail": locationDetail,
        "longitude": longitude,
        "latitude": latitude,
        "isDefault": isDefault,
      };
}
