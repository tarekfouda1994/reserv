class PaymentPrams {
  String? id;
  String? holderName;
  String? cardNumber;
  String? expiryDate;
  String? cvv;
  bool? isDefault;

  PaymentPrams(
      {this.id, this.expiryDate, this.cardNumber, this.cvv, this.holderName, this.isDefault});

  Map<String, dynamic> toJson() => {
        "id": id,
        "holderName": holderName,
        "cardNumber": cardNumber,
        "expiryDate": expiryDate,
        "cvv": cvv,
        "isDefault": isDefault,
      };
}
