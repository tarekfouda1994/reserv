class GeoCodeAddressEntity {
  double lat;
  double lng;
  String address;
  String city;
  String region;
  String state;
  String prov;
  String language;

  GeoCodeAddressEntity({
    this.lat = 0,
    this.lng = 0,
    this.address = "",
    this.city = "",
    this.region = "",
    this.state = "AE",
    this.prov = "",
    required this.language,
  });
}
