class VoucherParams {
  int pageNumber;
  int pageSize;
  int statusValue;
  bool loadImages;
  bool? refresh;

  VoucherParams(
      {required this.pageSize,
      required this.pageNumber,
      required this.loadImages,
      required this.statusValue,
      this.refresh});

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "loadImages": loadImages,
        "statusValue": statusValue
      };
}
