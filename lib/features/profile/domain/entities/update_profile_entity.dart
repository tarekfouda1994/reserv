class UpdateProfileParams {
  String? id;
  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  String birthDate;
  String nationalityId;
  String userPhoto;
  int? genderValue;

  UpdateProfileParams(
      {required this.email,
      required this.userPhoto,
      required this.phoneNumber,
      required this.nationalityId,
       this.genderValue,
      required this.firstName,
      required this.birthDate,
      required this.id,
      required this.lastName});

  Map<String, dynamic> toJson() => {
        "id": id,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "phoneNumber": phoneNumber,
        "birthDate": birthDate,
        "nationalityId": nationalityId,
        "userPhoto": userPhoto,
       if(genderValue!=null) "genderValue": genderValue,
      };
}
