// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_us_address_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ContactUsAddressModel _$$_ContactUsAddressModelFromJson(
        Map<String, dynamic> json) =>
    _$_ContactUsAddressModel(
      nameEnglish: json['nameEnglish'] as String,
      nameArabic: json['nameArabic'] as String,
      longitude: (json['longitude'] as num).toDouble(),
      latitude: (json['latitude'] as num).toDouble(),
    );

Map<String, dynamic> _$$_ContactUsAddressModelToJson(
        _$_ContactUsAddressModel instance) =>
    <String, dynamic>{
      'nameEnglish': instance.nameEnglish,
      'nameArabic': instance.nameArabic,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
    };
