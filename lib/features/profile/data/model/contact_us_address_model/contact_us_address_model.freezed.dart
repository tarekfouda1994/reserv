// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'contact_us_address_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ContactUsAddressModel _$ContactUsAddressModelFromJson(
    Map<String, dynamic> json) {
  return _ContactUsAddressModel.fromJson(json);
}

/// @nodoc
mixin _$ContactUsAddressModel {
  String get nameEnglish => throw _privateConstructorUsedError;
  String get nameArabic => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ContactUsAddressModelCopyWith<ContactUsAddressModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContactUsAddressModelCopyWith<$Res> {
  factory $ContactUsAddressModelCopyWith(ContactUsAddressModel value,
          $Res Function(ContactUsAddressModel) then) =
      _$ContactUsAddressModelCopyWithImpl<$Res>;
  $Res call(
      {String nameEnglish,
      String nameArabic,
      double longitude,
      double latitude});
}

/// @nodoc
class _$ContactUsAddressModelCopyWithImpl<$Res>
    implements $ContactUsAddressModelCopyWith<$Res> {
  _$ContactUsAddressModelCopyWithImpl(this._value, this._then);

  final ContactUsAddressModel _value;
  // ignore: unused_field
  final $Res Function(ContactUsAddressModel) _then;

  @override
  $Res call({
    Object? nameEnglish = freezed,
    Object? nameArabic = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
  }) {
    return _then(_value.copyWith(
      nameEnglish: nameEnglish == freezed
          ? _value.nameEnglish
          : nameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      nameArabic: nameArabic == freezed
          ? _value.nameArabic
          : nameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$$_ContactUsAddressModelCopyWith<$Res>
    implements $ContactUsAddressModelCopyWith<$Res> {
  factory _$$_ContactUsAddressModelCopyWith(_$_ContactUsAddressModel value,
          $Res Function(_$_ContactUsAddressModel) then) =
      __$$_ContactUsAddressModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String nameEnglish,
      String nameArabic,
      double longitude,
      double latitude});
}

/// @nodoc
class __$$_ContactUsAddressModelCopyWithImpl<$Res>
    extends _$ContactUsAddressModelCopyWithImpl<$Res>
    implements _$$_ContactUsAddressModelCopyWith<$Res> {
  __$$_ContactUsAddressModelCopyWithImpl(_$_ContactUsAddressModel _value,
      $Res Function(_$_ContactUsAddressModel) _then)
      : super(_value, (v) => _then(v as _$_ContactUsAddressModel));

  @override
  _$_ContactUsAddressModel get _value =>
      super._value as _$_ContactUsAddressModel;

  @override
  $Res call({
    Object? nameEnglish = freezed,
    Object? nameArabic = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
  }) {
    return _then(_$_ContactUsAddressModel(
      nameEnglish: nameEnglish == freezed
          ? _value.nameEnglish
          : nameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      nameArabic: nameArabic == freezed
          ? _value.nameArabic
          : nameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ContactUsAddressModel extends _ContactUsAddressModel {
  _$_ContactUsAddressModel(
      {required this.nameEnglish,
      required this.nameArabic,
      required this.longitude,
      required this.latitude})
      : super._();

  factory _$_ContactUsAddressModel.fromJson(Map<String, dynamic> json) =>
      _$$_ContactUsAddressModelFromJson(json);

  @override
  final String nameEnglish;
  @override
  final String nameArabic;
  @override
  final double longitude;
  @override
  final double latitude;

  @override
  String toString() {
    return 'ContactUsAddressModel(nameEnglish: $nameEnglish, nameArabic: $nameArabic, longitude: $longitude, latitude: $latitude)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ContactUsAddressModel &&
            const DeepCollectionEquality()
                .equals(other.nameEnglish, nameEnglish) &&
            const DeepCollectionEquality()
                .equals(other.nameArabic, nameArabic) &&
            const DeepCollectionEquality().equals(other.longitude, longitude) &&
            const DeepCollectionEquality().equals(other.latitude, latitude));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(nameEnglish),
      const DeepCollectionEquality().hash(nameArabic),
      const DeepCollectionEquality().hash(longitude),
      const DeepCollectionEquality().hash(latitude));

  @JsonKey(ignore: true)
  @override
  _$$_ContactUsAddressModelCopyWith<_$_ContactUsAddressModel> get copyWith =>
      __$$_ContactUsAddressModelCopyWithImpl<_$_ContactUsAddressModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ContactUsAddressModelToJson(
      this,
    );
  }
}

abstract class _ContactUsAddressModel extends ContactUsAddressModel {
  factory _ContactUsAddressModel(
      {required final String nameEnglish,
      required final String nameArabic,
      required final double longitude,
      required final double latitude}) = _$_ContactUsAddressModel;
  _ContactUsAddressModel._() : super._();

  factory _ContactUsAddressModel.fromJson(Map<String, dynamic> json) =
      _$_ContactUsAddressModel.fromJson;

  @override
  String get nameEnglish;
  @override
  String get nameArabic;
  @override
  double get longitude;
  @override
  double get latitude;
  @override
  @JsonKey(ignore: true)
  _$$_ContactUsAddressModelCopyWith<_$_ContactUsAddressModel> get copyWith =>
      throw _privateConstructorUsedError;
}
