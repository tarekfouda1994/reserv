import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'contact_us_address_model.freezed.dart';
part 'contact_us_address_model.g.dart';

@freezed
class ContactUsAddressModel with _$ContactUsAddressModel {
  ContactUsAddressModel._();

  @JsonSerializable(explicitToJson: true)
  factory ContactUsAddressModel({
    required String nameEnglish,
    required String nameArabic,
    required double longitude,
    required double latitude,
  }) = _ContactUsAddressModel;

  factory ContactUsAddressModel.fromJson(Map<String, dynamic> json) =>
      _$ContactUsAddressModelFromJson(json);

  String getAddressName() {
    return getIt<Utilities>().getLocalizedValue(nameArabic, nameEnglish);
  }
}
