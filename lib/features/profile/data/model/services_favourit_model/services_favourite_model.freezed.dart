// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'services_favourite_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServicesFavouriteModel _$ServicesFavouriteModelFromJson(
    Map<String, dynamic> json) {
  return _ServicesFavouriteModel.fromJson(json);
}

/// @nodoc
mixin _$ServicesFavouriteModel {
  String get businessID => throw _privateConstructorUsedError;
  set businessID(String value) => throw _privateConstructorUsedError;
  String get businessEnglishName => throw _privateConstructorUsedError;
  set businessEnglishName(String value) => throw _privateConstructorUsedError;
  String get businessArabicName => throw _privateConstructorUsedError;
  set businessArabicName(String value) => throw _privateConstructorUsedError;
  String get businessGender => throw _privateConstructorUsedError;
  set businessGender(String value) => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  set latitude(double value) => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  set longitude(double value) => throw _privateConstructorUsedError;
  String get businessAddressEN => throw _privateConstructorUsedError;
  set businessAddressEN(String value) => throw _privateConstructorUsedError;
  String get businessAddressAR => throw _privateConstructorUsedError;
  set businessAddressAR(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  String get distanceText => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  set distanceText(String value) => throw _privateConstructorUsedError;
  String get eBusinessServiceID => throw _privateConstructorUsedError;
  set eBusinessServiceID(String value) => throw _privateConstructorUsedError;
  String get businessServiceEnglishName => throw _privateConstructorUsedError;
  set businessServiceEnglishName(String value) =>
      throw _privateConstructorUsedError;
  String get businessServiceArabicName => throw _privateConstructorUsedError;
  set businessServiceArabicName(String value) =>
      throw _privateConstructorUsedError;
  num get servicePrice => throw _privateConstructorUsedError;
  set servicePrice(num value) => throw _privateConstructorUsedError;
  String get serviceID => throw _privateConstructorUsedError;
  set serviceID(String value) => throw _privateConstructorUsedError;
  String get serviceEnglishName => throw _privateConstructorUsedError;
  set serviceEnglishName(String value) => throw _privateConstructorUsedError;
  double get serviceRating => throw _privateConstructorUsedError;
  set serviceRating(double value) => throw _privateConstructorUsedError;
  double get businessRating => throw _privateConstructorUsedError;
  set businessRating(double value) => throw _privateConstructorUsedError;
  double get serviceDuration => throw _privateConstructorUsedError;
  set serviceDuration(double value) => throw _privateConstructorUsedError;
  String get serviceArabicName => throw _privateConstructorUsedError;
  set serviceArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceAvatar => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceAvatar(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: false)
  bool get isPercentage => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: false)
  set isPercentage(bool value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  double get discountPercentage => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  set discountPercentage(double value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  double get discount => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  set discount(double value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get discountPercentageValidDate => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set discountPercentageValidDate(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  num get discountPercentageServicePrice => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  set discountPercentageServicePrice(num value) =>
      throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyEnglishName => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyEnglishName(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyArabicName => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceTypeEnglishName => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceTypeEnglishName(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceTypeID => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceTypeID(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceTypeArabicName => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceTypeArabicName(String value) => throw _privateConstructorUsedError;
  bool get isDiscounted => throw _privateConstructorUsedError;
  set isDiscounted(bool value) => throw _privateConstructorUsedError;
  bool get hasActiveSlot => throw _privateConstructorUsedError;
  set hasActiveSlot(bool value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServicesFavouriteModelCopyWith<ServicesFavouriteModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServicesFavouriteModelCopyWith<$Res> {
  factory $ServicesFavouriteModelCopyWith(ServicesFavouriteModel value,
          $Res Function(ServicesFavouriteModel) then) =
      _$ServicesFavouriteModelCopyWithImpl<$Res>;
  $Res call(
      {String businessID,
      String businessEnglishName,
      String businessArabicName,
      String businessGender,
      double latitude,
      double longitude,
      String businessAddressEN,
      String businessAddressAR,
      @JsonKey(name: "distance")
          String distanceText,
      String eBusinessServiceID,
      String businessServiceEnglishName,
      String businessServiceArabicName,
      num servicePrice,
      String serviceID,
      String serviceEnglishName,
      double serviceRating,
      double businessRating,
      double serviceDuration,
      String serviceArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceAvatar,
      @JsonKey(nullable: true, defaultValue: false)
          bool isPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          double discountPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          double discount,
      @JsonKey(nullable: true, defaultValue: "")
          String discountPercentageValidDate,
      @JsonKey(nullable: true, defaultValue: 0)
          num discountPercentageServicePrice,
      @JsonKey(nullable: true, defaultValue: "")
          String currencyEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          String currencyArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceTypeEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceTypeID,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceTypeArabicName,
      bool isDiscounted,
      bool hasActiveSlot});
}

/// @nodoc
class _$ServicesFavouriteModelCopyWithImpl<$Res>
    implements $ServicesFavouriteModelCopyWith<$Res> {
  _$ServicesFavouriteModelCopyWithImpl(this._value, this._then);

  final ServicesFavouriteModel _value;
  // ignore: unused_field
  final $Res Function(ServicesFavouriteModel) _then;

  @override
  $Res call({
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? distanceText = freezed,
    Object? eBusinessServiceID = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? businessServiceArabicName = freezed,
    Object? servicePrice = freezed,
    Object? serviceID = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceRating = freezed,
    Object? businessRating = freezed,
    Object? serviceDuration = freezed,
    Object? serviceArabicName = freezed,
    Object? serviceAvatar = freezed,
    Object? isPercentage = freezed,
    Object? discountPercentage = freezed,
    Object? discount = freezed,
    Object? discountPercentageValidDate = freezed,
    Object? discountPercentageServicePrice = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeID = freezed,
    Object? serviceTypeArabicName = freezed,
    Object? isDiscounted = freezed,
    Object? hasActiveSlot = freezed,
  }) {
    return _then(_value.copyWith(
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      eBusinessServiceID: eBusinessServiceID == freezed
          ? _value.eBusinessServiceID
          : eBusinessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      servicePrice: servicePrice == freezed
          ? _value.servicePrice
          : servicePrice // ignore: cast_nullable_to_non_nullable
              as num,
      serviceID: serviceID == freezed
          ? _value.serviceID
          : serviceID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceRating: serviceRating == freezed
          ? _value.serviceRating
          : serviceRating // ignore: cast_nullable_to_non_nullable
              as double,
      businessRating: businessRating == freezed
          ? _value.businessRating
          : businessRating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as double,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAvatar: serviceAvatar == freezed
          ? _value.serviceAvatar
          : serviceAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      isPercentage: isPercentage == freezed
          ? _value.isPercentage
          : isPercentage // ignore: cast_nullable_to_non_nullable
              as bool,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as double,
      discountPercentageValidDate: discountPercentageValidDate == freezed
          ? _value.discountPercentageValidDate
          : discountPercentageValidDate // ignore: cast_nullable_to_non_nullable
              as String,
      discountPercentageServicePrice: discountPercentageServicePrice == freezed
          ? _value.discountPercentageServicePrice
          : discountPercentageServicePrice // ignore: cast_nullable_to_non_nullable
              as num,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeID: serviceTypeID == freezed
          ? _value.serviceTypeID
          : serviceTypeID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      isDiscounted: isDiscounted == freezed
          ? _value.isDiscounted
          : isDiscounted // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_ServicesFavouriteModelCopyWith<$Res>
    implements $ServicesFavouriteModelCopyWith<$Res> {
  factory _$$_ServicesFavouriteModelCopyWith(_$_ServicesFavouriteModel value,
          $Res Function(_$_ServicesFavouriteModel) then) =
      __$$_ServicesFavouriteModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String businessID,
      String businessEnglishName,
      String businessArabicName,
      String businessGender,
      double latitude,
      double longitude,
      String businessAddressEN,
      String businessAddressAR,
      @JsonKey(name: "distance")
          String distanceText,
      String eBusinessServiceID,
      String businessServiceEnglishName,
      String businessServiceArabicName,
      num servicePrice,
      String serviceID,
      String serviceEnglishName,
      double serviceRating,
      double businessRating,
      double serviceDuration,
      String serviceArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceAvatar,
      @JsonKey(nullable: true, defaultValue: false)
          bool isPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          double discountPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          double discount,
      @JsonKey(nullable: true, defaultValue: "")
          String discountPercentageValidDate,
      @JsonKey(nullable: true, defaultValue: 0)
          num discountPercentageServicePrice,
      @JsonKey(nullable: true, defaultValue: "")
          String currencyEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          String currencyArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceTypeEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceTypeID,
      @JsonKey(nullable: true, defaultValue: "")
          String serviceTypeArabicName,
      bool isDiscounted,
      bool hasActiveSlot});
}

/// @nodoc
class __$$_ServicesFavouriteModelCopyWithImpl<$Res>
    extends _$ServicesFavouriteModelCopyWithImpl<$Res>
    implements _$$_ServicesFavouriteModelCopyWith<$Res> {
  __$$_ServicesFavouriteModelCopyWithImpl(_$_ServicesFavouriteModel _value,
      $Res Function(_$_ServicesFavouriteModel) _then)
      : super(_value, (v) => _then(v as _$_ServicesFavouriteModel));

  @override
  _$_ServicesFavouriteModel get _value =>
      super._value as _$_ServicesFavouriteModel;

  @override
  $Res call({
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? distanceText = freezed,
    Object? eBusinessServiceID = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? businessServiceArabicName = freezed,
    Object? servicePrice = freezed,
    Object? serviceID = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceRating = freezed,
    Object? businessRating = freezed,
    Object? serviceDuration = freezed,
    Object? serviceArabicName = freezed,
    Object? serviceAvatar = freezed,
    Object? isPercentage = freezed,
    Object? discountPercentage = freezed,
    Object? discount = freezed,
    Object? discountPercentageValidDate = freezed,
    Object? discountPercentageServicePrice = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeID = freezed,
    Object? serviceTypeArabicName = freezed,
    Object? isDiscounted = freezed,
    Object? hasActiveSlot = freezed,
  }) {
    return _then(_$_ServicesFavouriteModel(
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      eBusinessServiceID: eBusinessServiceID == freezed
          ? _value.eBusinessServiceID
          : eBusinessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      servicePrice: servicePrice == freezed
          ? _value.servicePrice
          : servicePrice // ignore: cast_nullable_to_non_nullable
              as num,
      serviceID: serviceID == freezed
          ? _value.serviceID
          : serviceID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceRating: serviceRating == freezed
          ? _value.serviceRating
          : serviceRating // ignore: cast_nullable_to_non_nullable
              as double,
      businessRating: businessRating == freezed
          ? _value.businessRating
          : businessRating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as double,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAvatar: serviceAvatar == freezed
          ? _value.serviceAvatar
          : serviceAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      isPercentage: isPercentage == freezed
          ? _value.isPercentage
          : isPercentage // ignore: cast_nullable_to_non_nullable
              as bool,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as double,
      discountPercentageValidDate: discountPercentageValidDate == freezed
          ? _value.discountPercentageValidDate
          : discountPercentageValidDate // ignore: cast_nullable_to_non_nullable
              as String,
      discountPercentageServicePrice: discountPercentageServicePrice == freezed
          ? _value.discountPercentageServicePrice
          : discountPercentageServicePrice // ignore: cast_nullable_to_non_nullable
              as num,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeID: serviceTypeID == freezed
          ? _value.serviceTypeID
          : serviceTypeID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      isDiscounted: isDiscounted == freezed
          ? _value.isDiscounted
          : isDiscounted // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServicesFavouriteModel extends _ServicesFavouriteModel {
  _$_ServicesFavouriteModel(
      {required this.businessID,
      required this.businessEnglishName,
      required this.businessArabicName,
      required this.businessGender,
      required this.latitude,
      required this.longitude,
      required this.businessAddressEN,
      required this.businessAddressAR,
      @JsonKey(name: "distance")
          required this.distanceText,
      required this.eBusinessServiceID,
      required this.businessServiceEnglishName,
      required this.businessServiceArabicName,
      required this.servicePrice,
      required this.serviceID,
      required this.serviceEnglishName,
      required this.serviceRating,
      required this.businessRating,
      required this.serviceDuration,
      required this.serviceArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          required this.serviceAvatar,
      @JsonKey(nullable: true, defaultValue: false)
          required this.isPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          required this.discountPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          required this.discount,
      @JsonKey(nullable: true, defaultValue: "")
          required this.discountPercentageValidDate,
      @JsonKey(nullable: true, defaultValue: 0)
          required this.discountPercentageServicePrice,
      @JsonKey(nullable: true, defaultValue: "")
          required this.currencyEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          required this.currencyArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          required this.serviceTypeEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          required this.serviceTypeID,
      @JsonKey(nullable: true, defaultValue: "")
          required this.serviceTypeArabicName,
      required this.isDiscounted,
      required this.hasActiveSlot})
      : super._();

  factory _$_ServicesFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServicesFavouriteModelFromJson(json);

  @override
  String businessID;
  @override
  String businessEnglishName;
  @override
  String businessArabicName;
  @override
  String businessGender;
  @override
  double latitude;
  @override
  double longitude;
  @override
  String businessAddressEN;
  @override
  String businessAddressAR;
  @override
  @JsonKey(name: "distance")
  String distanceText;
  @override
  String eBusinessServiceID;
  @override
  String businessServiceEnglishName;
  @override
  String businessServiceArabicName;
  @override
  num servicePrice;
  @override
  String serviceID;
  @override
  String serviceEnglishName;
  @override
  double serviceRating;
  @override
  double businessRating;
  @override
  double serviceDuration;
  @override
  String serviceArabicName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String serviceAvatar;
  @override
  @JsonKey(nullable: true, defaultValue: false)
  bool isPercentage;
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  double discountPercentage;
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  double discount;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String discountPercentageValidDate;
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  num discountPercentageServicePrice;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String currencyEnglishName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String currencyArabicName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String serviceTypeEnglishName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String serviceTypeID;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String serviceTypeArabicName;
  @override
  bool isDiscounted;
  @override
  bool hasActiveSlot;

  @override
  String toString() {
    return 'ServicesFavouriteModel(businessID: $businessID, businessEnglishName: $businessEnglishName, businessArabicName: $businessArabicName, businessGender: $businessGender, latitude: $latitude, longitude: $longitude, businessAddressEN: $businessAddressEN, businessAddressAR: $businessAddressAR, distanceText: $distanceText, eBusinessServiceID: $eBusinessServiceID, businessServiceEnglishName: $businessServiceEnglishName, businessServiceArabicName: $businessServiceArabicName, servicePrice: $servicePrice, serviceID: $serviceID, serviceEnglishName: $serviceEnglishName, serviceRating: $serviceRating, businessRating: $businessRating, serviceDuration: $serviceDuration, serviceArabicName: $serviceArabicName, serviceAvatar: $serviceAvatar, isPercentage: $isPercentage, discountPercentage: $discountPercentage, discount: $discount, discountPercentageValidDate: $discountPercentageValidDate, discountPercentageServicePrice: $discountPercentageServicePrice, currencyEnglishName: $currencyEnglishName, currencyArabicName: $currencyArabicName, serviceTypeEnglishName: $serviceTypeEnglishName, serviceTypeID: $serviceTypeID, serviceTypeArabicName: $serviceTypeArabicName, isDiscounted: $isDiscounted, hasActiveSlot: $hasActiveSlot)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_ServicesFavouriteModelCopyWith<_$_ServicesFavouriteModel> get copyWith =>
      __$$_ServicesFavouriteModelCopyWithImpl<_$_ServicesFavouriteModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServicesFavouriteModelToJson(
      this,
    );
  }
}

abstract class _ServicesFavouriteModel extends ServicesFavouriteModel {
  factory _ServicesFavouriteModel(
      {required String businessID,
      required String businessEnglishName,
      required String businessArabicName,
      required String businessGender,
      required double latitude,
      required double longitude,
      required String businessAddressEN,
      required String businessAddressAR,
      @JsonKey(name: "distance")
          required String distanceText,
      required String eBusinessServiceID,
      required String businessServiceEnglishName,
      required String businessServiceArabicName,
      required num servicePrice,
      required String serviceID,
      required String serviceEnglishName,
      required double serviceRating,
      required double businessRating,
      required double serviceDuration,
      required String serviceArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          required String serviceAvatar,
      @JsonKey(nullable: true, defaultValue: false)
          required bool isPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          required double discountPercentage,
      @JsonKey(nullable: true, defaultValue: 0)
          required double discount,
      @JsonKey(nullable: true, defaultValue: "")
          required String discountPercentageValidDate,
      @JsonKey(nullable: true, defaultValue: 0)
          required num discountPercentageServicePrice,
      @JsonKey(nullable: true, defaultValue: "")
          required String currencyEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          required String currencyArabicName,
      @JsonKey(nullable: true, defaultValue: "")
          required String serviceTypeEnglishName,
      @JsonKey(nullable: true, defaultValue: "")
          required String serviceTypeID,
      @JsonKey(nullable: true, defaultValue: "")
          required String serviceTypeArabicName,
      required bool isDiscounted,
      required bool hasActiveSlot}) = _$_ServicesFavouriteModel;
  _ServicesFavouriteModel._() : super._();

  factory _ServicesFavouriteModel.fromJson(Map<String, dynamic> json) =
      _$_ServicesFavouriteModel.fromJson;

  @override
  String get businessID;
  set businessID(String value);
  @override
  String get businessEnglishName;
  set businessEnglishName(String value);
  @override
  String get businessArabicName;
  set businessArabicName(String value);
  @override
  String get businessGender;
  set businessGender(String value);
  @override
  double get latitude;
  set latitude(double value);
  @override
  double get longitude;
  set longitude(double value);
  @override
  String get businessAddressEN;
  set businessAddressEN(String value);
  @override
  String get businessAddressAR;
  set businessAddressAR(String value);
  @override
  @JsonKey(name: "distance")
  String get distanceText;
  @JsonKey(name: "distance")
  set distanceText(String value);
  @override
  String get eBusinessServiceID;
  set eBusinessServiceID(String value);
  @override
  String get businessServiceEnglishName;
  set businessServiceEnglishName(String value);
  @override
  String get businessServiceArabicName;
  set businessServiceArabicName(String value);
  @override
  num get servicePrice;
  set servicePrice(num value);
  @override
  String get serviceID;
  set serviceID(String value);
  @override
  String get serviceEnglishName;
  set serviceEnglishName(String value);
  @override
  double get serviceRating;
  set serviceRating(double value);
  @override
  double get businessRating;
  set businessRating(double value);
  @override
  double get serviceDuration;
  set serviceDuration(double value);
  @override
  String get serviceArabicName;
  set serviceArabicName(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceAvatar;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceAvatar(String value);
  @override
  @JsonKey(nullable: true, defaultValue: false)
  bool get isPercentage;
  @JsonKey(nullable: true, defaultValue: false)
  set isPercentage(bool value);
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  double get discountPercentage;
  @JsonKey(nullable: true, defaultValue: 0)
  set discountPercentage(double value);
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  double get discount;
  @JsonKey(nullable: true, defaultValue: 0)
  set discount(double value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get discountPercentageValidDate;
  @JsonKey(nullable: true, defaultValue: "")
  set discountPercentageValidDate(String value);
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  num get discountPercentageServicePrice;
  @JsonKey(nullable: true, defaultValue: 0)
  set discountPercentageServicePrice(num value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyEnglishName;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyEnglishName(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyArabicName;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyArabicName(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceTypeEnglishName;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceTypeEnglishName(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceTypeID;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceTypeID(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceTypeArabicName;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceTypeArabicName(String value);
  @override
  bool get isDiscounted;
  set isDiscounted(bool value);
  @override
  bool get hasActiveSlot;
  set hasActiveSlot(bool value);
  @override
  @JsonKey(ignore: true)
  _$$_ServicesFavouriteModelCopyWith<_$_ServicesFavouriteModel> get copyWith =>
      throw _privateConstructorUsedError;
}
