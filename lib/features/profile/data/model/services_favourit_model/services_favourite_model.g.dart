// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'services_favourite_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServicesFavouriteModel _$$_ServicesFavouriteModelFromJson(
        Map<String, dynamic> json) =>
    _$_ServicesFavouriteModel(
      businessID: json['businessID'] as String,
      businessEnglishName: json['businessEnglishName'] as String,
      businessArabicName: json['businessArabicName'] as String,
      businessGender: json['businessGender'] as String,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      businessAddressEN: json['businessAddressEN'] as String,
      businessAddressAR: json['businessAddressAR'] as String,
      distanceText: json['distance'] as String,
      eBusinessServiceID: json['eBusinessServiceID'] as String,
      businessServiceEnglishName: json['businessServiceEnglishName'] as String,
      businessServiceArabicName: json['businessServiceArabicName'] as String,
      servicePrice: json['servicePrice'] as num,
      serviceID: json['serviceID'] as String,
      serviceEnglishName: json['serviceEnglishName'] as String,
      serviceRating: (json['serviceRating'] as num).toDouble(),
      businessRating: (json['businessRating'] as num).toDouble(),
      serviceDuration: (json['serviceDuration'] as num).toDouble(),
      serviceArabicName: json['serviceArabicName'] as String,
      serviceAvatar: json['serviceAvatar'] as String? ?? '',
      isPercentage: json['isPercentage'] as bool? ?? false,
      discountPercentage: (json['discountPercentage'] as num?)?.toDouble() ?? 0,
      discount: (json['discount'] as num?)?.toDouble() ?? 0,
      discountPercentageValidDate:
          json['discountPercentageValidDate'] as String? ?? '',
      discountPercentageServicePrice:
          json['discountPercentageServicePrice'] as num? ?? 0,
      currencyEnglishName: json['currencyEnglishName'] as String? ?? '',
      currencyArabicName: json['currencyArabicName'] as String? ?? '',
      serviceTypeEnglishName: json['serviceTypeEnglishName'] as String? ?? '',
      serviceTypeID: json['serviceTypeID'] as String? ?? '',
      serviceTypeArabicName: json['serviceTypeArabicName'] as String? ?? '',
      isDiscounted: json['isDiscounted'] as bool,
      hasActiveSlot: json['hasActiveSlot'] as bool,
    );

Map<String, dynamic> _$$_ServicesFavouriteModelToJson(
        _$_ServicesFavouriteModel instance) =>
    <String, dynamic>{
      'businessID': instance.businessID,
      'businessEnglishName': instance.businessEnglishName,
      'businessArabicName': instance.businessArabicName,
      'businessGender': instance.businessGender,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'businessAddressEN': instance.businessAddressEN,
      'businessAddressAR': instance.businessAddressAR,
      'distance': instance.distanceText,
      'eBusinessServiceID': instance.eBusinessServiceID,
      'businessServiceEnglishName': instance.businessServiceEnglishName,
      'businessServiceArabicName': instance.businessServiceArabicName,
      'servicePrice': instance.servicePrice,
      'serviceID': instance.serviceID,
      'serviceEnglishName': instance.serviceEnglishName,
      'serviceRating': instance.serviceRating,
      'businessRating': instance.businessRating,
      'serviceDuration': instance.serviceDuration,
      'serviceArabicName': instance.serviceArabicName,
      'serviceAvatar': instance.serviceAvatar,
      'isPercentage': instance.isPercentage,
      'discountPercentage': instance.discountPercentage,
      'discount': instance.discount,
      'discountPercentageValidDate': instance.discountPercentageValidDate,
      'discountPercentageServicePrice': instance.discountPercentageServicePrice,
      'currencyEnglishName': instance.currencyEnglishName,
      'currencyArabicName': instance.currencyArabicName,
      'serviceTypeEnglishName': instance.serviceTypeEnglishName,
      'serviceTypeID': instance.serviceTypeID,
      'serviceTypeArabicName': instance.serviceTypeArabicName,
      'isDiscounted': instance.isDiscounted,
      'hasActiveSlot': instance.hasActiveSlot,
    };
