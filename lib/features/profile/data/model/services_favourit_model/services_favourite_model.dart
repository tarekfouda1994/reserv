import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'services_favourite_model.freezed.dart';

part 'services_favourite_model.g.dart';

@unfreezed
class ServicesFavouriteModel with _$ServicesFavouriteModel {
  ServicesFavouriteModel._();

  @JsonSerializable(explicitToJson: true)
  factory ServicesFavouriteModel({
    required String businessID,
    required String businessEnglishName,
    required String businessArabicName,
    required String businessGender,
    required double latitude,
    required double longitude,
    required String businessAddressEN,
    required String businessAddressAR,
    @JsonKey(name: "distance") required String distanceText,
    required String eBusinessServiceID,
    required String businessServiceEnglishName,
    required String businessServiceArabicName,
    required num servicePrice,
    required String serviceID,
    required String serviceEnglishName,
    required double serviceRating,
    required double businessRating,
    required double serviceDuration,
    required String serviceArabicName,
    @JsonKey(nullable: true, defaultValue: "") required String serviceAvatar,
    @JsonKey(nullable: true, defaultValue: false) required bool isPercentage,
    @JsonKey(nullable: true, defaultValue: 0) required double discountPercentage,
    @JsonKey(nullable: true, defaultValue: 0) required double discount,
    @JsonKey(nullable: true, defaultValue: "") required String discountPercentageValidDate,
    @JsonKey(nullable: true, defaultValue: 0) required num discountPercentageServicePrice,
    @JsonKey(nullable: true, defaultValue: "") required String currencyEnglishName,
    @JsonKey(nullable: true, defaultValue: "") required String currencyArabicName,
    @JsonKey(nullable: true, defaultValue: "") required String serviceTypeEnglishName,
    @JsonKey(nullable: true, defaultValue: "") required String serviceTypeID,
    @JsonKey(nullable: true, defaultValue: "") required String serviceTypeArabicName,
    required bool isDiscounted,
    required bool hasActiveSlot,
  }) = _ServicesFavouriteModel;

  factory ServicesFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$ServicesFavouriteModelFromJson(json);

  String get _distance {
    BuildContext context = getIt<GlobalContext>().context();
    var distance = getIt<Utilities>().convertNumToAr(
        context: context,
        value: distanceText
            .toString()
            .split(" ",).first,
        isDistance: true);
    if (distanceText == "" || distanceText == "0" || distanceText == "0.00") {
      return "";
    } else {
      return "$distance ${tr("km")}";
    }
  }

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(serviceArabicName, serviceEnglishName);
  }

  String getServicesAddress() {
    return getIt<Utilities>().getLocalizedValue(businessAddressAR, businessAddressEN);
  }

  String getBusiness() {
    return getIt<Utilities>().getLocalizedValue(businessArabicName, businessEnglishName);
  }

  String getCurrencyName() {
    return getIt<Utilities>().getLocalizedValue(currencyArabicName, currencyEnglishName);
  }

  String distance(BuildContext context) {
    if (_distance.isEmpty) {
      return "";
    } else {
      var device = context.read<DeviceCubit>().state.model;
      var isEn = device.locale == Locale('en', 'US');
      if (isEn) {
        return "$_distance ${tr("away")}";
      } else {
        return "${tr("away")} $_distance";
      }
    }
  }
}
