import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/profile/data/model/branche_model/branche_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'contact_us_model.freezed.dart';
part 'contact_us_model.g.dart';

@freezed
class ContactUsModel with _$ContactUsModel {
  ContactUsModel._();

  @JsonSerializable(explicitToJson: true)
  factory ContactUsModel({
    required String officeTimingEnglish,
    required String officeTimingArabic,
    required String phoneNo,
    required String webSite,
    required List<BrancheModel> branches,
  }) = _ContactUsModel;

  factory ContactUsModel.fromJson(Map<String, dynamic> json) => _$ContactUsModelFromJson(json);

  String getOfficeTimingName() {
    return getIt<Utilities>().getLocalizedValue(officeTimingArabic, officeTimingEnglish);
  }
}
