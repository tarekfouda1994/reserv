// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'contact_us_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ContactUsModel _$ContactUsModelFromJson(Map<String, dynamic> json) {
  return _ContactUsModel.fromJson(json);
}

/// @nodoc
mixin _$ContactUsModel {
  String get officeTimingEnglish => throw _privateConstructorUsedError;
  String get officeTimingArabic => throw _privateConstructorUsedError;
  String get phoneNo => throw _privateConstructorUsedError;
  String get webSite => throw _privateConstructorUsedError;
  List<BrancheModel> get branches => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ContactUsModelCopyWith<ContactUsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ContactUsModelCopyWith<$Res> {
  factory $ContactUsModelCopyWith(
          ContactUsModel value, $Res Function(ContactUsModel) then) =
      _$ContactUsModelCopyWithImpl<$Res>;
  $Res call(
      {String officeTimingEnglish,
      String officeTimingArabic,
      String phoneNo,
      String webSite,
      List<BrancheModel> branches});
}

/// @nodoc
class _$ContactUsModelCopyWithImpl<$Res>
    implements $ContactUsModelCopyWith<$Res> {
  _$ContactUsModelCopyWithImpl(this._value, this._then);

  final ContactUsModel _value;
  // ignore: unused_field
  final $Res Function(ContactUsModel) _then;

  @override
  $Res call({
    Object? officeTimingEnglish = freezed,
    Object? officeTimingArabic = freezed,
    Object? phoneNo = freezed,
    Object? webSite = freezed,
    Object? branches = freezed,
  }) {
    return _then(_value.copyWith(
      officeTimingEnglish: officeTimingEnglish == freezed
          ? _value.officeTimingEnglish
          : officeTimingEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      officeTimingArabic: officeTimingArabic == freezed
          ? _value.officeTimingArabic
          : officeTimingArabic // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNo: phoneNo == freezed
          ? _value.phoneNo
          : phoneNo // ignore: cast_nullable_to_non_nullable
              as String,
      webSite: webSite == freezed
          ? _value.webSite
          : webSite // ignore: cast_nullable_to_non_nullable
              as String,
      branches: branches == freezed
          ? _value.branches
          : branches // ignore: cast_nullable_to_non_nullable
              as List<BrancheModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_ContactUsModelCopyWith<$Res>
    implements $ContactUsModelCopyWith<$Res> {
  factory _$$_ContactUsModelCopyWith(
          _$_ContactUsModel value, $Res Function(_$_ContactUsModel) then) =
      __$$_ContactUsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String officeTimingEnglish,
      String officeTimingArabic,
      String phoneNo,
      String webSite,
      List<BrancheModel> branches});
}

/// @nodoc
class __$$_ContactUsModelCopyWithImpl<$Res>
    extends _$ContactUsModelCopyWithImpl<$Res>
    implements _$$_ContactUsModelCopyWith<$Res> {
  __$$_ContactUsModelCopyWithImpl(
      _$_ContactUsModel _value, $Res Function(_$_ContactUsModel) _then)
      : super(_value, (v) => _then(v as _$_ContactUsModel));

  @override
  _$_ContactUsModel get _value => super._value as _$_ContactUsModel;

  @override
  $Res call({
    Object? officeTimingEnglish = freezed,
    Object? officeTimingArabic = freezed,
    Object? phoneNo = freezed,
    Object? webSite = freezed,
    Object? branches = freezed,
  }) {
    return _then(_$_ContactUsModel(
      officeTimingEnglish: officeTimingEnglish == freezed
          ? _value.officeTimingEnglish
          : officeTimingEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      officeTimingArabic: officeTimingArabic == freezed
          ? _value.officeTimingArabic
          : officeTimingArabic // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNo: phoneNo == freezed
          ? _value.phoneNo
          : phoneNo // ignore: cast_nullable_to_non_nullable
              as String,
      webSite: webSite == freezed
          ? _value.webSite
          : webSite // ignore: cast_nullable_to_non_nullable
              as String,
      branches: branches == freezed
          ? _value._branches
          : branches // ignore: cast_nullable_to_non_nullable
              as List<BrancheModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ContactUsModel extends _ContactUsModel {
  _$_ContactUsModel(
      {required this.officeTimingEnglish,
      required this.officeTimingArabic,
      required this.phoneNo,
      required this.webSite,
      required final List<BrancheModel> branches})
      : _branches = branches,
        super._();

  factory _$_ContactUsModel.fromJson(Map<String, dynamic> json) =>
      _$$_ContactUsModelFromJson(json);

  @override
  final String officeTimingEnglish;
  @override
  final String officeTimingArabic;
  @override
  final String phoneNo;
  @override
  final String webSite;
  final List<BrancheModel> _branches;
  @override
  List<BrancheModel> get branches {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_branches);
  }

  @override
  String toString() {
    return 'ContactUsModel(officeTimingEnglish: $officeTimingEnglish, officeTimingArabic: $officeTimingArabic, phoneNo: $phoneNo, webSite: $webSite, branches: $branches)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ContactUsModel &&
            const DeepCollectionEquality()
                .equals(other.officeTimingEnglish, officeTimingEnglish) &&
            const DeepCollectionEquality()
                .equals(other.officeTimingArabic, officeTimingArabic) &&
            const DeepCollectionEquality().equals(other.phoneNo, phoneNo) &&
            const DeepCollectionEquality().equals(other.webSite, webSite) &&
            const DeepCollectionEquality().equals(other._branches, _branches));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(officeTimingEnglish),
      const DeepCollectionEquality().hash(officeTimingArabic),
      const DeepCollectionEquality().hash(phoneNo),
      const DeepCollectionEquality().hash(webSite),
      const DeepCollectionEquality().hash(_branches));

  @JsonKey(ignore: true)
  @override
  _$$_ContactUsModelCopyWith<_$_ContactUsModel> get copyWith =>
      __$$_ContactUsModelCopyWithImpl<_$_ContactUsModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ContactUsModelToJson(
      this,
    );
  }
}

abstract class _ContactUsModel extends ContactUsModel {
  factory _ContactUsModel(
      {required final String officeTimingEnglish,
      required final String officeTimingArabic,
      required final String phoneNo,
      required final String webSite,
      required final List<BrancheModel> branches}) = _$_ContactUsModel;
  _ContactUsModel._() : super._();

  factory _ContactUsModel.fromJson(Map<String, dynamic> json) =
      _$_ContactUsModel.fromJson;

  @override
  String get officeTimingEnglish;
  @override
  String get officeTimingArabic;
  @override
  String get phoneNo;
  @override
  String get webSite;
  @override
  List<BrancheModel> get branches;
  @override
  @JsonKey(ignore: true)
  _$$_ContactUsModelCopyWith<_$_ContactUsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
