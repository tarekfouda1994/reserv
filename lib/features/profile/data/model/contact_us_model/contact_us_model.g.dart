// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_us_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ContactUsModel _$$_ContactUsModelFromJson(Map<String, dynamic> json) =>
    _$_ContactUsModel(
      officeTimingEnglish: json['officeTimingEnglish'] as String,
      officeTimingArabic: json['officeTimingArabic'] as String,
      phoneNo: json['phoneNo'] as String,
      webSite: json['webSite'] as String,
      branches: (json['branches'] as List<dynamic>)
          .map((e) => BrancheModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ContactUsModelToJson(_$_ContactUsModel instance) =>
    <String, dynamic>{
      'officeTimingEnglish': instance.officeTimingEnglish,
      'officeTimingArabic': instance.officeTimingArabic,
      'phoneNo': instance.phoneNo,
      'webSite': instance.webSite,
      'branches': instance.branches.map((e) => e.toJson()).toList(),
    };
