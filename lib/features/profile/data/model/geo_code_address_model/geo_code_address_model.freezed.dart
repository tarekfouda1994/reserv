// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'geo_code_address_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

GeoCodeAddressModel _$GeoCodeAddressModelFromJson(Map<String, dynamic> json) {
  return _GeoCodeAddressModel.fromJson(json);
}

/// @nodoc
mixin _$GeoCodeAddressModel {
  @JsonKey(name: "staddress", defaultValue: "")
  dynamic get street => throw _privateConstructorUsedError;
  @JsonKey(name: "city", defaultValue: "")
  String get city => throw _privateConstructorUsedError;
  @JsonKey(name: "country", defaultValue: "")
  String get country => throw _privateConstructorUsedError;
  @JsonKey(name: "region", defaultValue: "")
  String? get region => throw _privateConstructorUsedError;
  @JsonKey(name: "state", defaultValue: "")
  String? get state => throw _privateConstructorUsedError;
  @JsonKey(name: "prov", defaultValue: "")
  String? get prov => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GeoCodeAddressModelCopyWith<GeoCodeAddressModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GeoCodeAddressModelCopyWith<$Res> {
  factory $GeoCodeAddressModelCopyWith(
          GeoCodeAddressModel value, $Res Function(GeoCodeAddressModel) then) =
      _$GeoCodeAddressModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: "staddress", defaultValue: "") dynamic street,
      @JsonKey(name: "city", defaultValue: "") String city,
      @JsonKey(name: "country", defaultValue: "") String country,
      @JsonKey(name: "region", defaultValue: "") String? region,
      @JsonKey(name: "state", defaultValue: "") String? state,
      @JsonKey(name: "prov", defaultValue: "") String? prov});
}

/// @nodoc
class _$GeoCodeAddressModelCopyWithImpl<$Res>
    implements $GeoCodeAddressModelCopyWith<$Res> {
  _$GeoCodeAddressModelCopyWithImpl(this._value, this._then);

  final GeoCodeAddressModel _value;
  // ignore: unused_field
  final $Res Function(GeoCodeAddressModel) _then;

  @override
  $Res call({
    Object? street = freezed,
    Object? city = freezed,
    Object? country = freezed,
    Object? region = freezed,
    Object? state = freezed,
    Object? prov = freezed,
  }) {
    return _then(_value.copyWith(
      street: street == freezed
          ? _value.street
          : street // ignore: cast_nullable_to_non_nullable
              as dynamic,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      region: region == freezed
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String?,
      state: state == freezed
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      prov: prov == freezed
          ? _value.prov
          : prov // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_GeoCodeAddressModelCopyWith<$Res>
    implements $GeoCodeAddressModelCopyWith<$Res> {
  factory _$$_GeoCodeAddressModelCopyWith(_$_GeoCodeAddressModel value,
          $Res Function(_$_GeoCodeAddressModel) then) =
      __$$_GeoCodeAddressModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: "staddress", defaultValue: "") dynamic street,
      @JsonKey(name: "city", defaultValue: "") String city,
      @JsonKey(name: "country", defaultValue: "") String country,
      @JsonKey(name: "region", defaultValue: "") String? region,
      @JsonKey(name: "state", defaultValue: "") String? state,
      @JsonKey(name: "prov", defaultValue: "") String? prov});
}

/// @nodoc
class __$$_GeoCodeAddressModelCopyWithImpl<$Res>
    extends _$GeoCodeAddressModelCopyWithImpl<$Res>
    implements _$$_GeoCodeAddressModelCopyWith<$Res> {
  __$$_GeoCodeAddressModelCopyWithImpl(_$_GeoCodeAddressModel _value,
      $Res Function(_$_GeoCodeAddressModel) _then)
      : super(_value, (v) => _then(v as _$_GeoCodeAddressModel));

  @override
  _$_GeoCodeAddressModel get _value => super._value as _$_GeoCodeAddressModel;

  @override
  $Res call({
    Object? street = freezed,
    Object? city = freezed,
    Object? country = freezed,
    Object? region = freezed,
    Object? state = freezed,
    Object? prov = freezed,
  }) {
    return _then(_$_GeoCodeAddressModel(
      street: street == freezed
          ? _value.street
          : street // ignore: cast_nullable_to_non_nullable
              as dynamic,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String,
      region: region == freezed
          ? _value.region
          : region // ignore: cast_nullable_to_non_nullable
              as String?,
      state: state == freezed
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      prov: prov == freezed
          ? _value.prov
          : prov // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_GeoCodeAddressModel extends _GeoCodeAddressModel {
  _$_GeoCodeAddressModel(
      {@JsonKey(name: "staddress", defaultValue: "") this.street,
      @JsonKey(name: "city", defaultValue: "") required this.city,
      @JsonKey(name: "country", defaultValue: "") required this.country,
      @JsonKey(name: "region", defaultValue: "") this.region,
      @JsonKey(name: "state", defaultValue: "") this.state,
      @JsonKey(name: "prov", defaultValue: "") this.prov})
      : super._();

  factory _$_GeoCodeAddressModel.fromJson(Map<String, dynamic> json) =>
      _$$_GeoCodeAddressModelFromJson(json);

  @override
  @JsonKey(name: "staddress", defaultValue: "")
  final dynamic street;
  @override
  @JsonKey(name: "city", defaultValue: "")
  final String city;
  @override
  @JsonKey(name: "country", defaultValue: "")
  final String country;
  @override
  @JsonKey(name: "region", defaultValue: "")
  final String? region;
  @override
  @JsonKey(name: "state", defaultValue: "")
  final String? state;
  @override
  @JsonKey(name: "prov", defaultValue: "")
  final String? prov;

  @override
  String toString() {
    return 'GeoCodeAddressModel(street: $street, city: $city, country: $country, region: $region, state: $state, prov: $prov)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GeoCodeAddressModel &&
            const DeepCollectionEquality().equals(other.street, street) &&
            const DeepCollectionEquality().equals(other.city, city) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality().equals(other.region, region) &&
            const DeepCollectionEquality().equals(other.state, state) &&
            const DeepCollectionEquality().equals(other.prov, prov));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(street),
      const DeepCollectionEquality().hash(city),
      const DeepCollectionEquality().hash(country),
      const DeepCollectionEquality().hash(region),
      const DeepCollectionEquality().hash(state),
      const DeepCollectionEquality().hash(prov));

  @JsonKey(ignore: true)
  @override
  _$$_GeoCodeAddressModelCopyWith<_$_GeoCodeAddressModel> get copyWith =>
      __$$_GeoCodeAddressModelCopyWithImpl<_$_GeoCodeAddressModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GeoCodeAddressModelToJson(
      this,
    );
  }
}

abstract class _GeoCodeAddressModel extends GeoCodeAddressModel {
  factory _GeoCodeAddressModel(
      {@JsonKey(name: "staddress", defaultValue: "")
          final dynamic street,
      @JsonKey(name: "city", defaultValue: "")
          required final String city,
      @JsonKey(name: "country", defaultValue: "")
          required final String country,
      @JsonKey(name: "region", defaultValue: "")
          final String? region,
      @JsonKey(name: "state", defaultValue: "")
          final String? state,
      @JsonKey(name: "prov", defaultValue: "")
          final String? prov}) = _$_GeoCodeAddressModel;
  _GeoCodeAddressModel._() : super._();

  factory _GeoCodeAddressModel.fromJson(Map<String, dynamic> json) =
      _$_GeoCodeAddressModel.fromJson;

  @override
  @JsonKey(name: "staddress", defaultValue: "")
  dynamic get street;
  @override
  @JsonKey(name: "city", defaultValue: "")
  String get city;
  @override
  @JsonKey(name: "country", defaultValue: "")
  String get country;
  @override
  @JsonKey(name: "region", defaultValue: "")
  String? get region;
  @override
  @JsonKey(name: "state", defaultValue: "")
  String? get state;
  @override
  @JsonKey(name: "prov", defaultValue: "")
  String? get prov;
  @override
  @JsonKey(ignore: true)
  _$$_GeoCodeAddressModelCopyWith<_$_GeoCodeAddressModel> get copyWith =>
      throw _privateConstructorUsedError;
}
