// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_code_address_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_GeoCodeAddressModel _$$_GeoCodeAddressModelFromJson(
        Map<String, dynamic> json) =>
    _$_GeoCodeAddressModel(
      street: json['staddress'] ?? '',
      city: json['city'] as String? ?? '',
      country: json['country'] as String? ?? '',
      region: json['region'] as String? ?? '',
      state: json['state'] as String? ?? '',
      prov: json['prov'] as String? ?? '',
    );

Map<String, dynamic> _$$_GeoCodeAddressModelToJson(
        _$_GeoCodeAddressModel instance) =>
    <String, dynamic>{
      'staddress': instance.street,
      'city': instance.city,
      'country': instance.country,
      'region': instance.region,
      'state': instance.state,
      'prov': instance.prov,
    };
