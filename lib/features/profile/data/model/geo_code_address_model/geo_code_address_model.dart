import 'package:freezed_annotation/freezed_annotation.dart';

part 'geo_code_address_model.freezed.dart';
part 'geo_code_address_model.g.dart';

@freezed
class GeoCodeAddressModel with _$GeoCodeAddressModel{
  GeoCodeAddressModel._();
  @JsonSerializable(explicitToJson: true)
  factory GeoCodeAddressModel({
    @JsonKey(name: "staddress", defaultValue: "") dynamic street,
    @JsonKey(name: "city", defaultValue: "") required String city,
    @JsonKey(name: "country", defaultValue: "") required String country,
    @JsonKey(name: "region", defaultValue: "") String? region,
    @JsonKey(name: "state", defaultValue: "") String? state,
    @JsonKey(name: "prov", defaultValue: "") String? prov,
  }) = _GeoCodeAddressModel;


  factory GeoCodeAddressModel.fromJson(Map<String, dynamic> json) =>
      _$GeoCodeAddressModelFromJson(json);

  String getStreet() {
    if (street is String) {
      return street;
    } else {
      return "";
    }
  }
}
