// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_total_favourite_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BusinessTotalFavouriteModel _$$_BusinessTotalFavouriteModelFromJson(
        Map<String, dynamic> json) =>
    _$_BusinessTotalFavouriteModel(
      totalRecords: json['totalRecords'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map((e) =>
              BusinesseFavouriteModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_BusinessTotalFavouriteModelToJson(
        _$_BusinessTotalFavouriteModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
