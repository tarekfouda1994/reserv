import 'package:flutter_tdd/features/profile/data/model/businesse_favourite_model/businesse_favourite_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'business_total_favourite_model.freezed.dart';
part 'business_total_favourite_model.g.dart';

@freezed
class BusinessTotalFavouriteModel with _$BusinessTotalFavouriteModel{
  @JsonSerializable(explicitToJson: true)
  factory BusinessTotalFavouriteModel({
    required int totalRecords,
    required List<BusinesseFavouriteModel>  itemsList,
  }) = _BusinessTotalFavouriteModel;


  factory BusinessTotalFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$BusinessTotalFavouriteModelFromJson(json);
}