// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'business_total_favourite_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BusinessTotalFavouriteModel _$BusinessTotalFavouriteModelFromJson(
    Map<String, dynamic> json) {
  return _BusinessTotalFavouriteModel.fromJson(json);
}

/// @nodoc
mixin _$BusinessTotalFavouriteModel {
  int get totalRecords => throw _privateConstructorUsedError;
  List<BusinesseFavouriteModel> get itemsList =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BusinessTotalFavouriteModelCopyWith<BusinessTotalFavouriteModel>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BusinessTotalFavouriteModelCopyWith<$Res> {
  factory $BusinessTotalFavouriteModelCopyWith(
          BusinessTotalFavouriteModel value,
          $Res Function(BusinessTotalFavouriteModel) then) =
      _$BusinessTotalFavouriteModelCopyWithImpl<$Res>;
  $Res call({int totalRecords, List<BusinesseFavouriteModel> itemsList});
}

/// @nodoc
class _$BusinessTotalFavouriteModelCopyWithImpl<$Res>
    implements $BusinessTotalFavouriteModelCopyWith<$Res> {
  _$BusinessTotalFavouriteModelCopyWithImpl(this._value, this._then);

  final BusinessTotalFavouriteModel _value;
  // ignore: unused_field
  final $Res Function(BusinessTotalFavouriteModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<BusinesseFavouriteModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_BusinessTotalFavouriteModelCopyWith<$Res>
    implements $BusinessTotalFavouriteModelCopyWith<$Res> {
  factory _$$_BusinessTotalFavouriteModelCopyWith(
          _$_BusinessTotalFavouriteModel value,
          $Res Function(_$_BusinessTotalFavouriteModel) then) =
      __$$_BusinessTotalFavouriteModelCopyWithImpl<$Res>;
  @override
  $Res call({int totalRecords, List<BusinesseFavouriteModel> itemsList});
}

/// @nodoc
class __$$_BusinessTotalFavouriteModelCopyWithImpl<$Res>
    extends _$BusinessTotalFavouriteModelCopyWithImpl<$Res>
    implements _$$_BusinessTotalFavouriteModelCopyWith<$Res> {
  __$$_BusinessTotalFavouriteModelCopyWithImpl(
      _$_BusinessTotalFavouriteModel _value,
      $Res Function(_$_BusinessTotalFavouriteModel) _then)
      : super(_value, (v) => _then(v as _$_BusinessTotalFavouriteModel));

  @override
  _$_BusinessTotalFavouriteModel get _value =>
      super._value as _$_BusinessTotalFavouriteModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_BusinessTotalFavouriteModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<BusinesseFavouriteModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BusinessTotalFavouriteModel implements _BusinessTotalFavouriteModel {
  _$_BusinessTotalFavouriteModel(
      {required this.totalRecords,
      required final List<BusinesseFavouriteModel> itemsList})
      : _itemsList = itemsList;

  factory _$_BusinessTotalFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$$_BusinessTotalFavouriteModelFromJson(json);

  @override
  final int totalRecords;
  final List<BusinesseFavouriteModel> _itemsList;
  @override
  List<BusinesseFavouriteModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'BusinessTotalFavouriteModel(totalRecords: $totalRecords, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BusinessTotalFavouriteModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_BusinessTotalFavouriteModelCopyWith<_$_BusinessTotalFavouriteModel>
      get copyWith => __$$_BusinessTotalFavouriteModelCopyWithImpl<
          _$_BusinessTotalFavouriteModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BusinessTotalFavouriteModelToJson(
      this,
    );
  }
}

abstract class _BusinessTotalFavouriteModel
    implements BusinessTotalFavouriteModel {
  factory _BusinessTotalFavouriteModel(
          {required final int totalRecords,
          required final List<BusinesseFavouriteModel> itemsList}) =
      _$_BusinessTotalFavouriteModel;

  factory _BusinessTotalFavouriteModel.fromJson(Map<String, dynamic> json) =
      _$_BusinessTotalFavouriteModel.fromJson;

  @override
  int get totalRecords;
  @override
  List<BusinesseFavouriteModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_BusinessTotalFavouriteModelCopyWith<_$_BusinessTotalFavouriteModel>
      get copyWith => throw _privateConstructorUsedError;
}
