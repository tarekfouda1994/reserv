import 'dart:convert';

import 'package:flutter/cupertino.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

class QuestionModel {
  QuestionModel({
    required this.id,
    required this.englishName,
    required this.arabicName,
    required this.questions,
    required this.selected,
    required this.key,
  });

  String id;
  String englishName;
  String arabicName;
  bool selected;
  List<QuestionItemModel> questions;
  GlobalKey key;

  factory QuestionModel.fromJson(String str) => QuestionModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory QuestionModel.fromMap(Map<String, dynamic> json) => QuestionModel(
        id: json["id"],
        englishName: json["englishName"],
        arabicName: json["arabicName"],
        key: GlobalKey(),
        selected: false,
        questions: List<QuestionItemModel>.from(
            json["questions"].map((x) => QuestionItemModel.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "englishName": englishName,
        "arabicName": arabicName,
        "questions": List<QuestionItemModel>.from(questions.map((x) => x)),
      };

  String getQuestionsCateName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}

class QuestionItemModel {
  QuestionItemModel({
    required this.id,
    required this.englishName,
    required this.arabicName,
    this.additionalNotes,
    required this.answers,
  });

  String id;
  String englishName;
  String arabicName;
  String? additionalNotes = "";
  List<AnswerItemModel> answers;

  factory QuestionItemModel.fromJson(String str) => QuestionItemModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory QuestionItemModel.fromMap(Map<String, dynamic> json) => QuestionItemModel(
        id: json["id"],
        englishName: json["englishName"],
        arabicName: json["arabicName"],
        additionalNotes: json["additionalNotes"],
        answers: List<AnswerItemModel>.from(json["answers"].map((x) => AnswerItemModel.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "englishName": englishName,
        "arabicName": arabicName,
        "additionalNotes": additionalNotes,
        "answers": List<AnswerItemModel>.from(answers.map((x) => x)),
      };

  String getQuestionsTitle() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}

class AnswerItemModel {
  AnswerItemModel({
    required this.id,
    required this.englishName,
    required this.arabicName,
    this.url,
  });

  String id;
  String englishName;
  String arabicName;
  String? url = "";

  factory AnswerItemModel.fromJson(String str) => AnswerItemModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AnswerItemModel.fromMap(Map<String, dynamic> json) => AnswerItemModel(
        id: json["id"],
        englishName: json["englishName"],
        arabicName: json["arabicName"],
        url: json["url"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "englishName": englishName,
        "arabicName": arabicName,
        "url": url,
      };

  String getAnswerTitle() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}
