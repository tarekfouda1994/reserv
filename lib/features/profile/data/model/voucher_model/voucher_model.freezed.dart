// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'voucher_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

VoucherModel _$VoucherModelFromJson(Map<String, dynamic> json) {
  return _VoucherModel.fromJson(json);
}

/// @nodoc
mixin _$VoucherModel {
  String get title => throw _privateConstructorUsedError;
  String get code => throw _privateConstructorUsedError;
  double get minimumSpendAmount => throw _privateConstructorUsedError;
  double get maximumDiscountAmount => throw _privateConstructorUsedError;
  double get discountPercentage => throw _privateConstructorUsedError;
  String get validUntilDate => throw _privateConstructorUsedError;
  String get issuanceDate => throw _privateConstructorUsedError;
  String get currencyEnglishName => throw _privateConstructorUsedError;
  String get currencyArabicName => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  int get quantity => throw _privateConstructorUsedError;
  int get currencyId => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  String get businessEnglishName => throw _privateConstructorUsedError;
  String get businessArabicName => throw _privateConstructorUsedError;
  List<String> get servicesEnglishName => throw _privateConstructorUsedError;
  List<String> get servicesArabicName => throw _privateConstructorUsedError;
  String get voucherStatus => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $VoucherModelCopyWith<VoucherModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoucherModelCopyWith<$Res> {
  factory $VoucherModelCopyWith(
          VoucherModel value, $Res Function(VoucherModel) then) =
      _$VoucherModelCopyWithImpl<$Res>;
  $Res call(
      {String title,
      String code,
      double minimumSpendAmount,
      double maximumDiscountAmount,
      double discountPercentage,
      String validUntilDate,
      String issuanceDate,
      String currencyEnglishName,
      String currencyArabicName,
      bool isActive,
      int quantity,
      int currencyId,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      List<String> servicesEnglishName,
      List<String> servicesArabicName,
      String voucherStatus});
}

/// @nodoc
class _$VoucherModelCopyWithImpl<$Res> implements $VoucherModelCopyWith<$Res> {
  _$VoucherModelCopyWithImpl(this._value, this._then);

  final VoucherModel _value;
  // ignore: unused_field
  final $Res Function(VoucherModel) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? code = freezed,
    Object? minimumSpendAmount = freezed,
    Object? maximumDiscountAmount = freezed,
    Object? discountPercentage = freezed,
    Object? validUntilDate = freezed,
    Object? issuanceDate = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? isActive = freezed,
    Object? quantity = freezed,
    Object? currencyId = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? servicesEnglishName = freezed,
    Object? servicesArabicName = freezed,
    Object? voucherStatus = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      minimumSpendAmount: minimumSpendAmount == freezed
          ? _value.minimumSpendAmount
          : minimumSpendAmount // ignore: cast_nullable_to_non_nullable
              as double,
      maximumDiscountAmount: maximumDiscountAmount == freezed
          ? _value.maximumDiscountAmount
          : maximumDiscountAmount // ignore: cast_nullable_to_non_nullable
              as double,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      validUntilDate: validUntilDate == freezed
          ? _value.validUntilDate
          : validUntilDate // ignore: cast_nullable_to_non_nullable
              as String,
      issuanceDate: issuanceDate == freezed
          ? _value.issuanceDate
          : issuanceDate // ignore: cast_nullable_to_non_nullable
              as String,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as int,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      servicesEnglishName: servicesEnglishName == freezed
          ? _value.servicesEnglishName
          : servicesEnglishName // ignore: cast_nullable_to_non_nullable
              as List<String>,
      servicesArabicName: servicesArabicName == freezed
          ? _value.servicesArabicName
          : servicesArabicName // ignore: cast_nullable_to_non_nullable
              as List<String>,
      voucherStatus: voucherStatus == freezed
          ? _value.voucherStatus
          : voucherStatus // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_VoucherModelCopyWith<$Res>
    implements $VoucherModelCopyWith<$Res> {
  factory _$$_VoucherModelCopyWith(
          _$_VoucherModel value, $Res Function(_$_VoucherModel) then) =
      __$$_VoucherModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String title,
      String code,
      double minimumSpendAmount,
      double maximumDiscountAmount,
      double discountPercentage,
      String validUntilDate,
      String issuanceDate,
      String currencyEnglishName,
      String currencyArabicName,
      bool isActive,
      int quantity,
      int currencyId,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      List<String> servicesEnglishName,
      List<String> servicesArabicName,
      String voucherStatus});
}

/// @nodoc
class __$$_VoucherModelCopyWithImpl<$Res>
    extends _$VoucherModelCopyWithImpl<$Res>
    implements _$$_VoucherModelCopyWith<$Res> {
  __$$_VoucherModelCopyWithImpl(
      _$_VoucherModel _value, $Res Function(_$_VoucherModel) _then)
      : super(_value, (v) => _then(v as _$_VoucherModel));

  @override
  _$_VoucherModel get _value => super._value as _$_VoucherModel;

  @override
  $Res call({
    Object? title = freezed,
    Object? code = freezed,
    Object? minimumSpendAmount = freezed,
    Object? maximumDiscountAmount = freezed,
    Object? discountPercentage = freezed,
    Object? validUntilDate = freezed,
    Object? issuanceDate = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? isActive = freezed,
    Object? quantity = freezed,
    Object? currencyId = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? servicesEnglishName = freezed,
    Object? servicesArabicName = freezed,
    Object? voucherStatus = freezed,
  }) {
    return _then(_$_VoucherModel(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      minimumSpendAmount: minimumSpendAmount == freezed
          ? _value.minimumSpendAmount
          : minimumSpendAmount // ignore: cast_nullable_to_non_nullable
              as double,
      maximumDiscountAmount: maximumDiscountAmount == freezed
          ? _value.maximumDiscountAmount
          : maximumDiscountAmount // ignore: cast_nullable_to_non_nullable
              as double,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      validUntilDate: validUntilDate == freezed
          ? _value.validUntilDate
          : validUntilDate // ignore: cast_nullable_to_non_nullable
              as String,
      issuanceDate: issuanceDate == freezed
          ? _value.issuanceDate
          : issuanceDate // ignore: cast_nullable_to_non_nullable
              as String,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as int,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      servicesEnglishName: servicesEnglishName == freezed
          ? _value._servicesEnglishName
          : servicesEnglishName // ignore: cast_nullable_to_non_nullable
              as List<String>,
      servicesArabicName: servicesArabicName == freezed
          ? _value._servicesArabicName
          : servicesArabicName // ignore: cast_nullable_to_non_nullable
              as List<String>,
      voucherStatus: voucherStatus == freezed
          ? _value.voucherStatus
          : voucherStatus // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_VoucherModel extends _VoucherModel {
  _$_VoucherModel(
      {required this.title,
      required this.code,
      required this.minimumSpendAmount,
      required this.maximumDiscountAmount,
      required this.discountPercentage,
      required this.validUntilDate,
      required this.issuanceDate,
      required this.currencyEnglishName,
      required this.currencyArabicName,
      required this.isActive,
      required this.quantity,
      required this.currencyId,
      required this.businessID,
      required this.businessEnglishName,
      required this.businessArabicName,
      required final List<String> servicesEnglishName,
      required final List<String> servicesArabicName,
      required this.voucherStatus})
      : _servicesEnglishName = servicesEnglishName,
        _servicesArabicName = servicesArabicName,
        super._();

  factory _$_VoucherModel.fromJson(Map<String, dynamic> json) =>
      _$$_VoucherModelFromJson(json);

  @override
  final String title;
  @override
  final String code;
  @override
  final double minimumSpendAmount;
  @override
  final double maximumDiscountAmount;
  @override
  final double discountPercentage;
  @override
  final String validUntilDate;
  @override
  final String issuanceDate;
  @override
  final String currencyEnglishName;
  @override
  final String currencyArabicName;
  @override
  final bool isActive;
  @override
  final int quantity;
  @override
  final int currencyId;
  @override
  final String businessID;
  @override
  final String businessEnglishName;
  @override
  final String businessArabicName;
  final List<String> _servicesEnglishName;
  @override
  List<String> get servicesEnglishName {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_servicesEnglishName);
  }

  final List<String> _servicesArabicName;
  @override
  List<String> get servicesArabicName {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_servicesArabicName);
  }

  @override
  final String voucherStatus;

  @override
  String toString() {
    return 'VoucherModel(title: $title, code: $code, minimumSpendAmount: $minimumSpendAmount, maximumDiscountAmount: $maximumDiscountAmount, discountPercentage: $discountPercentage, validUntilDate: $validUntilDate, issuanceDate: $issuanceDate, currencyEnglishName: $currencyEnglishName, currencyArabicName: $currencyArabicName, isActive: $isActive, quantity: $quantity, currencyId: $currencyId, businessID: $businessID, businessEnglishName: $businessEnglishName, businessArabicName: $businessArabicName, servicesEnglishName: $servicesEnglishName, servicesArabicName: $servicesArabicName, voucherStatus: $voucherStatus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_VoucherModel &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.code, code) &&
            const DeepCollectionEquality()
                .equals(other.minimumSpendAmount, minimumSpendAmount) &&
            const DeepCollectionEquality()
                .equals(other.maximumDiscountAmount, maximumDiscountAmount) &&
            const DeepCollectionEquality()
                .equals(other.discountPercentage, discountPercentage) &&
            const DeepCollectionEquality()
                .equals(other.validUntilDate, validUntilDate) &&
            const DeepCollectionEquality()
                .equals(other.issuanceDate, issuanceDate) &&
            const DeepCollectionEquality()
                .equals(other.currencyEnglishName, currencyEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.currencyArabicName, currencyArabicName) &&
            const DeepCollectionEquality().equals(other.isActive, isActive) &&
            const DeepCollectionEquality().equals(other.quantity, quantity) &&
            const DeepCollectionEquality()
                .equals(other.currencyId, currencyId) &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.businessEnglishName, businessEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.businessArabicName, businessArabicName) &&
            const DeepCollectionEquality()
                .equals(other._servicesEnglishName, _servicesEnglishName) &&
            const DeepCollectionEquality()
                .equals(other._servicesArabicName, _servicesArabicName) &&
            const DeepCollectionEquality()
                .equals(other.voucherStatus, voucherStatus));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(code),
      const DeepCollectionEquality().hash(minimumSpendAmount),
      const DeepCollectionEquality().hash(maximumDiscountAmount),
      const DeepCollectionEquality().hash(discountPercentage),
      const DeepCollectionEquality().hash(validUntilDate),
      const DeepCollectionEquality().hash(issuanceDate),
      const DeepCollectionEquality().hash(currencyEnglishName),
      const DeepCollectionEquality().hash(currencyArabicName),
      const DeepCollectionEquality().hash(isActive),
      const DeepCollectionEquality().hash(quantity),
      const DeepCollectionEquality().hash(currencyId),
      const DeepCollectionEquality().hash(businessID),
      const DeepCollectionEquality().hash(businessEnglishName),
      const DeepCollectionEquality().hash(businessArabicName),
      const DeepCollectionEquality().hash(_servicesEnglishName),
      const DeepCollectionEquality().hash(_servicesArabicName),
      const DeepCollectionEquality().hash(voucherStatus));

  @JsonKey(ignore: true)
  @override
  _$$_VoucherModelCopyWith<_$_VoucherModel> get copyWith =>
      __$$_VoucherModelCopyWithImpl<_$_VoucherModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_VoucherModelToJson(
      this,
    );
  }
}

abstract class _VoucherModel extends VoucherModel {
  factory _VoucherModel(
      {required final String title,
      required final String code,
      required final double minimumSpendAmount,
      required final double maximumDiscountAmount,
      required final double discountPercentage,
      required final String validUntilDate,
      required final String issuanceDate,
      required final String currencyEnglishName,
      required final String currencyArabicName,
      required final bool isActive,
      required final int quantity,
      required final int currencyId,
      required final String businessID,
      required final String businessEnglishName,
      required final String businessArabicName,
      required final List<String> servicesEnglishName,
      required final List<String> servicesArabicName,
      required final String voucherStatus}) = _$_VoucherModel;
  _VoucherModel._() : super._();

  factory _VoucherModel.fromJson(Map<String, dynamic> json) =
      _$_VoucherModel.fromJson;

  @override
  String get title;
  @override
  String get code;
  @override
  double get minimumSpendAmount;
  @override
  double get maximumDiscountAmount;
  @override
  double get discountPercentage;
  @override
  String get validUntilDate;
  @override
  String get issuanceDate;
  @override
  String get currencyEnglishName;
  @override
  String get currencyArabicName;
  @override
  bool get isActive;
  @override
  int get quantity;
  @override
  int get currencyId;
  @override
  String get businessID;
  @override
  String get businessEnglishName;
  @override
  String get businessArabicName;
  @override
  List<String> get servicesEnglishName;
  @override
  List<String> get servicesArabicName;
  @override
  String get voucherStatus;
  @override
  @JsonKey(ignore: true)
  _$$_VoucherModelCopyWith<_$_VoucherModel> get copyWith =>
      throw _privateConstructorUsedError;
}
