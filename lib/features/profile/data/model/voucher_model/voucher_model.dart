import 'dart:ui';

import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/constants/my_colors.dart';
import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'voucher_model.freezed.dart';
part 'voucher_model.g.dart';

@freezed
class VoucherModel with _$VoucherModel {
  VoucherModel._();

  @JsonSerializable(explicitToJson: true)
  factory VoucherModel({
    required String title,
    required String code,
    required double minimumSpendAmount,
    required double maximumDiscountAmount,
    required double discountPercentage,
    required String validUntilDate,
    required String issuanceDate,
    required String currencyEnglishName,
    required String currencyArabicName,
    required bool isActive,
    required int quantity,
    required int currencyId,
    required String businessID,
    required String businessEnglishName,
    required String businessArabicName,
    required List<String> servicesEnglishName,
    required List<String> servicesArabicName,
    required String voucherStatus,
  }) = _VoucherModel;

  factory VoucherModel.fromJson(Map<String, dynamic> json) => _$VoucherModelFromJson(json);

  String getBusinessName() {
    return getIt<Utilities>().getLocalizedValue(businessArabicName, businessEnglishName);
  }

  String getCurrencyName() {
    return getIt<Utilities>().getLocalizedValue(currencyArabicName, currencyEnglishName);
  }
}

extension StatusColor on String {
  Color getStatusBgColor() {
    switch (this) {
      case "Valid":
        return Color(0xffd6ecec);
      case "Expired":
        return Color(0xffffd1d8);
      default:
        return Color(0xfffff3ce);
    }
  }

  Color getStatusTextColor() {
    switch (this) {
      case "Valid":
        return Color(0xff288181);
      case "Expired":
        return Color(0xffed3a57);
      default:
        return Color(0xffeca773);
    }
  }

  Color getStatusContainerColor() {
    switch (this) {
      case "Valid":
        return MyColors.white;
      case "Expired":
        return Color(0xffed3a57);
      default:
        return Color(0xffeca773);
    }
  }

  Color getStatusContainerTextColor() {
    switch (this) {
      case "Valid":
        return MyColors.primary;
      default:
        return MyColors.white;
    }
  }
}
