// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_VoucherModel _$$_VoucherModelFromJson(Map<String, dynamic> json) =>
    _$_VoucherModel(
      title: json['title'] as String,
      code: json['code'] as String,
      minimumSpendAmount: (json['minimumSpendAmount'] as num).toDouble(),
      maximumDiscountAmount: (json['maximumDiscountAmount'] as num).toDouble(),
      discountPercentage: (json['discountPercentage'] as num).toDouble(),
      validUntilDate: json['validUntilDate'] as String,
      issuanceDate: json['issuanceDate'] as String,
      currencyEnglishName: json['currencyEnglishName'] as String,
      currencyArabicName: json['currencyArabicName'] as String,
      isActive: json['isActive'] as bool,
      quantity: json['quantity'] as int,
      currencyId: json['currencyId'] as int,
      businessID: json['businessID'] as String,
      businessEnglishName: json['businessEnglishName'] as String,
      businessArabicName: json['businessArabicName'] as String,
      servicesEnglishName: (json['servicesEnglishName'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      servicesArabicName: (json['servicesArabicName'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      voucherStatus: json['voucherStatus'] as String,
    );

Map<String, dynamic> _$$_VoucherModelToJson(_$_VoucherModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'code': instance.code,
      'minimumSpendAmount': instance.minimumSpendAmount,
      'maximumDiscountAmount': instance.maximumDiscountAmount,
      'discountPercentage': instance.discountPercentage,
      'validUntilDate': instance.validUntilDate,
      'issuanceDate': instance.issuanceDate,
      'currencyEnglishName': instance.currencyEnglishName,
      'currencyArabicName': instance.currencyArabicName,
      'isActive': instance.isActive,
      'quantity': instance.quantity,
      'currencyId': instance.currencyId,
      'businessID': instance.businessID,
      'businessEnglishName': instance.businessEnglishName,
      'businessArabicName': instance.businessArabicName,
      'servicesEnglishName': instance.servicesEnglishName,
      'servicesArabicName': instance.servicesArabicName,
      'voucherStatus': instance.voucherStatus,
    };
