// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'businesse_favourite_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BusinesseFavouriteModel _$$_BusinesseFavouriteModelFromJson(
        Map<String, dynamic> json) =>
    _$_BusinesseFavouriteModel(
      id: json['id'] as String,
      isActive: json['isActive'] as bool? ?? false,
      hasActiveSlot: json['hasActiveSlot'] as bool,
      englishName: json['englishName'] as String,
      arabicName: json['arabicName'] as String,
      businessCategoryIds: json['businessCategoryIds'] as String,
      businessStatusId: json['businessStatusId'] as int,
      areaId: json['areaId'] as int,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      businessCategoryArabicName: json['businessCategoryArabicName'] as String,
      businessCategoryEnglishName:
          json['businessCategoryEnglishName'] as String,
      location: json['location'] as String,
      businesssImage: json['businesssImage'] as String,
      dateCreated: DateTime.parse(json['dateCreated'] as String),
      packageId: json['packageId'],
      distanceText: json['distance'] as String,
      rating: (json['rating'] as num).toDouble(),
      businessAddressEN: json['businessAddressEN'] as String,
      businessAddressAR: json['businessAddressAR'] as String,
      numberOfService: json['numberOfService'] as int,
    );

Map<String, dynamic> _$$_BusinesseFavouriteModelToJson(
        _$_BusinesseFavouriteModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isActive': instance.isActive,
      'hasActiveSlot': instance.hasActiveSlot,
      'englishName': instance.englishName,
      'arabicName': instance.arabicName,
      'businessCategoryIds': instance.businessCategoryIds,
      'businessStatusId': instance.businessStatusId,
      'areaId': instance.areaId,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'businessCategoryArabicName': instance.businessCategoryArabicName,
      'businessCategoryEnglishName': instance.businessCategoryEnglishName,
      'location': instance.location,
      'businesssImage': instance.businesssImage,
      'dateCreated': instance.dateCreated.toIso8601String(),
      'packageId': instance.packageId,
      'distance': instance.distanceText,
      'rating': instance.rating,
      'businessAddressEN': instance.businessAddressEN,
      'businessAddressAR': instance.businessAddressAR,
      'numberOfService': instance.numberOfService,
    };
