// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'businesse_favourite_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BusinesseFavouriteModel _$BusinesseFavouriteModelFromJson(
    Map<String, dynamic> json) {
  return _BusinesseFavouriteModel.fromJson(json);
}

/// @nodoc
mixin _$BusinesseFavouriteModel {
  String get id => throw _privateConstructorUsedError;
  set id(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: false)
  bool get isActive => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: false)
  set isActive(bool value) => throw _privateConstructorUsedError;
  bool get hasActiveSlot => throw _privateConstructorUsedError;
  set hasActiveSlot(bool value) => throw _privateConstructorUsedError;
  String get englishName => throw _privateConstructorUsedError;
  set englishName(String value) => throw _privateConstructorUsedError;
  String get arabicName => throw _privateConstructorUsedError;
  set arabicName(String value) => throw _privateConstructorUsedError;
  String get businessCategoryIds => throw _privateConstructorUsedError;
  set businessCategoryIds(String value) => throw _privateConstructorUsedError;
  int get businessStatusId => throw _privateConstructorUsedError;
  set businessStatusId(int value) => throw _privateConstructorUsedError;
  int get areaId => throw _privateConstructorUsedError;
  set areaId(int value) => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  set latitude(double value) => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  set longitude(double value) => throw _privateConstructorUsedError;
  String get businessCategoryArabicName => throw _privateConstructorUsedError;
  set businessCategoryArabicName(String value) =>
      throw _privateConstructorUsedError;
  String get businessCategoryEnglishName => throw _privateConstructorUsedError;
  set businessCategoryEnglishName(String value) =>
      throw _privateConstructorUsedError;
  String get location => throw _privateConstructorUsedError;
  set location(String value) => throw _privateConstructorUsedError;
  String get businesssImage => throw _privateConstructorUsedError;
  set businesssImage(String value) => throw _privateConstructorUsedError;
  DateTime get dateCreated => throw _privateConstructorUsedError;
  set dateCreated(DateTime value) => throw _privateConstructorUsedError;
  dynamic get packageId => throw _privateConstructorUsedError;
  set packageId(dynamic value) => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  String get distanceText => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  set distanceText(String value) => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;
  set rating(double value) => throw _privateConstructorUsedError;
  String get businessAddressEN => throw _privateConstructorUsedError;
  set businessAddressEN(String value) => throw _privateConstructorUsedError;
  String get businessAddressAR => throw _privateConstructorUsedError;
  set businessAddressAR(String value) => throw _privateConstructorUsedError;
  int get numberOfService => throw _privateConstructorUsedError;
  set numberOfService(int value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BusinesseFavouriteModelCopyWith<BusinesseFavouriteModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BusinesseFavouriteModelCopyWith<$Res> {
  factory $BusinesseFavouriteModelCopyWith(BusinesseFavouriteModel value,
          $Res Function(BusinesseFavouriteModel) then) =
      _$BusinesseFavouriteModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      @JsonKey(nullable: true, defaultValue: false) bool isActive,
      bool hasActiveSlot,
      String englishName,
      String arabicName,
      String businessCategoryIds,
      int businessStatusId,
      int areaId,
      double latitude,
      double longitude,
      String businessCategoryArabicName,
      String businessCategoryEnglishName,
      String location,
      String businesssImage,
      DateTime dateCreated,
      dynamic packageId,
      @JsonKey(name: "distance") String distanceText,
      double rating,
      String businessAddressEN,
      String businessAddressAR,
      int numberOfService});
}

/// @nodoc
class _$BusinesseFavouriteModelCopyWithImpl<$Res>
    implements $BusinesseFavouriteModelCopyWith<$Res> {
  _$BusinesseFavouriteModelCopyWithImpl(this._value, this._then);

  final BusinesseFavouriteModel _value;
  // ignore: unused_field
  final $Res Function(BusinesseFavouriteModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? isActive = freezed,
    Object? hasActiveSlot = freezed,
    Object? englishName = freezed,
    Object? arabicName = freezed,
    Object? businessCategoryIds = freezed,
    Object? businessStatusId = freezed,
    Object? areaId = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessCategoryArabicName = freezed,
    Object? businessCategoryEnglishName = freezed,
    Object? location = freezed,
    Object? businesssImage = freezed,
    Object? dateCreated = freezed,
    Object? packageId = freezed,
    Object? distanceText = freezed,
    Object? rating = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? numberOfService = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessCategoryIds: businessCategoryIds == freezed
          ? _value.businessCategoryIds
          : businessCategoryIds // ignore: cast_nullable_to_non_nullable
              as String,
      businessStatusId: businessStatusId == freezed
          ? _value.businessStatusId
          : businessStatusId // ignore: cast_nullable_to_non_nullable
              as int,
      areaId: areaId == freezed
          ? _value.areaId
          : areaId // ignore: cast_nullable_to_non_nullable
              as int,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessCategoryArabicName: businessCategoryArabicName == freezed
          ? _value.businessCategoryArabicName
          : businessCategoryArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessCategoryEnglishName: businessCategoryEnglishName == freezed
          ? _value.businessCategoryEnglishName
          : businessCategoryEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      businesssImage: businesssImage == freezed
          ? _value.businesssImage
          : businesssImage // ignore: cast_nullable_to_non_nullable
              as String,
      dateCreated: dateCreated == freezed
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as DateTime,
      packageId: packageId == freezed
          ? _value.packageId
          : packageId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      numberOfService: numberOfService == freezed
          ? _value.numberOfService
          : numberOfService // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_BusinesseFavouriteModelCopyWith<$Res>
    implements $BusinesseFavouriteModelCopyWith<$Res> {
  factory _$$_BusinesseFavouriteModelCopyWith(_$_BusinesseFavouriteModel value,
          $Res Function(_$_BusinesseFavouriteModel) then) =
      __$$_BusinesseFavouriteModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      @JsonKey(nullable: true, defaultValue: false) bool isActive,
      bool hasActiveSlot,
      String englishName,
      String arabicName,
      String businessCategoryIds,
      int businessStatusId,
      int areaId,
      double latitude,
      double longitude,
      String businessCategoryArabicName,
      String businessCategoryEnglishName,
      String location,
      String businesssImage,
      DateTime dateCreated,
      dynamic packageId,
      @JsonKey(name: "distance") String distanceText,
      double rating,
      String businessAddressEN,
      String businessAddressAR,
      int numberOfService});
}

/// @nodoc
class __$$_BusinesseFavouriteModelCopyWithImpl<$Res>
    extends _$BusinesseFavouriteModelCopyWithImpl<$Res>
    implements _$$_BusinesseFavouriteModelCopyWith<$Res> {
  __$$_BusinesseFavouriteModelCopyWithImpl(_$_BusinesseFavouriteModel _value,
      $Res Function(_$_BusinesseFavouriteModel) _then)
      : super(_value, (v) => _then(v as _$_BusinesseFavouriteModel));

  @override
  _$_BusinesseFavouriteModel get _value =>
      super._value as _$_BusinesseFavouriteModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? isActive = freezed,
    Object? hasActiveSlot = freezed,
    Object? englishName = freezed,
    Object? arabicName = freezed,
    Object? businessCategoryIds = freezed,
    Object? businessStatusId = freezed,
    Object? areaId = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessCategoryArabicName = freezed,
    Object? businessCategoryEnglishName = freezed,
    Object? location = freezed,
    Object? businesssImage = freezed,
    Object? dateCreated = freezed,
    Object? packageId = freezed,
    Object? distanceText = freezed,
    Object? rating = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? numberOfService = freezed,
  }) {
    return _then(_$_BusinesseFavouriteModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessCategoryIds: businessCategoryIds == freezed
          ? _value.businessCategoryIds
          : businessCategoryIds // ignore: cast_nullable_to_non_nullable
              as String,
      businessStatusId: businessStatusId == freezed
          ? _value.businessStatusId
          : businessStatusId // ignore: cast_nullable_to_non_nullable
              as int,
      areaId: areaId == freezed
          ? _value.areaId
          : areaId // ignore: cast_nullable_to_non_nullable
              as int,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessCategoryArabicName: businessCategoryArabicName == freezed
          ? _value.businessCategoryArabicName
          : businessCategoryArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessCategoryEnglishName: businessCategoryEnglishName == freezed
          ? _value.businessCategoryEnglishName
          : businessCategoryEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      businesssImage: businesssImage == freezed
          ? _value.businesssImage
          : businesssImage // ignore: cast_nullable_to_non_nullable
              as String,
      dateCreated: dateCreated == freezed
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as DateTime,
      packageId: packageId == freezed
          ? _value.packageId
          : packageId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      numberOfService: numberOfService == freezed
          ? _value.numberOfService
          : numberOfService // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BusinesseFavouriteModel extends _BusinesseFavouriteModel {
  _$_BusinesseFavouriteModel(
      {required this.id,
      @JsonKey(nullable: true, defaultValue: false) required this.isActive,
      required this.hasActiveSlot,
      required this.englishName,
      required this.arabicName,
      required this.businessCategoryIds,
      required this.businessStatusId,
      required this.areaId,
      required this.latitude,
      required this.longitude,
      required this.businessCategoryArabicName,
      required this.businessCategoryEnglishName,
      required this.location,
      required this.businesssImage,
      required this.dateCreated,
      required this.packageId,
      @JsonKey(name: "distance") required this.distanceText,
      required this.rating,
      required this.businessAddressEN,
      required this.businessAddressAR,
      required this.numberOfService})
      : super._();

  factory _$_BusinesseFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$$_BusinesseFavouriteModelFromJson(json);

  @override
  String id;
  @override
  @JsonKey(nullable: true, defaultValue: false)
  bool isActive;
  @override
  bool hasActiveSlot;
  @override
  String englishName;
  @override
  String arabicName;
  @override
  String businessCategoryIds;
  @override
  int businessStatusId;
  @override
  int areaId;
  @override
  double latitude;
  @override
  double longitude;
  @override
  String businessCategoryArabicName;
  @override
  String businessCategoryEnglishName;
  @override
  String location;
  @override
  String businesssImage;
  @override
  DateTime dateCreated;
  @override
  dynamic packageId;
  @override
  @JsonKey(name: "distance")
  String distanceText;
  @override
  double rating;
  @override
  String businessAddressEN;
  @override
  String businessAddressAR;
  @override
  int numberOfService;

  @override
  String toString() {
    return 'BusinesseFavouriteModel(id: $id, isActive: $isActive, hasActiveSlot: $hasActiveSlot, englishName: $englishName, arabicName: $arabicName, businessCategoryIds: $businessCategoryIds, businessStatusId: $businessStatusId, areaId: $areaId, latitude: $latitude, longitude: $longitude, businessCategoryArabicName: $businessCategoryArabicName, businessCategoryEnglishName: $businessCategoryEnglishName, location: $location, businesssImage: $businesssImage, dateCreated: $dateCreated, packageId: $packageId, distanceText: $distanceText, rating: $rating, businessAddressEN: $businessAddressEN, businessAddressAR: $businessAddressAR, numberOfService: $numberOfService)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_BusinesseFavouriteModelCopyWith<_$_BusinesseFavouriteModel>
      get copyWith =>
          __$$_BusinesseFavouriteModelCopyWithImpl<_$_BusinesseFavouriteModel>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BusinesseFavouriteModelToJson(
      this,
    );
  }
}

abstract class _BusinesseFavouriteModel extends BusinesseFavouriteModel {
  factory _BusinesseFavouriteModel(
      {required String id,
      @JsonKey(nullable: true, defaultValue: false) required bool isActive,
      required bool hasActiveSlot,
      required String englishName,
      required String arabicName,
      required String businessCategoryIds,
      required int businessStatusId,
      required int areaId,
      required double latitude,
      required double longitude,
      required String businessCategoryArabicName,
      required String businessCategoryEnglishName,
      required String location,
      required String businesssImage,
      required DateTime dateCreated,
      required dynamic packageId,
      @JsonKey(name: "distance") required String distanceText,
      required double rating,
      required String businessAddressEN,
      required String businessAddressAR,
      required int numberOfService}) = _$_BusinesseFavouriteModel;
  _BusinesseFavouriteModel._() : super._();

  factory _BusinesseFavouriteModel.fromJson(Map<String, dynamic> json) =
      _$_BusinesseFavouriteModel.fromJson;

  @override
  String get id;
  set id(String value);
  @override
  @JsonKey(nullable: true, defaultValue: false)
  bool get isActive;
  @JsonKey(nullable: true, defaultValue: false)
  set isActive(bool value);
  @override
  bool get hasActiveSlot;
  set hasActiveSlot(bool value);
  @override
  String get englishName;
  set englishName(String value);
  @override
  String get arabicName;
  set arabicName(String value);
  @override
  String get businessCategoryIds;
  set businessCategoryIds(String value);
  @override
  int get businessStatusId;
  set businessStatusId(int value);
  @override
  int get areaId;
  set areaId(int value);
  @override
  double get latitude;
  set latitude(double value);
  @override
  double get longitude;
  set longitude(double value);
  @override
  String get businessCategoryArabicName;
  set businessCategoryArabicName(String value);
  @override
  String get businessCategoryEnglishName;
  set businessCategoryEnglishName(String value);
  @override
  String get location;
  set location(String value);
  @override
  String get businesssImage;
  set businesssImage(String value);
  @override
  DateTime get dateCreated;
  set dateCreated(DateTime value);
  @override
  dynamic get packageId;
  set packageId(dynamic value);
  @override
  @JsonKey(name: "distance")
  String get distanceText;
  @JsonKey(name: "distance")
  set distanceText(String value);
  @override
  double get rating;
  set rating(double value);
  @override
  String get businessAddressEN;
  set businessAddressEN(String value);
  @override
  String get businessAddressAR;
  set businessAddressAR(String value);
  @override
  int get numberOfService;
  set numberOfService(int value);
  @override
  @JsonKey(ignore: true)
  _$$_BusinesseFavouriteModelCopyWith<_$_BusinesseFavouriteModel>
      get copyWith => throw _privateConstructorUsedError;
}
