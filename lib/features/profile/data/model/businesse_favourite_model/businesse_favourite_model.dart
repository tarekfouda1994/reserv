import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'businesse_favourite_model.freezed.dart';

part 'businesse_favourite_model.g.dart';

@unfreezed
class BusinesseFavouriteModel with _$BusinesseFavouriteModel {
  BusinesseFavouriteModel._();

  @JsonSerializable(explicitToJson: true)
  factory BusinesseFavouriteModel({
    required String id,
    @JsonKey(nullable: true, defaultValue: false) required bool isActive,
    required bool hasActiveSlot,
    required String englishName,
    required String arabicName,
    required String businessCategoryIds,
    required int businessStatusId,
    required int areaId,
    required double latitude,
    required double longitude,
    required String businessCategoryArabicName,
    required String businessCategoryEnglishName,
    required String location,
    required String businesssImage,
    required DateTime dateCreated,
    required dynamic packageId,
    @JsonKey(name: "distance") required String distanceText,
    required double rating,
    required String businessAddressEN,
    required String businessAddressAR,
    required int numberOfService,
  }) = _BusinesseFavouriteModel;

  factory BusinesseFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$BusinesseFavouriteModelFromJson(json);

  String get distance {
    BuildContext context = getIt<GlobalContext>().context();
    return getIt<Utilities>().convertNumToAr(
      context: context,
      value: "${distanceText.toString().split(" ").first} ${tr("km")}",
      isDistance: true,
    );
  }

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }

  String getBusinessName() {
    return getIt<Utilities>()
        .getLocalizedValue(businessCategoryArabicName, businessCategoryEnglishName);
  }

  String getAddressName() {
    return getIt<Utilities>().getLocalizedValue(businessAddressAR, businessAddressEN);
  }
}
