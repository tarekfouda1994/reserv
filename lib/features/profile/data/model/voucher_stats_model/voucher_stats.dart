import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'voucher_stats.freezed.dart';
part 'voucher_stats.g.dart';

@freezed
class VoucherStats with _$VoucherStats{
  VoucherStats._();
  @JsonSerializable(explicitToJson: true)
  factory VoucherStats({
    required int statusID,
    required String statusEnglish,
    required String statusArabic,
    required int totalVoucher,
    required int order,
  }) = _VoucherStats;


  factory VoucherStats.fromJson(Map<String, dynamic> json) =>
      _$VoucherStatsFromJson(json);
  String getVoucherName() {
    return getIt<Utilities>().getLocalizedValue(statusArabic, statusEnglish);
  }
}