// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'voucher_stats.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

VoucherStats _$VoucherStatsFromJson(Map<String, dynamic> json) {
  return _VoucherStats.fromJson(json);
}

/// @nodoc
mixin _$VoucherStats {
  int get statusID => throw _privateConstructorUsedError;
  String get statusEnglish => throw _privateConstructorUsedError;
  String get statusArabic => throw _privateConstructorUsedError;
  int get totalVoucher => throw _privateConstructorUsedError;
  int get order => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $VoucherStatsCopyWith<VoucherStats> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoucherStatsCopyWith<$Res> {
  factory $VoucherStatsCopyWith(
          VoucherStats value, $Res Function(VoucherStats) then) =
      _$VoucherStatsCopyWithImpl<$Res>;
  $Res call(
      {int statusID,
      String statusEnglish,
      String statusArabic,
      int totalVoucher,
      int order});
}

/// @nodoc
class _$VoucherStatsCopyWithImpl<$Res> implements $VoucherStatsCopyWith<$Res> {
  _$VoucherStatsCopyWithImpl(this._value, this._then);

  final VoucherStats _value;
  // ignore: unused_field
  final $Res Function(VoucherStats) _then;

  @override
  $Res call({
    Object? statusID = freezed,
    Object? statusEnglish = freezed,
    Object? statusArabic = freezed,
    Object? totalVoucher = freezed,
    Object? order = freezed,
  }) {
    return _then(_value.copyWith(
      statusID: statusID == freezed
          ? _value.statusID
          : statusID // ignore: cast_nullable_to_non_nullable
              as int,
      statusEnglish: statusEnglish == freezed
          ? _value.statusEnglish
          : statusEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      statusArabic: statusArabic == freezed
          ? _value.statusArabic
          : statusArabic // ignore: cast_nullable_to_non_nullable
              as String,
      totalVoucher: totalVoucher == freezed
          ? _value.totalVoucher
          : totalVoucher // ignore: cast_nullable_to_non_nullable
              as int,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_VoucherStatsCopyWith<$Res>
    implements $VoucherStatsCopyWith<$Res> {
  factory _$$_VoucherStatsCopyWith(
          _$_VoucherStats value, $Res Function(_$_VoucherStats) then) =
      __$$_VoucherStatsCopyWithImpl<$Res>;
  @override
  $Res call(
      {int statusID,
      String statusEnglish,
      String statusArabic,
      int totalVoucher,
      int order});
}

/// @nodoc
class __$$_VoucherStatsCopyWithImpl<$Res>
    extends _$VoucherStatsCopyWithImpl<$Res>
    implements _$$_VoucherStatsCopyWith<$Res> {
  __$$_VoucherStatsCopyWithImpl(
      _$_VoucherStats _value, $Res Function(_$_VoucherStats) _then)
      : super(_value, (v) => _then(v as _$_VoucherStats));

  @override
  _$_VoucherStats get _value => super._value as _$_VoucherStats;

  @override
  $Res call({
    Object? statusID = freezed,
    Object? statusEnglish = freezed,
    Object? statusArabic = freezed,
    Object? totalVoucher = freezed,
    Object? order = freezed,
  }) {
    return _then(_$_VoucherStats(
      statusID: statusID == freezed
          ? _value.statusID
          : statusID // ignore: cast_nullable_to_non_nullable
              as int,
      statusEnglish: statusEnglish == freezed
          ? _value.statusEnglish
          : statusEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      statusArabic: statusArabic == freezed
          ? _value.statusArabic
          : statusArabic // ignore: cast_nullable_to_non_nullable
              as String,
      totalVoucher: totalVoucher == freezed
          ? _value.totalVoucher
          : totalVoucher // ignore: cast_nullable_to_non_nullable
              as int,
      order: order == freezed
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_VoucherStats extends _VoucherStats {
  _$_VoucherStats(
      {required this.statusID,
      required this.statusEnglish,
      required this.statusArabic,
      required this.totalVoucher,
      required this.order})
      : super._();

  factory _$_VoucherStats.fromJson(Map<String, dynamic> json) =>
      _$$_VoucherStatsFromJson(json);

  @override
  final int statusID;
  @override
  final String statusEnglish;
  @override
  final String statusArabic;
  @override
  final int totalVoucher;
  @override
  final int order;

  @override
  String toString() {
    return 'VoucherStats(statusID: $statusID, statusEnglish: $statusEnglish, statusArabic: $statusArabic, totalVoucher: $totalVoucher, order: $order)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_VoucherStats &&
            const DeepCollectionEquality().equals(other.statusID, statusID) &&
            const DeepCollectionEquality()
                .equals(other.statusEnglish, statusEnglish) &&
            const DeepCollectionEquality()
                .equals(other.statusArabic, statusArabic) &&
            const DeepCollectionEquality()
                .equals(other.totalVoucher, totalVoucher) &&
            const DeepCollectionEquality().equals(other.order, order));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(statusID),
      const DeepCollectionEquality().hash(statusEnglish),
      const DeepCollectionEquality().hash(statusArabic),
      const DeepCollectionEquality().hash(totalVoucher),
      const DeepCollectionEquality().hash(order));

  @JsonKey(ignore: true)
  @override
  _$$_VoucherStatsCopyWith<_$_VoucherStats> get copyWith =>
      __$$_VoucherStatsCopyWithImpl<_$_VoucherStats>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_VoucherStatsToJson(
      this,
    );
  }
}

abstract class _VoucherStats extends VoucherStats {
  factory _VoucherStats(
      {required final int statusID,
      required final String statusEnglish,
      required final String statusArabic,
      required final int totalVoucher,
      required final int order}) = _$_VoucherStats;
  _VoucherStats._() : super._();

  factory _VoucherStats.fromJson(Map<String, dynamic> json) =
      _$_VoucherStats.fromJson;

  @override
  int get statusID;
  @override
  String get statusEnglish;
  @override
  String get statusArabic;
  @override
  int get totalVoucher;
  @override
  int get order;
  @override
  @JsonKey(ignore: true)
  _$$_VoucherStatsCopyWith<_$_VoucherStats> get copyWith =>
      throw _privateConstructorUsedError;
}
