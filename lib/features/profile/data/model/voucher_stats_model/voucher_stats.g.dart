// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_stats.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_VoucherStats _$$_VoucherStatsFromJson(Map<String, dynamic> json) =>
    _$_VoucherStats(
      statusID: json['statusID'] as int,
      statusEnglish: json['statusEnglish'] as String,
      statusArabic: json['statusArabic'] as String,
      totalVoucher: json['totalVoucher'] as int,
      order: json['order'] as int,
    );

Map<String, dynamic> _$$_VoucherStatsToJson(_$_VoucherStats instance) =>
    <String, dynamic>{
      'statusID': instance.statusID,
      'statusEnglish': instance.statusEnglish,
      'statusArabic': instance.statusArabic,
      'totalVoucher': instance.totalVoucher,
      'order': instance.order,
    };
