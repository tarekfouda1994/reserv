import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_address_model/contact_us_address_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'branche_model.freezed.dart';
part 'branche_model.g.dart';

@unfreezed
class BrancheModel with _$BrancheModel {
  BrancheModel._();

  @JsonSerializable(explicitToJson: true)
  factory BrancheModel({
    @JsonKey(defaultValue: false, nullable: true) required bool showMore,
    required String nameEnglish,
    required String nameArabic,
    required List<ContactUsAddressModel> addresses,
    required List<String> mobileNo,
  }) = _BrancheModel;

  factory BrancheModel.fromJson(Map<String, dynamic> json) => _$BrancheModelFromJson(json);

  String getBranchName() {
    return getIt<Utilities>().getLocalizedValue(nameArabic, nameEnglish);
  }
}
