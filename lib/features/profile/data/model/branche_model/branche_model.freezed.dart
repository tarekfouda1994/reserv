// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'branche_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BrancheModel _$BrancheModelFromJson(Map<String, dynamic> json) {
  return _BrancheModel.fromJson(json);
}

/// @nodoc
mixin _$BrancheModel {
  @JsonKey(defaultValue: false, nullable: true)
  bool get showMore => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  set showMore(bool value) => throw _privateConstructorUsedError;
  String get nameEnglish => throw _privateConstructorUsedError;
  set nameEnglish(String value) => throw _privateConstructorUsedError;
  String get nameArabic => throw _privateConstructorUsedError;
  set nameArabic(String value) => throw _privateConstructorUsedError;
  List<ContactUsAddressModel> get addresses =>
      throw _privateConstructorUsedError;
  set addresses(List<ContactUsAddressModel> value) =>
      throw _privateConstructorUsedError;
  List<String> get mobileNo => throw _privateConstructorUsedError;
  set mobileNo(List<String> value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BrancheModelCopyWith<BrancheModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BrancheModelCopyWith<$Res> {
  factory $BrancheModelCopyWith(
          BrancheModel value, $Res Function(BrancheModel) then) =
      _$BrancheModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: false, nullable: true) bool showMore,
      String nameEnglish,
      String nameArabic,
      List<ContactUsAddressModel> addresses,
      List<String> mobileNo});
}

/// @nodoc
class _$BrancheModelCopyWithImpl<$Res> implements $BrancheModelCopyWith<$Res> {
  _$BrancheModelCopyWithImpl(this._value, this._then);

  final BrancheModel _value;
  // ignore: unused_field
  final $Res Function(BrancheModel) _then;

  @override
  $Res call({
    Object? showMore = freezed,
    Object? nameEnglish = freezed,
    Object? nameArabic = freezed,
    Object? addresses = freezed,
    Object? mobileNo = freezed,
  }) {
    return _then(_value.copyWith(
      showMore: showMore == freezed
          ? _value.showMore
          : showMore // ignore: cast_nullable_to_non_nullable
              as bool,
      nameEnglish: nameEnglish == freezed
          ? _value.nameEnglish
          : nameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      nameArabic: nameArabic == freezed
          ? _value.nameArabic
          : nameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      addresses: addresses == freezed
          ? _value.addresses
          : addresses // ignore: cast_nullable_to_non_nullable
              as List<ContactUsAddressModel>,
      mobileNo: mobileNo == freezed
          ? _value.mobileNo
          : mobileNo // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$$_BrancheModelCopyWith<$Res>
    implements $BrancheModelCopyWith<$Res> {
  factory _$$_BrancheModelCopyWith(
          _$_BrancheModel value, $Res Function(_$_BrancheModel) then) =
      __$$_BrancheModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: false, nullable: true) bool showMore,
      String nameEnglish,
      String nameArabic,
      List<ContactUsAddressModel> addresses,
      List<String> mobileNo});
}

/// @nodoc
class __$$_BrancheModelCopyWithImpl<$Res>
    extends _$BrancheModelCopyWithImpl<$Res>
    implements _$$_BrancheModelCopyWith<$Res> {
  __$$_BrancheModelCopyWithImpl(
      _$_BrancheModel _value, $Res Function(_$_BrancheModel) _then)
      : super(_value, (v) => _then(v as _$_BrancheModel));

  @override
  _$_BrancheModel get _value => super._value as _$_BrancheModel;

  @override
  $Res call({
    Object? showMore = freezed,
    Object? nameEnglish = freezed,
    Object? nameArabic = freezed,
    Object? addresses = freezed,
    Object? mobileNo = freezed,
  }) {
    return _then(_$_BrancheModel(
      showMore: showMore == freezed
          ? _value.showMore
          : showMore // ignore: cast_nullable_to_non_nullable
              as bool,
      nameEnglish: nameEnglish == freezed
          ? _value.nameEnglish
          : nameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      nameArabic: nameArabic == freezed
          ? _value.nameArabic
          : nameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      addresses: addresses == freezed
          ? _value.addresses
          : addresses // ignore: cast_nullable_to_non_nullable
              as List<ContactUsAddressModel>,
      mobileNo: mobileNo == freezed
          ? _value.mobileNo
          : mobileNo // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BrancheModel extends _BrancheModel {
  _$_BrancheModel(
      {@JsonKey(defaultValue: false, nullable: true) required this.showMore,
      required this.nameEnglish,
      required this.nameArabic,
      required this.addresses,
      required this.mobileNo})
      : super._();

  factory _$_BrancheModel.fromJson(Map<String, dynamic> json) =>
      _$$_BrancheModelFromJson(json);

  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool showMore;
  @override
  String nameEnglish;
  @override
  String nameArabic;
  @override
  List<ContactUsAddressModel> addresses;
  @override
  List<String> mobileNo;

  @override
  String toString() {
    return 'BrancheModel(showMore: $showMore, nameEnglish: $nameEnglish, nameArabic: $nameArabic, addresses: $addresses, mobileNo: $mobileNo)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_BrancheModelCopyWith<_$_BrancheModel> get copyWith =>
      __$$_BrancheModelCopyWithImpl<_$_BrancheModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BrancheModelToJson(
      this,
    );
  }
}

abstract class _BrancheModel extends BrancheModel {
  factory _BrancheModel(
      {@JsonKey(defaultValue: false, nullable: true) required bool showMore,
      required String nameEnglish,
      required String nameArabic,
      required List<ContactUsAddressModel> addresses,
      required List<String> mobileNo}) = _$_BrancheModel;
  _BrancheModel._() : super._();

  factory _BrancheModel.fromJson(Map<String, dynamic> json) =
      _$_BrancheModel.fromJson;

  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool get showMore;
  @JsonKey(defaultValue: false, nullable: true)
  set showMore(bool value);
  @override
  String get nameEnglish;
  set nameEnglish(String value);
  @override
  String get nameArabic;
  set nameArabic(String value);
  @override
  List<ContactUsAddressModel> get addresses;
  set addresses(List<ContactUsAddressModel> value);
  @override
  List<String> get mobileNo;
  set mobileNo(List<String> value);
  @override
  @JsonKey(ignore: true)
  _$$_BrancheModelCopyWith<_$_BrancheModel> get copyWith =>
      throw _privateConstructorUsedError;
}
