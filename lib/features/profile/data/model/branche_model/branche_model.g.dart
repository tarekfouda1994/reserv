// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'branche_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BrancheModel _$$_BrancheModelFromJson(Map<String, dynamic> json) =>
    _$_BrancheModel(
      showMore: json['showMore'] as bool? ?? false,
      nameEnglish: json['nameEnglish'] as String,
      nameArabic: json['nameArabic'] as String,
      addresses: (json['addresses'] as List<dynamic>)
          .map((e) => ContactUsAddressModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      mobileNo:
          (json['mobileNo'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$_BrancheModelToJson(_$_BrancheModel instance) =>
    <String, dynamic>{
      'showMore': instance.showMore,
      'nameEnglish': instance.nameEnglish,
      'nameArabic': instance.nameArabic,
      'addresses': instance.addresses.map((e) => e.toJson()).toList(),
      'mobileNo': instance.mobileNo,
    };
