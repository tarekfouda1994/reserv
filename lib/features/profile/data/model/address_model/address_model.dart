import 'package:freezed_annotation/freezed_annotation.dart';

part 'address_model.freezed.dart';
part 'address_model.g.dart';

@unfreezed
class AddressModel with _$AddressModel {
  @JsonSerializable(explicitToJson: true)
  factory AddressModel({
    required String id,
    required String addressType,
    required String locationDetail,
    required double longitude,
    required double latitude,
    required bool isDefault,
  }) = _AddressModel;

  factory AddressModel.fromJson(Map<String, dynamic> json) => _$AddressModelFromJson(json);
}
