// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'payment_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PaymentModel _$PaymentModelFromJson(Map<String, dynamic> json) {
  return _PaymentModel.fromJson(json);
}

/// @nodoc
mixin _$PaymentModel {
  String get id => throw _privateConstructorUsedError;
  set id(String value) => throw _privateConstructorUsedError;
  String get holderName => throw _privateConstructorUsedError;
  set holderName(String value) => throw _privateConstructorUsedError;
  String get cardNumber => throw _privateConstructorUsedError;
  set cardNumber(String value) => throw _privateConstructorUsedError;
  String get expiryDate => throw _privateConstructorUsedError;
  set expiryDate(String value) => throw _privateConstructorUsedError;
  String get cvv => throw _privateConstructorUsedError;
  set cvv(String value) => throw _privateConstructorUsedError;
  bool get isDefault => throw _privateConstructorUsedError;
  set isDefault(bool value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PaymentModelCopyWith<PaymentModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PaymentModelCopyWith<$Res> {
  factory $PaymentModelCopyWith(
          PaymentModel value, $Res Function(PaymentModel) then) =
      _$PaymentModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String holderName,
      String cardNumber,
      String expiryDate,
      String cvv,
      bool isDefault});
}

/// @nodoc
class _$PaymentModelCopyWithImpl<$Res> implements $PaymentModelCopyWith<$Res> {
  _$PaymentModelCopyWithImpl(this._value, this._then);

  final PaymentModel _value;
  // ignore: unused_field
  final $Res Function(PaymentModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? holderName = freezed,
    Object? cardNumber = freezed,
    Object? expiryDate = freezed,
    Object? cvv = freezed,
    Object? isDefault = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      holderName: holderName == freezed
          ? _value.holderName
          : holderName // ignore: cast_nullable_to_non_nullable
              as String,
      cardNumber: cardNumber == freezed
          ? _value.cardNumber
          : cardNumber // ignore: cast_nullable_to_non_nullable
              as String,
      expiryDate: expiryDate == freezed
          ? _value.expiryDate
          : expiryDate // ignore: cast_nullable_to_non_nullable
              as String,
      cvv: cvv == freezed
          ? _value.cvv
          : cvv // ignore: cast_nullable_to_non_nullable
              as String,
      isDefault: isDefault == freezed
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_PaymentModelCopyWith<$Res>
    implements $PaymentModelCopyWith<$Res> {
  factory _$$_PaymentModelCopyWith(
          _$_PaymentModel value, $Res Function(_$_PaymentModel) then) =
      __$$_PaymentModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String holderName,
      String cardNumber,
      String expiryDate,
      String cvv,
      bool isDefault});
}

/// @nodoc
class __$$_PaymentModelCopyWithImpl<$Res>
    extends _$PaymentModelCopyWithImpl<$Res>
    implements _$$_PaymentModelCopyWith<$Res> {
  __$$_PaymentModelCopyWithImpl(
      _$_PaymentModel _value, $Res Function(_$_PaymentModel) _then)
      : super(_value, (v) => _then(v as _$_PaymentModel));

  @override
  _$_PaymentModel get _value => super._value as _$_PaymentModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? holderName = freezed,
    Object? cardNumber = freezed,
    Object? expiryDate = freezed,
    Object? cvv = freezed,
    Object? isDefault = freezed,
  }) {
    return _then(_$_PaymentModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      holderName: holderName == freezed
          ? _value.holderName
          : holderName // ignore: cast_nullable_to_non_nullable
              as String,
      cardNumber: cardNumber == freezed
          ? _value.cardNumber
          : cardNumber // ignore: cast_nullable_to_non_nullable
              as String,
      expiryDate: expiryDate == freezed
          ? _value.expiryDate
          : expiryDate // ignore: cast_nullable_to_non_nullable
              as String,
      cvv: cvv == freezed
          ? _value.cvv
          : cvv // ignore: cast_nullable_to_non_nullable
              as String,
      isDefault: isDefault == freezed
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PaymentModel extends _PaymentModel {
  _$_PaymentModel(
      {required this.id,
      required this.holderName,
      required this.cardNumber,
      required this.expiryDate,
      required this.cvv,
      required this.isDefault})
      : super._();

  factory _$_PaymentModel.fromJson(Map<String, dynamic> json) =>
      _$$_PaymentModelFromJson(json);

  @override
  String id;
  @override
  String holderName;
  @override
  String cardNumber;
  @override
  String expiryDate;
  @override
  String cvv;
  @override
  bool isDefault;

  @override
  String toString() {
    return 'PaymentModel(id: $id, holderName: $holderName, cardNumber: $cardNumber, expiryDate: $expiryDate, cvv: $cvv, isDefault: $isDefault)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_PaymentModelCopyWith<_$_PaymentModel> get copyWith =>
      __$$_PaymentModelCopyWithImpl<_$_PaymentModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PaymentModelToJson(
      this,
    );
  }
}

abstract class _PaymentModel extends PaymentModel {
  factory _PaymentModel(
      {required String id,
      required String holderName,
      required String cardNumber,
      required String expiryDate,
      required String cvv,
      required bool isDefault}) = _$_PaymentModel;
  _PaymentModel._() : super._();

  factory _PaymentModel.fromJson(Map<String, dynamic> json) =
      _$_PaymentModel.fromJson;

  @override
  String get id;
  set id(String value);
  @override
  String get holderName;
  set holderName(String value);
  @override
  String get cardNumber;
  set cardNumber(String value);
  @override
  String get expiryDate;
  set expiryDate(String value);
  @override
  String get cvv;
  set cvv(String value);
  @override
  bool get isDefault;
  set isDefault(bool value);
  @override
  @JsonKey(ignore: true)
  _$$_PaymentModelCopyWith<_$_PaymentModel> get copyWith =>
      throw _privateConstructorUsedError;
}
