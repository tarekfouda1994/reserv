import 'package:freezed_annotation/freezed_annotation.dart';

part 'payment_model.freezed.dart';
part 'payment_model.g.dart';

@unfreezed
class PaymentModel with _$PaymentModel {
  PaymentModel._();

  @JsonSerializable(explicitToJson: true)
  factory PaymentModel({
    required String id,
    required String holderName,
    required String cardNumber,
    required String expiryDate,
    required String cvv,
    required bool isDefault,
  }) = _PaymentModel;

  factory PaymentModel.fromJson(Map<String, dynamic> json) => _$PaymentModelFromJson(json);

  Map<String, dynamic> toApiJson() => {
        "id": id,
        "holderName": holderName,
        "cardNumber": cardNumber,
        "expiryDate": expiryDate,
        "cvv": cvv,
        "isDefault": isDefault
      };
}
