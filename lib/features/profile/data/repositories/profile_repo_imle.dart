import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/profile/data/data_sources/profile_data_source.dart';
import 'package:flutter_tdd/features/profile/data/model/address_model/address_model.dart';
import 'package:flutter_tdd/features/profile/data/model/buisiness_total_favourite/business_total_favourite_model.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_model/contact_us_model.dart';
import 'package:flutter_tdd/features/profile/data/model/geo_code_address_model/geo_code_address_model.dart';
import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';
import 'package:flutter_tdd/features/profile/data/model/question_model/question_model.dart';
import 'package:flutter_tdd/features/profile/data/model/services_favourit_model/services_favourite_model.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_model/voucher_model.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_stats_model/voucher_stats.dart';
import 'package:flutter_tdd/features/profile/domain/entities/cahnge_pass_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/geo_code_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/payment_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_profile_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/voucher_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/wish_entity.dart';
import 'package:flutter_tdd/features/profile/domain/repositories/profile_repository.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ProfileRepository)
class AuthRepositoryImpl extends ProfileRepository {
  @override
  Future<Either<Failure, String>> updateProfile(
      UpdateProfileParams params) async {
    return await getIt<ProfileDataSource>().updateProfile(params);
  }

  @override
  Future<Either<Failure, UserModel>> getUserData(String params) async {
    return await getIt<ProfileDataSource>().getDataUser(params);
  }

  @override
  Future<Either<Failure, List<ServicesFavouriteModel>>> getFavouriteServices(
      TopBusinessParams params) async {
    return await getIt<ProfileDataSource>().getFavouriteServices(params);
  }

  @override
  Future<Either<Failure, bool>> updateWishes(WishParams params) async {
    return await getIt<ProfileDataSource>().updateWishes(params);
  }

  @override
  Future<Either<Failure, bool>> removeWishService(WishParams params) async {
    return await getIt<ProfileDataSource>().removeWishService(params);
  }

  @override
  Future<Either<Failure, BusinessTotalFavouriteModel>> getFavouriteBusiness(
      TopBusinessParams params) async {
    return await getIt<ProfileDataSource>().getFavouriteBusiness(params);
  }

  @override
  Future<Either<Failure, bool>> updateWishBusiness(String params) async {
    return await getIt<ProfileDataSource>().updateWishBusiness(params);
  }

  @override
  Future<Either<Failure, bool>> removeWishBusiness(String params) async {
    return await getIt<ProfileDataSource>().removeWishBusiness(params);
  }

  @override
  Future<Either<Failure, bool>> setPaymentDefault(String params) async {
    return await getIt<ProfileDataSource>().SetPaymentDefault(params);
  }

  @override
  Future<Either<Failure, bool>> createPayment(PaymentPrams params) async {
    return await getIt<ProfileDataSource>().createPayment(params);
  }

  @override
  Future<Either<Failure, bool>> deletePayment(String params) async {
    return await getIt<ProfileDataSource>().deletePayment(params);
  }

  @override
  Future<Either<Failure, List<PaymentModel>>> getAllPayment(bool params) async {
    return await getIt<ProfileDataSource>().getAllPayment(params);
  }

  @override
  Future<Either<Failure, bool>> changePass(ChangePassParams params) async {
    return await getIt<ProfileDataSource>().changePass(params);
  }

  @override
  Future<Either<Failure, List<VoucherModel>>> getAllVoucher(
      VoucherParams params) async {
    return await getIt<ProfileDataSource>().getAllVoucher(params);
  }

  @override
  Future<Either<Failure, bool>> setAddressDefault(String params) async {
    return await getIt<ProfileDataSource>().setAddressDefault(params);
  }

  @override
  Future<Either<Failure, bool>> deleteAddressCard(String params) async {
    return await getIt<ProfileDataSource>().deleteAddressCard(params);
  }

  @override
  Future<Either<Failure, List<AddressModel>>> getAllAddress(bool params) async {
    return await getIt<ProfileDataSource>().getAllAddress(params);
  }

  @override
  Future<Either<Failure, bool>> updateAddress(AddressPrams params) async {
    return await getIt<ProfileDataSource>().updateAddress(params);
  }

  @override
  Future<Either<Failure, List<QuestionModel>>> getQuestion(bool params) async {
    return await getIt<ProfileDataSource>().getQuestion(params);
  }

  @override
  Future<Either<Failure, bool>> logOut(String params) async {
    return await getIt<ProfileDataSource>().logOut(params);
  }

  @override
  Future<Either<Failure, ContactUsModel>> getContactUs(bool params) async {
    return await getIt<ProfileDataSource>().getContactUs(params);
  }

  @override
  Future<Either<Failure, List<VoucherStats>>> getVoucherStats() async {
    return await getIt<ProfileDataSource>().getVoucherStats();
  }

  @override
  Future<Either<Failure, GeoCodeAddressModel>> getAddress(
    GeoCodeAddressEntity param,
  ) async {
    return await getIt<ProfileDataSource>().getAddress(param);
  }

  @override
  Future<Either<Failure, String>> getTerms(bool params) async {
    return await getIt<ProfileDataSource>().getTerms(params);
  }
}
