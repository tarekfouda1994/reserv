import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/profile/data/model/buisiness_total_favourite/business_total_favourite_model.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_model/contact_us_model.dart';
import 'package:flutter_tdd/features/profile/data/model/geo_code_address_model/geo_code_address_model.dart';
import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';
import 'package:flutter_tdd/features/profile/data/model/question_model/question_model.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_stats_model/voucher_stats.dart';
import 'package:flutter_tdd/features/profile/domain/entities/cahnge_pass_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/geo_code_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/payment_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_profile_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/voucher_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';

import '../../../../core/models/user_model/user_model.dart';
import '../../domain/entities/wish_entity.dart';
import '../model/address_model/address_model.dart';
import '../model/services_favourit_model/services_favourite_model.dart';
import '../model/voucher_model/voucher_model.dart';

abstract class ProfileDataSource {
  Future<Either<Failure, String>> updateProfile(UpdateProfileParams params);

  Future<Either<Failure, UserModel>> getDataUser(String params);

  Future<Either<Failure, bool>> updateWishes(WishParams params);

  Future<Either<Failure, bool>> removeWishService(WishParams params);

  Future<Either<Failure, bool>> updateWishBusiness(String params);

  Future<Either<Failure, bool>> removeWishBusiness(String params);

  Future<Either<Failure, bool>> createPayment(PaymentPrams params);

  Future<Either<Failure, bool>> deletePayment(String params);

  Future<Either<Failure, bool>> logOut(String params);

  Future<Either<Failure, bool>> SetPaymentDefault(String params);

  Future<Either<Failure, List<ServicesFavouriteModel>>> getFavouriteServices(
      TopBusinessParams params);

  Future<Either<Failure, List<PaymentModel>>> getAllPayment(bool params);

  Future<Either<Failure, BusinessTotalFavouriteModel>> getFavouriteBusiness(
      TopBusinessParams params);

  Future<Either<Failure, bool>> changePass(ChangePassParams params);

  Future<Either<Failure, bool>> setAddressDefault(String params);

  Future<Either<Failure, bool>> deleteAddressCard(String params);

  Future<Either<Failure, bool>> updateAddress(AddressPrams params);

  Future<Either<Failure, List<AddressModel>>> getAllAddress(bool params);

  Future<Either<Failure, List<VoucherModel>>> getAllVoucher(
      VoucherParams params);

  Future<Either<Failure, List<QuestionModel>>> getQuestion(bool params);

  Future<Either<Failure, ContactUsModel>> getContactUs(bool params);

  Future<Either<Failure, List<VoucherStats>>> getVoucherStats();

  Future<Either<Failure, GeoCodeAddressModel>> getAddress(
    GeoCodeAddressEntity param,
  );

  Future<Either<Failure, String>> getTerms(bool params);

}
