import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/http/generic_http/generic_http.dart';
import 'package:flutter_tdd/core/http/models/http_request_model.dart';
import 'package:flutter_tdd/core/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/profile/data/data_sources/profile_data_source.dart';
import 'package:flutter_tdd/features/profile/data/model/buisiness_total_favourite/business_total_favourite_model.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_model/contact_us_model.dart';
import 'package:flutter_tdd/features/profile/data/model/geo_code_address_model/geo_code_address_model.dart';
import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';
import 'package:flutter_tdd/features/profile/data/model/question_model/question_model.dart';
import 'package:flutter_tdd/features/profile/data/model/services_favourit_model/services_favourite_model.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_model/voucher_model.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_stats_model/voucher_stats.dart';
import 'package:flutter_tdd/features/profile/domain/entities/cahnge_pass_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/geo_code_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/payment_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_profile_entity.dart';
import 'package:flutter_tdd/features/profile/domain/entities/voucher_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:injectable/injectable.dart';

import '../../domain/entities/wish_entity.dart';
import '../model/address_model/address_model.dart';

@Injectable(as: ProfileDataSource)
class AuthDataSourceImpl extends ProfileDataSource {
  @override
  Future<Either<Failure, String>> updateProfile(
      UpdateProfileParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.updateProfile,
      requestBody: params.toJson(),
      requestMethod: RequestMethod.post,
      showLoader: false,
      responseType: ResType.type,
    );
    return await GenericHttpImpl<String>()(model);
  }

  @override
  Future<Either<Failure, UserModel>> getDataUser(String params) async {
    var param = "?UserId=$params";
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getUserData + param,
      requestMethod: RequestMethod.get,
      refresh: true,
      responseType: ResType.model,
      toJsonFunc: (json) => UserModel.fromJson(json),
    );
    return await GenericHttpImpl<UserModel>()(model);
  }

  @override
  Future<Either<Failure, List<ServicesFavouriteModel>>> getFavouriteServices(
      TopBusinessParams params) async {
    @override
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getServiceFavourite,
      requestBody: params.toJson(),
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: params.refresh,
      toJsonFunc: (json) => List<ServicesFavouriteModel>.from(
          json.map((e) => ServicesFavouriteModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<ServicesFavouriteModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> updateWishes(WishParams params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.updateWishItem +
            "?businessServiceID=${params.businessServiceID}",
        requestMethod: RequestMethod.post,
        requestBody: params.toJson(),
        responseType: ResType.type,
        refresh: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> removeWishService(WishParams params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.updateWishItem +
            "?businessServiceID=${params.businessServiceID}",
        requestMethod: RequestMethod.post,
        requestBody: params.toJson(),
        responseType: ResType.type,
        refresh: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, BusinessTotalFavouriteModel>> getFavouriteBusiness(
      TopBusinessParams params) async {
    @override
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getBusinessesFavourite,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      responseKey: (data) => data,
      requestBody: params.toJson(),
      toJsonFunc: (json) => BusinessTotalFavouriteModel.fromJson(json),
    );
    return await GenericHttpImpl<BusinessTotalFavouriteModel>()(model);
  }

  @override
  Future<Either<Failure, bool>> updateWishBusiness(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.updateWishBusiness + "?businessID=$params",
        requestMethod: RequestMethod.post,
        responseType: ResType.type,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> removeWishBusiness(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.updateWishBusiness + "?businessID=$params",
        requestMethod: RequestMethod.post,
        responseType: ResType.type,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> SetPaymentDefault(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.setPaymentDefault + "?id=$params",
        requestMethod: RequestMethod.get,
        responseType: ResType.type,
        showLoader: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> createPayment(PaymentPrams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.createUpdatePayment,
      requestBody: params.toJson(),
      requestMethod: RequestMethod.post,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> deletePayment(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.deletePaymentCard + "?id=$params",
        requestMethod: RequestMethod.get,
        responseType: ResType.type,
        showLoader: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<PaymentModel>>> getAllPayment(bool params) async {
    @override
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllPayment,
      showError: false,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: params,
      toJsonFunc: (json) =>
          List<PaymentModel>.from(json.map((e) => PaymentModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<PaymentModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> changePass(ChangePassParams params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.changePass,
        requestMethod: RequestMethod.post,
        responseType: ResType.type,
        requestBody: params.toJson(),
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<VoucherModel>>> getAllVoucher(
      VoucherParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getVouchers,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      requestBody: params.toJson(),
      refresh: params.refresh ?? false,
      responseKey: (data) => data["itemsList"],
      toJsonFunc: (json) =>
          List<VoucherModel>.from(json.map((e) => VoucherModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<VoucherModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> setAddressDefault(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.setDefaultAddress + "?id=$params",
        requestMethod: RequestMethod.get,
        responseType: ResType.type,
        showLoader: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> deleteAddressCard(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.deleteAddress + "?id=$params",
        requestMethod: RequestMethod.get,
        responseType: ResType.type,
        showLoader: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<AddressModel>>> getAllAddress(bool params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllAddress,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: params,
      toJsonFunc: (json) =>
          List<AddressModel>.from(json.map((e) => AddressModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<AddressModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> updateAddress(AddressPrams params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.updateAddressCard,
        requestMethod: RequestMethod.post,
        responseType: ResType.type,
        requestBody: params.toJson(),
        showLoader: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<QuestionModel>>> getQuestion(bool params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getFAQ,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: params,
      responseKey: (data) => data,
      toJsonFunc: (json) =>
          List<QuestionModel>.from(json.map((e) => QuestionModel.fromMap(e))),
    );
    return await GenericHttpImpl<List<QuestionModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> logOut(String params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.logMeOut + "?deviceKey=$params",
        requestMethod: RequestMethod.post,
        responseType: ResType.type,
        showError: false,
        showLoader: true,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, ContactUsModel>> getContactUs(bool params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.contactUs,
      requestMethod: RequestMethod.get,
      refresh: params,
      responseType: ResType.model,
      toJsonFunc: (json) => ContactUsModel.fromJson(json),
    );
    return await GenericHttpImpl<ContactUsModel>()(model);
  }

  @override
  Future<Either<Failure, List<VoucherStats>>> getVoucherStats() async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getVoucherStats,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      responseKey: (data) => data,
      toJsonFunc: (json) => List<VoucherStats>.from(
        json.map(
          (e) => VoucherStats.fromJson(e),
        ),
      ),
    );
    return await GenericHttpImpl<List<VoucherStats>>()(model);
  }

  @override
  Future<Either<Failure, GeoCodeAddressModel>> getAddress(
      GeoCodeAddressEntity param) async {
    HttpRequestModel model = HttpRequestModel(
      url:
          "${ApiNames.GEOCODE_URL}${param.lat},${param.lng}?auth=${ApiNames.geoCodeAuthKey}&geoit=json&lang=${param.language}",
      requestMethod: RequestMethod.get,
      responseType: ResType.model,
      errorFunc: (error) => error["message"],
      toJsonFunc: (json) => GeoCodeAddressModel.fromJson(json),
    );
    return await GenericHttpImpl<GeoCodeAddressModel>()(model);
  }

  @override
  Future<Either<Failure, String>> getTerms(bool params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getTermsConditions,
      requestMethod: RequestMethod.get,
      responseKey: (data) => data["termsandConditions"],
      responseType: ResType.type,
      refresh: params,
    );
    return await GenericHttpImpl<String>()(model);
  }
}
