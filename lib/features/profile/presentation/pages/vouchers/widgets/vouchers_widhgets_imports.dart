import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/profile/data/model/voucher_stats_model/voucher_stats.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/vouchers/vouchers_imports.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../core/models/device_model/device_model.dart';
import '../../../../../../res.dart';
import '../../../../data/model/voucher_model/voucher_model.dart';

part 'build_empty_voucher.dart';
part 'build_tab_item.dart';
part 'build_tabs.dart';
part 'build_vouchers_card.dart';
