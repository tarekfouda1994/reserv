part of 'vouchers_widhgets_imports.dart';

class BuildVouchersCard extends StatelessWidget {
  final DeviceModel device;
  final VoucherModel voucherModel;
  final VoucherData voucherData;

  const BuildVouchersCard({
    Key? key,
    required this.device,
    required this.voucherModel,
    required this.voucherData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GenericBloc<bool> visibleService = GenericBloc(false);
    var lang = device.locale.languageCode;
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: visibleService,
        builder: (cxt, state) {
          return Container(
            margin: EdgeInsets.symmetric(vertical: 7),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.r),
              border: Border.all(color: MyColors.grey.withOpacity(.15)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: voucherModel.voucherStatus.getStatusBgColor(),
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(14.r),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            MyText(
                              title: getIt<Utilities>().convertNumToAr(
                                context: context,
                                value: "${voucherModel.getCurrencyName()} ${voucherModel.maximumDiscountAmount}",
                              ),
                              color: voucherModel.voucherStatus
                                  .getStatusTextColor(),
                              size: device.isTablet ? 16.sp : 20.sp,
                              fontWeight: FontWeight.w900,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: device.isTablet ? 8.w : 12.w,
                                  vertical: device.isTablet ? 12.h : 7.h),
                              margin: EdgeInsets.symmetric(
                                  vertical: device.isTablet ? 2.h : 6.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30.r),
                                color: voucherModel.voucherStatus
                                    .getStatusContainerColor(),
                                border: Border.all(
                                  color: voucherModel.voucherStatus
                                      .getStatusContainerTextColor(),
                                ),
                              ),
                              child: MyText(
                                alien: TextAlign.center,
                                size: device.isTablet ? 6.sp : 9.sp,
                                title: voucherModel.voucherStatus,
                                color: voucherModel.voucherStatus
                                    .getStatusContainerTextColor(),
                              ),
                            )
                          ]),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 20),
                        child: Row(
                          children: [
                            SvgPicture.asset(Res.ticket),
                            SizedBox(width: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                MyText(
                                    alien: TextAlign.center,
                                    size: device.isTablet ? 9.sp : 13.sp,
                                    title: voucherModel.title,
                                    color: MyColors.black,
                                    fontFamily:
                                        CustomFonts.primarySemiBoldFont),
                                MyText(
                                    alien: TextAlign.center,
                                    size: device.isTablet ? 6.sp : 10.sp,
                                    title:
                                        " ${tr("promo")}-${voucherModel.code}",
                                    color: MyColors.black)
                              ],
                            )
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          SvgPicture.asset(Res.building),
                          SizedBox(width: 10),
                          MyText(
                              alien: TextAlign.center,
                              size: device.isTablet ? 6.sp : 9.sp,
                              title: voucherModel.getBusinessName(),
                              color: MyColors.black)
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: device.isTablet ? 10.sp : 16,
                    bottom: 25,
                    left: 16,
                    right: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        alien: TextAlign.center,
                        size: device.isTablet ? 6.sp : 9.sp,
                        title: "${tr("services")} :",
                        color: MyColors.grey,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          MyText(
                            alien: TextAlign.center,
                            fontFamily: CustomFonts.primarySemiBoldFont,
                            size: device.isTablet ? 7.sp : 11.sp,
                            title: lang == "ar"
                                ? voucherModel.servicesArabicName.first
                                : voucherModel.servicesEnglishName.first,
                            color: MyColors.black,
                            fontWeight: FontWeight.bold,
                          ),
                          Visibility(
                            visible:
                                voucherModel.servicesEnglishName.length - 1 !=
                                    0,
                            child: InkWell(
                              onTap: () =>
                                  visibleService.onUpdateData(!state.data),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  MyText(
                                    alien: TextAlign.center,
                                    size: device.isTablet ? 6.sp : 9.sp,
                                    title: getIt<Utilities>().convertNumToAr(
                                      context: context,
                                      value: state.data
                                          ? "- ${tr("less")}"
                                          : "+${voucherModel.servicesEnglishName.length - 1} ${tr("more")} ",
                                    ),
                                    color: MyColors.black,
                                  ),
                                  Icon(
                                    state.data
                                        ? Icons.keyboard_arrow_up_outlined
                                        : Icons.keyboard_arrow_down_rounded,
                                    color: MyColors.black,
                                    size: 15,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox()
                        ],
                      ),
                      Visibility(
                        visible: state.data,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: MyText(
                              fontFamily: CustomFonts.primarySemiBoldFont,
                              alien: TextAlign.center,
                              size: device.isTablet ? 7.sp : 11.sp,
                              title: lang == "ar"
                                  ? voucherModel.servicesArabicName
                                      .sublist(1)
                                      .join(" - ")
                                  : voucherModel.servicesEnglishName
                                      .sublist(1)
                                      .join(" - "),
                              color: MyColors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: device.isTablet ? 7.sp : 0),
                  child: DottedLine(dashColor: MyColors.grey),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    bottom: device.isTablet ? 10.sp : 16,
                    right: 16,
                    left: 16,
                    top: 10,
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(Icons.access_time_rounded,
                              size: device.isTablet ? 10.sp : 17.sp,
                              color: MyColors.grey.withOpacity(.8)),
                          MyText(
                              alien: TextAlign.center,
                              size: device.isTablet ? 6.sp : 9.sp,
                              title: "${tr("issuance_date")} : ",
                              color: MyColors.grey),
                          MyText(
                            alien: TextAlign.center,
                            size: device.isTablet ? 6.sp : 9.sp,
                            title: voucherData.getDate(
                                device.locale.languageCode,
                                voucherModel.issuanceDate),
                            color: MyColors.black,
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          Icon(
                            Icons.access_time_rounded,
                            size: device.isTablet ? 10.sp : 17,
                            color: MyColors.grey.withOpacity(.8),
                          ),
                          MyText(
                            alien: TextAlign.center,
                            size: device.isTablet ? 6.sp : 9.sp,
                            title: "${tr("expiry_date")} : ",
                            color: MyColors.grey,
                          ),
                          MyText(
                            alien: TextAlign.center,
                            size: device.isTablet ? 6.sp : 9.sp,
                            title: voucherData.getDate(
                                device.locale.languageCode,
                                voucherModel.validUntilDate),
                            color: MyColors.black,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }
}
