part of 'vouchers_widhgets_imports.dart';

class BuildTabItem extends StatelessWidget {
  final VoucherData voucherData;
  final DeviceModel device;
  final VoucherStats voucherStatsModel;

  final int index;
  final int length;

  const BuildTabItem(
      {Key? key,
      required this.voucherData,
      required this.device,
      required this.index,
      required this.voucherStatsModel,
      required this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
        bloc: voucherData.activeTabsCubit,
        builder: (cxt, state) {
          return GestureDetector(
            onTap: () => voucherData.onTab(context, voucherStatsModel),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 18.r, vertical: 6.h),
              margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 5.r),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.r),
                  color: state.data == index
                      ? MyColors.primary
                      : MyColors.primary.withOpacity(.05)),
              child: Row(
                children: [
                  MyText(
                    alien: TextAlign.center,
                    size: device.isTablet ? 7.sp : 9.sp,
                    title: voucherStatsModel.getVoucherName(),
                    color:
                        state.data == index ? MyColors.white : MyColors.primary,
                  ),
                  Visibility(
                    visible: voucherStatsModel.totalVoucher != 0,
                    child: Container(
                      padding: EdgeInsets.all(2),
                      height: 30,
                      width: 30,
                      alignment: Alignment.center,
                      margin: EdgeInsetsDirectional.only(start: 8),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: state.data == index
                              ? MyColors.white
                              : MyColors.primary,
                        ),
                        color: state.data == index
                            ? MyColors.primary
                            : MyColors.white,
                      ),
                      child: MyText(
                        alien: TextAlign.center,
                        size: device.isTablet ? 7.sp : 10.sp,
                        title: getIt<Utilities>().convertNumToAr(
                          context: context,
                          value: voucherStatsModel.totalVoucher.toString(),
                        ),
                        color: state.data == index
                            ? MyColors.white
                            : MyColors.primary,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
