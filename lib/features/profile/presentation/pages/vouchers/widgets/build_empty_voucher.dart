part of 'vouchers_widhgets_imports.dart';

class BuildEmptyVouchers extends StatelessWidget {
  final DeviceModel device;
  final bool visible;

  const BuildEmptyVouchers({Key? key, required this.device, required this.visible})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * .19),
        child: Column(
          children: [
            SvgPicture.asset(Res.emptyVouchers),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: MyText(
                  title: tr("No_Vouchers"),
                  color: MyColors.black,
                  size: device.isTablet ? 8.sp : 12.sp),
            ),
            MyText(
                title: tr("vouchers_appear_here"),
                color: MyColors.grey,
                size: device.isTablet ? 7.sp : 11.sp),
          ],
        ),
      ),
    );
  }
}
