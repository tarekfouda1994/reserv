part of 'vouchers_widhgets_imports.dart';

class BuildTabs extends StatelessWidget {
  final VoucherData voucherData;
  final DeviceModel device;
  final int length;

  const BuildTabs(
      {Key? key,
      required this.device,
      required this.voucherData,
      required this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<VoucherStats>>,
            GenericState<List<VoucherStats>>>(
        bloc: voucherData.voucherStatusCubit,
        builder: (cxt, state) {
          return Container(
            padding:
                EdgeInsetsDirectional.only(start: device.isTablet ? 8.w : 10.w),
            height: device.isTablet ? 55.h : 50.h,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: state.data.length,
                itemBuilder: (cxt, index) {
                  return BuildTabItem(
                    length: length,
                    voucherStatsModel: state.data[index],
                    device: device,
                    voucherData: voucherData,
                    index: index,
                  );
                }),
          );
        });
  }
}
