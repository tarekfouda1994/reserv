part of 'vouchers_imports.dart';

class VoucherData {
  GenericBloc<List<VoucherModel>> voucherCubit = GenericBloc([]);
  GenericBloc<List<VoucherStats>> voucherStatusCubit = GenericBloc([]);
  GenericBloc<int> activeTabsCubit = GenericBloc(0);

  Future<void> _ficheData(BuildContext context, int statusValue,
      {bool refresh = true}) async {
    final params = _voucherParams(statusValue, refresh);
    var data = await GetAllVouchers().call(params);
    voucherCubit.onUpdateData(data);
  }

  VoucherParams _voucherParams(int statusValue, bool refresh) {
    return VoucherParams(
      pageNumber: 1,
      pageSize: 1000,
      loadImages: true,
      statusValue: statusValue,
      refresh: refresh,
    );
  }

  ficheStats(BuildContext context) async {
    var data = await GetVoucherStats()(true);
    voucherStatusCubit.onUpdateData(data);
    _ficheData(context, data.first.statusID, refresh: false);
    _ficheData(context, data.first.statusID);
  }

  onTab(BuildContext context, VoucherStats model) async {
    getIt<LoadingHelper>().showLoadingDialog();
    await _ficheData(context, model.statusID, refresh: false);
    await _ficheData(context, model.statusID);
    int index = voucherStatusCubit.state.data.indexOf(model);
    activeTabsCubit.onUpdateData(index);
    getIt<LoadingHelper>().dismissDialog();
  }

  String getDate(String languageCode, String date) {
    var formatter = DateFormat.yMMMd('ar_SA');
    if (languageCode == "ar") {
      return formatter.format(DateTime.parse(date));
    } else {
      return DateFormat("d MMM yyyy H:m").format(DateTime.parse(date));
    }
  }
}
