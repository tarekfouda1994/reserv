part of 'vouchers_imports.dart';

class Vouchers extends StatefulWidget {
  const Vouchers({Key? key}) : super(key: key);

  @override
  State<Vouchers> createState() => _VouchersState();
}

class _VouchersState extends State<Vouchers> {
  VoucherData voucherData = VoucherData();

  @override
  void initState() {
    voucherData.ficheStats(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      appBar: DefaultAppBar(title: tr("vouchers")),
      body: BlocBuilder<GenericBloc<List<VoucherModel>>,
              GenericState<List<VoucherModel>>>(
          bloc: voucherData.voucherCubit,
          builder: (cxt, state) {
            if (state is GenericUpdateState) {
              return Column(
                children: [
                  BuildTabs(
                    device: device,
                    voucherData: voucherData,
                    length: state.data.length,
                  ),
                  BuildEmptyVouchers(
                    device: device,
                    visible: state.data.isEmpty,
                  ),
                  Flexible(
                    child: ListView(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      children: [
                        ...List.generate(state.data.length, (index) {
                          return BuildVouchersCard(
                            device: device,
                            voucherModel: state.data[index],
                            voucherData: voucherData,
                          );
                        })
                      ],
                    ),
                  ),
                ],
              );
            }
            return ListView(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              children: [
                ...List.generate(3, (index) {
                  return BuildShimmerView(
                      height: MediaQuery.of(context).size.height * .3);
                })
              ],
            );
          }),
    );
  }
}
