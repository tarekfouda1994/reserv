part of 'frequently_asked_questions_Imports.dart';

class FAQ extends StatefulWidget {
  const FAQ({Key? key}) : super(key: key);

  @override
  State<FAQ> createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
  FQAData fqaData = FQAData();

  @override
  void initState() {
    fqaData.getQuestions(refresh: false);
    fqaData.getQuestions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return GradientScaffold(
      appBar: DefaultAppBar(
        title: tr("faq_name"),
        titleSize: device.isTablet ? 9.sp : 13.sp,
      ),
      body: BlocBuilder<GenericBloc<List<QuestionModel>>,
              GenericState<List<QuestionModel>>>(
          bloc: fqaData.questionCubit,
          builder: (cxt, state) {
            if (state is GenericUpdateState) {
              var items =
                  state.data.where((e) => e.questions.isNotEmpty).toList();
              return Stack(
                alignment: Alignment.topCenter,
                children: [
                  ScrollableListTabView(
                    tabHeight: device.isTablet ? 38.h : 44.h,
                    bodyAnimationDuration: const Duration(milliseconds: 100),
                    tabAnimationCurve: Curves.easeOut,
                    tabAnimationDuration: const Duration(milliseconds: 100),
                    tabs: List.generate(items.length, (indexTab) {
                      return ScrollableListTab(
                        tab: ListTab(
                          activeBackgroundColor: MyColors.primary,
                          borderRadius: BorderRadius.circular(20.r),
                          inactiveBackgroundColor:
                              MyColors.primary.withOpacity(.05),
                          borderColor: Colors.white,
                          label: Text(
                            items[indexTab].getQuestionsCateName().trim(),
                            textAlign: TextAlign.center,
                            textScaleFactor: 1.2,
                            style: TextStyle(
                              fontSize: device.isTablet ? 7.sp : 9.sp,
                              fontFamily: WidgetUtils.fontFamily,
                              fontWeight: (WidgetUtils.lang == "ar"
                                  ? FontWeight.w500
                                  : FontWeight.w200),
                            ),
                          ),
                        ),
                        body: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: BuildFeatureDetailsItem(
                            fqaData: fqaData,
                            questionModel: items[indexTab],
                          ),
                        ),
                      );
                    }),
                  ),
                  Container(
                    height: device.isTablet ? 38.h : 45.h,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Divider(height: 0),
                      ],
                    ),
                  )
                ],
              );
            }
            return ListView(
                children: List.generate(15, (index) {
              return BuildShimmerView(height: 35.h);
            }));
          }),
    );
  }
}
