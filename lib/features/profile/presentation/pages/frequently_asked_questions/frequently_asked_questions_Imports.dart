import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/scrollable_list_library.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/core/widgets/gradient_scaffold.dart';
import 'package:flutter_tdd/features/profile/data/model/question_model/question_model.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/get_questions.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/frequently_asked_questions/widgets/frequently_asked_questions_widgets_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'frequently_asked_questions.dart';
part 'frequently_asked_questions_data.dart';
