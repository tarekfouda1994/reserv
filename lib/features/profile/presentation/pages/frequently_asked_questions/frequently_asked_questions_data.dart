part of 'frequently_asked_questions_Imports.dart';

class FQAData {
  GenericBloc<String> showItemCubit = GenericBloc("");
  GenericBloc<List<QuestionModel>> questionCubit = GenericBloc([]);
  final ScrollController scrollController = ScrollController();
  int index = 0;

  getQuestions({bool refresh = true}) async {
    var data = await GetFrequentlyAskedQuestions()(refresh);
    if (data.isNotEmpty) data.first.selected = true;

    questionCubit.onUpdateData(data);
  }

  late QuestionModel questionModel;

  scrollToSubCat(GlobalKey? scrollKey, int index) {
    questionCubit.state.data.map((e) => e.selected = false).toList();
    questionCubit.state.data[index].selected = true;
    questionCubit.onUpdateData(questionCubit.state.data);
  }
}
