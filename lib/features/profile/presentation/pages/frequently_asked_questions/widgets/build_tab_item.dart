part of 'frequently_asked_questions_widgets_imports.dart';

class BuildTabItem extends StatelessWidget {
  final DeviceModel device;
  final FQAData fqaData;
  final QuestionModel questionModel;
  final int index;

  const BuildTabItem(
      {Key? key,
      required this.device,
      required this.fqaData,
      required this.questionModel,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: questionModel.questions.isNotEmpty &&
          questionModel.questions.any((e) => e.answers.isNotEmpty),
      child: InkWell(
        onTap: () => fqaData.scrollToSubCat(questionModel.key, index),
        borderRadius: BorderRadius.circular(35.r),
        child: Container(
          padding: EdgeInsets.symmetric(
              horizontal: device.isTablet ? 8.w : 12.w, vertical: device.isTablet ? 6.sp : 10),
          margin: EdgeInsets.symmetric(horizontal: 5, vertical: device.isTablet ? 6.sp : 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: questionModel.selected ? MyColors.primary : MyColors.primary.withOpacity(.04)),
          child: MyText(
              size: device.isTablet ? 6.sp : 10.sp,
              title: questionModel.getQuestionsCateName(),
              color: questionModel.selected ? MyColors.white : MyColors.primary),
        ),
      ),
    );
  }
}
