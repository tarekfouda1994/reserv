import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/bloc/device_cubit/device_cubit.dart';
import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/models/device_model/device_model.dart';
import '../../../../data/model/question_model/question_model.dart';
import '../frequently_asked_questions_Imports.dart';

part 'build_feature_details_item.dart';
part 'build_header.dart';
part 'build_item.dart';
part 'build_tab_item.dart';
part 'build_tabs.dart';
