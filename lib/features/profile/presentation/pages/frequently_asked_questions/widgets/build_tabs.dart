part of 'frequently_asked_questions_widgets_imports.dart';

class BuildTabs extends StatelessWidget {
  final DeviceModel device;
  final FQAData fqaData;
  final List<QuestionModel> questionsModel;

  const BuildTabs(
      {Key? key, required this.device, required this.questionsModel, required this.fqaData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:
          EdgeInsets.only(top: device.isTablet ? 10.sp : 16, bottom: device.isTablet ? 5.sp : 8),
      height: device.isTablet ? 35.sp : 60,
      child: Column(
        children: [
          Divider(height: 0, color: MyColors.primary.withOpacity(.05), thickness: 1.5),
          Flexible(
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: questionsModel.length,
                itemBuilder: (cxt, index) {
                  return BuildTabItem(
                      fqaData: fqaData,
                      device: device,
                      index: index,
                      questionModel: questionsModel[index]);
                }),
          ),
          Divider(
            height: 0,
            color: MyColors.primary.withOpacity(.05),
            thickness: 1.5,
          )
        ],
      ),
    );
  }
}
