part of 'frequently_asked_questions_widgets_imports.dart';

class BuildItem extends StatelessWidget {
  final FQAData fqaData;
  final int index;
  final QuestionModel questionModel;

  const BuildItem(
      {Key? key,
      required this.fqaData,
      required this.index,
      required this.questionModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;

    return BlocBuilder<GenericBloc<String>, GenericState<String>>(
      bloc: fqaData.showItemCubit,
      builder: (cxt, state) {
        return Visibility(
          visible: questionModel.questions[index].answers.isNotEmpty,
          child: InkWell(
            onTap: () => _onSwitchItem(state),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 15),
                  color: state.data == questionModel.questions[index].id
                      ? MyColors.primary
                      : MyColors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: MyText(
                          title: questionModel.questions[index]
                              .getQuestionsTitle(),
                          color: state.data == questionModel.questions[index].id
                              ? MyColors.white
                              : MyColors.primary,
                          size: device.isTablet ? 6.sp : 10.sp,
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(8),
                        height: 22.r,
                        width: 22.r,
                        decoration: BoxDecoration(
                            color: MyColors.white,
                            shape: BoxShape.circle,
                            border: Border.all(color: MyColors.primary)),
                        child: SvgPicture.asset(
                          state.data == questionModel.questions[index].id
                              ? Res.minusSmall
                              : Res.plusSmall,
                          height: 15.r,
                          width: 15.r,
                          color: state.data == questionModel.questions[index].id
                              ? MyColors.primary
                              : MyColors.black,
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: state.data == questionModel.questions[index].id,
                  child: Container(
                    color: MyColors.white,
                    width: MediaQuery.of(context).size.width,
                    child: Column(children: [
                      ...List.generate(
                        questionModel.questions[index].answers.length,
                        (indexAnswer) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 7,
                              horizontal: 15,
                            ),
                            child: Row(
                              children: [
                                Container(
                                  padding:
                                      EdgeInsets.all(device.isTablet ? 5 : 8),
                                  margin: EdgeInsetsDirectional.only(end: 8),
                                  decoration: BoxDecoration(
                                    color: MyColors.primary.withOpacity(.05),
                                    shape: BoxShape.circle,
                                  ),
                                  child: MyText(
                                    title: "${indexAnswer + 1}",
                                    color: MyColors.primary,
                                    size: device.isTablet ? 7.sp : 11.sp,
                                  ),
                                ),
                                Expanded(
                                  child: MyText(
                                    title: questionModel
                                        .questions[index].answers[indexAnswer]
                                        .getAnswerTitle(),
                                    color: MyColors.black,
                                    size: device.isTablet ? 6.sp : 9.sp,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    ]),
                  ),
                ),
                Visibility(
                  visible: state.data == questionModel.questions[index].id &&
                      questionModel.questions[index].additionalNotes != null,
                  child: Container(
                    color: MyColors.white,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 15, right: 15, bottom: 11.h),
                    child: MyText(
                      title:
                          questionModel.questions[index].additionalNotes ?? "",
                      color: MyColors.black,
                      size: device.isTablet ? 6.sp : 9.sp,
                    ),
                  ),
                ),
                Divider(
                  height: 0,
                  thickness:
                      state.data == questionModel.questions[index].id ? 4 : 1,
                  color: state.data == questionModel.questions[index].id
                      ? MyColors.primary
                      : MyColors.white,
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void _onSwitchItem(GenericState<String> state) {
    if (state.data == questionModel.questions[index].id) {
      fqaData.showItemCubit.onUpdateData("");
    } else {
      fqaData.showItemCubit.onUpdateData(questionModel.questions[index].id);
    }
  }
}
