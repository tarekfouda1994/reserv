part of 'frequently_asked_questions_widgets_imports.dart';

class BuildFeatureDetailsItem extends StatelessWidget {
  final FQAData fqaData;
  final QuestionModel questionModel;
  final String? svgIcon;
  final Widget? iconWidget;

  const BuildFeatureDetailsItem({
    Key? key,
    required this.fqaData,
    this.svgIcon,
    this.iconWidget,
    required this.questionModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          BuildHeader(
            svgIcon: svgIcon,
            iconWidget: iconWidget,
            questionModel: questionModel,
          ),
          ...List.generate(
            questionModel.questions.length,
            (index) => BuildItem(
              fqaData: fqaData,
              index: index,
              questionModel: questionModel,
            ),
          ),
        ],
      ),
    );
  }
}
