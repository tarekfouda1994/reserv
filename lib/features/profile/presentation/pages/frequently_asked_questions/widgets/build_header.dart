part of 'frequently_asked_questions_widgets_imports.dart';

class BuildHeader extends StatelessWidget {
  final String? svgIcon;
  final Widget? iconWidget;
  final QuestionModel questionModel;

  const BuildHeader(
      {Key? key, this.svgIcon, this.iconWidget, required this.questionModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Visibility(
      visible: questionModel.questions.isNotEmpty &&
          questionModel.questions.any((e) => e.answers.isNotEmpty),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            MyText(
                title: questionModel.getQuestionsCateName(),
                color: MyColors.primary,
                size: device.isTablet ? 7.sp : 11.sp,
                fontWeight: FontWeight.bold),
            Visibility(
              visible: questionModel.questions.isNotEmpty,
              child: Container(
                margin: EdgeInsets.all(8.r),
                padding: EdgeInsets.all(6.r),
                decoration: BoxDecoration(
                    color: MyColors.primary, shape: BoxShape.circle),
                child: MyText(
                  title: getIt<Utilities>().convertNumToAr(
                    context: context,
                    value: questionModel.questions
                        .where((element) => element.answers.isNotEmpty)
                        .length
                        .toString(),
                  ),
                  color: MyColors.white,
                  size: device.isTablet ? 8.sp : 10.sp,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
