import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_page_error.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/features/profile/data/model/businesse_favourite_model/businesse_favourite_model.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/favourite_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/widgets/favourite_widget_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_business_card.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

part 'businesses.dart';
part 'businesses_data.dart';
