part of 'businesses_imports.dart';

class Businesses extends StatefulWidget {
  final FavouriteData favouriteData;

  const Businesses({Key? key, required this.favouriteData}) : super(key: key);

  @override
  _BusinessesState createState() => _BusinessesState();
}

class _BusinessesState extends State<Businesses> {
  @override
  void initState() {
    widget.favouriteData.pagingController.addPageRequestListener((pageKey) {
      widget.favouriteData.ficheBusinessData(context, pageKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return RefreshIndicator(
      color: MyColors.primary,
      onRefresh: () => widget.favouriteData.ficheBusinessData(context, 1),
      child: PagedListView<int, BusinesseFavouriteModel>(
        pagingController: widget.favouriteData.pagingController,
        builderDelegate: PagedChildBuilderDelegate<BusinesseFavouriteModel>(
          firstPageProgressIndicatorBuilder: (context) => Column(
            children: List.generate(
                5,
                (index) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: BuildShimmerView(
                          height: device.isTablet ? 180.h : 140.h),
                    )),
          ),
          itemBuilder: (context, item, index) => SizedBox(
            child: BuildBusinessCard(
              shareLink: () => widget.favouriteData
                  .shareSinglePage(context, item.id, item.getBusinessName()),
              updateWish: () => widget.favouriteData.removeBusinessWish(item),
              businesseFavouriteModel: item,
            ),
          ),
          newPageProgressIndicatorBuilder: (con) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: CupertinoActivityIndicator(),
          ),
          firstPageErrorIndicatorBuilder: (context) => BuildPageError(
            onTap: () => widget.favouriteData.pagingController.refresh(),
          ),
          noItemsFoundIndicatorBuilder: (cxt) => BuildEmptyFavorite(
            device: device,
            visible: widget.favouriteData.pagingController.itemList!.isEmpty,
            title: tr("No_Businesses"),
            subTitle: tr("favorite_business_appear_here"),
          ),
        ),
      ),
    );
  }
}
