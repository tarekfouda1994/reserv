import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/features/profile/data/model/services_favourit_model/services_favourite_model.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/favourite_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/widgets/favourite_widget_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

part 'services.dart';
part 'services_data.dart';
