part of 'services_imports.dart';

class Services extends StatefulWidget {
  final FavouriteData favouriteData;

  const Services({Key? key, required this.favouriteData}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  final ServicesData servicesData = ServicesData();

  @override
  void initState() {
    widget.favouriteData.getFavourite(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServicesFavouriteModel>>,
        GenericState<List<ServicesFavouriteModel>>>(
      bloc: widget.favouriteData.favouriteCubit,
      builder: (_, state) {
        if (state is GenericUpdateState) {
          if (state.data.isEmpty) {
            return BuildEmptyFavorite(
              device: device,
              visible: state.data.isEmpty,
              title: tr("No_Services"),
              subTitle: tr("favorite_ser_appear_here"),
            );
          }
          return ListView.builder(
            itemCount: state.data.length,
            itemBuilder: (cxt, index) {
              return BuildServicesCardFavourite(
                  fromHome: false,
                  updateServiceWish: () =>
                      widget.favouriteData.updateServiceWish(state.data[index]),
                  sevicesModel: state.data[index]);
            },
          );
        }
        return ListView.builder(
          padding: EdgeInsets.symmetric(horizontal: 20),
          itemCount: 5,
          itemBuilder: (cxt, index) {
            return BuildShimmerView(height: MediaQuery.of(context).size.height * .25.h);
          },
        );
      },
    );
  }
}
