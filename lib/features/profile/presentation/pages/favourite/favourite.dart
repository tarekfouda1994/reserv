part of 'favourite_imports.dart';

class Favourite extends StatefulWidget {
  final bool fromHome;
  final HomeData? homeData;

  const Favourite({Key? key, required this.fromHome, this.homeData})
      : super(key: key);

  @override
  _FavouriteState createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> with TickerProviderStateMixin {
  final FavouriteData favouriteData = FavouriteData();

  @override
  void initState() {
    favouriteData.tabController = TabController(length: 2, vsync: this);
      favouriteData.ficheBusinessData(context, 1);
    favouriteData.getFavourite(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: GradientScaffold(
        appBar: DefaultAppBar(
            title: tr("favourites"),
            onBack: () => widget.homeData != null
                ? widget.homeData!.animateTabsPages(0, context)
                : AutoRouter.of(context).pop()),
        body: Column(
          children: [
            BuildTabBar(favouriteData: favouriteData),
            Flexible(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: favouriteData.tabController,
                children: [
                  Services(favouriteData: favouriteData),
                  Businesses(favouriteData: favouriteData),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
