part of 'favourite_widget_imports.dart';

class BuildServicesCardFavourite extends StatelessWidget {
  final bool fromHome;
  final ServicesFavouriteModel sevicesModel;
  final void Function() updateServiceWish;
  final void Function()? shareLink;

  const BuildServicesCardFavourite(
      {Key? key,
      required this.fromHome,
      required this.sevicesModel,
      required this.updateServiceWish,
      this.shareLink})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return Container(
      margin: fromHome
          ? EdgeInsetsDirectional.only(end: 12.w, bottom: 15.h, top: 5.h)
          : EdgeInsets.all(15.w),
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.borderCard),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, .1),
              offset: Offset(0.0, 2.0),
              blurRadius: 20.0,
            ),
          ],
          borderRadius: BorderRadius.circular(11.r),
          color: MyColors.white),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              AutoRouter.of(context).push(
                ReservationRootRoute(
                    businessID: sevicesModel.businessID,
                    serviceId: sevicesModel.serviceID),
              );
            },
            child: Column(
              children: [
                CachedImage(
                  url: sevicesModel.serviceAvatar.replaceAll("\\", "/"),
                  height: device.isTablet ? 240.sm : 160.sm,
                  bgColor: MyColors.defaultImgBg,
                  placeHolder: ServicePlaceholder(),
                  fit: BoxFit.cover,
                  borderWidth: 0,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(10.r)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              // Visibility(
                              //   visible: fromHome,
                              //   child: InkWell(
                              //     onTap: shareLink,
                              //     child: Container(
                              //       width: 30.r,
                              //       height: 30.r,
                              //       margin: EdgeInsets.symmetric(
                              //           vertical: 5, horizontal: 10),
                              //       decoration: BoxDecoration(
                              //         shape: BoxShape.circle,
                              //         color: MyColors.primary.withOpacity(.4),
                              //       ),
                              //       alignment: Alignment.center,
                              //       child: Icon(
                              //         Icons.share_outlined,
                              //         size: device.isTablet ? 12.sp : 18.sp,
                              //         color: MyColors.white,
                              //       ),
                              //     ),
                              //   ),
                              // ),
                              BuildFavouriteIcon(
                                onTap: updateServiceWish,
                                hasWish: true,
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: device.isTablet ? 5.sp : 8.sp,
                              horizontal: device.isTablet ? 8.sp : 10.sp,
                            ),
                            decoration: BoxDecoration(
                              color: MyColors.black,
                              borderRadius: BorderRadius.only(
                                topLeft: isEn
                                    ? Radius.circular(14.r)
                                    : Radius.circular(0),
                                topRight: isEn
                                    ? Radius.circular(0)
                                    : Radius.circular(14.r),
                              ),
                            ),
                            child: Row(
                              children: [
                                MyText(
                                  title:
                                      "${sevicesModel.getCurrencyName()} ${getIt<Utilities>().convertNumToAr(context: context, value: "${sevicesModel.servicePrice.round()}")}",
                                  color: MyColors.white,
                                  fontFamily: CustomFonts.primarySemiBoldFont,
                                  size: device.isTablet ? 7.sp : 11.sp,
                                ),
                                SizedBox(width: device.isTablet ? 8.w : 10.w),
                                Icon(
                                  Icons.access_time_outlined,
                                  size: device.isTablet ? 12.sp : 15.sp,
                                  color: MyColors.white,
                                ),
                                SizedBox(width: 5.w),
                                MyText(
                                  title:
                                      "${getIt<Utilities>().convertNumToAr(context: context, value: "${sevicesModel.serviceDuration.round()}")} ${tr("min")}",
                                  color: MyColors.white,
                                  size: device.isTablet ? 6.sp : 9.sp,
                                ),
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    children: [
                      Expanded(
                        child: MyText(
                          title: sevicesModel.getServiceName(),
                          color: MyColors.black,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                      ),
                      Offstage(
                        offstage: sevicesModel.serviceRating == 0,
                        child: Row(
                          children: [
                            MyText(
                              title: getIt<Utilities>().convertNumToAr(context: context,
                                  value: sevicesModel.serviceRating.toString()),
                              color: Color(0xffF1C800),
                              size: device.isTablet ? 7.sp : 11.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(width: 4.sp),
                            SvgPicture.asset(
                              Res.star,
                              color: Color(0xffF1C800),
                              width: 14.r,
                              height: 14.r,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => AutoRouter.of(context).push(
                    ProductDetailsRoute(id: sevicesModel.businessID),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Transform.scale(
                                  scale: .8,
                                  child: SvgPicture.asset(
                                    Res.building,
                                    width: device.isTablet ? 12.w : null,
                                  ),
                                ),
                                Expanded(
                                  child: MyText(
                                    title: sevicesModel.getBusiness(),
                                    color: MyColors.black,
                                    size: device.isTablet ? 7.sp : 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Offstage(
                                  offstage:
                                      sevicesModel.distance(context).isEmpty,
                                  child: MyText(
                                      title: sevicesModel.distance(context),
                                      color: MyColors.black,
                                      fontFamily:
                                          CustomFonts.primarySemiBoldFont,
                                      size: device.isTablet ? 5.5.sp : 9.sp),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4.h),
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                children: [
                                  Transform.scale(
                                    scale: .9,
                                    child: SvgPicture.asset(Res.marker_input,
                                        width: 14.sp, height: 15.sp),
                                  ),
                                  Expanded(
                                    child: MyText(
                                      title:
                                          "  ${sevicesModel.getServicesAddress()}",
                                      color: MyColors.blackOpacity,
                                      overflow: TextOverflow.ellipsis,
                                      size: device.isTablet ? 5.5.sp : 9.sp,
                                    ),
                                  ),
                                  SvgPicture.asset(Res.location_arrow_icon),
                                  Visibility(
                                    visible: sevicesModel.distance(context).isNotEmpty,
                                    child: InkWell(
                                      onTap: () => getIt<Utilities>()
                                          .trackingBusinessOnMap(
                                        sevicesModel.latitude,
                                        sevicesModel.longitude,
                                        sevicesModel.getBusiness(),
                                        sevicesModel.getServicesAddress(),
                                      ),
                                      child: MyText(
                                          decoration: TextDecoration.underline,
                                          title: tr("Direction"),
                                          color: Color(0xff4278F6),
                                          size:
                                              device.isTablet ? 5.5.sp : 9.sp),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
