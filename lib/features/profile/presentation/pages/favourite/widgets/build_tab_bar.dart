part of 'favourite_widget_imports.dart';

class BuildTabBar extends StatelessWidget {
  final FavouriteData favouriteData;

  const BuildTabBar({Key? key, required this.favouriteData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc, GenericState>(
        bloc: favouriteData.tabsCubit,
        builder: (context, state) {
          return BlocBuilder<GenericBloc<int>, GenericState<int>>(
              bloc: favouriteData.businessCount,
              builder: (context, businessState) {
                return BlocBuilder<GenericBloc<List<ServicesFavouriteModel>>,
                    GenericState<List<ServicesFavouriteModel>>>(
                  bloc: favouriteData.favouriteCubit,
                  builder: (context, serviceState) {
                    return Column(
                      children: [
                        Container(
                          height:device.isTablet?55.h: 50.h,
                          color: MyColors.white,
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(start: device.isTablet ? 8.w : 10.w),
                            child: Row(
                              children: [
                                BuildTabBarButton(
                                  count: serviceState.data.length,
                                  title: tr("services"),
                                  active: FavouriteTabs.services == state.data,
                                  onTap: () => favouriteData.animateToTab(FavouriteTabs.services),
                                ),
                                SizedBox(width: 5.w),
                                BuildTabBarButton(
                                  title: tr("businesses"),
                                  count: businessState.data,
                                  active: FavouriteTabs.business == state.data,
                                  onTap: () => favouriteData.animateToTab(FavouriteTabs.business),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Divider(height: 0),
                      ],
                    );
                  },
                );
              });
        });
  }
}
