part of 'favourite_widget_imports.dart';

class BuildTabBarButton extends StatelessWidget {
  final String title;
  final int? count;
  final bool active;
  final Function() onTap;

  const BuildTabBarButton({
    Key? key,
    required this.title,
    this.count,
    required this.active,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    final bgColor = active ? MyColors.primary : MyColors.primaryLightBg;
    final textColor = active ? MyColors.primaryLightBg : MyColors.primary;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18.r, vertical: 6.h),
        margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 5.r),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.r),
          color: bgColor,
        ),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              title: title,
              color: textColor,
              size: device.isTablet ? 7.sp : 9.sp,
            ),
            if (count != 0)
              Container(
                margin: EdgeInsetsDirectional.only(start: 5),
                width: device.isTablet ? 16.r : 23.r,
                height: device.isTablet ? 16.r : 23.r,
                decoration: BoxDecoration(
                  color: MyColors.primaryLightBg,
                  border: Border.all(
                    color: textColor,
                  ),
                  shape: BoxShape.circle,
                ),
                alignment: Alignment.center,
                child: MyText(
                  title: getIt<Utilities>().convertNumToAr(
                    context: context,
                    value: "$count",
                  ),
                  color: MyColors.primary,
                  size: device.isTablet ? 6.sp : 11.sp,
                ),
              )
          ],
        ),
      ),
    );
  }
}
