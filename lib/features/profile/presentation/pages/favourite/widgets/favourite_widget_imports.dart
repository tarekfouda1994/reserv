import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/enums.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/service_placeholder.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/models/device_model/device_model.dart';
import '../../../../../../core/routes/router_imports.gr.dart';
import '../../../../../../res.dart';
import '../../../../data/model/services_favourit_model/services_favourite_model.dart';
import '../favourite_imports.dart';

part 'build_empty_fovorite.dart';
part 'build_services_crard_favourite.dart';
part 'build_tab_bar.dart';
part 'build_tab_bar_button.dart';
