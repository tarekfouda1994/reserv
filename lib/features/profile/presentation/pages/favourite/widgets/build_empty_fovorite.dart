part of 'favourite_widget_imports.dart';

class BuildEmptyFavorite extends StatelessWidget {
  final DeviceModel device;
  final bool visible;
  final String title;
  final String subTitle;

  const BuildEmptyFavorite({Key? key, required this.device, required this.visible, required this.title, required this.subTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.white,
      child: Visibility(
        visible: visible,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * .19),
          child: Column(
            children: [
              SvgPicture.asset(Res.emptyFavorites),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: MyText(
                    title: title,
                    color: MyColors.black,
                    size: device.isTablet ? 8.sp : 12.sp),
              ),
              MyText(
                  title: subTitle,
                  color: MyColors.grey,
                  size: device.isTablet ? 7.sp : 11.sp),
            ],
          ),
        ),
      ),
    );
  }
}
