part of 'favourite_imports.dart';

class FavouriteData {
  late TabController tabController;
  final GenericBloc<FavouriteTabs> tabsCubit =
      GenericBloc(FavouriteTabs.services);
  final GenericBloc<int> businessCount = GenericBloc(0);
  final GenericBloc<List<ServicesFavouriteModel>> favouriteCubit =
      GenericBloc([]);

  final PagingController<int, BusinesseFavouriteModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  animateToTab(FavouriteTabs tab) {
    tabController.animateTo(tab.index);
    tabsCubit.onUpdateData(tab);
  }

  getFavourite(BuildContext context) async {
    var params = _favouriteServiceParams(context, pageSize: 100);
    var data = await GetFavouriteServices()(params);
    favouriteCubit.onUpdateData(data.reversed.toList());
  }

  TopBusinessParams _favouriteServiceParams(BuildContext context,
      {int? pageSize}) {
    var filter = context.read<SearchCubit>().state.filter;
    var lotLong = context.read<HomeLocationCubit>().state.model;
    var params = TopBusinessParams(
        loadImages: true,
        longitude: lotLong?.lng,
        latitude: lotLong?.lat,
        pageSize: pageSize ?? 5,
        pageNumber: 1,
        refresh: true,
        filter: filter);
    return params;
  }

  updateServiceWish(ServicesFavouriteModel model) async {
    var update = await RemoveWishService()(_wishParams(model));
    if (update) {
      favouriteCubit.state.data.remove(model);
      favouriteCubit.onUpdateData(favouriteCubit.state.data);
    }
  }

  WishParams _wishParams(ServicesFavouriteModel model) {
    return WishParams(
        businessServiceID: model.eBusinessServiceID,
        serviceID: model.serviceID);
  }

  ficheBusinessData(BuildContext context, int currentPage) async {
    var lotLong = context.read<HomeLocationCubit>().state.model;
    var params = TopBusinessParams(
      pageSize: pageSize,
      pageNumber: currentPage,
      longitude: lotLong?.lng,
      latitude: lotLong?.lat,
      loadImages: true,
      refresh: true,
    );
    var data = await GetFavouriteBusiness().call(params);
    final isLastPage = data!.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
    businessCount.onUpdateData(data.totalRecords);
  }

  removeBusinessWish(BusinesseFavouriteModel model) async {
    var update = await RemoveWishBusiness()(model.id);
    if (update) {
      var data = pagingController.itemList!;
      data.remove(model);
      pagingController.itemList = [];
      pagingController.itemList = data;
    }
  }

  void shareSinglePage(
      BuildContext context, String id, String businessName) async {
    return getIt<Utilities>().shareSinglePage(context, id, businessName);
  }
}
