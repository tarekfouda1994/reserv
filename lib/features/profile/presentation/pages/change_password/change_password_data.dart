part of 'change_password_imports.dart';

class ChangePasswordData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  final GenericBloc<bool> oldPassCubit = GenericBloc(false);
  final GenericBloc<bool> newPassCubit = GenericBloc(false);
  final GenericBloc<bool> confirmPassCubit = GenericBloc(false);
  TextEditingController oldPass = TextEditingController();
  TextEditingController newPass = TextEditingController();
  TextEditingController confirmNewPass = TextEditingController();

  changePass(BuildContext context) async {
    var user = context.read<UserCubit>().state.model;
    if (formKey.currentState!.validate() && !_checkPassText()) {
      btnKey.currentState?.animateForward();
      ChangePassParams changePassParams = _changePassParams(user);
      var result = await ChangePass()(changePassParams);
      if (result) {
        _onSuccess(context);
      } else {
        CustomToast.showSimpleToast(
          title: tr("wrong_password"),
          msg: tr("wrong_password_message"),
          type: ToastType.error,
        );
      }
      btnKey.currentState?.animateReverse();
    }
  }

  bool _checkPassText() {
    if (oldPass.text == confirmNewPass.text &&
        confirmNewPass.text == confirmNewPass.text) {
      CustomToast.showSimpleToast(
        msg: tr("checkPasswordMsg"),
        title: tr("checkPassword"),
      );
      return true;
    } else {
      return false;
    }
  }

  ChangePassParams _changePassParams(UserModel? user) {
    return ChangePassParams(
      email: user!.email,
      oldPassword: oldPass.text,
      newPassword: newPass.text,
    );
  }

  void _onSuccess(BuildContext context) {
    AutoRouter.of(context).pop();
    Future.delayed(
      Duration(milliseconds: 400),
      () {
        oldPass.clear();
        newPass.clear();
        confirmNewPass.clear();
        CustomToast.showSimpleToast(
          title: tr("success_toast"),
          msg: tr("success_change"),
          type: ToastType.success,
        );
      },
    );
  }
}
