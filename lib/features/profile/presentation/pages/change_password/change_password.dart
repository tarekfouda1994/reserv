part of 'change_password_imports.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
 final ChangePasswordData changePasswordData = ChangePasswordData();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: DefaultAppBar(title: tr("change_password")),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20.sm),
          child: Column(children: [
            Flexible(
              child: ListView(
                children: [
                  BuildFormInputs(changePasswordData: changePasswordData),
                ],
              ),
            ),
            BuildButton(changePasswordData: changePasswordData),
          ]),
        ),
      ),
    );
  }
}
