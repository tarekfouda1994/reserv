part of 'change_password_widget_imports.dart';

class BuildFormInputs extends StatelessWidget {
  final ChangePasswordData changePasswordData;

  const BuildFormInputs({Key? key, required this.changePasswordData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Form(
      key: changePasswordData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: changePasswordData.oldPassCubit,
              builder: (context, state) {
                return CustomTextField(
                  label: tr("old_password"),
                  controller: changePasswordData.oldPass,
                  fieldTypes:
                      !state.data ? FieldTypes.password : FieldTypes.normal,
                  type: TextInputType.visiblePassword,
                  action: TextInputAction.next,
                  validate: (value) => value?.validateEmpty(),
                      margin: EdgeInsets.only(bottom: device.isTablet ? 11.sp : 20),
                  suffixIcon: InkWell(
                      onTap: () {
                        changePasswordData.oldPassCubit
                            .onUpdateData(!state.data);
                      },
                      child: BuildSuffixIcon(
                        asset: state.data ? Res.eye : Res.eye_crossed,
                        assetColor: state.data ? MyColors.primary : null,
                      )),
                );
              }),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: changePasswordData.newPassCubit,
              builder: (context, state) {
                return CustomTextField(
                  label: tr("new_password"),
                  controller: changePasswordData.newPass,
                  fieldTypes:
                      !state.data ? FieldTypes.password : FieldTypes.normal,
                  type: TextInputType.visiblePassword,
                  action: TextInputAction.next,
                  validate: (value) => value?.validatePassword(),
                      margin: EdgeInsets.only(bottom: device.isTablet ? 11.sp : 20),
                  suffixIcon: InkWell(
                    onTap: () {
                      changePasswordData.newPassCubit.onUpdateData(!state.data);
                    },
                    child: BuildSuffixIcon(
                      asset: state.data ? Res.eye : Res.eye_crossed,
                      assetColor: state.data ? MyColors.primary : null,
                    ),
                  ),
                );
              }),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: changePasswordData.confirmPassCubit,
              builder: (context, state) {
                return CustomTextField(
                  label: tr("confirm_new_password"),
                  controller: changePasswordData.confirmNewPass,
                  fieldTypes:
                      !state.data ? FieldTypes.password : FieldTypes.normal,
                  type: TextInputType.visiblePassword,
                  action: TextInputAction.next,
                  validate: (value) => value?.validatePasswordConfirm(
                      pass: changePasswordData.newPass.text),
                      margin: EdgeInsets.only(bottom: device.isTablet ? 11.sp : 20),
                  suffixIcon: InkWell(
                    onTap: () {
                      changePasswordData.confirmPassCubit
                          .onUpdateData(!state.data);
                    },
                    child: BuildSuffixIcon(
                      asset: state.data ? Res.eye : Res.eye_crossed,
                      assetColor: state.data ? MyColors.primary : null,
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
