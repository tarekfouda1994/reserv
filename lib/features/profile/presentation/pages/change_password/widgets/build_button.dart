part of 'change_password_widget_imports.dart';

class BuildButton extends StatelessWidget {
  final ChangePasswordData changePasswordData;

  const BuildButton({Key? key, required this.changePasswordData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return LoadingButton(
      btnKey: changePasswordData.btnKey,
      title: tr("save_password"),
      onTap: () => changePasswordData.changePass(context),
      color: MyColors.primary,
      borderRadius: 30.r,
      margin: EdgeInsets.zero,
      fontSize: device.isTablet ? 7.sp : 11.sp,
      height:device.isTablet?60.sm: 50.sm,
    );
  }
}
