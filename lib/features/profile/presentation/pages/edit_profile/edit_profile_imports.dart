import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/custom_lib/country_calling/functions.dart';
import 'package:flutter_tdd/core/helpers/adaptive_picker.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/enums.dart';
import 'package:flutter_tdd/core/models/user_model/user_model.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/auth/data/model/countries_item_model/countries_item_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/ge_encrypted_value.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/get_all_countries.dart';
import 'package:flutter_tdd/features/auth/presentation/manager/user_cubit/user_cubit.dart';
import 'package:flutter_tdd/features/profile/domain/entities/update_profile_entity.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/get_user_data.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/update_profile.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'widgets/edit_profile_widgets_imports.dart';

part 'edit_profile.dart';
part 'edit_profile_data.dart';
