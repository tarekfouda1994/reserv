part of 'edit_profile_widgets_imports.dart';

class BuildEditButton extends StatelessWidget {
  final EditProfileData profileData;

  const BuildEditButton({Key? key, required this.profileData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding:  EdgeInsets.symmetric(vertical: 20.sm,horizontal: 16),
      child: LoadingButton(
        title: tr("save_details"),
        onTap: () => profileData.updateData(context),
        color: MyColors.primary,
        textColor: MyColors.white,
        btnKey: profileData.btnKey,
        borderRadius: 30.r,
        margin: EdgeInsets.zero,
        fontSize: device.isTablet ? 7.sp : 11.sp,

        height:device.isTablet?60.sm: 50.sm,
      ),
    );
  }
}
