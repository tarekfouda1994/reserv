import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/enums.dart';
import 'package:flutter_tdd/core/widgets/build_nationality_sheet.dart';
import 'package:flutter_tdd/core/widgets/build_radio_item.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/staff_placeholder.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/calendar_icon.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/email_icon.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/features/auth/data/model/countries_item_model/countries_item_model.dart';
import 'package:flutter_tdd/features/auth/presentation/manager/user_cubit/user_cubit.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/register/widgets/phone_component.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/edit_profile/edit_profile_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_custom_widgets/widgets/LoadingButton.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

part 'build_edit_button.dart';
part 'build_edit_profile_image.dart';
part 'build_edit_user_form.dart';
part 'build_gender_view.dart';
part 'build_nationality_view.dart';
part 'build_profile_image_options.dart';

