part of 'edit_profile_widgets_imports.dart';

class BuildEditProfileImage extends StatelessWidget {
  final EditProfileData profileData;

  const BuildEditProfileImage({Key? key, required this.profileData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = context.watch<UserCubit>().state.model!;
    return BlocBuilder<GenericBloc<String?>, GenericState<String?>>(
        bloc: profileData.userPhotoCubit,
        builder: (context, userPhotoState) {
          return Row(
            children: [
              SizedBox(
                height: 100.r,
                child: BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
                  bloc: profileData.imageCubit,
                  builder: (context, state) {
                    if (state.data != null) {
                      return Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: FileImage(state.data!),
                            fit: BoxFit.cover,
                          ),
                          shape: BoxShape.circle,
                        ),
                        width: 75.r,
                        height: 75.r,
                      );
                    } else if (user.photo.isNotEmpty &&
                        userPhotoState.data != null) {
                      return Visibility(
                        visible: user.photo.contains("https"),
                        child: CachedImage(
                            url: user.photo.replaceAll("\\", "/"),
                            width: 75.r,
                            height: 75.r,
                            boxShape: BoxShape.circle,
                            haveRadius: false,
                            bgColor: MyColors.defaultImgBg,
                            placeHolder:
                                StaffPlaceholder(width: 75.r, height: 75.r),
                            fit: BoxFit.cover),
                      );
                    }
                    return SvgPicture.asset(
                      Res.staff_default_img,
                      width: 75.r,
                      height: 75.r,
                    );
                  },
                ),
              ),
              BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
                  bloc: profileData.imageCubit,
                  builder: (context, state) {
                    return Row(
                      children: [
                        InkWell(
                          onTap: () => profileData.fetchUserImage(context),
                          child: BuildProfileImageOptions(
                            title: (state.data != null || user.photo.isNotEmpty)
                                ? tr("change")
                                : tr("upload"),
                            child: SvgPicture.asset(
                              (state.data != null || user.photo.isNotEmpty)
                                  ? Res.pencil
                                  : Res.upload,
                              color: MyColors.primary,
                              width: 12.r,
                              height: 12.r,
                            ),
                          ),
                        ),
                        if (profileData.imageCubit.state.data != null)
                          InkWell(
                            onTap: () {
                              profileData.imageCubit.onUpdateData(null);
                            },
                            child: BuildProfileImageOptions(
                                title: tr("delete"),
                                fontColor: MyColors.errorColor,
                                child: SvgPicture.asset(Res.trash)),
                          )
                      ],
                    );
                  }),
            ],
          );
        });
  }
}
