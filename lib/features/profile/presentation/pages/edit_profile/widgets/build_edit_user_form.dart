part of 'edit_profile_widgets_imports.dart';

class BuildEditUserForm extends StatelessWidget {
  final EditProfileData profileData;

  const BuildEditUserForm({Key? key, required this.profileData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserCubit>().state.model;
    return Form(
      key: profileData.formKey,
      child: Column(
        children: [
          CustomTextField(
            fieldTypes: FieldTypes.normal,
            controller: profileData.firstName,
            type: TextInputType.name,
            action: TextInputAction.next,
            validate: (value) => value?.validateName(),
            label: tr("first_name"),
            suffixIcon: BuildSuffixIcon(asset: Res.user),
            margin: EdgeInsets.only(bottom: 30.h, top: 10.h),
          ),
          CustomTextField(
            controller: profileData.lastName,
            fieldTypes: FieldTypes.normal,
            type: TextInputType.name,
            action: TextInputAction.next,
            validate: (value) => value?.validateName(),
            label: tr("last_name"),
            suffixIcon: BuildSuffixIcon(asset: Res.user),
            margin: EdgeInsets.only(bottom: 30.h),
          ),
          BuildGenderView(profileData: profileData),
          BlocConsumer<GenericBloc<String>, GenericState<String>>(
              bloc: profileData.dateCubit,
              builder: (cxt, state) {
                return CustomTextField(
                  onTab: () => profileData.setDateOfBirth(context),
                  controller: profileData.dateOfBirth,
                  fieldTypes: FieldTypes.clickable,
                  type: TextInputType.text,
                  action: TextInputAction.next,
                  label: tr("date_of_birth"),
                  validate: (value) =>
                      profileData.dateOfBirth.text.validateEmpty(),
                  hint: tr("date_hint"),
                  margin: EdgeInsets.only(bottom: 30.h),
                  suffixIcon: CalendarIcon(),
                );
              },
              listener: (cxt, listener) {
                profileData.dateOfBirth.text = listener.data;
              }),
          CustomTextField(
            controller: profileData.email,
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            label: tr("email"),
            validate: (value) => value?.validateEmail(),
            hint: "email@example.com",
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            margin: EdgeInsets.only(bottom: 30.h),
            suffixIcon: EmailIcon(),
          ),
          // BuildPhoneView(profileData: profileData),
          PhoneComponent(
            phone: profileData.phone,
            codeCubit: profileData.codeCubit,
            onViewCountries: () => profileData.showCountryDialog(context),
          ),
          CustomTextField(
            controller: profileData.nationality,
            onTab: () => nationalitySheet(context),
            fieldTypes: FieldTypes.clickable,
            type: TextInputType.text,
            action: TextInputAction.done,
            label: tr("nationality"),
            validate: (value) => profileData.nationality.text.validateEmpty(),
            hint: tr("nationality"),
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            margin: EdgeInsets.only(bottom: 15.h),
          ),
        ],
      ),
    );
  }

  void nationalitySheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: MyColors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25).r,
        ),
        builder: (cxt) {
          return BuildNationalitySheet(
            prevSelectedItem: profileData.nationality.text,
            onSelectCallBac: (item) {
              profileData.nationality.text = item.getNationName();
              profileData.selectedNationality = item;
            },
          );
        });
  }
}
