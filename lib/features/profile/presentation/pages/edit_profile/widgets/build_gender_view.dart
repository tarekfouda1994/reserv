part of 'edit_profile_widgets_imports.dart';

class BuildGenderView extends StatelessWidget {
  final EditProfileData profileData;

  const BuildGenderView({Key? key, required this.profileData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<Genders?>, GenericState<Genders?>>(
      bloc: profileData.genderCubit,
      builder: (context, state) {
        return Padding(
          padding: EdgeInsetsDirectional.only(bottom: 20, start: 6),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                title: tr("gender"),
                color: MyColors.blackOpacity,
                size: device.isTablet ? 5.sp : 9.sp,
              ),
              SizedBox(height: 10.r),
              Row(
                children: [
                  Expanded(
                    child: BuildRadioItem(
                        title: tr("male"),
                        changeValue: state.data,
                        onTap: () => profileData.genderCubit
                            .onUpdateData(Genders.male),
                        value: Genders.male),
                  ),
                  Expanded(
                    child: BuildRadioItem(
                        title: tr("female"),
                        changeValue: state.data,
                        onTap: () => profileData.genderCubit
                            .onUpdateData(Genders.female),
                        value: Genders.female),
                  ),

                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
