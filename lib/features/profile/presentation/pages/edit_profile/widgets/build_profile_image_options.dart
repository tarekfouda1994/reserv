part of'edit_profile_widgets_imports.dart';

class BuildProfileImageOptions extends StatelessWidget {
  final Widget child;
  final Color? fontColor;
  final String title;
  const BuildProfileImageOptions({Key? key, required this.child,  this.fontColor, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        Container(
            margin: EdgeInsets.symmetric(horizontal: 15,vertical: 3.h),
            width: 32.r,
            height: 32.r,
            decoration: BoxDecoration(
              color: MyColors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: MyColors.grey.withOpacity(.5),
                  offset: Offset(0.0, 2.0),
                  blurRadius: 5.0,
                ),
              ],
              shape: BoxShape.circle,
            ),
            alignment: Alignment.center,
            child:child),
        MyText(title: title, color:fontColor?? MyColors.primary, size: device.isTablet?5.sp:9.sp)
      ],
    );
  }
}
