part of 'edit_profile_imports.dart';

class EditProfileData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  final GenericBloc<Genders?> genderCubit = GenericBloc(null);
  final GenericBloc<File?> imageCubit = GenericBloc(null);
  final GenericBloc<String?> userPhotoCubit = GenericBloc(null);
  final GenericBloc<bool> termsCubit = GenericBloc(false);
  final GenericBloc<bool> offersCubit = GenericBloc(false);
  final GenericBloc<String> dateCubit = GenericBloc('');
  final GenericBloc<Country?> codeCubit = GenericBloc(null);

  final TextEditingController dateOfBirth = TextEditingController();

  final TextEditingController firstName = TextEditingController();
  final TextEditingController lastName = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController nationality = TextEditingController();
  CountriesItemModel? selectedNationality;

  DateTime? dateTimeOfBirth;

  void initProfileData(BuildContext context) {
    var user = context.read<UserCubit>().state.model;
    firstName.text = user!.firstName;
    lastName.text = user.lastName;
    email.text = user.email;
    initPhoneCode(user.phoneNumber, context);
    if (user.photo.isNotEmpty) userPhotoCubit.onUpdateData(user.photo);
    _setBirthDate(context);
    _setInitialGender(user);
    setInitialNationalValue(user.nationalityCountryId.toString());
  }

  void _setBirthDate(BuildContext context) {
    var user = context.read<UserCubit>().state.model;
    if (user?.birthDate != "0001-01-01T00:00:00" && user?.birthDate != null) {
      dateTimeOfBirth = DateTime.parse(user!.birthDate);
      dateCubit.onUpdateData(DateFormat("dd/MM/yyyy").format(dateTimeOfBirth!));
      dateOfBirth.text = DateFormat("dd/MM/yyyy").format(dateTimeOfBirth!);
    } else {
      dateOfBirth.text = "";
    }
  }

  void _setInitialGender(UserModel user) {
    if (user.genderEnglishName.isNotEmpty) {
      var gender =
          user.genderEnglishName == "Male" ? Genders.male : Genders.female;
      genderCubit.onUpdateData(gender);
    }
  }

  initPhoneCode(String mobile, BuildContext context) async {
    var phoneNum = mobile.replaceAll("+", "");
    var data = await getCountries(context);
    var country = data.firstWhereOrNull((e) {
      return phoneNum.split(' ').first.replaceAll("+", "") ==
          e.callingCode.replaceAll("+", "");
    });
    if (country != null) {
      _setInitPhoneCtr(phoneNum, country.callingCode);
      codeCubit.onUpdateData(
        Country(
          name: country.name,
          countryCode: country.countryCode,
          phoneCode: country.callingCode,
          e164Sc: 0,
          geographic: false,
          level: 0,
          example: '',
          displayName: '',
          displayNameNoCountryCode: '',
          e164Key: '',
        ),
      );
    }
  }

  void _setInitPhoneCtr(String phoneNum, String callingCode) {
    var mobileNumber =
        phoneNum.replaceAll(callingCode.replaceAll("+", ""), "").trim();
    phone.text = mobileNumber;
  }

  void showCountryDialog(BuildContext context) async {
    getIt<Utilities>().showCountryDialog(
      context,
      onSelect: (country) => codeCubit.onUpdateData(country),
    );
  }

  setInitialNationalValue(String id) async {
    var data = await getApiCounties();
    var item = data.firstWhereOrNull((element) => element.encryptId == id);
    if (item != null) {
      nationality.text = item.getNationName();
      selectedNationality = item;
    }
  }

  fetchUserImage(BuildContext context) async {
    var result = await getIt<Utilities>().getImage();
    imageCubit.onUpdateData(result);
    userPhotoCubit.onUpdateData("hhj");
    cropImage(context, result);
  }

  Future<void> cropImage(BuildContext context, File? image) async {
    getIt<Utilities>().cropImage(
      context,
      image,
      callback: (CroppedFile? croppedFile) {
        if (croppedFile != null) {
          imageCubit.onUpdateData(File(croppedFile.path));
        } else {
          imageCubit.onUpdateData(null);
        }
      },
    );
  }

  setDateOfBirth(BuildContext context) {
    AdaptivePicker.datePicker(
        context: context,
        title: tr("select_date_of_birth"),
        initial: DateTime.now().subtract(Duration(days: 1)),
        minDate: DateTime(1800),
        maxDate: DateTime.now(),
        onConfirm: (date) {
          if (date != null) {
            dateCubit.onUpdateData(DateFormat("dd/MM/yyyy").format(date));
            dateOfBirth.text = date.toUtc().toString();
            dateTimeOfBirth = date;
          }
        });
  }

  Future<List<CountriesItemModel>> getApiCounties() async {
    var data = await GetAllCounties().call(
      CountriesParams(
          isActive: true, pageNumber: 1, pageSize: 222, refresh: false),
    );
    return data;
  }

  Future<String?> getEncryptedValue(BuildContext context) async {
    var data = await GetEncryptedValue().call(1);
    return data;
  }

  updateData(BuildContext context) async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (genderCubit.state.data == null) {
      _genderValidation();
      return;
    }
    if (formKey.currentState!.validate()) {
      btnKey.currentState?.animateForward();
      var user = context.read<UserCubit>().state.model;
      final params = _updateProfileParams(user);
      await UpdateProfile().call(params).then((result) async {
        if (result != null) {
          var data = await GetUserData().call(user!.userId);
          _updateUserData(data, user, context);
          imageCubit.onUpdateData(null);
        }
      });
      btnKey.currentState?.animateReverse();
    }
  }

  void onArrowBack(BuildContext context, bool notCompletedData) {
    if (notCompletedData) {
      AutoRouter.of(context).pushAndPopUntil(
        HomeRoute(),
        predicate: (cxt) => false,
      );
    } else {
      Navigator.of(context).pop();
    }
  }

  Future<bool> onBack(BuildContext context, bool notCompletedData) async {
    onArrowBack(context, notCompletedData);
    return true;
  }

  _genderValidation() {
    CustomToast.showSimpleToast(
      title: tr("fieldsValidation"),
      msg: tr("genderValidation"),
      type: ToastType.error,
    );
  }

  void _updateUserData(UserModel? data, UserModel user, BuildContext context) {
    if (data != null) {
      var updatedUser = data.copyWith(token: user.token, userId: user.userId);
      getIt<Utilities>().saveUserData(updatedUser);
      context.read<UserCubit>().onUpdateUserData(updatedUser);
      _onSuccessUpdate(context);
    }
  }

  void _onSuccessUpdate(BuildContext context) {
    AutoRouter.of(context).pushAndPopUntil(
      HomeRoute(),
      predicate: (cxt) => false,
    );
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        CustomToast.showSnakeBar(
          tr("saveUpdateProfileMsg"),
          tr("successfulUpdate"),
          type: ToastType.success,
        );
      },
    );
  }

  String _base64Image() {
    String base64Image = "";
    if (imageCubit.state.data != null) {
      base64Image =
          "data:image/${imageCubit.state.data?.path.split(".").last};base64,";
      List<int> imageBytes = imageCubit.state.data!.readAsBytesSync();
      base64Image += base64Encode(imageBytes);
    }
    return base64Image;
  }

  String _phoneNum() {
    var callingCode = codeCubit.state.data?.phoneCode ?? "+971 ";
    var phoneNum = phone.text;
    if (phoneNum.startsWith("0")) {
      phoneNum = "$callingCode " + phoneNum.substring(1);
    } else {
      phoneNum = "$callingCode " + phoneNum;
    }
    return phoneNum;
  }

  UpdateProfileParams _updateProfileParams(UserModel? user) {
    String base64Image = _base64Image();
    String phoneNum = _phoneNum();
    return UpdateProfileParams(
      genderValue: genderCubit.state.data!.index + 1,
      nationalityId: selectedNationality!.encryptId,
      userPhoto: _userPhoto(base64Image, user),
      email: email.text,
      phoneNumber: phoneNum,
      birthDate: DateFormat("yyyy-MM-dd 'T' hh:mm:ss")
          .format(dateTimeOfBirth!)
          .replaceAll(" ", ""),
      firstName: firstName.text,
      id: user?.userId,
      lastName: lastName.text,
    );
  }

  String _userPhoto(String base64Image, UserModel? user) => base64Image.isEmpty
      ? (userPhotoCubit.state.data == null ? "" : user?.photo ?? "")
      : base64Image;
}
