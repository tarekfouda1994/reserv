part of 'edit_profile_imports.dart';

class EditProfile extends StatefulWidget {
  final bool notCompletedData;

  const EditProfile({Key? key, required this.notCompletedData}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final EditProfileData profileData = EditProfileData();

  @override
  void initState() {
    profileData.initProfileData(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return WillPopScope(
      onWillPop: () => profileData.onBack(context, widget.notCompletedData),
      child: Scaffold(
        appBar: DefaultAppBar(
          title: tr("myProfile"),
          onBack: () => profileData.onArrowBack(
            context,
            widget.notCompletedData,
          ),
        ),
        body: GestureDetector(
          onTap: FocusScope.of(context).unfocus,
          child: Column(
            children: [
              Flexible(
                child: ListView(
                  padding: EdgeInsets.symmetric(
                    horizontal: device.isTablet ? 10.sp : 15.w,
                  ),
                  children: [
                    BuildEditProfileImage(profileData: profileData),
                    BuildEditUserForm(profileData: profileData)
                  ],
                ),
              ),
              BuildEditButton(profileData: profileData)
            ],
          ),
        ),
      ),
    );
  }
}
