part of 'setting_imports.dart';

class Setting extends StatefulWidget {
  final bool formIntroScreen;

  const Setting({Key? key, required this.formIntroScreen}) : super(key: key);

  @override
  State<Setting> createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  SettingData settingData = SettingData();

  @override
  void initState() {
    settingData.initialLang(context);
    super.initState();
  }

  void refresh() {
    Future.delayed(Duration(milliseconds: 500), () {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      appBar: DefaultAppBar(
          title: tr("setting"),
          onBack: () => AutoRouter.of(context).pushAndPopUntil(
              HomeRoute(index: device.auth ? 3 : 2),
              predicate: (p) => true)),
      body: BlocBuilder<GenericBloc<Locale>, GenericState<Locale>>(
          bloc: settingData.langItemCubit,
          builder: (cxt, state) {
            return ListView(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 30),
                children: [
                  MyText(
                      title: tr("lang"),
                      color: MyColors.black,
                      size: device.isTablet ? 8.sp : 12.sp),
                  Padding(
                    padding: EdgeInsets.only(top: 15.h, bottom: 8.h),
                    child: BuildRadioItem(
                      title: "العربية",
                      changeValue: state.data,
                      onTap: () => settingData.selectLang(
                          context, Locale('ar', 'EG'), refresh),
                      value: Locale('ar', 'EG'),
                      fontFamily: CustomFonts.arabicFont,
                    ),
                  ),
                  BuildRadioItem(
                    title: tr("langEn"),
                    changeValue: state.data,
                    onTap: () => settingData.selectLang(
                        context, Locale('en', 'US'), refresh),
                    value: Locale('en', 'US'),
                  ),
                ]);
          }),
    );
  }
}
