part of 'setting_widgets_imports.dart';

class BuildLangAlert extends StatelessWidget {
  final SettingData settingData;

  const BuildLangAlert({Key? key, required this.settingData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<Locale>, GenericState<Locale>>(
      bloc: settingData.langItemCubit,
      builder: (cxt, state) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(14),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.r)),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // GestureDetector(
              //   onTap: () => settingData.selectLang(context, Locale('ar', 'EG')),
              //   child: Container(
              //       width: MediaQuery.of(context).size.width,
              //       padding: EdgeInsets.all(16),
              //       margin: EdgeInsets.symmetric(vertical: 5),
              //       decoration: BoxDecoration(
              //           color: state.data == Locale('ar', 'EG') ? MyColors.primary : MyColors.white,
              //           borderRadius: BorderRadius.circular(13),
              //           border: Border.all(color: MyColors.primary)),
              //       child: MyText(
              //           title: tr("arabic"),
              //           alien: TextAlign.center,
              //           color: state.data == Locale('ar', 'EG') ? MyColors.white : MyColors.primary,
              //           size: 12.sp,
              //           fontWeight: FontWeight.bold)),
              // ),
              // GestureDetector(
              //   onTap: () => settingData.selectLang(context, Locale('en', 'US')),
              //   child: Container(
              //     width: MediaQuery.of(context).size.width,
              //     padding: EdgeInsets.all(16),
              //     margin: EdgeInsets.symmetric(vertical: 5),
              //     decoration: BoxDecoration(
              //         color: state.data == Locale('en', 'US') ? MyColors.primary : MyColors.white,
              //         borderRadius: BorderRadius.circular(13),
              //         border: Border.all(color: MyColors.primary)),
              //     child: MyText(
              //         title: tr("langEn"),
              //         alien: TextAlign.center,
              //         color: state.data == Locale('en', 'US') ? MyColors.white : MyColors.primary,
              //         size: 12.sp,
              //         fontWeight: FontWeight.bold),
              //   ),
              // ),
            ],
          ),
        );
      },
    );
  }
}
