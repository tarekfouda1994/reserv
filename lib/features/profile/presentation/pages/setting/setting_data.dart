part of 'setting_imports.dart';

class SettingData {
  final GenericBloc<Locale> langItemCubit = GenericBloc(Locale('en', 'US'));

  langAlert(BuildContext context, SettingData settingData) {
    showDialog(
        context: context,
        builder: (cxt) {
          return BuildLangAlert(settingData: settingData);
        });
  }

  initialLang(BuildContext context) {
    Locale locale = context.read<DeviceCubit>().state.model.locale;
    if (locale == Locale('en', 'US')) {
      langItemCubit.onUpdateData(Locale('en', 'US'));
    } else {
      langItemCubit.onUpdateData(Locale('ar', 'EG'));
    }
  }

  selectLang(BuildContext context, Locale locale, Function() onRefresh) async {
    context.read<DeviceCubit>().updateLanguage(locale);
    langItemCubit.onUpdateData(locale);
    await _saveLangValue(context, locale);
    onRefresh();
  }

  _saveLangValue(BuildContext context, Locale locale) async {
    var prefs = await SharedPreferences.getInstance();
    if (locale.languageCode != prefs.getString("lang")) {
      Phoenix.rebirth(context);
    }
    prefs.setString("lang", locale.languageCode);
  }
}
