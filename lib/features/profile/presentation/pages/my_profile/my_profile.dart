part of 'my_profile_imports.dart';

class MyProfile extends StatefulWidget {
  const MyProfile({Key? key}) : super(key: key);

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  final MyProfileData myProfileData = MyProfileData();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: BuildProfileAppBar(),
      body: ListView(
        children: [
          BuildProfileCard(),
          BuildProfileItem(
            imageSize: 20.r,
            title: tr("my_appointment"),
            image: Res.appointmentActive,
            onTap: () => AutoRouter.of(context).push(HomeRoute(index: 1)),
          ),
          BuildProfileItem(
            title: tr("addresses"),
            image: Res.adresses,
            onTap: () => AutoRouter.of(context).push(AddressesRoute()),
          ),
          BuildProfileItem(
            title: tr("payment_methods"),
            image: Res.payment_methods,
            onTap: () => AutoRouter.of(context).push(PaymentRoute()),
          ),
          BuildProfileItem(
            title: tr("vouchers"),
            image: Res.voucher,
            onTap: () => AutoRouter.of(context).push(VouchersRoute()),
          ),
          BuildProfileItem(
            title: tr("change_password"),
            image: Res.password,
            onTap: () => AutoRouter.of(context).push(ChangePasswordRoute()),
          ),
          BuildProfileItem(
            title: tr("settings"),
            image: Res.settings,
            onTap: () => AutoRouter.of(context).push(
              SettingRoute(formIntroScreen: false),
            ),
          ),
          BuildProfileItem(
            title: tr("faq"),
            image: Res.faq,
            onTap: () => AutoRouter.of(context).push(FAQRoute()),
          ),
          BuildProfileItem(
            title: tr("contact_us"),
            image: Res.contact,
            onTap: () => AutoRouter.of(context).push(ContactUsRoute()),
          ),
          BuildProfileItem(
            title: tr("logout"),
            image: Res.logout,
            onTap: () => myProfileData.logOut(context),
          ),
        ],
      ),
    );
  }
}
