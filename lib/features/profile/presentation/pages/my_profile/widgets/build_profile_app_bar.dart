part of 'my_profile_widgets_imports.dart';

class BuildProfileAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  const BuildProfileAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return DefaultAppBar(title: tr("myProfile"), showBack: false, actions: [
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: () => AutoRouter.of(context).push(
              EditProfileRoute(notCompletedData: false),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 6.h),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.r),
                  color: MyColors.white),
              child: Row(
                children: [
                  SvgPicture.asset(
                    Res.pencil,
                    width: 12.w,
                    color: Color(0xff1A65FB),
                    height: 12.h,
                  ),
                  SizedBox(width: 4.w),
                  MyText(
                    title: tr("edit"),
                    color: Color(0xff1A65FB),
                    size: device.isTablet ? 6.sp : 13.sp,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ]);
  }

  @override
  Size get preferredSize {
    final cxt = getIt<GlobalContext>().context();
    final isTablet = cxt.read<DeviceCubit>().state.model.isTablet;
    return Size.fromHeight(isTablet ? 35.h : 40.h);
  }
}
