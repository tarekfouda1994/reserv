part of 'my_profile_widgets_imports.dart';

class BuildProfileItem extends StatelessWidget {
  final String title;
  final String image;
  final double? imageSize;
  final Widget? screen;
  final Function()? onTap;

  const BuildProfileItem({
    Key? key,
    required this.title,
    required this.image,
    this.screen,
    this.onTap, this.imageSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    var item = Container(
      padding: EdgeInsets.symmetric(
        horizontal: device.isTablet ? 12.w : 20.w,
      ),
      margin: EdgeInsets.only(top: 10.h),
      height: 40.h,
      child: Row(
        children: [
          SvgPicture.asset(
            image,
            width:imageSize?? 25.r,
            height:imageSize?? 25.r,
          ),
          SizedBox(width: 14.w),
          MyText(
            title: title,
            fontFamily: device.locale.languageCode=="en"
                ? "workSans"
                : "readexProMedium",
            color: MyColors.black,
            size: device.isTablet ? 7.sp : 11.5.sp,
          ),
          Spacer(),
          Icon(
            device.locale == Locale('en', 'US') ? Icons.east : Icons.west,
            size: device.isTablet ? 16.sp : 18.sp,
            color: Color(0xff005858),
          )
        ],
      ),
    );

    return InkWell(
      onTap: onTap,
      child: item,
    );
  }
}
