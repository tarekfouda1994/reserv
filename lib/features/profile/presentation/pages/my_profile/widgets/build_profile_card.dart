part of 'my_profile_widgets_imports.dart';

class BuildProfileCard extends StatelessWidget {
  const BuildProfileCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = context.watch<UserCubit>().state.model!;
    final device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(bottom: 15.h),
      decoration: BoxDecoration(color: Color(0xffF6F9F9)),
      width: MediaQuery.of(context).size.width,
      child: Row(
        children: [
          SizedBox(
            width: 85.r,
            height: 85.r,
            child: Stack(
              children: [
                CachedImage(
                  url: user.photo.replaceAll("\\", "/"),
                  width: 75.r,
                  height: 75.r,
                  boxShape: BoxShape.circle,
                  haveRadius: false,
                  fit: BoxFit.cover,
                  bgColor: MyColors.defaultImgBg,
                  placeHolder: StaffPlaceholder(height: 70.r, width: 70.r),
                ),
                Positioned(
                  bottom: 0.h,
                  right: 0.w,
                  child: SvgPicture.asset(
                    Res.pro_badge,
                    width: 40.r,
                    height: 40.r,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  title: tr("hi"),
                  color: MyColors.blackOpacity,
                  fontFamily: device.locale.languageCode=="en"
                      ? "workSans"
                      : "readexProMedium",
                  size: device.isTablet ? 7.sp : 12.sp,
                ),
                SizedBox(width: 6),
                MyText(
                  title: "${user.firstName} ${user.lastName}",
                  color: MyColors.black,
                  size: device.isTablet ? 8.sp : 12.sp,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
