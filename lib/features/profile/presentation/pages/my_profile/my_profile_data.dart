part of 'my_profile_imports.dart';

class MyProfileData {
  logOut(BuildContext context) async {
    var deviceId = await FirebaseMessaging.instance.getToken();
    await LogOut()(deviceId!).then((value) async {
      GoogleSignIn().signOut();
      await FacebookAuth.instance.logOut().then(
            (value) => FirebaseAuth.instance.signOut(),
          );
      getIt<SharedPreferences>().remove("token");
      getIt<SharedPreferences>().remove("user").then((value) {
        // context.read<HomeLocationCubit>().onLocationUpdated(null);
        GlobalState.instance.set("token", "");
        context.read<DeviceCubit>().updateLanguage(Locale('en', 'US'));
        Future.delayed(Duration(milliseconds: 400), () {
          Phoenix.rebirth(context);
        });
      });
    });
  }
}
