part of 'payment_widgtes_imports.dart';

class BuildPaymentHeader extends StatelessWidget {
  final DeviceModel model;

  const BuildPaymentHeader({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.primaryLight,
      padding: EdgeInsets.all(20).r,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6),
            child: SvgPicture.asset(
              Res.payment_methods,
              width: model.isTablet ? 45.h : 30.h,
              height: model.isTablet ? 45.h : 30.h,
            ),
          ),
          Container(
            width: model.isTablet
                ? MediaQuery.of(context).size.width * .16.w
                : MediaQuery.of(context).size.width * .55.w,
            child: MyText(
                title: tr("securely_save_card"),
                color: MyColors.black,
                size: model.isTablet ? 6.sp : 10.sp),
          )
        ],
      ),
    );
  }
}
