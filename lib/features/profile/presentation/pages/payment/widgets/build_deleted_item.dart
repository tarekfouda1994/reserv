part of 'payment_widgtes_imports.dart';

class BuildDeleteItem extends StatelessWidget {
  final PaymentData paymentData;
  final DeviceModel model;
  final PaymentModel paymentModel;

  const BuildDeleteItem(
      {Key? key, required this.paymentData, required this.model, required this.paymentModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 14.h, horizontal: device.isTablet ? 10.w : 14.w),
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
              color: MyColors.primary.withOpacity(.05), borderRadius: BorderRadius.circular(15)),
          child: Row(children: [
            Padding(
              padding: const EdgeInsetsDirectional.only(end: 10),
              child: SvgPicture.asset(
                  paymentModel.cardNumber.startsWith("5") ? Res.mastercard : Res.visacard),
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              MyText(
                  title:
                      paymentModel.cardNumber.startsWith("5") ? tr("Mastercard") : tr("Visa_card"),
                  color: MyColors.black.withOpacity(.6),
                  size: device.isTablet ? 7.sp : 10.sp),
              SizedBox(height: 4),
              MyText(
                  title:
                      "${paymentModel.cardNumber.split("").first}*********${paymentModel.cardNumber.substring(paymentModel.cardNumber.length - 2, paymentModel.cardNumber.length)}",
                  color: MyColors.black,
                  fontFamily: CustomFonts.primarySemiBoldFont,
                  size: device.isTablet ? 8.sp : 12.sp),
            ]),
          ]),
        )
      ],
    );
  }
}
