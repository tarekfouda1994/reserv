part of 'payment_widgtes_imports.dart';

class BuildPaymentItem extends StatelessWidget {
  final PaymentData paymentData;
  final DeviceModel model;
  final PaymentModel paymentModel;

  const BuildPaymentItem(
      {Key? key,
      required this.paymentData,
      required this.model,
      required this.paymentModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        InkWell(
          onTap: () => paymentData.setCardDefault(paymentModel),
          child: Container(
            padding: EdgeInsets.symmetric(
                vertical: device.isTablet ? 10.sp : 16.h,
                horizontal: device.isTablet ? 10.sp : 16.w),
            margin: EdgeInsets.symmetric(
                vertical: device.isTablet ? 4.sp : 7, horizontal: 16),
            decoration: BoxDecoration(
              color: paymentModel.isDefault ? MyColors.primary : null,
              borderRadius: BorderRadius.circular(15.r),
              border: Border.all(
                color: paymentModel.isDefault
                    ? MyColors.primary
                    : MyColors.grey.withOpacity(.15),
              ),
            ),
            child: Column(
              children: [
                Row(children: [
                  Expanded(
                    child: MyText(
                        title: paymentModel.holderName,
                        color: paymentModel.isDefault
                            ? MyColors.white
                            : MyColors.grey,
                        size: device.isTablet ? 7.sp : 11.sp),
                  ),
                  Container(
                      padding: EdgeInsets.all(device.isTablet ? 2.w : 2.h),
                      margin: EdgeInsets.symmetric(
                          horizontal: device.isTablet ? 5.sp : 8),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: paymentModel.isDefault
                                  ? MyColors.white
                                  : MyColors.primary.withOpacity(.5),
                              width: .5),
                          color: paymentModel.isDefault
                              ? MyColors.primary
                              : MyColors.white),
                      child: Icon(Icons.done,
                          color: MyColors.white,
                          size: device.isTablet ? 10.sp : 14.sp)),
                  MyText(
                      title: paymentModel.isDefault
                          ? tr("default")
                          : tr("set_default"),
                      color: paymentModel.isDefault
                          ? MyColors.white
                          : MyColors.grey,
                      size: device.isTablet ? 7.sp : 11.sp),
                ]),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Row(children: [
                    Expanded(
                      child: Row(children: [
                        SvgPicture.asset(paymentModel.cardNumber.startsWith("5")
                            ? Res.mastercard
                            : Res.visacard),
                        MyText(
                            title:
                                "  ${paymentModel.cardNumber.split("").first}*********${paymentModel.cardNumber.substring(paymentModel.cardNumber.length - 2, paymentModel.cardNumber.length)}",
                            color: paymentModel.isDefault
                                ? MyColors.white
                                : MyColors.black,
                            fontFamily: CustomFonts.primarySemiBoldFont,
                            size: device.isTablet ? 7.sp : 11.sp),
                      ]),
                    ),
                    InkWell(
                      onTap: () => paymentData.bottomSheetEditCard(
                          context, model, paymentData, paymentModel),
                      child: SvgPicture.asset(
                        Res.edit,
                        color: paymentModel.isDefault
                            ? MyColors.white
                            : MyColors.black,
                        height: device.isTablet ? 12.w : 17.w,
                        width: device.isTablet ? 12.w : 17.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => paymentData.bottomSheetDeleteCard(
                        context,
                        model,
                        paymentModel,
                        paymentData,
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.w),
                        child: SvgPicture.asset(
                          Res.trash,
                          color: paymentModel.isDefault
                              ? MyColors.white
                              : MyColors.black,
                          height: device.isTablet ? 12.w : 18.w,
                          width: device.isTablet ? 12.w : 18.w,
                        ),
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
