part of 'payment_widgtes_imports.dart';

class BuildDeleteBottomSheet extends StatelessWidget {
  final DeviceModel model;
  final PaymentModel paymentModel;
  final PaymentData paymentData;

  const BuildDeleteBottomSheet(
      {Key? key,
      required this.model,
      required this.paymentData,
      required this.paymentModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 16.sm, horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              ladingIcon: SvgPicture.asset(Res.trash),
              margin: const EdgeInsets.only(bottom: 15).r,
              title: tr("delete_card"),
              titleColor: Colors.red,
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(height: 5),
              MyText(
                title: tr("alert_delete"),
                color: Colors.black,
                size: model.isTablet ? 7.sp : 10.sp,
                fontWeight: FontWeight.bold,
              ),
              BuildDeleteItem(
                paymentModel: paymentModel,
                model: model,
                paymentData: paymentData,
              ),
            ]),
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () => AutoRouter.of(context).pop(),
                    child: Container(
                      alignment: Alignment.center,
                      height: model.isTablet ? 60.sm : 50.sm,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.r),
                        color: MyColors.black,
                      ),
                      child: MyText(
                          alien: TextAlign.center,
                          size: model.isTablet ? 7.sp : 11.sp,
                          title: tr("keep"),
                          color: MyColors.white),
                    ),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: InkWell(
                    onTap: () => paymentData.deleteCard(context, paymentModel),
                    child: Container(
                      alignment: Alignment.center,
                      height: model.isTablet ? 60.sm : 50.sm,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30.r),
                          border: Border.all(color: MyColors.errorColor)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MyText(
                              alien: TextAlign.center,
                              size: model.isTablet ? 7.sp : 11.sp,
                              title: tr("delete"),
                              color: MyColors.errorColor),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
