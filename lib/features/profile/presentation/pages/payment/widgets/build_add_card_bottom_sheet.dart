part of 'payment_widgtes_imports.dart';

class BuildAddCardBottomSheet extends StatelessWidget {
  final DeviceModel model;
  final PaymentData paymentData;

  const BuildAddCardBottomSheet(
      {Key? key, required this.model, required this.paymentData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20.sm),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(12.r),
            ),
            color: MyColors.white),
        child: Form(
          key: paymentData.addFormKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BuildHeaderBottomSheet(
                margin: EdgeInsets.only(bottom: 15.h),
                title: tr("add_new_card"),
              ),
              SizedBox(height: 7),
              CustomTextField(
                fillColor: Color(0xffF6F9F9),
                label: tr("holder_name"),
                controller: paymentData.holderName,
                margin: EdgeInsets.only(top: 10, bottom: 15.h),
                fieldTypes: FieldTypes.normal,
                hint: tr("holder_name"),
                type: TextInputType.name,
                action: TextInputAction.next,
                suffixIcon: UserIcon(),
                validate: (value) => value?.validateName(),
              ),
              CustomTextField(
                fillColor: Color(0xffF6F9F9),
                controller: paymentData.cardNumber,
                margin: EdgeInsets.only(top: 10, bottom: 15.h),
                fieldTypes: FieldTypes.normal,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(16),
                  if (model.locale.languageCode == "en") CardNumberFormatter(),
                ],
                label: tr("card_number"),
                hint: 'XXXX XXXX XXXX XXXX',
                type: TextInputType.number,
                action: TextInputAction.next,
                suffixIcon: CreditCard(),
                validate: (value) => value?.validateCard(context),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 4,
                    child: CustomTextField(
                      fillColor: Color(0xffF6F9F9),
                      controller: paymentData.expiryDate,
                      margin: EdgeInsets.only(top: 10, bottom: 15.h),
                      fieldTypes: FieldTypes.normal,
                      type: TextInputType.datetime,
                      hint:
                          "${tr("ex")} (${DateFormat("MM/yyyy").format(DateTime.now())})",
                      label: tr("expiry_date"),
                      action: TextInputAction.next,
                      inputFormatters: [
                        CardExpirationFormatter(),
                        LengthLimitingTextInputFormatter(7),
                      ],
                      validate: (value) => value?.validateExpiryDate(),
                      suffixIcon: CalendarIcon(),
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    flex: 3,
                    child: CustomTextField(
                      fillColor: Color(0xffF6F9F9),
                      controller: paymentData.cvv,
                      margin: EdgeInsets.only(top: 10, bottom: 15.h),
                      fieldTypes: FieldTypes.password,
                      hint: "xxx",
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(3),
                      ],
                      label: tr("CVV"),
                      type: TextInputType.number,
                      maxLength: 3,
                      suffixIcon: CreditCard(),
                      action: TextInputAction.done,
                      validate: (value) => value?.validateCVV(),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: DefaultButton(
                      onTap: () => AutoRouter.of(context).pop(),
                      title: tr("cancel"),
                      color: MyColors.black.withOpacity(.05),
                      textColor: MyColors.blackOpacity,
                      borderColor: MyColors.white,
                      borderRadius: BorderRadius.circular(40.r),
                      margin: EdgeInsets.zero,
                      fontSize: model.isTablet ? 7.sp : 11.sp,

                      height: model.isTablet ? 60.sm : 50.sm,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: DefaultButton(
                      onTap: () => paymentData.addCard(context),
                      title: tr("add_card"),
                      color: MyColors.primary,
                      textColor: MyColors.white,
                      borderColor: MyColors.white,
                      borderRadius: BorderRadius.circular(40.r),
                      margin: EdgeInsets.zero,
                      fontSize: model.isTablet ? 7.sp : 11.sp,

                      height: model.isTablet ? 60.sm : 50.sm,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
