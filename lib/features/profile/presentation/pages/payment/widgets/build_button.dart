part of 'payment_widgtes_imports.dart';

class BuildButton extends StatelessWidget {
  final DeviceModel model;
  final PaymentData data;
  final Color? bgColor;
  final Color? textColor;
  final CrossAxisAlignment? crossAxisAlignment;
  final bool? showDivider;

  const BuildButton(
      {Key? key,
      required this.model,
      required this.data,
      this.bgColor,
      this.textColor,
      this.crossAxisAlignment,
      this.showDivider})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return DefaultButton(
      title: '+ ${tr("add_card")}',
      onTap: () => data.bottomSheetAddCard(context, model, data),
      color: MyColors.primary,
      textColor: MyColors.white,
      borderRadius: BorderRadius.circular(30.r),
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 20.sm),
      fontSize: model.isTablet ? 7.sp : 11.sp,

      height:device.isTablet?60.sm: 50.sm,
    );
  }
}
