part of 'payment_widgtes_imports.dart';

class BuildEditCardBottomSheet extends StatelessWidget {
  final DeviceModel model;
  final PaymentData paymentData;
  final PaymentModel paymentModel;

  const BuildEditCardBottomSheet(
      {Key? key,
      required this.model,
      required this.paymentData,
      required this.paymentModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    paymentData.setCaredDefaultCubit.onUpdateData(paymentModel.isDefault);
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20.sm),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(12.r),
            ),
            color: MyColors.white),
        child: Form(
          key: paymentData.updateFormKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BuildHeaderBottomSheet(
                margin: EdgeInsets.only(bottom: 15.h),
                title: tr("edit_card"),
              ),
              SizedBox(height: 7),
              CustomTextField(
                controller: paymentData.editHolderName,
                margin: EdgeInsets.only(top: 10, bottom: 15.h),
                fieldTypes: FieldTypes.normal,
                hint: tr("holder_name"),
                fillColor: Color(0xffF6F9F9),
                label: tr("holder_name"),
                type: TextInputType.name,
                action: TextInputAction.next,
                suffixIcon: UserIcon(),
                validate: (value) => value?.validateName(),
              ),
              CustomTextField(
                controller: paymentData.editCardNumber,
                hint: tr("card_number"),
                label: tr("card_number"),
                fillColor: Color(0xffF6F9F9),
                margin: EdgeInsets.only(top: 10, bottom: 15.h),
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(16),
                  if (model.locale.languageCode == "en") CardNumberFormatter(),
                ],
                fieldTypes: FieldTypes.normal,
                type: TextInputType.number,
                action: TextInputAction.next,
                suffixIcon: CreditCard(),
                validate: (value) => value?.validateCard(context),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextField(
                          fillColor: Color(0xffF6F9F9),
                          controller: paymentData.editExpiryDate,
                          margin: EdgeInsets.only(top: 10, bottom: 15.h),
                          fieldTypes: FieldTypes.normal,
                          type: TextInputType.datetime,
                          hint:
                              "${tr("ex")} (${DateFormat("MM/yyyy").format(DateTime.now())})",
                          label: tr("expiry_date"),
                          action: TextInputAction.next,
                          inputFormatters: [
                            CardExpirationFormatter(),
                            LengthLimitingTextInputFormatter(7),
                          ],
                          validate: (value) => value?.validateExpiryDate(),
                          suffixIcon: CalendarIcon(),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextField(
                          label: tr("CVV"),
                          controller: paymentData.editCvv,
                          hint: "xxx",
                          margin: EdgeInsets.only(top: 10, bottom: 15.h),
                          fieldTypes: FieldTypes.password,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(3),
                          ],
                          type: TextInputType.number,
                          action: TextInputAction.done,
                          suffixIcon: CreditCard(),
                          validate: (value) => value?.validateCVV(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
                  bloc: paymentData.setCaredDefaultCubit,
                  builder: (context, state) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: InkWell(
                        onTap: () => paymentData.setCaredDefaultCubit
                            .onUpdateData(!state.data),
                        borderRadius: BorderRadius.circular(100),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                padding: EdgeInsets.all(2.h),
                                margin: EdgeInsets.symmetric(
                                    horizontal: isTablet ? 5.sp : 8),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: MyColors.primary.withOpacity(.3),
                                        width: .5),
                                    color: state.data
                                        ? MyColors.primary
                                        : MyColors.white),
                                child: Icon(Icons.done,
                                    color: MyColors.white,
                                    size: isTablet ? 10.sp : 14.sp)),
                            MyText(
                                title: tr("set_default"),
                                color: MyColors.black.withOpacity(.5),
                                size: isTablet ? 8.sp : 12.sp),
                          ],
                        ),
                      ),
                    );
                  }),
              const SizedBox(height: 15),
              Row(
                children: [
                  Expanded(
                    child: DefaultButton(
                      onTap: () => AutoRouter.of(context).pop(),
                      title: tr("cancel"),
                      color: MyColors.black.withOpacity(.05),
                      textColor: MyColors.blackOpacity,
                      borderColor: MyColors.white,
                      borderRadius: BorderRadius.circular(40.r),
                      margin: EdgeInsets.zero,
                      fontSize: model.isTablet ? 7.sp : 11.sp,
                      height: model.isTablet ? 60.sm : 50.sm,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: DefaultButton(
                      onTap: () =>
                          paymentData.updateCard(context, paymentModel),
                      title: tr("save_card"),
                      color: MyColors.primary,
                      textColor: MyColors.white,
                      borderColor: MyColors.white,
                      borderRadius: BorderRadius.circular(40.r),
                      margin: EdgeInsets.zero,
                      fontSize: model.isTablet ? 7.sp : 11.sp,
                      height: model.isTablet ? 60.sm : 50.sm,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
