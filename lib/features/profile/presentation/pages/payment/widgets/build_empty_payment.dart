part of 'payment_widgtes_imports.dart';

class BuildEmptyPayment extends StatelessWidget {
  final DeviceModel device;
  final bool visible;
  final PaymentData paymentData;

  const BuildEmptyPayment(
      {Key? key, required this.device, required this.visible, required this.paymentData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height * .19),
        child: Column(
          children: [
            SvgPicture.asset(Res.emptyPaymentMethods),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: MyText(
                  title: tr("No_Payment_Method"),
                  color: MyColors.black,
                  size: device.isTablet ? 8.sp : 12.sp),
            ),
            MyText(
                title: tr("payment_method_appear_here"),
                color: MyColors.grey,
                size: device.isTablet ? 7.sp : 11.sp),
          ],
        ),
      ),
    );
  }
}
