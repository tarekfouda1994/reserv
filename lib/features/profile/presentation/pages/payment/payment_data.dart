part of 'payment_imports.dart';

class PaymentData {
  final GlobalKey<CustomButtonState> btnKeb = GlobalKey();
  final GlobalKey<FormState> addFormKey = GlobalKey();
  final GlobalKey<FormState> updateFormKey = GlobalKey();
  final GenericBloc<List<PaymentModel>> paymentCubit = GenericBloc([]);
  final GenericBloc<bool> setCaredDefaultCubit = GenericBloc(false);
  final GenericBloc<String> exDateCubit = GenericBloc("");
  TextEditingController holderName = TextEditingController();
  TextEditingController cardNumber = TextEditingController();
  TextEditingController expiryDate = TextEditingController();
  TextEditingController cvv = TextEditingController();
  TextEditingController editHolderName = TextEditingController();
  TextEditingController editCardNumber = TextEditingController();
  TextEditingController editExpiryDate = TextEditingController();
  TextEditingController editCvv = TextEditingController();

  bottomSheetAddCard(
      BuildContext context, DeviceModel model, PaymentData paymentData) {
    Widget widget = BuildAddCardBottomSheet(
      model: model,
      paymentData: paymentData,
    );
    _showBottomSheet(context, widget: widget);
  }

  bottomSheetEditCard(BuildContext context, DeviceModel model,
      PaymentData paymentData, PaymentModel paymentModel) {
    _initPaymentCard(paymentModel);
    Widget widget = BuildEditCardBottomSheet(
      model: model,
      paymentData: paymentData,
      paymentModel: paymentModel,
    );
    _showBottomSheet(context, widget: widget);
  }

  _initPaymentCard(PaymentModel paymentModel) {
    editCardNumber.text = paymentModel.cardNumber;
    editCvv.text = paymentModel.cvv;
    editExpiryDate.text = paymentModel.expiryDate;
    editHolderName.text = paymentModel.holderName;
  }

  bottomSheetDeleteCard(
    BuildContext context,
    DeviceModel model,
    PaymentModel paymentModel,
    PaymentData paymentData,
  ) {
    Widget widget = BuildDeleteBottomSheet(
      model: model,
      paymentModel: paymentModel,
      paymentData: paymentData,
    );
    _showBottomSheet(context, widget: widget);
  }

  void _showBottomSheet(BuildContext context, {required Widget widget}) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) {
        return widget;
      },
    );
  }

  addCard(BuildContext context) async {
    if (addFormKey.currentState!.validate()) {
      getIt<LoadingHelper>().showLoadingDialog();
      PaymentPrams prams = PaymentPrams(
          holderName: holderName.text,
          expiryDate: expiryDate.text,
          cardNumber: cardNumber.text,
          cvv: cvv.text,
          isDefault: false);
      var result = await CreateUpdatePayment()(prams);
      _handleAddResult(context, result);
    }
  }

  _handleAddResult(BuildContext context, bool result) {
    if (result) {
      expiryDate.clear();
      holderName.clear();
      cardNumber.clear();
      cvv.clear();
      getAllPayment();
      getIt<LoadingHelper>().dismissDialog();
      AutoRouter.of(context).pop().then((value) {
        CustomToast.showSimpleToast(
          msg: tr("success_create"),
          type: ToastType.success,
          title: tr("success_toast"),
        );
      });
    }
  }

  getAllPayment({bool refresh = true}) async {
    var data = await GetAllPayment()(refresh);
    paymentCubit.onUpdateData(data);
  }

  updateCard(BuildContext context, PaymentModel paymentModel) async {
    if (updateFormKey.currentState!.validate()) {
      getIt<LoadingHelper>().showLoadingDialog();
      PaymentPrams prams = PaymentPrams(
        id: paymentModel.id,
        holderName: editHolderName.text,
        expiryDate: editExpiryDate.text,
        cardNumber: editCardNumber.text,
        cvv: editCvv.text,
        isDefault: setCaredDefaultCubit.state.data,
      );
      var result = await CreateUpdatePayment()(prams);
      getIt<LoadingHelper>().dismissDialog();
      if (result) {
        _updatePaymentCubit(paymentModel);
        AutoRouter.of(context).pop().then((value) {
          CustomToast.showSimpleToast(
            msg: tr("success_edit"),
            type: ToastType.success,
            title: tr("success_toast"),
          );
        });
      }
    }
  }

  _updatePaymentCubit(PaymentModel paymentModel) {
    int index = paymentCubit.state.data.indexOf(paymentModel);
    paymentCubit.state.data[index].holderName = editHolderName.text;
    paymentCubit.state.data[index].expiryDate = editExpiryDate.text;
    paymentCubit.state.data[index].cardNumber = editCardNumber.text;
    paymentCubit.state.data[index].cvv = editCvv.text;
    paymentCubit.state.data[index].isDefault = setCaredDefaultCubit.state.data;
    if (paymentCubit.state.data[index].isDefault) {
      paymentCubit.state.data.map((e) => e.isDefault = false).toList();
      paymentCubit.state.data[index].isDefault = true;
    }
    paymentCubit.onUpdateData(paymentCubit.state.data);
  }

  deleteCard(BuildContext context, PaymentModel model) async {
    getIt<LoadingHelper>().showLoadingDialog();
    var result = await DeletePaymentCard()(model.id);
    getIt<LoadingHelper>().dismissDialog();
    if (result) {
      AutoRouter.of(context).pop();
      paymentCubit.state.data.remove(model);
      paymentCubit.onUpdateData(paymentCubit.state.data);
      Future.delayed(Duration(milliseconds: 500), () {
        CustomToast.showSimpleToast(
          msg: tr("deleteCard"),
          type: ToastType.success,
          title: tr("success_toast"),
        );
      });
    }
  }

  setCardDefault(PaymentModel model) async {
    if (model.isDefault == false) {
      getIt<LoadingHelper>().showLoadingDialog();
      var result = await SetPaymentDefault()(model.id);
      getIt<LoadingHelper>().dismissDialog();
      if (result) {
        paymentCubit.state.data
            .map((e) => e.isDefault = e.isDefault = false)
            .toList();
        model.isDefault = true;
        paymentCubit.onUpdateData(paymentCubit.state.data);
        CustomToast.showSimpleToast(
          msg: tr("success_edit"),
          type: ToastType.success,
          title: tr("success_toast"),
        );
      }
    }
  }
}
