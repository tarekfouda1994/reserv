part of 'payment_imports.dart';

class Payment extends StatefulWidget {
  const Payment({Key? key}) : super(key: key);

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  final PaymentData paymentData = PaymentData();

  @override
  void initState() {
    paymentData.getAllPayment(refresh: false);
    paymentData.getAllPayment();
    // paymentData.expiryDate.text = "${DateTime.now().month}/${DateTime.now().year}";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Scaffold(
      appBar: DefaultAppBar(
        title: tr("payment_methods"),
      ),
      body: BlocBuilder<GenericBloc<List<PaymentModel>>,
          GenericState<List<PaymentModel>>>(
        bloc: paymentData.paymentCubit,
        builder: (cxt, state) {
          if (state is GenericUpdateState) {
            return Column(
              children: [
                Flexible(
                  child: ListView(
                    children: [
                      BuildPaymentHeader(model: device),
                      BuildEmptyPayment(
                        visible: state.data.isEmpty,
                        device: device,
                        paymentData: paymentData,
                      ),
                      ...List.generate(
                        state.data.length,
                        (index) => BuildPaymentItem(
                          paymentModel: state.data[index],
                          model: device,
                          paymentData: paymentData,
                        ),
                      ),
                    ],
                  ),
                ),
                BuildButton(model: device, data: paymentData, showDivider: true)
              ],
            );
          }
          return ListView(
            padding: EdgeInsets.symmetric(horizontal: 16),
            children: [
              BuildPaymentHeader(model: device),
              ...List.generate(
                  5,
                  (index) => BuildShimmerView(
                      height: device.isTablet
                          ? 80.h
                          : MediaQuery.of(context).size.height * .2)),
            ],
          );
        },
      ),
    );
  }
}
