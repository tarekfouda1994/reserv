import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/general/domain/entities/location_entity.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/select_location_address/widgets/select_loacation_address_widget_imports.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:location/location.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'select_location_address.dart';
part 'select_location_address_data.dart';
