part of 'select_location_address_imports.dart';

class SelectLocationAddressData {
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  final GenericBloc<LocationEntity> locationCubit =
      GenericBloc(LocationEntity(address: "", lng: 0, lat: 0));
  final Completer<GoogleMapController> controller = Completer();
  late LocationEntity locationModel;
  static String androidApiKey = "AIzaSyCs4U9JRZUbfjNvvmfDcKzSoRWS3x8SEUA";
  static String iosApiKey = "AIzaSyDlP1forlVKLM3S1BtiNA-1h-5SvIKpEgw";
  static String apiKeyOuto = Platform.isIOS ? iosApiKey : androidApiKey;
  GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: apiKeyOuto);

  double lat = 31.0414217;
  double? lng = 31.3653301;
  double? zoom;
  late PageController pageController;
  late GoogleMapController controllerMap;

  void getLocationAddress(BuildContext context) async {
    LatLng loc = LatLng(locationModel.lat, locationModel.lng);
    String address = await getIt<Utilities>().getAddress(loc, context);
    locationModel.address = address;
    locationCubit.onUpdateData(locationModel);
  }

  void changeLocation(BuildContext context) async {
    locationCubit.onUpdateData(locationModel);
    Navigator.pop(context, locationModel);
  }


  Future<void> displayPrediction(BuildContext context, Prediction? p) async {
    if (p != null) {
      PlacesDetailsResponse detail = await places.getDetailsByPlaceId(p.placeId!);
      lat = detail.result.geometry!.location.lat;
      lng = detail.result.geometry!.location.lng;
      moveCamera(context, lat, lng ?? 0);
      zoom = 10;
    }
  }

  moveCamera(BuildContext context, double lat, double lng) {
    controllerMap.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, lng), zoom: 14.0, bearing: 30.0, tilt: 45.0),
      ),
    );
  }
}
