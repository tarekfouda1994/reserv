part of 'select_location_address_imports.dart';

class SelectLocationAddress extends StatefulWidget {
  final LocationData loc;

  const SelectLocationAddress({Key? key, required this.loc}) : super(key: key);

  @override
  State<SelectLocationAddress> createState() => _SelectLocationAddressState();
}

class _SelectLocationAddressState extends State<SelectLocationAddress> {
  SelectLocationAddressData locationAddressData = SelectLocationAddressData();

  @override
  void initState() {
    locationAddressData.locationModel =
        LocationEntity(lat: widget.loc.latitude!, lng: widget.loc.longitude!);
    locationAddressData.locationCubit
        .onUpdateData(locationAddressData.locationModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      key: locationAddressData._scaffold,
      appBar: AppBar(
        title: BlocBuilder<GenericBloc<LocationEntity>,
            GenericState<LocationEntity>>(
          bloc: locationAddressData.locationCubit,
          builder: (context, state) {
            return MyText(
              title: state.data.address,
              size: 14,
              color: MyColors.white,
            );
          },
        ),
        backgroundColor: MyColors.primary,
        automaticallyImplyLeading: false,
        centerTitle: true,
        flexibleSpace: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
        ),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          BuildGoogleMap(
            locationAddressData: locationAddressData,
            deviceModel: device,
          ),
          BuildSaveLocationButton(
            locationAddressData: locationAddressData,
          ),
        ],
      ),
    );
  }
}
