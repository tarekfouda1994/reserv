part of "select_loacation_address_widget_imports.dart";

class BuildGoogleMap extends StatelessWidget {
  final SelectLocationAddressData locationAddressData;
  final DeviceModel deviceModel;

  const BuildGoogleMap({required this.locationAddressData, required this.deviceModel});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<LocationEntity>, GenericState<LocationEntity>>(
      bloc: locationAddressData.locationCubit,
      builder: (context, state) {
        CameraPosition _initialLoc =
            CameraPosition(target: LatLng(state.data.lat, state.data.lng), zoom: 16.4746);
        return Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: GoogleMap(
                  mapType: MapType.normal,
                  // markers: _markers,
                  initialCameraPosition: _initialLoc,
                  onMapCreated: (GoogleMapController controller) {
                    controller.setMapStyle(json.encode(MapStyle.server));
                    locationAddressData.controller.complete(controller);
                    locationAddressData.controllerMap = controller;
                  },
                  myLocationButtonEnabled: true,
                  myLocationEnabled: true,
                  rotateGesturesEnabled: true,
                  scrollGesturesEnabled: true,
                  trafficEnabled: true,
                  zoomControlsEnabled: true,
                  tiltGesturesEnabled: true,
                  compassEnabled: true,
                  indoorViewEnabled: true,
                  buildingsEnabled: true,
                  mapToolbarEnabled: true,
                  zoomGesturesEnabled: true,
                  onCameraIdle: () {
                    locationAddressData.getLocationAddress(context);
                  },
                  onTap: (location) {
                    locationAddressData.getLocationAddress(context);
                  },
                  onCameraMove: (loc) {
                    locationAddressData.locationModel = LocationEntity(
                      lat: loc.target.latitude,
                      lng: loc.target.longitude,
                      address: "",
                    );
                  }),
            ),
            SvgPicture.asset(
              Res.map_marker_active_svg,
              width: 45,
              height: 45,
            )
          ],
        );
      },
    );
  }
}
