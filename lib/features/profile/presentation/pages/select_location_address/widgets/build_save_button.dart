part of 'select_loacation_address_widget_imports.dart';

class BuildSaveLocationButton extends StatelessWidget {
  final SelectLocationAddressData locationAddressData;

  const BuildSaveLocationButton({required this.locationAddressData});

  @override
  Widget build(BuildContext context) {
    var isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        InkWell(
          onTap: () => locationAddressData.changeLocation(context),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 8).r,
            height: isTablet ? 60.sm : 50.sm,
            width: MediaQuery.of(context).size.width * .7,
            decoration: BoxDecoration(
                color: MyColors.primary,
                borderRadius: BorderRadius.circular(30)),
            alignment: Alignment.center,
            child: MyText(
              title: tr('save_location'),
              size: isTablet ? 8.sp : 12.sp,
              color: MyColors.white,
            ),
          ),
        ),
      ],
    );
  }
}
