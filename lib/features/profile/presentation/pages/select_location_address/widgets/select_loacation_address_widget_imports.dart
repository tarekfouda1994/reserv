import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/map_style.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../core/models/device_model/device_model.dart';
import '../../../../../../res.dart';
import '../../../../../general/domain/entities/location_entity.dart';
import '../select_location_address_imports.dart';

part 'build_google_map.dart';
part 'build_save_button.dart';
