part of 'contact_us_widgets_imports.dart';

class BuildBranchItem extends StatelessWidget {
  final DeviceModel device;
  final String branchName;
  final BrancheModel brancheModel;
  final ContactUsData contactUsData;

  const BuildBranchItem(
      {Key? key,
      required this.device,
      required this.branchName,
      required this.brancheModel,
      required this.contactUsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 16),
          child: Row(
            children: [
              Expanded(
                child: MyText(
                  title: branchName,
                  color: MyColors.black,
                  fontWeight: FontWeight.bold,
                  size: device.isTablet ? 8.5.sp : 13.sp,
                ),
              ),
              InkWell(
                onTap: () {
                  brancheModel.showMore = !brancheModel.showMore;
                  contactUsData.contactCubit.onUpdateData(
                    contactUsData.contactCubit.state.data,
                  );
                },
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                  decoration: BoxDecoration(
                      color: MyColors.white,
                      borderRadius: BorderRadius.circular(20.r)),
                  child: MyText(
                    title: brancheModel.showMore
                        ? "- ${tr("less")}"
                        : "+ ${tr("more")}",
                    color: MyColors.black,
                    size: device.isTablet ? 5.5.sp : 9.sp,
                  ),
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: brancheModel.showMore,
          child: CustomCard(
            child: Column(
              children: [
                ListView.separated(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: brancheModel.addresses.length,
                  itemBuilder: (cxt, addressIndex) {
                    return BuildSmallItem(
                      onTap: () =>
                          getIt<Utilities>().navigateToMapWithDirection(
                        lat: brancheModel.addresses[addressIndex].latitude,
                        lng: brancheModel.addresses[addressIndex].longitude,
                        title: brancheModel.addresses[addressIndex].nameEnglish,
                        context: context,
                      ),
                      device: device,
                      showArrow: false,
                      title:
                          brancheModel.addresses[addressIndex].getAddressName(),
                      icon: Container(
                        padding: EdgeInsets.all(6.sp),
                        margin: EdgeInsetsDirectional.only(end: 8.w),
                        decoration: BoxDecoration(
                            color: MyColors.primary.withOpacity(.05),
                            shape: BoxShape.circle),
                        child: SvgPicture.asset(
                          Res.location,
                          color: MyColors.primary,
                          width: 14.r,
                          height: 14.r,
                        ),
                      ),
                      backGround: MyColors.white,
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider();
                  },
                ),
                Divider(),
                ListView.separated(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (cxt, mobileIndex) {
                    return BuildSmallItem(
                        onTap: () => getIt<Utilities>().callPhone(
                            phone: brancheModel.mobileNo[mobileIndex]),
                        title: " ${brancheModel.mobileNo[mobileIndex]}",
                        device: device);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider();
                  },
                  itemCount: brancheModel.mobileNo.length,
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
