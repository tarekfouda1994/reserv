import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/widgets/custom_card.dart';
import 'package:flutter_tdd/features/profile/data/model/branche_model/branche_model.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_model/contact_us_model.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/contact_us/contact_us_imports.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../res.dart';

part 'build_branch_item.dart';
part 'build_contact_header.dart';
part 'build_small_item.dart';
