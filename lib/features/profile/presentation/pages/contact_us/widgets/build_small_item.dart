part of 'contact_us_widgets_imports.dart';

class BuildSmallItem extends StatelessWidget {
  final DeviceModel device;
  final String title;
  final Widget? icon;
  final Color? backGround;
  final double? height;
  final double? width;
  final bool? showArrow;
  final EdgeInsetsGeometry? margin;
  final void Function()? onTap;

  const BuildSmallItem(
      {Key? key,
      required this.device,
      required this.title,
      this.icon,
      this.backGround,
      this.height,
      this.width,
      this.margin,
      this.showArrow = true,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        margin: margin ?? EdgeInsets.symmetric(vertical: 6),
        child: Row(
          children: [
            icon ??
                SvgPicture.asset(
                  Res.phone,
                  width: device.isTablet ? 19.sp : null,
                ),
            Expanded(
              child: _buildTitleDirectionality(context),
            ),
            if (showArrow == true)
              Icon(Icons.arrow_forward_ios_outlined,
                  color: MyColors.primary,
                  size: device.isTablet ? 10.sp : 13.sp)
          ],
        ),
      ),
    );
  }

  Widget _buildTitleDirectionality(BuildContext context) {
    if (icon != null) {
      return MyText(
        title: title,
        color: MyColors.black,
        size: device.isTablet ? 7.sp : 11.sp,
      );
    }
    return Align(
      alignment: AlignmentDirectional.centerStart,
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: MyText(
          title: getIt<Utilities>().convertNumToAr(context: context, value: title),
          color: MyColors.black,
          size: device.isTablet ? 7.sp : 11.sp,
        ),
      ),
    );
  }
}
