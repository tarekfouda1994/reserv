part of 'contact_us_widgets_imports.dart';

class BuildContactHeader extends StatelessWidget {
  final DeviceModel device;
  final ContactUsModel contactUsModel;

  const BuildContactHeader(
      {Key? key, required this.device, required this.contactUsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCard(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 6),
            // padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(6.r),
                  margin: EdgeInsetsDirectional.only(end: 6.r),
                  decoration: BoxDecoration(
                    color: MyColors.primary.withOpacity(.04),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.access_time_rounded,
                    color: MyColors.primary,
                    size: device.isTablet ? 12.sp : 18.sp,
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                          title: tr("official_timing"),
                          fontFamily: CustomFonts.primarySemiBoldFont,
                          color: MyColors.black,
                          size: device.isTablet ? 7.sp : 11.sp),
                      MyText(
                        fontWeight: FontWeight.w100,
                        title: getIt<Utilities>().convertNumToAr(
                          context: context,
                          value: contactUsModel.getOfficeTimingName(),
                        ),
                        color: MyColors.black,
                        size: device.isTablet ? 7.sp : 11.sp,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Divider(),
          BuildSmallItem(
            onTap: () =>
                getIt<Utilities>().callPhone(phone: contactUsModel.phoneNo),
            title: " ${contactUsModel.phoneNo} ",
            device: device,
          ),
          Divider(),
          BuildSmallItem(
            icon: Container(
              padding: EdgeInsets.all(6.sp),
              decoration: BoxDecoration(
                  color: MyColors.primary.withOpacity(.04),
                  shape: BoxShape.circle),
              child: SvgPicture.asset(
                Res.globe,
                color: MyColors.primary,
                width: device.isTablet ? 15.r : 18.r,
                height: device.isTablet ? 15.r : 18.r,
              ),
            ),
            onTap: () => getIt<Utilities>().sendMail(contactUsModel.webSite),
            title: " ${contactUsModel.webSite}",
            device: device,
          ),
        ],
      ),
    );
  }
}
