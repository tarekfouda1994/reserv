part of 'contact_us_imports.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  State<ContactUs> createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  ContactUsData contactUsData = ContactUsData();

  @override
  void initState() {
    contactUsData.getContactData(refresh: false);
    contactUsData.getContactData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return GradientScaffold(
      appBar: DefaultAppBar(title: tr("contact_us")),
      body: BlocBuilder<GenericBloc<ContactUsModel?>,
              GenericState<ContactUsModel?>>(
          bloc: contactUsData.contactCubit,
          builder: (_, state) {
            if (state is GenericUpdateState) {
              return ListView(
                padding: EdgeInsets.symmetric(vertical: 10),
                children: [
                  BuildContactHeader(
                    device: device,
                    contactUsModel: state.data!,
                  ),
                  ...List.generate(
                    state.data!.branches.length,
                    (index) {
                      return BuildBranchItem(
                        contactUsData: contactUsData,
                        device: device,
                        branchName: state.data!.branches[index].getBranchName(),
                        brancheModel: state.data!.branches[index],
                      );
                    },
                  )
                ],
              );
            } else {
              return ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                itemCount: 10,
                itemBuilder: (cxt, index) {
                  return BuildShimmerView(height: 60.h);
                },
              );
            }
          }),
      // bottomSheet: BuildContactBottom(device: device),
    );
  }
}
