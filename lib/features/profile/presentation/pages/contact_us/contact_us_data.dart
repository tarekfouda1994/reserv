part of 'contact_us_imports.dart';

class ContactUsData {
  GenericBloc<ContactUsModel?> contactCubit = GenericBloc(null);

  getContactData({bool refresh = true}) async {
    var data = await GetContactUsData()(refresh);
    if (data != null) {
      data.branches.map((e) => e.addresses.toSet().toList());
      contactCubit.onUpdateData(data);
    }
  }
}
