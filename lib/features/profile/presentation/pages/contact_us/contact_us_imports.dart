import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/core/widgets/gradient_scaffold.dart';
import 'package:flutter_tdd/features/profile/data/model/contact_us_model/contact_us_model.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/get_contact_us_data.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/contact_us/widgets/contact_us_widgets_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'contact_us.dart';
part 'contact_us_data.dart';
