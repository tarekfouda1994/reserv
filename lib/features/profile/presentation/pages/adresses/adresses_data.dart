part of "adresses_imports.dart";

class AddressesData {
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  TextEditingController addressType = TextEditingController();
  TextEditingController location = TextEditingController();
  TextEditingController editLocation = TextEditingController();
  TextEditingController editAddressType = TextEditingController();
  final GlobalKey<FormState> updateFormKey = GlobalKey();
  final GenericBloc<List<AddressModel>> addressCubit = GenericBloc([]);
  final GenericBloc<bool> setAddressCubit = GenericBloc(false);
  final GenericBloc<LocationEntity?> locationCubit = GenericBloc(null);
  LocationEntity? locationModel;

  getAllAddresses({bool refresh = true}) async {
    var data = await GetAllAddresses()(refresh);
    addressCubit.onUpdateData(data);
    if (addressCubit.state.data.isEmpty) setAddressCubit.onUpdateData(true);
  }

  updateCard(BuildContext context, AddressModel model) async {
    if (updateFormKey.currentState!.validate()) {
      getIt<LoadingHelper>().showLoadingDialog();
      AddressPrams prams = _addressParams(model);
      var result = await UpdateAddressCard()(prams);
      getIt<LoadingHelper>().dismissDialog();
      if (result) {
        getAllAddresses();
        AutoRouter.of(context).pop().then((value) {
          CustomToast.showSimpleToast(
            msg: tr("success_edit"),
            type: ToastType.success,
            title: tr("success_toast"),
          );
        });
      }
    }
  }

  AddressPrams _addressParams(AddressModel model) {
    AddressPrams prams = AddressPrams(
      id: model.id,
      addressType: editAddressType.text,
      locationDetail: locationCubit.state.data?.address ?? model.locationDetail,
      latitude: locationCubit.state.data?.lat ?? model.latitude,
      longitude: locationCubit.state.data?.lng ?? model.longitude,
      isDefault: setAddressCubit.state.data,
    );
    return prams;
  }

  deleteCard(BuildContext context, AddressModel model) async {
    getIt<LoadingHelper>().showLoadingDialog();
    var result = await DeleteAddress()(model.id);
    getIt<LoadingHelper>().dismissDialog();
    if (result) {
      AutoRouter.of(context).pop();
      addressCubit.state.data.remove(model);
      addressCubit.onUpdateData(addressCubit.state.data);
      Future.delayed(Duration(milliseconds: 500), () {
        CustomToast.showSimpleToast(
          msg: tr("success_delete_address"),
          type: ToastType.success,
          title: tr("success_toast"),
        );
      });
    }
  }

  setCardDefault(AddressModel model) async {
    if (model.isDefault == false) {
      getIt<LoadingHelper>().showLoadingDialog();
      var result = await SetAddressDefault()(model.id);
      getIt<LoadingHelper>().dismissDialog();
      _changeCardState(result, model);
    }
  }

  void _changeCardState(bool result, AddressModel model) {
    if (result) {
      var data = addressCubit.state.data.map((e) {
        if (e == model) {
          e.isDefault = true;
        } else {
          e.isDefault = false;
        }
        return e;
      }).toList();
      addressCubit.onUpdateData(data);
      CustomToast.showSimpleToast(
          msg: tr("success_edit"),
          type: ToastType.success,
          title: tr("success_toast"));
    }
  }

  //
  // currentLocation(BuildContext context) async {
  //   var laLong = await getIt<Utilities>().getCurrentLocation(context);
  //   LatLng loc = LatLng(laLong?.latitude ?? 0, laLong?.longitude ?? 0);
  //   String address = await getIt<Utilities>().getAddress(loc, context);
  //   location.text = address;
  //   locationCubit.onUpdateData(LocationEntity(
  //       address: address,
  //       lng: laLong?.latitude ?? 0,
  //       lat: laLong?.latitude ?? 0));
  // }

  enterLocation(BuildContext context) async {
    var laLong = await getIt<Utilities>().getCurrentLocation(context);
    if (laLong != null) {
      var result = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (cxt) => SelectLocationAddress(loc: laLong)));
      locationCubit.onUpdateData(result);
      location.text = locationCubit.state.data?.address ?? "";
      editLocation.text = locationCubit.state.data?.address ?? "";
    }
  }

  bottomSheetUpdateAddress(BuildContext context, DeviceModel model,
      AddressesData addressesData, AddressModel addressModel) {
    editAddressType.text = addressModel.addressType;
    editLocation.text = addressModel.locationDetail.split("/").join(",");
    setAddressCubit.onUpdateData(addressModel.isDefault);
    Widget widget = BuildUpdateAddressBottomSheet(
        addressesData: addressesData,
        deviceModel: model,
        addressModel: addressModel);
    _showBottomSheet(context, widget);
  }

  bottomSheetDeleteCard(BuildContext context, DeviceModel deviceModel,
      AddressesData addressesData, AddressModel addressModel) {
    Widget widget = BuildDeleteAddressesBottomSheet(
        deviceModel: deviceModel,
        addressesData: addressesData,
        model: addressModel);
    _showBottomSheet(context, widget);
  }

  _showBottomSheet(BuildContext context, Widget widget) {
    showModalBottomSheet(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15.r),
          ),
        ),
        context: context,
        isScrollControlled: true,
        builder: (cxt) {
          return widget;
        });
  }

  void goToAddLoc(BuildContext context) async {
    var laLong = await getIt<Utilities>().getCurrentLocation(context);
    if (laLong != null) {
      locationModel = LocationEntity(
        lat: laLong.latitude ?? 0,
        lng: laLong.longitude ?? 0,
      );
      await AutoRouter.of(context).push(
        AddAddressRoute(locationEntity: locationModel!),
      );
    }
    getAllAddresses();
  }
}
