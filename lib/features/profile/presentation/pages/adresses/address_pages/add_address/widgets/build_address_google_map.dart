part of 'add_address_widgets_imports.dart';

class BuildAddressGoogleMap extends StatelessWidget {
  final AddAddressController addAddressController;

  const BuildAddressGoogleMap({Key? key, required this.addAddressController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<LocationEntity>, GenericState<LocationEntity>>(
      bloc: addAddressController.locationCubit,
      builder: (context, state) {
        CameraPosition _initialLoc =
            CameraPosition(target: LatLng(state.data.lat, state.data.lng), zoom: 14);
        return Stack(
          alignment: Alignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 16, horizontal: 20),
              width: MediaQuery.of(context).size.width,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(7.r),
                child: GoogleMap(
                    mapType: MapType.normal,
                    // markers: _markers,
                    initialCameraPosition: _initialLoc,
                    onMapCreated: (GoogleMapController controller) {
                      controller.setMapStyle(json.encode(MapStyle.server));
                      addAddressController.controller.complete(controller);
                      addAddressController.controllerMap = controller;
                      addAddressController.moveCamera(context);
                    },
                    myLocationButtonEnabled: true,
                    myLocationEnabled: true,
                    rotateGesturesEnabled: true,
                    scrollGesturesEnabled: true,
                    trafficEnabled: true,
                    zoomControlsEnabled: false,
                    tiltGesturesEnabled: true,
                    compassEnabled: true,
                    indoorViewEnabled: true,
                    buildingsEnabled: true,
                    mapToolbarEnabled: true,
                    zoomGesturesEnabled: true,
                    onCameraIdle: () {
                      addAddressController.getLocationAddress(context);
                    },
                    onTap: (location) {
                      addAddressController.getLocationAddress(context);
                    },
                    onCameraMove: (loc) {
                      addAddressController.locationCubit.onUpdateData(LocationEntity(
                        lat: loc.target.latitude,
                        lng: loc.target.longitude,
                        address: "",
                      ));
                    }),
              ),
            ),
            SvgPicture.asset(
              Res.map_marker_active_svg,
              width: 45,
              height: 45,
            ),
            Positioned(
              bottom: 20,
              left: 3,
              right: 3,
              child: BuildSearchInput(
                addAddressController: addAddressController,
              ),
            ),
          ],
        );
      },
    );
  }
}
