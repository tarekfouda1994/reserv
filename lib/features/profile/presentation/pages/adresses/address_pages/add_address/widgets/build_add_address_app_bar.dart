part of'add_address_widgets_imports.dart';

class BuildAddAddressAppBar extends StatelessWidget implements PreferredSizeWidget{
  const BuildAddAddressAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return DefaultAppBar(
        // titleSize: device.isTablet ? 7.sp : 11.sp,
        showBack: false,
        title: tr("addNewAddress"),
        actions: [
          InkWell(
              onTap: () => AutoRouter.of(context).pop(),
              child: Container(
                  margin: const EdgeInsetsDirectional.only(end: 14),
                  padding: EdgeInsets.all(device.isTablet ? 3.h : 6.r),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: MyColors.black),
                  child: Icon(Icons.clear,
                      size: device.isTablet ? 14.sp : 17.sp,
                      color: MyColors.white)))
        ]);
  }

  @override
  Size get preferredSize => Size.fromHeight(40.h);
}
