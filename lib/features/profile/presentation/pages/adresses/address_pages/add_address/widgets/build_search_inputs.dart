part of 'add_address_widgets_imports.dart';

class BuildSearchInput extends StatelessWidget {
  final AddAddressController addAddressController;

  const BuildSearchInput({Key? key, required this.addAddressController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    bool isTablet = deviceModel.isTablet;
    return InkWell(
      onTap: () async {
        Prediction? p = await PlacesAutocomplete.show(
          types: [],
          language: deviceModel.locale.languageCode,
          strictbounds: false,
          components: [],
          context: context,
          apiKey: CustomFonts.googleMapKey,
          mode: Mode.fullscreen,
          logo: Container(),
          onError: (value) {
            return;
          },
        );
        addAddressController.displayPrediction(context, p);
      },
      child: Container(
        height: isTablet ? 70.sm : 50.sm,
        margin: const EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xffefefef)),
          borderRadius: BorderRadius.circular(10.r),
          color: MyColors.white,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Container(
          width: MediaQuery.of(context).size.width * .92,
          child: Row(
            children: [
              Icon(
                Icons.search,
                size: 26,
                color: MyColors.primary,
              ),
              SizedBox(width: 10),
              MyText(
                title: tr("searchMap"),
                color: MyColors.blackOpacity,
                size: 12,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
