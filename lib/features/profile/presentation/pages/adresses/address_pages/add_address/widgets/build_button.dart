part of 'add_address_widgets_imports.dart';

class BuildAddAddressButton extends StatelessWidget {
  final AddAddressController addAddressController;

  const BuildAddAddressButton({Key? key, required this.addAddressController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: 16,vertical: 20.sm),
      child: Row(
        children: [
          Expanded(
            child: DefaultButton(
              title: tr("cancel"),
              onTap: () => AutoRouter.of(context).pop(),
              color: MyColors.black.withOpacity(.05),
              textColor: MyColors.blackOpacity,
              borderColor: MyColors.white,
              borderRadius:BorderRadius.circular(30.r),
              margin: EdgeInsets.zero,
              fontSize: device.isTablet ? 7.sp : 11.sp,

              height:device.isTablet?60.sm: 50.sm,
            ),
          ),
          SizedBox(width: 8),
          Expanded(
            child: DefaultButton(
              title: tr("save_address"),
              onTap: () => addAddressController.addCard(context),
              color: MyColors.primary,
              textColor: MyColors.white,
              borderColor: MyColors.white,
              borderRadius:BorderRadius.circular(30.r),
              margin: EdgeInsets.zero,
              fontSize: device.isTablet ? 7.sp : 11.sp,

              height:device.isTablet?60.sm: 50.sm,
            ),
          ),
        ],
      ),
    );
  }
}
