part of 'add_address_imports.dart';

class AddAddress extends StatefulWidget {
  final LocationEntity locationEntity;
  const AddAddress({Key? key, required this.locationEntity}) : super(key: key);

  @override
  State<AddAddress> createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  AddAddressController addAddressController = AddAddressController();

  @override
  void initState() {
    addAddressController.locationCubit.onUpdateData(widget.locationEntity);
    addAddressController.getLocationAddress(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: MyColors.white,
        key: addAddressController._scaffold,
        appBar: BuildAddAddressAppBar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height - (350 + 40.h),
                child: BuildAddressGoogleMap(addAddressController: addAddressController),
              ),
              BuildAddAddressInputs(addAddressController: addAddressController),
            ],
          ),
        ),
        bottomNavigationBar: BuildAddAddressButton(
          addAddressController: addAddressController,
        ),
      ),
    );
  }
}
