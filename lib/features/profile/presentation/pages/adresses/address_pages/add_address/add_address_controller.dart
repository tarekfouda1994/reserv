part of 'add_address_imports.dart';

class AddAddressController {
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  TextEditingController addressType = TextEditingController();
  TextEditingController location = TextEditingController();
  TextEditingController editLocation = TextEditingController();
  TextEditingController editAddressType = TextEditingController();
  final GlobalKey<FormState> addFormKey = GlobalKey();
  final GlobalKey<FormState> updateFormKey = GlobalKey();
  final GenericBloc<List<AddressModel>> addressCubit = GenericBloc([]);
  final GenericBloc<bool> setAddressCubit = GenericBloc(false);
  final GenericBloc<LocationEntity> locationCubit =
      GenericBloc(LocationEntity(lat: 0, lng: 0));
  final Completer<GoogleMapController> controller = Completer();

  GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: CustomFonts.googleMapKey);

  double? zoom;
  late PageController pageController;
  late GoogleMapController controllerMap;

  void getLocationAddress(BuildContext context) async {
    LatLng loc =
        LatLng(locationCubit.state.data.lat, locationCubit.state.data.lng);
    String address = await getIt<Utilities>().getAddress(loc, context);
    locationCubit.state.data.address =
        address.split("  ").join(" / ").toString().replaceFirst("/", "");
    location.text = locationCubit.state.data.address;
    locationCubit.onUpdateData(locationCubit.state.data);
  }

  Future<void> displayPrediction(BuildContext context, Prediction? p) async {
    if (p != null) {
      var deviceModel = context.read<DeviceCubit>().state.model;
      PlacesDetailsResponse detail = await places.getDetailsByPlaceId(
          p.placeId!,
          language: deviceModel.locale.languageCode);
      locationCubit.onUpdateData(LocationEntity(
          lat: detail.result.geometry!.location.lat,
          address: detail.result.formattedAddress ?? "",
          lng: detail.result.geometry!.location.lng));
      location.text = detail.result.formattedAddress ?? "";
      moveCamera(context);
      zoom = 14;
    }
  }

  moveCamera(BuildContext context) {
    controllerMap.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
            target: LatLng(
                locationCubit.state.data.lat, locationCubit.state.data.lng),
            zoom: 14,
            bearing: 30.0,
            tilt: 45.0),
      ),
    );
  }

  addCard(BuildContext context) async {
    if (addFormKey.currentState!.validate()) {
      getIt<LoadingHelper>().showLoadingDialog();
      if (_validLatLng()) {
        CustomToast.showSimpleToast(
          title: tr("Warning_Toast"),
          msg: tr(
            "alert_select_location",
          ),
          type: ToastType.warning,
        );
        return;
      }
      AddressPrams prams = _addressPrams();
      await UpdateAddressCard()(prams).then((result) {
        if (result) {
          _onSuccessAdd(context);
        }
      });
    }
  }

  bool _validLatLng() {
    return locationCubit.state.data.lat == 0 &&
        locationCubit.state.data.lng == 0;
  }

  void _onSuccessAdd(BuildContext context) {
    addressType.clear();
    location.clear();
    setAddressCubit.onUpdateData(false);
    getIt<LoadingHelper>().dismissDialog();
    AutoRouter.of(context).pop().then((value) {
      CustomToast.showSimpleToast(
        title: tr("success_toast"),
        msg: tr("save_successfully"),
        type: ToastType.success,
      );
    });
  }

  AddressPrams _addressPrams() {
    return AddressPrams(
        addressType: addressType.text,
        locationDetail: location.text,
        latitude: locationCubit.state.data.lat,
        longitude: locationCubit.state.data.lng,
        isDefault: setAddressCubit.state.data);
  }

  void changeLocation(BuildContext context) async {
    locationCubit.onUpdateData(locationCubit.state.data);
    Navigator.pop(context, locationCubit.state.data);
  }
}
