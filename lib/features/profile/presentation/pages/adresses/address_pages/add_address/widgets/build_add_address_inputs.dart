part of'add_address_widgets_imports.dart';

class BuildAddAddressInputs extends StatelessWidget {
 final AddAddressController addAddressController;
  const BuildAddAddressInputs({Key? key, required this.addAddressController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return   BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: addAddressController.setAddressCubit,
        builder: (cxt, state) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: addAddressController.addFormKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomTextField(
                      fillColor: Color(0xffF6F9F9),
                        controller: addAddressController.addressType,
                        margin:  EdgeInsets.only(top: 10,bottom: 15.h),
                        fieldTypes: FieldTypes.normal,
                        label: tr("address_type"),
                        hint: "${tr("ex")} ${tr("office")}",
                        type: TextInputType.name,
                        action: TextInputAction.done,
                        validate: (value) => value?.validateEmpty(),
                    ),
                    CustomTextField(
                      fillColor: Color(0xffF6F9F9),
                      label: tr("location"),
                      controller: addAddressController.location,
                      margin:  EdgeInsets.only(top: 10,bottom: 15.h),
                      fieldTypes: FieldTypes.disable,
                      hint: "${tr("ex")} ${tr("address_ex")}...",
                      type: TextInputType.none,
                      action: TextInputAction.none,
                      validate: (value) {},
                      suffixIcon: BuildSuffixIcon(asset: Res.marker_input,scale: .4),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: InkWell(
                        onTap: () => addAddressController.setAddressCubit
                            .onUpdateData(!state.data),
                        borderRadius: BorderRadius.circular(100),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                padding: EdgeInsets.all(2.h),
                                margin: EdgeInsets.symmetric(
                                    horizontal:
                                    device.isTablet ? 5.sp : 8),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: MyColors.primary.withOpacity(.3),
                                        width: .5),
                                    color: state.data
                                        ? MyColors.primary
                                        : MyColors.white),
                                child: Icon(Icons.done,
                                    color: MyColors.white,
                                    size:
                                    device.isTablet ? 10.sp : 14.sp)),
                            MyText(
                                title: tr("set_address"),
                                color: MyColors.black.withOpacity(.5),
                                size: device.isTablet ? 8.sp : 12.sp),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
