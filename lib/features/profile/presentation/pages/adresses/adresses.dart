part of "adresses_imports.dart";

class Addresses extends StatefulWidget {
  const Addresses({Key? key}) : super(key: key);

  @override
  State<Addresses> createState() => _AddressesState();
}

class _AddressesState extends State<Addresses> {
  AddressesData addressesData = AddressesData();

  @override
  void initState() {
    addressesData.getAllAddresses(refresh: false);
    addressesData.getAllAddresses(refresh: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      appBar: DefaultAppBar(title: tr("addresses"), elevation: .3),
      body: BlocBuilder<GenericBloc<List<AddressModel>>,
              GenericState<List<AddressModel>>>(
          bloc: addressesData.addressCubit,
          builder: (cxt, state) {
            if (state is GenericUpdateState) {
              return Column(
                children: [
                  Flexible(
                    child: ListView(padding: EdgeInsets.all(16.r), children: [
                      BuildEmptyAddress(
                          addressesData: addressesData,
                          device: device,
                          visible: state.data.isEmpty),
                      ...List.generate(state.data.length, (index) {
                        return BuildAddressesItem(
                          device: device,
                          typeCard: AddressesCardType.screen,
                          addressesData: addressesData,
                          addressModel: state.data[index],
                        );
                      }),
                    ]),
                  ),
                  BuildAddAddressesButton(
                    deviceModel: device,
                    addressesData: addressesData,
                  ),
                ],
              );
            } else {
              return ListView(padding: EdgeInsets.all(16.r), children: [
                ...List.generate(5, (index) {
                  return BuildShimmerView(height: 100.h);
                }),
              ]);
            }
          }),
    );
  }
}
