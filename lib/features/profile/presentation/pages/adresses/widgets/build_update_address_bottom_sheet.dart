part of 'adresses_widgets_imports.dart';

class BuildUpdateAddressBottomSheet extends StatelessWidget {
  final AddressesData addressesData;
  final DeviceModel deviceModel;
  final AddressModel addressModel;

  const BuildUpdateAddressBottomSheet(
      {Key? key,
      required this.addressesData,
      required this.deviceModel,
      required this.addressModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: addressesData.setAddressCubit,
        builder: (cxt, state) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20.sm,horizontal: 16),
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(12.r))),
              child: Form(
                key: addressesData.updateFormKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BuildHeaderBottomSheet(
                      margin: EdgeInsets.only(bottom: 15.h),
                      title: tr("edit_address"),
                    ),
                    SizedBox(height: 8),
                    CustomTextField(
                        fillColor: Color(0xffF6F9F9),
                        controller: addressesData.editAddressType,
                        margin: EdgeInsets.only(top: 10, bottom: 15.h),
                        fieldTypes: FieldTypes.normal,
                        label: tr("address_type"),
                        hint: "${tr("ex")} ${tr("office")}",
                        type: TextInputType.name,
                        action: TextInputAction.next,
                        suffixIcon: NotesIcon(),
                        validate: (value) => value?.validateEmpty()),
                    CustomTextField(
                      fillColor: Color(0xffF6F9F9),
                      label: tr("location"),
                      onTab: () => addressesData.enterLocation(context),
                      controller: addressesData.editLocation,
                      margin: EdgeInsets.only(top: 10, bottom: 15.h),
                      fieldTypes: FieldTypes.clickable,
                      hint: "${tr("ex")} ${tr("address_ex")}...",
                      type: TextInputType.name,
                      action: TextInputAction.next,
                      validate: (value) => addressesData.editLocation.text.validateEmpty(),
                      suffixIcon: MarkerIcon(),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: InkWell(
                        onTap: () => addressesData.setAddressCubit
                            .onUpdateData(!state.data),
                        borderRadius: BorderRadius.circular(100),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                padding: EdgeInsets.all(2.h),
                                margin: EdgeInsets.symmetric(
                                    horizontal:
                                        deviceModel.isTablet ? 5.sp : 8),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: MyColors.primary.withOpacity(.3),
                                        width: .5),
                                    color: state.data
                                        ? MyColors.primary
                                        : MyColors.white),
                                child: Icon(Icons.done,
                                    color: MyColors.white,
                                    size:
                                        deviceModel.isTablet ? 10.sp : 14.sp)),
                            MyText(
                                title: tr("set_address"),
                                color: MyColors.black.withOpacity(.5),
                                size: deviceModel.isTablet ? 8.sp : 12.sp),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Expanded(
                          child: DefaultButton(
                            onTap: () => AutoRouter.of(context).pop(),
                            title: tr("cancel"),
                            color: MyColors.black.withOpacity(.05),
                            textColor: MyColors.blackOpacity,
                            borderColor: MyColors.white,
                            borderRadius: BorderRadius.circular(30.r),
                            margin: EdgeInsets.zero,
                            fontSize: deviceModel.isTablet ? 7.sp : 11.sp,

                            height:deviceModel.isTablet?60.sm: 50.sm,
                          ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: DefaultButton(
                            onTap: () =>
                                addressesData.updateCard(context, addressModel),
                            title: tr("save_address"),
                            color: MyColors.primary,
                            textColor: MyColors.white,
                            borderColor: MyColors.white,
                            borderRadius: BorderRadius.circular(30.r),
                            margin: EdgeInsets.zero,
                            fontSize: deviceModel.isTablet ? 7.sp : 11.sp,

                            height:deviceModel.isTablet?60.sm: 50.sm,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
