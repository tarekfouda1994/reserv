part of "adresses_widgets_imports.dart";

class BuildAddAddressesButton extends StatelessWidget {
  final DeviceModel deviceModel;
  final AddressesData addressesData;
  final Color? bgColors;
  final Color? textColor;
  final MainAxisAlignment? mainAxisAlignment;

  const BuildAddAddressesButton(
      {Key? key,
      required this.deviceModel,
      required this.addressesData,
      this.bgColors,
      this.textColor,
      this.mainAxisAlignment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return DefaultButton(
      title: '+ ${tr("add_new_address")}',
      onTap: () =>addressesData.goToAddLoc(context),
      color: MyColors.primary,
      textColor: MyColors.white,
      borderRadius:BorderRadius.circular(30.r),
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 20.sm),
      height: device.isTablet?60.sm: 50.sm,
      fontSize: deviceModel.isTablet ? 7.sp : 11.sp,

    );
  }
}
