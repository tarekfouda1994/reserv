part of 'adresses_widgets_imports.dart';

class BuildEmptyAddress extends StatelessWidget {
  final bool visible;
  final DeviceModel device;
  final AddressesData addressesData;

  const BuildEmptyAddress(
      {Key? key,
      required this.visible,
      required this.device,
      required this.addressesData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: Padding(
        padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.height * .19),
        child: Column(
          children: [
            SvgPicture.asset(Res.emptyAdresses),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: MyText(
                title: tr("No_Address_Yet"),
                color: MyColors.black,
                size: device.isTablet ? 8.sp : 12.sp,
              ),
            ),
            MyText(
              title: tr("address_toast"),
              color: MyColors.grey,
              size: device.isTablet ? 7.sp : 11.sp,
            ),
          ],
        ),
      ),
    );
  }
}
