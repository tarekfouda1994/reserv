part of "adresses_widgets_imports.dart";

class BuildDeleteAddressesBottomSheet extends StatelessWidget {
  final AddressesData addressesData;
  final DeviceModel deviceModel;
  final AddressModel model;

  const BuildDeleteAddressesBottomSheet(
      {Key? key,
      required this.addressesData,
      required this.deviceModel,
      required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.all(15.h),
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15.r),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              margin: const EdgeInsetsDirectional.only(bottom: 15),
              ladingIcon: SvgPicture.asset(Res.trash),
              title: " " + tr("delete_address"),
              titleColor: Color(0xffED3A57),
            ),
            SizedBox(height: 7),
            MyText(
              title: tr("alert_delete"),
              color: Colors.black,
              size: deviceModel.isTablet ? 7.sp : 10.sp,
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.r),
                color: MyColors.primary.withOpacity(.05),
              ),
              child: ListTile(
                title: MyText(
                    title: model.addressType,
                    color: Colors.grey.withOpacity(.9),
                    size: deviceModel.isTablet ? 6.sp : 9.sp,
                    fontWeight: FontWeight.bold),
                leading: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: MyColors.white,
                        borderRadius: BorderRadius.circular(6.r)),
                    child: SvgPicture.asset(Res.location,
                        height: 20, width: 20, color: MyColors.black)),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 3, bottom: 5),
                  child: MyText(
                      title: model.locationDetail,
                      color: Colors.black,
                      size: deviceModel.isTablet ? 6.sp : 9.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () => AutoRouter.of(context).pop(),
                    child: Container(
                      alignment: Alignment.center,
                      height: deviceModel.isTablet ? 60.sm : 50.sm,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.r),
                        color: MyColors.black,
                      ),
                      child: MyText(
                          alien: TextAlign.center,
                          size: deviceModel.isTablet ? 7.sp : 11.sp,
                          title: tr("keep"),
                          color: MyColors.white),
                    ),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: InkWell(
                    onTap: () => addressesData.deleteCard(context, model),
                    child: Container(
                      alignment: Alignment.center,
                      height: deviceModel.isTablet ? 60.sm : 50.sm,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.r),
                        border: Border.all(color: MyColors.errorColor),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MyText(
                              alien: TextAlign.center,
                              size: deviceModel.isTablet ? 7.sp : 11.sp,
                              title: tr("delete"),
                              color: MyColors.errorColor),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
