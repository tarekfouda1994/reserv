import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/widgets/build_header_bottom_sheet.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/marker_icon.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/notes_icon.dart';
import 'package:flutter_tdd/features/profile/data/model/address_model/address_model.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../res.dart';
import '../adresses_imports.dart';

part 'build_add_adresses_button.dart';
part 'build_adresses_item.dart';
part 'build_delete_addresses_bottom_sheet.dart';
part 'build_empty_address.dart';
part 'build_update_address_bottom_sheet.dart';
