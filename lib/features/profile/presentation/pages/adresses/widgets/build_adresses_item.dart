part of "adresses_widgets_imports.dart";

enum AddressesCardType { alert, screen }

class BuildAddressesItem extends StatelessWidget {
  final AddressesCardType typeCard;
  final DeviceModel device;
  final AddressesData addressesData;
  final AddressModel addressModel;

  const BuildAddressesItem({
    Key? key,
    required this.device,
    required this.typeCard,
    required this.addressesData,
    required this.addressModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: device.isTablet ? 4.sp : 7),
      decoration: BoxDecoration(
          color: addressModel.isDefault ? MyColors.primary : null,
          borderRadius: BorderRadius.circular(15.r),
          border: Border.all(color: MyColors.grey.withOpacity(.15))),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        InkWell(
          onTap: () => addressesData.setCardDefault(addressModel),
          child: Container(
            padding: EdgeInsets.all(device.isTablet ? 10.sp : 16),
            decoration: BoxDecoration(
              color: addressModel.isDefault
                  ? MyColors.primary
                  : MyColors.primary.withOpacity(.05),
              borderRadius: BorderRadius.vertical(top: Radius.circular(14.r)),
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                      title: addressModel.addressType,
                      color: addressModel.isDefault
                          ? MyColors.white
                          : MyColors.grey,
                      size: device.isTablet ? 7.sp : 11.sp),
                  Row(
                    children: [
                      Visibility(
                        visible: typeCard == AddressesCardType.screen,
                        child: Container(
                            padding: EdgeInsets.all(2.h),
                            margin: EdgeInsets.symmetric(
                                horizontal: device.isTablet ? 5.sp : 8),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: MyColors.primaryLight, width: .5),
                                color: addressModel.isDefault
                                    ? MyColors.primary
                                    : MyColors.white),
                            child: Icon(Icons.done,
                                color: MyColors.white,
                                size: device.isTablet ? 10.sp : 14.sp)),
                      ),
                      MyText(
                          title: addressModel.isDefault
                              ? tr("default")
                              : tr("set_default"),
                          color: addressModel.isDefault
                              ? MyColors.white
                              : MyColors.grey,
                          size: device.isTablet ? 8.sp : 11.sp),
                    ],
                  ),
                ]),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: device.isTablet ? 10.sp : 16,
              vertical: device.isTablet ? 8.sp : 6),
          child: MyText(
              title: addressModel.locationDetail,
              color: addressModel.isDefault ? MyColors.white : MyColors.black,
              size: device.isTablet ? 8.sp : 11.sp),
        ),
        Visibility(
          visible: typeCard == AddressesCardType.screen,
          child: Padding(
            padding:
                EdgeInsets.symmetric(vertical: device.isTablet ? 7.sp : 9.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () => addressesData.bottomSheetUpdateAddress(
                      context, device, addressesData, addressModel),
                  child: SvgPicture.asset(Res.edit,
                      color: addressModel.isDefault
                          ? MyColors.white
                          : MyColors.black,
                      height: device.isTablet ? 13.w : 19.w,
                      width: device.isTablet ? 13.w : 19.w),
                ),
                SizedBox(width: 8),
                InkWell(
                  onTap: () => addressesData.bottomSheetDeleteCard(
                    context,
                    device,
                    addressesData,
                    addressModel,
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: device.isTablet ? 10.sp : 16.w,
                    ),
                    child: SvgPicture.asset(
                      Res.trash,
                      color: addressModel.isDefault
                          ? MyColors.white
                          : MyColors.black,
                      height: device.isTablet ? 13.w : 19.w,
                      width: device.isTablet ? 13.w : 19.w,
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ]),
    );
  }
}
