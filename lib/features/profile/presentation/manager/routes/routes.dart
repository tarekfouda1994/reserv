import 'package:auto_route/auto_route.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/adresses/address_pages/add_address/add_address_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/change_password/change_password_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/edit_profile/edit_profile_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/payment/payment_imports.dart';

import '../../pages/adresses/adresses_imports.dart';
import '../../pages/contact_us/contact_us_imports.dart';
import '../../pages/favourite/favourite_imports.dart';
import '../../pages/frequently_asked_questions/frequently_asked_questions_Imports.dart';
import '../../pages/setting/setting_imports.dart';
import '../../pages/vouchers/vouchers_imports.dart';

const profileRoutes = [
  CustomRoute(
    page: EditProfile,
    durationInMilliseconds: 800,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(page: Payment),
  CustomRoute(page: Addresses),
  CustomRoute(page: Vouchers),
  CustomRoute(page: Setting),
  CustomRoute(page: ChangePassword),
  CustomRoute(page: Favourite),
  CustomRoute(page: FAQ),
  CustomRoute(page: ContactUs),
  CustomRoute(page: AddAddress),
];
