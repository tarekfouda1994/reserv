class LocationEntity {
  double lat;
  double lng;
  String address;
  String title;

  LocationEntity({this.lat = 0, this.lng = 0, this.address = "",this.title=""});
}
