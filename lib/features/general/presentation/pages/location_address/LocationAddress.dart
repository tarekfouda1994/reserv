part of 'LocationAddressImports.dart';

class LocationAddress extends StatefulWidget {
  final LocationEntity location;

  LocationAddress({required this.location});

  @override
  State<StatefulWidget> createState() => _LocationAddress();
}

class _LocationAddress extends State<LocationAddress> {
  LocationAddressData locationAddressData = LocationAddressData();

  @override
  void initState() {
    locationAddressData.getAllAddresses(context, refresh: false);

    double lat =
        widget.location.lat == 0 ? 24.463765085943148 : widget.location.lat;
    double lng =
        widget.location.lng == 0 ? 55.06315838545561 : widget.location.lng;
    locationAddressData.locCubit
        .onUpdateData(LocationEntity(lat: lat, lng: lng));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      key: locationAddressData._scaffold,
      appBar: BuildLocationAppBar(locationAddressData: locationAddressData),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              BuildGoogleMapView(locationAddressData: locationAddressData),
              BuildMyAddressBody(locationAddressData: locationAddressData),
            ],
          ),
        ),
      ),
      bottomNavigationBar:
          BuildSaveButton(locationAddressData: locationAddressData),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
