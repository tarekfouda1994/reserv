part of 'LocationAddressImports.dart';

class LocationAddressData {
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  final Completer<GoogleMapController> controller = Completer();
  final GenericBloc<List<AddressModel>> addressCubit = GenericBloc([]);
  final GenericBloc<LocationEntity?> locCubit = GenericBloc(null);

  double lat = 31.0414217;
  double? lng = 31.3653301;
  double? zoom;
  late PageController pageController;
  late GoogleMapController controllerMap;

  Future<void> getLocationAddress(BuildContext context) async {
    var location = locCubit.state.data!;
    LatLng loc = LatLng(location.lat, location.lng);
    print("lat==>${loc.latitude}");
    print("lng==>${loc.longitude}");
    String address = await getIt<Utilities>().getAddress(loc, context);
    locCubit.onUpdateData(
        LocationEntity(lng: location.lng, lat: location.lat, address: address));
    print("======>>$address");
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(
      data.buffer.asUint8List(),
    );
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  void changeLocation(BuildContext context) async {
    // getIt<LoadingHelper>().showLoadingDialog();
    var loc = locCubit.state.data;
    await _updateHomeLocCubit(context, loc!, loc.address);
    // getIt<LoadingHelper>().dismissDialog();
    await _popToHomeFromMap(context, loc, loc.address);
    // }
  }

  // Future<String> getAddress() async {
  //   var loc = locCubit.state.data;
  //   GeoCode geoCode = GeoCode();
  //   var address =
  //       await geoCode.reverseGeocoding(latitude: loc!.lat, longitude: loc.lng);
  //   return address.city ?? "";
  // }

  Future<void> _updateHomeLocCubit(
      BuildContext context, LocationEntity loc, String address) async {
    await context.read<HomeLocationCubit>().onLocationUpdated(LocationEntity(
        lat: loc.lat,
        lng: loc.lng,
        title: address.split("  ").toList()[1].toString()));
  }

  Future<void> _popToHomeFromMap(
      BuildContext context, LocationEntity loc, String address) async {
    await AutoRouter.of(context).pop(LocationEntity(
        lat: loc.lat,
        lng: loc.lng,
        title: address.split("  ").toList()[1].toString()));
    AutoRouter.of(context).pop();
  }

  void getCurrentLoc(BuildContext context) async {
    var laLong = await getIt<Utilities>().getCurrentLocation(context);
    locCubit.onUpdateData(LocationEntity(
        lat: laLong?.latitude ?? 0, lng: laLong?.longitude ?? 0));
    moveCamera(context, laLong?.latitude ?? 0, laLong?.longitude ?? 0);
    getIt<LoadingHelper>().showLoadingDialog();
    await getLocationAddress(context);
    getIt<LoadingHelper>().dismissDialog();
  }

  void moveCamera(BuildContext context, double lat, double lng) {
    controllerMap.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
            target: LatLng(lat, lng), zoom: 14.0, bearing: 30.0, tilt: 45.0),
      ),
    );
  }

  GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: CustomFonts.googleMapKey);

  Future<void> displayPrediction(BuildContext context, Prediction? p) async {
    if (p != null) {
      var deviceModel = context.read<DeviceCubit>().state.model;
      PlacesDetailsResponse detail = await places.getDetailsByPlaceId(
          p.placeId!,
          language: deviceModel.locale.languageCode);
      lat = detail.result.geometry!.location.lat;
      lng = detail.result.geometry!.location.lng;
      moveCamera(context, lat, lng ?? 0);
      zoom = 10;
    }
  }

 Future<void> getAllAddresses(BuildContext context, {bool refresh = true}) async {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      var data = await GetAllAddresses()(refresh);
      for (AddressModel e in data) {
        e.isDefault = false;
      }
      addressCubit.onUpdateData(data);
    }
  }

  void selectAddress(BuildContext context, AddressModel model) async {
    var data = addressCubit.state.data.map((e) {
      e.isDefault = false;
      if (e == model) {
        e.isDefault = true;
      }
      return e;
    }).toList();
    _updateAddressCubit(data, context, model);
    await _popHomeFormMyAddress(context, model);
  }

  void _updateAddressCubit(
      List<AddressModel> data, BuildContext context, AddressModel model) {
    addressCubit.onUpdateData(data);
    context.read<HomeLocationCubit>().onLocationUpdated(
      LocationEntity(
        lat: model.latitude,
        lng: model.longitude,
        title: model.addressType,
      ),
    );
  }

  Future<void> _popHomeFormMyAddress(
      BuildContext context,
      AddressModel model,
      ) async {
    await AutoRouter.of(context).pop();
    await AutoRouter.of(context).pop(LocationEntity(
        lat: model.latitude, lng: model.longitude, title: model.addressType));
    AutoRouter.of(context).pop();
  }

 void addressListBottomSheet(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (cxt) {
          return BuildMyAddressesList(
            bloc: addressCubit,
            onSelectLocation: (model) => selectAddress(context, model)
          );
        });
  }
}
