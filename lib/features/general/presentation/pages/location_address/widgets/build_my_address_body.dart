part of 'LocationWidgetsImports.dart';

class BuildMyAddressBody extends StatelessWidget {
  final LocationAddressData locationAddressData;

  const BuildMyAddressBody({Key? key, required this.locationAddressData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<AddressModel>>,
        GenericState<List<AddressModel>>>(
      bloc: locationAddressData.addressCubit,
      builder: (cxt, state) {
        if (state is GenericUpdateState) {
          return Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    locationAddressData.addressListBottomSheet(context);
                  },
                  child: Visibility(
                    visible: state.data.isNotEmpty,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SvgPicture.asset(Res.home_location),
                        MyText(
                            title: " ${tr("My_addresses")}",
                            color: MyColors.black,
                            size: device.isTablet ? 5.sp : 9.sp),
                      ],
                    ),
                    replacement: SizedBox(),
                  ),
                ),
                InkWell(
                  onTap: () => locationAddressData.getCurrentLoc(context),
                  child: Row(
                    children: [
                      MyText(
                          title: tr("current_location"),
                          color: const Color(0xff1A65FB),
                          size: device.isTablet ? 5.sp : 9.sp),
                      SvgPicture.asset(
                        Res.target,
                        color: const Color(0xff1A65FB),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else
          return Padding(
            padding: const EdgeInsets.only(top: 15),
            child: InkWell(
              onTap: () => locationAddressData.getCurrentLoc(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  MyText(
                      title: tr("current_location"),
                      color: MyColors.blue,
                      size: device.isTablet ? 5.sp : 9.sp),
                  SvgPicture.asset(
                    Res.target,
                    color: MyColors.blue,
                  ),
                ],
              ),
            ),
          );
      },
    );
  }
}
