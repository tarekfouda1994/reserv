part of 'LocationWidgetsImports.dart';

class BuildLocationAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  final LocationAddressData locationAddressData;

  const BuildLocationAppBar({Key? key, required this.locationAddressData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultAppBar(
      title: tr("select_location"),
      bottom: PreferredSize(
          preferredSize: Size.fromHeight(2), child: Divider(height: 0)),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(65);
}
