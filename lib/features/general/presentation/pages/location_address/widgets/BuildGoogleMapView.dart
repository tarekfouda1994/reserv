part of 'LocationWidgetsImports.dart';

class BuildGoogleMapView extends StatelessWidget {
  final LocationAddressData locationAddressData;

  const BuildGoogleMapView({required this.locationAddressData});

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    double sizeHeight = device.isTablet ? .82 : .7;
    return BlocBuilder<GenericBloc<LocationEntity?>,
        GenericState<LocationEntity?>>(
      bloc: locationAddressData.locCubit,
      builder: (context, state) {
        CameraPosition _initialLoc = CameraPosition(
            target: LatLng(state.data?.lat ?? 0, state.data?.lng ?? 0),
            zoom: 12.4746);
        return Stack(
          alignment: Alignment.center,
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * sizeHeight,
                decoration: BoxDecoration(color: MyColors.white),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.r),
                  child: GoogleMap(
                      padding: EdgeInsets.only(
                          bottom: locationAddressData
                                  .addressCubit.state.data.isNotEmpty
                              ? 45
                              : 5,
                          top: 55),
                      mapType: MapType.normal,
                      initialCameraPosition: _initialLoc,
                      onMapCreated: (GoogleMapController controller) async {
                        controller.setMapStyle(json.encode(MapStyle.server));
                        locationAddressData.controller.complete(controller);
                        locationAddressData.controllerMap = controller;
                        locationAddressData.moveCamera(context,
                            state.data?.lat ?? 0, state.data?.lng ?? 0);
                      },
                      myLocationButtonEnabled: false,
                      myLocationEnabled: true,
                      rotateGesturesEnabled: true,
                      scrollGesturesEnabled: true,
                      trafficEnabled: false,
                      zoomControlsEnabled: false,
                      tiltGesturesEnabled: true,
                      compassEnabled: true,
                      indoorViewEnabled: true,
                      buildingsEnabled: true,
                      mapToolbarEnabled: true,
                      zoomGesturesEnabled: true,
                      onCameraIdle: () {
                        locationAddressData.getLocationAddress(context);
                      },
                      onTap: (location) {
                        locationAddressData.getLocationAddress(context);
                      },
                      onCameraMove: (loc) {
                        locationAddressData.locCubit.onUpdateData(
                            LocationEntity(
                                lat: loc.target.latitude,
                                lng: loc.target.longitude,
                                address: ""));
                      }),
                )),
            SvgPicture.asset(Res.map_marker_active_svg, width: 45, height: 45),
            Positioned(
              bottom: 25,
              right: 5,
              left: 5,
              child: BuildSearchField(
                displayPrediction: (cxt, b) {
                  return locationAddressData.displayPrediction(cxt, b);
                },
              ),
            )
          ],
        );
      },
    );
  }
}
