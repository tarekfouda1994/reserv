part of 'LocationWidgetsImports.dart';

class BuildSaveButton extends StatelessWidget {
  final LocationAddressData locationAddressData;

  const BuildSaveButton({required this.locationAddressData});

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return DefaultButton(
      title: tr("confirm_location"),
      onTap: () => locationAddressData.changeLocation(context),
      color: MyColors.primary,
      textColor: MyColors.white,
      borderColor: MyColors.white,
      borderRadius: BorderRadius.circular(30.r),
      margin: EdgeInsets.only(bottom: 8,right: 16,left: 16).r,
      fontSize: device.isTablet ? 7.sp : 11.sp,

      height: device.isTablet ? 60.sm : 50.sm,
    );
  }
}
