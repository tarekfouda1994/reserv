part of 'LocationWidgetsImports.dart';

class BuildHeaderBottomSheet extends StatelessWidget {
  const BuildHeaderBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: MyColors.primary,
      height: 50.h,
      padding: const EdgeInsets.all(16.0),
      child: MyText(
          color: MyColors.white,
          title: tr("swipe_up"),
          size: 11.sp,
          alien: TextAlign.center),
    );
  }
}
