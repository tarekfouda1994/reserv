part of 'LocationWidgetsImports.dart';

class BuildMyAddressesList extends StatelessWidget {
  final GenericBloc<List<AddressModel>> bloc;
  final void Function(AddressModel) onSelectLocation;

  const BuildMyAddressesList({
    Key? key,
    required this.bloc,
    required this.onSelectLocation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<AddressModel>>,
            GenericState<List<AddressModel>>>(
        bloc: bloc,
        builder: (cxt, state) {
          if (state is GenericUpdateState) {
            return Container(
              height: (MediaQuery.of(context).size.height *
                          (state.data.length > 3 ? .8 : .14)) *
                      state.data.length +
                  170,
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(10.r))),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MyText(
                            title: tr("select_address"),
                            color: MyColors.black,
                            fontWeight: FontWeight.bold,
                            size: device.isTablet ? 8.5.sp : 13.sp),
                        InkWell(
                          onTap: () => Navigator.of(context).pop(),
                          child: CircleAvatar(
                            backgroundColor: MyColors.black,
                            radius: 15.sp,
                            child: Icon(
                              Icons.close,
                              size: device.isTablet ? 15.sp : 20.sp,
                              color: MyColors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Flexible(
                      child: ListView(
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.symmetric(vertical: 20),
                            decoration: BoxDecoration(
                                border: Border.all(color: MyColors.greyLight),
                                borderRadius: BorderRadius.circular(20)),
                            child: ListView.separated(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.symmetric(vertical: 10),
                              itemBuilder: (BuildContext context, int index) {
                                return BuildMyAddressItem(
                                  onTap: (addressModel) {
                                    onSelectLocation(addressModel);
                                  },
                                  addressModel: state.data[index],
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return Divider();
                              },
                              itemCount: state.data.length,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        DefaultButton(
                          width: 150.w,
                          height: device.isTablet ? 50.sm : 45.sm,
                          color: Color(0xffF6F9F9),
                          borderRadius: BorderRadius.circular(40.r),
                          textColor: MyColors.blackOpacity,
                          fontWeight: FontWeight.w400,
                          title: tr("cancel"),
                          onTap: () => AutoRouter.of(context).pop(),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          } else {
            return Container();
          }
        });
  }
}
