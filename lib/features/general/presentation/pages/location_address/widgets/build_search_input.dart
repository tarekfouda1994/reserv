part of 'LocationWidgetsImports.dart';

class BuildSearchField extends StatelessWidget {
  final Future Function(BuildContext context, Prediction? p) displayPrediction;
  final void Function()? onTapMyAddress;

  const BuildSearchField({
    Key? key,
    required this.displayPrediction,
    this.onTapMyAddress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    bool isTablet = deviceModel.isTablet;
    return Container(
      height: isTablet ? 75.sm : 50.sm,
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(horizontal: 10).r,
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xffefefef)),
        borderRadius: BorderRadius.circular(10.r),
        color: MyColors.white,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12).r,
      child: Container(
        width: MediaQuery.of(context).size.width * .92,
        child: Row(
          children: [
            Expanded(
              child: InkWell(
                onTap: () async {
                  Prediction? p = await PlacesAutocomplete.show(
                    types: [],
                    language: deviceModel.locale.languageCode,
                    strictbounds: false,
                    components: [],
                    context: context,
                    apiKey: CustomFonts.googleMapKey,
                    mode: Mode.fullscreen,
                    logo: Container(),
                    onError: (value) {
                      return;
                    },
                  );
                  displayPrediction(context, p);
                },
                child: Row(
                  children: [
                    SvgPicture.asset(
                      Res.Search,
                      width: isTablet ? 10.w : null,
                      color: MyColors.black,
                    ),
                    SizedBox(width: 10),
                    MyText(
                      title: tr("searchMap"),
                      color: MyColors.blackOpacity,
                      size: isTablet ? 7.sp : 9.sp,
                    ),
                  ],
                ),
              ),
            ),
            if(onTapMyAddress!=null)
            Row(
              children: [
                VerticalDivider(width: 0),
                InkWell(
                  onTap: onTapMyAddress,
                  child: Padding(
                    padding: const EdgeInsetsDirectional.only(start: 12),
                    child: MyText(
                      title: tr("My_addresses"),
                      color: MyColors.black,
                      size: isTablet ? 7.sp : 9.sp,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
