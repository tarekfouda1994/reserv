part of 'LocationWidgetsImports.dart';

class BuildMyAddressItem extends StatelessWidget {
  final AddressModel addressModel;
  final void Function(AddressModel addressModel) onTap;

  const BuildMyAddressItem({
    Key? key,
    required this.addressModel,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap:(){
        onTap(addressModel);
      },
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        MyText(
            title: addressModel.addressType,
            color: MyColors.grey,
            size: device.isTablet ? 7.sp : 11.sp),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: [
              Expanded(
                child: MyText(
                    title: addressModel.locationDetail,
                    fontFamily: CustomFonts.primarySemiBoldFont,
                    color: MyColors.black,
                    size: device.isTablet ? 7.sp : 11.sp),
              ),
              SizedBox(width: 25),
              Transform.rotate(
                angle: device.locale == Locale('en', 'US') ? 0 : 3.16,
                child: SvgPicture.asset(Res.arrowForward),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
