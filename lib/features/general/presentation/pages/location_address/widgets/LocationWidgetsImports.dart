import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/map_style.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/google_places_library.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/general/domain/entities/location_entity.dart';
import 'package:flutter_tdd/res.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../profile/data/model/address_model/address_model.dart';
import '../LocationAddressImports.dart';

part 'BuildGoogleMapView.dart';
part 'BuildSaveButton.dart';
part 'build_bottom_sheet_header.dart';
part 'build_location_app_bar.dart';
part 'build_mu_address_item.dart';
part 'build_my_address_body.dart';
part 'build_my_adresses_list.dart';
part 'build_search_input.dart';
