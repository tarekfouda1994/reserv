part of 'terms_imports.dart';

class Terms extends StatefulWidget {
  const Terms({Key? key}) : super(key: key);

  @override
  _TermsState createState() => _TermsState();
}

class _TermsState extends State<Terms> {
  final TermsData termsData = TermsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: tr("termsConditions")),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10).r,
        child: BlocBuilder<GenericBloc<String>, GenericState<String>>(
            bloc: termsData.termsCubit,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                return SingleChildScrollView(
                  child: Html(
                    data: state.data,
                  ),
                );
              } else {
                return Column(
                  children: List.generate(20, (index) {
                    return BuildShimmerView(
                      height: 5,
                      width: index == 19 ? 200 : null,
                    );
                  }),
                );
              }
            }),
      ),
    );
  }
}
