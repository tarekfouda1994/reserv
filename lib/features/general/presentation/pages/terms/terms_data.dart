part of 'terms_imports.dart';

class TermsData {
  final GenericBloc<String> termsCubit = GenericBloc("");

  TermsData() {
    _getTerms(refresh: false);
    _getTerms();
  }

  Future<void> _getTerms({bool refresh = true}) async {
    await GetTerms().call(refresh).then(
      (data) {
        termsCubit.onUpdateData(data);
      },
    );
  }
}
