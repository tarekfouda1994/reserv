import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/get_terms.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'terms.dart';
part 'terms_data.dart';
