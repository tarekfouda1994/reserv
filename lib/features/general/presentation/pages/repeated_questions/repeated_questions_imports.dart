import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'repeated_questions.dart';
part 'repeated_questions_data.dart';
