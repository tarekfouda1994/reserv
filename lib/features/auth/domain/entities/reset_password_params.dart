class ResetPasswordParams {
  String password;
  String confirmPassword;

  ResetPasswordParams({required this.password, required this.confirmPassword});
}
