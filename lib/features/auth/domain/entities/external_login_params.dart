class ExternalLoginParams {
  String firstName;
  String lastName;
  String? email;
  String? phoneNumber;
  String? dateOfBirth;
  int? genderValue;
  String userPhoto;
  int externalUserType;

  ExternalLoginParams({
    required this.firstName,
    required this.lastName,
    this.email,
    this.phoneNumber,
    this.genderValue,
    this.dateOfBirth,
    required this.userPhoto,
    required this.externalUserType,
  });

  Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        if (email != "" && email != null) "email": email,
        if (phoneNumber != "" && phoneNumber != null)
          "phoneNumber": phoneNumber,
        if (genderValue != "" && genderValue != null)
          "genderValue": genderValue,
        if (dateOfBirth != "" && dateOfBirth != null)
          "dateOfBirth": dateOfBirth,
        "userPhoto": userPhoto,
        "externalUserType": externalUserType,
      };
}
