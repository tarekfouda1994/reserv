import 'package:flutter_tdd/core/models/enums.dart';

class RegisterPrams {
  String email;
  String lastName;
  String firstName;
  String phoneNumber;
  Genders genderValue;
  String nationalityId;
  String dateOfBirth;
  String password;
  String confirmPassword;
  String userPhoto;

  RegisterPrams({
    required this.password,
    required this.lastName,
    required this.email,
    required this.confirmPassword,
    required this.dateOfBirth,
    required this.firstName,
    required this.nationalityId,
    required this.genderValue,
    required this.phoneNumber,
    required this.userPhoto,
  });

  Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        "phoneNumber": phoneNumber,
        "email": email,
        if (genderValue != Genders.other) "genderValue": genderValue.index + 1,
        if (nationalityId.isNotEmpty) "nationalityId": nationalityId,
        "dateOfBirth": dateOfBirth,
        "password": password,
        "confirmPassword": confirmPassword,
        "userPhoto": userPhoto,
      };
}
