import 'dart:io';

class LoginParams {
  String email;
  String password;
  String? deviceId;

  LoginParams({
    required this.password,
    required this.email,
    required this.deviceId,
  });

  Map<String, dynamic> toJson() => {
        "userName": email,
        "password": password,
        "deviceToken": deviceId,
        "deviceType": Platform.isIOS ? "ios" : "android",
      };
}
