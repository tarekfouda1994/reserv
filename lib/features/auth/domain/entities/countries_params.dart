class CountriesParams {
  bool isActive;
  int pageNumber;
  int pageSize;
  bool refresh;

  CountriesParams(
      {required this.isActive,
      required this.pageNumber,
      required this.pageSize,
      required this.refresh});

  Map<String, dynamic> toJson() => {
        "isActive": isActive,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
      };
}
