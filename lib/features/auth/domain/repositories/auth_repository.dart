import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/features/auth/data/model/countries_item_model/countries_item_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/register_params.dart';

import '../../../../core/errors/failures.dart';
import '../../data/model/login_model/login_model.dart';
import '../entities/external_login_params.dart';
import '../entities/login_params.dart';

abstract class AuthRepository {
  Future<Either<Failure, LoginModel>> userLogin(LoginParams params);

  Future<Either<Failure, dynamic>> registerUser(RegisterPrams params);

  Future<Either<Failure, bool>> forgetPass(String email);

  Future<Either<Failure, String>> getEncryptedValue(int val);

  Future<Either<Failure, List<CountriesItemModel>>> getAllCountries(CountriesParams params);

  Future<Either<Failure, LoginModel>> externalLogin(ExternalLoginParams params);
}
