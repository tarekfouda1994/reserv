import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/auth/domain/repositories/auth_repository.dart';

class GetEncryptedValue implements UseCase<String?, int> {
  @override
  Future<String?> call(int params) async {
    var data = await getIt<AuthRepository>().getEncryptedValue(params);
    return data.fold((l) => null, (r) => r);
  }
}
