import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/auth/domain/repositories/auth_repository.dart';

class ForgetPass implements UseCase<bool?, String> {
  @override
  Future<bool?> call(String email) async {
    var result = await getIt<AuthRepository>().forgetPass(email);
    return result.fold((l) => null, (r) => r);
  }
}
