import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/auth/domain/entities/login_params.dart';
import 'package:flutter_tdd/features/auth/domain/repositories/auth_repository.dart';

import '../../data/model/login_model/login_model.dart';

class SetUserLogin implements UseCase<LoginModel?, LoginParams> {
  SetUserLogin(BuildContext context);

  @override
  Future<LoginModel?> call(LoginParams params) async {
    var result = await getIt<AuthRepository>().userLogin(params);
    if (result.isRight()) {
      return result.fold((l) => null, (r) => r);
    }
    return null;
  }
}
