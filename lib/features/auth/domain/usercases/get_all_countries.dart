import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/auth/data/model/countries_item_model/countries_item_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/repositories/auth_repository.dart';

class GetAllCounties implements UseCase<List<CountriesItemModel>, CountriesParams> {
  @override
  Future<List<CountriesItemModel>> call(CountriesParams params) async {
    var data = await getIt<AuthRepository>().getAllCountries(params);
    return data.fold((l) => [], (r) => r);
  }
}
