import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/auth/domain/entities/external_login_params.dart';
import 'package:flutter_tdd/features/auth/domain/repositories/auth_repository.dart';

import '../../data/model/login_model/login_model.dart';

class MediaLogin implements UseCase<LoginModel?, ExternalLoginParams> {
  @override
  Future<LoginModel?> call(ExternalLoginParams params) async {
    var data = await getIt<AuthRepository>().externalLogin(params);
    return data.fold((l) => null, (r) => r);
  }
}
