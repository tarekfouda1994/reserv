import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/auth/domain/entities/register_params.dart';
import 'package:flutter_tdd/features/auth/domain/repositories/auth_repository.dart';

class SetRegisterUser implements UseCase<bool, RegisterPrams> {
  @override
  Future<bool> call(RegisterPrams params) async {
    var result = await getIt<AuthRepository>().registerUser(params);
    return result.isRight();
  }
}
