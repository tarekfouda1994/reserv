import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

class BuildFieldHeader extends StatelessWidget {
  final String title;

  const BuildFieldHeader({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.h),
      child: Row(
        children: [
          MyText(
            title: title,
            color: MyColors.black,
            size: device.isTablet ? 6.sp : 10.sp,
          ),
        ],
      ),
    );
  }
}
