import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../core/bloc/device_cubit/device_cubit.dart';

class BuildHeaderTitle extends StatelessWidget {
  final String title;
  final String subTitle;

  const BuildHeaderTitle({
    Key? key,
    required this.title,
    required this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MyText(
          title: title,
          color: MyColors.black,
          size: device.isTablet ? 10.sp : 16.sp,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 15, bottom: 40),
          child: MyText(
            title: subTitle,
            color: MyColors.blackOpacity,
            size: device.isTablet ? 9.sp : 14.sp,
            letterSpace: 1.1,
          ),
        ),
      ],
    );
  }
}
