import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildAuthAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final bool? centerTitle;
  final bool? boldText;
  final double? textSize;
  final Function()? onBack;

  const BuildAuthAppBar(
      {Key? key,
      this.title,
      this.onBack,
      this.centerTitle,
      this.textSize,
      this.boldText = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: EdgeInsets.only(top: 5.h),
      child: AppBar(
        elevation: 0,
        centerTitle: centerTitle ?? false,
        title: title != null
            ? MyText(
                title: title ?? "",
                color: MyColors.black,
                size: textSize ?? (device.isTablet ? 16.sp : 18.sp),
                fontWeight: boldText == true ? FontWeight.bold : null,
              )
            : null,
        backgroundColor: Colors.transparent,
        leadingWidth: device.isTablet ? 30.w : 50.sp,
        leading: InkWell(
          onTap: onBack ?? AutoRouter.of(context).pop,
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: device.isTablet ? 2.sp : 8.w,
            ),
            decoration: BoxDecoration(
              color: MyColors.primary.withOpacity(.7),
              shape: BoxShape.circle,
            ),
            alignment: Alignment.center,
            child: Icon(
              Icons.arrow_back,
              size: device.isTablet ? 10.sp : 20.sp,
              color: MyColors.white,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(55.h);
}
