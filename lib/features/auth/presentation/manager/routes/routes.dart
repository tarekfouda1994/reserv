import 'package:auto_route/auto_route.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/confirmation_forget_password/confirmation_forget_password_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/login/login_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/register/register_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/reset_password/reset_password_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/sent_success/sent_success_imports.dart';


const authRoutes = [
  CustomRoute(page: ResetPassword),
  CustomRoute(page: SentSuccess),
  CustomRoute(page: Register),
  CustomRoute(page: Login),
  CustomRoute(page: Register),
  CustomRoute(page: ConfirmationForgetPassword),
];
