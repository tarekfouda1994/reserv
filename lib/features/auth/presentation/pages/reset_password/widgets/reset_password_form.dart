part of 'reset_password_widgets_imports.dart';

class ResetPasswordForm extends StatelessWidget {
  final ResetPasswordData resetPasswordData;

  const ResetPasswordForm({Key? key, required this.resetPasswordData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextField(
          fieldTypes: FieldTypes.password,
          type: TextInputType.text,
          action: TextInputAction.next,
          controller: resetPasswordData.password,
          validate: (value) => value?.validatePassword(),
          label: tr("new_pass"),
          onChange: resetPasswordData.onChangePass,
        ),
        CustomTextField(
          fieldTypes: FieldTypes.password,
          type: TextInputType.text,
          action: TextInputAction.done,
          validate: (value) =>
              value?.validatePasswordConfirm(pass: resetPasswordData.password.text),
          label: tr("repeat_pass"),
          margin: EdgeInsets.only(top: 20),
          controller: resetPasswordData.confirmPassword,
          onChange: resetPasswordData.onChangeConfirmPass,
        ),
      ],
    );
  }
}
