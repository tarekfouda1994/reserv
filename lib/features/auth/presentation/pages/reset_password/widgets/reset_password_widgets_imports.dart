import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/features/auth/domain/entities/reset_password_params.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/reset_password/reset_password_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/bloc/device_cubit/device_cubit.dart';

part 'reset_password_button.dart';
part 'reset_password_form.dart';
