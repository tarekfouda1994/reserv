import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/auth/domain/entities/reset_password_params.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/reset_password/widgets/reset_password_widgets_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/widgets/build_auth_app_bar.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../widgets/build_header_title.dart';

part 'reset_password.dart';
part 'reset_password_data.dart';
