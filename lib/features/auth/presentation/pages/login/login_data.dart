part of 'login_imports.dart';

class LoginData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  final GenericBloc<bool> passCubit = GenericBloc(false);

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  setUserLogin(BuildContext context, ProfileData profileData) async {
    if (formKey.currentState!.validate()) {
      btnKey.currentState!.animateForward();
      var deviceId = await FirebaseMessaging.instance.getToken();
      LoginParams params = _loginParams(deviceId);
      var result = await SetUserLogin(context)(params);
      if (result != null) {
        var data = await GetUserData().call(result.userId);
        if (data != null) {
          _setUserData(context, data, result);
        }
      }
      btnKey.currentState!.animateReverse();
    }
  }

  LoginParams _loginParams(String? deviceId) {
    return LoginParams(
      password: password.text,
      deviceId: deviceId,
      email: email.text,
    );
  }

  void _setUserData(BuildContext context, UserModel data, LoginModel result) {
    context.read<DeviceCubit>().updateUserAuth(true);
    data = _userModelCopyWith(data, result);
    getIt<Utilities>().saveUserData(data);
    GlobalState.instance.set("token", result.authenticationToken);
    context.read<UserCubit>().onUpdateUserData(data);
    _whenUserDataUpdated(context, result);
  }

  UserModel _userModelCopyWith(UserModel data, LoginModel result) {
    return data.copyWith(
      token: result.authenticationToken,
      refreshToken: result.refreshToken,
      userId: result.userId,
    );
  }

  void _whenUserDataUpdated(BuildContext context, LoginModel result) {
    btnKey.currentState!.animateReverse();
    AutoRouter.of(context)
        .pushAndPopUntil(HomeRoute(), predicate: (cxt) => false)
        .then((value) => null);
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        CustomToast.showSimpleToast(
          title: tr("success_login"),
          type: ToastType.success,
          msg: "${tr("welcome_back")} ${result.firstName}",
        );
      },
    );
  }
}
