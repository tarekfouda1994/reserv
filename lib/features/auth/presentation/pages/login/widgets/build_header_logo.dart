part of 'login_widgets_imports.dart';

class BuildHeaderLogo extends StatelessWidget {
  const BuildHeaderLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        SvgPicture.asset(Res.LoginLogo),
        Padding(
          padding: EdgeInsets.only(top: 8.h, bottom: 40.h),
          child: MyText(
            title: tr("Account_Login"),
            color: MyColors.primary,
            fontWeight: FontWeight.bold,
            size: device.isTablet ? 8.sp : 11.sp,
          ),
        ),
      ],
    );
  }
}
