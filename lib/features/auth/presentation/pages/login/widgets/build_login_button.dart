part of 'login_widgets_imports.dart';

class BuildLoginButton extends StatelessWidget {
  final LoginData loginData;
  final ProfileData profileData;

  const BuildLoginButton({Key? key, required this.loginData, required this.profileData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return LoadingButton(
      title: tr("login"),
      borderRadius: 40.r,
      onTap: () => loginData.setUserLogin(context, profileData),
      color: MyColors.primary,
      textColor: MyColors.white,
      btnKey: loginData.btnKey,
      margin: EdgeInsets.only(top: 30.h),
      fontSize: device.isTablet ? 8.sp : 12.sp,
      height: 35.h,
    );
  }
}
