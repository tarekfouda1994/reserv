part of 'login_widgets_imports.dart';

class BuildForgetPasswordView extends StatelessWidget {
  final ProfileData profileData;

  const BuildForgetPasswordView({Key? key, required this.profileData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 18.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () => AutoRouter.of(context).push(ConfirmationForgetPasswordRoute(profileData: profileData)),
            // onTap: () => profileData.pagesCubit.onUpdateData(ProfilePages.confirmationEmail),
            child: MyText(
              title: tr("forget_my_pass"),
              color: MyColors.primary,
              size: device.isTablet ? 7.sp : 11.sp,
            ),
          ),
        ],
      ),
    );
  }
}
