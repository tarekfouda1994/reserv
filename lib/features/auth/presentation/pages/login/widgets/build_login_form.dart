part of 'login_widgets_imports.dart';

class BuildLoginForm extends StatelessWidget {
  final LoginData loginData;
  final ProfileData profileData;

  const BuildLoginForm(
      {Key? key, required this.loginData, required this.profileData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: loginData.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextField(
            controller: loginData.email,
            fieldTypes: FieldTypes.normal,
            label: tr("Email"),
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            validate: (value) => value?.validateEmail(),
            hint: "${tr("ex")} Johndoe@example.com",
            autoValidateMode: AutovalidateMode.onUserInteraction,
            suffixIcon: EmailIcon(),
            margin: EdgeInsets.only(bottom: 20),
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: loginData.passCubit,
              builder: (context, state) {
                return CustomTextField(
                  controller: loginData.password,
                  fieldTypes:
                      !state.data ? FieldTypes.password : FieldTypes.normal,
                  type: TextInputType.text,
                  action: TextInputAction.done,
                  label: tr("password"),
                  onSubmit: () => loginData.setUserLogin(context, profileData),
                  validate: (value) => value?.validateEmpty(),
                  hint: "${tr("ex")} password",
                  autoValidateMode: AutovalidateMode.onUserInteraction,
                  suffixIcon: InkWell(
                      onTap: () =>
                          loginData.passCubit.onUpdateData(!state.data),
                      child: PasswordIcon(showPass: state.data),
                  ),
                );
              }),
        ],
      ),
    );
  }
}
