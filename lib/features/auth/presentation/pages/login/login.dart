part of 'login_imports.dart';

class Login extends StatefulWidget {
  final ProfileData profileData;

  const Login({Key? key, required this.profileData}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final LoginData loginData = LoginData();

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: BuildAuthAppBar(
        onBack: () => AutoRouter.of(context).pop(),
        // onBack: () => widget.profileData.pagesCubit.onUpdateData(ProfilePages.intro),
      ),
      body: GestureDetector(
        onTap: FocusScope.of(context).unfocus,
        child: ListView(
          padding: EdgeInsets.symmetric(
            horizontal: device.isTablet ? 10.sp : 15.w,
            vertical: 20.h,
          ),
          children: [
            BuildHeaderLogo(),
            BuildLoginForm(loginData: loginData, profileData: widget.profileData),
            BuildLoginButton(loginData: loginData, profileData: widget.profileData),
            BuildForgetPasswordView(profileData: widget.profileData),
          ],
        ),
      ),
    );
  }
}
