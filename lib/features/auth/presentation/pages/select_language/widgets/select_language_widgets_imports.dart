
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/custom_lib/country_calling/country.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/select_language/select_language_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'build_intro_header.dart';
part 'build_language_filed.dart';
part 'build_languages_view.dart';
part 'select_country_field.dart';
part 'select_lang_button.dart';
