
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/custom_lib/country_calling/country.dart';
import 'package:flutter_tdd/core/custom_lib/country_calling/functions.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../widgets/build_header_title.dart';
import 'widgets/select_language_widgets_imports.dart';

part 'select_language.dart';
part 'select_language_data.dart';
