part of 'confirmation_forget_pass_widget_imports.dart';

class BuildResetPassForm extends StatelessWidget {
  final ConfirmationForgetPasswordController controller;

  const BuildResetPassForm({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: controller.formKey,
      child: CustomTextField(
        controller: controller.email,
        fieldTypes: FieldTypes.normal,
        margin: EdgeInsets.symmetric(vertical: 20.sm, horizontal: 16),
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        type: TextInputType.emailAddress,
        action: TextInputAction.done,
        validate: (value) => value?.validateEmail(),
        label: tr("email"),
        suffixIcon: BuildSuffixIcon(
            asset: Res.envelope_input, assetColor: MyColors.black),
      ),
    );
  }
}
