part of 'confirmation_forget_pass_widget_imports.dart';

class BuildResetPassButton extends StatelessWidget {
  final ProfileData profileData;
  final ConfirmationForgetPasswordController
      confirmationForgetPasswordController;

  const BuildResetPassButton(
      {Key? key,
      required this.profileData,
      required this.confirmationForgetPasswordController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        LoadingButton(
          btnKey: confirmationForgetPasswordController.btnKey,
          title: tr("send_email"),
          onTap: () => confirmationForgetPasswordController.onSubmitSend(
              context, profileData),
          color: MyColors.primary,
          textColor: MyColors.white,
          borderRadius: 40.r,
          margin: EdgeInsets.symmetric(horizontal: 40.r),
          fontSize: device.isTablet ? 8.sp : 12.sp,
          height: 35.h,
        ),
        SizedBox(height: 20.sm),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
              alien: TextAlign.center,
              title: tr("Remember_password"),
              color: MyColors.blackOpacity,
              size: device.isTablet ? 7.sp : 11.sp,
            ),
            GestureDetector(
              // onTap: () =>
              //     profileData.pagesCubit.onUpdateData(ProfilePages.login),
              onTap: () => AutoRouter.of(context).pop(),
              child: MyText(
                alien: TextAlign.center,
                title: tr("login"),
                color: MyColors.primary,
                size: device.isTablet ? 7.sp : 11.sp,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
