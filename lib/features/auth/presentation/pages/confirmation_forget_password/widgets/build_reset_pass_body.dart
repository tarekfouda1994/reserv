part of 'confirmation_forget_pass_widget_imports.dart';

class BuildResetPassBody extends StatelessWidget {
  final ConfirmationForgetPasswordController controller;

  const BuildResetPassBody({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 70.w),
      child: Column(
        children: [
          SizedBox(height: 80.h),
          SvgPicture.asset(Res.sendResetPassword),
          Padding(
            padding: EdgeInsets.only(top: 8.h, bottom: 60.h),
            child: MyText(
              title: tr("reset_pass"),
              color: MyColors.primary,
              size: device.isTablet ? 8.sp : 11.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
          MyText(
            alien: TextAlign.center,
            title: "${tr("reset_link_registered_email")} :",
            color: MyColors.blackOpacity,
            size: device.isTablet ? 7.sp : 11.sp,
          ),
        ],
      ),
    );
  }
}
