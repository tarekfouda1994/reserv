part of 'confirmation_forget_password_imports.dart';

class ConfirmationForgetPasswordController {
  GlobalKey<CustomButtonState> btnKey = GlobalKey();
  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController email = TextEditingController();


  onSubmitSend(BuildContext context, ProfileData profileData) async {
    if (formKey.currentState!.validate()) {
      btnKey.currentState!.animateForward();
      var result = await ForgetPass()(email.text);
      btnKey.currentState!.animateReverse();
      if (result != null) {
        profileData.email = email.text;
        AutoRouter.of(context).push(SentSuccessRoute(profileData: profileData));
      }
    }
  }
}
