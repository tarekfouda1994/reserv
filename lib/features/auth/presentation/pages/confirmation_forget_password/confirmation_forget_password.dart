part of 'confirmation_forget_password_imports.dart';

class ConfirmationForgetPassword extends StatefulWidget {
  final ProfileData profileData;

  const ConfirmationForgetPassword({Key? key, required this.profileData})
      : super(key: key);

  @override
  State<ConfirmationForgetPassword> createState() =>
      _ConfirmationForgetPasswordState();
}

class _ConfirmationForgetPasswordState
    extends State<ConfirmationForgetPassword> {
  ConfirmationForgetPasswordController controller =
      ConfirmationForgetPasswordController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        onBack: () => AutoRouter.of(context).pop(),
        title: tr("reset_pass"),
      ),
      body: ListView(
        children: [
          BuildResetPassBody(controller: controller),
          BuildResetPassForm(controller: controller),
          BuildResetPassButton(
            profileData: widget.profileData,
            confirmationForgetPasswordController: controller,
          ),
        ],
      ),
    );
  }
}
