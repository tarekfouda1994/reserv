import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/set_forget_pass.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/profile/profile_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'widgets/confirmation_forget_pass_widget_imports.dart';

part 'confirmation_forget_password.dart';
part 'confirmation_forget_password_controller.dart';