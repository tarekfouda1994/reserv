
import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/adaptive_picker.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/enums.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/features/auth/data/model/countries_item_model/countries_item_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/register_params.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/ge_encrypted_value.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/set_register_user.dart';
import 'package:flutter_tdd/features/auth/presentation/widgets/build_auth_app_bar.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/profile/profile_imports.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:tf_custom_widgets/Inputs/custom_dropDown/CustomDropDown.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'widgets/register_widgets_imports.dart';

part 'register.dart';
part 'register_data.dart';
