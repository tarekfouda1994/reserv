part of 'register_widgets_imports.dart';

class BuildRegisterForm extends StatelessWidget {
  final RegisterData registerData;

  const BuildRegisterForm({Key? key, required this.registerData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: registerData.formKey,
      child: Column(
        children: [
          // InlineFieldValidationView(
          //   controller: registerData.firstName,
          //   validate: (value)=> value?.validateEmpty(),
          //   child: Row(
          //     children: [
          //
          //     ],
          //   ),
          // ),
          CustomTextField(
            fieldTypes: FieldTypes.normal,
            controller: registerData.firstName,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            label: tr("first_name"),
            validate: (value) => value?.validateName(),
            hint: "${tr("ex")} ${tr("ex_name")}",
            suffixIcon: UserIcon(),
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            margin: EdgeInsets.only(bottom: 15.h),
          ),
          CustomTextField(
            controller: registerData.lastName,
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            label: tr("last_name"),
            suffixIcon: UserIcon(),
            validate: (value) => value?.validateName(),
            hint: "${tr("ex")} ${tr("ex_name")}",
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            margin: EdgeInsets.only(bottom: 15.h),
          ),
          BuildGenderView(registerData: registerData),
          BlocConsumer<GenericBloc<String>, GenericState<String>>(
              bloc: registerData.dateCubit,
              builder: (cxt, state) {
                return CustomTextField(
                  onTab: () => registerData.setDateOfBirth(context),
                  controller: registerData.dateOfBirth,
                  fieldTypes: FieldTypes.clickable,
                  type: TextInputType.text,
                  label: tr("date_of_birth"),
                  action: TextInputAction.next,
                  validate: (value) => state.data.validateEmpty(),
                  hint: tr("date_hint"),
                  margin: EdgeInsets.only(bottom: 15.h),
                  suffixIcon: CalendarIcon(),
                );
              },
              listener: (cxt, listener) {
                registerData.dateOfBirth.text = listener.data;
              }),
          CustomTextField(
            controller: registerData.email,
            fieldTypes: FieldTypes.normal,
            type: TextInputType.emailAddress,
            action: TextInputAction.next,
            label: tr("email"),
            validate: (value) => value?.validateEmail(),
            hint: "email@example.com",
            margin: EdgeInsets.only(bottom: 15.h),
            contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            suffixIcon: EmailIcon(),
          ),
          PhoneComponent(
            phone: registerData.phone,
            codeCubit: registerData.codeCubit,
            onViewCountries: () => registerData.showCountryDialog(context),
          ),
          // BuildNationalityView(registerData: registerData),
          Column(
            children: [
              CustomTextField(
                onTab: () => nationalitySheet(context),
                fieldTypes: FieldTypes.clickable,
                controller: registerData.nationality,
                type: TextInputType.text,
                action: TextInputAction.done,
                label: tr("nationality"),
                validate: (value) => registerData.nationality.text.validateEmpty(),
                hint: tr("nationality"),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                margin: EdgeInsets.only(bottom: 15.h),
              ),
            ],
          ),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: registerData.passCubit,
              builder: (context, passState) {
                return CustomTextField(
                  fieldTypes:
                      !passState.data ? FieldTypes.password : FieldTypes.normal,
                  controller: registerData.password,
                  label: tr("password"),
                  type: TextInputType.visiblePassword,
                  action: TextInputAction.next,
                  validate: (value) => value?.validatePassword(),
                  hint: "Password",
                  margin: EdgeInsets.only(bottom: 15.h),
                  suffixIcon: InkWell(
                    onTap: () {
                      registerData.passCubit.onUpdateData(!passState.data);
                    },
                    child: PasswordIcon(
                      assetColor: passState.data ? MyColors.primary : null,
                      showPass: passState.data,
                    ),
                  ),
                );
              }),
          BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: registerData.confirmPassCubit,
              builder: (context, state) {
                return CustomTextField(
                  controller: registerData.confirmPass,
                  fieldTypes:
                      !state.data ? FieldTypes.password : FieldTypes.normal,
                  type: TextInputType.visiblePassword,
                  action: TextInputAction.next,
                  label: tr("confirm_pass"),
                  validate: (value) => value?.validatePasswordConfirm(
                      pass: registerData.password.text),
                  hint: tr("confirm_pass"),
                  margin: EdgeInsets.only(bottom: 15.h),
                  suffixIcon: InkWell(
                    onTap: () {
                      registerData.confirmPassCubit.onUpdateData(!state.data);
                    },
                    child: PasswordIcon(
                      assetColor: state.data ? MyColors.primary : null,
                      showPass: state.data,
                    ),
                  ),
                );
              }),
        ],
      ),
    );
  }

  void nationalitySheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: MyColors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25).r,
        ),
        builder: (cxt) {
          return BuildNationalitySheet(
            prevSelectedItem: registerData.nationality.text,
            onSelectCallBac: (item) {
              registerData.nationality.text = item.getNationName();
              registerData.selectedNationality = item;
            },
          );
        });
  }
}
