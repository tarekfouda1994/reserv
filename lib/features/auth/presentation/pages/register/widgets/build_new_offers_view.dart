part of 'register_widgets_imports.dart';

class BuildNewOffersView extends StatelessWidget {
  final RegisterData registerData;

  const BuildNewOffersView({Key? key, required this.registerData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: registerData.offersCubit,
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15.h),
          child: Row(
            children: [
              SizedBox(
                height: 16.h,
                width: device.isTablet ? 10.w : 28.w,
                child: Checkbox(
                  value: state.data,
                  activeColor: MyColors.primary,
                  onChanged: (value) => registerData.offersCubit.onUpdateData(value!),
                ),
              ),
              Expanded(
                child: MyText(
                  title: tr("receive_news"),
                  color: MyColors.blackOpacity,
                  size: device.isTablet ? 6.sp : 9.sp,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
