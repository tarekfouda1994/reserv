part of 'register_widgets_imports.dart';

class BuildNationalityView extends StatelessWidget {
  final RegisterData registerData;

  const BuildNationalityView({Key? key, required this.registerData})
      : super(key: key);

  getCurrentImage(GenericState<CountriesItemModel?> state) {
    if (state.data != null) {
      if (state.data!.flagImage.isNotEmpty) {
        return SizedBox(
          width: 65,
          child: Row(
            children: [
              Container(
                width: 60,
                height: 42.h,
                padding: EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                  color: MyColors.greyWhite,
                  borderRadius: BorderRadius.horizontal(
                    left: Radius.circular(10),
                  ),
                ),
                child: CachedImage(
                  url: state.data?.flagImage ?? "",
                ),
              ),
            ],
          ),
        );
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextField(
          onTab: () => nationalitySheet(context),
          fieldTypes: FieldTypes.clickable,
          controller: registerData.nationality,
          type: TextInputType.text,
          action: TextInputAction.none,
          label: tr("nationality"),
          validate: (value) => value?.validateEmpty(),
          hint: tr("nationality"),
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          margin: EdgeInsets.only(bottom: 15.h),
        ),
      ],
    );
  }

  void nationalitySheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: MyColors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25).r,
        ),
        builder: (cxt) {
          return BuildNationalitySheet(
            prevSelectedItem: registerData.nationality.text,
            onSelectCallBac: (item) {
              registerData.nationality.text = item.getNationName();
              registerData.selectedNationality = item;
            },
          );
        });
  }
}
