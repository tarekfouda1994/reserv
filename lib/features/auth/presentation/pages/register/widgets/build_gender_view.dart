part of 'register_widgets_imports.dart';

class BuildGenderView extends StatelessWidget {
  final RegisterData registerData;

  const BuildGenderView({Key? key, required this.registerData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<Genders?>, GenericState<Genders?>>(
      bloc: registerData.genderCubit,
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Column(
            children: [
              BuildFieldHeader(title: tr("gender")),
              Row(
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        BuildRadioItem(
                          title: tr("male"),
                          changeValue: state.data,
                          onTap: () => registerData.genderCubit
                              .onUpdateData(Genders.male),
                          value: Genders.male,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        BuildRadioItem(
                          title: tr("female"),
                          changeValue: state.data,
                          onTap: () => registerData.genderCubit
                              .onUpdateData(Genders.female),
                          value: Genders.female,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
