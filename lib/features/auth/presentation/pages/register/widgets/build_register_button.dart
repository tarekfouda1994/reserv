part of 'register_widgets_imports.dart';

class BuildRegisterButton extends StatelessWidget {
  final RegisterData registerData;
  final ProfileData profileData;

  const BuildRegisterButton(
      {Key? key, required this.registerData, required this.profileData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: LoadingButton(
        title: tr("create_account"),
        onTap: () => registerData.setRegisterUserData(context, profileData),
        color: MyColors.primary,
        borderRadius: 30.r,
        textColor: MyColors.white,
        btnKey: registerData.btnKey,
        margin: EdgeInsets.symmetric(
            horizontal: device.isTablet ? 10.w : 20.w, vertical: 10.h),
        fontSize: device.isTablet ? 8.sp : 12.sp,
        height: 35.h,
      ),
    );
  }
}
