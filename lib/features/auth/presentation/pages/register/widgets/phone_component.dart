import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/card_number_formatter.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/phone_icon.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class PhoneComponent extends StatelessWidget {
  final TextEditingController phone;
  final VoidCallback onViewCountries;
  final GenericBloc<Country?> codeCubit;

  const PhoneComponent({
    Key? key,
    required this.phone,
    required this.onViewCountries,
    required this.codeCubit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: EdgeInsets.only(bottom: 15.h),
      child: BlocBuilder<GenericBloc<Country?>, GenericState<Country?>>(
          bloc: codeCubit,
          builder: (context, state) {
            bool isUAT = state.data?.countryCode == "AE" ||
                state.data?.countryCode == null;
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () => onViewCountries(),
                  child: Container(
                    padding: EdgeInsetsDirectional.only(end: 10),
                    decoration: BoxDecoration(
                        color: Color(0xffF6F9F9),
                        borderRadius: BorderRadius.circular(8)),
                    child: Row(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: 50,
                          height: model.isTablet ? 80.sm : 52.sm,
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: state.data == null
                              ? Image.asset(
                                  "flags/are.png",
                                )
                              : Text(
                                  state.data?.flagEmoji ?? "",
                                  style: TextStyle(
                                    fontSize: 22,
                                  ),
                                ),
                        ),
                        SizedBox(width: 2.w),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0).r,
                          child: MyText(
                            alien: TextAlign.center,
                            title: state.data?.phoneCode ?? "+971",
                            color: MyColors.black,
                            size: model.isTablet ? 8.sp : 12.sp,
                          ),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          size: model.isTablet ? 30 : 20,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 15),
                Expanded(
                  child: CustomTextField(
                    controller: phone,
                    fieldTypes: FieldTypes.normal,
                    label: tr("phone"),
                    type: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      LengthLimitingTextInputFormatter(isUAT ? 9 : 10),
                      if (model.locale.languageCode == "en")
                        PhoneNumberFormatter(),
                    ],
                    action: TextInputAction.next,
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    suffixIcon: PhoneIcon(),
                    validate: (value) =>
                        phone.text.validatePhone(lengthNum: isUAT ? 9 : 10),
                    hint: isUAT ? "*** *** ***" : "*** *** ****",
                  ),
                ),
              ],
            );
          }),
    );
  }

  String convertToArabicNumber(String number) {
    String res = '';

    final arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
    for (var element in number.characters) {
      res += arabic[int.parse(element)];
    }
    return res;
  }
}
