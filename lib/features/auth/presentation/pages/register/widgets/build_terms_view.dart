part of 'register_widgets_imports.dart';

class BuildTermsView extends StatelessWidget {
  final RegisterData registerData;

  const BuildTermsView({Key? key, required this.registerData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: registerData.termsCubit,
      builder: (context, state) {
        return Wrap(
          runAlignment: WrapAlignment.center,
          alignment: WrapAlignment.start,
          runSpacing: 6.w,
          crossAxisAlignment: WrapCrossAlignment.start,
          children: [
            SizedBox(
              height: 16.h,
              width: device.isTablet ? 10.w : 28.w,
              child: Checkbox(
                value: state.data,
                activeColor: MyColors.primary,
                onChanged: (value) => registerData.termsCubit.onUpdateData(value!),
              ),
            ),
            MyText(
              title: tr("agree_reserve"),
              color: MyColors.blackOpacity,
              size: device.isTablet ? 6.sp : 9.sp,
            ),
            InkWell(
              onTap: ()=> AutoRouter.of(context).push(const TermsRoute()),
              child: MyText(
                title: tr("reserve_terms"),
                color: MyColors.primary,
                size: device.isTablet ? 6.sp : 9.sp,
              ),
            ),
            // MyText(
            //   title: " & ",
            //   color: MyColors.blackOpacity,
            //   size: device.isTablet ? 6.sp : 9.sp,
            // ),
            // MyText(
            //   title: tr("reserve_terms"),
            //   color: MyColors.primary,
            //   size: device.isTablet ? 6.sp : 9.sp,
            // ),
          ],
        );
      },
    );
  }
}

