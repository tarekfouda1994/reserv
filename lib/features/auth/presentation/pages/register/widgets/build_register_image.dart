part of 'register_widgets_imports.dart';

class BuildRegisterImage extends StatelessWidget {
  final RegisterData controller;

  const BuildRegisterImage({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          height: 100.r,
          child: BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
            bloc: controller.imageCubit,
            builder: (context, state) {
              if (state.data != null) {
                return Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: FileImage(state.data!),
                      fit: BoxFit.cover,
                    ),
                    shape: BoxShape.circle,
                  ),
                  width: 75.r,
                  height: 75.r,
                );
              } else
                return SvgPicture.asset(
                  Res.staff_default_img,
                  width: 75.r,
                  height: 75.r,
                );
            },
          ),
        ),
        BlocBuilder<GenericBloc<File?>, GenericState<File?>>(
            bloc: controller.imageCubit,
            builder: (context, state) {
              return Row(
                children: [
                  InkWell(
                    onTap: () => controller.fetchUserImage(context),
                    child: BuildProfileImageOptions(
                      title: (state.data != null) ? tr("change") : tr("upload"),
                      child: SvgPicture.asset(
                        (state.data != null) ? Res.pencil : Res.upload,
                        color: MyColors.primary,
                        width: 12.r,
                        height: 12.r,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: controller.imageCubit.state.data != null,
                    child: InkWell(
                      onTap: () {
                        controller.imageCubit.onUpdateData(null);
                      },
                      child: BuildProfileImageOptions(
                        title: tr("delete"),
                        fontColor: MyColors.errorColor,
                        child: SvgPicture.asset(Res.trash),
                      ),
                    ),
                  ),
                ],
              );
            }),
      ],
    );
  }
}
