part of 'register_imports.dart';

class RegisterData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<CustomButtonState> btnKey = GlobalKey();
  final GlobalKey<DropdownSearchState> countriesKey = GlobalKey();
  final GenericBloc<bool> passCubit = GenericBloc(false);
  final GenericBloc<bool> confirmPassCubit = GenericBloc(false);
  final GenericBloc<Genders?> genderCubit = GenericBloc(null);
  final GenericBloc<File?> imageCubit = GenericBloc(null);
  final GenericBloc<CountriesItemModel?> nationCubit = GenericBloc(null);
  final GenericBloc<bool> termsCubit = GenericBloc(false);
  final GenericBloc<bool> offersCubit = GenericBloc(false);
  final GenericBloc<String> dateCubit = GenericBloc('');
  final GenericBloc<Country?> codeCubit = GenericBloc(null);

  final TextEditingController firstName = TextEditingController();
  final TextEditingController lastName = TextEditingController();
  final TextEditingController dateOfBirth = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController nationality = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();

  CountriesItemModel? selectedNationality;
  late DateTime dateTimeOfBirth;

  String callingCode = "+971";

  void setSelectCounties(CountriesItemModel model) {
    selectedNationality = model;
    nationCubit.onUpdateData(model);
  }

  setDateOfBirth(BuildContext context) {
    WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
    Future.delayed(Duration(milliseconds: 200), () => _datePicker(context));
  }

  _datePicker(BuildContext context) {
    AdaptivePicker.datePicker(
      context: context,
      title: tr("select_date_of_birth"),
      initial: DateTime.now().subtract(Duration(days: 365 * 16)),
      minDate: DateTime(1800),
      maxDate: DateTime.now().subtract(Duration(days: 365 * 16)),
      onConfirm: (date) => _onConfirmDate(context, date),
    );
  }

  void _onConfirmDate(BuildContext context, DateTime? date) {
    final languageCode =
        context.read<DeviceCubit>().state.model.locale.languageCode;
    if (date != null) {
      dateOfBirth.text = date.toUtc().toString();
      dateCubit
          .onUpdateData(DateFormat("dd/MM/yyyy", languageCode).format(date));
      dateTimeOfBirth = date;
      // Future.delayed(
      //   const Duration(milliseconds: 400),
      //   () => formKey.currentState!.validate(),
      // );
    }
  }

  Future<String?> getEncryptedValue(BuildContext context) async {
    var data = await GetEncryptedValue().call(1);
    return data;
  }

  setRegisterUserData(BuildContext context, ProfileData profileData) async {
    if (_checkValidation()) {
      if (!termsCubit.state.data) {
        CustomToast.showSimpleToast(
          title: tr("Info"),
          msg: tr("pleas_accept_terms"),
          type: ToastType.info,
        );
        return;
      }
      await _onCallRegister(context, profileData);
    } else {
      _onValidationError();
    }
  }

  bool _checkValidation() => formKey.currentState!.validate();

  Future<void> _onCallRegister(
      BuildContext context, ProfileData profileData) async {
    if (genderCubit.state.data == null) {
      CustomToast.showSimpleToast(
        title: tr("fieldsValidation"),
        msg: tr("genderValidation"),
        type: ToastType.error,
      );
      return;
    }
    RegisterPrams params = _registerPrams();
    btnKey.currentState!.animateForward();
    await SetRegisterUser()(params).then((value) {
      btnKey.currentState!.animateReverse();
      if (value) {
        _onSuccessRegister(context, profileData);
      }
    });
  }

  void _onValidationError() {
    CustomToast.showSimpleToast(
      title: tr("fieldsValidation"),
      msg: tr("enterAllRequiredFields"),
      type: ToastType.error,
    );
  }

  void _onSuccessRegister(BuildContext context, ProfileData profileData) {
    CustomToast.showSimpleToast(
      title: tr("success_create_account"),
      msg: tr("success_register"),
      type: ToastType.success,
    );
    Future.delayed(
      Duration(milliseconds: 1500),
      () {
        clearData();
        // ActiveAccountRoute
        AutoRouter.of(context).push(LoginRoute(profileData: profileData));
      },
    );
  }

  void clearData() {
    firstName.clear();
    lastName.clear();
    email.clear();
    password.clear();
    confirmPass.clear();
    phone.clear();
    selectedNationality = null;
    imageCubit.onUpdateData(null);
    genderCubit.onUpdateData(null);
  }

  String _handlePhone() {
    var callingCode = codeCubit.state.data?.phoneCode ?? "+971";
    var phoneNum = phone.text;

    if (phoneNum.startsWith("0")) {
      callingCode += phoneNum.substring(1);
    } else {
      callingCode += phoneNum;
    }
    return callingCode;
  }

  RegisterPrams _registerPrams() {
    String phoneNum = _handlePhone();
    return RegisterPrams(
      firstName: firstName.text,
      password: password.text,
      lastName: lastName.text,
      confirmPassword: confirmPass.text,
      phoneNumber: phoneNum,
      email: email.text,
      userPhoto: _base64Image(),
      nationalityId: selectedNationality?.encryptId ?? "",
      genderValue: genderCubit.state.data!,
      dateOfBirth: DateFormat("yyyy-MM-dd 'T' hh:mm:ss")
          .format(dateTimeOfBirth)
          .replaceAll(" ", ""),
    );
  }

  fetchUserImage(BuildContext context) async {
    var result = await getIt<Utilities>().getImage();
    imageCubit.onUpdateData(result);
    cropImage(context, result);
  }

  String _base64Image() {
    String base64Image = "";
    if (imageCubit.state.data != null) {
      base64Image =
          "data:image/${imageCubit.state.data?.path.split(".").last};base64,";
      List<int> imageBytes = imageCubit.state.data!.readAsBytesSync();
      base64Image += base64Encode(imageBytes);
    }
    return base64Image;
  }

  Future<void> cropImage(BuildContext context, File? image) async {
    getIt<Utilities>().cropImage(
      context,
      image,
      callback: (CroppedFile? croppedFile) {
        if (croppedFile != null) {
          imageCubit.onUpdateData(File(croppedFile.path));
        } else {
          imageCubit.onUpdateData(null);
        }
      },
    );
  }

  void showCountryDialog(BuildContext context) async {
    getIt<Utilities>().showCountryDialog(
      context,
      onSelect: (country) => codeCubit.onUpdateData(country),
    );
  }
}
