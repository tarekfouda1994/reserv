part of 'register_imports.dart';

class Register extends StatefulWidget {
  final ProfileData profileData;

  const Register({Key? key, required this.profileData}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final RegisterData registerData = RegisterData();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: BuildAuthAppBar(
          textSize: device.isTablet ? 10.sp : 14.sp,
          boldText: true,
          onBack: () => AutoRouter.of(context).pop(),
          title: tr("create_new_account")),
      body: GestureDetector(
        onTap: FocusScope.of(context).unfocus,
        child: Column(
          children: [
            Flexible(
              child: ListView(
                padding: EdgeInsets.symmetric(
                  horizontal: device.isTablet ? 10.w : 15.w,
                  vertical: device.isTablet ? 5.w : 20.h,
                ),
                children: [
                  BuildRegisterImage(controller: registerData),
                  BuildRegisterForm(registerData: registerData),
                  BuildTermsView(registerData: registerData),
                  // BuildNewOffersView(registerData: registerData),
                ],
              ),
            ),
            BuildRegisterButton(
              registerData: registerData,
              profileData: widget.profileData,
            ),
          ],
        ),
      ),
    );
  }
}
