part of 'intro_screen_imports.dart';

class IntroScreen extends StatefulWidget {
  final ProfileData profileData;

  const IntroScreen({Key? key, required this.profileData}) : super(key: key);

  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      body: Container(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            BuildIntroHeader(),
            BuildIntroOptionsView(),
            BuildIntroButton(
              onTap: () => widget.profileData.googleLogin(context),
              title: tr("continue"),
              bgColor: Colors.grey.shade200,
              textColor: MyColors.primaryDark,
              image: Res.google,
            ),
            BuildIntroButton(
              onTap: () => widget.profileData.faceBookLogin(),
              title: tr("continue"),
              bgColor: MyColors.blueFaceBook,
              textColor: MyColors.white,
              image: Res.facebook,
            ),
            BuildIntroButton(
              // onTap: () => widget.profileData.pagesCubit
              //     .onUpdateData(ProfilePages.login),
              onTap: ()=> AutoRouter.of(context).push(LoginRoute( profileData: widget.profileData)),
              title: tr("email_login"),
              textColor: MyColors.white,
              bgColor: MyColors.primary,
              borderColor: MyColors.primary,
              image: Res.email,
            ),
            BuildRegisterButton(profileData: widget.profileData)
          ],
        ),
      ),
    );
  }
}
