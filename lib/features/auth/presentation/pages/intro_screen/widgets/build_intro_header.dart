part of 'intro_widgets_imports.dart';

class BuildIntroHeader extends StatelessWidget {
  const BuildIntroHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40.w),
      margin: EdgeInsets.only(top: 35.h),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(Res.intro_bg),
        alignment: Alignment.topCenter,
        fit: BoxFit.fitWidth,
      )),
      alignment: Alignment.topCenter,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            // padding: EdgeInsets.only(bottom: 20.h),
            height: MediaQuery.of(context).size.height * .25,
            alignment: Alignment.center,
            child: SvgPicture.asset(
              Res.logo,
              width: 100.w,
              height: 100.h,
            ),
          ),
          // SizedBox(height: 10.h),
          MyText(
            title: tr("smoothest_salon"),
            color: MyColors.primary,
            size: device.isTablet ? 16.sp : 20.sp,
            letterSpace: device.locale == "en" ? 1.2 : .4,
            wordSpace: 1.5,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 8.h),
          MyText(
            title: tr("get_glow"),
            color: MyColors.secondary,
            size: device.isTablet ? 10.sp : 12.sp,
            letterSpace: device.locale == "en" ? 1.2 : .4,
            wordSpace: 1.5,
          ),
        ],
      ),
    );
  }
}
