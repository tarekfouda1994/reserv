part of 'intro_widgets_imports.dart';

class BuildRegisterButton extends StatelessWidget {
  final ProfileData profileData;

  const BuildRegisterButton({Key? key, required this.profileData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      // onTap: () => profileData.pagesCubit.onUpdateData(ProfilePages.register),
      onTap: () => AutoRouter.of(context).push(RegisterRoute(profileData: profileData)),
      child: Container(
        height: 40.h,
        margin: EdgeInsets.only(left: 20.w, right: 20.w, top: 24.h),
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: MyColors.grey.withOpacity(.8))),
        alignment: Alignment.center,
        child: Row(
          children: [
            SizedBox(width: MediaQuery.of(context).size.width * .25),
            Image.asset(
              Res.addFriend,
              width: 25,
              height: 25,
              color: MyColors.grey,
            ),
            SizedBox(width: 12),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  title: tr("no_account"),
                  color: MyColors.grey,
                  size: device.isTablet ? 5.5.sp : 9.sp,
                ),
                SizedBox(height: 3.w),
                MyText(
                  title: tr("register"),
                  color: MyColors.black,
                  size: device.isTablet ? 7.sp : 11.sp,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
