part of 'intro_widgets_imports.dart';

class BuildIntroButton extends StatelessWidget {
  final String title;
  final Color? bgColor;
  final Color? textColor;
  final Color? borderColor;
  final String? image;
  final Function() onTap;

  const BuildIntroButton({
    Key? key,
    required this.title,
    required this.onTap,
    this.bgColor,
    this.borderColor,
    this.textColor,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 40.h,
        margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 6.h),
        decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: borderColor ?? Colors.transparent)),
        alignment: Alignment.center,
        child: Row(
          children: [
            SizedBox(width: MediaQuery.of(context).size.width * .25),
            if (image != null)
              Image.asset(
                image ?? "",
                width: 30.r,
                height: 30.r,
              ),
            SizedBox(width: 10.w),
            Expanded(
              child: MyText(
                title: title,
                color: textColor ?? MyColors.black,
                alien: TextAlign.start,
                size: device.isTablet ? 7.sp : 11.sp,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
