part of 'intro_widgets_imports.dart';

class BuildIntroOptionsView extends StatelessWidget {
  const BuildIntroOptionsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 25.h, horizontal: 30.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          BuildIntroOptionsItem(
            onTap: ()=>
              AutoRouter.of(context).push(SettingRoute(formIntroScreen: true)),
            title:device.locale.languageCode=="en"? tr("langEn"):tr("langAr"),
            image: Res.language,
          ),
          BuildIntroOptionsItem(
            onTap: () => AutoRouter.of(context).push(ContactUsRoute()),
            title: tr("support"),
            image: Res.support,
          ),
          BuildIntroOptionsItem(
            onTap: () => AutoRouter.of(context).push(FAQRoute()),
            title: tr("faq"),
            image: Res.faq_new,
          ),
        ],
      ),
    );
  }
}
