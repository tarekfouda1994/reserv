part of 'intro_widgets_imports.dart';

class BuildIntroOptionsItem extends StatelessWidget {
  final String title;
  final String image;
  final void Function() onTap;

  const BuildIntroOptionsItem(
      {Key? key, required this.title, required this.image, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap: onTap,
      child: Row(mainAxisSize: MainAxisSize.min, children: [
        SvgPicture.asset(image, width: 25.r, height: 25.r),
        MyText(
          title: title,
          fontFamily: device.locale.languageCode == "en"
              ? "workSans"
              : "readexProMedium",
          size: device.isTablet ? 6.sp : 10.sp,
          color: MyColors.black,
        ),
      ]),
    );
  }
}
