import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/features/auth/presentation/widgets/build_auth_app_bar.dart';
import 'package:flutter_tdd/features/auth/presentation/widgets/build_pin_field.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../core/localization/localization_methods.dart';
import '../../widgets/build_header_title.dart';
import 'widgets/active_account_widgets_imports.dart';

part 'active_account.dart';
part 'active_account_data.dart';
