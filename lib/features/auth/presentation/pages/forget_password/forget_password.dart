part of 'forget_password_imports.dart';

class ForgetPassword extends StatefulWidget {
  final ProfileData profileData;

  const ForgetPassword({Key? key, required this.profileData}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  final ForgetPasswordData forgetPasswordData = ForgetPasswordData();

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: BuildAuthAppBar(
        title: "Forget Password",
        textSize: device.isTablet?7.sp:11.sp,
        onBack: () => AutoRouter.of(context).pop(),
        // onBack: () => widget.profileData.pagesCubit.onUpdateData(ProfilePages.login),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: ListView(
          padding: EdgeInsets.symmetric(
            horizontal: device.isTablet ? 10.sp : 15.w,
            vertical: 20.h,
          ),
          children: [
            BuildHeaderLogo(),
            SizedBox(height: 30),
            BuildForgetForm(forgetPasswordData: forgetPasswordData),
            BuildForgetPasswordButton(
              forgetPasswordData: forgetPasswordData,
              profileData: widget.profileData,
            ),
          ],
        ),
      ),
    );
  }
}
