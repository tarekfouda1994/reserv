import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/forget_password/widgets/forget_password_widgets_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/widgets/build_auth_app_bar.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/profile/profile_imports.dart';

part 'forget_password.dart';
part 'forget_password_data.dart';
