part of 'forget_password_imports.dart';

class ForgetPasswordData {
  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController email = TextEditingController();

  Future<void> onSubmitForget(
    BuildContext context,
    ProfileData profileData,
  ) async {
    if (formKey.currentState!.validate()) {
      profileData.email = email.text;
      AutoRouter.of(context)
          .push(ConfirmationForgetPasswordRoute(profileData: profileData));
      // profileData.pagesCubit.onUpdateData(ProfilePages.confirmationEmail);
    }
  }
}
