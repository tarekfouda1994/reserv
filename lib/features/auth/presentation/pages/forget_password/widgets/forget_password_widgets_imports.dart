import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/forget_password/forget_password_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/profile/profile_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'build_forget_form.dart';
part 'build_forget_password_button.dart';
part 'build_header_logo.dart';
