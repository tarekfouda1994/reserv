part of 'forget_password_widgets_imports.dart';

class BuildForgetPasswordButton extends StatelessWidget {
  final ForgetPasswordData forgetPasswordData;
  final ProfileData profileData;

  const BuildForgetPasswordButton({
    Key? key,
    required this.forgetPasswordData,
    required this.profileData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return DefaultButton(
      title: tr("send_email"),
      onTap: () => forgetPasswordData.onSubmitForget(context ,profileData),
      color: MyColors.primary,
      textColor: MyColors.white,
      borderRadius: BorderRadius.circular(40.r),
      margin: EdgeInsets.only(top: 30.h),
      fontSize: device.isTablet ? 8.sp : 12.sp,
      height: 35.h,
    );
  }
}
