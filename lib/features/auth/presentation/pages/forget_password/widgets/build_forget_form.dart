part of 'forget_password_widgets_imports.dart';

class BuildForgetForm extends StatelessWidget {
  final ForgetPasswordData forgetPasswordData;

  const BuildForgetForm({Key? key, required this.forgetPasswordData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: forgetPasswordData.formKey,
      child: CustomTextField(
        controller: forgetPasswordData.email,
        fieldTypes: FieldTypes.normal,
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        type: TextInputType.emailAddress,
        action: TextInputAction.next,
        validate: (value) => value?.validateEmail(),
        label: tr("email"),
        suffixIcon: BuildSuffixIcon(asset: Res.envelope_input),
      ),
    );
  }
}
