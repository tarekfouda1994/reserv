part of 'forget_password_widgets_imports.dart';

class BuildHeaderLogo extends StatelessWidget {
  const BuildHeaderLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          SvgPicture.asset(
            Res.forgot_password,
            height: 100.r,
            width: 100.r,
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.h),
            child: MyText(
              title: tr("forget_pass"),
              color: MyColors.primary,
              size: device.isTablet ? 8.sp : 12.sp,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.h, bottom: 40.h),
            child: MyText(
              title: tr("enter_email_to_get_Pass"),
              color: MyColors.blackOpacity,
              size: device.isTablet ? 8.sp : 12.sp,
              alien: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
