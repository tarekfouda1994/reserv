part of 'sent_success_widgets_imports.dart';

class BuildLoginButton extends StatelessWidget {
  final ProfileData profileData;

  const BuildLoginButton({
    Key? key,
    required this.profileData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return DefaultButton(
      title: tr("login"),
      // onTap: () => profileData.pagesCubit.onUpdateData(ProfilePages.login),
      onTap: () => AutoRouter.of(context).push(LoginRoute(profileData: profileData)),
      color: MyColors.primary,
      textColor: MyColors.white,
      borderRadius: BorderRadius.circular(40.r),
      margin: EdgeInsets.symmetric(vertical: 16.r,horizontal: 40.r),
      fontSize: device.isTablet ? 8.sp : 12.sp,
      height: 35.h,
    );
  }
}
