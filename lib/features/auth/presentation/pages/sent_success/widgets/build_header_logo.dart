part of 'sent_success_widgets_imports.dart';

class BuildHeaderLogo extends StatelessWidget {
  final ProfileData profileData;

  const BuildHeaderLogo({Key? key, required this.profileData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 80.h),
        SvgPicture.asset(Res.emailSent),
        Padding(
          padding: EdgeInsets.only(top: 8.h, bottom: 60.h),
          child: MyText(
              title: tr("email_sent_successfully"),
              color: MyColors.primary,
              size: 11.sp,
              fontWeight: FontWeight.bold),
        ),
        MyText(
            alien: TextAlign.center,
            title: tr("pleas_follow_instructions"),
            color: MyColors.blackOpacity,
            size: 11.sp),MyText(
            alien: TextAlign.center,
            title: "${tr("reserved_registered_email")}:",
            color: MyColors.blackOpacity,
            size: 11.sp),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0),
          child: MyText(
              title: profileData.email!,
              color: MyColors.black,
              size: 11.sp,
              fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
