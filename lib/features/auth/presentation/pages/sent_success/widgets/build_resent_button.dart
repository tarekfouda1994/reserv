part of 'sent_success_widgets_imports.dart';

class BuildResentButton extends StatelessWidget {
  final ProfileData profileData;
  final SentSuccessController sentSuccessController;

  const BuildResentButton(
      {Key? key,
      required this.profileData,
      required this.sentSuccessController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        MyText(
          title: tr("do_not_receive"),
          color: MyColors.blackOpacity,

          size: device.isTablet ? 8.sp : 12.sp,
        ),
        SizedBox(width: 4.w),
        InkWell(
          onTap: () => sentSuccessController.onSubmitSend(profileData),
          child: MyText(

            title: tr("resend"),
            color: MyColors.primary,
            size: device.isTablet ? 8.sp : 12.sp,
          ),
        ),
      ],
    );
  }
}
