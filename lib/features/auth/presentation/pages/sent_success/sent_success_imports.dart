import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/loading_helper.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/set_forget_pass.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/profile/profile_imports.dart';

import 'widgets/sent_success_widgets_imports.dart';

part 'sent_success.dart';
part 'sent_success_controller.dart';
