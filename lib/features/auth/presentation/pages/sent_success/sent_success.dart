part of 'sent_success_imports.dart';

class SentSuccess extends StatefulWidget {
  final ProfileData profileData;

  const SentSuccess({Key? key, required this.profileData}) : super(key: key);

  @override
  _SentSuccessState createState() => _SentSuccessState();
}

class _SentSuccessState extends State<SentSuccess> {
  SentSuccessController sentSuccessController = SentSuccessController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: tr("Email_Sent"),
        onBack: ()=> AutoRouter.of(context).pop(),
        // onBack: () => widget.profileData.pagesCubit
        //     .onUpdateData(ProfilePages.confirmationEmail),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: ListView(
          children: [
            BuildHeaderLogo(profileData: widget.profileData),
            BuildLoginButton(profileData: widget.profileData),
            BuildResentButton(
                profileData: widget.profileData,
                sentSuccessController: sentSuccessController)
          ],
        ),
      ),
    );
  }
}
