part of 'sent_success_imports.dart';

class SentSuccessController {

  onSubmitSend(ProfileData profileData) async {
     getIt<LoadingHelper>().showLoadingDialog();
    var result = await ForgetPass()(profileData.email!);
    if (result != null) {
      getIt<LoadingHelper>().dismissDialog();
    }
  }
}
