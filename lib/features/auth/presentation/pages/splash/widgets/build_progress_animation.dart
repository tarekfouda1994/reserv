part of 'splash_widgets_imports.dart';

class BuildProgressAnimation extends StatelessWidget {
  final SplashData splashData;

  const BuildProgressAnimation({Key? key, required this.splashData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return TweenAnimationBuilder<double>(
      tween: Tween<double>(begin: 0, end: 1),
      duration: const Duration(milliseconds: 1500),
      onEnd: () {
        splashData.checkUser(context);
      },
      builder: (BuildContext context, double value, Widget? child) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              MyText(
                title: getLocalizedValueText(context, value),
                color: MyColors.white,
                size: device.isTablet ? 8.sp : 12.sp,
              ),
              SizedBox(height: 2),
              ClipRRect(
                borderRadius: BorderRadius.circular(5.r),
                child: LinearProgressIndicator(
                  value: value,
                  color: MyColors.white,
                  backgroundColor: Colors.white.withOpacity(.2),
                  minHeight: 4,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  String getLocalizedValueText(BuildContext context, double value) =>
      getIt<Utilities>().convertNumToAr(context: context, value: "${(value * 100).round()}%");
}
