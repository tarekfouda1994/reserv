part of 'splash_widgets_imports.dart';

class BuildFooterContent extends StatelessWidget {
  final SplashData splashData;

  const BuildFooterContent({Key? key, required this.splashData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: device.isTablet ? 33.sp : 40.h,
        horizontal: device.isTablet ? 8.sp : 10.w,
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                  title: tr("smoothest_and_booking"),
                  color: MyColors.white,
                  size: device.isTablet ? 16.sp : 22.sp,
                  letterSpace: device.locale == "en" ? 1.sp : .4,
                ),
                SizedBox(
                  width: 100.w,
                  height: 15.h,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30.r),
                    child: Divider(
                      thickness: 4,
                      color: MyColors.secondary,
                    ),
                  ),
                ),
                // SizedBox(height: 10.h),
                MyText(
                  title: tr("get_glow"),
                  color: MyColors.secondary,
                  size: device.isTablet ? 10.sp : 12.sp,
                ),
              ],
            ),
          ),
          Spacer(),
          BuildProgressAnimation(splashData: splashData),
        ],
      ),
    );
  }
}
