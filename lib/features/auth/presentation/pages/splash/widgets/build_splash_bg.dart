part of 'splash_widgets_imports.dart';

class buildSplashBg extends StatelessWidget {
  final String image;
  final Widget child;
  final double heightPercent;

  const buildSplashBg(
      {Key? key, required this.image, required this.child, required this.heightPercent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * heightPercent,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage(image), fit: BoxFit.fill),
      ),
      child: child,
    );
  }
}
