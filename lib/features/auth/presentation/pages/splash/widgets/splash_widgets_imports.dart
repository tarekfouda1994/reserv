import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/splash/splash_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

part 'build_footer_content.dart';
part 'build_header_content.dart';
part 'build_progress_animation.dart';
part 'build_splash_bg.dart';
part 'build_splash_image.dart';
