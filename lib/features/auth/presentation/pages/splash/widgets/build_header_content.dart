part of 'splash_widgets_imports.dart';

class BuildHeaderContent extends StatelessWidget {
  const BuildHeaderContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        BuildSplashImage(),
      ],
    );
  }
}
