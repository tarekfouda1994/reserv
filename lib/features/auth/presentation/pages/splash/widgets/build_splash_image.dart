part of 'splash_widgets_imports.dart';

class BuildSplashImage extends StatelessWidget {
  const BuildSplashImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      Res.splash,
      height: 300.h,
    );
  }
}
