part of 'splash_imports.dart';

class SplashData {
  late SharedPreferences preferences;

  void manipulateSaveData(BuildContext context) async {
    var device = context.read<DeviceCubit>().state.model;
    retrieveLangValue(context);
    InitCustomPackages.instance.initCustomWidgets(
      language: device.locale.languageCode,
      isTablet: device.isTablet,
    );
    preferences = await SharedPreferences.getInstance();
    getIt<LoadingHelper>().dismissDialog();
  }

  checkUser(BuildContext context) {
    var userStr = preferences.getString("user");
    var cubit = context.read<DeviceCubit>();
    if (userStr != null) {
      cubit.updateUserAuth(true);
      var user = UserModel.fromJson(json.decode(userStr));
      context.read<UserCubit>().onUpdateUserData(user);
      GlobalState.instance.set("token", user.token);
      if (user.phoneNumber.isEmpty) {
        AutoRouter.of(context).push(EditProfileRoute(notCompletedData: true));
        Future.delayed(
          Duration(milliseconds: 400),
          () {
            CustomToast.showSimpleToast(
              title: tr("Warning_Toast"),
              msg: tr("completeYourData"),
              type: ToastType.warning,
            );
          },
        );
        return;
      }
    } else {
      cubit.updateUserAuth(false);
    }
    AutoRouter.of(context).push(HomeRoute());
  }

  retrieveLangValue(BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    String value = prefs.getString("lang") ?? "";
    if (value.isNotEmpty) {
      if (value == "en") {
        context.read<DeviceCubit>().updateLanguage(Locale('en', 'US'));
      } else {
        context.read<DeviceCubit>().updateLanguage(Locale('ar', 'EG'));
      }
    }
  }
}
