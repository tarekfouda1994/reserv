part of 'splash_imports.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final SplashData splashData = SplashData();

  @override
  void initState() {
    splashData.manipulateSaveData(context);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    precacheImage(const AssetImage(Res.splash_header), context);
    precacheImage(const AssetImage(Res.splash_footer), context);
    precacheImage(const AssetImage(Res.splash), context);
    precacheImage(const AssetImage(Res.markerActive), context);
    precacheImage(const AssetImage(Res.markerActiveBg), context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var device  = context.read<DeviceCubit>().state.model;
    InitCustomPackages.instance.initCustomWidgets(
      language: device.locale.languageCode,
      isTablet: device.isTablet,
    );
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: MediaQuery.of(context).size.height * .08,
              child: SvgPicture.asset(
                Res.logo,
                fit: BoxFit.fill,
                width: 130.w,
                height: 130.h,
              ),
            ),
            Positioned(
              top: 0,
              child: buildSplashBg(
                heightPercent: .74,
                image: Res.splash_header,
                child: BuildHeaderContent(),
              ),
            ),
            Positioned(
              bottom: 0,
              child: buildSplashBg(
                heightPercent: .341,
                image: Res.splash_footer,
                child: BuildFooterContent(splashData: splashData),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
