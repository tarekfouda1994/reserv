import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/external_login_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/register_params.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/http/generic_http/api_names.dart';
import '../../../../core/http/generic_http/generic_http.dart';
import '../../../../core/http/models/http_request_model.dart';
import '../../domain/entities/login_params.dart';
import '../model/countries_item_model/countries_item_model.dart';
import '../model/login_model/login_model.dart';
import 'auth_data_source.dart';

@Injectable(as: AuthDataSource)
class AuthDataSourceImpl extends AuthDataSource {
  @override
  Future<Either<Failure, LoginModel>> userLogin(LoginParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.login,
      requestBody: params.toJson(),
      requestMethod: RequestMethod.post,
      showLoader: false,
      refresh: true,
      responseType: ResType.model,
      responseKey: (data) => data,
      toJsonFunc: (json) {
        return LoginModel.fromJson(json);
      },
      errorFunc: (data) => data["title"],
    );
    return await GenericHttpImpl<LoginModel>()(model);
  }

  @override
  Future<Either<Failure, dynamic>> registerUser(RegisterPrams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.register,
      requestBody: params.toJson(),
      requestMethod: RequestMethod.post,
      showLoader: false,
      responseType: ResType.type,
      errorFunc: (data) => data,
    );
    return await GenericHttpImpl<dynamic>()(model);
  }

  @override
  Future<Either<Failure, bool>> forgetPass(String email) async {
    var param = "?email=$email";

    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.forgetPassword +param,
      requestMethod: RequestMethod.post,
      responseType: ResType.type,
      responseKey: (data) => data["isSuccess"],
      showLoader: false,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, String>> getEncryptedValue(int val) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getEncrypted,
      requestBody: {"val": val},
      requestMethod: RequestMethod.post,
      responseType: ResType.type,
      responseKey: (data) => data,
      showLoader: false,
    );
    return await GenericHttpImpl<String>()(model);
  }

  @override
  Future<Either<Failure, List<CountriesItemModel>>> getAllCountries(CountriesParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllCountries,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: false,
      requestBody: params.toJson(),
      responseKey: (data) => data["itemsList"],
      toJsonFunc: (json) => List<CountriesItemModel>.from(
        json.map((e) => CountriesItemModel.fromJson(e)),
      ),
    );
    return await GenericHttpImpl<List<CountriesItemModel>>()(model);
  }

  @override
  Future<Either<Failure, LoginModel>> externalLogin(ExternalLoginParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.externalLogin,
      requestBody: params.toJson(),
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      responseKey: (data) => data,
      toJsonFunc: (json) => LoginModel.fromJson(json),
      showLoader: true,
      errorFunc: (data) => data["title"],
    );
    return await GenericHttpImpl<LoginModel>()(model);
  }
}
