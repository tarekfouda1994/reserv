import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/login_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/register_params.dart';

import '../../domain/entities/external_login_params.dart';
import '../model/countries_item_model/countries_item_model.dart';
import '../model/login_model/login_model.dart';

abstract class AuthDataSource {
  Future<Either<Failure, LoginModel>> userLogin(LoginParams params);

  Future<Either<Failure, dynamic>> registerUser(RegisterPrams params);

  Future<Either<Failure, bool>> forgetPass(String email);

  Future<Either<Failure, LoginModel>> externalLogin(ExternalLoginParams params);

  Future<Either<Failure, String>> getEncryptedValue(int val);

  Future<Either<Failure, List<CountriesItemModel>>> getAllCountries(CountriesParams params);
}
