import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/external_login_params.dart';
import 'package:flutter_tdd/features/auth/domain/entities/register_params.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/helpers/di.dart';
import '../../domain/entities/login_params.dart';
import '../../domain/repositories/auth_repository.dart';
import '../data_sources/auth_data_source.dart';
import '../model/countries_item_model/countries_item_model.dart';
import '../model/login_model/login_model.dart';

@Injectable(as: AuthRepository)
class AuthRepositoryImpl extends AuthRepository {
  @override
  Future<Either<Failure, LoginModel>> userLogin(LoginParams params) async {
    return await getIt<AuthDataSource>().userLogin(params);
  }

  @override
  Future<Either<Failure, dynamic>> registerUser(RegisterPrams params) async {
    return await getIt<AuthDataSource>().registerUser(params);
  }

  @override
  Future<Either<Failure, bool>> forgetPass(String email) async {
    return await getIt<AuthDataSource>().forgetPass(email);
  }

  @override
  Future<Either<Failure, String>> getEncryptedValue(int val) async {
    return await getIt<AuthDataSource>().getEncryptedValue(val);
  }

  @override
  Future<Either<Failure, List<CountriesItemModel>>> getAllCountries(CountriesParams params) async {
    return await getIt<AuthDataSource>().getAllCountries(params);
  }

  @override
  Future<Either<Failure, LoginModel>> externalLogin(ExternalLoginParams params) async {
    return await getIt<AuthDataSource>().externalLogin(params);
  }
}
