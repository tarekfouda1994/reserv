// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'countries_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CountriesItemModel _$CountriesItemModelFromJson(Map<String, dynamic> json) {
  return _CountriesItemModel.fromJson(json);
}

/// @nodoc
mixin _$CountriesItemModel {
  String get nationalityArabicName => throw _privateConstructorUsedError;
  set nationalityArabicName(String value) => throw _privateConstructorUsedError;
  String get nationalityEnglishName => throw _privateConstructorUsedError;
  set nationalityEnglishName(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get calingCode => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set calingCode(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get flagImage => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set flagImage(String value) => throw _privateConstructorUsedError;
  String get encryptId => throw _privateConstructorUsedError;
  set encryptId(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get isoCode3 => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set isoCode3(String value) => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  set id(int value) => throw _privateConstructorUsedError;
  String get arabicName => throw _privateConstructorUsedError;
  set arabicName(String value) => throw _privateConstructorUsedError;
  String get englishName => throw _privateConstructorUsedError;
  set englishName(String value) => throw _privateConstructorUsedError;
  bool? get isSelected => throw _privateConstructorUsedError;
  set isSelected(bool? value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CountriesItemModelCopyWith<CountriesItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CountriesItemModelCopyWith<$Res> {
  factory $CountriesItemModelCopyWith(
          CountriesItemModel value, $Res Function(CountriesItemModel) then) =
      _$CountriesItemModelCopyWithImpl<$Res>;
  $Res call(
      {String nationalityArabicName,
      String nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true) String calingCode,
      @JsonKey(defaultValue: "", nullable: true) String flagImage,
      String encryptId,
      @JsonKey(defaultValue: "", nullable: true) String isoCode3,
      int id,
      String arabicName,
      String englishName,
      bool? isSelected});
}

/// @nodoc
class _$CountriesItemModelCopyWithImpl<$Res>
    implements $CountriesItemModelCopyWith<$Res> {
  _$CountriesItemModelCopyWithImpl(this._value, this._then);

  final CountriesItemModel _value;
  // ignore: unused_field
  final $Res Function(CountriesItemModel) _then;

  @override
  $Res call({
    Object? nationalityArabicName = freezed,
    Object? nationalityEnglishName = freezed,
    Object? calingCode = freezed,
    Object? flagImage = freezed,
    Object? encryptId = freezed,
    Object? isoCode3 = freezed,
    Object? id = freezed,
    Object? arabicName = freezed,
    Object? englishName = freezed,
    Object? isSelected = freezed,
  }) {
    return _then(_value.copyWith(
      nationalityArabicName: nationalityArabicName == freezed
          ? _value.nationalityArabicName
          : nationalityArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityEnglishName: nationalityEnglishName == freezed
          ? _value.nationalityEnglishName
          : nationalityEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      calingCode: calingCode == freezed
          ? _value.calingCode
          : calingCode // ignore: cast_nullable_to_non_nullable
              as String,
      flagImage: flagImage == freezed
          ? _value.flagImage
          : flagImage // ignore: cast_nullable_to_non_nullable
              as String,
      encryptId: encryptId == freezed
          ? _value.encryptId
          : encryptId // ignore: cast_nullable_to_non_nullable
              as String,
      isoCode3: isoCode3 == freezed
          ? _value.isoCode3
          : isoCode3 // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      isSelected: isSelected == freezed
          ? _value.isSelected
          : isSelected // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$$_CountriesItemModelCopyWith<$Res>
    implements $CountriesItemModelCopyWith<$Res> {
  factory _$$_CountriesItemModelCopyWith(_$_CountriesItemModel value,
          $Res Function(_$_CountriesItemModel) then) =
      __$$_CountriesItemModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String nationalityArabicName,
      String nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true) String calingCode,
      @JsonKey(defaultValue: "", nullable: true) String flagImage,
      String encryptId,
      @JsonKey(defaultValue: "", nullable: true) String isoCode3,
      int id,
      String arabicName,
      String englishName,
      bool? isSelected});
}

/// @nodoc
class __$$_CountriesItemModelCopyWithImpl<$Res>
    extends _$CountriesItemModelCopyWithImpl<$Res>
    implements _$$_CountriesItemModelCopyWith<$Res> {
  __$$_CountriesItemModelCopyWithImpl(
      _$_CountriesItemModel _value, $Res Function(_$_CountriesItemModel) _then)
      : super(_value, (v) => _then(v as _$_CountriesItemModel));

  @override
  _$_CountriesItemModel get _value => super._value as _$_CountriesItemModel;

  @override
  $Res call({
    Object? nationalityArabicName = freezed,
    Object? nationalityEnglishName = freezed,
    Object? calingCode = freezed,
    Object? flagImage = freezed,
    Object? encryptId = freezed,
    Object? isoCode3 = freezed,
    Object? id = freezed,
    Object? arabicName = freezed,
    Object? englishName = freezed,
    Object? isSelected = freezed,
  }) {
    return _then(_$_CountriesItemModel(
      nationalityArabicName: nationalityArabicName == freezed
          ? _value.nationalityArabicName
          : nationalityArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityEnglishName: nationalityEnglishName == freezed
          ? _value.nationalityEnglishName
          : nationalityEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      calingCode: calingCode == freezed
          ? _value.calingCode
          : calingCode // ignore: cast_nullable_to_non_nullable
              as String,
      flagImage: flagImage == freezed
          ? _value.flagImage
          : flagImage // ignore: cast_nullable_to_non_nullable
              as String,
      encryptId: encryptId == freezed
          ? _value.encryptId
          : encryptId // ignore: cast_nullable_to_non_nullable
              as String,
      isoCode3: isoCode3 == freezed
          ? _value.isoCode3
          : isoCode3 // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      isSelected: isSelected == freezed
          ? _value.isSelected
          : isSelected // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_CountriesItemModel extends _CountriesItemModel {
  _$_CountriesItemModel(
      {required this.nationalityArabicName,
      required this.nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true) required this.calingCode,
      @JsonKey(defaultValue: "", nullable: true) required this.flagImage,
      required this.encryptId,
      @JsonKey(defaultValue: "", nullable: true) required this.isoCode3,
      required this.id,
      required this.arabicName,
      required this.englishName,
      required this.isSelected})
      : super._();

  factory _$_CountriesItemModel.fromJson(Map<String, dynamic> json) =>
      _$$_CountriesItemModelFromJson(json);

  @override
  String nationalityArabicName;
  @override
  String nationalityEnglishName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String calingCode;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String flagImage;
  @override
  String encryptId;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String isoCode3;
  @override
  int id;
  @override
  String arabicName;
  @override
  String englishName;
  @override
  bool? isSelected;

  @override
  String toString() {
    return 'CountriesItemModel(nationalityArabicName: $nationalityArabicName, nationalityEnglishName: $nationalityEnglishName, calingCode: $calingCode, flagImage: $flagImage, encryptId: $encryptId, isoCode3: $isoCode3, id: $id, arabicName: $arabicName, englishName: $englishName, isSelected: $isSelected)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_CountriesItemModelCopyWith<_$_CountriesItemModel> get copyWith =>
      __$$_CountriesItemModelCopyWithImpl<_$_CountriesItemModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CountriesItemModelToJson(
      this,
    );
  }
}

abstract class _CountriesItemModel extends CountriesItemModel {
  factory _CountriesItemModel(
      {required String nationalityArabicName,
      required String nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true) required String calingCode,
      @JsonKey(defaultValue: "", nullable: true) required String flagImage,
      required String encryptId,
      @JsonKey(defaultValue: "", nullable: true) required String isoCode3,
      required int id,
      required String arabicName,
      required String englishName,
      required bool? isSelected}) = _$_CountriesItemModel;
  _CountriesItemModel._() : super._();

  factory _CountriesItemModel.fromJson(Map<String, dynamic> json) =
      _$_CountriesItemModel.fromJson;

  @override
  String get nationalityArabicName;
  set nationalityArabicName(String value);
  @override
  String get nationalityEnglishName;
  set nationalityEnglishName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get calingCode;
  @JsonKey(defaultValue: "", nullable: true)
  set calingCode(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get flagImage;
  @JsonKey(defaultValue: "", nullable: true)
  set flagImage(String value);
  @override
  String get encryptId;
  set encryptId(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get isoCode3;
  @JsonKey(defaultValue: "", nullable: true)
  set isoCode3(String value);
  @override
  int get id;
  set id(int value);
  @override
  String get arabicName;
  set arabicName(String value);
  @override
  String get englishName;
  set englishName(String value);
  @override
  bool? get isSelected;
  set isSelected(bool? value);
  @override
  @JsonKey(ignore: true)
  _$$_CountriesItemModelCopyWith<_$_CountriesItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}
