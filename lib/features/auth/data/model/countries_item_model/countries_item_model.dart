import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'countries_item_model.freezed.dart';
part 'countries_item_model.g.dart';

@unfreezed
class CountriesItemModel with _$CountriesItemModel {
  CountriesItemModel._();

  @JsonSerializable(explicitToJson: true)
  factory CountriesItemModel({
    required String nationalityArabicName,
    required String nationalityEnglishName,
    @JsonKey(defaultValue: "", nullable: true) required String calingCode,
    @JsonKey(defaultValue: "", nullable: true) required String flagImage,
    required String encryptId,
    @JsonKey(defaultValue: "", nullable: true) required String isoCode3,
    required int id,
    required String arabicName,
    required String englishName,
    required bool? isSelected,
  }) = _CountriesItemModel;

  factory CountriesItemModel.fromJson(Map<String, dynamic> json) =>
      _$CountriesItemModelFromJson(json);

  String getNationName() {
    return getIt<Utilities>().getLocalizedValue(nationalityArabicName, nationalityEnglishName);
  }

  String getCountryName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}
