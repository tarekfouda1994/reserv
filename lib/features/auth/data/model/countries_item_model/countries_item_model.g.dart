// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'countries_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CountriesItemModel _$$_CountriesItemModelFromJson(
        Map<String, dynamic> json) =>
    _$_CountriesItemModel(
      nationalityArabicName: json['nationalityArabicName'] as String,
      nationalityEnglishName: json['nationalityEnglishName'] as String,
      calingCode: json['calingCode'] as String? ?? '',
      flagImage: json['flagImage'] as String? ?? '',
      encryptId: json['encryptId'] as String,
      isoCode3: json['isoCode3'] as String? ?? '',
      id: json['id'] as int,
      arabicName: json['arabicName'] as String,
      englishName: json['englishName'] as String,
      isSelected: json['isSelected'] as bool?,
    );

Map<String, dynamic> _$$_CountriesItemModelToJson(
        _$_CountriesItemModel instance) =>
    <String, dynamic>{
      'nationalityArabicName': instance.nationalityArabicName,
      'nationalityEnglishName': instance.nationalityEnglishName,
      'calingCode': instance.calingCode,
      'flagImage': instance.flagImage,
      'encryptId': instance.encryptId,
      'isoCode3': instance.isoCode3,
      'id': instance.id,
      'arabicName': instance.arabicName,
      'englishName': instance.englishName,
      'isSelected': instance.isSelected,
    };
