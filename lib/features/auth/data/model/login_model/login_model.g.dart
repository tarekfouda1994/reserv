// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LoginModel _$$_LoginModelFromJson(Map<String, dynamic> json) =>
    _$_LoginModel(
      userId: json['userId'] as String,
      businessId: json['businessId'] as String?,
      businessArabicName: json['businessArabicName'] as String?,
      businessEnglishName: json['businessEnglishName'] as String?,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      email: json['email'] as String,
      contactNumber: json['contactNumber'] as String?,
      firstTimeLogin: json['firstTimeLogin'] as bool,
      userTypeId: json['userTypeId'] as int,
      authenticationToken: json['authenticationToken'] as String,
      refreshToken: json['refreshToken'] as String,
      isAdmin: json['isAdmin'] as bool,
      userPermissions: json['userPermissions'] as List<dynamic>,
    );

Map<String, dynamic> _$$_LoginModelToJson(_$_LoginModel instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'businessId': instance.businessId,
      'businessArabicName': instance.businessArabicName,
      'businessEnglishName': instance.businessEnglishName,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'contactNumber': instance.contactNumber,
      'firstTimeLogin': instance.firstTimeLogin,
      'userTypeId': instance.userTypeId,
      'authenticationToken': instance.authenticationToken,
      'refreshToken': instance.refreshToken,
      'isAdmin': instance.isAdmin,
      'userPermissions': instance.userPermissions,
    };
