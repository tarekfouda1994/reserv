// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'login_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LoginModel _$LoginModelFromJson(Map<String, dynamic> json) {
  return _LoginModel.fromJson(json);
}

/// @nodoc
mixin _$LoginModel {
  String get userId => throw _privateConstructorUsedError;
  String? get businessId => throw _privateConstructorUsedError;
  String? get businessArabicName => throw _privateConstructorUsedError;
  String? get businessEnglishName => throw _privateConstructorUsedError;
  String get firstName => throw _privateConstructorUsedError;
  String get lastName => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String? get contactNumber => throw _privateConstructorUsedError;
  bool get firstTimeLogin => throw _privateConstructorUsedError;
  int get userTypeId => throw _privateConstructorUsedError;
  String get authenticationToken => throw _privateConstructorUsedError;
  String get refreshToken => throw _privateConstructorUsedError;
  bool get isAdmin => throw _privateConstructorUsedError;
  List<dynamic> get userPermissions => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginModelCopyWith<LoginModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginModelCopyWith<$Res> {
  factory $LoginModelCopyWith(
          LoginModel value, $Res Function(LoginModel) then) =
      _$LoginModelCopyWithImpl<$Res>;
  $Res call(
      {String userId,
      String? businessId,
      String? businessArabicName,
      String? businessEnglishName,
      String firstName,
      String lastName,
      String email,
      String? contactNumber,
      bool firstTimeLogin,
      int userTypeId,
      String authenticationToken,
      String refreshToken,
      bool isAdmin,
      List<dynamic> userPermissions});
}

/// @nodoc
class _$LoginModelCopyWithImpl<$Res> implements $LoginModelCopyWith<$Res> {
  _$LoginModelCopyWithImpl(this._value, this._then);

  final LoginModel _value;
  // ignore: unused_field
  final $Res Function(LoginModel) _then;

  @override
  $Res call({
    Object? userId = freezed,
    Object? businessId = freezed,
    Object? businessArabicName = freezed,
    Object? businessEnglishName = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? email = freezed,
    Object? contactNumber = freezed,
    Object? firstTimeLogin = freezed,
    Object? userTypeId = freezed,
    Object? authenticationToken = freezed,
    Object? refreshToken = freezed,
    Object? isAdmin = freezed,
    Object? userPermissions = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      businessId: businessId == freezed
          ? _value.businessId
          : businessId // ignore: cast_nullable_to_non_nullable
              as String?,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      firstTimeLogin: firstTimeLogin == freezed
          ? _value.firstTimeLogin
          : firstTimeLogin // ignore: cast_nullable_to_non_nullable
              as bool,
      userTypeId: userTypeId == freezed
          ? _value.userTypeId
          : userTypeId // ignore: cast_nullable_to_non_nullable
              as int,
      authenticationToken: authenticationToken == freezed
          ? _value.authenticationToken
          : authenticationToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: refreshToken == freezed
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      isAdmin: isAdmin == freezed
          ? _value.isAdmin
          : isAdmin // ignore: cast_nullable_to_non_nullable
              as bool,
      userPermissions: userPermissions == freezed
          ? _value.userPermissions
          : userPermissions // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc
abstract class _$$_LoginModelCopyWith<$Res>
    implements $LoginModelCopyWith<$Res> {
  factory _$$_LoginModelCopyWith(
          _$_LoginModel value, $Res Function(_$_LoginModel) then) =
      __$$_LoginModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String userId,
      String? businessId,
      String? businessArabicName,
      String? businessEnglishName,
      String firstName,
      String lastName,
      String email,
      String? contactNumber,
      bool firstTimeLogin,
      int userTypeId,
      String authenticationToken,
      String refreshToken,
      bool isAdmin,
      List<dynamic> userPermissions});
}

/// @nodoc
class __$$_LoginModelCopyWithImpl<$Res> extends _$LoginModelCopyWithImpl<$Res>
    implements _$$_LoginModelCopyWith<$Res> {
  __$$_LoginModelCopyWithImpl(
      _$_LoginModel _value, $Res Function(_$_LoginModel) _then)
      : super(_value, (v) => _then(v as _$_LoginModel));

  @override
  _$_LoginModel get _value => super._value as _$_LoginModel;

  @override
  $Res call({
    Object? userId = freezed,
    Object? businessId = freezed,
    Object? businessArabicName = freezed,
    Object? businessEnglishName = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? email = freezed,
    Object? contactNumber = freezed,
    Object? firstTimeLogin = freezed,
    Object? userTypeId = freezed,
    Object? authenticationToken = freezed,
    Object? refreshToken = freezed,
    Object? isAdmin = freezed,
    Object? userPermissions = freezed,
  }) {
    return _then(_$_LoginModel(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      businessId: businessId == freezed
          ? _value.businessId
          : businessId // ignore: cast_nullable_to_non_nullable
              as String?,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      firstTimeLogin: firstTimeLogin == freezed
          ? _value.firstTimeLogin
          : firstTimeLogin // ignore: cast_nullable_to_non_nullable
              as bool,
      userTypeId: userTypeId == freezed
          ? _value.userTypeId
          : userTypeId // ignore: cast_nullable_to_non_nullable
              as int,
      authenticationToken: authenticationToken == freezed
          ? _value.authenticationToken
          : authenticationToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: refreshToken == freezed
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      isAdmin: isAdmin == freezed
          ? _value.isAdmin
          : isAdmin // ignore: cast_nullable_to_non_nullable
              as bool,
      userPermissions: userPermissions == freezed
          ? _value._userPermissions
          : userPermissions // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_LoginModel implements _LoginModel {
  _$_LoginModel(
      {required this.userId,
      this.businessId,
      this.businessArabicName,
      this.businessEnglishName,
      required this.firstName,
      required this.lastName,
      required this.email,
      this.contactNumber,
      required this.firstTimeLogin,
      required this.userTypeId,
      required this.authenticationToken,
      required this.refreshToken,
      required this.isAdmin,
      required final List<dynamic> userPermissions})
      : _userPermissions = userPermissions;

  factory _$_LoginModel.fromJson(Map<String, dynamic> json) =>
      _$$_LoginModelFromJson(json);

  @override
  final String userId;
  @override
  final String? businessId;
  @override
  final String? businessArabicName;
  @override
  final String? businessEnglishName;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String email;
  @override
  final String? contactNumber;
  @override
  final bool firstTimeLogin;
  @override
  final int userTypeId;
  @override
  final String authenticationToken;
  @override
  final String refreshToken;
  @override
  final bool isAdmin;
  final List<dynamic> _userPermissions;
  @override
  List<dynamic> get userPermissions {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_userPermissions);
  }

  @override
  String toString() {
    return 'LoginModel(userId: $userId, businessId: $businessId, businessArabicName: $businessArabicName, businessEnglishName: $businessEnglishName, firstName: $firstName, lastName: $lastName, email: $email, contactNumber: $contactNumber, firstTimeLogin: $firstTimeLogin, userTypeId: $userTypeId, authenticationToken: $authenticationToken, refreshToken: $refreshToken, isAdmin: $isAdmin, userPermissions: $userPermissions)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LoginModel &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality()
                .equals(other.businessId, businessId) &&
            const DeepCollectionEquality()
                .equals(other.businessArabicName, businessArabicName) &&
            const DeepCollectionEquality()
                .equals(other.businessEnglishName, businessEnglishName) &&
            const DeepCollectionEquality().equals(other.firstName, firstName) &&
            const DeepCollectionEquality().equals(other.lastName, lastName) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality()
                .equals(other.contactNumber, contactNumber) &&
            const DeepCollectionEquality()
                .equals(other.firstTimeLogin, firstTimeLogin) &&
            const DeepCollectionEquality()
                .equals(other.userTypeId, userTypeId) &&
            const DeepCollectionEquality()
                .equals(other.authenticationToken, authenticationToken) &&
            const DeepCollectionEquality()
                .equals(other.refreshToken, refreshToken) &&
            const DeepCollectionEquality().equals(other.isAdmin, isAdmin) &&
            const DeepCollectionEquality()
                .equals(other._userPermissions, _userPermissions));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(userId),
      const DeepCollectionEquality().hash(businessId),
      const DeepCollectionEquality().hash(businessArabicName),
      const DeepCollectionEquality().hash(businessEnglishName),
      const DeepCollectionEquality().hash(firstName),
      const DeepCollectionEquality().hash(lastName),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(contactNumber),
      const DeepCollectionEquality().hash(firstTimeLogin),
      const DeepCollectionEquality().hash(userTypeId),
      const DeepCollectionEquality().hash(authenticationToken),
      const DeepCollectionEquality().hash(refreshToken),
      const DeepCollectionEquality().hash(isAdmin),
      const DeepCollectionEquality().hash(_userPermissions));

  @JsonKey(ignore: true)
  @override
  _$$_LoginModelCopyWith<_$_LoginModel> get copyWith =>
      __$$_LoginModelCopyWithImpl<_$_LoginModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LoginModelToJson(
      this,
    );
  }
}

abstract class _LoginModel implements LoginModel {
  factory _LoginModel(
      {required final String userId,
      final String? businessId,
      final String? businessArabicName,
      final String? businessEnglishName,
      required final String firstName,
      required final String lastName,
      required final String email,
      final String? contactNumber,
      required final bool firstTimeLogin,
      required final int userTypeId,
      required final String authenticationToken,
      required final String refreshToken,
      required final bool isAdmin,
      required final List<dynamic> userPermissions}) = _$_LoginModel;

  factory _LoginModel.fromJson(Map<String, dynamic> json) =
      _$_LoginModel.fromJson;

  @override
  String get userId;
  @override
  String? get businessId;
  @override
  String? get businessArabicName;
  @override
  String? get businessEnglishName;
  @override
  String get firstName;
  @override
  String get lastName;
  @override
  String get email;
  @override
  String? get contactNumber;
  @override
  bool get firstTimeLogin;
  @override
  int get userTypeId;
  @override
  String get authenticationToken;
  @override
  String get refreshToken;
  @override
  bool get isAdmin;
  @override
  List<dynamic> get userPermissions;
  @override
  @JsonKey(ignore: true)
  _$$_LoginModelCopyWith<_$_LoginModel> get copyWith =>
      throw _privateConstructorUsedError;
}
