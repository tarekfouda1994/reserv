import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_model.freezed.dart';
part 'login_model.g.dart';

@freezed
class LoginModel with _$LoginModel {
  @JsonSerializable(explicitToJson: true)
  factory LoginModel({
    required String userId,
    String? businessId,
    String? businessArabicName,
    String? businessEnglishName,
    required String firstName,
    required String lastName,
    required String email,
    String? contactNumber,
    required bool firstTimeLogin,
    required int userTypeId,
    required String authenticationToken,
    required String refreshToken,
    required bool isAdmin,
    required List userPermissions,
  }) = _LoginModel;

  factory LoginModel.fromJson(Map<String, dynamic> json) => _$LoginModelFromJson(json);
}
