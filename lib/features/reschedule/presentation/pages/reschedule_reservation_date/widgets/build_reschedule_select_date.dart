part of'reschedule_date_widgets_imports.dart';

class BuildRescheduleSelectDate extends StatelessWidget {
  final RescheduleAppointmentData rootData;
  final List<DateTime> selectedDates;

  const BuildRescheduleSelectDate({
    Key? key,
    required this.rootData,
    required this.selectedDates,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var services = rootData.rescheduleStaffData.servicesBloc.state.data;
    return Column(
      children: [
        BlocBuilder<GenericBloc<List<ServiceDateModel>>, GenericState<List<ServiceDateModel>>>(
          bloc: rootData.rescheduleReservationDateData.availableDatesCubit,
          builder: (context, state) {
            final List<DateTime> disableDates = state.data
                .where((element) => !element.isServiceAvailable)
                .map((e) => e.date)
                .toList();
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10).r,
                  child: MyText(
                    title: tr("select_date"),
                    color: MyColors.black,
                    size: device.isTablet ? 8.5.sp : 12.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16).r,
                  padding: EdgeInsets.symmetric(vertical: 15,horizontal: 8).r,
                  decoration: BoxDecoration(
                      color: MyColors.white,
                      borderRadius: BorderRadius.circular(15).r
                  ),
                  child: CustomCalender(
                    showRowTitle: false,
                    showIcons: true,
                    selectedDates: selectedDates,
                    disableDates: disableDates,
                    onSelect: (disable, date) => rootData.rescheduleReservationDateData.onSelectDate(disable ,date, services,rootData),
                  ),
                ),
              ],
            );
          },
        ),
      ],
    );
  }
}
