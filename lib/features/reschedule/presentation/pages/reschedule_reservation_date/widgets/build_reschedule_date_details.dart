part of "reschedule_date_widgets_imports.dart";

class BuildRescheduleDateDetails extends StatelessWidget {
  final RescheduleAppointmentData rootData;

  const BuildRescheduleDateDetails({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var services = rootData.rescheduleStaffData.servicesBloc.state.data;
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: MyColors.primary, width: 2),
        borderRadius: BorderRadius.circular(15).r,
        color: MyColors.headerBackGroundColor,
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20).r,
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(15).r,
              ),
            ),
            child: Row(
              children: [
                SvgPicture.asset(
                  Res.calendar_note,
                  height: device.isTablet ? 30.h : 40.h,
                  width: device.isTablet ? 30.w : 40.w,
                  fit: BoxFit.fill,
                ),
                SizedBox(width: 8.sm),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          SvgPicture.asset(Res.warning),
                          SizedBox(width: 8.sm),
                          MyText(
                            title: tr("together_reservation_unavailable"),
                            color: MyColors.infoColor,
                            size: device.isTablet ? 7.sp : 10.sp,
                          )
                        ],
                      ),
                      SizedBox(height: 8.sm),
                      MyText(
                        title: tr("select_these_service"),
                        color: MyColors.blackOpacity,
                        size: device.isTablet ? 6.sp : 8.sp,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15).r,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(
                  title: tr("Choose_different_date"),
                  color: MyColors.black,
                  size: device.isTablet ? 7.sp : 10.sp,
                  fontWeight: FontWeight.bold,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8).r,
                  child: MyText(
                    title: tr("Or"),
                    color: MyColors.blackOpacity,
                    size: device.isTablet ? 7.sp : 10.sp,
                  ),
                ),
                InkWell(
                  onTap: () => rootData.rescheduleReservationDateData
                      .onChangeReserveTogether(
                    rootData,
                    !rootData
                        .rescheduleReservationDateData.switchedCubit.state.data,
                    services,
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10).r,
                    margin: EdgeInsets.symmetric(horizontal: 30).r,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8).r,
                      color: MyColors.white,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          Res.switchIcon,
                        ),
                        SizedBox(width: 8.sm),
                        MyText(
                          title: tr("switch_manual"),
                          color: MyColors.primary,
                          size: device.isTablet ? 7.sp : 10.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
