part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleSwitchDate extends StatelessWidget {
  final RescheduleAppointmentData rootData;
  final DateTime? data;

  const BuildRescheduleSwitchDate({Key? key, required this.rootData, this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: rootData.rescheduleReservationDateData.switchedCubit,
      builder: (_, typeState) {
        return AnimatedSwitcher(
          duration: Duration(milliseconds: 500),
          reverseDuration: Duration(milliseconds: 500),
          child: typeState.data
              ? BuildRescheduleReserveTogether(rootData: rootData)
              : BuildRescheduleNotReserveTogetherView(
                  rootData: rootData,
                  dateTime: data,
                ),
        );
      },
    );
  }
}
