part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleNotReserveTogetherView extends StatelessWidget {
  final RescheduleAppointmentData rootData;
  final DateTime? dateTime;

  const BuildRescheduleNotReserveTogetherView({
    Key? key,
    required this.rootData,
    required this.dateTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
        GenericState<List<ServiceListModel>>>(
      bloc: rootData.rescheduleStaffData.servicesBloc,
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildAvailableTitle(),
            CustomCard(
              padding: EdgeInsets.symmetric(vertical: 10.h),
              border: Border.all(color: MyColors.primary, width: 1.6),
              child: Column(
                children: List.generate(state.data.length, (index) {
                  return BuildRescheduleAvailableTime(
                    index: index,
                    length: state.data.length,
                    serviceListModel: state.data[index],
                    dateTime: dateTime,
                    rootData: rootData,
                  );
                }),
              ),
            ),
          ],
        );
      },
    );
  }
}
