part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleAvailableTime extends StatelessWidget {
  final ServiceListModel serviceListModel;
  final DateTime? dateTime;
  final RescheduleAppointmentData rootData;
  final int index;
  final int length;

  const BuildRescheduleAvailableTime({
    Key? key,
    required this.serviceListModel,
    required this.dateTime,
    required this.rootData,
    required this.index,
    required this.length,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: MyText(
                      title: serviceListModel.getServiceName(),
                      color: MyColors.black,
                      fontWeight: FontWeight.bold,
                      size: device.isTablet ? 7.sp : 9.sp,
                    ),
                  ),
                  InkWell(
                    onTap: () => rootData.rescheduleReservationDateData
                        .bottomSheetSelectTime(
                      context,
                      serviceListModel,
                      rootData.rescheduleReservationDateData,
                      rootData,
                      dateTime,
                      index,
                    ),
                    child: Row(
                      children: [
                        Visibility(
                            visible:
                                serviceListModel.selectedTimeForService.isEmpty,
                            child: SvgPicture.asset(Res.ellipse)),
                        MyText(
                          title:
                              serviceListModel.selectedTimeForService.isNotEmpty
                                  ? tr("change_time")
                                  : " ${tr("select_time")}",
                          color: MyColors.primary,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 9.sp,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 3.h),
              Row(
                children: [
                  Icon(Icons.access_time_outlined,
                      color: MyColors.black,
                      size: device.isTablet ? 14.sp : 18.sp),
                  MyText(
                    title:getIt<Utilities>().convertNumToAr(context: context, value: " ${serviceListModel.selectedTimeForService}"),
                    color: MyColors.black,
                    size: device.isTablet ? 6.sp : 9.sp,
                  ),
                ],
              ),
            ],
          ),
        ),
        Visibility(
          visible: index != length - 1,
          child: Divider(height: 30),
        ),
      ],
    );
  }
}
