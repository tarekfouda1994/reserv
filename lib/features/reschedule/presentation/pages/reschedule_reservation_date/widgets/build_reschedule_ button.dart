part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleButton extends StatelessWidget {
  final RescheduleAppointmentData rootData;

  const BuildRescheduleButton({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: MyColors.white,
        boxShadow: [
          BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
            child: InkWell(
              onTap: () => rootData.rescheduleReservationDateData
                  .submitReschedule(context, rootData),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: device.isTablet ? 50.h : 40.h,
                decoration: BoxDecoration(
                  color: MyColors.primary,
                  borderRadius: BorderRadius.circular(40.r),
                ),
                padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      MyText(
                        title: tr("confirm_reschedule"),
                        color: MyColors.white,
                        size: device.isTablet ? 7.sp : 11.sp,
                      ),
                      SizedBox(width: 5),
                      Transform.rotate(
                        angle: device.locale == Locale('en', 'US') ? 0 : 3.16,
                        child: SvgPicture.asset(
                          Res.arrowForward,
                          height: device.locale == Locale('en', 'US')
                              ? null
                              : device.isTablet
                                  ? 9.sp
                                  : 15.sp,
                          color: MyColors.white,
                        ),
                      ),
                    ]),
              ),
            ),
          )
        ],
      ),
    );
  }
}
