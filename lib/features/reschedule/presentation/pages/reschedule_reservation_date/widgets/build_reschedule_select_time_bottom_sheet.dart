part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleSelectTimeBottomSheet extends StatefulWidget {
  final RescheduleAppointmentData rootData;
  final ServiceListModel serviceListModel;
  final DateTime? dateTime;
  final int indexItem;

  const BuildRescheduleSelectTimeBottomSheet(
      {Key? key,
      required this.rootData,
      required this.serviceListModel,
      this.dateTime,
      required this.indexItem})
      : super(key: key);

  @override
  State<BuildRescheduleSelectTimeBottomSheet> createState() =>
      _BuildRescheduleSelectTimeBottomSheetState();
}

class _BuildRescheduleSelectTimeBottomSheetState
    extends State<BuildRescheduleSelectTimeBottomSheet> {
  final GenericBloc<List<String>> timesCubit = GenericBloc([]);

  fetchData() {
    timesCubit.onUpdateToInitState([]);
    widget.rootData.rescheduleReservationDateData
        .fetchTimeSlots(widget.serviceListModel.businessServiceID,
            widget.serviceListModel.staffId, widget.dateTime ?? DateTime.now())
        .then((value) {
      var selectedTimes = widget.rootData.rescheduleReservationDateData
          .selectedServiceCubit.state.data;
      value.removeWhere((e) => selectedTimes.any((time) => time.time == e));
      timesCubit.onUpdateData(value.toSet().toList());
    });
  }

  @override
  Widget build(BuildContext context) {
    fetchData();
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<SelectServiceTimeEntity>>,
            GenericState<List<SelectServiceTimeEntity>>>(
        bloc: widget.rootData.rescheduleReservationDateData.selectedServiceCubit,
        builder: (context, selectedState) {
          return Container(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(15.r),
              ),
            ),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BuildHeaderBottomSheet(
                    title: tr("Select_Service_Time"),
                  ),
                  MyText(
                    title: widget.serviceListModel.getServiceName(),
                    color: MyColors.grey,
                    fontWeight: FontWeight.bold,
                    size: device.isTablet ? 6.sp : 10.sp,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    height: 250.h,
                    child: BlocBuilder<GenericBloc<List<String>>,
                        GenericState<List<String>>>(
                      bloc: timesCubit,
                      builder: (context, state) {
                        if (state is GenericUpdateState) {
                          return ListView.separated(
                            itemCount: state.data.length,
                            itemBuilder: (cxt, index) {
                              return InkWell(
                                onTap: () => widget
                                    .rootData.rescheduleReservationDateData
                                    .addSelectedService(
                                  context,
                                  widget.serviceListModel.businessServiceID,
                                  state.data[index],
                                  index,
                                  widget.serviceListModel,
                                  widget.rootData,
                                ),
                                child: Container(
                                  height: 45.h,
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 14).r,
                                  alignment: Alignment.center,
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: MyText(
                                          color: MyColors.black,
                                          title: getIt<Utilities>()
                                              .convertNumToAr(
                                                  context: context, value: state.data[index]),
                                          size: device.isTablet ? 7.sp : 10.sp,
                                          fontFamily:
                                              CustomFonts.primarySemiBoldFont,
                                        ),
                                      ),
                                      Transform.rotate(
                                        angle:
                                            device.locale == Locale('en', 'US')
                                                ? 0
                                                : 3.16,
                                        child:
                                            SvgPicture.asset(Res.arrowForward),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return Divider(height: 0);
                            },
                          );
                        }
                        return ListView.builder(
                          itemCount: 5,
                          itemBuilder: (cxt, index) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: BuildShimmerView(
                                height: 50.h,
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                ]),
          );
        });
  }
}
