part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleReserveTogether extends StatelessWidget {
  final RescheduleAppointmentData rootData;

  const BuildRescheduleReserveTogether({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: const EdgeInsets.all(15).r,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20.sp),
          MyText(
            title: tr("available_times"),
            color: MyColors.black,
            size: device.isTablet ? 7.sp : 11.sp,
            fontWeight: FontWeight.bold,
          ),
          const SizedBox(height: 15),
          BlocBuilder<GenericBloc<BestTimeEntity>,
              GenericState<BestTimeEntity>>(
            bloc: rootData.rescheduleReservationDateData.bestTimesCubit,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                if (state.data.times.isEmpty) {
                  return BuildRescheduleDateDetails(
                    rootData: rootData,
                  );
                }
                return Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12).r,
                    color: MyColors.white,
                  ),
                  child: ListView.separated(
                    shrinkWrap: true,
                    itemCount: state.data.times.length,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.zero,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () => rootData.rescheduleReservationDateData
                            .selectAvailableTimeItem(rootData, index),
                        child: Container(
                          height: 45.h,
                          color: state.data.index == index
                              ? MyColors.primary
                              : null,
                          padding: EdgeInsets.symmetric(horizontal: 10).r,
                          alignment: Alignment.center,
                          child: Row(
                            children: [
                              Expanded(
                                child: MyText(
                                  color: state.data.index == index
                                      ? MyColors.white
                                      : MyColors.black,
                                  title:getIt<Utilities>().convertNumToAr(context: context, value: state.data.times[index]),
                                  size: device.isTablet ? 7.sp : 10.sp,
                                  fontFamily: CustomFonts.primarySemiBoldFont,
                                ),
                              ),
                              Transform.rotate(
                                angle: device.locale == Locale('en', 'US') ? 0 : 3.16,
                                child: SvgPicture.asset(
                                  Res.arrowForward,
                                  color: state.data.index == index
                                      ? MyColors.white
                                      : MyColors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(
                        height: 1,
                      );
                    },
                  ),
                );
              }
              return Column(
                children: List.generate(
                  5,
                  (index) {
                    return BuildShimmerView(
                      height: 35.h,
                      margin:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 5).r,
                    );
                  },
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
