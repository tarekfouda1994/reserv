part of 'reschedule_date_widgets_imports.dart';

class BuildRescheduleDateHeader extends StatelessWidget {
  final RescheduleAppointmentData rootData;

  const BuildRescheduleDateHeader({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var services = rootData.rescheduleStaffData.servicesBloc.state.data;
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: rootData.rescheduleReservationDateData.switchedCubit,
        builder: (cxt, state) {
          if (rootData.rescheduleStaffData.servicesBloc.state.data.length !=
              1) {
            return Container(
              padding: const EdgeInsets.all(12).r,
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 15).r,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: state.data
                      ? MyColors.reserveTogetherBg
                      : MyColors.disableTogetherBg,
                  border: Border.all(color: MyColors.white, width: 2.sp),
                  borderRadius: BorderRadius.circular(15.r)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: MyText(
                          title: tr("reserve_together"),
                          color: MyColors.black,
                          size: device.isTablet ? 8.sp : 12.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 35.w,
                        height: 10.h,
                        child: Switch.adaptive(
                          activeColor: MyColors.primary,
                          value: state.data,
                          onChanged: (val) => rootData
                              .rescheduleReservationDateData
                              .onChangeReserveTogether(rootData, val, services),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 15.sp),
                  MyText(
                    title: tr("desc_reserve"),

                    color: MyColors.black,
                    size: device.isTablet ? 7.sp : 10.sp,
                  ),
                ],
              ),
            );
          } else {
            return SizedBox(height: 20);
          }
        });
  }
}
