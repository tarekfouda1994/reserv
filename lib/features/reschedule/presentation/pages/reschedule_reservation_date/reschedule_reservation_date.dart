part of 'reschedule_reservation_date_imports.dart';

class RescheduleReservationDate extends StatefulWidget {
  final RescheduleAppointmentData reservationAppointmentData;

  const RescheduleReservationDate(
      {Key? key, required this.reservationAppointmentData})
      : super(key: key);

  @override
  State<RescheduleReservationDate> createState() =>
      _RescheduleReservationDateState();
}

class _RescheduleReservationDateState extends State<RescheduleReservationDate> {
  late RescheduleReservationDateData rescheduleReservationDateData;
  late RescheduleAppointmentData rootData;

  @override
  void initState() {
    rootData = widget.reservationAppointmentData;
    rescheduleReservationDateData =
        widget.reservationAppointmentData.rescheduleReservationDateData;
    rescheduleReservationDateData.fetchServicesAvailableDays(rootData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: BlocBuilder<GenericBloc<List<DateTime>>,
              GenericState<List<DateTime>>>(
            bloc: rescheduleReservationDateData.dateCubit,
            builder: (context, state) {
              return ListView(
                padding: EdgeInsets.zero,
                children: [
                  BuildRescheduleDateHeader(rootData: rootData),
                  BuildRescheduleSelectDate(
                    selectedDates: state.data,
                    rootData: rootData,
                  ),
                  BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
                    bloc: rescheduleReservationDateData.availableDateCubit,
                    builder: (_, statusState) {
                      if (!statusState.data) {
                        return BuildRescheduleUnavailableDate(
                          rootData: rootData,
                        );
                      }
                      return BuildRescheduleSwitchDate(
                        rootData: rootData,
                        data: state.data.firstOrNull,
                      );
                    },
                  ),
                ],
              );
            },
          ),
        ),
        BuildRescheduleButton(rootData: rootData),
      ],
    );
  }
}
