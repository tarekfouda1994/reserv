part of 'reschedule_reservation_date_imports.dart';

class RescheduleReservationDateData {
  final GenericBloc<List<DateTime>> dateCubit = GenericBloc([]);
  final GenericBloc<bool> availableDateCubit = GenericBloc(true);
  final GenericBloc<bool> switchedCubit = GenericBloc(true);

  final GenericBloc<BestTimeEntity> bestTimesCubit =
      GenericBloc(BestTimeEntity(index: -1, times: []));
  final GenericBloc<List<SelectServiceTimeEntity>> selectedServiceCubit =
      GenericBloc([]);
  final GenericBloc<List<ServiceDateModel>> availableDatesCubit =
      GenericBloc([]);

  final String businessId;
  late String selectedTime = '-';

  RescheduleReservationDateData(this.businessId);

  addSelectedService(BuildContext context, String id, String time, int index,
      ServiceListModel serviceListModel, RescheduleAppointmentData rootData) {
    var existItem =
        selectedServiceCubit.state.data.firstWhereOrNull((e) => e.id == id);
    if (existItem != null) {
      selectedServiceCubit.state.data.remove(existItem);
    }
    _updateServiceItemTime(id, time, serviceListModel, rootData);
    AutoRouter.of(context).pop();
  }

  void _updateServiceItemTime(String id, String time,
      ServiceListModel serviceListModel, RescheduleAppointmentData rootData) {
    selectedServiceCubit.state.data.add(
      SelectServiceTimeEntity(
        id: id,
        time: time,
      ),
    );
    selectedServiceCubit.onUpdateData(selectedServiceCubit.state.data);
    serviceListModel.selectedTimeForService = time;
    rootData.rescheduleStaffData.servicesBloc
        .onUpdateData(rootData.rescheduleStaffData.servicesBloc.state.data);
  }

  fetchServicesAvailableDays(RescheduleAppointmentData rootData) async {
    getIt<LoadingHelper>().showLoadingDialog();
    availableDatesCubit.onUpdateToInitState([]);
    var services = rootData.rescheduleStaffData.servicesBloc.state.data;
    var requestedServices = services.map((e) {
      return RequestedServiceEntity(
          businessServiceID: e.businessServiceID,
          staffID: e.staffId == "0" ? e.staffDetail.first.staffID : e.staffId);
    }).toList();
    var result = await fetchAvailableDays(requestedServices);
    availableDatesCubit.onUpdateData(result);
    _selectDefaultAvailableDate(result);
    getIt<LoadingHelper>().dismissDialog();
  }

  void _selectDefaultAvailableDate(List<ServiceDateModel> data) {
    var firstAvailableDate =
        data.firstWhereOrNull((element) => element.isServiceAvailable);
    if (firstAvailableDate != null) {
      dateCubit.onUpdateData([firstAvailableDate.date]);
    }
  }

  fetchAvailableDays(List<RequestedServiceEntity> requestedServices) async {
    final date = DateTime.now();
    AvailableDaysEntity params = _serviceDateEntity(requestedServices, date);
    var data = await GetServiceAvailableDays()(params);
    return data;
  }

  AvailableDaysEntity _serviceDateEntity(
      List<RequestedServiceEntity> requestedServices, DateTime date) {
    final params = AvailableDaysEntity(
      businessId: businessId,
      requestedServices: requestedServices,
      dateFrom: getIt<Utilities>().formatDate(date),
      dateTo: getIt<Utilities>().formatDate(date.add(Duration(days: 60))),
    );
    return params;
  }

  Future<List<String>> fetchTimeSlots(
      String serviceId, String staffId, DateTime dateTime) async {
    TimeSlotEntity params = _timeSlotEntityParams(serviceId, staffId, dateTime);
    var data = await GetTimeSlots()(params);
    return data;
  }

  TimeSlotEntity _timeSlotEntityParams(
      String serviceId, String staffId, DateTime dateTime) {
    final params = TimeSlotEntity(
      businessId: businessId,
      serviceId: serviceId,
      staffId: staffId,
      dateTime: DateFormat("yyyy-MM-dd").format(dateTime),
    );
    return params;
  }

  onChangeReserveTogether(RescheduleAppointmentData rootData, bool value,
      List<ServiceListModel> services) async {
    if (services.length >= 2) {
      switchedCubit.onUpdateData(value);
      if (value) {
        _doReserveTogether(services);
      } else {
        _doIndividualReserve(rootData);
      }
    }
  }

  void _doIndividualReserve(RescheduleAppointmentData rootData) {
    selectedServiceCubit.onUpdateData([]);
    bestTimesCubit.onUpdateToInitState(BestTimeEntity(index: -1, times: []));
    rootData.rescheduleStaffData.servicesBloc.state.data
        .map((e) => e.selectedTimeForService = '-')
        .toList();
    rootData.rescheduleStaffData.servicesBloc
        .onUpdateData(rootData.rescheduleStaffData.servicesBloc.state.data);
    CustomToast.showSimpleToast(
      msg: tr("select_individually"),
      title: tr("Reservation_Slots"),
      type: ToastType.info,
    );
  }

  void _doReserveTogether(List<ServiceListModel> services) {
    applyTogetherFunc(services);
    selectedServiceCubit.onUpdateData([]);
    bestTimesCubit.onUpdateToInitState(BestTimeEntity(index: -1, times: []));
    CustomToast.showSimpleToast(
      msg: tr("select_together"),
      title: tr("Reservation_Slots"),
      type: ToastType.info,
    );
  }

  applyTogetherFunc(List<ServiceListModel> services) async {
    var requestServices = services.map((e) {
      return RequestedServiceEntity(
          businessServiceID: e.businessServiceID, staffID: e.staffId);
    }).toList();
    if (requestServices.isNotEmpty) {
      bestTimesCubit.onUpdateToInitState(BestTimeEntity(index: -1, times: []));
      DateTime dateTime = dateCubit.state.data.firstOrNull ?? DateTime.now();
      var params = TogetherEntity(
        businessID: businessId,
        isReserveTogether: true,
        reservationDate: DateFormat("yyyy-MM-dd").format(dateTime),
        requestedServices: requestServices,
      );

      var result = await GetReserveTogetherData()(params);
      if (result != null) {
        bestTimesCubit.state.data.times.addAll(
          result.servicesWithStaff.firstOrNull?.timeSlots ?? [],
        );
        bestTimesCubit.onUpdateData(bestTimesCubit.state.data);
      }
    }
  }

  void submitReschedule(
    BuildContext context,
    RescheduleAppointmentData rootData,
  ) async {
    _handleReserveTogetherSelectedItem();
    if (_checkIfCanSubmit(rootData)) {
      getIt<LoadingHelper>().showLoadingDialog();
      var services = _getRescheduleServicesParams(rootData);
      var result = await MakeReschedule()(
        RescheduleParamsEntity(
          services: services,
          orderNumber: rootData.orderNumber,
        ),
      );
      if (result != null) {
        await _successReservationData(context, rootData);
        AppointmentOrdersModel? appointmentOrdersModel =
            _appointmentOrderModel(context);
        getIt<LoadingHelper>().dismissDialog();
        CustomToast.showSimpleToast(
            title: tr("success_toast"),
            msg: tr("reschedule_successfully"),
            type: ToastType.success);
        AutoRouter.of(context).popAndPush(DashboardDetailsRoute(
            status: appointmentOrdersModel!.statusId,
            appointmentOrdersModel: appointmentOrdersModel));
      }
    }
  }

  AppointmentOrdersModel? _appointmentOrderModel(BuildContext context) {
    var appointmentOrdersModel = context
        .read<ReservationDetailsCubit>()
        .state
        .model
        ?.appointmentOrdersModel;
    return appointmentOrdersModel;
  }

  _successReservationData(
      BuildContext context, RescheduleAppointmentData rootData) async {
    var device = context.read<DeviceCubit>().state.model;
    List<AppointmentOrdersModel> data = await getOrders(
      context,
      rootData: rootData,
      orderNumber: rootData.orderNumber,
    );
    _successReservationModel(
      context,
      rootData.orderNumber,
      rootData,
      device,
      data,
    );
  }

  _successReservationModel(
      BuildContext context,
      String orderNumber,
      RescheduleAppointmentData rootData,
      DeviceModel device,
      List<AppointmentOrdersModel> data) {
    ReservationDetailsWithoutLoginModel successReserveModel =
        _successReserveModel(orderNumber, rootData, device, data);
    context
        .read<ReservationDetailsCubit>()
        .onUpdateReservationData(successReserveModel);
  }

  ReservationDetailsWithoutLoginModel _successReserveModel(
      String orderNumber,
      RescheduleAppointmentData rootData,
      DeviceModel device,
      List<AppointmentOrdersModel> data) {
    var successReserveModel = ReservationDetailsWithoutLoginModel(
      refNumber: orderNumber,
      businessName: rootData.rescheduleStaffData.detailsCubit.state.data!
          .getBusinessName(),
      reservationTimes: rootData
          .rescheduleReservationDateData.selectedServiceCubit.state.data
          .map((e) => e.time)
          .toList(),
      date: formatDate(
        rootData.rescheduleReservationDateData.dateCubit.state.data,
        rootData.rescheduleReservationDateData.dateCubit.state.data.firstOrNull,
        device.locale,
      ),
      appointmentOrdersModel: data.first,
    );
    return successReserveModel;
  }

  Future<List<AppointmentOrdersModel>> getOrders(
    BuildContext context, {
    required RescheduleAppointmentData rootData,
    required String orderNumber,
  }) async {
    var params = AppointmentOrderParams(
      refresh: false,
      lastName: rootData.lastName,
      orderNumber: orderNumber,
    );
    var data = await GetDashboardData()(params);
    return data;
  }

  String formatDate(List listDate, DateTime? dateTime, Locale locale) {
    String date = "";
    var formatter = DateFormat.yMMMMEEEEd('ar_SA');
    if (listDate != []) {
      if (locale == Locale('en', 'US')) {
        date = DateFormat("EEEE d-MMM-yyyy").format(dateTime ?? DateTime(0));
      } else {
        date = formatter.format(dateTime ?? DateTime(0));
      }
    }
    return date;
  }

  List<RescheduleParams> _getRescheduleServicesParams(
    RescheduleAppointmentData rootData,
  ) {
    var selectedServices = rootData.rescheduleStaffData.servicesBloc.state.data;
    var date = dateCubit.state.data.first;
    var times = selectedServiceCubit.state.data;
    return rootData.services.map((e) {
      e.staffID = selectedServices
          .firstWhere((serv) => serv.businessServiceID == e.businessServiceId)
          .staffId;
      e.appointmentDate = getIt<Utilities>().formatDate(date);
      e.fromTime = times
          .firstWhere((time) {
            return time.id == e.businessServiceId;
          })
          .time
          .split(" - ")
          .first;
      return e;
    }).toList();
  }

  bool _checkIfCanSubmit(RescheduleAppointmentData rootData) {
    if (switchedCubit.state.data && bestTimesCubit.state.data.index >= 0) {
      return true;
    }
    if (selectedServiceCubit.state.data.length !=
        rootData.rescheduleStaffData.servicesBloc.state.data.length) {
      CustomToast.showSimpleToast(
        msg: tr("please_select_appointment_time"),
        title: tr("missing_appointment_time"),
      );
      return false;
    } else {
      return true;
    }
  }

  void _handleReserveTogetherSelectedItem() {
    // if (switchedCubit.state.data) {
    //   int index = bestTimesCubit.state.data.index;
    //   if (index >= 0) {
    //     selectedServiceCubit.state.data.clear();
    //     for (var element in bestTimesCubit.state.data.times[index]) {
    //       var item = SelectServiceTimeEntity(
    //           id: element.service,
    //           time: DateFormat("HH:mm").format(element.date));
    //       selectedServiceCubit.state.data.add(item);
    //     }
    //     selectedServiceCubit.onUpdateData(selectedServiceCubit.state.data);
    //   }
    // }
  }

  bottomSheetSelectTime(
      BuildContext context,
      ServiceListModel serviceListModel,
      RescheduleReservationDateData dateData,
      RescheduleAppointmentData rootData,
      DateTime? dateTime,
      int indexItem) {
    showModalBottomSheet(
      elevation: 10,
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      builder: (cxt) {
        return BuildRescheduleSelectTimeBottomSheet(
          rootData: rootData,
          serviceListModel: serviceListModel,
          dateTime: dateTime,
          indexItem: indexItem,
        );
      },
    );
  }

  selectAvailableTimeItem(RescheduleAppointmentData rootData, index) {
    bestTimesCubit.state.data.index = index;
    var timesEntity =
        rootData.rescheduleStaffData.servicesBloc.state.data.map((e) {
      return SelectServiceTimeEntity(
        id: e.businessServiceID,
        time: bestTimesCubit.state.data.times[index],
      );
    }).toList();
    selectedServiceCubit.onUpdateData(timesEntity);
    bestTimesCubit.onUpdateData(bestTimesCubit.state.data);
  }

  onSelectDate(bool disabled, DateTime dateTime,
      List<ServiceListModel> services, RescheduleAppointmentData rootData) {
    dateCubit.onUpdateData([dateTime]);
    if (disabled) {
      availableDateCubit.onUpdateData(false);
    } else {
      availableDateCubit.onUpdateData(true);
      selectedServiceCubit.onUpdateData([]);
      if (switchedCubit.state.data) {
        applyTogetherFunc(services);
      }
    }
    _removeOldServicesTimes(services, rootData);
  }

  _removeOldServicesTimes(
      List<ServiceListModel> services, RescheduleAppointmentData rootData) {
    if (!switchedCubit.state.data) {
      services.map((e) => e.selectedTimeForService = "-").toList();
      rootData.rescheduleStaffData.servicesBloc
          .onUpdateData(rootData.rescheduleStaffData.servicesBloc.state.data);
    }
  }
}
