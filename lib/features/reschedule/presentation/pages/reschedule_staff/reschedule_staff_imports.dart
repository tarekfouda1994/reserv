
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_appointment/reschedule_appointment_imports.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_types_model/service_types_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/business_service_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/use_cases/get_company_services.dart';
import 'package:flutter_tdd/features/search/data/models/service_details_model/service_details_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/service_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/use_cases/get_service_details.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'widgets/reschedule_staff_widgets_imports.dart';

part 'reschedule_staff.dart';
part 'reschedule_staff_data.dart';