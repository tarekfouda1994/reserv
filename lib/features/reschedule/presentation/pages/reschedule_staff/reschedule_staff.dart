part of 'reschedule_staff_imports.dart';

class RescheduleStaff extends StatefulWidget {
  final RescheduleAppointmentData reservationAppointmentData;

  const RescheduleStaff({Key? key, required this.reservationAppointmentData})
      : super(key: key);

  @override
  State<RescheduleStaff> createState() => _RescheduleStaffState();
}

class _RescheduleStaffState extends State<RescheduleStaff> {
  late RescheduleStaffData rescheduleStaffData;
  late RescheduleAppointmentData rootData;

  @override
  void initState() {
    rootData = widget.reservationAppointmentData;
    rescheduleStaffData = widget.reservationAppointmentData.rescheduleStaffData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<ServiceTypesModel>>,
            GenericState<List<ServiceTypesModel>>>(
        bloc: rescheduleStaffData.companyServicesBloc,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            return Column(children: [
              BuildRescheduleStaffHeader(
                  rescheduleStaffData: rescheduleStaffData),
              BuildServicesCatTabs(
                  rescheduleStaffData: rescheduleStaffData,
                  serviceTypes: state.data),
              BuildListServiceItems(
                serviceTypes: state.data,
                rescheduleStaffData: rescheduleStaffData,
              ),
              BuildRescheduleStaffBottom(rootData: rootData),
            ]);
          }
          return BuildLoadingRescheduleStaff(
            rescheduleStaffData: rescheduleStaffData,
          );
        });
  }
}
