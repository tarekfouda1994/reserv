part of 'reschedule_staff_imports.dart';

class RescheduleStaffData {
  final GenericBloc<ServiceDetailsModel?> detailsCubit = GenericBloc(null);
  final GenericBloc<List<ServiceListModel>> servicesBloc = GenericBloc([]);
  final GenericBloc<List<ServiceTypesModel>> companyServicesBloc =
      GenericBloc([]);
  final ScrollController scrollController = ScrollController();

  getServiceDetails(BuildContext context, RescheduleAppointmentData rootData,
      {bool refresh = true}) async {
    var data = await GetServiceDetails().call(
      ServiceDetailsPrams(id: rootData.businessId, refresh: refresh),
    );
    if (data != null) {
      detailsCubit.onUpdateData(data);
    }
  }

  getCompanyServices(
      BuildContext context, RescheduleAppointmentData rootData) async {
    var data = await GetCompanyServices().call(
      BusinessServicePrams(
        id: rootData.businessId,
        loadImages: true,
      ),
    );
    if (data.isNotEmpty) {
      for (var element in data) {
        for (var service in element.servicesList) {
          if (service.staffDetail.isNotEmpty) {
            _addAnyOneStaff(service);
            _addSelectedServices(rootData, service);
          }
        }
      }
    }
    data = _onSelectServices(data, rootData);
    data.first.selected = true;
    companyServicesBloc.onUpdateData(data);
    servicesBloc.onUpdateData(servicesBloc.state.data);
  }

  List<ServiceTypesModel> _onSelectServices(
      List<ServiceTypesModel> data, RescheduleAppointmentData rootData) {
    data = data.map((e) {
      e.servicesList = e.servicesList
          .where((element) => _checkServiceExist(rootData, element))
          .toList();
      return e;
    }).toList();
    return data;
  }

  void _addSelectedServices(
    RescheduleAppointmentData rootData,
    ServiceListModel service,
  ) {
    if (_checkServiceExist(rootData, service)) {
      _selectDefaultStaff(service, rootData);
      servicesBloc.state.data.add(service);
    }
  }

  bool _checkServiceExist(
          RescheduleAppointmentData rootData, ServiceListModel service) =>
      rootData.services.any(
        (serv) => serv.businessServiceId == service.businessServiceID,
      );

  void _selectDefaultStaff(
      ServiceListModel service, RescheduleAppointmentData rootData) {
    var staffId = rootData.services
        .firstWhereOrNull(
          (staff) => staff.businessServiceId == service.businessServiceID,
        )
        ?.staffID;
    staffId ??= service.staffDetail.first.staffID;
    service.staffId = staffId;
  }

  void _addAnyOneStaff(ServiceListModel service) {
    service.staffDetail = service.staffDetail.toSet().toList();
    service.staffDetail.sort((a, b) => b.days
        .where((day) => day.isDateAvailable)
        .length
        .compareTo(a.days.where((day) => day.isDateAvailable).length));
    service.staffDetail.insert(
        0,
        service.staffDetail.first.copyWith(
            staffNameArabic: tr("any_one"),
            staffNameEnglish: tr("any_one"),
            staffID: "0"));
  }

  nextPage(RescheduleAppointmentData rootData) {
    if (rootData.rescheduleStaffData.servicesBloc.state.data.length == 0) {
      CustomToast.showSimpleToast(
          msg: tr("worn_to_select_service"),
          title: tr("Missing_reservation_service"),
          type: ToastType.info);
      return;
    }

    for (var e in servicesBloc.state.data) {
      if (e.staffDetail.isEmpty) {
        CustomToast.showSimpleToast(
          msg:
              "${e.getServiceName()} ${tr("staff")} is empty, select another one",
          title: tr("Missing_reservation_staff"),
          type: ToastType.info,
        );
      }
      if (e.staffId == "0") {
        e.staffId = e.staffDetail[1].staffID;
      }
      if (e.staffId.isEmpty) {
        CustomToast.showSimpleToast(
          msg: "${tr("select")} ${e.getServiceName()} ${tr("staff")}",
          title: tr("Warning_Toast"),
          type: ToastType.warning,
        );
        return;
      }
    }
    rootData.rescheduleReservationDateData
        .applyTogetherFunc(servicesBloc.state.data);
    rootData.goNextPage();
  }

  addStaff(ServiceListModel model, int index) {
    servicesBloc.state.data
        .removeWhere((e) => e.businessServiceID == model.businessServiceID);
    servicesBloc.state.data.add(model);
    var data = servicesBloc.state.data.map((e) {
      if (e.businessServiceID == model.businessServiceID) {
        e.staffId = model.staffDetail[index].staffID;
        e.staffName = model.staffDetail[index].getStaffName();
      }
      return e;
    }).toList();
    servicesBloc.onUpdateData(data);
  }

  removeStaff(ServiceListModel model) {
    servicesBloc.state.data.map((e) {
      if (e == model) {
        e.staffId = "";
        e.staffName = "";
      }
      return e;
    }).toList();
    servicesBloc.onUpdateData(servicesBloc.state.data);
  }

  scrollToServiceType(GlobalKey key, int index) {
    int lastIndex = companyServicesBloc.state.data
        .indexWhere((element) => element.selected);
    companyServicesBloc.state.data[lastIndex].selected = false;
    companyServicesBloc.state.data[index].selected = true;
    companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
    scrollController.position.ensureVisible(
        key.currentContext!.findRenderObject()!,
        duration: const Duration(milliseconds: 400),
        alignment: 0.1);
  }
}
