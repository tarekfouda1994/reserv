part of 'reschedule_staff_widgets_imports.dart';

class BuildListServiceItems extends StatelessWidget {
  final RescheduleStaffData rescheduleStaffData;
  final List<ServiceTypesModel> serviceTypes;

  const BuildListServiceItems(
      {Key? key, required this.rescheduleStaffData, required this.serviceTypes})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Flexible(
      child: ListView(
        controller: rescheduleStaffData.scrollController,
        children: List.generate(serviceTypes.length, (index) {
          return Column(
            key: serviceTypes[index].key,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Visibility(
                visible: serviceTypes[index].servicesList.isNotEmpty,
                child: BuildCatTitle(
                    title: serviceTypes[index].getServiceTypeName(),
                    count: serviceTypes[index].servicesList.length,
                    model: device),
              ),
              CustomCard(
                padding: EdgeInsets.zero,
                child: ListView.separated(
                  itemCount: serviceTypes[index].servicesList.length,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return BuildServiceItem(
                      rescheduleStaffData: rescheduleStaffData,
                      length: serviceTypes[index].servicesList.length,
                      serviceListModel: serviceTypes[index].servicesList[i],
                      index: i,
                      id: serviceTypes[index].businessId,
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Container(height: 3);
                  },
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
