part of'reschedule_staff_widgets_imports.dart';

class BuildTabsItem extends StatelessWidget {
  final RescheduleStaffData rescheduleStaffData;
  final ServiceTypesModel serviceTypesModel;
  final int index;
  const BuildTabsItem({Key? key, required this.rescheduleStaffData, required this.serviceTypesModel, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => rescheduleStaffData.scrollToServiceType(serviceTypesModel.key, index),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18.w),
        margin: EdgeInsets.symmetric(vertical: 7.h, horizontal: 3.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40.r),
          color: serviceTypesModel.selected ? MyColors.primary : MyColors.headerBackGroundColor,
        ),
        alignment: Alignment.center,
        child: MyText(
          alien: TextAlign.center,
          size: 9.sp,
          title: serviceTypesModel.getServiceTypeName(),
          color: serviceTypesModel.selected ? MyColors.headerBackGroundColor : MyColors.primary,
        ),
      ),
    );
  }
}
