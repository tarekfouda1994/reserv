part of 'reschedule_staff_widgets_imports.dart';

class BuildRescheduleStaffHeader extends StatelessWidget {
  final RescheduleStaffData rescheduleStaffData;

  const BuildRescheduleStaffHeader(
      {Key? key, required this.rescheduleStaffData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<ServiceDetailsModel?>,
        GenericState<ServiceDetailsModel?>>(
      bloc: rescheduleStaffData.detailsCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Container(
            color: MyColors.primary,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25).r,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(Res.building,
                    height: 18.r, width: 18.r, color: MyColors.white),
                SizedBox(width: 10.sm),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: MyText(
                                fontFamily: CustomFonts.primarySemiBoldFont,
                                title: state.data?.getBusinessName() ?? "",
                                color: MyColors.white,
                                size: device.isTablet ? 7.sp : 11.sp),
                          ),
                          MyText(
                              title: "${state.data?.rating} ",
                              color: MyColors.white,
                              fontFamily: CustomFonts.primarySemiBoldFont,
                              size: device.isTablet ? 5.5.sp : 9.sp),
                          SvgPicture.asset(
                            Res.star,
                            color: MyColors.white,
                            height: 12.sp,
                            width: 12.sp,
                          ),
                        ],
                      ),
                      SizedBox(height: 8.h),
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () => AutoRouter.of(context).push(
                                  TrackingMapRoute(
                                      businessName:
                                          state.data!.getBusinessName(),
                                      address: state.data!.getAddressName(),
                                      lat: state.data!.latitude,
                                      lng: state.data!.longitude)),
                              child: Row(children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 2.0),
                                  child: SvgPicture.asset(
                                      Res.location_arrow_icon,
                                      color: Color(0xffABDE54)),
                                ),
                                MyText(
                                    decoration: TextDecoration.underline,
                                    title: tr("direction") + " ",
                                    color: Color(0xffABDE54),
                                    size: device.isTablet ? 5.sp : 9.sp),
                                MyText(
                                    title: state.data?.serviceDistance ?? "",
                                    color: MyColors.white,
                                    size: device.isTablet ? 5.5.sp : 9.sp),
                              ]),
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsetsDirectional.only(end: 3, start: 25.w),
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: MyColors.white),
                          ),
                          MyText(
                              title: state.data?.getAddressName() == "Male"
                                  ? tr("men")
                                  : tr("women"),
                              color: MyColors.white,
                              size: device.isTablet ? 5.5.sp : 9.sp),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }
        return BuildShimmerView(
            height: 88.h, width: MediaQuery.of(context).size.width);
      },
    );
  }
}
