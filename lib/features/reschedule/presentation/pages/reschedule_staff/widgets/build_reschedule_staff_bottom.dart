part of 'reschedule_staff_widgets_imports.dart';

class BuildRescheduleStaffBottom extends StatelessWidget {
  final RescheduleAppointmentData rootData;

  const BuildRescheduleStaffBottom({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
        GenericState<List<ServiceListModel>>>(
      bloc: rootData.rescheduleStaffData.servicesBloc,
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
            color: MyColors.white,
            boxShadow: [
              BoxShadow(
                color: MyColors.greyWhite,
                spreadRadius: 2,
                blurRadius: 2,
              )
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              MyText(
                                title:
                                    "${state.data.lastOrNull?.getCurrencyName() ?? ""} ${state.data.fold(0, (num prev, e) => prev + e.price)} ",
                                color: MyColors.black,
                                fontWeight: FontWeight.bold,
                                size: device.isTablet ? 8.sp : 12.sp,
                              ),
                              Expanded(
                                child: MyText(

                                  title: tr("including_vat"),
                                  color: MyColors.grey,
                                  size: device.isTablet ? 5.5.sp : 9.sp,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.h),
                        ],
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () =>
                            rootData.rescheduleStaffData.nextPage(rootData),
                        child: Container(
                          height: device.isTablet ? 50.h : 40.h,
                          decoration: BoxDecoration(
                              color: MyColors.primary,
                              borderRadius: BorderRadius.circular(40.r)),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.w, vertical: 10.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              MyText(
                                  title: tr("proceed"),
                                  color: MyColors.white,
                                  size: device.isTablet ? 7.sp : 11.sp),
                              SvgPicture.asset(
                                device.locale == Locale('en', 'US')
                                    ? Res.arrowForward
                                    : Res.arrow,
                                height: device.locale == Locale('en', 'US')
                                    ? null
                                    : device.isTablet
                                        ? 9.sp
                                        : 15.sp,
                                color: MyColors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
