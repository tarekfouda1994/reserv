part of'reschedule_staff_widgets_imports.dart';

class BuildServicesCatTabs extends StatelessWidget {
  final RescheduleStaffData rescheduleStaffData;
  final List<ServiceTypesModel> serviceTypes;
  const BuildServicesCatTabs({Key? key, required this.rescheduleStaffData, required this.serviceTypes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      height: device.isTablet ? 45.h : 48.h,
      color: MyColors.white,
      margin: EdgeInsets.symmetric(vertical: 1.h),
      child: ListView.builder(
        padding: const EdgeInsetsDirectional.only(start: 10),
        itemCount: serviceTypes.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (cxt, index) {
          int i = serviceTypes.indexOf(serviceTypes.firstWhereOrNull((element) => element.servicesList.isNotEmpty)??serviceTypes[index]);
          return Visibility(
            visible: serviceTypes[index].servicesList.isNotEmpty,
            child: BuildTabsItem(
              index: index,
              rescheduleStaffData: rescheduleStaffData,
              serviceTypesModel: serviceTypes[index],
            ),
          );
        },
      ),
    );
  }
}
