part of'reschedule_staff_widgets_imports.dart';

class BuildLoadingRescheduleStaff extends StatelessWidget {
  final RescheduleStaffData rescheduleStaffData;
  const BuildLoadingRescheduleStaff({Key? key, required this.rescheduleStaffData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
          child: ListView(
            children: [
              BuildRescheduleStaffHeader(
                  rescheduleStaffData: rescheduleStaffData),
              BuildShimmerView(
                  height: 30.h,
                  width: MediaQuery.of(context).size.width * .9),
              ...List.generate(
                7,
                    (index) => BuildShimmerView(
                    height: 100.h,
                    width: MediaQuery.of(context).size.width * .9),
              )
            ],
          ),
        ),
      ],
    );
  }
}
