part of'reschedule_staff_widgets_imports.dart';

class BuildServiceItem extends StatelessWidget {
  final RescheduleStaffData rescheduleStaffData;
  final Function()? action;
  final Color? dividerColor;
  final int index;
  final int length;
  final String id;
  final ServiceListModel serviceListModel;
  const BuildServiceItem({Key? key, required this.rescheduleStaffData, this.action, this.dividerColor, required this.index, required this.length, required this.id, required this.serviceListModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(13.r),
        border: Border.all(
          color:  MyColors.primary,
        ),
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 5.h),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 13.w, vertical: 7.h),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () => AutoRouter.of(context)
                        .push(ProductDetailsRoute(id: id)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          title: serviceListModel.getServiceName(),
                          color: MyColors.primary,
                          fontFamily: CustomFonts.primarySemiBoldFont,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                        SizedBox(height: 8.h),
                        Container(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width * .65,
                          child: Row(
                            children: [
                              MyText(
                                title:
                                "${serviceListModel.getCurrencyName()} ${serviceListModel.price}",
                                color: MyColors.black,
                                size: device.isTablet ? 5.5.sp : 9.sp,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(width: 10.h),
                              Icon(
                                Icons.access_time,
                                color: MyColors.greyLight,
                                size: device.isTablet ? 12.sp : 15.sp,
                              ),
                              MyText(
                                  title: " " + serviceListModel.serviceDurationText,
                                  color: MyColors.blackOpacity,
                                  size: device.isTablet ? 5.5.sp : 9.sp),
                              Container(
                                margin: EdgeInsetsDirectional.only(
                                    end: 3, start: 20.w),
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: MyColors.greyLight),
                              ),
                              Expanded(
                                child: MyText(
                                    title: serviceListModel.getServiceName() ==
                                        "Male"
                                        ? tr("men")
                                        : tr("women"),
                                    color: MyColors.blackOpacity,
                                    size: device.isTablet ? 5.5.sp : 9.sp),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  if (serviceListModel.picture.isNotEmpty)
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          height: device.isTablet ? 70.h : 59.h,
                          child: Column(
                            children: [
                              CachedImage(
                                url: serviceListModel.picture.first
                                    .replaceAll("\\", "/"),
                                width: device.isTablet ? 35.w : 50.w,
                                height: device.isTablet ? 35.w : 50.w,
                                borderRadius: BorderRadius.circular(6.r),
                                fit: BoxFit.cover,
                                bgColor: MyColors.defaultImgBg,
                                placeHolder: ServicePlaceholder(),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 31.h,
                          width: 32.w,
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color:
                              MyColors.primary,
                              border: Border.all(
                                  color:  MyColors.white,
                                  width: 1.5)),
                          child: Icon( Icons.done ,
                              color:
                              MyColors.white ,
                              size: device.isTablet ? 12.sp : 17.sp),
                        ),
                      ],
                    ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: serviceListModel.staffDetail.isNotEmpty,
            child: Container(
              height: device.isTablet ? 65.h : 60.h,
              decoration:
              BoxDecoration(color: MyColors.primary.withOpacity(.04)),
              child: BuildScrollableBodyOfItems(
                addStaff:(ind)=> rescheduleStaffData.addStaff(serviceListModel, ind),
                removeStaff:()=> rescheduleStaffData.removeStaff(serviceListModel),
                servicesBloc: rescheduleStaffData.servicesBloc,
                model: device,
                serviceListModel: serviceListModel,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
