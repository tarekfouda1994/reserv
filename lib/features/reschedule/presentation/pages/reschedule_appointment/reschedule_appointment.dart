part of 'reschedule_appointment_imports.dart';

class RescheduleAppointment extends StatefulWidget {
  final String businessID;
  final String orderNumber;
  final String lastName;
  final List<RescheduleParams> services;

  const RescheduleAppointment(
      {Key? key,
      required this.businessID,
      required this.services,
      required this.orderNumber, required this.lastName})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _RescheduleAppointmentState();
}

class _RescheduleAppointmentState extends State<RescheduleAppointment> {
  late RescheduleAppointmentData reservationAppointmentData;

  @override
  void initState() {
    reservationAppointmentData = RescheduleAppointmentData(
      widget.businessID,
      widget.services,
      widget.lastName,
      context,
      widget.orderNumber,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GradientScaffold(
      appBar: DefaultAppBar(
        titleWidget: BuildRescheduleAppBarTitle(
            reservationAppointmentData: reservationAppointmentData),
        onBack: () => reservationAppointmentData.goPreviousPage(context),
        actions: [
          BuildRescheduleStepperView(
              reservationAppointmentData: reservationAppointmentData),
        ],
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: reservationAppointmentData.controller,
        onPageChanged: (index) =>
            reservationAppointmentData.pagesCubit.onUpdateData(index),
        children: [
          RescheduleStaff(
              reservationAppointmentData: reservationAppointmentData),
          RescheduleReservationDate(
              reservationAppointmentData: reservationAppointmentData)
        ],
      ),
    );
  }
}
