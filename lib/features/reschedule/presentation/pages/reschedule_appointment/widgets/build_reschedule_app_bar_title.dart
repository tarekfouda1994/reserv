part of'reschedule_widgets_imports.dart';


class BuildRescheduleAppBarTitle extends StatelessWidget {
  final RescheduleAppointmentData reservationAppointmentData;
  const BuildRescheduleAppBarTitle({Key? key, required this.reservationAppointmentData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: reservationAppointmentData.pagesCubit,
      builder: (context, state) {
        return MyText(
          title: reservationAppointmentData.appBarTitle(state.data, device.auth),
          color: MyColors.black,
          size: device.isTablet ? 8.sp : 12.sp,
          fontWeight: FontWeight.bold,
        );
      },
    );
  }
}
