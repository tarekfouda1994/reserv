part of'reschedule_widgets_imports.dart';

class BuildRescheduleStepperView extends StatelessWidget {
  final RescheduleAppointmentData reservationAppointmentData;
  const BuildRescheduleStepperView({Key? key, required this.reservationAppointmentData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: reservationAppointmentData.pagesCubit,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: device.isTablet ? 0.h : 8.h),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: device.isTablet ? 5.h : 6),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.r),
              color: MyColors.grey.withOpacity(.02),
              border: Border.all(color: MyColors.grey.withOpacity(.5), width: .2)),
          child: Row(children: [
            MyText(
                title: "${tr("step")} ${state.data + 1}/${"2"} ",
                color: MyColors.primaryDark,
                size: device.isTablet ? 6.sp : 9.sp,
                fontWeight: FontWeight.bold),
            Container(
                margin: const EdgeInsets.symmetric(horizontal: 2),
                width: 8.h,
                height: 8.h,
                decoration: BoxDecoration(
                    color: state.data == 0 ? MyColors.primary.withOpacity(.5) : MyColors.primary,
                    shape: BoxShape.circle)),
            Visibility(
              visible: state.data >= 1,
              child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 2),
                  width: 8.h,
                  height: 8.h,
                  decoration: BoxDecoration(
                      color: state.data == 1 ? MyColors.primary.withOpacity(.5) : MyColors.primary,
                      shape: BoxShape.circle)),
              replacement: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 2),
                  width: 8.h,
                  height: 8.h,
                  decoration: BoxDecoration(
                      color: MyColors.primary.withOpacity(.05), shape: BoxShape.circle)),
            ),
          ]),
        );
      },
    );
  }
}
