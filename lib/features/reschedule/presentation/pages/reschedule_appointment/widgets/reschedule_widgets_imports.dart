import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_appointment/reschedule_appointment_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'build_reschedule_app_bar_title.dart';
part 'build_reschedule_stepper_view.dart';