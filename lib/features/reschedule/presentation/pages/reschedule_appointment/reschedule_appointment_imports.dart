import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/core/widgets/gradient_scaffold.dart';
import 'package:flutter_tdd/features/reschedule/domain/entities/reschedule_entity.dart';
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_appointment/widgets/reschedule_widgets_imports.dart';
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_reservation_date/reschedule_reservation_date_imports.dart';
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_staff/reschedule_staff_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'reschedule_appointment.dart';
part 'reschedule_appointment_data.dart';