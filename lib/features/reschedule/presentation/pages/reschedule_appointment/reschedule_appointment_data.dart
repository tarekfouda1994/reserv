part of 'reschedule_appointment_imports.dart';

class RescheduleAppointmentData {
  final RescheduleStaffData rescheduleStaffData = RescheduleStaffData();
  late RescheduleReservationDateData rescheduleReservationDateData;

  final GenericBloc<int> pagesCubit = GenericBloc(0);
  late PageController controller;
  final List<RescheduleParams> services;
  final String businessId;
  final String orderNumber;
  final String lastName;

  RescheduleAppointmentData(
    this.businessId,
    this.services,
    this.lastName,
    BuildContext context,
    this.orderNumber,
  ) {
    rescheduleReservationDateData = RescheduleReservationDateData(businessId);
    controller = PageController(initialPage: 0);
    pagesCubit.onUpdateData(0);
    rescheduleStaffData.getServiceDetails(context, this);
    rescheduleStaffData.getCompanyServices(context, this);
  }

  void goNextPage() {
    pagesCubit.onUpdateData(pagesCubit.state.data + 1);
    controller.nextPage(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );
  }

  void goPreviousPage(BuildContext context) {
    if (pagesCubit.state.data == 0) {
      AutoRouter.of(context).pop();
    } else {
      pagesCubit.onUpdateData(pagesCubit.state.data - 1);
      controller.previousPage(
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  String appBarTitle(int index, bool auth) {
    late String title;
    if (index == 0) {
      title = tr("select_services");
    } else if (index == 1) {
      title = tr("pick_datetime");
    }
    return title;
  }
}
