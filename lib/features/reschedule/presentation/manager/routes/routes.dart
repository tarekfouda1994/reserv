import 'package:auto_route/auto_route.dart';
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_appointment/reschedule_appointment_imports.dart';

const rescheduleRoutes = [
  CustomRoute<bool?>(page: RescheduleAppointment),
];
