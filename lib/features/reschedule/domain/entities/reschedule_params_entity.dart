import 'reschedule_entity.dart';

class RescheduleParamsEntity{
  final List<RescheduleParams> services;
  final String orderNumber;

  RescheduleParamsEntity({required this.services, required this.orderNumber});
}