class RescheduleParams {
  String id;
  String businessId;
  String businessServiceName;
  String businessServiceId;
  String appointmentDate;
  String fromTime;
  int appointmentType;
  String staffID;

  RescheduleParams({
    required this.id,
    required this.businessId,
    required this.appointmentType,
    required this.staffID,
    required this.businessServiceId,
    required this.appointmentDate,
    required this.businessServiceName,
    required this.fromTime,
  });

  Map<String, dynamic> toJson() => {
        "appointmentID": id,
        "businessId": businessId,
        "businessServiceName": businessServiceName,
        "businessServiceId": businessServiceId,
        "appointmentDate": appointmentDate,
        "fromTime": fromTime,
        "staffID": staffID,
        "appointmentType": appointmentType,
      };
}
