import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reschedule/domain/entities/reschedule_params_entity.dart';
import 'package:flutter_tdd/features/reschedule/domain/repositories/reschedule_repo.dart';


class MakeReschedule implements UseCase<String?,RescheduleParamsEntity>{
  @override
  Future<String?> call(RescheduleParamsEntity params) async{
    var result = await getIt<RescheduleRepo>().makeReschedule(params);
    return result.fold((l) => null, (r) => r);
  }
}