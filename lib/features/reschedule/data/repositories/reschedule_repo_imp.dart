import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/features/reschedule/data/data_sources/reschedule_data_source.dart';
import 'package:flutter_tdd/features/reschedule/domain/entities/reschedule_params_entity.dart';
import 'package:flutter_tdd/features/reschedule/domain/repositories/reschedule_repo.dart';
import 'package:injectable/injectable.dart';



@Injectable(as: RescheduleRepo)
class RescheduleRepoImpl extends RescheduleRepo {
  @override
  Future<Either<Failure, String>> makeReschedule(RescheduleParamsEntity params)async{
    return await getIt<RescheduleDataSource>().makeReschedule(params);
  }

}
