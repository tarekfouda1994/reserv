
import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/reschedule/domain/entities/reschedule_params_entity.dart';

abstract class RescheduleDataSource {

  Future<Either<Failure, String>> makeReschedule(RescheduleParamsEntity params);


}