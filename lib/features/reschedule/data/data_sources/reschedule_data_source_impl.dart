import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/http/generic_http/generic_http.dart';
import 'package:flutter_tdd/core/http/models/http_request_model.dart';
import 'package:flutter_tdd/features/reschedule/data/data_sources/reschedule_data_source.dart';
import 'package:flutter_tdd/features/reschedule/domain/entities/reschedule_params_entity.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: RescheduleDataSource)
class RescheduleDataSourceImpl extends RescheduleDataSource {
  @override
  Future<Either<Failure, String>> makeReschedule(
      RescheduleParamsEntity params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.bulkReschedule,
      requestMethod: RequestMethod.put,
      responseType: ResType.type,
      requestBody: {
        "orderNumber": params.orderNumber,
        "list": params.services.map((e) => e.toJson()).toList(),
      },
      responseKey: (data) => data[0]["referenceId"],
      showLoader: true,
    );
    return await GenericHttpImpl<String>()(model);
  }
}
