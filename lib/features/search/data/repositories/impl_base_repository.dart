import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/features/search/data/data_sources/impl_home_remote_data_source.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_details/appointment_details.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_orders/appointment_orders.dart';
import 'package:flutter_tdd/features/search/data/models/business_search_model/business_search_model.dart';
import 'package:flutter_tdd/features/search/data/models/business_staff_model/business_staff_model.dart';
import 'package:flutter_tdd/features/search/data/models/chat_model/chat_model.dart';
import 'package:flutter_tdd/features/search/data/models/count_chat_model/count_chat_model.dart';
import 'package:flutter_tdd/features/search/data/models/home_banners/home_banners_model.dart';
import 'package:flutter_tdd/features/search/data/models/hot_deals_model/hot_deals_model.dart';
import 'package:flutter_tdd/features/search/data/models/note_model/notes_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_cat_model/popular_cat_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_search_model/popular_search_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_services_model/popular_services_model.dart';
import 'package:flutter_tdd/features/search/data/models/reviews_model/reviews_model.dart';
import 'package:flutter_tdd/features/search/data/models/service_details_model/service_details_model.dart';
import 'package:flutter_tdd/features/search/data/models/service_search_model/service_search_model.dart';
import 'package:flutter_tdd/features/search/data/models/status_model/status_model.dart';
import 'package:flutter_tdd/features/search/data/models/top_businesses_model/top_businesses_model.dart';
import 'package:flutter_tdd/features/search/data/models/trinding_model/trinding_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/add_msg_chat_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/appointment_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/appointment_order_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/banners_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_image_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_staff_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/cancel_order_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/edit_note_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/get_notes_params.dart';
import 'package:flutter_tdd/features/search/domain/entites/hot_deals_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/msg_count_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/note_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/search_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/service_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/services_rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/sevice_search_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:flutter_tdd/features/search/domain/entites/trinding_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';
import 'package:injectable/injectable.dart';


@Injectable(as: BaseRepository)
class ImplBaseRepository extends BaseRepository {

  @override
  Future<Either<Failure, HotDealsModel>> getHotDeals(
      HotDealsPrams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getHotDeals(param);
  }

  @override
  Future<Either<Failure, TrindingModel>> getTrending(
      TrendingPrams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getTrending(param);
  }

  @override
  Future<Either<Failure, List<PopularServicesModel>>> getPopular(
      bool param) async {
    return await getIt<ImplHomeRemoteDataSource>().getPopular(param);
  }

  @override
  Future<Either<Failure, List<PopularSearchModel>>> getPopularSearch(
      bool param) async {
    return await getIt<ImplHomeRemoteDataSource>().getPopularSearch(param);
  }

  @override
  Future<Either<Failure, List<PopularSearchModel>>> getRecentSearch(
      bool param) async {
    return await getIt<ImplHomeRemoteDataSource>().getRecentSearch(param);
  }

  @override
  Future<Either<Failure, BusinessSearchModel>> getAllSearch(
      SearchPrams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getAllSearch(param);
  }

  @override
  Future<Either<Failure, ServiceDetailsModel>> getServiceDetails(
      ServiceDetailsPrams prams) async {
    return await getIt<ImplHomeRemoteDataSource>().getServiceDetails(prams);
  }

  @override
  Future<Either<Failure, ReviewsModel>> getServiceReviews(
      ServiceDetailsPrams prams) async {
    return await getIt<ImplHomeRemoteDataSource>().getServiceReviews(prams);
  }

  @override
  Future<Either<Failure, ServiceSearchModel>> getAllServiceSearch(
      ServiceSearchParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getAllServiceSearch(param);
  }

  @override
  Future<Either<Failure, List<AppointmentOrdersModel>>> getDashboardData(
      AppointmentOrderParams prams) async {
    return await getIt<ImplHomeRemoteDataSource>().getDashboardData(prams);
  }

  @override
  Future<Either<Failure, List<StatusModel>>> getStatus(bool prams) async {
    return await getIt<ImplHomeRemoteDataSource>().getStatus(prams);
  }

  @override
  Future<Either<Failure, AppointmentDetails>> getAppointmentDetails(
      AppointmentDetailsParams prams) async {
    return await getIt<ImplHomeRemoteDataSource>().getAppointmentDetails(prams);
  }

  @override
  Future<Either<Failure, List<BusinessStaffModel>>> getBusinessStaff(
      BusinessStaffParams prams) async {
    return await getIt<ImplHomeRemoteDataSource>().getBusinessStaff(prams);
  }

  @override
  Future<Either<Failure, bool>> cancelOrder(CancelOrderParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().cancelOrder(param);
  }

  @override
  Future<Either<Failure, bool>> cancelOrderDetails(
      CancelOrderParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().cancelOrderDetails(param);
  }

  @override
  Future<Either<Failure, List<dynamic>>> getAppointmentImage(
      BusinessImagesParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getAppointmentImage(param);
  }

  @override
  Future<Either<Failure, bool>> addNote(AddNoteParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().addNote(param);
  }

  @override
  Future<Either<Failure, List<NotesModel>>> getNotes(
      GetNoteParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getNotes(param);
  }

  @override
  Future<Either<Failure, bool>> removeAllNote(String param) async {
    return await getIt<ImplHomeRemoteDataSource>().removeAllNote(param);
  }

  @override
  Future<Either<Failure, bool>> removeNote(String param) async {
    return await getIt<ImplHomeRemoteDataSource>().removeNote(param);
  }

  @override
  Future<Either<Failure, HomeBannersModel>> getMainBanners(
      BannersParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getMainBanners(param);
  }

  @override
  Future<Either<Failure, TopBusinessesModel>> getTopBusinesses(
      TopBusinessParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getTopBusinesses(param);
  }

  @override
  Future<Either<Failure, TrindingModel>> getHomeServices(
      TrendingPrams param) async {
    return await getIt<ImplHomeRemoteDataSource>().getHomeServices(param);
  }

  @override
  Future<Either<Failure, bool>> rating(RateParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().rating(param);
  }

  @override
  Future<Either<Failure, bool>> ratingServices(ServicesRateParams param) async {
    return await getIt<ImplHomeRemoteDataSource>().ratingServices(param);
  }

  @override
  Future<Either<Failure, bool>> addMsgChat(AddMsgChatParams param) async{
    return await getIt<ImplHomeRemoteDataSource>().addMsgChat(param);
  }

  @override
  Future<Either<Failure, List<ChatModel>>> getAllMsgChat(String param) async{
    return await getIt<ImplHomeRemoteDataSource>().getAllMsgChat(param);
  }

  @override
  Future<Either<Failure, List<CountChatModel>>> getMsgCount(MsgCountParams param) async{
    return await getIt<ImplHomeRemoteDataSource>().getMsgCount(param);
  }

  @override
  Future<Either<Failure, List<PopularCatModel>>> getPopularCat(TopBusinessParams param) async{
    return await getIt<ImplHomeRemoteDataSource>().getPopularCat(param);
  }

  @override
  Future<Either<Failure, bool>> editNote(EditNoteParams param)async {
    return await getIt<ImplHomeRemoteDataSource>().editNote(param);
  }
}
