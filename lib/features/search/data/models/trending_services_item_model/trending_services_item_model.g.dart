// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trending_services_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TrendingServicesItemModel _$$_TrendingServicesItemModelFromJson(
        Map<String, dynamic> json) =>
    _$_TrendingServicesItemModel(
      businessServiceID: json['businessServiceID'] as int?,
      businessID: json['businessID'] as String,
      businessEnglishName: json['businessEnglishName'] as String,
      businessArabicName: json['businessArabicName'] as String,
      businessGender: json['businessGender'] as String? ?? '',
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      businessAddressEN: json['businessAddressEN'] as String,
      businessAddressAR: json['businessAddressAR'] as String,
      distanceText: json['distance'] as String,
      serviceID: json['serviceID'] as String,
      serviceGender: json['serviceGender'] as String?,
      eBusinessServiceID: json['eBusinessServiceID'] as String,
      businessRating: (json['businessRating'] as num).toDouble(),
      serviceDuration: json['serviceDuration'] as int,
      serviceDurationText: json['serviceDurationText'] as String,
      businessServiceEnglishName: json['businessServiceEnglishName'] as String,
      servicePrice: (json['servicePrice'] as num?)?.toDouble() ?? 0,
      businessServiceArabicName:
          json['businessServiceArabicName'] as String? ?? '',
      serviceEnglishName: json['serviceEnglishName'] as String? ?? '',
      serviceArabicName: json['serviceArabicName'] as String? ?? '',
      busserviceArabicNameinessID:
          json['busserviceArabicNameinessID'] as String? ?? '',
      serviceAvatar: json['serviceAvatar'] as String? ?? '',
      isPercentage: json['isPercentage'] as bool? ?? false,
      discountPercentage: (json['discountPercentage'] as num?)?.toDouble() ?? 0,
      discount: json['discount'] ?? 0,
      discountPercentageValidDate:
          json['discountPercentageValidDate'] as String? ?? '',
      discountPercentageServicePrice:
          (json['discountPercentageServicePrice'] as num?)?.toDouble() ?? 0,
      currencyEnglishName: json['currencyEnglishName'] as String? ?? '',
      currencyArabicName: json['currencyArabicName'] as String? ?? '',
      serviceTypeID: json['serviceTypeID'] as String? ?? '',
      hasActiveSlot: json['hasActiveSlot'] as bool?,
      serviceTypeEnglishName: json['serviceTypeEnglishName'] as String,
      serviceTypeArabicName: json['serviceTypeArabicName'] as String,
      hasWishItem: json['hasWishItem'] as bool?,
      serviceRating: (json['serviceRating'] as num).toDouble(),
    );

Map<String, dynamic> _$$_TrendingServicesItemModelToJson(
        _$_TrendingServicesItemModel instance) =>
    <String, dynamic>{
      'businessServiceID': instance.businessServiceID,
      'businessID': instance.businessID,
      'businessEnglishName': instance.businessEnglishName,
      'businessArabicName': instance.businessArabicName,
      'businessGender': instance.businessGender,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'businessAddressEN': instance.businessAddressEN,
      'businessAddressAR': instance.businessAddressAR,
      'distance': instance.distanceText,
      'serviceID': instance.serviceID,
      'serviceGender': instance.serviceGender,
      'eBusinessServiceID': instance.eBusinessServiceID,
      'businessRating': instance.businessRating,
      'serviceDuration': instance.serviceDuration,
      'serviceDurationText': instance.serviceDurationText,
      'businessServiceEnglishName': instance.businessServiceEnglishName,
      'servicePrice': instance.servicePrice,
      'businessServiceArabicName': instance.businessServiceArabicName,
      'serviceEnglishName': instance.serviceEnglishName,
      'serviceArabicName': instance.serviceArabicName,
      'busserviceArabicNameinessID': instance.busserviceArabicNameinessID,
      'serviceAvatar': instance.serviceAvatar,
      'isPercentage': instance.isPercentage,
      'discountPercentage': instance.discountPercentage,
      'discount': instance.discount,
      'discountPercentageValidDate': instance.discountPercentageValidDate,
      'discountPercentageServicePrice': instance.discountPercentageServicePrice,
      'currencyEnglishName': instance.currencyEnglishName,
      'currencyArabicName': instance.currencyArabicName,
      'serviceTypeID': instance.serviceTypeID,
      'hasActiveSlot': instance.hasActiveSlot,
      'serviceTypeEnglishName': instance.serviceTypeEnglishName,
      'serviceTypeArabicName': instance.serviceTypeArabicName,
      'hasWishItem': instance.hasWishItem,
      'serviceRating': instance.serviceRating,
    };
