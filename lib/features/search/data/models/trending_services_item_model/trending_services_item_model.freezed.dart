// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'trending_services_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TrendingServicesItemModel _$TrendingServicesItemModelFromJson(
    Map<String, dynamic> json) {
  return _TrendingServicesItemModel.fromJson(json);
}

/// @nodoc
mixin _$TrendingServicesItemModel {
  int? get businessServiceID => throw _privateConstructorUsedError;
  set businessServiceID(int? value) => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  set businessID(String value) => throw _privateConstructorUsedError;
  String get businessEnglishName => throw _privateConstructorUsedError;
  set businessEnglishName(String value) => throw _privateConstructorUsedError;
  String get businessArabicName => throw _privateConstructorUsedError;
  set businessArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get businessGender => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set businessGender(String value) => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  set latitude(double value) => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  set longitude(double value) => throw _privateConstructorUsedError;
  String get businessAddressEN => throw _privateConstructorUsedError;
  set businessAddressEN(String value) => throw _privateConstructorUsedError;
  String get businessAddressAR => throw _privateConstructorUsedError;
  set businessAddressAR(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  String get distanceText => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  set distanceText(String value) => throw _privateConstructorUsedError;
  String get serviceID => throw _privateConstructorUsedError;
  set serviceID(String value) => throw _privateConstructorUsedError;
  String? get serviceGender => throw _privateConstructorUsedError;
  set serviceGender(String? value) => throw _privateConstructorUsedError;
  String get eBusinessServiceID => throw _privateConstructorUsedError;
  set eBusinessServiceID(String value) => throw _privateConstructorUsedError;
  double get businessRating => throw _privateConstructorUsedError;
  set businessRating(double value) => throw _privateConstructorUsedError;
  int get serviceDuration => throw _privateConstructorUsedError;
  set serviceDuration(int value) => throw _privateConstructorUsedError;
  String get serviceDurationText => throw _privateConstructorUsedError;
  set serviceDurationText(String value) => throw _privateConstructorUsedError;
  String get businessServiceEnglishName => throw _privateConstructorUsedError;
  set businessServiceEnglishName(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  double get servicePrice => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set servicePrice(double value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get businessServiceArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set businessServiceArabicName(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceEnglishName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceEnglishName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get busserviceArabicNameinessID => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set busserviceArabicNameinessID(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceAvatar => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceAvatar(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  bool get isPercentage => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  set isPercentage(bool value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  double get discountPercentage => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set discountPercentage(double value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  dynamic get discount => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set discount(dynamic value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get discountPercentageValidDate => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set discountPercentageValidDate(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  double get discountPercentageServicePrice =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set discountPercentageServicePrice(double value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get currencyEnglishName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set currencyEnglishName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get currencyArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set currencyArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceTypeID => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceTypeID(String value) => throw _privateConstructorUsedError;
  bool? get hasActiveSlot => throw _privateConstructorUsedError;
  set hasActiveSlot(bool? value) => throw _privateConstructorUsedError;
  String get serviceTypeEnglishName => throw _privateConstructorUsedError;
  set serviceTypeEnglishName(String value) =>
      throw _privateConstructorUsedError;
  String get serviceTypeArabicName => throw _privateConstructorUsedError;
  set serviceTypeArabicName(String value) => throw _privateConstructorUsedError;
  bool? get hasWishItem => throw _privateConstructorUsedError;
  set hasWishItem(bool? value) => throw _privateConstructorUsedError;
  double get serviceRating => throw _privateConstructorUsedError;
  set serviceRating(double value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TrendingServicesItemModelCopyWith<TrendingServicesItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrendingServicesItemModelCopyWith<$Res> {
  factory $TrendingServicesItemModelCopyWith(TrendingServicesItemModel value,
          $Res Function(TrendingServicesItemModel) then) =
      _$TrendingServicesItemModelCopyWithImpl<$Res>;
  $Res call(
      {int? businessServiceID,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String businessGender,
      double latitude,
      double longitude,
      String businessAddressEN,
      String businessAddressAR,
      @JsonKey(name: "distance")
          String distanceText,
      String serviceID,
      String? serviceGender,
      String eBusinessServiceID,
      double businessRating,
      int serviceDuration,
      String serviceDurationText,
      String businessServiceEnglishName,
      @JsonKey(defaultValue: 0, nullable: true)
          double servicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          String businessServiceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String busserviceArabicNameinessID,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          bool isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          double discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          dynamic discount,
      @JsonKey(defaultValue: "", nullable: true)
          String discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          double discountPercentageServicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          String currencyEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String currencyArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceTypeID,
      bool? hasActiveSlot,
      String serviceTypeEnglishName,
      String serviceTypeArabicName,
      bool? hasWishItem,
      double serviceRating});
}

/// @nodoc
class _$TrendingServicesItemModelCopyWithImpl<$Res>
    implements $TrendingServicesItemModelCopyWith<$Res> {
  _$TrendingServicesItemModelCopyWithImpl(this._value, this._then);

  final TrendingServicesItemModel _value;
  // ignore: unused_field
  final $Res Function(TrendingServicesItemModel) _then;

  @override
  $Res call({
    Object? businessServiceID = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? distanceText = freezed,
    Object? serviceID = freezed,
    Object? serviceGender = freezed,
    Object? eBusinessServiceID = freezed,
    Object? businessRating = freezed,
    Object? serviceDuration = freezed,
    Object? serviceDurationText = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? servicePrice = freezed,
    Object? businessServiceArabicName = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? busserviceArabicNameinessID = freezed,
    Object? serviceAvatar = freezed,
    Object? isPercentage = freezed,
    Object? discountPercentage = freezed,
    Object? discount = freezed,
    Object? discountPercentageValidDate = freezed,
    Object? discountPercentageServicePrice = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? serviceTypeID = freezed,
    Object? hasActiveSlot = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeArabicName = freezed,
    Object? hasWishItem = freezed,
    Object? serviceRating = freezed,
  }) {
    return _then(_value.copyWith(
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as int?,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      serviceID: serviceID == freezed
          ? _value.serviceID
          : serviceID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceGender: serviceGender == freezed
          ? _value.serviceGender
          : serviceGender // ignore: cast_nullable_to_non_nullable
              as String?,
      eBusinessServiceID: eBusinessServiceID == freezed
          ? _value.eBusinessServiceID
          : eBusinessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessRating: businessRating == freezed
          ? _value.businessRating
          : businessRating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      servicePrice: servicePrice == freezed
          ? _value.servicePrice
          : servicePrice // ignore: cast_nullable_to_non_nullable
              as double,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      busserviceArabicNameinessID: busserviceArabicNameinessID == freezed
          ? _value.busserviceArabicNameinessID
          : busserviceArabicNameinessID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAvatar: serviceAvatar == freezed
          ? _value.serviceAvatar
          : serviceAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      isPercentage: isPercentage == freezed
          ? _value.isPercentage
          : isPercentage // ignore: cast_nullable_to_non_nullable
              as bool,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as dynamic,
      discountPercentageValidDate: discountPercentageValidDate == freezed
          ? _value.discountPercentageValidDate
          : discountPercentageValidDate // ignore: cast_nullable_to_non_nullable
              as String,
      discountPercentageServicePrice: discountPercentageServicePrice == freezed
          ? _value.discountPercentageServicePrice
          : discountPercentageServicePrice // ignore: cast_nullable_to_non_nullable
              as double,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeID: serviceTypeID == freezed
          ? _value.serviceTypeID
          : serviceTypeID // ignore: cast_nullable_to_non_nullable
              as String,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool?,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool?,
      serviceRating: serviceRating == freezed
          ? _value.serviceRating
          : serviceRating // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$$_TrendingServicesItemModelCopyWith<$Res>
    implements $TrendingServicesItemModelCopyWith<$Res> {
  factory _$$_TrendingServicesItemModelCopyWith(
          _$_TrendingServicesItemModel value,
          $Res Function(_$_TrendingServicesItemModel) then) =
      __$$_TrendingServicesItemModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? businessServiceID,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String businessGender,
      double latitude,
      double longitude,
      String businessAddressEN,
      String businessAddressAR,
      @JsonKey(name: "distance")
          String distanceText,
      String serviceID,
      String? serviceGender,
      String eBusinessServiceID,
      double businessRating,
      int serviceDuration,
      String serviceDurationText,
      String businessServiceEnglishName,
      @JsonKey(defaultValue: 0, nullable: true)
          double servicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          String businessServiceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String busserviceArabicNameinessID,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          bool isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          double discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          dynamic discount,
      @JsonKey(defaultValue: "", nullable: true)
          String discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          double discountPercentageServicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          String currencyEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String currencyArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String serviceTypeID,
      bool? hasActiveSlot,
      String serviceTypeEnglishName,
      String serviceTypeArabicName,
      bool? hasWishItem,
      double serviceRating});
}

/// @nodoc
class __$$_TrendingServicesItemModelCopyWithImpl<$Res>
    extends _$TrendingServicesItemModelCopyWithImpl<$Res>
    implements _$$_TrendingServicesItemModelCopyWith<$Res> {
  __$$_TrendingServicesItemModelCopyWithImpl(
      _$_TrendingServicesItemModel _value,
      $Res Function(_$_TrendingServicesItemModel) _then)
      : super(_value, (v) => _then(v as _$_TrendingServicesItemModel));

  @override
  _$_TrendingServicesItemModel get _value =>
      super._value as _$_TrendingServicesItemModel;

  @override
  $Res call({
    Object? businessServiceID = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? distanceText = freezed,
    Object? serviceID = freezed,
    Object? serviceGender = freezed,
    Object? eBusinessServiceID = freezed,
    Object? businessRating = freezed,
    Object? serviceDuration = freezed,
    Object? serviceDurationText = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? servicePrice = freezed,
    Object? businessServiceArabicName = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? busserviceArabicNameinessID = freezed,
    Object? serviceAvatar = freezed,
    Object? isPercentage = freezed,
    Object? discountPercentage = freezed,
    Object? discount = freezed,
    Object? discountPercentageValidDate = freezed,
    Object? discountPercentageServicePrice = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? serviceTypeID = freezed,
    Object? hasActiveSlot = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeArabicName = freezed,
    Object? hasWishItem = freezed,
    Object? serviceRating = freezed,
  }) {
    return _then(_$_TrendingServicesItemModel(
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as int?,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      serviceID: serviceID == freezed
          ? _value.serviceID
          : serviceID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceGender: serviceGender == freezed
          ? _value.serviceGender
          : serviceGender // ignore: cast_nullable_to_non_nullable
              as String?,
      eBusinessServiceID: eBusinessServiceID == freezed
          ? _value.eBusinessServiceID
          : eBusinessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessRating: businessRating == freezed
          ? _value.businessRating
          : businessRating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      servicePrice: servicePrice == freezed
          ? _value.servicePrice
          : servicePrice // ignore: cast_nullable_to_non_nullable
              as double,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      busserviceArabicNameinessID: busserviceArabicNameinessID == freezed
          ? _value.busserviceArabicNameinessID
          : busserviceArabicNameinessID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAvatar: serviceAvatar == freezed
          ? _value.serviceAvatar
          : serviceAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      isPercentage: isPercentage == freezed
          ? _value.isPercentage
          : isPercentage // ignore: cast_nullable_to_non_nullable
              as bool,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as dynamic,
      discountPercentageValidDate: discountPercentageValidDate == freezed
          ? _value.discountPercentageValidDate
          : discountPercentageValidDate // ignore: cast_nullable_to_non_nullable
              as String,
      discountPercentageServicePrice: discountPercentageServicePrice == freezed
          ? _value.discountPercentageServicePrice
          : discountPercentageServicePrice // ignore: cast_nullable_to_non_nullable
              as double,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeID: serviceTypeID == freezed
          ? _value.serviceTypeID
          : serviceTypeID // ignore: cast_nullable_to_non_nullable
              as String,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool?,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool?,
      serviceRating: serviceRating == freezed
          ? _value.serviceRating
          : serviceRating // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_TrendingServicesItemModel extends _TrendingServicesItemModel {
  _$_TrendingServicesItemModel(
      {required this.businessServiceID,
      required this.businessID,
      required this.businessEnglishName,
      required this.businessArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.businessGender,
      required this.latitude,
      required this.longitude,
      required this.businessAddressEN,
      required this.businessAddressAR,
      @JsonKey(name: "distance")
          required this.distanceText,
      required this.serviceID,
      required this.serviceGender,
      required this.eBusinessServiceID,
      required this.businessRating,
      required this.serviceDuration,
      required this.serviceDurationText,
      required this.businessServiceEnglishName,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.servicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          required this.businessServiceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.serviceEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.busserviceArabicNameinessID,
      @JsonKey(defaultValue: "", nullable: true)
          required this.serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          required this.isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.discount,
      @JsonKey(defaultValue: "", nullable: true)
          required this.discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.discountPercentageServicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          required this.currencyEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.currencyArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.serviceTypeID,
      required this.hasActiveSlot,
      required this.serviceTypeEnglishName,
      required this.serviceTypeArabicName,
      required this.hasWishItem,
      required this.serviceRating})
      : super._();

  factory _$_TrendingServicesItemModel.fromJson(Map<String, dynamic> json) =>
      _$$_TrendingServicesItemModelFromJson(json);

  @override
  int? businessServiceID;
  @override
  String businessID;
  @override
  String businessEnglishName;
  @override
  String businessArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String businessGender;
  @override
  double latitude;
  @override
  double longitude;
  @override
  String businessAddressEN;
  @override
  String businessAddressAR;
  @override
  @JsonKey(name: "distance")
  String distanceText;
  @override
  String serviceID;
  @override
  String? serviceGender;
  @override
  String eBusinessServiceID;
  @override
  double businessRating;
  @override
  int serviceDuration;
  @override
  String serviceDurationText;
  @override
  String businessServiceEnglishName;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double servicePrice;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String businessServiceArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String serviceEnglishName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String serviceArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String busserviceArabicNameinessID;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String serviceAvatar;
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool isPercentage;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double discountPercentage;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  dynamic discount;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String discountPercentageValidDate;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double discountPercentageServicePrice;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String currencyEnglishName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String currencyArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String serviceTypeID;
  @override
  bool? hasActiveSlot;
  @override
  String serviceTypeEnglishName;
  @override
  String serviceTypeArabicName;
  @override
  bool? hasWishItem;
  @override
  double serviceRating;

  @override
  String toString() {
    return 'TrendingServicesItemModel(businessServiceID: $businessServiceID, businessID: $businessID, businessEnglishName: $businessEnglishName, businessArabicName: $businessArabicName, businessGender: $businessGender, latitude: $latitude, longitude: $longitude, businessAddressEN: $businessAddressEN, businessAddressAR: $businessAddressAR, distanceText: $distanceText, serviceID: $serviceID, serviceGender: $serviceGender, eBusinessServiceID: $eBusinessServiceID, businessRating: $businessRating, serviceDuration: $serviceDuration, serviceDurationText: $serviceDurationText, businessServiceEnglishName: $businessServiceEnglishName, servicePrice: $servicePrice, businessServiceArabicName: $businessServiceArabicName, serviceEnglishName: $serviceEnglishName, serviceArabicName: $serviceArabicName, busserviceArabicNameinessID: $busserviceArabicNameinessID, serviceAvatar: $serviceAvatar, isPercentage: $isPercentage, discountPercentage: $discountPercentage, discount: $discount, discountPercentageValidDate: $discountPercentageValidDate, discountPercentageServicePrice: $discountPercentageServicePrice, currencyEnglishName: $currencyEnglishName, currencyArabicName: $currencyArabicName, serviceTypeID: $serviceTypeID, hasActiveSlot: $hasActiveSlot, serviceTypeEnglishName: $serviceTypeEnglishName, serviceTypeArabicName: $serviceTypeArabicName, hasWishItem: $hasWishItem, serviceRating: $serviceRating)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_TrendingServicesItemModelCopyWith<_$_TrendingServicesItemModel>
      get copyWith => __$$_TrendingServicesItemModelCopyWithImpl<
          _$_TrendingServicesItemModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TrendingServicesItemModelToJson(
      this,
    );
  }
}

abstract class _TrendingServicesItemModel extends TrendingServicesItemModel {
  factory _TrendingServicesItemModel(
      {required int? businessServiceID,
      required String businessID,
      required String businessEnglishName,
      required String businessArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required String businessGender,
      required double latitude,
      required double longitude,
      required String businessAddressEN,
      required String businessAddressAR,
      @JsonKey(name: "distance")
          required String distanceText,
      required String serviceID,
      required String? serviceGender,
      required String eBusinessServiceID,
      required double businessRating,
      required int serviceDuration,
      required String serviceDurationText,
      required String businessServiceEnglishName,
      @JsonKey(defaultValue: 0, nullable: true)
          required double servicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          required String businessServiceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required String serviceEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required String serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required String busserviceArabicNameinessID,
      @JsonKey(defaultValue: "", nullable: true)
          required String serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          required bool isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required double discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required dynamic discount,
      @JsonKey(defaultValue: "", nullable: true)
          required String discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          required double discountPercentageServicePrice,
      @JsonKey(defaultValue: "", nullable: true)
          required String currencyEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required String currencyArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required String serviceTypeID,
      required bool? hasActiveSlot,
      required String serviceTypeEnglishName,
      required String serviceTypeArabicName,
      required bool? hasWishItem,
      required double serviceRating}) = _$_TrendingServicesItemModel;
  _TrendingServicesItemModel._() : super._();

  factory _TrendingServicesItemModel.fromJson(Map<String, dynamic> json) =
      _$_TrendingServicesItemModel.fromJson;

  @override
  int? get businessServiceID;
  set businessServiceID(int? value);
  @override
  String get businessID;
  set businessID(String value);
  @override
  String get businessEnglishName;
  set businessEnglishName(String value);
  @override
  String get businessArabicName;
  set businessArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get businessGender;
  @JsonKey(defaultValue: "", nullable: true)
  set businessGender(String value);
  @override
  double get latitude;
  set latitude(double value);
  @override
  double get longitude;
  set longitude(double value);
  @override
  String get businessAddressEN;
  set businessAddressEN(String value);
  @override
  String get businessAddressAR;
  set businessAddressAR(String value);
  @override
  @JsonKey(name: "distance")
  String get distanceText;
  @JsonKey(name: "distance")
  set distanceText(String value);
  @override
  String get serviceID;
  set serviceID(String value);
  @override
  String? get serviceGender;
  set serviceGender(String? value);
  @override
  String get eBusinessServiceID;
  set eBusinessServiceID(String value);
  @override
  double get businessRating;
  set businessRating(double value);
  @override
  int get serviceDuration;
  set serviceDuration(int value);
  @override
  String get serviceDurationText;
  set serviceDurationText(String value);
  @override
  String get businessServiceEnglishName;
  set businessServiceEnglishName(String value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double get servicePrice;
  @JsonKey(defaultValue: 0, nullable: true)
  set servicePrice(double value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get businessServiceArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set businessServiceArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceEnglishName;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceEnglishName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get busserviceArabicNameinessID;
  @JsonKey(defaultValue: "", nullable: true)
  set busserviceArabicNameinessID(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceAvatar;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceAvatar(String value);
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool get isPercentage;
  @JsonKey(defaultValue: false, nullable: true)
  set isPercentage(bool value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double get discountPercentage;
  @JsonKey(defaultValue: 0, nullable: true)
  set discountPercentage(double value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  dynamic get discount;
  @JsonKey(defaultValue: 0, nullable: true)
  set discount(dynamic value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get discountPercentageValidDate;
  @JsonKey(defaultValue: "", nullable: true)
  set discountPercentageValidDate(String value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double get discountPercentageServicePrice;
  @JsonKey(defaultValue: 0, nullable: true)
  set discountPercentageServicePrice(double value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get currencyEnglishName;
  @JsonKey(defaultValue: "", nullable: true)
  set currencyEnglishName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get currencyArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set currencyArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceTypeID;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceTypeID(String value);
  @override
  bool? get hasActiveSlot;
  set hasActiveSlot(bool? value);
  @override
  String get serviceTypeEnglishName;
  set serviceTypeEnglishName(String value);
  @override
  String get serviceTypeArabicName;
  set serviceTypeArabicName(String value);
  @override
  bool? get hasWishItem;
  set hasWishItem(bool? value);
  @override
  double get serviceRating;
  set serviceRating(double value);
  @override
  @JsonKey(ignore: true)
  _$$_TrendingServicesItemModelCopyWith<_$_TrendingServicesItemModel>
      get copyWith => throw _privateConstructorUsedError;
}
