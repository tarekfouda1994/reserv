import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'trending_services_item_model.freezed.dart';
part 'trending_services_item_model.g.dart';

@unfreezed
class TrendingServicesItemModel with _$TrendingServicesItemModel {
  TrendingServicesItemModel._();

  @JsonSerializable(explicitToJson: true)
  factory TrendingServicesItemModel({
    required int? businessServiceID,
    required String businessID,
    required String businessEnglishName,
    required String businessArabicName,
    @JsonKey(defaultValue: "", nullable: true) required String businessGender,
    required double latitude,
    required double longitude,
    required String businessAddressEN,
    required String businessAddressAR,
    @JsonKey(name: "distance") required String distanceText,
    required String serviceID,
    required String? serviceGender,
    required String eBusinessServiceID,
    required double businessRating,
    required int serviceDuration,
    required String serviceDurationText,
    required String businessServiceEnglishName,
    @JsonKey(defaultValue: 0, nullable: true) required double servicePrice,
    @JsonKey(defaultValue: "", nullable: true)
        required String businessServiceArabicName,
    @JsonKey(defaultValue: "", nullable: true)
        required String serviceEnglishName,
    @JsonKey(defaultValue: "", nullable: true)
        required String serviceArabicName,
    @JsonKey(defaultValue: "", nullable: true)
        required String busserviceArabicNameinessID,
    @JsonKey(defaultValue: "", nullable: true) required String serviceAvatar,
    @JsonKey(defaultValue: false, nullable: true) required bool isPercentage,
    @JsonKey(defaultValue: 0, nullable: true)
        required double discountPercentage,
    @JsonKey(defaultValue: 0, nullable: true) required dynamic discount,
    @JsonKey(defaultValue: "", nullable: true)
        required String discountPercentageValidDate,
    @JsonKey(defaultValue: 0, nullable: true)
        required double discountPercentageServicePrice,
    @JsonKey(defaultValue: "", nullable: true)
        required String currencyEnglishName,
    @JsonKey(defaultValue: "", nullable: true)
        required String currencyArabicName,
    @JsonKey(defaultValue: "", nullable: true) required String serviceTypeID,
    required bool? hasActiveSlot,
    required String serviceTypeEnglishName,
    required String serviceTypeArabicName,
    required bool? hasWishItem,
    required double serviceRating,
  }) = _TrendingServicesItemModel;

  factory TrendingServicesItemModel.fromJson(Map<String, dynamic> json) =>
      _$TrendingServicesItemModelFromJson(json);

  factory TrendingServicesItemModel.fromMap(Map<String, dynamic> json) =>
      TrendingServicesItemModel(
        serviceRating: json["serviceRating"] ?? 0,
        businessServiceID: json["businessServiceID"] ?? 0,
        businessID: json["businessID"] ?? "",
        businessEnglishName: json["businessEnglishName"],
        businessArabicName: json["businessArabicName"],
        businessGender: json["businessGender"] ?? "",
        serviceGender: json["serviceGender"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        businessAddressEN: json["businessAddressEN"],
        businessAddressAR: json["businessAddressAR"],
        distanceText: json["distance"],
        serviceID: json["serviceID"],
        eBusinessServiceID: json["eBusinessServiceID"],
        businessServiceEnglishName: json["businessServiceEnglishName"],
        servicePrice: json["servicePrice"],
        businessServiceArabicName: json["businessServiceArabicName"],
        serviceEnglishName: json["serviceEnglishName"],
        serviceArabicName: json["serviceArabicName"],
        busserviceArabicNameinessID: json["busserviceArabicNameinessID"] ?? "",
        serviceAvatar: json["serviceAvatar"],
        isPercentage: json["isPercentage"] ?? false,
        discountPercentage: json["discountPercentage"] ?? "",
        discount: num.parse((json["discount"] ?? "0").toString()),
        discountPercentageValidDate: json["discountPercentageValidDate"] ?? "",
        discountPercentageServicePrice:
            (json["discountPercentageServicePrice"] ?? 0),
        currencyEnglishName: json["currencyEnglishName"],
        currencyArabicName: json["currencyArabicName"],
        serviceTypeID: json["serviceTypeID"] ?? 0,
        serviceTypeEnglishName: json["serviceTypeEnglishName"],
        serviceTypeArabicName: json["serviceTypeArabicName"],
        hasWishItem: json["hasWishItem"] ?? false,
        hasActiveSlot: json["hasActiveSlot"] ?? false,
        businessRating: json["businessRating"] ?? 0,
        serviceDurationText: json["serviceDurationText"] ?? "",
        serviceDuration: json["serviceDuration"] ?? 0,
      );

  String getServiceName() {
    return getIt<Utilities>()
        .getLocalizedValue(serviceArabicName, serviceEnglishName);
  }

  String getServicesAddress() {
    return getIt<Utilities>()
        .getLocalizedValue(businessAddressAR, businessAddressEN);
  }

  String getBusiness() {
    return getIt<Utilities>()
        .getLocalizedValue(businessArabicName, businessEnglishName);
  }

  String getCurrencyName() {
    return getIt<Utilities>()
        .getLocalizedValue(currencyArabicName, currencyEnglishName);
  }

  String get _distance {
    BuildContext context = getIt<GlobalContext>().context();
    var distance = getIt<Utilities>()
        .convertNumToAr(context: context, value: distanceText.toString().split(" ").first, isDistance: true);
    if (distance == "0" || distance == "" || distance == "0.0") {
      return "";
    } else {
      return distance + " " + tr("km");
    }
  }

  String getServiceGender() {
    switch (serviceGender ?? "") {
      case "1":
        return tr("men");
      case "2":
        return tr("women");
      default:
        return "";
    }
  }

  String get durationToString {
    var duration = Duration(minutes: serviceDuration);
    List<String> parts = duration.toString().split(':');
    if (parts[0].replaceAll("0", "").trim().isEmpty &&
        parts[1].replaceAll("0", "").trim().isNotEmpty) {
      return '${parts[1].padLeft(2, '0')} ${tr("min")}';
    } else if (parts[0].replaceAll("0", "").trim().isNotEmpty &&
        parts[1].replaceAll("0", "").trim().isEmpty) {
      return parts[0].padRight(2, ' ${tr("h")}');
    }
    return '${parts[0].padRight(2, ' ${tr("h")}')}: ${parts[1].padLeft(2, '0')} ${tr("min")}';
  }

  String distance(BuildContext context) {
    if (_distance.isEmpty) {
      return "";
    } else {
      var device = context.read<DeviceCubit>().state.model;
      var isEn = device.locale == Locale('en', 'US');
      if (isEn) {
        return "$_distance ${tr("away")}";
      } else {
        return "${tr("away")} $_distance";
      }
    }
  }
}
