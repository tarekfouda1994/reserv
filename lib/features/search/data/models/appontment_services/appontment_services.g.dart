// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appontment_services.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppontmentServices _$$_AppontmentServicesFromJson(
        Map<String, dynamic> json) =>
    _$_AppontmentServices(
      serviceId: json['serviceId'] as String? ?? '',
      serviceNameEN: json['businessServiceEnglishName'] as String,
      serviceNameAR: json['businessServiceArabicName'] as String,
      amount: json['amount'] as num? ?? 0,
      statusId: json['orderStatusId'] as int,
      businessId: json['businessId'] as String,
      completionDate: json['completionDate'] as String? ?? '',
      remarks: json['remarks'] as String,
      appointmentDate: json['appointmentDate'] as String,
      fromTime: json['fromTime'] as String,
      toTime: json['toTime'] as String,
      slotTime: json['ServiceDurationText'] as String? ?? '',
      orderStatus: json['orderStatus'] as int?,
      serviceEn: json['serviceEnglishName'] as String,
      serviceAr: json['serviceArabicName'] as String,
      appointmentId: json['appointmentId'] as String,
      serviceDurationText: json['serviceDurationText'] as String,
      serviceDuration: json['serviceDuration'] as int,
      staffID: json['staffID'] as String,
      formattedAppointmentDate: json['formattedAppointmentDate'] as String,
      businessServiceGender: json['businessServiceGender'] as String? ?? '',
      detailID: json['detailID'] as String,
      orderStatusEN: json['orderStatusEN'] as String,
      orderStatusAR: json['orderStatusAR'] as String?,
      businessServiceID: json['businessServiceID'] as String,
      businessServiceImage: json['businessServiceImage'] as String? ?? '',
      hasWishItem: json['hasWishItem'] as bool,
      cancelled: json['cancelled'] as bool,
      isActive: json['isActive'] as bool,
      staffEnglishName: json['staffEnglishName'] as String? ?? '',
      staffAvatar: json['staffAvatar'] as String? ?? '',
      staffArabicName: json['staffArabicName'] as String? ?? '',
    );

Map<String, dynamic> _$$_AppontmentServicesToJson(
        _$_AppontmentServices instance) =>
    <String, dynamic>{
      'serviceId': instance.serviceId,
      'businessServiceEnglishName': instance.serviceNameEN,
      'businessServiceArabicName': instance.serviceNameAR,
      'amount': instance.amount,
      'orderStatusId': instance.statusId,
      'businessId': instance.businessId,
      'completionDate': instance.completionDate,
      'remarks': instance.remarks,
      'appointmentDate': instance.appointmentDate,
      'fromTime': instance.fromTime,
      'toTime': instance.toTime,
      'ServiceDurationText': instance.slotTime,
      'orderStatus': instance.orderStatus,
      'serviceEnglishName': instance.serviceEn,
      'serviceArabicName': instance.serviceAr,
      'appointmentId': instance.appointmentId,
      'serviceDurationText': instance.serviceDurationText,
      'serviceDuration': instance.serviceDuration,
      'staffID': instance.staffID,
      'formattedAppointmentDate': instance.formattedAppointmentDate,
      'businessServiceGender': instance.businessServiceGender,
      'detailID': instance.detailID,
      'orderStatusEN': instance.orderStatusEN,
      'orderStatusAR': instance.orderStatusAR,
      'businessServiceID': instance.businessServiceID,
      'businessServiceImage': instance.businessServiceImage,
      'hasWishItem': instance.hasWishItem,
      'cancelled': instance.cancelled,
      'isActive': instance.isActive,
      'staffEnglishName': instance.staffEnglishName,
      'staffAvatar': instance.staffAvatar,
      'staffArabicName': instance.staffArabicName,
    };
