import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'appontment_services.freezed.dart';

part 'appontment_services.g.dart';

@unfreezed
class AppontmentServices with _$AppontmentServices {
  AppontmentServices._();

  @JsonSerializable(explicitToJson: true)
  factory AppontmentServices({
    @JsonKey(nullable: true, defaultValue: "") required String serviceId,
    @JsonKey(name: "businessServiceEnglishName") required String serviceNameEN,
    @JsonKey(name: "businessServiceArabicName") required String serviceNameAR,
    @JsonKey(nullable: true, defaultValue: 0) required num amount,
    @JsonKey(name: "orderStatusId") required int statusId,
    required String businessId,
    @JsonKey(defaultValue: "", nullable: true) required String completionDate,
    required String remarks,
    required String appointmentDate,
    required String fromTime,
    required String toTime,
    @JsonKey(name: "ServiceDurationText", defaultValue: "")
        required String slotTime,
    required int? orderStatus,
    @JsonKey(name: "serviceEnglishName") required String serviceEn,
    @JsonKey(name: "serviceArabicName") required String serviceAr,
    required String appointmentId,
    required String serviceDurationText,
    required int serviceDuration,
    required String staffID,
    required String formattedAppointmentDate,
    @JsonKey(nullable: true, defaultValue: "")
        required String businessServiceGender,
    required String detailID,
    required String orderStatusEN,
    required String? orderStatusAR,
    required String businessServiceID,
    @JsonKey(nullable: true, defaultValue: "")
        required String businessServiceImage,
    required bool hasWishItem,
    required bool cancelled,
    required bool isActive,
    @JsonKey(defaultValue: "", nullable: true) required String staffEnglishName,
    @JsonKey(defaultValue: "", nullable: true) required String staffAvatar,
    @JsonKey(defaultValue: "", nullable: true) required String staffArabicName,
  }) = _AppontmentServices;

  factory AppontmentServices.fromJson(Map<String, dynamic> json) =>
      _$AppontmentServicesFromJson(json);

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(serviceAr, serviceEn);
  }

  String getStaffName() {
    return getIt<Utilities>()
        .getLocalizedValue(staffArabicName, staffEnglishName);
  }

  String getStatus() {
    return getIt<Utilities>()
        .getLocalizedValue(orderStatusAR ?? "", orderStatusEN);
  }
}
