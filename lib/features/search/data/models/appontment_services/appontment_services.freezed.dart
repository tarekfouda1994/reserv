// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'appontment_services.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppontmentServices _$AppontmentServicesFromJson(Map<String, dynamic> json) {
  return _AppontmentServices.fromJson(json);
}

/// @nodoc
mixin _$AppontmentServices {
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceId => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceId(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "businessServiceEnglishName")
  String get serviceNameEN => throw _privateConstructorUsedError;
  @JsonKey(name: "businessServiceEnglishName")
  set serviceNameEN(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "businessServiceArabicName")
  String get serviceNameAR => throw _privateConstructorUsedError;
  @JsonKey(name: "businessServiceArabicName")
  set serviceNameAR(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  num get amount => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  set amount(num value) => throw _privateConstructorUsedError;
  @JsonKey(name: "orderStatusId")
  int get statusId => throw _privateConstructorUsedError;
  @JsonKey(name: "orderStatusId")
  set statusId(int value) => throw _privateConstructorUsedError;
  String get businessId => throw _privateConstructorUsedError;
  set businessId(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get completionDate => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set completionDate(String value) => throw _privateConstructorUsedError;
  String get remarks => throw _privateConstructorUsedError;
  set remarks(String value) => throw _privateConstructorUsedError;
  String get appointmentDate => throw _privateConstructorUsedError;
  set appointmentDate(String value) => throw _privateConstructorUsedError;
  String get fromTime => throw _privateConstructorUsedError;
  set fromTime(String value) => throw _privateConstructorUsedError;
  String get toTime => throw _privateConstructorUsedError;
  set toTime(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "ServiceDurationText", defaultValue: "")
  String get slotTime => throw _privateConstructorUsedError;
  @JsonKey(name: "ServiceDurationText", defaultValue: "")
  set slotTime(String value) => throw _privateConstructorUsedError;
  int? get orderStatus => throw _privateConstructorUsedError;
  set orderStatus(int? value) => throw _privateConstructorUsedError;
  @JsonKey(name: "serviceEnglishName")
  String get serviceEn => throw _privateConstructorUsedError;
  @JsonKey(name: "serviceEnglishName")
  set serviceEn(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "serviceArabicName")
  String get serviceAr => throw _privateConstructorUsedError;
  @JsonKey(name: "serviceArabicName")
  set serviceAr(String value) => throw _privateConstructorUsedError;
  String get appointmentId => throw _privateConstructorUsedError;
  set appointmentId(String value) => throw _privateConstructorUsedError;
  String get serviceDurationText => throw _privateConstructorUsedError;
  set serviceDurationText(String value) => throw _privateConstructorUsedError;
  int get serviceDuration => throw _privateConstructorUsedError;
  set serviceDuration(int value) => throw _privateConstructorUsedError;
  String get staffID => throw _privateConstructorUsedError;
  set staffID(String value) => throw _privateConstructorUsedError;
  String get formattedAppointmentDate => throw _privateConstructorUsedError;
  set formattedAppointmentDate(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get businessServiceGender => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set businessServiceGender(String value) => throw _privateConstructorUsedError;
  String get detailID => throw _privateConstructorUsedError;
  set detailID(String value) => throw _privateConstructorUsedError;
  String get orderStatusEN => throw _privateConstructorUsedError;
  set orderStatusEN(String value) => throw _privateConstructorUsedError;
  String? get orderStatusAR => throw _privateConstructorUsedError;
  set orderStatusAR(String? value) => throw _privateConstructorUsedError;
  String get businessServiceID => throw _privateConstructorUsedError;
  set businessServiceID(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get businessServiceImage => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set businessServiceImage(String value) => throw _privateConstructorUsedError;
  bool get hasWishItem => throw _privateConstructorUsedError;
  set hasWishItem(bool value) => throw _privateConstructorUsedError;
  bool get cancelled => throw _privateConstructorUsedError;
  set cancelled(bool value) => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  set isActive(bool value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get staffEnglishName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set staffEnglishName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get staffAvatar => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set staffAvatar(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get staffArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set staffArabicName(String value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppontmentServicesCopyWith<AppontmentServices> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppontmentServicesCopyWith<$Res> {
  factory $AppontmentServicesCopyWith(
          AppontmentServices value, $Res Function(AppontmentServices) then) =
      _$AppontmentServicesCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(nullable: true, defaultValue: "") String serviceId,
      @JsonKey(name: "businessServiceEnglishName") String serviceNameEN,
      @JsonKey(name: "businessServiceArabicName") String serviceNameAR,
      @JsonKey(nullable: true, defaultValue: 0) num amount,
      @JsonKey(name: "orderStatusId") int statusId,
      String businessId,
      @JsonKey(defaultValue: "", nullable: true) String completionDate,
      String remarks,
      String appointmentDate,
      String fromTime,
      String toTime,
      @JsonKey(name: "ServiceDurationText", defaultValue: "") String slotTime,
      int? orderStatus,
      @JsonKey(name: "serviceEnglishName") String serviceEn,
      @JsonKey(name: "serviceArabicName") String serviceAr,
      String appointmentId,
      String serviceDurationText,
      int serviceDuration,
      String staffID,
      String formattedAppointmentDate,
      @JsonKey(nullable: true, defaultValue: "") String businessServiceGender,
      String detailID,
      String orderStatusEN,
      String? orderStatusAR,
      String businessServiceID,
      @JsonKey(nullable: true, defaultValue: "") String businessServiceImage,
      bool hasWishItem,
      bool cancelled,
      bool isActive,
      @JsonKey(defaultValue: "", nullable: true) String staffEnglishName,
      @JsonKey(defaultValue: "", nullable: true) String staffAvatar,
      @JsonKey(defaultValue: "", nullable: true) String staffArabicName});
}

/// @nodoc
class _$AppontmentServicesCopyWithImpl<$Res>
    implements $AppontmentServicesCopyWith<$Res> {
  _$AppontmentServicesCopyWithImpl(this._value, this._then);

  final AppontmentServices _value;
  // ignore: unused_field
  final $Res Function(AppontmentServices) _then;

  @override
  $Res call({
    Object? serviceId = freezed,
    Object? serviceNameEN = freezed,
    Object? serviceNameAR = freezed,
    Object? amount = freezed,
    Object? statusId = freezed,
    Object? businessId = freezed,
    Object? completionDate = freezed,
    Object? remarks = freezed,
    Object? appointmentDate = freezed,
    Object? fromTime = freezed,
    Object? toTime = freezed,
    Object? slotTime = freezed,
    Object? orderStatus = freezed,
    Object? serviceEn = freezed,
    Object? serviceAr = freezed,
    Object? appointmentId = freezed,
    Object? serviceDurationText = freezed,
    Object? serviceDuration = freezed,
    Object? staffID = freezed,
    Object? formattedAppointmentDate = freezed,
    Object? businessServiceGender = freezed,
    Object? detailID = freezed,
    Object? orderStatusEN = freezed,
    Object? orderStatusAR = freezed,
    Object? businessServiceID = freezed,
    Object? businessServiceImage = freezed,
    Object? hasWishItem = freezed,
    Object? cancelled = freezed,
    Object? isActive = freezed,
    Object? staffEnglishName = freezed,
    Object? staffAvatar = freezed,
    Object? staffArabicName = freezed,
  }) {
    return _then(_value.copyWith(
      serviceId: serviceId == freezed
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
      serviceNameEN: serviceNameEN == freezed
          ? _value.serviceNameEN
          : serviceNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      serviceNameAR: serviceNameAR == freezed
          ? _value.serviceNameAR
          : serviceNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      amount: amount == freezed
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as num,
      statusId: statusId == freezed
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      businessId: businessId == freezed
          ? _value.businessId
          : businessId // ignore: cast_nullable_to_non_nullable
              as String,
      completionDate: completionDate == freezed
          ? _value.completionDate
          : completionDate // ignore: cast_nullable_to_non_nullable
              as String,
      remarks: remarks == freezed
          ? _value.remarks
          : remarks // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentDate: appointmentDate == freezed
          ? _value.appointmentDate
          : appointmentDate // ignore: cast_nullable_to_non_nullable
              as String,
      fromTime: fromTime == freezed
          ? _value.fromTime
          : fromTime // ignore: cast_nullable_to_non_nullable
              as String,
      toTime: toTime == freezed
          ? _value.toTime
          : toTime // ignore: cast_nullable_to_non_nullable
              as String,
      slotTime: slotTime == freezed
          ? _value.slotTime
          : slotTime // ignore: cast_nullable_to_non_nullable
              as String,
      orderStatus: orderStatus == freezed
          ? _value.orderStatus
          : orderStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      serviceEn: serviceEn == freezed
          ? _value.serviceEn
          : serviceEn // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAr: serviceAr == freezed
          ? _value.serviceAr
          : serviceAr // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentId: appointmentId == freezed
          ? _value.appointmentId
          : appointmentId // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      staffID: staffID == freezed
          ? _value.staffID
          : staffID // ignore: cast_nullable_to_non_nullable
              as String,
      formattedAppointmentDate: formattedAppointmentDate == freezed
          ? _value.formattedAppointmentDate
          : formattedAppointmentDate // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceGender: businessServiceGender == freezed
          ? _value.businessServiceGender
          : businessServiceGender // ignore: cast_nullable_to_non_nullable
              as String,
      detailID: detailID == freezed
          ? _value.detailID
          : detailID // ignore: cast_nullable_to_non_nullable
              as String,
      orderStatusEN: orderStatusEN == freezed
          ? _value.orderStatusEN
          : orderStatusEN // ignore: cast_nullable_to_non_nullable
              as String,
      orderStatusAR: orderStatusAR == freezed
          ? _value.orderStatusAR
          : orderStatusAR // ignore: cast_nullable_to_non_nullable
              as String?,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceImage: businessServiceImage == freezed
          ? _value.businessServiceImage
          : businessServiceImage // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      cancelled: cancelled == freezed
          ? _value.cancelled
          : cancelled // ignore: cast_nullable_to_non_nullable
              as bool,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      staffEnglishName: staffEnglishName == freezed
          ? _value.staffEnglishName
          : staffEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      staffAvatar: staffAvatar == freezed
          ? _value.staffAvatar
          : staffAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      staffArabicName: staffArabicName == freezed
          ? _value.staffArabicName
          : staffArabicName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_AppontmentServicesCopyWith<$Res>
    implements $AppontmentServicesCopyWith<$Res> {
  factory _$$_AppontmentServicesCopyWith(_$_AppontmentServices value,
          $Res Function(_$_AppontmentServices) then) =
      __$$_AppontmentServicesCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(nullable: true, defaultValue: "") String serviceId,
      @JsonKey(name: "businessServiceEnglishName") String serviceNameEN,
      @JsonKey(name: "businessServiceArabicName") String serviceNameAR,
      @JsonKey(nullable: true, defaultValue: 0) num amount,
      @JsonKey(name: "orderStatusId") int statusId,
      String businessId,
      @JsonKey(defaultValue: "", nullable: true) String completionDate,
      String remarks,
      String appointmentDate,
      String fromTime,
      String toTime,
      @JsonKey(name: "ServiceDurationText", defaultValue: "") String slotTime,
      int? orderStatus,
      @JsonKey(name: "serviceEnglishName") String serviceEn,
      @JsonKey(name: "serviceArabicName") String serviceAr,
      String appointmentId,
      String serviceDurationText,
      int serviceDuration,
      String staffID,
      String formattedAppointmentDate,
      @JsonKey(nullable: true, defaultValue: "") String businessServiceGender,
      String detailID,
      String orderStatusEN,
      String? orderStatusAR,
      String businessServiceID,
      @JsonKey(nullable: true, defaultValue: "") String businessServiceImage,
      bool hasWishItem,
      bool cancelled,
      bool isActive,
      @JsonKey(defaultValue: "", nullable: true) String staffEnglishName,
      @JsonKey(defaultValue: "", nullable: true) String staffAvatar,
      @JsonKey(defaultValue: "", nullable: true) String staffArabicName});
}

/// @nodoc
class __$$_AppontmentServicesCopyWithImpl<$Res>
    extends _$AppontmentServicesCopyWithImpl<$Res>
    implements _$$_AppontmentServicesCopyWith<$Res> {
  __$$_AppontmentServicesCopyWithImpl(
      _$_AppontmentServices _value, $Res Function(_$_AppontmentServices) _then)
      : super(_value, (v) => _then(v as _$_AppontmentServices));

  @override
  _$_AppontmentServices get _value => super._value as _$_AppontmentServices;

  @override
  $Res call({
    Object? serviceId = freezed,
    Object? serviceNameEN = freezed,
    Object? serviceNameAR = freezed,
    Object? amount = freezed,
    Object? statusId = freezed,
    Object? businessId = freezed,
    Object? completionDate = freezed,
    Object? remarks = freezed,
    Object? appointmentDate = freezed,
    Object? fromTime = freezed,
    Object? toTime = freezed,
    Object? slotTime = freezed,
    Object? orderStatus = freezed,
    Object? serviceEn = freezed,
    Object? serviceAr = freezed,
    Object? appointmentId = freezed,
    Object? serviceDurationText = freezed,
    Object? serviceDuration = freezed,
    Object? staffID = freezed,
    Object? formattedAppointmentDate = freezed,
    Object? businessServiceGender = freezed,
    Object? detailID = freezed,
    Object? orderStatusEN = freezed,
    Object? orderStatusAR = freezed,
    Object? businessServiceID = freezed,
    Object? businessServiceImage = freezed,
    Object? hasWishItem = freezed,
    Object? cancelled = freezed,
    Object? isActive = freezed,
    Object? staffEnglishName = freezed,
    Object? staffAvatar = freezed,
    Object? staffArabicName = freezed,
  }) {
    return _then(_$_AppontmentServices(
      serviceId: serviceId == freezed
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
      serviceNameEN: serviceNameEN == freezed
          ? _value.serviceNameEN
          : serviceNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      serviceNameAR: serviceNameAR == freezed
          ? _value.serviceNameAR
          : serviceNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      amount: amount == freezed
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as num,
      statusId: statusId == freezed
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      businessId: businessId == freezed
          ? _value.businessId
          : businessId // ignore: cast_nullable_to_non_nullable
              as String,
      completionDate: completionDate == freezed
          ? _value.completionDate
          : completionDate // ignore: cast_nullable_to_non_nullable
              as String,
      remarks: remarks == freezed
          ? _value.remarks
          : remarks // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentDate: appointmentDate == freezed
          ? _value.appointmentDate
          : appointmentDate // ignore: cast_nullable_to_non_nullable
              as String,
      fromTime: fromTime == freezed
          ? _value.fromTime
          : fromTime // ignore: cast_nullable_to_non_nullable
              as String,
      toTime: toTime == freezed
          ? _value.toTime
          : toTime // ignore: cast_nullable_to_non_nullable
              as String,
      slotTime: slotTime == freezed
          ? _value.slotTime
          : slotTime // ignore: cast_nullable_to_non_nullable
              as String,
      orderStatus: orderStatus == freezed
          ? _value.orderStatus
          : orderStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      serviceEn: serviceEn == freezed
          ? _value.serviceEn
          : serviceEn // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAr: serviceAr == freezed
          ? _value.serviceAr
          : serviceAr // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentId: appointmentId == freezed
          ? _value.appointmentId
          : appointmentId // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      staffID: staffID == freezed
          ? _value.staffID
          : staffID // ignore: cast_nullable_to_non_nullable
              as String,
      formattedAppointmentDate: formattedAppointmentDate == freezed
          ? _value.formattedAppointmentDate
          : formattedAppointmentDate // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceGender: businessServiceGender == freezed
          ? _value.businessServiceGender
          : businessServiceGender // ignore: cast_nullable_to_non_nullable
              as String,
      detailID: detailID == freezed
          ? _value.detailID
          : detailID // ignore: cast_nullable_to_non_nullable
              as String,
      orderStatusEN: orderStatusEN == freezed
          ? _value.orderStatusEN
          : orderStatusEN // ignore: cast_nullable_to_non_nullable
              as String,
      orderStatusAR: orderStatusAR == freezed
          ? _value.orderStatusAR
          : orderStatusAR // ignore: cast_nullable_to_non_nullable
              as String?,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceImage: businessServiceImage == freezed
          ? _value.businessServiceImage
          : businessServiceImage // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      cancelled: cancelled == freezed
          ? _value.cancelled
          : cancelled // ignore: cast_nullable_to_non_nullable
              as bool,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      staffEnglishName: staffEnglishName == freezed
          ? _value.staffEnglishName
          : staffEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      staffAvatar: staffAvatar == freezed
          ? _value.staffAvatar
          : staffAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      staffArabicName: staffArabicName == freezed
          ? _value.staffArabicName
          : staffArabicName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_AppontmentServices extends _AppontmentServices {
  _$_AppontmentServices(
      {@JsonKey(nullable: true, defaultValue: "")
          required this.serviceId,
      @JsonKey(name: "businessServiceEnglishName")
          required this.serviceNameEN,
      @JsonKey(name: "businessServiceArabicName")
          required this.serviceNameAR,
      @JsonKey(nullable: true, defaultValue: 0)
          required this.amount,
      @JsonKey(name: "orderStatusId")
          required this.statusId,
      required this.businessId,
      @JsonKey(defaultValue: "", nullable: true)
          required this.completionDate,
      required this.remarks,
      required this.appointmentDate,
      required this.fromTime,
      required this.toTime,
      @JsonKey(name: "ServiceDurationText", defaultValue: "")
          required this.slotTime,
      required this.orderStatus,
      @JsonKey(name: "serviceEnglishName")
          required this.serviceEn,
      @JsonKey(name: "serviceArabicName")
          required this.serviceAr,
      required this.appointmentId,
      required this.serviceDurationText,
      required this.serviceDuration,
      required this.staffID,
      required this.formattedAppointmentDate,
      @JsonKey(nullable: true, defaultValue: "")
          required this.businessServiceGender,
      required this.detailID,
      required this.orderStatusEN,
      required this.orderStatusAR,
      required this.businessServiceID,
      @JsonKey(nullable: true, defaultValue: "")
          required this.businessServiceImage,
      required this.hasWishItem,
      required this.cancelled,
      required this.isActive,
      @JsonKey(defaultValue: "", nullable: true)
          required this.staffEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.staffAvatar,
      @JsonKey(defaultValue: "", nullable: true)
          required this.staffArabicName})
      : super._();

  factory _$_AppontmentServices.fromJson(Map<String, dynamic> json) =>
      _$$_AppontmentServicesFromJson(json);

  @override
  @JsonKey(nullable: true, defaultValue: "")
  String serviceId;
  @override
  @JsonKey(name: "businessServiceEnglishName")
  String serviceNameEN;
  @override
  @JsonKey(name: "businessServiceArabicName")
  String serviceNameAR;
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  num amount;
  @override
  @JsonKey(name: "orderStatusId")
  int statusId;
  @override
  String businessId;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String completionDate;
  @override
  String remarks;
  @override
  String appointmentDate;
  @override
  String fromTime;
  @override
  String toTime;
  @override
  @JsonKey(name: "ServiceDurationText", defaultValue: "")
  String slotTime;
  @override
  int? orderStatus;
  @override
  @JsonKey(name: "serviceEnglishName")
  String serviceEn;
  @override
  @JsonKey(name: "serviceArabicName")
  String serviceAr;
  @override
  String appointmentId;
  @override
  String serviceDurationText;
  @override
  int serviceDuration;
  @override
  String staffID;
  @override
  String formattedAppointmentDate;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String businessServiceGender;
  @override
  String detailID;
  @override
  String orderStatusEN;
  @override
  String? orderStatusAR;
  @override
  String businessServiceID;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String businessServiceImage;
  @override
  bool hasWishItem;
  @override
  bool cancelled;
  @override
  bool isActive;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String staffEnglishName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String staffAvatar;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String staffArabicName;

  @override
  String toString() {
    return 'AppontmentServices(serviceId: $serviceId, serviceNameEN: $serviceNameEN, serviceNameAR: $serviceNameAR, amount: $amount, statusId: $statusId, businessId: $businessId, completionDate: $completionDate, remarks: $remarks, appointmentDate: $appointmentDate, fromTime: $fromTime, toTime: $toTime, slotTime: $slotTime, orderStatus: $orderStatus, serviceEn: $serviceEn, serviceAr: $serviceAr, appointmentId: $appointmentId, serviceDurationText: $serviceDurationText, serviceDuration: $serviceDuration, staffID: $staffID, formattedAppointmentDate: $formattedAppointmentDate, businessServiceGender: $businessServiceGender, detailID: $detailID, orderStatusEN: $orderStatusEN, orderStatusAR: $orderStatusAR, businessServiceID: $businessServiceID, businessServiceImage: $businessServiceImage, hasWishItem: $hasWishItem, cancelled: $cancelled, isActive: $isActive, staffEnglishName: $staffEnglishName, staffAvatar: $staffAvatar, staffArabicName: $staffArabicName)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_AppontmentServicesCopyWith<_$_AppontmentServices> get copyWith =>
      __$$_AppontmentServicesCopyWithImpl<_$_AppontmentServices>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppontmentServicesToJson(
      this,
    );
  }
}

abstract class _AppontmentServices extends AppontmentServices {
  factory _AppontmentServices(
      {@JsonKey(nullable: true, defaultValue: "")
          required String serviceId,
      @JsonKey(name: "businessServiceEnglishName")
          required String serviceNameEN,
      @JsonKey(name: "businessServiceArabicName")
          required String serviceNameAR,
      @JsonKey(nullable: true, defaultValue: 0)
          required num amount,
      @JsonKey(name: "orderStatusId")
          required int statusId,
      required String businessId,
      @JsonKey(defaultValue: "", nullable: true)
          required String completionDate,
      required String remarks,
      required String appointmentDate,
      required String fromTime,
      required String toTime,
      @JsonKey(name: "ServiceDurationText", defaultValue: "")
          required String slotTime,
      required int? orderStatus,
      @JsonKey(name: "serviceEnglishName")
          required String serviceEn,
      @JsonKey(name: "serviceArabicName")
          required String serviceAr,
      required String appointmentId,
      required String serviceDurationText,
      required int serviceDuration,
      required String staffID,
      required String formattedAppointmentDate,
      @JsonKey(nullable: true, defaultValue: "")
          required String businessServiceGender,
      required String detailID,
      required String orderStatusEN,
      required String? orderStatusAR,
      required String businessServiceID,
      @JsonKey(nullable: true, defaultValue: "")
          required String businessServiceImage,
      required bool hasWishItem,
      required bool cancelled,
      required bool isActive,
      @JsonKey(defaultValue: "", nullable: true)
          required String staffEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required String staffAvatar,
      @JsonKey(defaultValue: "", nullable: true)
          required String staffArabicName}) = _$_AppontmentServices;
  _AppontmentServices._() : super._();

  factory _AppontmentServices.fromJson(Map<String, dynamic> json) =
      _$_AppontmentServices.fromJson;

  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get serviceId;
  @JsonKey(nullable: true, defaultValue: "")
  set serviceId(String value);
  @override
  @JsonKey(name: "businessServiceEnglishName")
  String get serviceNameEN;
  @JsonKey(name: "businessServiceEnglishName")
  set serviceNameEN(String value);
  @override
  @JsonKey(name: "businessServiceArabicName")
  String get serviceNameAR;
  @JsonKey(name: "businessServiceArabicName")
  set serviceNameAR(String value);
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  num get amount;
  @JsonKey(nullable: true, defaultValue: 0)
  set amount(num value);
  @override
  @JsonKey(name: "orderStatusId")
  int get statusId;
  @JsonKey(name: "orderStatusId")
  set statusId(int value);
  @override
  String get businessId;
  set businessId(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get completionDate;
  @JsonKey(defaultValue: "", nullable: true)
  set completionDate(String value);
  @override
  String get remarks;
  set remarks(String value);
  @override
  String get appointmentDate;
  set appointmentDate(String value);
  @override
  String get fromTime;
  set fromTime(String value);
  @override
  String get toTime;
  set toTime(String value);
  @override
  @JsonKey(name: "ServiceDurationText", defaultValue: "")
  String get slotTime;
  @JsonKey(name: "ServiceDurationText", defaultValue: "")
  set slotTime(String value);
  @override
  int? get orderStatus;
  set orderStatus(int? value);
  @override
  @JsonKey(name: "serviceEnglishName")
  String get serviceEn;
  @JsonKey(name: "serviceEnglishName")
  set serviceEn(String value);
  @override
  @JsonKey(name: "serviceArabicName")
  String get serviceAr;
  @JsonKey(name: "serviceArabicName")
  set serviceAr(String value);
  @override
  String get appointmentId;
  set appointmentId(String value);
  @override
  String get serviceDurationText;
  set serviceDurationText(String value);
  @override
  int get serviceDuration;
  set serviceDuration(int value);
  @override
  String get staffID;
  set staffID(String value);
  @override
  String get formattedAppointmentDate;
  set formattedAppointmentDate(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get businessServiceGender;
  @JsonKey(nullable: true, defaultValue: "")
  set businessServiceGender(String value);
  @override
  String get detailID;
  set detailID(String value);
  @override
  String get orderStatusEN;
  set orderStatusEN(String value);
  @override
  String? get orderStatusAR;
  set orderStatusAR(String? value);
  @override
  String get businessServiceID;
  set businessServiceID(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get businessServiceImage;
  @JsonKey(nullable: true, defaultValue: "")
  set businessServiceImage(String value);
  @override
  bool get hasWishItem;
  set hasWishItem(bool value);
  @override
  bool get cancelled;
  set cancelled(bool value);
  @override
  bool get isActive;
  set isActive(bool value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get staffEnglishName;
  @JsonKey(defaultValue: "", nullable: true)
  set staffEnglishName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get staffAvatar;
  @JsonKey(defaultValue: "", nullable: true)
  set staffAvatar(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get staffArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set staffArabicName(String value);
  @override
  @JsonKey(ignore: true)
  _$$_AppontmentServicesCopyWith<_$_AppontmentServices> get copyWith =>
      throw _privateConstructorUsedError;
}
