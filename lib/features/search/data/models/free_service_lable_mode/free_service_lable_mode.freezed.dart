// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'free_service_lable_mode.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FreeServiceLableMode _$FreeServiceLableModeFromJson(Map<String, dynamic> json) {
  return _FreeServiceLableMode.fromJson(json);
}

/// @nodoc
mixin _$FreeServiceLableMode {
  num get value => throw _privateConstructorUsedError;
  String get label => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FreeServiceLableModeCopyWith<FreeServiceLableMode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FreeServiceLableModeCopyWith<$Res> {
  factory $FreeServiceLableModeCopyWith(FreeServiceLableMode value,
          $Res Function(FreeServiceLableMode) then) =
      _$FreeServiceLableModeCopyWithImpl<$Res>;
  $Res call({num value, String label});
}

/// @nodoc
class _$FreeServiceLableModeCopyWithImpl<$Res>
    implements $FreeServiceLableModeCopyWith<$Res> {
  _$FreeServiceLableModeCopyWithImpl(this._value, this._then);

  final FreeServiceLableMode _value;
  // ignore: unused_field
  final $Res Function(FreeServiceLableMode) _then;

  @override
  $Res call({
    Object? value = freezed,
    Object? label = freezed,
  }) {
    return _then(_value.copyWith(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_FreeServiceLableModeCopyWith<$Res>
    implements $FreeServiceLableModeCopyWith<$Res> {
  factory _$$_FreeServiceLableModeCopyWith(_$_FreeServiceLableMode value,
          $Res Function(_$_FreeServiceLableMode) then) =
      __$$_FreeServiceLableModeCopyWithImpl<$Res>;
  @override
  $Res call({num value, String label});
}

/// @nodoc
class __$$_FreeServiceLableModeCopyWithImpl<$Res>
    extends _$FreeServiceLableModeCopyWithImpl<$Res>
    implements _$$_FreeServiceLableModeCopyWith<$Res> {
  __$$_FreeServiceLableModeCopyWithImpl(_$_FreeServiceLableMode _value,
      $Res Function(_$_FreeServiceLableMode) _then)
      : super(_value, (v) => _then(v as _$_FreeServiceLableMode));

  @override
  _$_FreeServiceLableMode get _value => super._value as _$_FreeServiceLableMode;

  @override
  $Res call({
    Object? value = freezed,
    Object? label = freezed,
  }) {
    return _then(_$_FreeServiceLableMode(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_FreeServiceLableMode implements _FreeServiceLableMode {
  _$_FreeServiceLableMode({required this.value, required this.label});

  factory _$_FreeServiceLableMode.fromJson(Map<String, dynamic> json) =>
      _$$_FreeServiceLableModeFromJson(json);

  @override
  final num value;
  @override
  final String label;

  @override
  String toString() {
    return 'FreeServiceLableMode(value: $value, label: $label)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FreeServiceLableMode &&
            const DeepCollectionEquality().equals(other.value, value) &&
            const DeepCollectionEquality().equals(other.label, label));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(value),
      const DeepCollectionEquality().hash(label));

  @JsonKey(ignore: true)
  @override
  _$$_FreeServiceLableModeCopyWith<_$_FreeServiceLableMode> get copyWith =>
      __$$_FreeServiceLableModeCopyWithImpl<_$_FreeServiceLableMode>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FreeServiceLableModeToJson(
      this,
    );
  }
}

abstract class _FreeServiceLableMode implements FreeServiceLableMode {
  factory _FreeServiceLableMode(
      {required final num value,
      required final String label}) = _$_FreeServiceLableMode;

  factory _FreeServiceLableMode.fromJson(Map<String, dynamic> json) =
      _$_FreeServiceLableMode.fromJson;

  @override
  num get value;
  @override
  String get label;
  @override
  @JsonKey(ignore: true)
  _$$_FreeServiceLableModeCopyWith<_$_FreeServiceLableMode> get copyWith =>
      throw _privateConstructorUsedError;
}
