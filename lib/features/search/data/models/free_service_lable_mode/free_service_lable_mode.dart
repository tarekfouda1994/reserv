import 'package:freezed_annotation/freezed_annotation.dart';

part 'free_service_lable_mode.freezed.dart';
part 'free_service_lable_mode.g.dart';

@freezed
class FreeServiceLableMode with _$FreeServiceLableMode {
  @JsonSerializable(explicitToJson: true)
  factory FreeServiceLableMode({
    required num value,
    required String label,
  }) = _FreeServiceLableMode;

  factory FreeServiceLableMode.fromJson(Map<String, dynamic> json) =>
      _$FreeServiceLableModeFromJson(json);
}
