// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'free_service_lable_mode.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FreeServiceLableMode _$$_FreeServiceLableModeFromJson(
        Map<String, dynamic> json) =>
    _$_FreeServiceLableMode(
      value: json['value'] as num,
      label: json['label'] as String,
    );

Map<String, dynamic> _$$_FreeServiceLableModeToJson(
        _$_FreeServiceLableMode instance) =>
    <String, dynamic>{
      'value': instance.value,
      'label': instance.label,
    };
