// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'trinding_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TrindingModel _$TrindingModelFromJson(Map<String, dynamic> json) {
  return _TrindingModel.fromJson(json);
}

/// @nodoc
mixin _$TrindingModel {
  int get totalRecords => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  List<TrendingServicesItemModel> get itemsList =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TrindingModelCopyWith<TrindingModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrindingModelCopyWith<$Res> {
  factory $TrindingModelCopyWith(
          TrindingModel value, $Res Function(TrindingModel) then) =
      _$TrindingModelCopyWithImpl<$Res>;
  $Res call(
      {int totalRecords,
      int pageSize,
      List<TrendingServicesItemModel> itemsList});
}

/// @nodoc
class _$TrindingModelCopyWithImpl<$Res>
    implements $TrindingModelCopyWith<$Res> {
  _$TrindingModelCopyWithImpl(this._value, this._then);

  final TrindingModel _value;
  // ignore: unused_field
  final $Res Function(TrindingModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<TrendingServicesItemModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_TrindingModelCopyWith<$Res>
    implements $TrindingModelCopyWith<$Res> {
  factory _$$_TrindingModelCopyWith(
          _$_TrindingModel value, $Res Function(_$_TrindingModel) then) =
      __$$_TrindingModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {int totalRecords,
      int pageSize,
      List<TrendingServicesItemModel> itemsList});
}

/// @nodoc
class __$$_TrindingModelCopyWithImpl<$Res>
    extends _$TrindingModelCopyWithImpl<$Res>
    implements _$$_TrindingModelCopyWith<$Res> {
  __$$_TrindingModelCopyWithImpl(
      _$_TrindingModel _value, $Res Function(_$_TrindingModel) _then)
      : super(_value, (v) => _then(v as _$_TrindingModel));

  @override
  _$_TrindingModel get _value => super._value as _$_TrindingModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_TrindingModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<TrendingServicesItemModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_TrindingModel implements _TrindingModel {
  _$_TrindingModel(
      {required this.totalRecords,
      required this.pageSize,
      required final List<TrendingServicesItemModel> itemsList})
      : _itemsList = itemsList;

  factory _$_TrindingModel.fromJson(Map<String, dynamic> json) =>
      _$$_TrindingModelFromJson(json);

  @override
  final int totalRecords;
  @override
  final int pageSize;
  final List<TrendingServicesItemModel> _itemsList;
  @override
  List<TrendingServicesItemModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'TrindingModel(totalRecords: $totalRecords, pageSize: $pageSize, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TrindingModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality().equals(other.pageSize, pageSize) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(pageSize),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_TrindingModelCopyWith<_$_TrindingModel> get copyWith =>
      __$$_TrindingModelCopyWithImpl<_$_TrindingModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TrindingModelToJson(
      this,
    );
  }
}

abstract class _TrindingModel implements TrindingModel {
  factory _TrindingModel(
          {required final int totalRecords,
          required final int pageSize,
          required final List<TrendingServicesItemModel> itemsList}) =
      _$_TrindingModel;

  factory _TrindingModel.fromJson(Map<String, dynamic> json) =
      _$_TrindingModel.fromJson;

  @override
  int get totalRecords;
  @override
  int get pageSize;
  @override
  List<TrendingServicesItemModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_TrindingModelCopyWith<_$_TrindingModel> get copyWith =>
      throw _privateConstructorUsedError;
}
