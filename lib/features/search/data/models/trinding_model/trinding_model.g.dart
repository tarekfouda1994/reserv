// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trinding_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TrindingModel _$$_TrindingModelFromJson(Map<String, dynamic> json) =>
    _$_TrindingModel(
      totalRecords: json['totalRecords'] as int,
      pageSize: json['pageSize'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map((e) =>
              TrendingServicesItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TrindingModelToJson(_$_TrindingModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'pageSize': instance.pageSize,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
