import 'package:flutter_tdd/features/search/data/models/trending_services_item_model/trending_services_item_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'trinding_model.freezed.dart';
part 'trinding_model.g.dart';

@freezed
class TrindingModel with _$TrindingModel {
  @JsonSerializable(explicitToJson: true)
  factory TrindingModel({
    required int totalRecords,
    required int pageSize,
    required List<TrendingServicesItemModel> itemsList,
  }) = _TrindingModel;

  factory TrindingModel.fromJson(Map<String, dynamic> json) => _$TrindingModelFromJson(json);

  factory TrindingModel.fromMap(Map<String, dynamic> json) => TrindingModel(
        pageSize: json["pageSize"],
        itemsList: List<TrendingServicesItemModel>.from(
            json["itemsList"].map((e) => TrendingServicesItemModel.fromMap(e))),
        totalRecords: json["totalRecords"],
      );
}
