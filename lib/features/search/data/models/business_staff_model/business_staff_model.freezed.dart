// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'business_staff_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BusinessStaffModel _$BusinessStaffModelFromJson(Map<String, dynamic> json) {
  return _BusinessStaffModel.fromJson(json);
}

/// @nodoc
mixin _$BusinessStaffModel {
  String get id => throw _privateConstructorUsedError;
  String get firstNameEnglish => throw _privateConstructorUsedError;
  String get lastNameEnglish => throw _privateConstructorUsedError;
  String get firstNameArabic => throw _privateConstructorUsedError;
  String get lastNameArabic => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  int get jobTitleID => throw _privateConstructorUsedError;
  int get genderID => throw _privateConstructorUsedError;
  String get jobTitleEnglish => throw _privateConstructorUsedError;
  String get jobTitleArabic => throw _privateConstructorUsedError;
  String get contactPersonMobile => throw _privateConstructorUsedError;
  String get contactPersonEmail => throw _privateConstructorUsedError;
  int get employmentContractID => throw _privateConstructorUsedError;
  String get employmentDate => throw _privateConstructorUsedError;
  String get expiryDate => throw _privateConstructorUsedError;
  int get statusId => throw _privateConstructorUsedError;
  String get businessServiceIDs => throw _privateConstructorUsedError;
  String get businessServiceEnglishName => throw _privateConstructorUsedError;
  String get businessServiceArabicName => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BusinessStaffModelCopyWith<BusinessStaffModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BusinessStaffModelCopyWith<$Res> {
  factory $BusinessStaffModelCopyWith(
          BusinessStaffModel value, $Res Function(BusinessStaffModel) then) =
      _$BusinessStaffModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String firstNameEnglish,
      String lastNameEnglish,
      String firstNameArabic,
      String lastNameArabic,
      String avatar,
      String businessID,
      int jobTitleID,
      int genderID,
      String jobTitleEnglish,
      String jobTitleArabic,
      String contactPersonMobile,
      String contactPersonEmail,
      int employmentContractID,
      String employmentDate,
      String expiryDate,
      int statusId,
      String businessServiceIDs,
      String businessServiceEnglishName,
      String businessServiceArabicName,
      bool isActive,
      double rating});
}

/// @nodoc
class _$BusinessStaffModelCopyWithImpl<$Res>
    implements $BusinessStaffModelCopyWith<$Res> {
  _$BusinessStaffModelCopyWithImpl(this._value, this._then);

  final BusinessStaffModel _value;
  // ignore: unused_field
  final $Res Function(BusinessStaffModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstNameEnglish = freezed,
    Object? lastNameEnglish = freezed,
    Object? firstNameArabic = freezed,
    Object? lastNameArabic = freezed,
    Object? avatar = freezed,
    Object? businessID = freezed,
    Object? jobTitleID = freezed,
    Object? genderID = freezed,
    Object? jobTitleEnglish = freezed,
    Object? jobTitleArabic = freezed,
    Object? contactPersonMobile = freezed,
    Object? contactPersonEmail = freezed,
    Object? employmentContractID = freezed,
    Object? employmentDate = freezed,
    Object? expiryDate = freezed,
    Object? statusId = freezed,
    Object? businessServiceIDs = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? businessServiceArabicName = freezed,
    Object? isActive = freezed,
    Object? rating = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      firstNameEnglish: firstNameEnglish == freezed
          ? _value.firstNameEnglish
          : firstNameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      lastNameEnglish: lastNameEnglish == freezed
          ? _value.lastNameEnglish
          : lastNameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      firstNameArabic: firstNameArabic == freezed
          ? _value.firstNameArabic
          : firstNameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      lastNameArabic: lastNameArabic == freezed
          ? _value.lastNameArabic
          : lastNameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      jobTitleID: jobTitleID == freezed
          ? _value.jobTitleID
          : jobTitleID // ignore: cast_nullable_to_non_nullable
              as int,
      genderID: genderID == freezed
          ? _value.genderID
          : genderID // ignore: cast_nullable_to_non_nullable
              as int,
      jobTitleEnglish: jobTitleEnglish == freezed
          ? _value.jobTitleEnglish
          : jobTitleEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      jobTitleArabic: jobTitleArabic == freezed
          ? _value.jobTitleArabic
          : jobTitleArabic // ignore: cast_nullable_to_non_nullable
              as String,
      contactPersonMobile: contactPersonMobile == freezed
          ? _value.contactPersonMobile
          : contactPersonMobile // ignore: cast_nullable_to_non_nullable
              as String,
      contactPersonEmail: contactPersonEmail == freezed
          ? _value.contactPersonEmail
          : contactPersonEmail // ignore: cast_nullable_to_non_nullable
              as String,
      employmentContractID: employmentContractID == freezed
          ? _value.employmentContractID
          : employmentContractID // ignore: cast_nullable_to_non_nullable
              as int,
      employmentDate: employmentDate == freezed
          ? _value.employmentDate
          : employmentDate // ignore: cast_nullable_to_non_nullable
              as String,
      expiryDate: expiryDate == freezed
          ? _value.expiryDate
          : expiryDate // ignore: cast_nullable_to_non_nullable
              as String,
      statusId: statusId == freezed
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      businessServiceIDs: businessServiceIDs == freezed
          ? _value.businessServiceIDs
          : businessServiceIDs // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$$_BusinessStaffModelCopyWith<$Res>
    implements $BusinessStaffModelCopyWith<$Res> {
  factory _$$_BusinessStaffModelCopyWith(_$_BusinessStaffModel value,
          $Res Function(_$_BusinessStaffModel) then) =
      __$$_BusinessStaffModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String firstNameEnglish,
      String lastNameEnglish,
      String firstNameArabic,
      String lastNameArabic,
      String avatar,
      String businessID,
      int jobTitleID,
      int genderID,
      String jobTitleEnglish,
      String jobTitleArabic,
      String contactPersonMobile,
      String contactPersonEmail,
      int employmentContractID,
      String employmentDate,
      String expiryDate,
      int statusId,
      String businessServiceIDs,
      String businessServiceEnglishName,
      String businessServiceArabicName,
      bool isActive,
      double rating});
}

/// @nodoc
class __$$_BusinessStaffModelCopyWithImpl<$Res>
    extends _$BusinessStaffModelCopyWithImpl<$Res>
    implements _$$_BusinessStaffModelCopyWith<$Res> {
  __$$_BusinessStaffModelCopyWithImpl(
      _$_BusinessStaffModel _value, $Res Function(_$_BusinessStaffModel) _then)
      : super(_value, (v) => _then(v as _$_BusinessStaffModel));

  @override
  _$_BusinessStaffModel get _value => super._value as _$_BusinessStaffModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstNameEnglish = freezed,
    Object? lastNameEnglish = freezed,
    Object? firstNameArabic = freezed,
    Object? lastNameArabic = freezed,
    Object? avatar = freezed,
    Object? businessID = freezed,
    Object? jobTitleID = freezed,
    Object? genderID = freezed,
    Object? jobTitleEnglish = freezed,
    Object? jobTitleArabic = freezed,
    Object? contactPersonMobile = freezed,
    Object? contactPersonEmail = freezed,
    Object? employmentContractID = freezed,
    Object? employmentDate = freezed,
    Object? expiryDate = freezed,
    Object? statusId = freezed,
    Object? businessServiceIDs = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? businessServiceArabicName = freezed,
    Object? isActive = freezed,
    Object? rating = freezed,
  }) {
    return _then(_$_BusinessStaffModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      firstNameEnglish: firstNameEnglish == freezed
          ? _value.firstNameEnglish
          : firstNameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      lastNameEnglish: lastNameEnglish == freezed
          ? _value.lastNameEnglish
          : lastNameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      firstNameArabic: firstNameArabic == freezed
          ? _value.firstNameArabic
          : firstNameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      lastNameArabic: lastNameArabic == freezed
          ? _value.lastNameArabic
          : lastNameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      jobTitleID: jobTitleID == freezed
          ? _value.jobTitleID
          : jobTitleID // ignore: cast_nullable_to_non_nullable
              as int,
      genderID: genderID == freezed
          ? _value.genderID
          : genderID // ignore: cast_nullable_to_non_nullable
              as int,
      jobTitleEnglish: jobTitleEnglish == freezed
          ? _value.jobTitleEnglish
          : jobTitleEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      jobTitleArabic: jobTitleArabic == freezed
          ? _value.jobTitleArabic
          : jobTitleArabic // ignore: cast_nullable_to_non_nullable
              as String,
      contactPersonMobile: contactPersonMobile == freezed
          ? _value.contactPersonMobile
          : contactPersonMobile // ignore: cast_nullable_to_non_nullable
              as String,
      contactPersonEmail: contactPersonEmail == freezed
          ? _value.contactPersonEmail
          : contactPersonEmail // ignore: cast_nullable_to_non_nullable
              as String,
      employmentContractID: employmentContractID == freezed
          ? _value.employmentContractID
          : employmentContractID // ignore: cast_nullable_to_non_nullable
              as int,
      employmentDate: employmentDate == freezed
          ? _value.employmentDate
          : employmentDate // ignore: cast_nullable_to_non_nullable
              as String,
      expiryDate: expiryDate == freezed
          ? _value.expiryDate
          : expiryDate // ignore: cast_nullable_to_non_nullable
              as String,
      statusId: statusId == freezed
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      businessServiceIDs: businessServiceIDs == freezed
          ? _value.businessServiceIDs
          : businessServiceIDs // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BusinessStaffModel extends _BusinessStaffModel {
  _$_BusinessStaffModel(
      {required this.id,
      required this.firstNameEnglish,
      required this.lastNameEnglish,
      required this.firstNameArabic,
      required this.lastNameArabic,
      required this.avatar,
      required this.businessID,
      required this.jobTitleID,
      required this.genderID,
      required this.jobTitleEnglish,
      required this.jobTitleArabic,
      required this.contactPersonMobile,
      required this.contactPersonEmail,
      required this.employmentContractID,
      required this.employmentDate,
      required this.expiryDate,
      required this.statusId,
      required this.businessServiceIDs,
      required this.businessServiceEnglishName,
      required this.businessServiceArabicName,
      required this.isActive,
      required this.rating})
      : super._();

  factory _$_BusinessStaffModel.fromJson(Map<String, dynamic> json) =>
      _$$_BusinessStaffModelFromJson(json);

  @override
  final String id;
  @override
  final String firstNameEnglish;
  @override
  final String lastNameEnglish;
  @override
  final String firstNameArabic;
  @override
  final String lastNameArabic;
  @override
  final String avatar;
  @override
  final String businessID;
  @override
  final int jobTitleID;
  @override
  final int genderID;
  @override
  final String jobTitleEnglish;
  @override
  final String jobTitleArabic;
  @override
  final String contactPersonMobile;
  @override
  final String contactPersonEmail;
  @override
  final int employmentContractID;
  @override
  final String employmentDate;
  @override
  final String expiryDate;
  @override
  final int statusId;
  @override
  final String businessServiceIDs;
  @override
  final String businessServiceEnglishName;
  @override
  final String businessServiceArabicName;
  @override
  final bool isActive;
  @override
  final double rating;

  @override
  String toString() {
    return 'BusinessStaffModel(id: $id, firstNameEnglish: $firstNameEnglish, lastNameEnglish: $lastNameEnglish, firstNameArabic: $firstNameArabic, lastNameArabic: $lastNameArabic, avatar: $avatar, businessID: $businessID, jobTitleID: $jobTitleID, genderID: $genderID, jobTitleEnglish: $jobTitleEnglish, jobTitleArabic: $jobTitleArabic, contactPersonMobile: $contactPersonMobile, contactPersonEmail: $contactPersonEmail, employmentContractID: $employmentContractID, employmentDate: $employmentDate, expiryDate: $expiryDate, statusId: $statusId, businessServiceIDs: $businessServiceIDs, businessServiceEnglishName: $businessServiceEnglishName, businessServiceArabicName: $businessServiceArabicName, isActive: $isActive, rating: $rating)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BusinessStaffModel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.firstNameEnglish, firstNameEnglish) &&
            const DeepCollectionEquality()
                .equals(other.lastNameEnglish, lastNameEnglish) &&
            const DeepCollectionEquality()
                .equals(other.firstNameArabic, firstNameArabic) &&
            const DeepCollectionEquality()
                .equals(other.lastNameArabic, lastNameArabic) &&
            const DeepCollectionEquality().equals(other.avatar, avatar) &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.jobTitleID, jobTitleID) &&
            const DeepCollectionEquality().equals(other.genderID, genderID) &&
            const DeepCollectionEquality()
                .equals(other.jobTitleEnglish, jobTitleEnglish) &&
            const DeepCollectionEquality()
                .equals(other.jobTitleArabic, jobTitleArabic) &&
            const DeepCollectionEquality()
                .equals(other.contactPersonMobile, contactPersonMobile) &&
            const DeepCollectionEquality()
                .equals(other.contactPersonEmail, contactPersonEmail) &&
            const DeepCollectionEquality()
                .equals(other.employmentContractID, employmentContractID) &&
            const DeepCollectionEquality()
                .equals(other.employmentDate, employmentDate) &&
            const DeepCollectionEquality()
                .equals(other.expiryDate, expiryDate) &&
            const DeepCollectionEquality().equals(other.statusId, statusId) &&
            const DeepCollectionEquality()
                .equals(other.businessServiceIDs, businessServiceIDs) &&
            const DeepCollectionEquality().equals(
                other.businessServiceEnglishName, businessServiceEnglishName) &&
            const DeepCollectionEquality().equals(
                other.businessServiceArabicName, businessServiceArabicName) &&
            const DeepCollectionEquality().equals(other.isActive, isActive) &&
            const DeepCollectionEquality().equals(other.rating, rating));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(firstNameEnglish),
        const DeepCollectionEquality().hash(lastNameEnglish),
        const DeepCollectionEquality().hash(firstNameArabic),
        const DeepCollectionEquality().hash(lastNameArabic),
        const DeepCollectionEquality().hash(avatar),
        const DeepCollectionEquality().hash(businessID),
        const DeepCollectionEquality().hash(jobTitleID),
        const DeepCollectionEquality().hash(genderID),
        const DeepCollectionEquality().hash(jobTitleEnglish),
        const DeepCollectionEquality().hash(jobTitleArabic),
        const DeepCollectionEquality().hash(contactPersonMobile),
        const DeepCollectionEquality().hash(contactPersonEmail),
        const DeepCollectionEquality().hash(employmentContractID),
        const DeepCollectionEquality().hash(employmentDate),
        const DeepCollectionEquality().hash(expiryDate),
        const DeepCollectionEquality().hash(statusId),
        const DeepCollectionEquality().hash(businessServiceIDs),
        const DeepCollectionEquality().hash(businessServiceEnglishName),
        const DeepCollectionEquality().hash(businessServiceArabicName),
        const DeepCollectionEquality().hash(isActive),
        const DeepCollectionEquality().hash(rating)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_BusinessStaffModelCopyWith<_$_BusinessStaffModel> get copyWith =>
      __$$_BusinessStaffModelCopyWithImpl<_$_BusinessStaffModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BusinessStaffModelToJson(
      this,
    );
  }
}

abstract class _BusinessStaffModel extends BusinessStaffModel {
  factory _BusinessStaffModel(
      {required final String id,
      required final String firstNameEnglish,
      required final String lastNameEnglish,
      required final String firstNameArabic,
      required final String lastNameArabic,
      required final String avatar,
      required final String businessID,
      required final int jobTitleID,
      required final int genderID,
      required final String jobTitleEnglish,
      required final String jobTitleArabic,
      required final String contactPersonMobile,
      required final String contactPersonEmail,
      required final int employmentContractID,
      required final String employmentDate,
      required final String expiryDate,
      required final int statusId,
      required final String businessServiceIDs,
      required final String businessServiceEnglishName,
      required final String businessServiceArabicName,
      required final bool isActive,
      required final double rating}) = _$_BusinessStaffModel;
  _BusinessStaffModel._() : super._();

  factory _BusinessStaffModel.fromJson(Map<String, dynamic> json) =
      _$_BusinessStaffModel.fromJson;

  @override
  String get id;
  @override
  String get firstNameEnglish;
  @override
  String get lastNameEnglish;
  @override
  String get firstNameArabic;
  @override
  String get lastNameArabic;
  @override
  String get avatar;
  @override
  String get businessID;
  @override
  int get jobTitleID;
  @override
  int get genderID;
  @override
  String get jobTitleEnglish;
  @override
  String get jobTitleArabic;
  @override
  String get contactPersonMobile;
  @override
  String get contactPersonEmail;
  @override
  int get employmentContractID;
  @override
  String get employmentDate;
  @override
  String get expiryDate;
  @override
  int get statusId;
  @override
  String get businessServiceIDs;
  @override
  String get businessServiceEnglishName;
  @override
  String get businessServiceArabicName;
  @override
  bool get isActive;
  @override
  double get rating;
  @override
  @JsonKey(ignore: true)
  _$$_BusinessStaffModelCopyWith<_$_BusinessStaffModel> get copyWith =>
      throw _privateConstructorUsedError;
}
