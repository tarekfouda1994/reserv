// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_staff_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BusinessStaffModel _$$_BusinessStaffModelFromJson(
        Map<String, dynamic> json) =>
    _$_BusinessStaffModel(
      id: json['id'] as String,
      firstNameEnglish: json['firstNameEnglish'] as String,
      lastNameEnglish: json['lastNameEnglish'] as String,
      firstNameArabic: json['firstNameArabic'] as String,
      lastNameArabic: json['lastNameArabic'] as String,
      avatar: json['avatar'] as String,
      businessID: json['businessID'] as String,
      jobTitleID: json['jobTitleID'] as int,
      genderID: json['genderID'] as int,
      jobTitleEnglish: json['jobTitleEnglish'] as String,
      jobTitleArabic: json['jobTitleArabic'] as String,
      contactPersonMobile: json['contactPersonMobile'] as String,
      contactPersonEmail: json['contactPersonEmail'] as String,
      employmentContractID: json['employmentContractID'] as int,
      employmentDate: json['employmentDate'] as String,
      expiryDate: json['expiryDate'] as String,
      statusId: json['statusId'] as int,
      businessServiceIDs: json['businessServiceIDs'] as String,
      businessServiceEnglishName: json['businessServiceEnglishName'] as String,
      businessServiceArabicName: json['businessServiceArabicName'] as String,
      isActive: json['isActive'] as bool,
      rating: (json['rating'] as num).toDouble(),
    );

Map<String, dynamic> _$$_BusinessStaffModelToJson(
        _$_BusinessStaffModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'firstNameEnglish': instance.firstNameEnglish,
      'lastNameEnglish': instance.lastNameEnglish,
      'firstNameArabic': instance.firstNameArabic,
      'lastNameArabic': instance.lastNameArabic,
      'avatar': instance.avatar,
      'businessID': instance.businessID,
      'jobTitleID': instance.jobTitleID,
      'genderID': instance.genderID,
      'jobTitleEnglish': instance.jobTitleEnglish,
      'jobTitleArabic': instance.jobTitleArabic,
      'contactPersonMobile': instance.contactPersonMobile,
      'contactPersonEmail': instance.contactPersonEmail,
      'employmentContractID': instance.employmentContractID,
      'employmentDate': instance.employmentDate,
      'expiryDate': instance.expiryDate,
      'statusId': instance.statusId,
      'businessServiceIDs': instance.businessServiceIDs,
      'businessServiceEnglishName': instance.businessServiceEnglishName,
      'businessServiceArabicName': instance.businessServiceArabicName,
      'isActive': instance.isActive,
      'rating': instance.rating,
    };
