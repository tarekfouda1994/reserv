import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'business_staff_model.freezed.dart';
part 'business_staff_model.g.dart';

@freezed
class BusinessStaffModel with _$BusinessStaffModel {
  BusinessStaffModel._();

  @JsonSerializable(explicitToJson: true)
  factory BusinessStaffModel({
    required String id,
    required String firstNameEnglish,
    required String lastNameEnglish,
    required String firstNameArabic,
    required String lastNameArabic,
    required String avatar,
    required String businessID,
    required int jobTitleID,
    required int genderID,
    required String jobTitleEnglish,
    required String jobTitleArabic,
    required String contactPersonMobile,
    required String contactPersonEmail,
    required int employmentContractID,
    required String employmentDate,
    required String expiryDate,
    required int statusId,
    required String businessServiceIDs,
    required String businessServiceEnglishName,
    required String businessServiceArabicName,
    required bool isActive,
    required double rating,
  }) = _BusinessStaffModel;

  factory BusinessStaffModel.fromJson(Map<String, dynamic> json) =>
      _$BusinessStaffModelFromJson(json);

  String getBusinessServiceName() {
    return getIt<Utilities>()
        .getLocalizedValue(businessServiceArabicName, businessServiceEnglishName);
  }

  String getJopTitle() {
    return getIt<Utilities>().getLocalizedValue(jobTitleArabic, jobTitleEnglish);
  }

  String getFirstName() {
    return getIt<Utilities>().getLocalizedValue(firstNameArabic, firstNameEnglish);
  }

  String getLastName() {
    return getIt<Utilities>().getLocalizedValue(lastNameArabic, lastNameEnglish);
  }
}
