// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sevice_search_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceSearchItemModel _$ServiceSearchItemModelFromJson(
    Map<String, dynamic> json) {
  return _ServiceSearchItemModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceSearchItemModel {
  String get id => throw _privateConstructorUsedError;
  int get totalResult => throw _privateConstructorUsedError;
  String get serviceTypeID => throw _privateConstructorUsedError;
  String get serviceTypeEnglishName => throw _privateConstructorUsedError;
  String get serviceTypeArabicName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceSearchItemModelCopyWith<ServiceSearchItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceSearchItemModelCopyWith<$Res> {
  factory $ServiceSearchItemModelCopyWith(ServiceSearchItemModel value,
          $Res Function(ServiceSearchItemModel) then) =
      _$ServiceSearchItemModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      int totalResult,
      String serviceTypeID,
      String serviceTypeEnglishName,
      String serviceTypeArabicName});
}

/// @nodoc
class _$ServiceSearchItemModelCopyWithImpl<$Res>
    implements $ServiceSearchItemModelCopyWith<$Res> {
  _$ServiceSearchItemModelCopyWithImpl(this._value, this._then);

  final ServiceSearchItemModel _value;
  // ignore: unused_field
  final $Res Function(ServiceSearchItemModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? totalResult = freezed,
    Object? serviceTypeID = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeArabicName = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      totalResult: totalResult == freezed
          ? _value.totalResult
          : totalResult // ignore: cast_nullable_to_non_nullable
              as int,
      serviceTypeID: serviceTypeID == freezed
          ? _value.serviceTypeID
          : serviceTypeID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceSearchItemModelCopyWith<$Res>
    implements $ServiceSearchItemModelCopyWith<$Res> {
  factory _$$_ServiceSearchItemModelCopyWith(_$_ServiceSearchItemModel value,
          $Res Function(_$_ServiceSearchItemModel) then) =
      __$$_ServiceSearchItemModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      int totalResult,
      String serviceTypeID,
      String serviceTypeEnglishName,
      String serviceTypeArabicName});
}

/// @nodoc
class __$$_ServiceSearchItemModelCopyWithImpl<$Res>
    extends _$ServiceSearchItemModelCopyWithImpl<$Res>
    implements _$$_ServiceSearchItemModelCopyWith<$Res> {
  __$$_ServiceSearchItemModelCopyWithImpl(_$_ServiceSearchItemModel _value,
      $Res Function(_$_ServiceSearchItemModel) _then)
      : super(_value, (v) => _then(v as _$_ServiceSearchItemModel));

  @override
  _$_ServiceSearchItemModel get _value =>
      super._value as _$_ServiceSearchItemModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? totalResult = freezed,
    Object? serviceTypeID = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeArabicName = freezed,
  }) {
    return _then(_$_ServiceSearchItemModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      totalResult: totalResult == freezed
          ? _value.totalResult
          : totalResult // ignore: cast_nullable_to_non_nullable
              as int,
      serviceTypeID: serviceTypeID == freezed
          ? _value.serviceTypeID
          : serviceTypeID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceSearchItemModel extends _ServiceSearchItemModel {
  _$_ServiceSearchItemModel(
      {required this.id,
      required this.totalResult,
      required this.serviceTypeID,
      required this.serviceTypeEnglishName,
      required this.serviceTypeArabicName})
      : super._();

  factory _$_ServiceSearchItemModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceSearchItemModelFromJson(json);

  @override
  final String id;
  @override
  final int totalResult;
  @override
  final String serviceTypeID;
  @override
  final String serviceTypeEnglishName;
  @override
  final String serviceTypeArabicName;

  @override
  String toString() {
    return 'ServiceSearchItemModel(id: $id, totalResult: $totalResult, serviceTypeID: $serviceTypeID, serviceTypeEnglishName: $serviceTypeEnglishName, serviceTypeArabicName: $serviceTypeArabicName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServiceSearchItemModel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.totalResult, totalResult) &&
            const DeepCollectionEquality()
                .equals(other.serviceTypeID, serviceTypeID) &&
            const DeepCollectionEquality()
                .equals(other.serviceTypeEnglishName, serviceTypeEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.serviceTypeArabicName, serviceTypeArabicName));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(totalResult),
      const DeepCollectionEquality().hash(serviceTypeID),
      const DeepCollectionEquality().hash(serviceTypeEnglishName),
      const DeepCollectionEquality().hash(serviceTypeArabicName));

  @JsonKey(ignore: true)
  @override
  _$$_ServiceSearchItemModelCopyWith<_$_ServiceSearchItemModel> get copyWith =>
      __$$_ServiceSearchItemModelCopyWithImpl<_$_ServiceSearchItemModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceSearchItemModelToJson(
      this,
    );
  }
}

abstract class _ServiceSearchItemModel extends ServiceSearchItemModel {
  factory _ServiceSearchItemModel(
      {required final String id,
      required final int totalResult,
      required final String serviceTypeID,
      required final String serviceTypeEnglishName,
      required final String serviceTypeArabicName}) = _$_ServiceSearchItemModel;
  _ServiceSearchItemModel._() : super._();

  factory _ServiceSearchItemModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceSearchItemModel.fromJson;

  @override
  String get id;
  @override
  int get totalResult;
  @override
  String get serviceTypeID;
  @override
  String get serviceTypeEnglishName;
  @override
  String get serviceTypeArabicName;
  @override
  @JsonKey(ignore: true)
  _$$_ServiceSearchItemModelCopyWith<_$_ServiceSearchItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}
