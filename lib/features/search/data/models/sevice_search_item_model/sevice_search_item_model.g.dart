// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sevice_search_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceSearchItemModel _$$_ServiceSearchItemModelFromJson(
        Map<String, dynamic> json) =>
    _$_ServiceSearchItemModel(
      id: json['id'] as String,
      totalResult: json['totalResult'] as int,
      serviceTypeID: json['serviceTypeID'] as String,
      serviceTypeEnglishName: json['serviceTypeEnglishName'] as String,
      serviceTypeArabicName: json['serviceTypeArabicName'] as String,
    );

Map<String, dynamic> _$$_ServiceSearchItemModelToJson(
        _$_ServiceSearchItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'totalResult': instance.totalResult,
      'serviceTypeID': instance.serviceTypeID,
      'serviceTypeEnglishName': instance.serviceTypeEnglishName,
      'serviceTypeArabicName': instance.serviceTypeArabicName,
    };
