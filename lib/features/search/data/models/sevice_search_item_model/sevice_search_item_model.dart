import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'sevice_search_item_model.freezed.dart';
part 'sevice_search_item_model.g.dart';

@freezed
class ServiceSearchItemModel with _$ServiceSearchItemModel {
  ServiceSearchItemModel._();

  @JsonSerializable(explicitToJson: true)
  factory ServiceSearchItemModel({
    required String id,
    required int totalResult,
    required String serviceTypeID,
    required String serviceTypeEnglishName,
    required String serviceTypeArabicName,
    // required int businessServiceID,
    // required String businessID,
    // required String businessEnglishName,
    // required String businessArabicName,
    // required String? serviceGender,
    // required String? businessGenderEN,
    // required String? businessGenderAR,
    // required double latitude,
    // required double longitude,
    // required String businessAddressEN,
    // required String businessAddressAR,
    // required String distance,
    // required String eBusinessServiceID,
    // required String businessServiceEnglishName,
    // required String businessServiceArabicName,
    // required num servicePrice,
    // required String serviceID,
    // required String serviceEnglishName,
    // required String serviceArabicName,
    // required String serviceAvatar,
    // required String currencyEnglishName,
    // required String currencyArabicName,
    // required String serviceTypeID,
    // required String serviceTypeEnglishName,
    // required String serviceTypeArabicName,
    // required double serviceRating,
    // required double businessRating,
    // required int serviceDuration,
    // required String serviceDurationText,
    // required String businessGender,

  }) = _ServiceSearchItemModel;

  factory ServiceSearchItemModel.fromJson(Map<String, dynamic> json) =>
      _$ServiceSearchItemModelFromJson(json);

  String getServiceTypeName() {
    return getIt<Utilities>().getLocalizedValue(serviceTypeArabicName, serviceTypeEnglishName);
  }
}
