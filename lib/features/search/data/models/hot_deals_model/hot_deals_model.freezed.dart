// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'hot_deals_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

HotDealsModel _$HotDealsModelFromJson(Map<String, dynamic> json) {
  return _HotDealsModel.fromJson(json);
}

/// @nodoc
mixin _$HotDealsModel {
  int get totalRecords => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  List<HotDealsItemModel> get itemsList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HotDealsModelCopyWith<HotDealsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HotDealsModelCopyWith<$Res> {
  factory $HotDealsModelCopyWith(
          HotDealsModel value, $Res Function(HotDealsModel) then) =
      _$HotDealsModelCopyWithImpl<$Res>;
  $Res call(
      {int totalRecords, int pageSize, List<HotDealsItemModel> itemsList});
}

/// @nodoc
class _$HotDealsModelCopyWithImpl<$Res>
    implements $HotDealsModelCopyWith<$Res> {
  _$HotDealsModelCopyWithImpl(this._value, this._then);

  final HotDealsModel _value;
  // ignore: unused_field
  final $Res Function(HotDealsModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<HotDealsItemModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_HotDealsModelCopyWith<$Res>
    implements $HotDealsModelCopyWith<$Res> {
  factory _$$_HotDealsModelCopyWith(
          _$_HotDealsModel value, $Res Function(_$_HotDealsModel) then) =
      __$$_HotDealsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {int totalRecords, int pageSize, List<HotDealsItemModel> itemsList});
}

/// @nodoc
class __$$_HotDealsModelCopyWithImpl<$Res>
    extends _$HotDealsModelCopyWithImpl<$Res>
    implements _$$_HotDealsModelCopyWith<$Res> {
  __$$_HotDealsModelCopyWithImpl(
      _$_HotDealsModel _value, $Res Function(_$_HotDealsModel) _then)
      : super(_value, (v) => _then(v as _$_HotDealsModel));

  @override
  _$_HotDealsModel get _value => super._value as _$_HotDealsModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_HotDealsModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<HotDealsItemModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_HotDealsModel implements _HotDealsModel {
  _$_HotDealsModel(
      {required this.totalRecords,
      required this.pageSize,
      required final List<HotDealsItemModel> itemsList})
      : _itemsList = itemsList;

  factory _$_HotDealsModel.fromJson(Map<String, dynamic> json) =>
      _$$_HotDealsModelFromJson(json);

  @override
  final int totalRecords;
  @override
  final int pageSize;
  final List<HotDealsItemModel> _itemsList;
  @override
  List<HotDealsItemModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'HotDealsModel(totalRecords: $totalRecords, pageSize: $pageSize, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HotDealsModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality().equals(other.pageSize, pageSize) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(pageSize),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_HotDealsModelCopyWith<_$_HotDealsModel> get copyWith =>
      __$$_HotDealsModelCopyWithImpl<_$_HotDealsModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_HotDealsModelToJson(
      this,
    );
  }
}

abstract class _HotDealsModel implements HotDealsModel {
  factory _HotDealsModel(
      {required final int totalRecords,
      required final int pageSize,
      required final List<HotDealsItemModel> itemsList}) = _$_HotDealsModel;

  factory _HotDealsModel.fromJson(Map<String, dynamic> json) =
      _$_HotDealsModel.fromJson;

  @override
  int get totalRecords;
  @override
  int get pageSize;
  @override
  List<HotDealsItemModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_HotDealsModelCopyWith<_$_HotDealsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
