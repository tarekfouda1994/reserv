// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hot_deals_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_HotDealsModel _$$_HotDealsModelFromJson(Map<String, dynamic> json) =>
    _$_HotDealsModel(
      totalRecords: json['totalRecords'] as int,
      pageSize: json['pageSize'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map((e) => HotDealsItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_HotDealsModelToJson(_$_HotDealsModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'pageSize': instance.pageSize,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
