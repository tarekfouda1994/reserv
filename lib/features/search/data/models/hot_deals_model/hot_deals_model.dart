import 'package:flutter_tdd/features/search/data/models/hot_deals_item_model/hot_deals_item_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'hot_deals_model.freezed.dart';
part 'hot_deals_model.g.dart';

@freezed
class HotDealsModel with _$HotDealsModel {
  @JsonSerializable(explicitToJson: true)
  factory HotDealsModel({
    required int totalRecords,
    required int pageSize,
    required List<HotDealsItemModel> itemsList,
  }) = _HotDealsModel;

  factory HotDealsModel.fromJson(Map<String, dynamic> json) => _$HotDealsModelFromJson(json);
}
