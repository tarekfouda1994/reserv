import 'package:freezed_annotation/freezed_annotation.dart';

part 'count_chat_model.freezed.dart';
part 'count_chat_model.g.dart';

@freezed
class CountChatModel with _$CountChatModel{
  @JsonSerializable(explicitToJson: true)
  factory CountChatModel({
required int serverUnReadMessageCount,
  }) = _CountChatModel;


  factory CountChatModel.fromJson(Map<String, dynamic> json) =>
      _$CountChatModelFromJson(json);
}