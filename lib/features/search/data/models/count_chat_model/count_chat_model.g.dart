// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'count_chat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CountChatModel _$$_CountChatModelFromJson(Map<String, dynamic> json) =>
    _$_CountChatModel(
      serverUnReadMessageCount: json['serverUnReadMessageCount'] as int,
    );

Map<String, dynamic> _$$_CountChatModelToJson(_$_CountChatModel instance) =>
    <String, dynamic>{
      'serverUnReadMessageCount': instance.serverUnReadMessageCount,
    };
