// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'count_chat_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CountChatModel _$CountChatModelFromJson(Map<String, dynamic> json) {
  return _CountChatModel.fromJson(json);
}

/// @nodoc
mixin _$CountChatModel {
  int get serverUnReadMessageCount => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CountChatModelCopyWith<CountChatModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CountChatModelCopyWith<$Res> {
  factory $CountChatModelCopyWith(
          CountChatModel value, $Res Function(CountChatModel) then) =
      _$CountChatModelCopyWithImpl<$Res>;
  $Res call({int serverUnReadMessageCount});
}

/// @nodoc
class _$CountChatModelCopyWithImpl<$Res>
    implements $CountChatModelCopyWith<$Res> {
  _$CountChatModelCopyWithImpl(this._value, this._then);

  final CountChatModel _value;
  // ignore: unused_field
  final $Res Function(CountChatModel) _then;

  @override
  $Res call({
    Object? serverUnReadMessageCount = freezed,
  }) {
    return _then(_value.copyWith(
      serverUnReadMessageCount: serverUnReadMessageCount == freezed
          ? _value.serverUnReadMessageCount
          : serverUnReadMessageCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_CountChatModelCopyWith<$Res>
    implements $CountChatModelCopyWith<$Res> {
  factory _$$_CountChatModelCopyWith(
          _$_CountChatModel value, $Res Function(_$_CountChatModel) then) =
      __$$_CountChatModelCopyWithImpl<$Res>;
  @override
  $Res call({int serverUnReadMessageCount});
}

/// @nodoc
class __$$_CountChatModelCopyWithImpl<$Res>
    extends _$CountChatModelCopyWithImpl<$Res>
    implements _$$_CountChatModelCopyWith<$Res> {
  __$$_CountChatModelCopyWithImpl(
      _$_CountChatModel _value, $Res Function(_$_CountChatModel) _then)
      : super(_value, (v) => _then(v as _$_CountChatModel));

  @override
  _$_CountChatModel get _value => super._value as _$_CountChatModel;

  @override
  $Res call({
    Object? serverUnReadMessageCount = freezed,
  }) {
    return _then(_$_CountChatModel(
      serverUnReadMessageCount: serverUnReadMessageCount == freezed
          ? _value.serverUnReadMessageCount
          : serverUnReadMessageCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_CountChatModel implements _CountChatModel {
  _$_CountChatModel({required this.serverUnReadMessageCount});

  factory _$_CountChatModel.fromJson(Map<String, dynamic> json) =>
      _$$_CountChatModelFromJson(json);

  @override
  final int serverUnReadMessageCount;

  @override
  String toString() {
    return 'CountChatModel(serverUnReadMessageCount: $serverUnReadMessageCount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CountChatModel &&
            const DeepCollectionEquality().equals(
                other.serverUnReadMessageCount, serverUnReadMessageCount));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType,
      const DeepCollectionEquality().hash(serverUnReadMessageCount));

  @JsonKey(ignore: true)
  @override
  _$$_CountChatModelCopyWith<_$_CountChatModel> get copyWith =>
      __$$_CountChatModelCopyWithImpl<_$_CountChatModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CountChatModelToJson(
      this,
    );
  }
}

abstract class _CountChatModel implements CountChatModel {
  factory _CountChatModel({required final int serverUnReadMessageCount}) =
      _$_CountChatModel;

  factory _CountChatModel.fromJson(Map<String, dynamic> json) =
      _$_CountChatModel.fromJson;

  @override
  int get serverUnReadMessageCount;
  @override
  @JsonKey(ignore: true)
  _$$_CountChatModelCopyWith<_$_CountChatModel> get copyWith =>
      throw _privateConstructorUsedError;
}
