import 'package:flutter_tdd/features/search/data/models/business_item_search_model/business_item_search_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'business_search_model.freezed.dart';
part 'business_search_model.g.dart';

@freezed
class BusinessSearchModel with _$BusinessSearchModel {
  @JsonSerializable(explicitToJson: true)
  factory BusinessSearchModel({
    required int totalRecords,
    required int pageSize,
    required List<BusinessItemSearchModel> itemsList,
  }) = _BusinessSearchModel;

  factory BusinessSearchModel.fromJson(Map<String, dynamic> json) =>
      _$BusinessSearchModelFromJson(json);
}
