// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_search_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BusinessSearchModel _$$_BusinessSearchModelFromJson(
        Map<String, dynamic> json) =>
    _$_BusinessSearchModel(
      totalRecords: json['totalRecords'] as int,
      pageSize: json['pageSize'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map((e) =>
              BusinessItemSearchModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_BusinessSearchModelToJson(
        _$_BusinessSearchModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'pageSize': instance.pageSize,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
