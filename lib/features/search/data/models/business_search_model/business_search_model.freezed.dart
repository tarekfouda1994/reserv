// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'business_search_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BusinessSearchModel _$BusinessSearchModelFromJson(Map<String, dynamic> json) {
  return _BusinessSearchModel.fromJson(json);
}

/// @nodoc
mixin _$BusinessSearchModel {
  int get totalRecords => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  List<BusinessItemSearchModel> get itemsList =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BusinessSearchModelCopyWith<BusinessSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BusinessSearchModelCopyWith<$Res> {
  factory $BusinessSearchModelCopyWith(
          BusinessSearchModel value, $Res Function(BusinessSearchModel) then) =
      _$BusinessSearchModelCopyWithImpl<$Res>;
  $Res call(
      {int totalRecords,
      int pageSize,
      List<BusinessItemSearchModel> itemsList});
}

/// @nodoc
class _$BusinessSearchModelCopyWithImpl<$Res>
    implements $BusinessSearchModelCopyWith<$Res> {
  _$BusinessSearchModelCopyWithImpl(this._value, this._then);

  final BusinessSearchModel _value;
  // ignore: unused_field
  final $Res Function(BusinessSearchModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<BusinessItemSearchModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_BusinessSearchModelCopyWith<$Res>
    implements $BusinessSearchModelCopyWith<$Res> {
  factory _$$_BusinessSearchModelCopyWith(_$_BusinessSearchModel value,
          $Res Function(_$_BusinessSearchModel) then) =
      __$$_BusinessSearchModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {int totalRecords,
      int pageSize,
      List<BusinessItemSearchModel> itemsList});
}

/// @nodoc
class __$$_BusinessSearchModelCopyWithImpl<$Res>
    extends _$BusinessSearchModelCopyWithImpl<$Res>
    implements _$$_BusinessSearchModelCopyWith<$Res> {
  __$$_BusinessSearchModelCopyWithImpl(_$_BusinessSearchModel _value,
      $Res Function(_$_BusinessSearchModel) _then)
      : super(_value, (v) => _then(v as _$_BusinessSearchModel));

  @override
  _$_BusinessSearchModel get _value => super._value as _$_BusinessSearchModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_BusinessSearchModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<BusinessItemSearchModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BusinessSearchModel implements _BusinessSearchModel {
  _$_BusinessSearchModel(
      {required this.totalRecords,
      required this.pageSize,
      required final List<BusinessItemSearchModel> itemsList})
      : _itemsList = itemsList;

  factory _$_BusinessSearchModel.fromJson(Map<String, dynamic> json) =>
      _$$_BusinessSearchModelFromJson(json);

  @override
  final int totalRecords;
  @override
  final int pageSize;
  final List<BusinessItemSearchModel> _itemsList;
  @override
  List<BusinessItemSearchModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'BusinessSearchModel(totalRecords: $totalRecords, pageSize: $pageSize, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BusinessSearchModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality().equals(other.pageSize, pageSize) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(pageSize),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_BusinessSearchModelCopyWith<_$_BusinessSearchModel> get copyWith =>
      __$$_BusinessSearchModelCopyWithImpl<_$_BusinessSearchModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BusinessSearchModelToJson(
      this,
    );
  }
}

abstract class _BusinessSearchModel implements BusinessSearchModel {
  factory _BusinessSearchModel(
          {required final int totalRecords,
          required final int pageSize,
          required final List<BusinessItemSearchModel> itemsList}) =
      _$_BusinessSearchModel;

  factory _BusinessSearchModel.fromJson(Map<String, dynamic> json) =
      _$_BusinessSearchModel.fromJson;

  @override
  int get totalRecords;
  @override
  int get pageSize;
  @override
  List<BusinessItemSearchModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_BusinessSearchModelCopyWith<_$_BusinessSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}
