import 'package:flutter_tdd/features/search/data/models/top_business_item_model/top_business_item_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'top_businesses_model.freezed.dart';
part 'top_businesses_model.g.dart';

@freezed
class TopBusinessesModel with _$TopBusinessesModel {
  @JsonSerializable(explicitToJson: true)
  factory TopBusinessesModel({
    required int totalRecords,
    required int pageSize,
    required List<TopBusinessItemModel> itemsList,
  }) = _TopBusinessesModel;

  factory TopBusinessesModel.fromJson(Map<String, dynamic> json) =>
      _$TopBusinessesModelFromJson(json);
}
