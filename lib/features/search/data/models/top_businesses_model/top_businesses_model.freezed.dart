// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'top_businesses_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TopBusinessesModel _$TopBusinessesModelFromJson(Map<String, dynamic> json) {
  return _TopBusinessesModel.fromJson(json);
}

/// @nodoc
mixin _$TopBusinessesModel {
  int get totalRecords => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  List<TopBusinessItemModel> get itemsList =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TopBusinessesModelCopyWith<TopBusinessesModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TopBusinessesModelCopyWith<$Res> {
  factory $TopBusinessesModelCopyWith(
          TopBusinessesModel value, $Res Function(TopBusinessesModel) then) =
      _$TopBusinessesModelCopyWithImpl<$Res>;
  $Res call(
      {int totalRecords, int pageSize, List<TopBusinessItemModel> itemsList});
}

/// @nodoc
class _$TopBusinessesModelCopyWithImpl<$Res>
    implements $TopBusinessesModelCopyWith<$Res> {
  _$TopBusinessesModelCopyWithImpl(this._value, this._then);

  final TopBusinessesModel _value;
  // ignore: unused_field
  final $Res Function(TopBusinessesModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<TopBusinessItemModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_TopBusinessesModelCopyWith<$Res>
    implements $TopBusinessesModelCopyWith<$Res> {
  factory _$$_TopBusinessesModelCopyWith(_$_TopBusinessesModel value,
          $Res Function(_$_TopBusinessesModel) then) =
      __$$_TopBusinessesModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {int totalRecords, int pageSize, List<TopBusinessItemModel> itemsList});
}

/// @nodoc
class __$$_TopBusinessesModelCopyWithImpl<$Res>
    extends _$TopBusinessesModelCopyWithImpl<$Res>
    implements _$$_TopBusinessesModelCopyWith<$Res> {
  __$$_TopBusinessesModelCopyWithImpl(
      _$_TopBusinessesModel _value, $Res Function(_$_TopBusinessesModel) _then)
      : super(_value, (v) => _then(v as _$_TopBusinessesModel));

  @override
  _$_TopBusinessesModel get _value => super._value as _$_TopBusinessesModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_TopBusinessesModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<TopBusinessItemModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_TopBusinessesModel implements _TopBusinessesModel {
  _$_TopBusinessesModel(
      {required this.totalRecords,
      required this.pageSize,
      required final List<TopBusinessItemModel> itemsList})
      : _itemsList = itemsList;

  factory _$_TopBusinessesModel.fromJson(Map<String, dynamic> json) =>
      _$$_TopBusinessesModelFromJson(json);

  @override
  final int totalRecords;
  @override
  final int pageSize;
  final List<TopBusinessItemModel> _itemsList;
  @override
  List<TopBusinessItemModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'TopBusinessesModel(totalRecords: $totalRecords, pageSize: $pageSize, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TopBusinessesModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality().equals(other.pageSize, pageSize) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(pageSize),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_TopBusinessesModelCopyWith<_$_TopBusinessesModel> get copyWith =>
      __$$_TopBusinessesModelCopyWithImpl<_$_TopBusinessesModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TopBusinessesModelToJson(
      this,
    );
  }
}

abstract class _TopBusinessesModel implements TopBusinessesModel {
  factory _TopBusinessesModel(
          {required final int totalRecords,
          required final int pageSize,
          required final List<TopBusinessItemModel> itemsList}) =
      _$_TopBusinessesModel;

  factory _TopBusinessesModel.fromJson(Map<String, dynamic> json) =
      _$_TopBusinessesModel.fromJson;

  @override
  int get totalRecords;
  @override
  int get pageSize;
  @override
  List<TopBusinessItemModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_TopBusinessesModelCopyWith<_$_TopBusinessesModel> get copyWith =>
      throw _privateConstructorUsedError;
}
