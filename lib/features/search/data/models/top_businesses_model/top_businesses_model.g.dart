// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'top_businesses_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TopBusinessesModel _$$_TopBusinessesModelFromJson(
        Map<String, dynamic> json) =>
    _$_TopBusinessesModel(
      totalRecords: json['totalRecords'] as int,
      pageSize: json['pageSize'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map((e) => TopBusinessItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TopBusinessesModelToJson(
        _$_TopBusinessesModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'pageSize': instance.pageSize,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
