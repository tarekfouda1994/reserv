// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'include_service_ids_label.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IncludeServiceIdsLabel _$IncludeServiceIdsLabelFromJson(
    Map<String, dynamic> json) {
  return _IncludeServiceIdsLabel.fromJson(json);
}

/// @nodoc
mixin _$IncludeServiceIdsLabel {
  num get value => throw _privateConstructorUsedError;
  String get label => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IncludeServiceIdsLabelCopyWith<IncludeServiceIdsLabel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IncludeServiceIdsLabelCopyWith<$Res> {
  factory $IncludeServiceIdsLabelCopyWith(IncludeServiceIdsLabel value,
          $Res Function(IncludeServiceIdsLabel) then) =
      _$IncludeServiceIdsLabelCopyWithImpl<$Res>;
  $Res call({num value, String label});
}

/// @nodoc
class _$IncludeServiceIdsLabelCopyWithImpl<$Res>
    implements $IncludeServiceIdsLabelCopyWith<$Res> {
  _$IncludeServiceIdsLabelCopyWithImpl(this._value, this._then);

  final IncludeServiceIdsLabel _value;
  // ignore: unused_field
  final $Res Function(IncludeServiceIdsLabel) _then;

  @override
  $Res call({
    Object? value = freezed,
    Object? label = freezed,
  }) {
    return _then(_value.copyWith(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_IncludeServiceIdsLabelCopyWith<$Res>
    implements $IncludeServiceIdsLabelCopyWith<$Res> {
  factory _$$_IncludeServiceIdsLabelCopyWith(_$_IncludeServiceIdsLabel value,
          $Res Function(_$_IncludeServiceIdsLabel) then) =
      __$$_IncludeServiceIdsLabelCopyWithImpl<$Res>;
  @override
  $Res call({num value, String label});
}

/// @nodoc
class __$$_IncludeServiceIdsLabelCopyWithImpl<$Res>
    extends _$IncludeServiceIdsLabelCopyWithImpl<$Res>
    implements _$$_IncludeServiceIdsLabelCopyWith<$Res> {
  __$$_IncludeServiceIdsLabelCopyWithImpl(_$_IncludeServiceIdsLabel _value,
      $Res Function(_$_IncludeServiceIdsLabel) _then)
      : super(_value, (v) => _then(v as _$_IncludeServiceIdsLabel));

  @override
  _$_IncludeServiceIdsLabel get _value =>
      super._value as _$_IncludeServiceIdsLabel;

  @override
  $Res call({
    Object? value = freezed,
    Object? label = freezed,
  }) {
    return _then(_$_IncludeServiceIdsLabel(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as num,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_IncludeServiceIdsLabel implements _IncludeServiceIdsLabel {
  _$_IncludeServiceIdsLabel({required this.value, required this.label});

  factory _$_IncludeServiceIdsLabel.fromJson(Map<String, dynamic> json) =>
      _$$_IncludeServiceIdsLabelFromJson(json);

  @override
  final num value;
  @override
  final String label;

  @override
  String toString() {
    return 'IncludeServiceIdsLabel(value: $value, label: $label)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_IncludeServiceIdsLabel &&
            const DeepCollectionEquality().equals(other.value, value) &&
            const DeepCollectionEquality().equals(other.label, label));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(value),
      const DeepCollectionEquality().hash(label));

  @JsonKey(ignore: true)
  @override
  _$$_IncludeServiceIdsLabelCopyWith<_$_IncludeServiceIdsLabel> get copyWith =>
      __$$_IncludeServiceIdsLabelCopyWithImpl<_$_IncludeServiceIdsLabel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_IncludeServiceIdsLabelToJson(
      this,
    );
  }
}

abstract class _IncludeServiceIdsLabel implements IncludeServiceIdsLabel {
  factory _IncludeServiceIdsLabel(
      {required final num value,
      required final String label}) = _$_IncludeServiceIdsLabel;

  factory _IncludeServiceIdsLabel.fromJson(Map<String, dynamic> json) =
      _$_IncludeServiceIdsLabel.fromJson;

  @override
  num get value;
  @override
  String get label;
  @override
  @JsonKey(ignore: true)
  _$$_IncludeServiceIdsLabelCopyWith<_$_IncludeServiceIdsLabel> get copyWith =>
      throw _privateConstructorUsedError;
}
