// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'include_service_ids_label.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_IncludeServiceIdsLabel _$$_IncludeServiceIdsLabelFromJson(
        Map<String, dynamic> json) =>
    _$_IncludeServiceIdsLabel(
      value: json['value'] as num,
      label: json['label'] as String,
    );

Map<String, dynamic> _$$_IncludeServiceIdsLabelToJson(
        _$_IncludeServiceIdsLabel instance) =>
    <String, dynamic>{
      'value': instance.value,
      'label': instance.label,
    };
