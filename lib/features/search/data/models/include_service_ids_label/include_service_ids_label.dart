import 'package:freezed_annotation/freezed_annotation.dart';

part 'include_service_ids_label.freezed.dart';
part 'include_service_ids_label.g.dart';

@freezed
class IncludeServiceIdsLabel with _$IncludeServiceIdsLabel {
  @JsonSerializable(explicitToJson: true)
  factory IncludeServiceIdsLabel({
    required num value,
    required String label,
  }) = _IncludeServiceIdsLabel;

  factory IncludeServiceIdsLabel.fromJson(Map<String, dynamic> json) =>
      _$IncludeServiceIdsLabelFromJson(json);
}
