// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_reviews_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceReviewsModel _$$_ServiceReviewsModelFromJson(
        Map<String, dynamic> json) =>
    _$_ServiceReviewsModel(
      rating: (json['rating'] as num).toDouble(),
      serviceTypeEnglishName: json['serviceTypeEnglishName'] as String,
      serviceTypeArabicName: json['serviceTypeArabicName'] as String,
      serviceEnglishName: json['serviceEnglishName'] as String,
      serviceArabicName: json['serviceArabicName'] as String,
      businessServiceId: json['businessServiceId'] as String? ?? '',
      formatedRemarksDate: json['formatedRemarksDate'] as String,
      id: json['id'] as String,
      remarks: json['remarks'] as String,
      remarksByEN: json['remarksByEN'] as String,
      remarksByAR: json['remarksByAR'] as String,
      remarksDate: DateTime.parse(json['remarksDate'] as String),
      picture: json['picture'] as String,
    );

Map<String, dynamic> _$$_ServiceReviewsModelToJson(
        _$_ServiceReviewsModel instance) =>
    <String, dynamic>{
      'rating': instance.rating,
      'serviceTypeEnglishName': instance.serviceTypeEnglishName,
      'serviceTypeArabicName': instance.serviceTypeArabicName,
      'serviceEnglishName': instance.serviceEnglishName,
      'serviceArabicName': instance.serviceArabicName,
      'businessServiceId': instance.businessServiceId,
      'formatedRemarksDate': instance.formatedRemarksDate,
      'id': instance.id,
      'remarks': instance.remarks,
      'remarksByEN': instance.remarksByEN,
      'remarksByAR': instance.remarksByAR,
      'remarksDate': instance.remarksDate.toIso8601String(),
      'picture': instance.picture,
    };
