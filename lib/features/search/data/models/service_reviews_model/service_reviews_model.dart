import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'service_reviews_model.freezed.dart';
part 'service_reviews_model.g.dart';

@freezed
class ServiceReviewsModel with _$ServiceReviewsModel {
  ServiceReviewsModel._();

  @JsonSerializable(explicitToJson: true)
  factory ServiceReviewsModel({
    required double rating,
    required String serviceTypeEnglishName,
    required String serviceTypeArabicName,
    required String serviceEnglishName,
    required String serviceArabicName,
    @JsonKey(defaultValue: "", nullable: true) required String businessServiceId,
    required String formatedRemarksDate,
    required String id,
    required String remarks,
    required String remarksByEN,
    required String remarksByAR,
    required DateTime remarksDate,
    required String picture,
  }) = _ServiceReviewsModel;

  factory ServiceReviewsModel.fromJson(Map<String, dynamic> json) =>
      _$ServiceReviewsModelFromJson(json);

  getServiceName() {
    return getIt<Utilities>().getLocalizedValue(serviceArabicName, serviceEnglishName);
  }

  getUserName() {
    return getIt<Utilities>().getLocalizedValue(remarksByAR, remarksByEN);
  }
}
