// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'service_reviews_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceReviewsModel _$ServiceReviewsModelFromJson(Map<String, dynamic> json) {
  return _ServiceReviewsModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceReviewsModel {
  double get rating => throw _privateConstructorUsedError;
  String get serviceTypeEnglishName => throw _privateConstructorUsedError;
  String get serviceTypeArabicName => throw _privateConstructorUsedError;
  String get serviceEnglishName => throw _privateConstructorUsedError;
  String get serviceArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get businessServiceId => throw _privateConstructorUsedError;
  String get formatedRemarksDate => throw _privateConstructorUsedError;
  String get id => throw _privateConstructorUsedError;
  String get remarks => throw _privateConstructorUsedError;
  String get remarksByEN => throw _privateConstructorUsedError;
  String get remarksByAR => throw _privateConstructorUsedError;
  DateTime get remarksDate => throw _privateConstructorUsedError;
  String get picture => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceReviewsModelCopyWith<ServiceReviewsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceReviewsModelCopyWith<$Res> {
  factory $ServiceReviewsModelCopyWith(
          ServiceReviewsModel value, $Res Function(ServiceReviewsModel) then) =
      _$ServiceReviewsModelCopyWithImpl<$Res>;
  $Res call(
      {double rating,
      String serviceTypeEnglishName,
      String serviceTypeArabicName,
      String serviceEnglishName,
      String serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true) String businessServiceId,
      String formatedRemarksDate,
      String id,
      String remarks,
      String remarksByEN,
      String remarksByAR,
      DateTime remarksDate,
      String picture});
}

/// @nodoc
class _$ServiceReviewsModelCopyWithImpl<$Res>
    implements $ServiceReviewsModelCopyWith<$Res> {
  _$ServiceReviewsModelCopyWithImpl(this._value, this._then);

  final ServiceReviewsModel _value;
  // ignore: unused_field
  final $Res Function(ServiceReviewsModel) _then;

  @override
  $Res call({
    Object? rating = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeArabicName = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? businessServiceId = freezed,
    Object? formatedRemarksDate = freezed,
    Object? id = freezed,
    Object? remarks = freezed,
    Object? remarksByEN = freezed,
    Object? remarksByAR = freezed,
    Object? remarksDate = freezed,
    Object? picture = freezed,
  }) {
    return _then(_value.copyWith(
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceId: businessServiceId == freezed
          ? _value.businessServiceId
          : businessServiceId // ignore: cast_nullable_to_non_nullable
              as String,
      formatedRemarksDate: formatedRemarksDate == freezed
          ? _value.formatedRemarksDate
          : formatedRemarksDate // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      remarks: remarks == freezed
          ? _value.remarks
          : remarks // ignore: cast_nullable_to_non_nullable
              as String,
      remarksByEN: remarksByEN == freezed
          ? _value.remarksByEN
          : remarksByEN // ignore: cast_nullable_to_non_nullable
              as String,
      remarksByAR: remarksByAR == freezed
          ? _value.remarksByAR
          : remarksByAR // ignore: cast_nullable_to_non_nullable
              as String,
      remarksDate: remarksDate == freezed
          ? _value.remarksDate
          : remarksDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      picture: picture == freezed
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceReviewsModelCopyWith<$Res>
    implements $ServiceReviewsModelCopyWith<$Res> {
  factory _$$_ServiceReviewsModelCopyWith(_$_ServiceReviewsModel value,
          $Res Function(_$_ServiceReviewsModel) then) =
      __$$_ServiceReviewsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {double rating,
      String serviceTypeEnglishName,
      String serviceTypeArabicName,
      String serviceEnglishName,
      String serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true) String businessServiceId,
      String formatedRemarksDate,
      String id,
      String remarks,
      String remarksByEN,
      String remarksByAR,
      DateTime remarksDate,
      String picture});
}

/// @nodoc
class __$$_ServiceReviewsModelCopyWithImpl<$Res>
    extends _$ServiceReviewsModelCopyWithImpl<$Res>
    implements _$$_ServiceReviewsModelCopyWith<$Res> {
  __$$_ServiceReviewsModelCopyWithImpl(_$_ServiceReviewsModel _value,
      $Res Function(_$_ServiceReviewsModel) _then)
      : super(_value, (v) => _then(v as _$_ServiceReviewsModel));

  @override
  _$_ServiceReviewsModel get _value => super._value as _$_ServiceReviewsModel;

  @override
  $Res call({
    Object? rating = freezed,
    Object? serviceTypeEnglishName = freezed,
    Object? serviceTypeArabicName = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? businessServiceId = freezed,
    Object? formatedRemarksDate = freezed,
    Object? id = freezed,
    Object? remarks = freezed,
    Object? remarksByEN = freezed,
    Object? remarksByAR = freezed,
    Object? remarksDate = freezed,
    Object? picture = freezed,
  }) {
    return _then(_$_ServiceReviewsModel(
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceTypeEnglishName: serviceTypeEnglishName == freezed
          ? _value.serviceTypeEnglishName
          : serviceTypeEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceTypeArabicName: serviceTypeArabicName == freezed
          ? _value.serviceTypeArabicName
          : serviceTypeArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceId: businessServiceId == freezed
          ? _value.businessServiceId
          : businessServiceId // ignore: cast_nullable_to_non_nullable
              as String,
      formatedRemarksDate: formatedRemarksDate == freezed
          ? _value.formatedRemarksDate
          : formatedRemarksDate // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      remarks: remarks == freezed
          ? _value.remarks
          : remarks // ignore: cast_nullable_to_non_nullable
              as String,
      remarksByEN: remarksByEN == freezed
          ? _value.remarksByEN
          : remarksByEN // ignore: cast_nullable_to_non_nullable
              as String,
      remarksByAR: remarksByAR == freezed
          ? _value.remarksByAR
          : remarksByAR // ignore: cast_nullable_to_non_nullable
              as String,
      remarksDate: remarksDate == freezed
          ? _value.remarksDate
          : remarksDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      picture: picture == freezed
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceReviewsModel extends _ServiceReviewsModel {
  _$_ServiceReviewsModel(
      {required this.rating,
      required this.serviceTypeEnglishName,
      required this.serviceTypeArabicName,
      required this.serviceEnglishName,
      required this.serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.businessServiceId,
      required this.formatedRemarksDate,
      required this.id,
      required this.remarks,
      required this.remarksByEN,
      required this.remarksByAR,
      required this.remarksDate,
      required this.picture})
      : super._();

  factory _$_ServiceReviewsModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceReviewsModelFromJson(json);

  @override
  final double rating;
  @override
  final String serviceTypeEnglishName;
  @override
  final String serviceTypeArabicName;
  @override
  final String serviceEnglishName;
  @override
  final String serviceArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String businessServiceId;
  @override
  final String formatedRemarksDate;
  @override
  final String id;
  @override
  final String remarks;
  @override
  final String remarksByEN;
  @override
  final String remarksByAR;
  @override
  final DateTime remarksDate;
  @override
  final String picture;

  @override
  String toString() {
    return 'ServiceReviewsModel(rating: $rating, serviceTypeEnglishName: $serviceTypeEnglishName, serviceTypeArabicName: $serviceTypeArabicName, serviceEnglishName: $serviceEnglishName, serviceArabicName: $serviceArabicName, businessServiceId: $businessServiceId, formatedRemarksDate: $formatedRemarksDate, id: $id, remarks: $remarks, remarksByEN: $remarksByEN, remarksByAR: $remarksByAR, remarksDate: $remarksDate, picture: $picture)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServiceReviewsModel &&
            const DeepCollectionEquality().equals(other.rating, rating) &&
            const DeepCollectionEquality()
                .equals(other.serviceTypeEnglishName, serviceTypeEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.serviceTypeArabicName, serviceTypeArabicName) &&
            const DeepCollectionEquality()
                .equals(other.serviceEnglishName, serviceEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.serviceArabicName, serviceArabicName) &&
            const DeepCollectionEquality()
                .equals(other.businessServiceId, businessServiceId) &&
            const DeepCollectionEquality()
                .equals(other.formatedRemarksDate, formatedRemarksDate) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.remarks, remarks) &&
            const DeepCollectionEquality()
                .equals(other.remarksByEN, remarksByEN) &&
            const DeepCollectionEquality()
                .equals(other.remarksByAR, remarksByAR) &&
            const DeepCollectionEquality()
                .equals(other.remarksDate, remarksDate) &&
            const DeepCollectionEquality().equals(other.picture, picture));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(rating),
      const DeepCollectionEquality().hash(serviceTypeEnglishName),
      const DeepCollectionEquality().hash(serviceTypeArabicName),
      const DeepCollectionEquality().hash(serviceEnglishName),
      const DeepCollectionEquality().hash(serviceArabicName),
      const DeepCollectionEquality().hash(businessServiceId),
      const DeepCollectionEquality().hash(formatedRemarksDate),
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(remarks),
      const DeepCollectionEquality().hash(remarksByEN),
      const DeepCollectionEquality().hash(remarksByAR),
      const DeepCollectionEquality().hash(remarksDate),
      const DeepCollectionEquality().hash(picture));

  @JsonKey(ignore: true)
  @override
  _$$_ServiceReviewsModelCopyWith<_$_ServiceReviewsModel> get copyWith =>
      __$$_ServiceReviewsModelCopyWithImpl<_$_ServiceReviewsModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceReviewsModelToJson(
      this,
    );
  }
}

abstract class _ServiceReviewsModel extends ServiceReviewsModel {
  factory _ServiceReviewsModel(
      {required final double rating,
      required final String serviceTypeEnglishName,
      required final String serviceTypeArabicName,
      required final String serviceEnglishName,
      required final String serviceArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required final String businessServiceId,
      required final String formatedRemarksDate,
      required final String id,
      required final String remarks,
      required final String remarksByEN,
      required final String remarksByAR,
      required final DateTime remarksDate,
      required final String picture}) = _$_ServiceReviewsModel;
  _ServiceReviewsModel._() : super._();

  factory _ServiceReviewsModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceReviewsModel.fromJson;

  @override
  double get rating;
  @override
  String get serviceTypeEnglishName;
  @override
  String get serviceTypeArabicName;
  @override
  String get serviceEnglishName;
  @override
  String get serviceArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get businessServiceId;
  @override
  String get formatedRemarksDate;
  @override
  String get id;
  @override
  String get remarks;
  @override
  String get remarksByEN;
  @override
  String get remarksByAR;
  @override
  DateTime get remarksDate;
  @override
  String get picture;
  @override
  @JsonKey(ignore: true)
  _$$_ServiceReviewsModelCopyWith<_$_ServiceReviewsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
