import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'popular_cat_model.freezed.dart';
part 'popular_cat_model.g.dart';

@unfreezed
class PopularCatModel with _$PopularCatModel {
  PopularCatModel._();

  @JsonSerializable(explicitToJson: true)
  factory PopularCatModel({
    @JsonKey(defaultValue: 0,nullable: true) required int parentServiceTypeId,
   @JsonKey(defaultValue: 0,nullable: true) required int eBusinessTypeId,
    required String arabicName,
    required String englishName,
    required String colorClass,
    required String iconClass,
    required String avatar,
    required int id,
    required bool isActive,
    required bool? isSelected,
    required String encryptId,
  }) = _PopularCatModel;

  factory PopularCatModel.fromJson(Map<String, dynamic> json) =>
      _$PopularCatModelFromJson(json);

  String getPopularCatName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}
