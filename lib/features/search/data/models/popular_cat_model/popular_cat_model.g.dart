// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_cat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PopularCatModel _$$_PopularCatModelFromJson(Map<String, dynamic> json) =>
    _$_PopularCatModel(
      parentServiceTypeId: json['parentServiceTypeId'] as int? ?? 0,
      eBusinessTypeId: json['eBusinessTypeId'] as int? ?? 0,
      arabicName: json['arabicName'] as String,
      englishName: json['englishName'] as String,
      colorClass: json['colorClass'] as String,
      iconClass: json['iconClass'] as String,
      avatar: json['avatar'] as String,
      id: json['id'] as int,
      isActive: json['isActive'] as bool,
      isSelected: json['isSelected'] as bool?,
      encryptId: json['encryptId'] as String,
    );

Map<String, dynamic> _$$_PopularCatModelToJson(_$_PopularCatModel instance) =>
    <String, dynamic>{
      'parentServiceTypeId': instance.parentServiceTypeId,
      'eBusinessTypeId': instance.eBusinessTypeId,
      'arabicName': instance.arabicName,
      'englishName': instance.englishName,
      'colorClass': instance.colorClass,
      'iconClass': instance.iconClass,
      'avatar': instance.avatar,
      'id': instance.id,
      'isActive': instance.isActive,
      'isSelected': instance.isSelected,
      'encryptId': instance.encryptId,
    };
