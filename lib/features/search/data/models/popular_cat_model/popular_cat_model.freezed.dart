// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'popular_cat_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PopularCatModel _$PopularCatModelFromJson(Map<String, dynamic> json) {
  return _PopularCatModel.fromJson(json);
}

/// @nodoc
mixin _$PopularCatModel {
  @JsonKey(defaultValue: 0, nullable: true)
  int get parentServiceTypeId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set parentServiceTypeId(int value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  int get eBusinessTypeId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set eBusinessTypeId(int value) => throw _privateConstructorUsedError;
  String get arabicName => throw _privateConstructorUsedError;
  set arabicName(String value) => throw _privateConstructorUsedError;
  String get englishName => throw _privateConstructorUsedError;
  set englishName(String value) => throw _privateConstructorUsedError;
  String get colorClass => throw _privateConstructorUsedError;
  set colorClass(String value) => throw _privateConstructorUsedError;
  String get iconClass => throw _privateConstructorUsedError;
  set iconClass(String value) => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  set avatar(String value) => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  set id(int value) => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  set isActive(bool value) => throw _privateConstructorUsedError;
  bool? get isSelected => throw _privateConstructorUsedError;
  set isSelected(bool? value) => throw _privateConstructorUsedError;
  String get encryptId => throw _privateConstructorUsedError;
  set encryptId(String value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PopularCatModelCopyWith<PopularCatModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PopularCatModelCopyWith<$Res> {
  factory $PopularCatModelCopyWith(
          PopularCatModel value, $Res Function(PopularCatModel) then) =
      _$PopularCatModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: 0, nullable: true) int parentServiceTypeId,
      @JsonKey(defaultValue: 0, nullable: true) int eBusinessTypeId,
      String arabicName,
      String englishName,
      String colorClass,
      String iconClass,
      String avatar,
      int id,
      bool isActive,
      bool? isSelected,
      String encryptId});
}

/// @nodoc
class _$PopularCatModelCopyWithImpl<$Res>
    implements $PopularCatModelCopyWith<$Res> {
  _$PopularCatModelCopyWithImpl(this._value, this._then);

  final PopularCatModel _value;
  // ignore: unused_field
  final $Res Function(PopularCatModel) _then;

  @override
  $Res call({
    Object? parentServiceTypeId = freezed,
    Object? eBusinessTypeId = freezed,
    Object? arabicName = freezed,
    Object? englishName = freezed,
    Object? colorClass = freezed,
    Object? iconClass = freezed,
    Object? avatar = freezed,
    Object? id = freezed,
    Object? isActive = freezed,
    Object? isSelected = freezed,
    Object? encryptId = freezed,
  }) {
    return _then(_value.copyWith(
      parentServiceTypeId: parentServiceTypeId == freezed
          ? _value.parentServiceTypeId
          : parentServiceTypeId // ignore: cast_nullable_to_non_nullable
              as int,
      eBusinessTypeId: eBusinessTypeId == freezed
          ? _value.eBusinessTypeId
          : eBusinessTypeId // ignore: cast_nullable_to_non_nullable
              as int,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      colorClass: colorClass == freezed
          ? _value.colorClass
          : colorClass // ignore: cast_nullable_to_non_nullable
              as String,
      iconClass: iconClass == freezed
          ? _value.iconClass
          : iconClass // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      isSelected: isSelected == freezed
          ? _value.isSelected
          : isSelected // ignore: cast_nullable_to_non_nullable
              as bool?,
      encryptId: encryptId == freezed
          ? _value.encryptId
          : encryptId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PopularCatModelCopyWith<$Res>
    implements $PopularCatModelCopyWith<$Res> {
  factory _$$_PopularCatModelCopyWith(
          _$_PopularCatModel value, $Res Function(_$_PopularCatModel) then) =
      __$$_PopularCatModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: 0, nullable: true) int parentServiceTypeId,
      @JsonKey(defaultValue: 0, nullable: true) int eBusinessTypeId,
      String arabicName,
      String englishName,
      String colorClass,
      String iconClass,
      String avatar,
      int id,
      bool isActive,
      bool? isSelected,
      String encryptId});
}

/// @nodoc
class __$$_PopularCatModelCopyWithImpl<$Res>
    extends _$PopularCatModelCopyWithImpl<$Res>
    implements _$$_PopularCatModelCopyWith<$Res> {
  __$$_PopularCatModelCopyWithImpl(
      _$_PopularCatModel _value, $Res Function(_$_PopularCatModel) _then)
      : super(_value, (v) => _then(v as _$_PopularCatModel));

  @override
  _$_PopularCatModel get _value => super._value as _$_PopularCatModel;

  @override
  $Res call({
    Object? parentServiceTypeId = freezed,
    Object? eBusinessTypeId = freezed,
    Object? arabicName = freezed,
    Object? englishName = freezed,
    Object? colorClass = freezed,
    Object? iconClass = freezed,
    Object? avatar = freezed,
    Object? id = freezed,
    Object? isActive = freezed,
    Object? isSelected = freezed,
    Object? encryptId = freezed,
  }) {
    return _then(_$_PopularCatModel(
      parentServiceTypeId: parentServiceTypeId == freezed
          ? _value.parentServiceTypeId
          : parentServiceTypeId // ignore: cast_nullable_to_non_nullable
              as int,
      eBusinessTypeId: eBusinessTypeId == freezed
          ? _value.eBusinessTypeId
          : eBusinessTypeId // ignore: cast_nullable_to_non_nullable
              as int,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      colorClass: colorClass == freezed
          ? _value.colorClass
          : colorClass // ignore: cast_nullable_to_non_nullable
              as String,
      iconClass: iconClass == freezed
          ? _value.iconClass
          : iconClass // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      isSelected: isSelected == freezed
          ? _value.isSelected
          : isSelected // ignore: cast_nullable_to_non_nullable
              as bool?,
      encryptId: encryptId == freezed
          ? _value.encryptId
          : encryptId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PopularCatModel extends _PopularCatModel {
  _$_PopularCatModel(
      {@JsonKey(defaultValue: 0, nullable: true)
          required this.parentServiceTypeId,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.eBusinessTypeId,
      required this.arabicName,
      required this.englishName,
      required this.colorClass,
      required this.iconClass,
      required this.avatar,
      required this.id,
      required this.isActive,
      required this.isSelected,
      required this.encryptId})
      : super._();

  factory _$_PopularCatModel.fromJson(Map<String, dynamic> json) =>
      _$$_PopularCatModelFromJson(json);

  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int parentServiceTypeId;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int eBusinessTypeId;
  @override
  String arabicName;
  @override
  String englishName;
  @override
  String colorClass;
  @override
  String iconClass;
  @override
  String avatar;
  @override
  int id;
  @override
  bool isActive;
  @override
  bool? isSelected;
  @override
  String encryptId;

  @override
  String toString() {
    return 'PopularCatModel(parentServiceTypeId: $parentServiceTypeId, eBusinessTypeId: $eBusinessTypeId, arabicName: $arabicName, englishName: $englishName, colorClass: $colorClass, iconClass: $iconClass, avatar: $avatar, id: $id, isActive: $isActive, isSelected: $isSelected, encryptId: $encryptId)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_PopularCatModelCopyWith<_$_PopularCatModel> get copyWith =>
      __$$_PopularCatModelCopyWithImpl<_$_PopularCatModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PopularCatModelToJson(
      this,
    );
  }
}

abstract class _PopularCatModel extends PopularCatModel {
  factory _PopularCatModel(
      {@JsonKey(defaultValue: 0, nullable: true)
          required int parentServiceTypeId,
      @JsonKey(defaultValue: 0, nullable: true)
          required int eBusinessTypeId,
      required String arabicName,
      required String englishName,
      required String colorClass,
      required String iconClass,
      required String avatar,
      required int id,
      required bool isActive,
      required bool? isSelected,
      required String encryptId}) = _$_PopularCatModel;
  _PopularCatModel._() : super._();

  factory _PopularCatModel.fromJson(Map<String, dynamic> json) =
      _$_PopularCatModel.fromJson;

  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int get parentServiceTypeId;
  @JsonKey(defaultValue: 0, nullable: true)
  set parentServiceTypeId(int value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int get eBusinessTypeId;
  @JsonKey(defaultValue: 0, nullable: true)
  set eBusinessTypeId(int value);
  @override
  String get arabicName;
  set arabicName(String value);
  @override
  String get englishName;
  set englishName(String value);
  @override
  String get colorClass;
  set colorClass(String value);
  @override
  String get iconClass;
  set iconClass(String value);
  @override
  String get avatar;
  set avatar(String value);
  @override
  int get id;
  set id(int value);
  @override
  bool get isActive;
  set isActive(bool value);
  @override
  bool? get isSelected;
  set isSelected(bool? value);
  @override
  String get encryptId;
  set encryptId(String value);
  @override
  @JsonKey(ignore: true)
  _$$_PopularCatModelCopyWith<_$_PopularCatModel> get copyWith =>
      throw _privateConstructorUsedError;
}
