// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hot_deals_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_HotDealsItemModel _$$_HotDealsItemModelFromJson(Map<String, dynamic> json) =>
    _$_HotDealsItemModel(
      promoteAdsID: json['promoteAdsID'] as String,
      promoteAdsTitle: json['promoteAdsTitle'] as String,
      mainBanner: json['mainBanner'] as bool,
      hotDeal: json['hotDeal'] as bool,
      avatar: json['avatar'] as String,
      fromDate: json['fromDate'] as String? ?? '',
      toDate: json['toDate'] as String? ?? '',
      businessID: json['businessID'] as String,
      businessEnglishName: json['businessEnglishName'] as String,
      businessArabicName: json['businessArabicName'] as String,
      businessGender: json['businessGender'] as String,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      businessAddressEN: json['businessAddressEN'] as String,
      businessAddressAR: json['businessAddressAR'] as String,
      distance: json['distance'] as String,
      businessServiceID: json['businessServiceID'] as String,
      businessServiceEnglishName: json['businessServiceEnglishName'] as String,
      businessServiceArabicName: json['businessServiceArabicName'] as String,
      servicePrice: (json['servicePrice'] as num).toDouble(),
      serviceID: json['serviceID'] as String,
      serviceEnglishName: json['serviceEnglishName'] as String,
      serviceArabicName: json['serviceArabicName'] as String,
      serviceAvatar: json['serviceAvatar'] as String,
      isPercentage: json['isPercentage'] as bool? ?? false,
      discountPercentage: json['discountPercentage'] as num? ?? 0,
      discount: json['discount'] as num? ?? 0,
      currencyEnglishName: json['currencyEnglishName'] as String,
      currencyArabicName: json['currencyArabicName'] as String,
      serviceDuration: json['serviceDuration'] as int,
      serviceDurationText: json['serviceDurationText'] as String,
      businessRating: (json['businessRating'] as num?)?.toDouble() ?? 0,
      serviceRating: (json['serviceRating'] as num?)?.toDouble() ?? 0,
      discountPercentageValidDate:
          json['discountPercentageValidDate'] as String? ?? '',
      discountPercentageServicePrice:
          json['discountPercentageServicePrice'] as num? ?? 0,
      hasWishItem: json['hasWishItem'] as bool,
      hasActiveSlot: json['hasActiveSlot'] as bool,
    );

Map<String, dynamic> _$$_HotDealsItemModelToJson(
        _$_HotDealsItemModel instance) =>
    <String, dynamic>{
      'promoteAdsID': instance.promoteAdsID,
      'promoteAdsTitle': instance.promoteAdsTitle,
      'mainBanner': instance.mainBanner,
      'hotDeal': instance.hotDeal,
      'avatar': instance.avatar,
      'fromDate': instance.fromDate,
      'toDate': instance.toDate,
      'businessID': instance.businessID,
      'businessEnglishName': instance.businessEnglishName,
      'businessArabicName': instance.businessArabicName,
      'businessGender': instance.businessGender,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'businessAddressEN': instance.businessAddressEN,
      'businessAddressAR': instance.businessAddressAR,
      'distance': instance.distance,
      'businessServiceID': instance.businessServiceID,
      'businessServiceEnglishName': instance.businessServiceEnglishName,
      'businessServiceArabicName': instance.businessServiceArabicName,
      'servicePrice': instance.servicePrice,
      'serviceID': instance.serviceID,
      'serviceEnglishName': instance.serviceEnglishName,
      'serviceArabicName': instance.serviceArabicName,
      'serviceAvatar': instance.serviceAvatar,
      'isPercentage': instance.isPercentage,
      'discountPercentage': instance.discountPercentage,
      'discount': instance.discount,
      'currencyEnglishName': instance.currencyEnglishName,
      'currencyArabicName': instance.currencyArabicName,
      'serviceDuration': instance.serviceDuration,
      'serviceDurationText': instance.serviceDurationText,
      'businessRating': instance.businessRating,
      'serviceRating': instance.serviceRating,
      'discountPercentageValidDate': instance.discountPercentageValidDate,
      'discountPercentageServicePrice': instance.discountPercentageServicePrice,
      'hasWishItem': instance.hasWishItem,
      'hasActiveSlot': instance.hasActiveSlot,
    };
