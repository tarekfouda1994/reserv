import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'hot_deals_item_model.freezed.dart';
part 'hot_deals_item_model.g.dart';

@freezed
class HotDealsItemModel with _$HotDealsItemModel {
  HotDealsItemModel._();

  @JsonSerializable(explicitToJson: true)
  factory HotDealsItemModel({
    required String promoteAdsID,
    required String promoteAdsTitle,
    required bool mainBanner,
    required bool hotDeal,
    required String avatar,
    @JsonKey(defaultValue: "", nullable: true) required String fromDate,
    @JsonKey(defaultValue: "", nullable: true) required String toDate,
    required String businessID,
    required String businessEnglishName,
    required String businessArabicName,
    required String businessGender,
    required double latitude,
    required double longitude,
    required String businessAddressEN,
    required String businessAddressAR,
    required String distance,
    required String businessServiceID,
    required String businessServiceEnglishName,
    required String businessServiceArabicName,
    required double servicePrice,
    required String serviceID,
    required String serviceEnglishName,
    required String serviceArabicName,
    required String serviceAvatar,
    @JsonKey(defaultValue: false, nullable: true) required bool isPercentage,
    @JsonKey(defaultValue: 0, nullable: true) required num discountPercentage,
    @JsonKey(defaultValue: 0, nullable: true) required num discount,
    required String currencyEnglishName,
    required String currencyArabicName,
    required int serviceDuration,
    required String serviceDurationText,
    @JsonKey(defaultValue: 0, nullable: true) required double businessRating,
    @JsonKey(defaultValue: 0, nullable: true) required double serviceRating,
    @JsonKey(defaultValue: "", nullable: true) required String discountPercentageValidDate,
    @JsonKey(defaultValue: 0, nullable: true) required num discountPercentageServicePrice,
    required bool hasWishItem,
    required bool hasActiveSlot,
  }) = _HotDealsItemModel;

  factory HotDealsItemModel.fromJson(Map<String, dynamic> json) =>
      _$HotDealsItemModelFromJson(json);

  String getPromoteAdsTitle() {
    if (promoteAdsTitle.contains("|")) {
      var idx = promoteAdsTitle.split("|");
      return getIt<Utilities>().getLocalizedValue(idx.first, idx.last);
    }
    return promoteAdsTitle;
  }

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(serviceArabicName, serviceEnglishName);
  }
}
