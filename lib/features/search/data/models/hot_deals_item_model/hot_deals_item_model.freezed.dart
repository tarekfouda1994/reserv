// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'hot_deals_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

HotDealsItemModel _$HotDealsItemModelFromJson(Map<String, dynamic> json) {
  return _HotDealsItemModel.fromJson(json);
}

/// @nodoc
mixin _$HotDealsItemModel {
  String get promoteAdsID => throw _privateConstructorUsedError;
  String get promoteAdsTitle => throw _privateConstructorUsedError;
  bool get mainBanner => throw _privateConstructorUsedError;
  bool get hotDeal => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get fromDate => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get toDate => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  String get businessEnglishName => throw _privateConstructorUsedError;
  String get businessArabicName => throw _privateConstructorUsedError;
  String get businessGender => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  String get businessAddressEN => throw _privateConstructorUsedError;
  String get businessAddressAR => throw _privateConstructorUsedError;
  String get distance => throw _privateConstructorUsedError;
  String get businessServiceID => throw _privateConstructorUsedError;
  String get businessServiceEnglishName => throw _privateConstructorUsedError;
  String get businessServiceArabicName => throw _privateConstructorUsedError;
  double get servicePrice => throw _privateConstructorUsedError;
  String get serviceID => throw _privateConstructorUsedError;
  String get serviceEnglishName => throw _privateConstructorUsedError;
  String get serviceArabicName => throw _privateConstructorUsedError;
  String get serviceAvatar => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  bool get isPercentage => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  num get discountPercentage => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  num get discount => throw _privateConstructorUsedError;
  String get currencyEnglishName => throw _privateConstructorUsedError;
  String get currencyArabicName => throw _privateConstructorUsedError;
  int get serviceDuration => throw _privateConstructorUsedError;
  String get serviceDurationText => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  double get businessRating => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  double get serviceRating => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get discountPercentageValidDate => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  num get discountPercentageServicePrice => throw _privateConstructorUsedError;
  bool get hasWishItem => throw _privateConstructorUsedError;
  bool get hasActiveSlot => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HotDealsItemModelCopyWith<HotDealsItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HotDealsItemModelCopyWith<$Res> {
  factory $HotDealsItemModelCopyWith(
          HotDealsItemModel value, $Res Function(HotDealsItemModel) then) =
      _$HotDealsItemModelCopyWithImpl<$Res>;
  $Res call(
      {String promoteAdsID,
      String promoteAdsTitle,
      bool mainBanner,
      bool hotDeal,
      String avatar,
      @JsonKey(defaultValue: "", nullable: true)
          String fromDate,
      @JsonKey(defaultValue: "", nullable: true)
          String toDate,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      String businessGender,
      double latitude,
      double longitude,
      String businessAddressEN,
      String businessAddressAR,
      String distance,
      String businessServiceID,
      String businessServiceEnglishName,
      String businessServiceArabicName,
      double servicePrice,
      String serviceID,
      String serviceEnglishName,
      String serviceArabicName,
      String serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          bool isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          num discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          num discount,
      String currencyEnglishName,
      String currencyArabicName,
      int serviceDuration,
      String serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true)
          double businessRating,
      @JsonKey(defaultValue: 0, nullable: true)
          double serviceRating,
      @JsonKey(defaultValue: "", nullable: true)
          String discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          num discountPercentageServicePrice,
      bool hasWishItem,
      bool hasActiveSlot});
}

/// @nodoc
class _$HotDealsItemModelCopyWithImpl<$Res>
    implements $HotDealsItemModelCopyWith<$Res> {
  _$HotDealsItemModelCopyWithImpl(this._value, this._then);

  final HotDealsItemModel _value;
  // ignore: unused_field
  final $Res Function(HotDealsItemModel) _then;

  @override
  $Res call({
    Object? promoteAdsID = freezed,
    Object? promoteAdsTitle = freezed,
    Object? mainBanner = freezed,
    Object? hotDeal = freezed,
    Object? avatar = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? distance = freezed,
    Object? businessServiceID = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? businessServiceArabicName = freezed,
    Object? servicePrice = freezed,
    Object? serviceID = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? serviceAvatar = freezed,
    Object? isPercentage = freezed,
    Object? discountPercentage = freezed,
    Object? discount = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? serviceDuration = freezed,
    Object? serviceDurationText = freezed,
    Object? businessRating = freezed,
    Object? serviceRating = freezed,
    Object? discountPercentageValidDate = freezed,
    Object? discountPercentageServicePrice = freezed,
    Object? hasWishItem = freezed,
    Object? hasActiveSlot = freezed,
  }) {
    return _then(_value.copyWith(
      promoteAdsID: promoteAdsID == freezed
          ? _value.promoteAdsID
          : promoteAdsID // ignore: cast_nullable_to_non_nullable
              as String,
      promoteAdsTitle: promoteAdsTitle == freezed
          ? _value.promoteAdsTitle
          : promoteAdsTitle // ignore: cast_nullable_to_non_nullable
              as String,
      mainBanner: mainBanner == freezed
          ? _value.mainBanner
          : mainBanner // ignore: cast_nullable_to_non_nullable
              as bool,
      hotDeal: hotDeal == freezed
          ? _value.hotDeal
          : hotDeal // ignore: cast_nullable_to_non_nullable
              as bool,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: fromDate == freezed
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: toDate == freezed
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      distance: distance == freezed
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      servicePrice: servicePrice == freezed
          ? _value.servicePrice
          : servicePrice // ignore: cast_nullable_to_non_nullable
              as double,
      serviceID: serviceID == freezed
          ? _value.serviceID
          : serviceID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAvatar: serviceAvatar == freezed
          ? _value.serviceAvatar
          : serviceAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      isPercentage: isPercentage == freezed
          ? _value.isPercentage
          : isPercentage // ignore: cast_nullable_to_non_nullable
              as bool,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as num,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as num,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      businessRating: businessRating == freezed
          ? _value.businessRating
          : businessRating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceRating: serviceRating == freezed
          ? _value.serviceRating
          : serviceRating // ignore: cast_nullable_to_non_nullable
              as double,
      discountPercentageValidDate: discountPercentageValidDate == freezed
          ? _value.discountPercentageValidDate
          : discountPercentageValidDate // ignore: cast_nullable_to_non_nullable
              as String,
      discountPercentageServicePrice: discountPercentageServicePrice == freezed
          ? _value.discountPercentageServicePrice
          : discountPercentageServicePrice // ignore: cast_nullable_to_non_nullable
              as num,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_HotDealsItemModelCopyWith<$Res>
    implements $HotDealsItemModelCopyWith<$Res> {
  factory _$$_HotDealsItemModelCopyWith(_$_HotDealsItemModel value,
          $Res Function(_$_HotDealsItemModel) then) =
      __$$_HotDealsItemModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String promoteAdsID,
      String promoteAdsTitle,
      bool mainBanner,
      bool hotDeal,
      String avatar,
      @JsonKey(defaultValue: "", nullable: true)
          String fromDate,
      @JsonKey(defaultValue: "", nullable: true)
          String toDate,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      String businessGender,
      double latitude,
      double longitude,
      String businessAddressEN,
      String businessAddressAR,
      String distance,
      String businessServiceID,
      String businessServiceEnglishName,
      String businessServiceArabicName,
      double servicePrice,
      String serviceID,
      String serviceEnglishName,
      String serviceArabicName,
      String serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          bool isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          num discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          num discount,
      String currencyEnglishName,
      String currencyArabicName,
      int serviceDuration,
      String serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true)
          double businessRating,
      @JsonKey(defaultValue: 0, nullable: true)
          double serviceRating,
      @JsonKey(defaultValue: "", nullable: true)
          String discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          num discountPercentageServicePrice,
      bool hasWishItem,
      bool hasActiveSlot});
}

/// @nodoc
class __$$_HotDealsItemModelCopyWithImpl<$Res>
    extends _$HotDealsItemModelCopyWithImpl<$Res>
    implements _$$_HotDealsItemModelCopyWith<$Res> {
  __$$_HotDealsItemModelCopyWithImpl(
      _$_HotDealsItemModel _value, $Res Function(_$_HotDealsItemModel) _then)
      : super(_value, (v) => _then(v as _$_HotDealsItemModel));

  @override
  _$_HotDealsItemModel get _value => super._value as _$_HotDealsItemModel;

  @override
  $Res call({
    Object? promoteAdsID = freezed,
    Object? promoteAdsTitle = freezed,
    Object? mainBanner = freezed,
    Object? hotDeal = freezed,
    Object? avatar = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? distance = freezed,
    Object? businessServiceID = freezed,
    Object? businessServiceEnglishName = freezed,
    Object? businessServiceArabicName = freezed,
    Object? servicePrice = freezed,
    Object? serviceID = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? serviceAvatar = freezed,
    Object? isPercentage = freezed,
    Object? discountPercentage = freezed,
    Object? discount = freezed,
    Object? currencyEnglishName = freezed,
    Object? currencyArabicName = freezed,
    Object? serviceDuration = freezed,
    Object? serviceDurationText = freezed,
    Object? businessRating = freezed,
    Object? serviceRating = freezed,
    Object? discountPercentageValidDate = freezed,
    Object? discountPercentageServicePrice = freezed,
    Object? hasWishItem = freezed,
    Object? hasActiveSlot = freezed,
  }) {
    return _then(_$_HotDealsItemModel(
      promoteAdsID: promoteAdsID == freezed
          ? _value.promoteAdsID
          : promoteAdsID // ignore: cast_nullable_to_non_nullable
              as String,
      promoteAdsTitle: promoteAdsTitle == freezed
          ? _value.promoteAdsTitle
          : promoteAdsTitle // ignore: cast_nullable_to_non_nullable
              as String,
      mainBanner: mainBanner == freezed
          ? _value.mainBanner
          : mainBanner // ignore: cast_nullable_to_non_nullable
              as bool,
      hotDeal: hotDeal == freezed
          ? _value.hotDeal
          : hotDeal // ignore: cast_nullable_to_non_nullable
              as bool,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: fromDate == freezed
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: toDate == freezed
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      distance: distance == freezed
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceEnglishName: businessServiceEnglishName == freezed
          ? _value.businessServiceEnglishName
          : businessServiceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceArabicName: businessServiceArabicName == freezed
          ? _value.businessServiceArabicName
          : businessServiceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      servicePrice: servicePrice == freezed
          ? _value.servicePrice
          : servicePrice // ignore: cast_nullable_to_non_nullable
              as double,
      serviceID: serviceID == freezed
          ? _value.serviceID
          : serviceID // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceAvatar: serviceAvatar == freezed
          ? _value.serviceAvatar
          : serviceAvatar // ignore: cast_nullable_to_non_nullable
              as String,
      isPercentage: isPercentage == freezed
          ? _value.isPercentage
          : isPercentage // ignore: cast_nullable_to_non_nullable
              as bool,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as num,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as num,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      businessRating: businessRating == freezed
          ? _value.businessRating
          : businessRating // ignore: cast_nullable_to_non_nullable
              as double,
      serviceRating: serviceRating == freezed
          ? _value.serviceRating
          : serviceRating // ignore: cast_nullable_to_non_nullable
              as double,
      discountPercentageValidDate: discountPercentageValidDate == freezed
          ? _value.discountPercentageValidDate
          : discountPercentageValidDate // ignore: cast_nullable_to_non_nullable
              as String,
      discountPercentageServicePrice: discountPercentageServicePrice == freezed
          ? _value.discountPercentageServicePrice
          : discountPercentageServicePrice // ignore: cast_nullable_to_non_nullable
              as num,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_HotDealsItemModel extends _HotDealsItemModel {
  _$_HotDealsItemModel(
      {required this.promoteAdsID,
      required this.promoteAdsTitle,
      required this.mainBanner,
      required this.hotDeal,
      required this.avatar,
      @JsonKey(defaultValue: "", nullable: true)
          required this.fromDate,
      @JsonKey(defaultValue: "", nullable: true)
          required this.toDate,
      required this.businessID,
      required this.businessEnglishName,
      required this.businessArabicName,
      required this.businessGender,
      required this.latitude,
      required this.longitude,
      required this.businessAddressEN,
      required this.businessAddressAR,
      required this.distance,
      required this.businessServiceID,
      required this.businessServiceEnglishName,
      required this.businessServiceArabicName,
      required this.servicePrice,
      required this.serviceID,
      required this.serviceEnglishName,
      required this.serviceArabicName,
      required this.serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          required this.isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.discount,
      required this.currencyEnglishName,
      required this.currencyArabicName,
      required this.serviceDuration,
      required this.serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.businessRating,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.serviceRating,
      @JsonKey(defaultValue: "", nullable: true)
          required this.discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.discountPercentageServicePrice,
      required this.hasWishItem,
      required this.hasActiveSlot})
      : super._();

  factory _$_HotDealsItemModel.fromJson(Map<String, dynamic> json) =>
      _$$_HotDealsItemModelFromJson(json);

  @override
  final String promoteAdsID;
  @override
  final String promoteAdsTitle;
  @override
  final bool mainBanner;
  @override
  final bool hotDeal;
  @override
  final String avatar;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String fromDate;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String toDate;
  @override
  final String businessID;
  @override
  final String businessEnglishName;
  @override
  final String businessArabicName;
  @override
  final String businessGender;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final String businessAddressEN;
  @override
  final String businessAddressAR;
  @override
  final String distance;
  @override
  final String businessServiceID;
  @override
  final String businessServiceEnglishName;
  @override
  final String businessServiceArabicName;
  @override
  final double servicePrice;
  @override
  final String serviceID;
  @override
  final String serviceEnglishName;
  @override
  final String serviceArabicName;
  @override
  final String serviceAvatar;
  @override
  @JsonKey(defaultValue: false, nullable: true)
  final bool isPercentage;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final num discountPercentage;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final num discount;
  @override
  final String currencyEnglishName;
  @override
  final String currencyArabicName;
  @override
  final int serviceDuration;
  @override
  final String serviceDurationText;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final double businessRating;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final double serviceRating;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String discountPercentageValidDate;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final num discountPercentageServicePrice;
  @override
  final bool hasWishItem;
  @override
  final bool hasActiveSlot;

  @override
  String toString() {
    return 'HotDealsItemModel(promoteAdsID: $promoteAdsID, promoteAdsTitle: $promoteAdsTitle, mainBanner: $mainBanner, hotDeal: $hotDeal, avatar: $avatar, fromDate: $fromDate, toDate: $toDate, businessID: $businessID, businessEnglishName: $businessEnglishName, businessArabicName: $businessArabicName, businessGender: $businessGender, latitude: $latitude, longitude: $longitude, businessAddressEN: $businessAddressEN, businessAddressAR: $businessAddressAR, distance: $distance, businessServiceID: $businessServiceID, businessServiceEnglishName: $businessServiceEnglishName, businessServiceArabicName: $businessServiceArabicName, servicePrice: $servicePrice, serviceID: $serviceID, serviceEnglishName: $serviceEnglishName, serviceArabicName: $serviceArabicName, serviceAvatar: $serviceAvatar, isPercentage: $isPercentage, discountPercentage: $discountPercentage, discount: $discount, currencyEnglishName: $currencyEnglishName, currencyArabicName: $currencyArabicName, serviceDuration: $serviceDuration, serviceDurationText: $serviceDurationText, businessRating: $businessRating, serviceRating: $serviceRating, discountPercentageValidDate: $discountPercentageValidDate, discountPercentageServicePrice: $discountPercentageServicePrice, hasWishItem: $hasWishItem, hasActiveSlot: $hasActiveSlot)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HotDealsItemModel &&
            const DeepCollectionEquality()
                .equals(other.promoteAdsID, promoteAdsID) &&
            const DeepCollectionEquality()
                .equals(other.promoteAdsTitle, promoteAdsTitle) &&
            const DeepCollectionEquality()
                .equals(other.mainBanner, mainBanner) &&
            const DeepCollectionEquality().equals(other.hotDeal, hotDeal) &&
            const DeepCollectionEquality().equals(other.avatar, avatar) &&
            const DeepCollectionEquality().equals(other.fromDate, fromDate) &&
            const DeepCollectionEquality().equals(other.toDate, toDate) &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.businessEnglishName, businessEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.businessArabicName, businessArabicName) &&
            const DeepCollectionEquality()
                .equals(other.businessGender, businessGender) &&
            const DeepCollectionEquality().equals(other.latitude, latitude) &&
            const DeepCollectionEquality().equals(other.longitude, longitude) &&
            const DeepCollectionEquality()
                .equals(other.businessAddressEN, businessAddressEN) &&
            const DeepCollectionEquality()
                .equals(other.businessAddressAR, businessAddressAR) &&
            const DeepCollectionEquality().equals(other.distance, distance) &&
            const DeepCollectionEquality()
                .equals(other.businessServiceID, businessServiceID) &&
            const DeepCollectionEquality().equals(
                other.businessServiceEnglishName, businessServiceEnglishName) &&
            const DeepCollectionEquality().equals(
                other.businessServiceArabicName, businessServiceArabicName) &&
            const DeepCollectionEquality()
                .equals(other.servicePrice, servicePrice) &&
            const DeepCollectionEquality().equals(other.serviceID, serviceID) &&
            const DeepCollectionEquality()
                .equals(other.serviceEnglishName, serviceEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.serviceArabicName, serviceArabicName) &&
            const DeepCollectionEquality()
                .equals(other.serviceAvatar, serviceAvatar) &&
            const DeepCollectionEquality()
                .equals(other.isPercentage, isPercentage) &&
            const DeepCollectionEquality()
                .equals(other.discountPercentage, discountPercentage) &&
            const DeepCollectionEquality().equals(other.discount, discount) &&
            const DeepCollectionEquality()
                .equals(other.currencyEnglishName, currencyEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.currencyArabicName, currencyArabicName) &&
            const DeepCollectionEquality()
                .equals(other.serviceDuration, serviceDuration) &&
            const DeepCollectionEquality()
                .equals(other.serviceDurationText, serviceDurationText) &&
            const DeepCollectionEquality()
                .equals(other.businessRating, businessRating) &&
            const DeepCollectionEquality()
                .equals(other.serviceRating, serviceRating) &&
            const DeepCollectionEquality().equals(
                other.discountPercentageValidDate,
                discountPercentageValidDate) &&
            const DeepCollectionEquality().equals(
                other.discountPercentageServicePrice,
                discountPercentageServicePrice) &&
            const DeepCollectionEquality()
                .equals(other.hasWishItem, hasWishItem) &&
            const DeepCollectionEquality()
                .equals(other.hasActiveSlot, hasActiveSlot));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(promoteAdsID),
        const DeepCollectionEquality().hash(promoteAdsTitle),
        const DeepCollectionEquality().hash(mainBanner),
        const DeepCollectionEquality().hash(hotDeal),
        const DeepCollectionEquality().hash(avatar),
        const DeepCollectionEquality().hash(fromDate),
        const DeepCollectionEquality().hash(toDate),
        const DeepCollectionEquality().hash(businessID),
        const DeepCollectionEquality().hash(businessEnglishName),
        const DeepCollectionEquality().hash(businessArabicName),
        const DeepCollectionEquality().hash(businessGender),
        const DeepCollectionEquality().hash(latitude),
        const DeepCollectionEquality().hash(longitude),
        const DeepCollectionEquality().hash(businessAddressEN),
        const DeepCollectionEquality().hash(businessAddressAR),
        const DeepCollectionEquality().hash(distance),
        const DeepCollectionEquality().hash(businessServiceID),
        const DeepCollectionEquality().hash(businessServiceEnglishName),
        const DeepCollectionEquality().hash(businessServiceArabicName),
        const DeepCollectionEquality().hash(servicePrice),
        const DeepCollectionEquality().hash(serviceID),
        const DeepCollectionEquality().hash(serviceEnglishName),
        const DeepCollectionEquality().hash(serviceArabicName),
        const DeepCollectionEquality().hash(serviceAvatar),
        const DeepCollectionEquality().hash(isPercentage),
        const DeepCollectionEquality().hash(discountPercentage),
        const DeepCollectionEquality().hash(discount),
        const DeepCollectionEquality().hash(currencyEnglishName),
        const DeepCollectionEquality().hash(currencyArabicName),
        const DeepCollectionEquality().hash(serviceDuration),
        const DeepCollectionEquality().hash(serviceDurationText),
        const DeepCollectionEquality().hash(businessRating),
        const DeepCollectionEquality().hash(serviceRating),
        const DeepCollectionEquality().hash(discountPercentageValidDate),
        const DeepCollectionEquality().hash(discountPercentageServicePrice),
        const DeepCollectionEquality().hash(hasWishItem),
        const DeepCollectionEquality().hash(hasActiveSlot)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_HotDealsItemModelCopyWith<_$_HotDealsItemModel> get copyWith =>
      __$$_HotDealsItemModelCopyWithImpl<_$_HotDealsItemModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_HotDealsItemModelToJson(
      this,
    );
  }
}

abstract class _HotDealsItemModel extends HotDealsItemModel {
  factory _HotDealsItemModel(
      {required final String promoteAdsID,
      required final String promoteAdsTitle,
      required final bool mainBanner,
      required final bool hotDeal,
      required final String avatar,
      @JsonKey(defaultValue: "", nullable: true)
          required final String fromDate,
      @JsonKey(defaultValue: "", nullable: true)
          required final String toDate,
      required final String businessID,
      required final String businessEnglishName,
      required final String businessArabicName,
      required final String businessGender,
      required final double latitude,
      required final double longitude,
      required final String businessAddressEN,
      required final String businessAddressAR,
      required final String distance,
      required final String businessServiceID,
      required final String businessServiceEnglishName,
      required final String businessServiceArabicName,
      required final double servicePrice,
      required final String serviceID,
      required final String serviceEnglishName,
      required final String serviceArabicName,
      required final String serviceAvatar,
      @JsonKey(defaultValue: false, nullable: true)
          required final bool isPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required final num discountPercentage,
      @JsonKey(defaultValue: 0, nullable: true)
          required final num discount,
      required final String currencyEnglishName,
      required final String currencyArabicName,
      required final int serviceDuration,
      required final String serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true)
          required final double businessRating,
      @JsonKey(defaultValue: 0, nullable: true)
          required final double serviceRating,
      @JsonKey(defaultValue: "", nullable: true)
          required final String discountPercentageValidDate,
      @JsonKey(defaultValue: 0, nullable: true)
          required final num discountPercentageServicePrice,
      required final bool hasWishItem,
      required final bool hasActiveSlot}) = _$_HotDealsItemModel;
  _HotDealsItemModel._() : super._();

  factory _HotDealsItemModel.fromJson(Map<String, dynamic> json) =
      _$_HotDealsItemModel.fromJson;

  @override
  String get promoteAdsID;
  @override
  String get promoteAdsTitle;
  @override
  bool get mainBanner;
  @override
  bool get hotDeal;
  @override
  String get avatar;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get fromDate;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get toDate;
  @override
  String get businessID;
  @override
  String get businessEnglishName;
  @override
  String get businessArabicName;
  @override
  String get businessGender;
  @override
  double get latitude;
  @override
  double get longitude;
  @override
  String get businessAddressEN;
  @override
  String get businessAddressAR;
  @override
  String get distance;
  @override
  String get businessServiceID;
  @override
  String get businessServiceEnglishName;
  @override
  String get businessServiceArabicName;
  @override
  double get servicePrice;
  @override
  String get serviceID;
  @override
  String get serviceEnglishName;
  @override
  String get serviceArabicName;
  @override
  String get serviceAvatar;
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool get isPercentage;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  num get discountPercentage;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  num get discount;
  @override
  String get currencyEnglishName;
  @override
  String get currencyArabicName;
  @override
  int get serviceDuration;
  @override
  String get serviceDurationText;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double get businessRating;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double get serviceRating;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get discountPercentageValidDate;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  num get discountPercentageServicePrice;
  @override
  bool get hasWishItem;
  @override
  bool get hasActiveSlot;
  @override
  @JsonKey(ignore: true)
  _$$_HotDealsItemModelCopyWith<_$_HotDealsItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}
