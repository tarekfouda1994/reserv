// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'top_business_item_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TopBusinessItemModel _$TopBusinessItemModelFromJson(Map<String, dynamic> json) {
  return _TopBusinessItemModel.fromJson(json);
}

/// @nodoc
mixin _$TopBusinessItemModel {
  @JsonKey(defaultValue: "", nullable: true)
  String get id => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set id(String value) => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  set isActive(bool value) => throw _privateConstructorUsedError;
  String get englishName => throw _privateConstructorUsedError;
  set englishName(String value) => throw _privateConstructorUsedError;
  String get arabicName => throw _privateConstructorUsedError;
  set arabicName(String value) => throw _privateConstructorUsedError;
  int get businessStatusId => throw _privateConstructorUsedError;
  set businessStatusId(int value) => throw _privateConstructorUsedError;
  String? get businessCategoryIds => throw _privateConstructorUsedError;
  set businessCategoryIds(String? value) => throw _privateConstructorUsedError;
  String? get businessCategoryArabicName => throw _privateConstructorUsedError;
  set businessCategoryArabicName(String? value) =>
      throw _privateConstructorUsedError;
  String? get businessCategoryEnglishName => throw _privateConstructorUsedError;
  set businessCategoryEnglishName(String? value) =>
      throw _privateConstructorUsedError;
  String get businessAddressAR => throw _privateConstructorUsedError;
  set businessAddressAR(String value) => throw _privateConstructorUsedError;
  String get businessAddressEN => throw _privateConstructorUsedError;
  set businessAddressEN(String value) => throw _privateConstructorUsedError;
  String get location => throw _privateConstructorUsedError;
  set location(String value) => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  set longitude(double value) => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  set latitude(double value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get businesssImage => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set businesssImage(String value) => throw _privateConstructorUsedError;
  String get userId => throw _privateConstructorUsedError;
  set userId(String value) => throw _privateConstructorUsedError;
  String get dateCreated => throw _privateConstructorUsedError;
  set dateCreated(String value) => throw _privateConstructorUsedError;
  int get areaId => throw _privateConstructorUsedError;
  set areaId(int value) => throw _privateConstructorUsedError;
  int get numberOfService => throw _privateConstructorUsedError;
  set numberOfService(int value) => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;
  set rating(double value) => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  String get distanceText => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  set distanceText(String value) => throw _privateConstructorUsedError;
  bool get hasWishItem => throw _privateConstructorUsedError;
  set hasWishItem(bool value) => throw _privateConstructorUsedError;
  bool get hasActiveSlot => throw _privateConstructorUsedError;
  set hasActiveSlot(bool value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  int get packageId => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  set packageId(int value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TopBusinessItemModelCopyWith<TopBusinessItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TopBusinessItemModelCopyWith<$Res> {
  factory $TopBusinessItemModelCopyWith(TopBusinessItemModel value,
          $Res Function(TopBusinessItemModel) then) =
      _$TopBusinessItemModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: "", nullable: true) String id,
      bool isActive,
      String englishName,
      String arabicName,
      int businessStatusId,
      String? businessCategoryIds,
      String? businessCategoryArabicName,
      String? businessCategoryEnglishName,
      String businessAddressAR,
      String businessAddressEN,
      String location,
      double longitude,
      double latitude,
      @JsonKey(nullable: true, defaultValue: "") String businesssImage,
      String userId,
      String dateCreated,
      int areaId,
      int numberOfService,
      double rating,
      @JsonKey(name: "distance") String distanceText,
      bool hasWishItem,
      bool hasActiveSlot,
      @JsonKey(nullable: true, defaultValue: 0) int packageId});
}

/// @nodoc
class _$TopBusinessItemModelCopyWithImpl<$Res>
    implements $TopBusinessItemModelCopyWith<$Res> {
  _$TopBusinessItemModelCopyWithImpl(this._value, this._then);

  final TopBusinessItemModel _value;
  // ignore: unused_field
  final $Res Function(TopBusinessItemModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? isActive = freezed,
    Object? englishName = freezed,
    Object? arabicName = freezed,
    Object? businessStatusId = freezed,
    Object? businessCategoryIds = freezed,
    Object? businessCategoryArabicName = freezed,
    Object? businessCategoryEnglishName = freezed,
    Object? businessAddressAR = freezed,
    Object? businessAddressEN = freezed,
    Object? location = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
    Object? businesssImage = freezed,
    Object? userId = freezed,
    Object? dateCreated = freezed,
    Object? areaId = freezed,
    Object? numberOfService = freezed,
    Object? rating = freezed,
    Object? distanceText = freezed,
    Object? hasWishItem = freezed,
    Object? hasActiveSlot = freezed,
    Object? packageId = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessStatusId: businessStatusId == freezed
          ? _value.businessStatusId
          : businessStatusId // ignore: cast_nullable_to_non_nullable
              as int,
      businessCategoryIds: businessCategoryIds == freezed
          ? _value.businessCategoryIds
          : businessCategoryIds // ignore: cast_nullable_to_non_nullable
              as String?,
      businessCategoryArabicName: businessCategoryArabicName == freezed
          ? _value.businessCategoryArabicName
          : businessCategoryArabicName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessCategoryEnglishName: businessCategoryEnglishName == freezed
          ? _value.businessCategoryEnglishName
          : businessCategoryEnglishName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      businesssImage: businesssImage == freezed
          ? _value.businesssImage
          : businesssImage // ignore: cast_nullable_to_non_nullable
              as String,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      dateCreated: dateCreated == freezed
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as String,
      areaId: areaId == freezed
          ? _value.areaId
          : areaId // ignore: cast_nullable_to_non_nullable
              as int,
      numberOfService: numberOfService == freezed
          ? _value.numberOfService
          : numberOfService // ignore: cast_nullable_to_non_nullable
              as int,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
      packageId: packageId == freezed
          ? _value.packageId
          : packageId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_TopBusinessItemModelCopyWith<$Res>
    implements $TopBusinessItemModelCopyWith<$Res> {
  factory _$$_TopBusinessItemModelCopyWith(_$_TopBusinessItemModel value,
          $Res Function(_$_TopBusinessItemModel) then) =
      __$$_TopBusinessItemModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: "", nullable: true) String id,
      bool isActive,
      String englishName,
      String arabicName,
      int businessStatusId,
      String? businessCategoryIds,
      String? businessCategoryArabicName,
      String? businessCategoryEnglishName,
      String businessAddressAR,
      String businessAddressEN,
      String location,
      double longitude,
      double latitude,
      @JsonKey(nullable: true, defaultValue: "") String businesssImage,
      String userId,
      String dateCreated,
      int areaId,
      int numberOfService,
      double rating,
      @JsonKey(name: "distance") String distanceText,
      bool hasWishItem,
      bool hasActiveSlot,
      @JsonKey(nullable: true, defaultValue: 0) int packageId});
}

/// @nodoc
class __$$_TopBusinessItemModelCopyWithImpl<$Res>
    extends _$TopBusinessItemModelCopyWithImpl<$Res>
    implements _$$_TopBusinessItemModelCopyWith<$Res> {
  __$$_TopBusinessItemModelCopyWithImpl(_$_TopBusinessItemModel _value,
      $Res Function(_$_TopBusinessItemModel) _then)
      : super(_value, (v) => _then(v as _$_TopBusinessItemModel));

  @override
  _$_TopBusinessItemModel get _value => super._value as _$_TopBusinessItemModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? isActive = freezed,
    Object? englishName = freezed,
    Object? arabicName = freezed,
    Object? businessStatusId = freezed,
    Object? businessCategoryIds = freezed,
    Object? businessCategoryArabicName = freezed,
    Object? businessCategoryEnglishName = freezed,
    Object? businessAddressAR = freezed,
    Object? businessAddressEN = freezed,
    Object? location = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
    Object? businesssImage = freezed,
    Object? userId = freezed,
    Object? dateCreated = freezed,
    Object? areaId = freezed,
    Object? numberOfService = freezed,
    Object? rating = freezed,
    Object? distanceText = freezed,
    Object? hasWishItem = freezed,
    Object? hasActiveSlot = freezed,
    Object? packageId = freezed,
  }) {
    return _then(_$_TopBusinessItemModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessStatusId: businessStatusId == freezed
          ? _value.businessStatusId
          : businessStatusId // ignore: cast_nullable_to_non_nullable
              as int,
      businessCategoryIds: businessCategoryIds == freezed
          ? _value.businessCategoryIds
          : businessCategoryIds // ignore: cast_nullable_to_non_nullable
              as String?,
      businessCategoryArabicName: businessCategoryArabicName == freezed
          ? _value.businessCategoryArabicName
          : businessCategoryArabicName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessCategoryEnglishName: businessCategoryEnglishName == freezed
          ? _value.businessCategoryEnglishName
          : businessCategoryEnglishName // ignore: cast_nullable_to_non_nullable
              as String?,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      businesssImage: businesssImage == freezed
          ? _value.businesssImage
          : businesssImage // ignore: cast_nullable_to_non_nullable
              as String,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      dateCreated: dateCreated == freezed
          ? _value.dateCreated
          : dateCreated // ignore: cast_nullable_to_non_nullable
              as String,
      areaId: areaId == freezed
          ? _value.areaId
          : areaId // ignore: cast_nullable_to_non_nullable
              as int,
      numberOfService: numberOfService == freezed
          ? _value.numberOfService
          : numberOfService // ignore: cast_nullable_to_non_nullable
              as int,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      hasActiveSlot: hasActiveSlot == freezed
          ? _value.hasActiveSlot
          : hasActiveSlot // ignore: cast_nullable_to_non_nullable
              as bool,
      packageId: packageId == freezed
          ? _value.packageId
          : packageId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_TopBusinessItemModel extends _TopBusinessItemModel {
  _$_TopBusinessItemModel(
      {@JsonKey(defaultValue: "", nullable: true) required this.id,
      required this.isActive,
      required this.englishName,
      required this.arabicName,
      required this.businessStatusId,
      required this.businessCategoryIds,
      required this.businessCategoryArabicName,
      required this.businessCategoryEnglishName,
      required this.businessAddressAR,
      required this.businessAddressEN,
      required this.location,
      required this.longitude,
      required this.latitude,
      @JsonKey(nullable: true, defaultValue: "") required this.businesssImage,
      required this.userId,
      required this.dateCreated,
      required this.areaId,
      required this.numberOfService,
      required this.rating,
      @JsonKey(name: "distance") required this.distanceText,
      required this.hasWishItem,
      required this.hasActiveSlot,
      @JsonKey(nullable: true, defaultValue: 0) required this.packageId})
      : super._();

  factory _$_TopBusinessItemModel.fromJson(Map<String, dynamic> json) =>
      _$$_TopBusinessItemModelFromJson(json);

  @override
  @JsonKey(defaultValue: "", nullable: true)
  String id;
  @override
  bool isActive;
  @override
  String englishName;
  @override
  String arabicName;
  @override
  int businessStatusId;
  @override
  String? businessCategoryIds;
  @override
  String? businessCategoryArabicName;
  @override
  String? businessCategoryEnglishName;
  @override
  String businessAddressAR;
  @override
  String businessAddressEN;
  @override
  String location;
  @override
  double longitude;
  @override
  double latitude;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String businesssImage;
  @override
  String userId;
  @override
  String dateCreated;
  @override
  int areaId;
  @override
  int numberOfService;
  @override
  double rating;
  @override
  @JsonKey(name: "distance")
  String distanceText;
  @override
  bool hasWishItem;
  @override
  bool hasActiveSlot;
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  int packageId;

  @override
  String toString() {
    return 'TopBusinessItemModel(id: $id, isActive: $isActive, englishName: $englishName, arabicName: $arabicName, businessStatusId: $businessStatusId, businessCategoryIds: $businessCategoryIds, businessCategoryArabicName: $businessCategoryArabicName, businessCategoryEnglishName: $businessCategoryEnglishName, businessAddressAR: $businessAddressAR, businessAddressEN: $businessAddressEN, location: $location, longitude: $longitude, latitude: $latitude, businesssImage: $businesssImage, userId: $userId, dateCreated: $dateCreated, areaId: $areaId, numberOfService: $numberOfService, rating: $rating, distanceText: $distanceText, hasWishItem: $hasWishItem, hasActiveSlot: $hasActiveSlot, packageId: $packageId)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_TopBusinessItemModelCopyWith<_$_TopBusinessItemModel> get copyWith =>
      __$$_TopBusinessItemModelCopyWithImpl<_$_TopBusinessItemModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TopBusinessItemModelToJson(
      this,
    );
  }
}

abstract class _TopBusinessItemModel extends TopBusinessItemModel {
  factory _TopBusinessItemModel(
      {@JsonKey(defaultValue: "", nullable: true)
          required String id,
      required bool isActive,
      required String englishName,
      required String arabicName,
      required int businessStatusId,
      required String? businessCategoryIds,
      required String? businessCategoryArabicName,
      required String? businessCategoryEnglishName,
      required String businessAddressAR,
      required String businessAddressEN,
      required String location,
      required double longitude,
      required double latitude,
      @JsonKey(nullable: true, defaultValue: "")
          required String businesssImage,
      required String userId,
      required String dateCreated,
      required int areaId,
      required int numberOfService,
      required double rating,
      @JsonKey(name: "distance")
          required String distanceText,
      required bool hasWishItem,
      required bool hasActiveSlot,
      @JsonKey(nullable: true, defaultValue: 0)
          required int packageId}) = _$_TopBusinessItemModel;
  _TopBusinessItemModel._() : super._();

  factory _TopBusinessItemModel.fromJson(Map<String, dynamic> json) =
      _$_TopBusinessItemModel.fromJson;

  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get id;
  @JsonKey(defaultValue: "", nullable: true)
  set id(String value);
  @override
  bool get isActive;
  set isActive(bool value);
  @override
  String get englishName;
  set englishName(String value);
  @override
  String get arabicName;
  set arabicName(String value);
  @override
  int get businessStatusId;
  set businessStatusId(int value);
  @override
  String? get businessCategoryIds;
  set businessCategoryIds(String? value);
  @override
  String? get businessCategoryArabicName;
  set businessCategoryArabicName(String? value);
  @override
  String? get businessCategoryEnglishName;
  set businessCategoryEnglishName(String? value);
  @override
  String get businessAddressAR;
  set businessAddressAR(String value);
  @override
  String get businessAddressEN;
  set businessAddressEN(String value);
  @override
  String get location;
  set location(String value);
  @override
  double get longitude;
  set longitude(double value);
  @override
  double get latitude;
  set latitude(double value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get businesssImage;
  @JsonKey(nullable: true, defaultValue: "")
  set businesssImage(String value);
  @override
  String get userId;
  set userId(String value);
  @override
  String get dateCreated;
  set dateCreated(String value);
  @override
  int get areaId;
  set areaId(int value);
  @override
  int get numberOfService;
  set numberOfService(int value);
  @override
  double get rating;
  set rating(double value);
  @override
  @JsonKey(name: "distance")
  String get distanceText;
  @JsonKey(name: "distance")
  set distanceText(String value);
  @override
  bool get hasWishItem;
  set hasWishItem(bool value);
  @override
  bool get hasActiveSlot;
  set hasActiveSlot(bool value);
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  int get packageId;
  @JsonKey(nullable: true, defaultValue: 0)
  set packageId(int value);
  @override
  @JsonKey(ignore: true)
  _$$_TopBusinessItemModelCopyWith<_$_TopBusinessItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}
