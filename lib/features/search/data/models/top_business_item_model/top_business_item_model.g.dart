// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'top_business_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TopBusinessItemModel _$$_TopBusinessItemModelFromJson(
        Map<String, dynamic> json) =>
    _$_TopBusinessItemModel(
      id: json['id'] as String? ?? '',
      isActive: json['isActive'] as bool,
      englishName: json['englishName'] as String,
      arabicName: json['arabicName'] as String,
      businessStatusId: json['businessStatusId'] as int,
      businessCategoryIds: json['businessCategoryIds'] as String?,
      businessCategoryArabicName: json['businessCategoryArabicName'] as String?,
      businessCategoryEnglishName:
          json['businessCategoryEnglishName'] as String?,
      businessAddressAR: json['businessAddressAR'] as String,
      businessAddressEN: json['businessAddressEN'] as String,
      location: json['location'] as String,
      longitude: (json['longitude'] as num).toDouble(),
      latitude: (json['latitude'] as num).toDouble(),
      businesssImage: json['businesssImage'] as String? ?? '',
      userId: json['userId'] as String,
      dateCreated: json['dateCreated'] as String,
      areaId: json['areaId'] as int,
      numberOfService: json['numberOfService'] as int,
      rating: (json['rating'] as num).toDouble(),
      distanceText: json['distance'] as String,
      hasWishItem: json['hasWishItem'] as bool,
      hasActiveSlot: json['hasActiveSlot'] as bool,
      packageId: json['packageId'] as int? ?? 0,
    );

Map<String, dynamic> _$$_TopBusinessItemModelToJson(
        _$_TopBusinessItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isActive': instance.isActive,
      'englishName': instance.englishName,
      'arabicName': instance.arabicName,
      'businessStatusId': instance.businessStatusId,
      'businessCategoryIds': instance.businessCategoryIds,
      'businessCategoryArabicName': instance.businessCategoryArabicName,
      'businessCategoryEnglishName': instance.businessCategoryEnglishName,
      'businessAddressAR': instance.businessAddressAR,
      'businessAddressEN': instance.businessAddressEN,
      'location': instance.location,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'businesssImage': instance.businesssImage,
      'userId': instance.userId,
      'dateCreated': instance.dateCreated,
      'areaId': instance.areaId,
      'numberOfService': instance.numberOfService,
      'rating': instance.rating,
      'distance': instance.distanceText,
      'hasWishItem': instance.hasWishItem,
      'hasActiveSlot': instance.hasActiveSlot,
      'packageId': instance.packageId,
    };
