import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'top_business_item_model.freezed.dart';
part 'top_business_item_model.g.dart';

@unfreezed
class TopBusinessItemModel with _$TopBusinessItemModel {
  TopBusinessItemModel._();

  @JsonSerializable(explicitToJson: true)
  factory TopBusinessItemModel({
    @JsonKey(defaultValue: "", nullable: true) required String id,
    required bool isActive,
    required String englishName,
    required String arabicName,
    required int businessStatusId,
    required String? businessCategoryIds,
    required String? businessCategoryArabicName,
    required String? businessCategoryEnglishName,
    required String businessAddressAR,
    required String businessAddressEN,
    required String location,
    required double longitude,
    required double latitude,
    @JsonKey(nullable: true, defaultValue: "") required String businesssImage,
    required String userId,
    required String dateCreated,
    required int areaId,
    required int numberOfService,
    required double rating,
    @JsonKey(name: "distance") required String distanceText,
    required bool hasWishItem,
    required bool hasActiveSlot,
    @JsonKey(nullable: true, defaultValue: 0) required int packageId,
  }) = _TopBusinessItemModel;

  factory TopBusinessItemModel.fromJson(Map<String, dynamic> json) =>
      _$TopBusinessItemModelFromJson(json);

  String getBusinessName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }

  String getCategoryName() {
    return getIt<Utilities>().getLocalizedValue(
        businessCategoryArabicName ?? "", businessCategoryEnglishName ?? "");
  }

  String getAddressName() {
    return getIt<Utilities>().getLocalizedValue(
      businessAddressAR,
      businessAddressEN,
    );
  }

  String _distance(BuildContext context) {
    BuildContext context = getIt<GlobalContext>().context();
    var distance = getIt<Utilities>()
        .convertNumToAr(context: context, value: distanceText.toString().split(" ").first, isDistance: true);
    if (distanceText == "" || distanceText == "0" || distanceText == "0.00") {
      return "";
    } else {
      return "$distance ${tr("km")}";
    }
  }

  String distance(BuildContext context) {
    if (_distance(context).isEmpty) {
      return "";
    } else {
      var device = context.read<DeviceCubit>().state.model;
      var isEn = device.locale == Locale('en', 'US');
      if (isEn) {
        return "${_distance(context)} ${tr("away")}";
      } else {
        return "${tr("away")} ${_distance(context)}";
      }
    }
  }
}
