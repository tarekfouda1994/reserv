// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'popular_search_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PopularSearchModel _$PopularSearchModelFromJson(Map<String, dynamic> json) {
  return _PopularSearchModel.fromJson(json);
}

/// @nodoc
mixin _$PopularSearchModel {
  @JsonKey(defaultValue: "", nullable: true)
  String get deviceId => throw _privateConstructorUsedError;
  String get entityId => throw _privateConstructorUsedError;
  String get entityNameEN => throw _privateConstructorUsedError;
  String get entityNameAR => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PopularSearchModelCopyWith<PopularSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PopularSearchModelCopyWith<$Res> {
  factory $PopularSearchModelCopyWith(
          PopularSearchModel value, $Res Function(PopularSearchModel) then) =
      _$PopularSearchModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: "", nullable: true) String deviceId,
      String entityId,
      String entityNameEN,
      String entityNameAR});
}

/// @nodoc
class _$PopularSearchModelCopyWithImpl<$Res>
    implements $PopularSearchModelCopyWith<$Res> {
  _$PopularSearchModelCopyWithImpl(this._value, this._then);

  final PopularSearchModel _value;
  // ignore: unused_field
  final $Res Function(PopularSearchModel) _then;

  @override
  $Res call({
    Object? deviceId = freezed,
    Object? entityId = freezed,
    Object? entityNameEN = freezed,
    Object? entityNameAR = freezed,
  }) {
    return _then(_value.copyWith(
      deviceId: deviceId == freezed
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as String,
      entityId: entityId == freezed
          ? _value.entityId
          : entityId // ignore: cast_nullable_to_non_nullable
              as String,
      entityNameEN: entityNameEN == freezed
          ? _value.entityNameEN
          : entityNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      entityNameAR: entityNameAR == freezed
          ? _value.entityNameAR
          : entityNameAR // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PopularSearchModelCopyWith<$Res>
    implements $PopularSearchModelCopyWith<$Res> {
  factory _$$_PopularSearchModelCopyWith(_$_PopularSearchModel value,
          $Res Function(_$_PopularSearchModel) then) =
      __$$_PopularSearchModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: "", nullable: true) String deviceId,
      String entityId,
      String entityNameEN,
      String entityNameAR});
}

/// @nodoc
class __$$_PopularSearchModelCopyWithImpl<$Res>
    extends _$PopularSearchModelCopyWithImpl<$Res>
    implements _$$_PopularSearchModelCopyWith<$Res> {
  __$$_PopularSearchModelCopyWithImpl(
      _$_PopularSearchModel _value, $Res Function(_$_PopularSearchModel) _then)
      : super(_value, (v) => _then(v as _$_PopularSearchModel));

  @override
  _$_PopularSearchModel get _value => super._value as _$_PopularSearchModel;

  @override
  $Res call({
    Object? deviceId = freezed,
    Object? entityId = freezed,
    Object? entityNameEN = freezed,
    Object? entityNameAR = freezed,
  }) {
    return _then(_$_PopularSearchModel(
      deviceId: deviceId == freezed
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as String,
      entityId: entityId == freezed
          ? _value.entityId
          : entityId // ignore: cast_nullable_to_non_nullable
              as String,
      entityNameEN: entityNameEN == freezed
          ? _value.entityNameEN
          : entityNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      entityNameAR: entityNameAR == freezed
          ? _value.entityNameAR
          : entityNameAR // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PopularSearchModel extends _PopularSearchModel {
  _$_PopularSearchModel(
      {@JsonKey(defaultValue: "", nullable: true) required this.deviceId,
      required this.entityId,
      required this.entityNameEN,
      required this.entityNameAR})
      : super._();

  factory _$_PopularSearchModel.fromJson(Map<String, dynamic> json) =>
      _$$_PopularSearchModelFromJson(json);

  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String deviceId;
  @override
  final String entityId;
  @override
  final String entityNameEN;
  @override
  final String entityNameAR;

  @override
  String toString() {
    return 'PopularSearchModel(deviceId: $deviceId, entityId: $entityId, entityNameEN: $entityNameEN, entityNameAR: $entityNameAR)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PopularSearchModel &&
            const DeepCollectionEquality().equals(other.deviceId, deviceId) &&
            const DeepCollectionEquality().equals(other.entityId, entityId) &&
            const DeepCollectionEquality()
                .equals(other.entityNameEN, entityNameEN) &&
            const DeepCollectionEquality()
                .equals(other.entityNameAR, entityNameAR));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(deviceId),
      const DeepCollectionEquality().hash(entityId),
      const DeepCollectionEquality().hash(entityNameEN),
      const DeepCollectionEquality().hash(entityNameAR));

  @JsonKey(ignore: true)
  @override
  _$$_PopularSearchModelCopyWith<_$_PopularSearchModel> get copyWith =>
      __$$_PopularSearchModelCopyWithImpl<_$_PopularSearchModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PopularSearchModelToJson(
      this,
    );
  }
}

abstract class _PopularSearchModel extends PopularSearchModel {
  factory _PopularSearchModel(
      {@JsonKey(defaultValue: "", nullable: true)
          required final String deviceId,
      required final String entityId,
      required final String entityNameEN,
      required final String entityNameAR}) = _$_PopularSearchModel;
  _PopularSearchModel._() : super._();

  factory _PopularSearchModel.fromJson(Map<String, dynamic> json) =
      _$_PopularSearchModel.fromJson;

  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get deviceId;
  @override
  String get entityId;
  @override
  String get entityNameEN;
  @override
  String get entityNameAR;
  @override
  @JsonKey(ignore: true)
  _$$_PopularSearchModelCopyWith<_$_PopularSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}
