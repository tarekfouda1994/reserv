// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_search_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PopularSearchModel _$$_PopularSearchModelFromJson(
        Map<String, dynamic> json) =>
    _$_PopularSearchModel(
      deviceId: json['deviceId'] as String? ?? '',
      entityId: json['entityId'] as String,
      entityNameEN: json['entityNameEN'] as String,
      entityNameAR: json['entityNameAR'] as String,
    );

Map<String, dynamic> _$$_PopularSearchModelToJson(
        _$_PopularSearchModel instance) =>
    <String, dynamic>{
      'deviceId': instance.deviceId,
      'entityId': instance.entityId,
      'entityNameEN': instance.entityNameEN,
      'entityNameAR': instance.entityNameAR,
    };
