import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'popular_search_model.freezed.dart';
part 'popular_search_model.g.dart';

@freezed
class PopularSearchModel with _$PopularSearchModel {
  PopularSearchModel._();

  @JsonSerializable(explicitToJson: true)
  factory PopularSearchModel({
    @JsonKey(defaultValue: "", nullable: true) required String deviceId,
    required String entityId,
    required String entityNameEN,
    required String entityNameAR,
  }) = _PopularSearchModel;

  factory PopularSearchModel.fromJson(Map<String, dynamic> json) =>
      _$PopularSearchModelFromJson(json);

  String getPopularSearchName() {
    return getIt<Utilities>().getLocalizedValue(entityNameAR, entityNameEN);
  }
}
