// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'banner_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BannerModel _$BannerModelFromJson(Map<String, dynamic> json) {
  return _BannerModel.fromJson(json);
}

/// @nodoc
mixin _$BannerModel {
  String get promoteAdsID => throw _privateConstructorUsedError;
  String get promoteAdsTitle => throw _privateConstructorUsedError;
  bool get mainBanner => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get toDate => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get fromDate => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  String get businessEnglishName => throw _privateConstructorUsedError;
  String get businessArabicName => throw _privateConstructorUsedError;
  String get businessGender => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get businessServiceIds => throw _privateConstructorUsedError;
  String get serviceIds => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BannerModelCopyWith<BannerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BannerModelCopyWith<$Res> {
  factory $BannerModelCopyWith(
          BannerModel value, $Res Function(BannerModel) then) =
      _$BannerModelCopyWithImpl<$Res>;
  $Res call(
      {String promoteAdsID,
      String promoteAdsTitle,
      bool mainBanner,
      String avatar,
      @JsonKey(defaultValue: "", nullable: true) String toDate,
      @JsonKey(defaultValue: "", nullable: true) String fromDate,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      String businessGender,
      double latitude,
      double longitude,
      @JsonKey(defaultValue: "", nullable: true) String businessServiceIds,
      String serviceIds});
}

/// @nodoc
class _$BannerModelCopyWithImpl<$Res> implements $BannerModelCopyWith<$Res> {
  _$BannerModelCopyWithImpl(this._value, this._then);

  final BannerModel _value;
  // ignore: unused_field
  final $Res Function(BannerModel) _then;

  @override
  $Res call({
    Object? promoteAdsID = freezed,
    Object? promoteAdsTitle = freezed,
    Object? mainBanner = freezed,
    Object? avatar = freezed,
    Object? toDate = freezed,
    Object? fromDate = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessServiceIds = freezed,
    Object? serviceIds = freezed,
  }) {
    return _then(_value.copyWith(
      promoteAdsID: promoteAdsID == freezed
          ? _value.promoteAdsID
          : promoteAdsID // ignore: cast_nullable_to_non_nullable
              as String,
      promoteAdsTitle: promoteAdsTitle == freezed
          ? _value.promoteAdsTitle
          : promoteAdsTitle // ignore: cast_nullable_to_non_nullable
              as String,
      mainBanner: mainBanner == freezed
          ? _value.mainBanner
          : mainBanner // ignore: cast_nullable_to_non_nullable
              as bool,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: toDate == freezed
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: fromDate == freezed
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessServiceIds: businessServiceIds == freezed
          ? _value.businessServiceIds
          : businessServiceIds // ignore: cast_nullable_to_non_nullable
              as String,
      serviceIds: serviceIds == freezed
          ? _value.serviceIds
          : serviceIds // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_BannerModelCopyWith<$Res>
    implements $BannerModelCopyWith<$Res> {
  factory _$$_BannerModelCopyWith(
          _$_BannerModel value, $Res Function(_$_BannerModel) then) =
      __$$_BannerModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String promoteAdsID,
      String promoteAdsTitle,
      bool mainBanner,
      String avatar,
      @JsonKey(defaultValue: "", nullable: true) String toDate,
      @JsonKey(defaultValue: "", nullable: true) String fromDate,
      String businessID,
      String businessEnglishName,
      String businessArabicName,
      String businessGender,
      double latitude,
      double longitude,
      @JsonKey(defaultValue: "", nullable: true) String businessServiceIds,
      String serviceIds});
}

/// @nodoc
class __$$_BannerModelCopyWithImpl<$Res> extends _$BannerModelCopyWithImpl<$Res>
    implements _$$_BannerModelCopyWith<$Res> {
  __$$_BannerModelCopyWithImpl(
      _$_BannerModel _value, $Res Function(_$_BannerModel) _then)
      : super(_value, (v) => _then(v as _$_BannerModel));

  @override
  _$_BannerModel get _value => super._value as _$_BannerModel;

  @override
  $Res call({
    Object? promoteAdsID = freezed,
    Object? promoteAdsTitle = freezed,
    Object? mainBanner = freezed,
    Object? avatar = freezed,
    Object? toDate = freezed,
    Object? fromDate = freezed,
    Object? businessID = freezed,
    Object? businessEnglishName = freezed,
    Object? businessArabicName = freezed,
    Object? businessGender = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? businessServiceIds = freezed,
    Object? serviceIds = freezed,
  }) {
    return _then(_$_BannerModel(
      promoteAdsID: promoteAdsID == freezed
          ? _value.promoteAdsID
          : promoteAdsID // ignore: cast_nullable_to_non_nullable
              as String,
      promoteAdsTitle: promoteAdsTitle == freezed
          ? _value.promoteAdsTitle
          : promoteAdsTitle // ignore: cast_nullable_to_non_nullable
              as String,
      mainBanner: mainBanner == freezed
          ? _value.mainBanner
          : mainBanner // ignore: cast_nullable_to_non_nullable
              as bool,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      toDate: toDate == freezed
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: fromDate == freezed
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessEnglishName: businessEnglishName == freezed
          ? _value.businessEnglishName
          : businessEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      businessArabicName: businessArabicName == freezed
          ? _value.businessArabicName
          : businessArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      businessServiceIds: businessServiceIds == freezed
          ? _value.businessServiceIds
          : businessServiceIds // ignore: cast_nullable_to_non_nullable
              as String,
      serviceIds: serviceIds == freezed
          ? _value.serviceIds
          : serviceIds // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BannerModel extends _BannerModel {
  _$_BannerModel(
      {required this.promoteAdsID,
      required this.promoteAdsTitle,
      required this.mainBanner,
      required this.avatar,
      @JsonKey(defaultValue: "", nullable: true)
          required this.toDate,
      @JsonKey(defaultValue: "", nullable: true)
          required this.fromDate,
      required this.businessID,
      required this.businessEnglishName,
      required this.businessArabicName,
      required this.businessGender,
      required this.latitude,
      required this.longitude,
      @JsonKey(defaultValue: "", nullable: true)
          required this.businessServiceIds,
      required this.serviceIds})
      : super._();

  factory _$_BannerModel.fromJson(Map<String, dynamic> json) =>
      _$$_BannerModelFromJson(json);

  @override
  final String promoteAdsID;
  @override
  final String promoteAdsTitle;
  @override
  final bool mainBanner;
  @override
  final String avatar;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String toDate;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String fromDate;
  @override
  final String businessID;
  @override
  final String businessEnglishName;
  @override
  final String businessArabicName;
  @override
  final String businessGender;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String businessServiceIds;
  @override
  final String serviceIds;

  @override
  String toString() {
    return 'BannerModel(promoteAdsID: $promoteAdsID, promoteAdsTitle: $promoteAdsTitle, mainBanner: $mainBanner, avatar: $avatar, toDate: $toDate, fromDate: $fromDate, businessID: $businessID, businessEnglishName: $businessEnglishName, businessArabicName: $businessArabicName, businessGender: $businessGender, latitude: $latitude, longitude: $longitude, businessServiceIds: $businessServiceIds, serviceIds: $serviceIds)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BannerModel &&
            const DeepCollectionEquality()
                .equals(other.promoteAdsID, promoteAdsID) &&
            const DeepCollectionEquality()
                .equals(other.promoteAdsTitle, promoteAdsTitle) &&
            const DeepCollectionEquality()
                .equals(other.mainBanner, mainBanner) &&
            const DeepCollectionEquality().equals(other.avatar, avatar) &&
            const DeepCollectionEquality().equals(other.toDate, toDate) &&
            const DeepCollectionEquality().equals(other.fromDate, fromDate) &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.businessEnglishName, businessEnglishName) &&
            const DeepCollectionEquality()
                .equals(other.businessArabicName, businessArabicName) &&
            const DeepCollectionEquality()
                .equals(other.businessGender, businessGender) &&
            const DeepCollectionEquality().equals(other.latitude, latitude) &&
            const DeepCollectionEquality().equals(other.longitude, longitude) &&
            const DeepCollectionEquality()
                .equals(other.businessServiceIds, businessServiceIds) &&
            const DeepCollectionEquality()
                .equals(other.serviceIds, serviceIds));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(promoteAdsID),
      const DeepCollectionEquality().hash(promoteAdsTitle),
      const DeepCollectionEquality().hash(mainBanner),
      const DeepCollectionEquality().hash(avatar),
      const DeepCollectionEquality().hash(toDate),
      const DeepCollectionEquality().hash(fromDate),
      const DeepCollectionEquality().hash(businessID),
      const DeepCollectionEquality().hash(businessEnglishName),
      const DeepCollectionEquality().hash(businessArabicName),
      const DeepCollectionEquality().hash(businessGender),
      const DeepCollectionEquality().hash(latitude),
      const DeepCollectionEquality().hash(longitude),
      const DeepCollectionEquality().hash(businessServiceIds),
      const DeepCollectionEquality().hash(serviceIds));

  @JsonKey(ignore: true)
  @override
  _$$_BannerModelCopyWith<_$_BannerModel> get copyWith =>
      __$$_BannerModelCopyWithImpl<_$_BannerModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BannerModelToJson(
      this,
    );
  }
}

abstract class _BannerModel extends BannerModel {
  factory _BannerModel(
      {required final String promoteAdsID,
      required final String promoteAdsTitle,
      required final bool mainBanner,
      required final String avatar,
      @JsonKey(defaultValue: "", nullable: true)
          required final String toDate,
      @JsonKey(defaultValue: "", nullable: true)
          required final String fromDate,
      required final String businessID,
      required final String businessEnglishName,
      required final String businessArabicName,
      required final String businessGender,
      required final double latitude,
      required final double longitude,
      @JsonKey(defaultValue: "", nullable: true)
          required final String businessServiceIds,
      required final String serviceIds}) = _$_BannerModel;
  _BannerModel._() : super._();

  factory _BannerModel.fromJson(Map<String, dynamic> json) =
      _$_BannerModel.fromJson;

  @override
  String get promoteAdsID;
  @override
  String get promoteAdsTitle;
  @override
  bool get mainBanner;
  @override
  String get avatar;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get toDate;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get fromDate;
  @override
  String get businessID;
  @override
  String get businessEnglishName;
  @override
  String get businessArabicName;
  @override
  String get businessGender;
  @override
  double get latitude;
  @override
  double get longitude;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get businessServiceIds;
  @override
  String get serviceIds;
  @override
  @JsonKey(ignore: true)
  _$$_BannerModelCopyWith<_$_BannerModel> get copyWith =>
      throw _privateConstructorUsedError;
}
