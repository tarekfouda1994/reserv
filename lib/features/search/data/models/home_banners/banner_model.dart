import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'banner_model.freezed.dart';
part 'banner_model.g.dart';

@freezed
class BannerModel with _$BannerModel {
  BannerModel._();

  @JsonSerializable(explicitToJson: true)
  factory BannerModel({
    required String promoteAdsID,
    required String promoteAdsTitle,
    required bool mainBanner,
    required String avatar,
    @JsonKey(defaultValue: "", nullable: true) required String toDate,
    @JsonKey(defaultValue: "", nullable: true) required String fromDate,
    required String businessID,
    required String businessEnglishName,
    required String businessArabicName,
    required String businessGender,
    required double latitude,
    required double longitude,
    @JsonKey(defaultValue: "", nullable: true) required String businessServiceIds,
    required String serviceIds,
  }) = _BannerModel;

  factory BannerModel.fromJson(Map<String, dynamic> json) => _$BannerModelFromJson(json);

  String getPromoteAdsTitle() {
    if (promoteAdsTitle.contains("|")) {
      var idx = promoteAdsTitle.split("|");
      return getIt<Utilities>().getLocalizedValue(idx.first, idx.last);
    }
    return promoteAdsTitle;
  }

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(businessArabicName, businessEnglishName);
  }
}
