// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BannerModel _$$_BannerModelFromJson(Map<String, dynamic> json) =>
    _$_BannerModel(
      promoteAdsID: json['promoteAdsID'] as String,
      promoteAdsTitle: json['promoteAdsTitle'] as String,
      mainBanner: json['mainBanner'] as bool,
      avatar: json['avatar'] as String,
      toDate: json['toDate'] as String? ?? '',
      fromDate: json['fromDate'] as String? ?? '',
      businessID: json['businessID'] as String,
      businessEnglishName: json['businessEnglishName'] as String,
      businessArabicName: json['businessArabicName'] as String,
      businessGender: json['businessGender'] as String,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      businessServiceIds: json['businessServiceIds'] as String? ?? '',
      serviceIds: json['serviceIds'] as String,
    );

Map<String, dynamic> _$$_BannerModelToJson(_$_BannerModel instance) =>
    <String, dynamic>{
      'promoteAdsID': instance.promoteAdsID,
      'promoteAdsTitle': instance.promoteAdsTitle,
      'mainBanner': instance.mainBanner,
      'avatar': instance.avatar,
      'toDate': instance.toDate,
      'fromDate': instance.fromDate,
      'businessID': instance.businessID,
      'businessEnglishName': instance.businessEnglishName,
      'businessArabicName': instance.businessArabicName,
      'businessGender': instance.businessGender,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'businessServiceIds': instance.businessServiceIds,
      'serviceIds': instance.serviceIds,
    };
