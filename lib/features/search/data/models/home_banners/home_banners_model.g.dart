// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_banners_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_HomeBannersModel _$$_HomeBannersModelFromJson(Map<String, dynamic> json) =>
    _$_HomeBannersModel(
      totalRecords: json['totalRecords'] as int,
      pageSize: json['pageSize'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map((e) => BannerModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_HomeBannersModelToJson(_$_HomeBannersModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'pageSize': instance.pageSize,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
