import 'package:flutter_tdd/features/search/data/models/home_banners/banner_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_banners_model.freezed.dart';
part 'home_banners_model.g.dart';

@freezed
class HomeBannersModel with _$HomeBannersModel {
  @JsonSerializable(explicitToJson: true)
  factory HomeBannersModel({
    required int totalRecords,
    required int pageSize,
    required List<BannerModel> itemsList,
  }) = _HomeBannersModel;

  factory HomeBannersModel.fromJson(Map<String, dynamic> json) => _$HomeBannersModelFromJson(json);
}
