// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_banners_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

HomeBannersModel _$HomeBannersModelFromJson(Map<String, dynamic> json) {
  return _HomeBannersModel.fromJson(json);
}

/// @nodoc
mixin _$HomeBannersModel {
  int get totalRecords => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  List<BannerModel> get itemsList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HomeBannersModelCopyWith<HomeBannersModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeBannersModelCopyWith<$Res> {
  factory $HomeBannersModelCopyWith(
          HomeBannersModel value, $Res Function(HomeBannersModel) then) =
      _$HomeBannersModelCopyWithImpl<$Res>;
  $Res call({int totalRecords, int pageSize, List<BannerModel> itemsList});
}

/// @nodoc
class _$HomeBannersModelCopyWithImpl<$Res>
    implements $HomeBannersModelCopyWith<$Res> {
  _$HomeBannersModelCopyWithImpl(this._value, this._then);

  final HomeBannersModel _value;
  // ignore: unused_field
  final $Res Function(HomeBannersModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<BannerModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_HomeBannersModelCopyWith<$Res>
    implements $HomeBannersModelCopyWith<$Res> {
  factory _$$_HomeBannersModelCopyWith(
          _$_HomeBannersModel value, $Res Function(_$_HomeBannersModel) then) =
      __$$_HomeBannersModelCopyWithImpl<$Res>;
  @override
  $Res call({int totalRecords, int pageSize, List<BannerModel> itemsList});
}

/// @nodoc
class __$$_HomeBannersModelCopyWithImpl<$Res>
    extends _$HomeBannersModelCopyWithImpl<$Res>
    implements _$$_HomeBannersModelCopyWith<$Res> {
  __$$_HomeBannersModelCopyWithImpl(
      _$_HomeBannersModel _value, $Res Function(_$_HomeBannersModel) _then)
      : super(_value, (v) => _then(v as _$_HomeBannersModel));

  @override
  _$_HomeBannersModel get _value => super._value as _$_HomeBannersModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_HomeBannersModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<BannerModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_HomeBannersModel implements _HomeBannersModel {
  _$_HomeBannersModel(
      {required this.totalRecords,
      required this.pageSize,
      required final List<BannerModel> itemsList})
      : _itemsList = itemsList;

  factory _$_HomeBannersModel.fromJson(Map<String, dynamic> json) =>
      _$$_HomeBannersModelFromJson(json);

  @override
  final int totalRecords;
  @override
  final int pageSize;
  final List<BannerModel> _itemsList;
  @override
  List<BannerModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'HomeBannersModel(totalRecords: $totalRecords, pageSize: $pageSize, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HomeBannersModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality().equals(other.pageSize, pageSize) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(pageSize),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_HomeBannersModelCopyWith<_$_HomeBannersModel> get copyWith =>
      __$$_HomeBannersModelCopyWithImpl<_$_HomeBannersModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_HomeBannersModelToJson(
      this,
    );
  }
}

abstract class _HomeBannersModel implements HomeBannersModel {
  factory _HomeBannersModel(
      {required final int totalRecords,
      required final int pageSize,
      required final List<BannerModel> itemsList}) = _$_HomeBannersModel;

  factory _HomeBannersModel.fromJson(Map<String, dynamic> json) =
      _$_HomeBannersModel.fromJson;

  @override
  int get totalRecords;
  @override
  int get pageSize;
  @override
  List<BannerModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_HomeBannersModelCopyWith<_$_HomeBannersModel> get copyWith =>
      throw _privateConstructorUsedError;
}
