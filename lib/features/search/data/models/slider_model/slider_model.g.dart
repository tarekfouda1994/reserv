// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'slider_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SliderModel _$$_SliderModelFromJson(Map<String, dynamic> json) =>
    _$_SliderModel(
      id: json['id'] as String,
      fileName: json['fileName'] as String,
      mimeType: json['mimeType'] as String,
      size: json['size'] as int,
      fileContent: json['fileContent'] as String,
      entityState: json['entityState'] as int,
      hasReadError: json['hasReadError'] as bool,
      isDefault: json['isDefault'] as bool,
      isLogo: json['isLogo'] as bool,
    );

Map<String, dynamic> _$$_SliderModelToJson(_$_SliderModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fileName': instance.fileName,
      'mimeType': instance.mimeType,
      'size': instance.size,
      'fileContent': instance.fileContent,
      'entityState': instance.entityState,
      'hasReadError': instance.hasReadError,
      'isDefault': instance.isDefault,
      'isLogo': instance.isLogo,
    };
