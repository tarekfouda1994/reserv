// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'slider_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SliderModel _$SliderModelFromJson(Map<String, dynamic> json) {
  return _SliderModel.fromJson(json);
}

/// @nodoc
mixin _$SliderModel {
  String get id => throw _privateConstructorUsedError;
  String get fileName => throw _privateConstructorUsedError;
  String get mimeType => throw _privateConstructorUsedError;
  int get size => throw _privateConstructorUsedError;
  String get fileContent => throw _privateConstructorUsedError;
  int get entityState => throw _privateConstructorUsedError;
  bool get hasReadError => throw _privateConstructorUsedError;
  bool get isDefault => throw _privateConstructorUsedError;
  bool get isLogo => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SliderModelCopyWith<SliderModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SliderModelCopyWith<$Res> {
  factory $SliderModelCopyWith(
          SliderModel value, $Res Function(SliderModel) then) =
      _$SliderModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String fileName,
      String mimeType,
      int size,
      String fileContent,
      int entityState,
      bool hasReadError,
      bool isDefault,
      bool isLogo});
}

/// @nodoc
class _$SliderModelCopyWithImpl<$Res> implements $SliderModelCopyWith<$Res> {
  _$SliderModelCopyWithImpl(this._value, this._then);

  final SliderModel _value;
  // ignore: unused_field
  final $Res Function(SliderModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? fileName = freezed,
    Object? mimeType = freezed,
    Object? size = freezed,
    Object? fileContent = freezed,
    Object? entityState = freezed,
    Object? hasReadError = freezed,
    Object? isDefault = freezed,
    Object? isLogo = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      fileName: fileName == freezed
          ? _value.fileName
          : fileName // ignore: cast_nullable_to_non_nullable
              as String,
      mimeType: mimeType == freezed
          ? _value.mimeType
          : mimeType // ignore: cast_nullable_to_non_nullable
              as String,
      size: size == freezed
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int,
      fileContent: fileContent == freezed
          ? _value.fileContent
          : fileContent // ignore: cast_nullable_to_non_nullable
              as String,
      entityState: entityState == freezed
          ? _value.entityState
          : entityState // ignore: cast_nullable_to_non_nullable
              as int,
      hasReadError: hasReadError == freezed
          ? _value.hasReadError
          : hasReadError // ignore: cast_nullable_to_non_nullable
              as bool,
      isDefault: isDefault == freezed
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
      isLogo: isLogo == freezed
          ? _value.isLogo
          : isLogo // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_SliderModelCopyWith<$Res>
    implements $SliderModelCopyWith<$Res> {
  factory _$$_SliderModelCopyWith(
          _$_SliderModel value, $Res Function(_$_SliderModel) then) =
      __$$_SliderModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String fileName,
      String mimeType,
      int size,
      String fileContent,
      int entityState,
      bool hasReadError,
      bool isDefault,
      bool isLogo});
}

/// @nodoc
class __$$_SliderModelCopyWithImpl<$Res> extends _$SliderModelCopyWithImpl<$Res>
    implements _$$_SliderModelCopyWith<$Res> {
  __$$_SliderModelCopyWithImpl(
      _$_SliderModel _value, $Res Function(_$_SliderModel) _then)
      : super(_value, (v) => _then(v as _$_SliderModel));

  @override
  _$_SliderModel get _value => super._value as _$_SliderModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? fileName = freezed,
    Object? mimeType = freezed,
    Object? size = freezed,
    Object? fileContent = freezed,
    Object? entityState = freezed,
    Object? hasReadError = freezed,
    Object? isDefault = freezed,
    Object? isLogo = freezed,
  }) {
    return _then(_$_SliderModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      fileName: fileName == freezed
          ? _value.fileName
          : fileName // ignore: cast_nullable_to_non_nullable
              as String,
      mimeType: mimeType == freezed
          ? _value.mimeType
          : mimeType // ignore: cast_nullable_to_non_nullable
              as String,
      size: size == freezed
          ? _value.size
          : size // ignore: cast_nullable_to_non_nullable
              as int,
      fileContent: fileContent == freezed
          ? _value.fileContent
          : fileContent // ignore: cast_nullable_to_non_nullable
              as String,
      entityState: entityState == freezed
          ? _value.entityState
          : entityState // ignore: cast_nullable_to_non_nullable
              as int,
      hasReadError: hasReadError == freezed
          ? _value.hasReadError
          : hasReadError // ignore: cast_nullable_to_non_nullable
              as bool,
      isDefault: isDefault == freezed
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
      isLogo: isLogo == freezed
          ? _value.isLogo
          : isLogo // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_SliderModel implements _SliderModel {
  _$_SliderModel(
      {required this.id,
      required this.fileName,
      required this.mimeType,
      required this.size,
      required this.fileContent,
      required this.entityState,
      required this.hasReadError,
      required this.isDefault,
      required this.isLogo});

  factory _$_SliderModel.fromJson(Map<String, dynamic> json) =>
      _$$_SliderModelFromJson(json);

  @override
  final String id;
  @override
  final String fileName;
  @override
  final String mimeType;
  @override
  final int size;
  @override
  final String fileContent;
  @override
  final int entityState;
  @override
  final bool hasReadError;
  @override
  final bool isDefault;
  @override
  final bool isLogo;

  @override
  String toString() {
    return 'SliderModel(id: $id, fileName: $fileName, mimeType: $mimeType, size: $size, fileContent: $fileContent, entityState: $entityState, hasReadError: $hasReadError, isDefault: $isDefault, isLogo: $isLogo)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SliderModel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.fileName, fileName) &&
            const DeepCollectionEquality().equals(other.mimeType, mimeType) &&
            const DeepCollectionEquality().equals(other.size, size) &&
            const DeepCollectionEquality()
                .equals(other.fileContent, fileContent) &&
            const DeepCollectionEquality()
                .equals(other.entityState, entityState) &&
            const DeepCollectionEquality()
                .equals(other.hasReadError, hasReadError) &&
            const DeepCollectionEquality().equals(other.isDefault, isDefault) &&
            const DeepCollectionEquality().equals(other.isLogo, isLogo));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(fileName),
      const DeepCollectionEquality().hash(mimeType),
      const DeepCollectionEquality().hash(size),
      const DeepCollectionEquality().hash(fileContent),
      const DeepCollectionEquality().hash(entityState),
      const DeepCollectionEquality().hash(hasReadError),
      const DeepCollectionEquality().hash(isDefault),
      const DeepCollectionEquality().hash(isLogo));

  @JsonKey(ignore: true)
  @override
  _$$_SliderModelCopyWith<_$_SliderModel> get copyWith =>
      __$$_SliderModelCopyWithImpl<_$_SliderModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SliderModelToJson(
      this,
    );
  }
}

abstract class _SliderModel implements SliderModel {
  factory _SliderModel(
      {required final String id,
      required final String fileName,
      required final String mimeType,
      required final int size,
      required final String fileContent,
      required final int entityState,
      required final bool hasReadError,
      required final bool isDefault,
      required final bool isLogo}) = _$_SliderModel;

  factory _SliderModel.fromJson(Map<String, dynamic> json) =
      _$_SliderModel.fromJson;

  @override
  String get id;
  @override
  String get fileName;
  @override
  String get mimeType;
  @override
  int get size;
  @override
  String get fileContent;
  @override
  int get entityState;
  @override
  bool get hasReadError;
  @override
  bool get isDefault;
  @override
  bool get isLogo;
  @override
  @JsonKey(ignore: true)
  _$$_SliderModelCopyWith<_$_SliderModel> get copyWith =>
      throw _privateConstructorUsedError;
}
