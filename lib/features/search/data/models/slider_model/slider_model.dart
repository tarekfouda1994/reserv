import 'package:freezed_annotation/freezed_annotation.dart';

part 'slider_model.freezed.dart';
part 'slider_model.g.dart';

@freezed
class SliderModel with _$SliderModel {
  @JsonSerializable(explicitToJson: true)
  factory SliderModel({
    required String id,
    required String fileName,
    required String mimeType,
    required int size,
    required String fileContent,
    required int entityState,
    required bool hasReadError,
    required bool isDefault,
    required bool isLogo,
  }) = _SliderModel;

  factory SliderModel.fromJson(Map<String, dynamic> json) => _$SliderModelFromJson(json);
}
