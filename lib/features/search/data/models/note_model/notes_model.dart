import 'package:freezed_annotation/freezed_annotation.dart';

part 'notes_model.freezed.dart';
part 'notes_model.g.dart';

@unfreezed
class NotesModel with _$NotesModel {
  @JsonSerializable(explicitToJson: true)
  factory NotesModel({
    required String id,
    required String note,
  }) = _NotesModel;

  factory NotesModel.fromJson(Map<String, dynamic> json) => _$NotesModelFromJson(json);
}
