// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notes_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NotesModel _$$_NotesModelFromJson(Map<String, dynamic> json) =>
    _$_NotesModel(
      id: json['id'] as String,
      note: json['note'] as String,
    );

Map<String, dynamic> _$$_NotesModelToJson(_$_NotesModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'note': instance.note,
    };
