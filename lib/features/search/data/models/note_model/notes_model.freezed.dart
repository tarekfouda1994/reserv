// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'notes_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

NotesModel _$NotesModelFromJson(Map<String, dynamic> json) {
  return _NotesModel.fromJson(json);
}

/// @nodoc
mixin _$NotesModel {
  String get id => throw _privateConstructorUsedError;
  set id(String value) => throw _privateConstructorUsedError;
  String get note => throw _privateConstructorUsedError;
  set note(String value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NotesModelCopyWith<NotesModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NotesModelCopyWith<$Res> {
  factory $NotesModelCopyWith(
          NotesModel value, $Res Function(NotesModel) then) =
      _$NotesModelCopyWithImpl<$Res>;
  $Res call({String id, String note});
}

/// @nodoc
class _$NotesModelCopyWithImpl<$Res> implements $NotesModelCopyWith<$Res> {
  _$NotesModelCopyWithImpl(this._value, this._then);

  final NotesModel _value;
  // ignore: unused_field
  final $Res Function(NotesModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? note = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      note: note == freezed
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_NotesModelCopyWith<$Res>
    implements $NotesModelCopyWith<$Res> {
  factory _$$_NotesModelCopyWith(
          _$_NotesModel value, $Res Function(_$_NotesModel) then) =
      __$$_NotesModelCopyWithImpl<$Res>;
  @override
  $Res call({String id, String note});
}

/// @nodoc
class __$$_NotesModelCopyWithImpl<$Res> extends _$NotesModelCopyWithImpl<$Res>
    implements _$$_NotesModelCopyWith<$Res> {
  __$$_NotesModelCopyWithImpl(
      _$_NotesModel _value, $Res Function(_$_NotesModel) _then)
      : super(_value, (v) => _then(v as _$_NotesModel));

  @override
  _$_NotesModel get _value => super._value as _$_NotesModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? note = freezed,
  }) {
    return _then(_$_NotesModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      note: note == freezed
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_NotesModel implements _NotesModel {
  _$_NotesModel({required this.id, required this.note});

  factory _$_NotesModel.fromJson(Map<String, dynamic> json) =>
      _$$_NotesModelFromJson(json);

  @override
  String id;
  @override
  String note;

  @override
  String toString() {
    return 'NotesModel(id: $id, note: $note)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_NotesModelCopyWith<_$_NotesModel> get copyWith =>
      __$$_NotesModelCopyWithImpl<_$_NotesModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_NotesModelToJson(
      this,
    );
  }
}

abstract class _NotesModel implements NotesModel {
  factory _NotesModel({required String id, required String note}) =
      _$_NotesModel;

  factory _NotesModel.fromJson(Map<String, dynamic> json) =
      _$_NotesModel.fromJson;

  @override
  String get id;
  set id(String value);
  @override
  String get note;
  set note(String value);
  @override
  @JsonKey(ignore: true)
  _$$_NotesModelCopyWith<_$_NotesModel> get copyWith =>
      throw _privateConstructorUsedError;
}
