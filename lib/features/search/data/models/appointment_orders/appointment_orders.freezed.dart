// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'appointment_orders.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppointmentOrdersModel _$AppointmentOrdersModelFromJson(
    Map<String, dynamic> json) {
  return _AppointmentOrdersModel.fromJson(json);
}

/// @nodoc
mixin _$AppointmentOrdersModel {
  String get bookingOrderID => throw _privateConstructorUsedError;
  int get appointmentType => throw _privateConstructorUsedError;
  String get orderNumber => throw _privateConstructorUsedError;
  @JsonKey(name: "nameEN")
  String get firstName => throw _privateConstructorUsedError;
  @JsonKey(name: "nameAR")
  String get lastName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get contactNumber => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  DateTime get bookingDate => throw _privateConstructorUsedError;
  String get bookingDateTime => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  String get businessNameEN => throw _privateConstructorUsedError;
  String get businessNameAR => throw _privateConstructorUsedError;
  String get businessAddressEN => throw _privateConstructorUsedError;
  String get businessAddressAR => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  int get statusId => throw _privateConstructorUsedError;
  String get statusNameEN => throw _privateConstructorUsedError;
  String get statusNameAR => throw _privateConstructorUsedError;
  double get feeDetail => throw _privateConstructorUsedError;
  String get businessContactNumber => throw _privateConstructorUsedError;
  DateTime get activeAppDateTime => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get businessImage => throw _privateConstructorUsedError;
  List<ServicesModel> get services => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppointmentOrdersModelCopyWith<AppointmentOrdersModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppointmentOrdersModelCopyWith<$Res> {
  factory $AppointmentOrdersModelCopyWith(AppointmentOrdersModel value,
          $Res Function(AppointmentOrdersModel) then) =
      _$AppointmentOrdersModelCopyWithImpl<$Res>;
  $Res call(
      {String bookingOrderID,
      int appointmentType,
      String orderNumber,
      @JsonKey(name: "nameEN") String firstName,
      @JsonKey(name: "nameAR") String lastName,
      @JsonKey(defaultValue: "", nullable: true) String contactNumber,
      String email,
      DateTime bookingDate,
      String bookingDateTime,
      String businessID,
      String businessNameEN,
      String businessNameAR,
      String businessAddressEN,
      String businessAddressAR,
      double latitude,
      double longitude,
      int statusId,
      String statusNameEN,
      String statusNameAR,
      double feeDetail,
      String businessContactNumber,
      DateTime activeAppDateTime,
      @JsonKey(defaultValue: "", nullable: true) String businessImage,
      List<ServicesModel> services});
}

/// @nodoc
class _$AppointmentOrdersModelCopyWithImpl<$Res>
    implements $AppointmentOrdersModelCopyWith<$Res> {
  _$AppointmentOrdersModelCopyWithImpl(this._value, this._then);

  final AppointmentOrdersModel _value;
  // ignore: unused_field
  final $Res Function(AppointmentOrdersModel) _then;

  @override
  $Res call({
    Object? bookingOrderID = freezed,
    Object? appointmentType = freezed,
    Object? orderNumber = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? contactNumber = freezed,
    Object? email = freezed,
    Object? bookingDate = freezed,
    Object? bookingDateTime = freezed,
    Object? businessID = freezed,
    Object? businessNameEN = freezed,
    Object? businessNameAR = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? statusId = freezed,
    Object? statusNameEN = freezed,
    Object? statusNameAR = freezed,
    Object? feeDetail = freezed,
    Object? businessContactNumber = freezed,
    Object? activeAppDateTime = freezed,
    Object? businessImage = freezed,
    Object? services = freezed,
  }) {
    return _then(_value.copyWith(
      bookingOrderID: bookingOrderID == freezed
          ? _value.bookingOrderID
          : bookingOrderID // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentType: appointmentType == freezed
          ? _value.appointmentType
          : appointmentType // ignore: cast_nullable_to_non_nullable
              as int,
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      bookingDate: bookingDate == freezed
          ? _value.bookingDate
          : bookingDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      bookingDateTime: bookingDateTime == freezed
          ? _value.bookingDateTime
          : bookingDateTime // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameEN: businessNameEN == freezed
          ? _value.businessNameEN
          : businessNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameAR: businessNameAR == freezed
          ? _value.businessNameAR
          : businessNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      statusId: statusId == freezed
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      statusNameEN: statusNameEN == freezed
          ? _value.statusNameEN
          : statusNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      statusNameAR: statusNameAR == freezed
          ? _value.statusNameAR
          : statusNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      feeDetail: feeDetail == freezed
          ? _value.feeDetail
          : feeDetail // ignore: cast_nullable_to_non_nullable
              as double,
      businessContactNumber: businessContactNumber == freezed
          ? _value.businessContactNumber
          : businessContactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      activeAppDateTime: activeAppDateTime == freezed
          ? _value.activeAppDateTime
          : activeAppDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      businessImage: businessImage == freezed
          ? _value.businessImage
          : businessImage // ignore: cast_nullable_to_non_nullable
              as String,
      services: services == freezed
          ? _value.services
          : services // ignore: cast_nullable_to_non_nullable
              as List<ServicesModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_AppointmentOrdersModelCopyWith<$Res>
    implements $AppointmentOrdersModelCopyWith<$Res> {
  factory _$$_AppointmentOrdersModelCopyWith(_$_AppointmentOrdersModel value,
          $Res Function(_$_AppointmentOrdersModel) then) =
      __$$_AppointmentOrdersModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String bookingOrderID,
      int appointmentType,
      String orderNumber,
      @JsonKey(name: "nameEN") String firstName,
      @JsonKey(name: "nameAR") String lastName,
      @JsonKey(defaultValue: "", nullable: true) String contactNumber,
      String email,
      DateTime bookingDate,
      String bookingDateTime,
      String businessID,
      String businessNameEN,
      String businessNameAR,
      String businessAddressEN,
      String businessAddressAR,
      double latitude,
      double longitude,
      int statusId,
      String statusNameEN,
      String statusNameAR,
      double feeDetail,
      String businessContactNumber,
      DateTime activeAppDateTime,
      @JsonKey(defaultValue: "", nullable: true) String businessImage,
      List<ServicesModel> services});
}

/// @nodoc
class __$$_AppointmentOrdersModelCopyWithImpl<$Res>
    extends _$AppointmentOrdersModelCopyWithImpl<$Res>
    implements _$$_AppointmentOrdersModelCopyWith<$Res> {
  __$$_AppointmentOrdersModelCopyWithImpl(_$_AppointmentOrdersModel _value,
      $Res Function(_$_AppointmentOrdersModel) _then)
      : super(_value, (v) => _then(v as _$_AppointmentOrdersModel));

  @override
  _$_AppointmentOrdersModel get _value =>
      super._value as _$_AppointmentOrdersModel;

  @override
  $Res call({
    Object? bookingOrderID = freezed,
    Object? appointmentType = freezed,
    Object? orderNumber = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? contactNumber = freezed,
    Object? email = freezed,
    Object? bookingDate = freezed,
    Object? bookingDateTime = freezed,
    Object? businessID = freezed,
    Object? businessNameEN = freezed,
    Object? businessNameAR = freezed,
    Object? businessAddressEN = freezed,
    Object? businessAddressAR = freezed,
    Object? latitude = freezed,
    Object? longitude = freezed,
    Object? statusId = freezed,
    Object? statusNameEN = freezed,
    Object? statusNameAR = freezed,
    Object? feeDetail = freezed,
    Object? businessContactNumber = freezed,
    Object? activeAppDateTime = freezed,
    Object? businessImage = freezed,
    Object? services = freezed,
  }) {
    return _then(_$_AppointmentOrdersModel(
      bookingOrderID: bookingOrderID == freezed
          ? _value.bookingOrderID
          : bookingOrderID // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentType: appointmentType == freezed
          ? _value.appointmentType
          : appointmentType // ignore: cast_nullable_to_non_nullable
              as int,
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      bookingDate: bookingDate == freezed
          ? _value.bookingDate
          : bookingDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      bookingDateTime: bookingDateTime == freezed
          ? _value.bookingDateTime
          : bookingDateTime // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameEN: businessNameEN == freezed
          ? _value.businessNameEN
          : businessNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameAR: businessNameAR == freezed
          ? _value.businessNameAR
          : businessNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressEN: businessAddressEN == freezed
          ? _value.businessAddressEN
          : businessAddressEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessAddressAR: businessAddressAR == freezed
          ? _value.businessAddressAR
          : businessAddressAR // ignore: cast_nullable_to_non_nullable
              as String,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      statusId: statusId == freezed
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      statusNameEN: statusNameEN == freezed
          ? _value.statusNameEN
          : statusNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      statusNameAR: statusNameAR == freezed
          ? _value.statusNameAR
          : statusNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      feeDetail: feeDetail == freezed
          ? _value.feeDetail
          : feeDetail // ignore: cast_nullable_to_non_nullable
              as double,
      businessContactNumber: businessContactNumber == freezed
          ? _value.businessContactNumber
          : businessContactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      activeAppDateTime: activeAppDateTime == freezed
          ? _value.activeAppDateTime
          : activeAppDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      businessImage: businessImage == freezed
          ? _value.businessImage
          : businessImage // ignore: cast_nullable_to_non_nullable
              as String,
      services: services == freezed
          ? _value._services
          : services // ignore: cast_nullable_to_non_nullable
              as List<ServicesModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_AppointmentOrdersModel extends _AppointmentOrdersModel {
  _$_AppointmentOrdersModel(
      {required this.bookingOrderID,
      required this.appointmentType,
      required this.orderNumber,
      @JsonKey(name: "nameEN") required this.firstName,
      @JsonKey(name: "nameAR") required this.lastName,
      @JsonKey(defaultValue: "", nullable: true) required this.contactNumber,
      required this.email,
      required this.bookingDate,
      required this.bookingDateTime,
      required this.businessID,
      required this.businessNameEN,
      required this.businessNameAR,
      required this.businessAddressEN,
      required this.businessAddressAR,
      required this.latitude,
      required this.longitude,
      required this.statusId,
      required this.statusNameEN,
      required this.statusNameAR,
      required this.feeDetail,
      required this.businessContactNumber,
      required this.activeAppDateTime,
      @JsonKey(defaultValue: "", nullable: true) required this.businessImage,
      required final List<ServicesModel> services})
      : _services = services,
        super._();

  factory _$_AppointmentOrdersModel.fromJson(Map<String, dynamic> json) =>
      _$$_AppointmentOrdersModelFromJson(json);

  @override
  final String bookingOrderID;
  @override
  final int appointmentType;
  @override
  final String orderNumber;
  @override
  @JsonKey(name: "nameEN")
  final String firstName;
  @override
  @JsonKey(name: "nameAR")
  final String lastName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String contactNumber;
  @override
  final String email;
  @override
  final DateTime bookingDate;
  @override
  final String bookingDateTime;
  @override
  final String businessID;
  @override
  final String businessNameEN;
  @override
  final String businessNameAR;
  @override
  final String businessAddressEN;
  @override
  final String businessAddressAR;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final int statusId;
  @override
  final String statusNameEN;
  @override
  final String statusNameAR;
  @override
  final double feeDetail;
  @override
  final String businessContactNumber;
  @override
  final DateTime activeAppDateTime;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String businessImage;
  final List<ServicesModel> _services;
  @override
  List<ServicesModel> get services {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_services);
  }

  @override
  String toString() {
    return 'AppointmentOrdersModel(bookingOrderID: $bookingOrderID, appointmentType: $appointmentType, orderNumber: $orderNumber, firstName: $firstName, lastName: $lastName, contactNumber: $contactNumber, email: $email, bookingDate: $bookingDate, bookingDateTime: $bookingDateTime, businessID: $businessID, businessNameEN: $businessNameEN, businessNameAR: $businessNameAR, businessAddressEN: $businessAddressEN, businessAddressAR: $businessAddressAR, latitude: $latitude, longitude: $longitude, statusId: $statusId, statusNameEN: $statusNameEN, statusNameAR: $statusNameAR, feeDetail: $feeDetail, businessContactNumber: $businessContactNumber, activeAppDateTime: $activeAppDateTime, businessImage: $businessImage, services: $services)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AppointmentOrdersModel &&
            const DeepCollectionEquality()
                .equals(other.bookingOrderID, bookingOrderID) &&
            const DeepCollectionEquality()
                .equals(other.appointmentType, appointmentType) &&
            const DeepCollectionEquality()
                .equals(other.orderNumber, orderNumber) &&
            const DeepCollectionEquality().equals(other.firstName, firstName) &&
            const DeepCollectionEquality().equals(other.lastName, lastName) &&
            const DeepCollectionEquality()
                .equals(other.contactNumber, contactNumber) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality()
                .equals(other.bookingDate, bookingDate) &&
            const DeepCollectionEquality()
                .equals(other.bookingDateTime, bookingDateTime) &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.businessNameEN, businessNameEN) &&
            const DeepCollectionEquality()
                .equals(other.businessNameAR, businessNameAR) &&
            const DeepCollectionEquality()
                .equals(other.businessAddressEN, businessAddressEN) &&
            const DeepCollectionEquality()
                .equals(other.businessAddressAR, businessAddressAR) &&
            const DeepCollectionEquality().equals(other.latitude, latitude) &&
            const DeepCollectionEquality().equals(other.longitude, longitude) &&
            const DeepCollectionEquality().equals(other.statusId, statusId) &&
            const DeepCollectionEquality()
                .equals(other.statusNameEN, statusNameEN) &&
            const DeepCollectionEquality()
                .equals(other.statusNameAR, statusNameAR) &&
            const DeepCollectionEquality().equals(other.feeDetail, feeDetail) &&
            const DeepCollectionEquality()
                .equals(other.businessContactNumber, businessContactNumber) &&
            const DeepCollectionEquality()
                .equals(other.activeAppDateTime, activeAppDateTime) &&
            const DeepCollectionEquality()
                .equals(other.businessImage, businessImage) &&
            const DeepCollectionEquality().equals(other._services, _services));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(bookingOrderID),
        const DeepCollectionEquality().hash(appointmentType),
        const DeepCollectionEquality().hash(orderNumber),
        const DeepCollectionEquality().hash(firstName),
        const DeepCollectionEquality().hash(lastName),
        const DeepCollectionEquality().hash(contactNumber),
        const DeepCollectionEquality().hash(email),
        const DeepCollectionEquality().hash(bookingDate),
        const DeepCollectionEquality().hash(bookingDateTime),
        const DeepCollectionEquality().hash(businessID),
        const DeepCollectionEquality().hash(businessNameEN),
        const DeepCollectionEquality().hash(businessNameAR),
        const DeepCollectionEquality().hash(businessAddressEN),
        const DeepCollectionEquality().hash(businessAddressAR),
        const DeepCollectionEquality().hash(latitude),
        const DeepCollectionEquality().hash(longitude),
        const DeepCollectionEquality().hash(statusId),
        const DeepCollectionEquality().hash(statusNameEN),
        const DeepCollectionEquality().hash(statusNameAR),
        const DeepCollectionEquality().hash(feeDetail),
        const DeepCollectionEquality().hash(businessContactNumber),
        const DeepCollectionEquality().hash(activeAppDateTime),
        const DeepCollectionEquality().hash(businessImage),
        const DeepCollectionEquality().hash(_services)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_AppointmentOrdersModelCopyWith<_$_AppointmentOrdersModel> get copyWith =>
      __$$_AppointmentOrdersModelCopyWithImpl<_$_AppointmentOrdersModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppointmentOrdersModelToJson(
      this,
    );
  }
}

abstract class _AppointmentOrdersModel extends AppointmentOrdersModel {
  factory _AppointmentOrdersModel(
      {required final String bookingOrderID,
      required final int appointmentType,
      required final String orderNumber,
      @JsonKey(name: "nameEN")
          required final String firstName,
      @JsonKey(name: "nameAR")
          required final String lastName,
      @JsonKey(defaultValue: "", nullable: true)
          required final String contactNumber,
      required final String email,
      required final DateTime bookingDate,
      required final String bookingDateTime,
      required final String businessID,
      required final String businessNameEN,
      required final String businessNameAR,
      required final String businessAddressEN,
      required final String businessAddressAR,
      required final double latitude,
      required final double longitude,
      required final int statusId,
      required final String statusNameEN,
      required final String statusNameAR,
      required final double feeDetail,
      required final String businessContactNumber,
      required final DateTime activeAppDateTime,
      @JsonKey(defaultValue: "", nullable: true)
          required final String businessImage,
      required final List<ServicesModel> services}) = _$_AppointmentOrdersModel;
  _AppointmentOrdersModel._() : super._();

  factory _AppointmentOrdersModel.fromJson(Map<String, dynamic> json) =
      _$_AppointmentOrdersModel.fromJson;

  @override
  String get bookingOrderID;
  @override
  int get appointmentType;
  @override
  String get orderNumber;
  @override
  @JsonKey(name: "nameEN")
  String get firstName;
  @override
  @JsonKey(name: "nameAR")
  String get lastName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get contactNumber;
  @override
  String get email;
  @override
  DateTime get bookingDate;
  @override
  String get bookingDateTime;
  @override
  String get businessID;
  @override
  String get businessNameEN;
  @override
  String get businessNameAR;
  @override
  String get businessAddressEN;
  @override
  String get businessAddressAR;
  @override
  double get latitude;
  @override
  double get longitude;
  @override
  int get statusId;
  @override
  String get statusNameEN;
  @override
  String get statusNameAR;
  @override
  double get feeDetail;
  @override
  String get businessContactNumber;
  @override
  DateTime get activeAppDateTime;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get businessImage;
  @override
  List<ServicesModel> get services;
  @override
  @JsonKey(ignore: true)
  _$$_AppointmentOrdersModelCopyWith<_$_AppointmentOrdersModel> get copyWith =>
      throw _privateConstructorUsedError;
}
