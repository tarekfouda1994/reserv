import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/res.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';
import '../services_model/services_model.dart';

part 'appointment_orders.freezed.dart';
part 'appointment_orders.g.dart';

@freezed
class AppointmentOrdersModel with _$AppointmentOrdersModel {
  AppointmentOrdersModel._();

  @JsonSerializable(explicitToJson: true)
  factory AppointmentOrdersModel({
    required String bookingOrderID,
    required int appointmentType,
    required String orderNumber,
   @JsonKey(name: "nameEN") required String firstName,
    @JsonKey(name: "nameAR") required String lastName,
    @JsonKey(defaultValue: "", nullable: true) required String contactNumber,
    required String email,
    required DateTime bookingDate,
    required String bookingDateTime,
    required String businessID,
    required String businessNameEN,
    required String businessNameAR,
    required String businessAddressEN,
    required String businessAddressAR,
    required double latitude,
    required double longitude,
    required int statusId,
    required String statusNameEN,
    required String statusNameAR,
    required double feeDetail,
    required String businessContactNumber,
    required DateTime activeAppDateTime,
    @JsonKey(defaultValue: "", nullable: true) required String businessImage,
    required List<ServicesModel> services,
  }) = _AppointmentOrdersModel;

  factory AppointmentOrdersModel.fromJson(Map<String, dynamic> json) =>
      _$AppointmentOrdersModelFromJson(json);

  String getBusinessName() {
    return getIt<Utilities>().getLocalizedValue(businessNameAR, businessNameEN);
  }

  String getUserName() {
    return "$firstName $lastName";
  }

  String getAddressName() {
    return getIt<Utilities>()
        .getLocalizedValue(businessAddressAR, businessAddressEN);
  }

  String getStatus() {
    return getIt<Utilities>().getLocalizedValue(statusNameAR, statusNameEN);
  }
}

extension StatusColor on int {
  Color getStatusBgColor() {
    switch (this) {
      case 0:
        return Color(0xffffbb00);
      case 1:
        return MyColors.primary;
      case 2:
        return Color(0xffED3A57);
      case 3:
        return MyColors.secondary;
      case 4:
        return MyColors.black;
      default:
        return MyColors.primary;
    }
  }



  Color getStatusBgDateColor() {
    switch (this) {
      case 0:
        return Color(0xffffbb00);
      case 1:
        return MyColors.primary;
      case 2:
        return Color(0xffED3A57);
      case 3:
        return MyColors.secondary;
      case 4:
        return MyColors.black;
      default:
        return MyColors.reserveTogetherBg;
    }
  }

  String getGroupIcon() {
    switch (this) {
      case 0:
        return Res.Group_yellow;
      case 1:
        return Res.appointmentActive;
      case 2:
        return Res.Group_red;
      case 3:
        return Res.Group_reschedule;
      case 4:
        return Res.Group_black;
      default:
        return Res.appointmentActive;
    }
  }

  String getStatusIcon() {
    switch (this) {
      case 0:
        return Res.cross;
      case 1:
        return Res.check_black;
      case 2:
        return Res.cross;
      case 3:
        return Res.refresh;
      case 4:
        return Res.time_delete;
      default:
        return Res.check_black;
    }
  }

  Color getStatusTextColor() {
    switch (this) {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
        return MyColors.white;
      default:
        return MyColors.primary;
    }
  } Color getDashboardStatusTextColor() {
    switch (this) {
      case 1:
      return MyColors.primary;
      case 0:
        return Colors.amber;
      case 2:
        return Color(0xffED3A57);
      case 3:
      case 4:
        return MyColors.black;
      default:
        return MyColors.primary;
    }
  }

  Color getDateColor() {
    switch (this) {
      case 1:
      case 2:
      case 4:
        return MyColors.white;
      case 0:
      case 3:
        return MyColors.black;
      default:
        return MyColors.primary;
    }
  }
}
