// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_orders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppointmentOrdersModel _$$_AppointmentOrdersModelFromJson(
        Map<String, dynamic> json) =>
    _$_AppointmentOrdersModel(
      bookingOrderID: json['bookingOrderID'] as String,
      appointmentType: json['appointmentType'] as int,
      orderNumber: json['orderNumber'] as String,
      firstName: json['nameEN'] as String,
      lastName: json['nameAR'] as String,
      contactNumber: json['contactNumber'] as String? ?? '',
      email: json['email'] as String,
      bookingDate: DateTime.parse(json['bookingDate'] as String),
      bookingDateTime: json['bookingDateTime'] as String,
      businessID: json['businessID'] as String,
      businessNameEN: json['businessNameEN'] as String,
      businessNameAR: json['businessNameAR'] as String,
      businessAddressEN: json['businessAddressEN'] as String,
      businessAddressAR: json['businessAddressAR'] as String,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      statusId: json['statusId'] as int,
      statusNameEN: json['statusNameEN'] as String,
      statusNameAR: json['statusNameAR'] as String,
      feeDetail: (json['feeDetail'] as num).toDouble(),
      businessContactNumber: json['businessContactNumber'] as String,
      activeAppDateTime: DateTime.parse(json['activeAppDateTime'] as String),
      businessImage: json['businessImage'] as String? ?? '',
      services: (json['services'] as List<dynamic>)
          .map((e) => ServicesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AppointmentOrdersModelToJson(
        _$_AppointmentOrdersModel instance) =>
    <String, dynamic>{
      'bookingOrderID': instance.bookingOrderID,
      'appointmentType': instance.appointmentType,
      'orderNumber': instance.orderNumber,
      'nameEN': instance.firstName,
      'nameAR': instance.lastName,
      'contactNumber': instance.contactNumber,
      'email': instance.email,
      'bookingDate': instance.bookingDate.toIso8601String(),
      'bookingDateTime': instance.bookingDateTime,
      'businessID': instance.businessID,
      'businessNameEN': instance.businessNameEN,
      'businessNameAR': instance.businessNameAR,
      'businessAddressEN': instance.businessAddressEN,
      'businessAddressAR': instance.businessAddressAR,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'statusId': instance.statusId,
      'statusNameEN': instance.statusNameEN,
      'statusNameAR': instance.statusNameAR,
      'feeDetail': instance.feeDetail,
      'businessContactNumber': instance.businessContactNumber,
      'activeAppDateTime': instance.activeAppDateTime.toIso8601String(),
      'businessImage': instance.businessImage,
      'services': instance.services.map((e) => e.toJson()).toList(),
    };
