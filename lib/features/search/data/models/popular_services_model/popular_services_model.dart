import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'popular_services_model.freezed.dart';
part 'popular_services_model.g.dart';

@freezed
class PopularServicesModel with _$PopularServicesModel {
  PopularServicesModel._();

  @JsonSerializable(explicitToJson: true)
  factory PopularServicesModel({
    required String englishName,
    required String arabicName,
    @JsonKey(defaultValue: "", nullable: true) required String avatar,
  }) = _PopularServicesModel;

  factory PopularServicesModel.fromJson(Map<String, dynamic> json) =>
      _$PopularServicesModelFromJson(json);

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}
