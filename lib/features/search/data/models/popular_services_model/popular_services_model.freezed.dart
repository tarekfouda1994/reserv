// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'popular_services_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PopularServicesModel _$PopularServicesModelFromJson(Map<String, dynamic> json) {
  return _PopularServicesModel.fromJson(json);
}

/// @nodoc
mixin _$PopularServicesModel {
  String get englishName => throw _privateConstructorUsedError;
  String get arabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get avatar => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PopularServicesModelCopyWith<PopularServicesModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PopularServicesModelCopyWith<$Res> {
  factory $PopularServicesModelCopyWith(PopularServicesModel value,
          $Res Function(PopularServicesModel) then) =
      _$PopularServicesModelCopyWithImpl<$Res>;
  $Res call(
      {String englishName,
      String arabicName,
      @JsonKey(defaultValue: "", nullable: true) String avatar});
}

/// @nodoc
class _$PopularServicesModelCopyWithImpl<$Res>
    implements $PopularServicesModelCopyWith<$Res> {
  _$PopularServicesModelCopyWithImpl(this._value, this._then);

  final PopularServicesModel _value;
  // ignore: unused_field
  final $Res Function(PopularServicesModel) _then;

  @override
  $Res call({
    Object? englishName = freezed,
    Object? arabicName = freezed,
    Object? avatar = freezed,
  }) {
    return _then(_value.copyWith(
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PopularServicesModelCopyWith<$Res>
    implements $PopularServicesModelCopyWith<$Res> {
  factory _$$_PopularServicesModelCopyWith(_$_PopularServicesModel value,
          $Res Function(_$_PopularServicesModel) then) =
      __$$_PopularServicesModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String englishName,
      String arabicName,
      @JsonKey(defaultValue: "", nullable: true) String avatar});
}

/// @nodoc
class __$$_PopularServicesModelCopyWithImpl<$Res>
    extends _$PopularServicesModelCopyWithImpl<$Res>
    implements _$$_PopularServicesModelCopyWith<$Res> {
  __$$_PopularServicesModelCopyWithImpl(_$_PopularServicesModel _value,
      $Res Function(_$_PopularServicesModel) _then)
      : super(_value, (v) => _then(v as _$_PopularServicesModel));

  @override
  _$_PopularServicesModel get _value => super._value as _$_PopularServicesModel;

  @override
  $Res call({
    Object? englishName = freezed,
    Object? arabicName = freezed,
    Object? avatar = freezed,
  }) {
    return _then(_$_PopularServicesModel(
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PopularServicesModel extends _PopularServicesModel {
  _$_PopularServicesModel(
      {required this.englishName,
      required this.arabicName,
      @JsonKey(defaultValue: "", nullable: true) required this.avatar})
      : super._();

  factory _$_PopularServicesModel.fromJson(Map<String, dynamic> json) =>
      _$$_PopularServicesModelFromJson(json);

  @override
  final String englishName;
  @override
  final String arabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  final String avatar;

  @override
  String toString() {
    return 'PopularServicesModel(englishName: $englishName, arabicName: $arabicName, avatar: $avatar)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PopularServicesModel &&
            const DeepCollectionEquality()
                .equals(other.englishName, englishName) &&
            const DeepCollectionEquality()
                .equals(other.arabicName, arabicName) &&
            const DeepCollectionEquality().equals(other.avatar, avatar));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(englishName),
      const DeepCollectionEquality().hash(arabicName),
      const DeepCollectionEquality().hash(avatar));

  @JsonKey(ignore: true)
  @override
  _$$_PopularServicesModelCopyWith<_$_PopularServicesModel> get copyWith =>
      __$$_PopularServicesModelCopyWithImpl<_$_PopularServicesModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PopularServicesModelToJson(
      this,
    );
  }
}

abstract class _PopularServicesModel extends PopularServicesModel {
  factory _PopularServicesModel(
      {required final String englishName,
      required final String arabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required final String avatar}) = _$_PopularServicesModel;
  _PopularServicesModel._() : super._();

  factory _PopularServicesModel.fromJson(Map<String, dynamic> json) =
      _$_PopularServicesModel.fromJson;

  @override
  String get englishName;
  @override
  String get arabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get avatar;
  @override
  @JsonKey(ignore: true)
  _$$_PopularServicesModelCopyWith<_$_PopularServicesModel> get copyWith =>
      throw _privateConstructorUsedError;
}
