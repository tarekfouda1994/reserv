// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'popular_services_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PopularServicesModel _$$_PopularServicesModelFromJson(
        Map<String, dynamic> json) =>
    _$_PopularServicesModel(
      englishName: json['englishName'] as String,
      arabicName: json['arabicName'] as String,
      avatar: json['avatar'] as String? ?? '',
    );

Map<String, dynamic> _$$_PopularServicesModelToJson(
        _$_PopularServicesModel instance) =>
    <String, dynamic>{
      'englishName': instance.englishName,
      'arabicName': instance.arabicName,
      'avatar': instance.avatar,
    };
