// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatModel _$$_ChatModelFromJson(Map<String, dynamic> json) => _$_ChatModel(
      orderNumber: json['orderNumber'] as String,
      businessID: json['businessID'] as String,
      chatStatus: json['chatStatus'] as int,
      bookingOrderID: json['bookingOrderID'] as String,
      message: json['message'] as String? ?? '',
      insertedDate: json['insertedDate'] as String,
      insertedTime: json['insertedTime'] as String,
      businessUserPhotoPath: json['businessUserPhotoPath'] as String? ?? '',
      hasReadMessage: json['hasReadMessage'] as bool,
      isActive: json['isActive'] as bool,
      isClient: json['isClient'] as bool,
    );

Map<String, dynamic> _$$_ChatModelToJson(_$_ChatModel instance) =>
    <String, dynamic>{
      'orderNumber': instance.orderNumber,
      'businessID': instance.businessID,
      'chatStatus': instance.chatStatus,
      'bookingOrderID': instance.bookingOrderID,
      'message': instance.message,
      'insertedDate': instance.insertedDate,
      'insertedTime': instance.insertedTime,
      'businessUserPhotoPath': instance.businessUserPhotoPath,
      'hasReadMessage': instance.hasReadMessage,
      'isActive': instance.isActive,
      'isClient': instance.isClient,
    };
