import 'package:freezed_annotation/freezed_annotation.dart';

part 'chat_model.freezed.dart';
part 'chat_model.g.dart';

@freezed
class ChatModel with _$ChatModel {
  @JsonSerializable(explicitToJson: true)
  factory ChatModel({
    required String orderNumber,
    required String businessID,
    required int chatStatus,
    required String bookingOrderID,
    @JsonKey(nullable: true, defaultValue: "") required String message,
    required String insertedDate,
    required String insertedTime,
    @JsonKey(nullable: true, defaultValue: "")
        required String businessUserPhotoPath,
    required bool hasReadMessage,
    required bool isActive,
    required bool isClient,
  }) = _ChatModel;

  factory ChatModel.fromJson(Map<String, dynamic> json) =>
      _$ChatModelFromJson(json);
}
