// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chat_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatModel _$ChatModelFromJson(Map<String, dynamic> json) {
  return _ChatModel.fromJson(json);
}

/// @nodoc
mixin _$ChatModel {
  String get orderNumber => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  int get chatStatus => throw _privateConstructorUsedError;
  String get bookingOrderID => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get message => throw _privateConstructorUsedError;
  String get insertedDate => throw _privateConstructorUsedError;
  String get insertedTime => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get businessUserPhotoPath => throw _privateConstructorUsedError;
  bool get hasReadMessage => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  bool get isClient => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatModelCopyWith<ChatModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatModelCopyWith<$Res> {
  factory $ChatModelCopyWith(ChatModel value, $Res Function(ChatModel) then) =
      _$ChatModelCopyWithImpl<$Res>;
  $Res call(
      {String orderNumber,
      String businessID,
      int chatStatus,
      String bookingOrderID,
      @JsonKey(nullable: true, defaultValue: "") String message,
      String insertedDate,
      String insertedTime,
      @JsonKey(nullable: true, defaultValue: "") String businessUserPhotoPath,
      bool hasReadMessage,
      bool isActive,
      bool isClient});
}

/// @nodoc
class _$ChatModelCopyWithImpl<$Res> implements $ChatModelCopyWith<$Res> {
  _$ChatModelCopyWithImpl(this._value, this._then);

  final ChatModel _value;
  // ignore: unused_field
  final $Res Function(ChatModel) _then;

  @override
  $Res call({
    Object? orderNumber = freezed,
    Object? businessID = freezed,
    Object? chatStatus = freezed,
    Object? bookingOrderID = freezed,
    Object? message = freezed,
    Object? insertedDate = freezed,
    Object? insertedTime = freezed,
    Object? businessUserPhotoPath = freezed,
    Object? hasReadMessage = freezed,
    Object? isActive = freezed,
    Object? isClient = freezed,
  }) {
    return _then(_value.copyWith(
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      chatStatus: chatStatus == freezed
          ? _value.chatStatus
          : chatStatus // ignore: cast_nullable_to_non_nullable
              as int,
      bookingOrderID: bookingOrderID == freezed
          ? _value.bookingOrderID
          : bookingOrderID // ignore: cast_nullable_to_non_nullable
              as String,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      insertedDate: insertedDate == freezed
          ? _value.insertedDate
          : insertedDate // ignore: cast_nullable_to_non_nullable
              as String,
      insertedTime: insertedTime == freezed
          ? _value.insertedTime
          : insertedTime // ignore: cast_nullable_to_non_nullable
              as String,
      businessUserPhotoPath: businessUserPhotoPath == freezed
          ? _value.businessUserPhotoPath
          : businessUserPhotoPath // ignore: cast_nullable_to_non_nullable
              as String,
      hasReadMessage: hasReadMessage == freezed
          ? _value.hasReadMessage
          : hasReadMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      isClient: isClient == freezed
          ? _value.isClient
          : isClient // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_ChatModelCopyWith<$Res> implements $ChatModelCopyWith<$Res> {
  factory _$$_ChatModelCopyWith(
          _$_ChatModel value, $Res Function(_$_ChatModel) then) =
      __$$_ChatModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String orderNumber,
      String businessID,
      int chatStatus,
      String bookingOrderID,
      @JsonKey(nullable: true, defaultValue: "") String message,
      String insertedDate,
      String insertedTime,
      @JsonKey(nullable: true, defaultValue: "") String businessUserPhotoPath,
      bool hasReadMessage,
      bool isActive,
      bool isClient});
}

/// @nodoc
class __$$_ChatModelCopyWithImpl<$Res> extends _$ChatModelCopyWithImpl<$Res>
    implements _$$_ChatModelCopyWith<$Res> {
  __$$_ChatModelCopyWithImpl(
      _$_ChatModel _value, $Res Function(_$_ChatModel) _then)
      : super(_value, (v) => _then(v as _$_ChatModel));

  @override
  _$_ChatModel get _value => super._value as _$_ChatModel;

  @override
  $Res call({
    Object? orderNumber = freezed,
    Object? businessID = freezed,
    Object? chatStatus = freezed,
    Object? bookingOrderID = freezed,
    Object? message = freezed,
    Object? insertedDate = freezed,
    Object? insertedTime = freezed,
    Object? businessUserPhotoPath = freezed,
    Object? hasReadMessage = freezed,
    Object? isActive = freezed,
    Object? isClient = freezed,
  }) {
    return _then(_$_ChatModel(
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      chatStatus: chatStatus == freezed
          ? _value.chatStatus
          : chatStatus // ignore: cast_nullable_to_non_nullable
              as int,
      bookingOrderID: bookingOrderID == freezed
          ? _value.bookingOrderID
          : bookingOrderID // ignore: cast_nullable_to_non_nullable
              as String,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      insertedDate: insertedDate == freezed
          ? _value.insertedDate
          : insertedDate // ignore: cast_nullable_to_non_nullable
              as String,
      insertedTime: insertedTime == freezed
          ? _value.insertedTime
          : insertedTime // ignore: cast_nullable_to_non_nullable
              as String,
      businessUserPhotoPath: businessUserPhotoPath == freezed
          ? _value.businessUserPhotoPath
          : businessUserPhotoPath // ignore: cast_nullable_to_non_nullable
              as String,
      hasReadMessage: hasReadMessage == freezed
          ? _value.hasReadMessage
          : hasReadMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      isClient: isClient == freezed
          ? _value.isClient
          : isClient // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ChatModel implements _ChatModel {
  _$_ChatModel(
      {required this.orderNumber,
      required this.businessID,
      required this.chatStatus,
      required this.bookingOrderID,
      @JsonKey(nullable: true, defaultValue: "")
          required this.message,
      required this.insertedDate,
      required this.insertedTime,
      @JsonKey(nullable: true, defaultValue: "")
          required this.businessUserPhotoPath,
      required this.hasReadMessage,
      required this.isActive,
      required this.isClient});

  factory _$_ChatModel.fromJson(Map<String, dynamic> json) =>
      _$$_ChatModelFromJson(json);

  @override
  final String orderNumber;
  @override
  final String businessID;
  @override
  final int chatStatus;
  @override
  final String bookingOrderID;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String message;
  @override
  final String insertedDate;
  @override
  final String insertedTime;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String businessUserPhotoPath;
  @override
  final bool hasReadMessage;
  @override
  final bool isActive;
  @override
  final bool isClient;

  @override
  String toString() {
    return 'ChatModel(orderNumber: $orderNumber, businessID: $businessID, chatStatus: $chatStatus, bookingOrderID: $bookingOrderID, message: $message, insertedDate: $insertedDate, insertedTime: $insertedTime, businessUserPhotoPath: $businessUserPhotoPath, hasReadMessage: $hasReadMessage, isActive: $isActive, isClient: $isClient)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatModel &&
            const DeepCollectionEquality()
                .equals(other.orderNumber, orderNumber) &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.chatStatus, chatStatus) &&
            const DeepCollectionEquality()
                .equals(other.bookingOrderID, bookingOrderID) &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality()
                .equals(other.insertedDate, insertedDate) &&
            const DeepCollectionEquality()
                .equals(other.insertedTime, insertedTime) &&
            const DeepCollectionEquality()
                .equals(other.businessUserPhotoPath, businessUserPhotoPath) &&
            const DeepCollectionEquality()
                .equals(other.hasReadMessage, hasReadMessage) &&
            const DeepCollectionEquality().equals(other.isActive, isActive) &&
            const DeepCollectionEquality().equals(other.isClient, isClient));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(orderNumber),
      const DeepCollectionEquality().hash(businessID),
      const DeepCollectionEquality().hash(chatStatus),
      const DeepCollectionEquality().hash(bookingOrderID),
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(insertedDate),
      const DeepCollectionEquality().hash(insertedTime),
      const DeepCollectionEquality().hash(businessUserPhotoPath),
      const DeepCollectionEquality().hash(hasReadMessage),
      const DeepCollectionEquality().hash(isActive),
      const DeepCollectionEquality().hash(isClient));

  @JsonKey(ignore: true)
  @override
  _$$_ChatModelCopyWith<_$_ChatModel> get copyWith =>
      __$$_ChatModelCopyWithImpl<_$_ChatModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatModelToJson(
      this,
    );
  }
}

abstract class _ChatModel implements ChatModel {
  factory _ChatModel(
      {required final String orderNumber,
      required final String businessID,
      required final int chatStatus,
      required final String bookingOrderID,
      @JsonKey(nullable: true, defaultValue: "")
          required final String message,
      required final String insertedDate,
      required final String insertedTime,
      @JsonKey(nullable: true, defaultValue: "")
          required final String businessUserPhotoPath,
      required final bool hasReadMessage,
      required final bool isActive,
      required final bool isClient}) = _$_ChatModel;

  factory _ChatModel.fromJson(Map<String, dynamic> json) =
      _$_ChatModel.fromJson;

  @override
  String get orderNumber;
  @override
  String get businessID;
  @override
  int get chatStatus;
  @override
  String get bookingOrderID;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get message;
  @override
  String get insertedDate;
  @override
  String get insertedTime;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get businessUserPhotoPath;
  @override
  bool get hasReadMessage;
  @override
  bool get isActive;
  @override
  bool get isClient;
  @override
  @JsonKey(ignore: true)
  _$$_ChatModelCopyWith<_$_ChatModel> get copyWith =>
      throw _privateConstructorUsedError;
}
