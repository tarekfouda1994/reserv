// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'order_gift_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

OrderGiftModel _$OrderGiftModelFromJson(Map<String, dynamic> json) {
  return _OrderGiftModel.fromJson(json);
}

/// @nodoc
mixin _$OrderGiftModel {
  @JsonKey(defaultValue: 0, nullable: true)
  int get bookingOrderId => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantFirstName => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantLastName => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantEmail => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantMobile => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get wishes => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OrderGiftModelCopyWith<OrderGiftModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrderGiftModelCopyWith<$Res> {
  factory $OrderGiftModelCopyWith(
          OrderGiftModel value, $Res Function(OrderGiftModel) then) =
      _$OrderGiftModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: 0, nullable: true) int bookingOrderId,
      @JsonKey(nullable: true, defaultValue: "") String applicantFirstName,
      @JsonKey(nullable: true, defaultValue: "") String applicantLastName,
      @JsonKey(nullable: true, defaultValue: "") String applicantEmail,
      @JsonKey(nullable: true, defaultValue: "") String applicantMobile,
      @JsonKey(nullable: true, defaultValue: "") String wishes});
}

/// @nodoc
class _$OrderGiftModelCopyWithImpl<$Res>
    implements $OrderGiftModelCopyWith<$Res> {
  _$OrderGiftModelCopyWithImpl(this._value, this._then);

  final OrderGiftModel _value;
  // ignore: unused_field
  final $Res Function(OrderGiftModel) _then;

  @override
  $Res call({
    Object? bookingOrderId = freezed,
    Object? applicantFirstName = freezed,
    Object? applicantLastName = freezed,
    Object? applicantEmail = freezed,
    Object? applicantMobile = freezed,
    Object? wishes = freezed,
  }) {
    return _then(_value.copyWith(
      bookingOrderId: bookingOrderId == freezed
          ? _value.bookingOrderId
          : bookingOrderId // ignore: cast_nullable_to_non_nullable
              as int,
      applicantFirstName: applicantFirstName == freezed
          ? _value.applicantFirstName
          : applicantFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      applicantLastName: applicantLastName == freezed
          ? _value.applicantLastName
          : applicantLastName // ignore: cast_nullable_to_non_nullable
              as String,
      applicantEmail: applicantEmail == freezed
          ? _value.applicantEmail
          : applicantEmail // ignore: cast_nullable_to_non_nullable
              as String,
      applicantMobile: applicantMobile == freezed
          ? _value.applicantMobile
          : applicantMobile // ignore: cast_nullable_to_non_nullable
              as String,
      wishes: wishes == freezed
          ? _value.wishes
          : wishes // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_OrderGiftModelCopyWith<$Res>
    implements $OrderGiftModelCopyWith<$Res> {
  factory _$$_OrderGiftModelCopyWith(
          _$_OrderGiftModel value, $Res Function(_$_OrderGiftModel) then) =
      __$$_OrderGiftModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: 0, nullable: true) int bookingOrderId,
      @JsonKey(nullable: true, defaultValue: "") String applicantFirstName,
      @JsonKey(nullable: true, defaultValue: "") String applicantLastName,
      @JsonKey(nullable: true, defaultValue: "") String applicantEmail,
      @JsonKey(nullable: true, defaultValue: "") String applicantMobile,
      @JsonKey(nullable: true, defaultValue: "") String wishes});
}

/// @nodoc
class __$$_OrderGiftModelCopyWithImpl<$Res>
    extends _$OrderGiftModelCopyWithImpl<$Res>
    implements _$$_OrderGiftModelCopyWith<$Res> {
  __$$_OrderGiftModelCopyWithImpl(
      _$_OrderGiftModel _value, $Res Function(_$_OrderGiftModel) _then)
      : super(_value, (v) => _then(v as _$_OrderGiftModel));

  @override
  _$_OrderGiftModel get _value => super._value as _$_OrderGiftModel;

  @override
  $Res call({
    Object? bookingOrderId = freezed,
    Object? applicantFirstName = freezed,
    Object? applicantLastName = freezed,
    Object? applicantEmail = freezed,
    Object? applicantMobile = freezed,
    Object? wishes = freezed,
  }) {
    return _then(_$_OrderGiftModel(
      bookingOrderId: bookingOrderId == freezed
          ? _value.bookingOrderId
          : bookingOrderId // ignore: cast_nullable_to_non_nullable
              as int,
      applicantFirstName: applicantFirstName == freezed
          ? _value.applicantFirstName
          : applicantFirstName // ignore: cast_nullable_to_non_nullable
              as String,
      applicantLastName: applicantLastName == freezed
          ? _value.applicantLastName
          : applicantLastName // ignore: cast_nullable_to_non_nullable
              as String,
      applicantEmail: applicantEmail == freezed
          ? _value.applicantEmail
          : applicantEmail // ignore: cast_nullable_to_non_nullable
              as String,
      applicantMobile: applicantMobile == freezed
          ? _value.applicantMobile
          : applicantMobile // ignore: cast_nullable_to_non_nullable
              as String,
      wishes: wishes == freezed
          ? _value.wishes
          : wishes // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_OrderGiftModel implements _OrderGiftModel {
  _$_OrderGiftModel(
      {@JsonKey(defaultValue: 0, nullable: true)
          required this.bookingOrderId,
      @JsonKey(nullable: true, defaultValue: "")
          required this.applicantFirstName,
      @JsonKey(nullable: true, defaultValue: "")
          required this.applicantLastName,
      @JsonKey(nullable: true, defaultValue: "")
          required this.applicantEmail,
      @JsonKey(nullable: true, defaultValue: "")
          required this.applicantMobile,
      @JsonKey(nullable: true, defaultValue: "")
          required this.wishes});

  factory _$_OrderGiftModel.fromJson(Map<String, dynamic> json) =>
      _$$_OrderGiftModelFromJson(json);

  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final int bookingOrderId;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String applicantFirstName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String applicantLastName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String applicantEmail;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String applicantMobile;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  final String wishes;

  @override
  String toString() {
    return 'OrderGiftModel(bookingOrderId: $bookingOrderId, applicantFirstName: $applicantFirstName, applicantLastName: $applicantLastName, applicantEmail: $applicantEmail, applicantMobile: $applicantMobile, wishes: $wishes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OrderGiftModel &&
            const DeepCollectionEquality()
                .equals(other.bookingOrderId, bookingOrderId) &&
            const DeepCollectionEquality()
                .equals(other.applicantFirstName, applicantFirstName) &&
            const DeepCollectionEquality()
                .equals(other.applicantLastName, applicantLastName) &&
            const DeepCollectionEquality()
                .equals(other.applicantEmail, applicantEmail) &&
            const DeepCollectionEquality()
                .equals(other.applicantMobile, applicantMobile) &&
            const DeepCollectionEquality().equals(other.wishes, wishes));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(bookingOrderId),
      const DeepCollectionEquality().hash(applicantFirstName),
      const DeepCollectionEquality().hash(applicantLastName),
      const DeepCollectionEquality().hash(applicantEmail),
      const DeepCollectionEquality().hash(applicantMobile),
      const DeepCollectionEquality().hash(wishes));

  @JsonKey(ignore: true)
  @override
  _$$_OrderGiftModelCopyWith<_$_OrderGiftModel> get copyWith =>
      __$$_OrderGiftModelCopyWithImpl<_$_OrderGiftModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_OrderGiftModelToJson(
      this,
    );
  }
}

abstract class _OrderGiftModel implements OrderGiftModel {
  factory _OrderGiftModel(
      {@JsonKey(defaultValue: 0, nullable: true)
          required final int bookingOrderId,
      @JsonKey(nullable: true, defaultValue: "")
          required final String applicantFirstName,
      @JsonKey(nullable: true, defaultValue: "")
          required final String applicantLastName,
      @JsonKey(nullable: true, defaultValue: "")
          required final String applicantEmail,
      @JsonKey(nullable: true, defaultValue: "")
          required final String applicantMobile,
      @JsonKey(nullable: true, defaultValue: "")
          required final String wishes}) = _$_OrderGiftModel;

  factory _OrderGiftModel.fromJson(Map<String, dynamic> json) =
      _$_OrderGiftModel.fromJson;

  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int get bookingOrderId;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantFirstName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantLastName;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantEmail;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get applicantMobile;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get wishes;
  @override
  @JsonKey(ignore: true)
  _$$_OrderGiftModelCopyWith<_$_OrderGiftModel> get copyWith =>
      throw _privateConstructorUsedError;
}
