import 'package:freezed_annotation/freezed_annotation.dart';

part 'order_gift_model.freezed.dart';
part 'order_gift_model.g.dart';

@freezed
class OrderGiftModel with _$OrderGiftModel {
  @JsonSerializable(explicitToJson: true)
  factory OrderGiftModel({
    @JsonKey(defaultValue: 0, nullable: true) required int bookingOrderId,
    @JsonKey(nullable: true, defaultValue: "") required String applicantFirstName,
    @JsonKey(nullable: true, defaultValue: "") required String applicantLastName,
    @JsonKey(nullable: true, defaultValue: "") required String applicantEmail,
    @JsonKey(nullable: true, defaultValue: "") required String applicantMobile,
    @JsonKey(nullable: true, defaultValue: "") required String wishes,
  }) = _OrderGiftModel;

  factory OrderGiftModel.fromJson(Map<String, dynamic> json) => _$OrderGiftModelFromJson(json);
}
