// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_gift_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_OrderGiftModel _$$_OrderGiftModelFromJson(Map<String, dynamic> json) =>
    _$_OrderGiftModel(
      bookingOrderId: json['bookingOrderId'] as int? ?? 0,
      applicantFirstName: json['applicantFirstName'] as String? ?? '',
      applicantLastName: json['applicantLastName'] as String? ?? '',
      applicantEmail: json['applicantEmail'] as String? ?? '',
      applicantMobile: json['applicantMobile'] as String? ?? '',
      wishes: json['wishes'] as String? ?? '',
    );

Map<String, dynamic> _$$_OrderGiftModelToJson(_$_OrderGiftModel instance) =>
    <String, dynamic>{
      'bookingOrderId': instance.bookingOrderId,
      'applicantFirstName': instance.applicantFirstName,
      'applicantLastName': instance.applicantLastName,
      'applicantEmail': instance.applicantEmail,
      'applicantMobile': instance.applicantMobile,
      'wishes': instance.wishes,
    };
