// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'ad_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AdModel _$AdModelFromJson(Map<String, dynamic> json) {
  return _AdModel.fromJson(json);
}

/// @nodoc
mixin _$AdModel {
  String get id => throw _privateConstructorUsedError;
  String get titleEnglish => throw _privateConstructorUsedError;
  String get titleArabic => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;
  String get url => throw _privateConstructorUsedError;
  DateTime get fromDate => throw _privateConstructorUsedError;
  DateTime get toDate => throw _privateConstructorUsedError;
  int get sortOrder => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  String get notes => throw _privateConstructorUsedError;
  String get createdBy => throw _privateConstructorUsedError;
  DateTime get createdDate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AdModelCopyWith<AdModel> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AdModelCopyWith<$Res> {
  factory $AdModelCopyWith(AdModel value, $Res Function(AdModel) then) =
      _$AdModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String titleEnglish,
      String titleArabic,
      String image,
      String url,
      DateTime fromDate,
      DateTime toDate,
      int sortOrder,
      bool isActive,
      String notes,
      String createdBy,
      DateTime createdDate});
}

/// @nodoc
class _$AdModelCopyWithImpl<$Res> implements $AdModelCopyWith<$Res> {
  _$AdModelCopyWithImpl(this._value, this._then);

  final AdModel _value;
  // ignore: unused_field
  final $Res Function(AdModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? titleEnglish = freezed,
    Object? titleArabic = freezed,
    Object? image = freezed,
    Object? url = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? sortOrder = freezed,
    Object? isActive = freezed,
    Object? notes = freezed,
    Object? createdBy = freezed,
    Object? createdDate = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      titleEnglish: titleEnglish == freezed
          ? _value.titleEnglish
          : titleEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      titleArabic: titleArabic == freezed
          ? _value.titleArabic
          : titleArabic // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: fromDate == freezed
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      toDate: toDate == freezed
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      sortOrder: sortOrder == freezed
          ? _value.sortOrder
          : sortOrder // ignore: cast_nullable_to_non_nullable
              as int,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      notes: notes == freezed
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      createdBy: createdBy == freezed
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
abstract class _$$_AdModelCopyWith<$Res> implements $AdModelCopyWith<$Res> {
  factory _$$_AdModelCopyWith(
          _$_AdModel value, $Res Function(_$_AdModel) then) =
      __$$_AdModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String titleEnglish,
      String titleArabic,
      String image,
      String url,
      DateTime fromDate,
      DateTime toDate,
      int sortOrder,
      bool isActive,
      String notes,
      String createdBy,
      DateTime createdDate});
}

/// @nodoc
class __$$_AdModelCopyWithImpl<$Res> extends _$AdModelCopyWithImpl<$Res>
    implements _$$_AdModelCopyWith<$Res> {
  __$$_AdModelCopyWithImpl(_$_AdModel _value, $Res Function(_$_AdModel) _then)
      : super(_value, (v) => _then(v as _$_AdModel));

  @override
  _$_AdModel get _value => super._value as _$_AdModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? titleEnglish = freezed,
    Object? titleArabic = freezed,
    Object? image = freezed,
    Object? url = freezed,
    Object? fromDate = freezed,
    Object? toDate = freezed,
    Object? sortOrder = freezed,
    Object? isActive = freezed,
    Object? notes = freezed,
    Object? createdBy = freezed,
    Object? createdDate = freezed,
  }) {
    return _then(_$_AdModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      titleEnglish: titleEnglish == freezed
          ? _value.titleEnglish
          : titleEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      titleArabic: titleArabic == freezed
          ? _value.titleArabic
          : titleArabic // ignore: cast_nullable_to_non_nullable
              as String,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      fromDate: fromDate == freezed
          ? _value.fromDate
          : fromDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      toDate: toDate == freezed
          ? _value.toDate
          : toDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
      sortOrder: sortOrder == freezed
          ? _value.sortOrder
          : sortOrder // ignore: cast_nullable_to_non_nullable
              as int,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      notes: notes == freezed
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      createdBy: createdBy == freezed
          ? _value.createdBy
          : createdBy // ignore: cast_nullable_to_non_nullable
              as String,
      createdDate: createdDate == freezed
          ? _value.createdDate
          : createdDate // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_AdModel implements _AdModel {
  _$_AdModel(
      {required this.id,
      required this.titleEnglish,
      required this.titleArabic,
      required this.image,
      required this.url,
      required this.fromDate,
      required this.toDate,
      required this.sortOrder,
      required this.isActive,
      required this.notes,
      required this.createdBy,
      required this.createdDate});

  factory _$_AdModel.fromJson(Map<String, dynamic> json) =>
      _$$_AdModelFromJson(json);

  @override
  final String id;
  @override
  final String titleEnglish;
  @override
  final String titleArabic;
  @override
  final String image;
  @override
  final String url;
  @override
  final DateTime fromDate;
  @override
  final DateTime toDate;
  @override
  final int sortOrder;
  @override
  final bool isActive;
  @override
  final String notes;
  @override
  final String createdBy;
  @override
  final DateTime createdDate;

  @override
  String toString() {
    return 'AdModel(id: $id, titleEnglish: $titleEnglish, titleArabic: $titleArabic, image: $image, url: $url, fromDate: $fromDate, toDate: $toDate, sortOrder: $sortOrder, isActive: $isActive, notes: $notes, createdBy: $createdBy, createdDate: $createdDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AdModel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.titleEnglish, titleEnglish) &&
            const DeepCollectionEquality()
                .equals(other.titleArabic, titleArabic) &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality().equals(other.url, url) &&
            const DeepCollectionEquality().equals(other.fromDate, fromDate) &&
            const DeepCollectionEquality().equals(other.toDate, toDate) &&
            const DeepCollectionEquality().equals(other.sortOrder, sortOrder) &&
            const DeepCollectionEquality().equals(other.isActive, isActive) &&
            const DeepCollectionEquality().equals(other.notes, notes) &&
            const DeepCollectionEquality().equals(other.createdBy, createdBy) &&
            const DeepCollectionEquality()
                .equals(other.createdDate, createdDate));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(titleEnglish),
      const DeepCollectionEquality().hash(titleArabic),
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(url),
      const DeepCollectionEquality().hash(fromDate),
      const DeepCollectionEquality().hash(toDate),
      const DeepCollectionEquality().hash(sortOrder),
      const DeepCollectionEquality().hash(isActive),
      const DeepCollectionEquality().hash(notes),
      const DeepCollectionEquality().hash(createdBy),
      const DeepCollectionEquality().hash(createdDate));

  @JsonKey(ignore: true)
  @override
  _$$_AdModelCopyWith<_$_AdModel> get copyWith =>
      __$$_AdModelCopyWithImpl<_$_AdModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AdModelToJson(
      this,
    );
  }
}

abstract class _AdModel implements AdModel {
  factory _AdModel(
      {required final String id,
      required final String titleEnglish,
      required final String titleArabic,
      required final String image,
      required final String url,
      required final DateTime fromDate,
      required final DateTime toDate,
      required final int sortOrder,
      required final bool isActive,
      required final String notes,
      required final String createdBy,
      required final DateTime createdDate}) = _$_AdModel;

  factory _AdModel.fromJson(Map<String, dynamic> json) = _$_AdModel.fromJson;

  @override
  String get id;
  @override
  String get titleEnglish;
  @override
  String get titleArabic;
  @override
  String get image;
  @override
  String get url;
  @override
  DateTime get fromDate;
  @override
  DateTime get toDate;
  @override
  int get sortOrder;
  @override
  bool get isActive;
  @override
  String get notes;
  @override
  String get createdBy;
  @override
  DateTime get createdDate;
  @override
  @JsonKey(ignore: true)
  _$$_AdModelCopyWith<_$_AdModel> get copyWith =>
      throw _privateConstructorUsedError;
}
