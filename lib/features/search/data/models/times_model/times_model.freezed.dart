// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'times_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TimesModel _$TimesModelFromJson(Map<String, dynamic> json) {
  return _TimesModel.fromJson(json);
}

/// @nodoc
mixin _$TimesModel {
  String get key => throw _privateConstructorUsedError;
  set key(String value) => throw _privateConstructorUsedError;
  String get value => throw _privateConstructorUsedError;
  set value(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  bool get activeItem => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  set activeItem(bool value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TimesModelCopyWith<TimesModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimesModelCopyWith<$Res> {
  factory $TimesModelCopyWith(
          TimesModel value, $Res Function(TimesModel) then) =
      _$TimesModelCopyWithImpl<$Res>;
  $Res call(
      {String key,
      String value,
      @JsonKey(defaultValue: false, nullable: true) bool activeItem});
}

/// @nodoc
class _$TimesModelCopyWithImpl<$Res> implements $TimesModelCopyWith<$Res> {
  _$TimesModelCopyWithImpl(this._value, this._then);

  final TimesModel _value;
  // ignore: unused_field
  final $Res Function(TimesModel) _then;

  @override
  $Res call({
    Object? key = freezed,
    Object? value = freezed,
    Object? activeItem = freezed,
  }) {
    return _then(_value.copyWith(
      key: key == freezed
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      activeItem: activeItem == freezed
          ? _value.activeItem
          : activeItem // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_TimesModelCopyWith<$Res>
    implements $TimesModelCopyWith<$Res> {
  factory _$$_TimesModelCopyWith(
          _$_TimesModel value, $Res Function(_$_TimesModel) then) =
      __$$_TimesModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String key,
      String value,
      @JsonKey(defaultValue: false, nullable: true) bool activeItem});
}

/// @nodoc
class __$$_TimesModelCopyWithImpl<$Res> extends _$TimesModelCopyWithImpl<$Res>
    implements _$$_TimesModelCopyWith<$Res> {
  __$$_TimesModelCopyWithImpl(
      _$_TimesModel _value, $Res Function(_$_TimesModel) _then)
      : super(_value, (v) => _then(v as _$_TimesModel));

  @override
  _$_TimesModel get _value => super._value as _$_TimesModel;

  @override
  $Res call({
    Object? key = freezed,
    Object? value = freezed,
    Object? activeItem = freezed,
  }) {
    return _then(_$_TimesModel(
      key: key == freezed
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      activeItem: activeItem == freezed
          ? _value.activeItem
          : activeItem // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_TimesModel implements _TimesModel {
  _$_TimesModel(
      {required this.key,
      required this.value,
      @JsonKey(defaultValue: false, nullable: true) required this.activeItem});

  factory _$_TimesModel.fromJson(Map<String, dynamic> json) =>
      _$$_TimesModelFromJson(json);

  @override
  String key;
  @override
  String value;
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool activeItem;

  @override
  String toString() {
    return 'TimesModel(key: $key, value: $value, activeItem: $activeItem)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_TimesModelCopyWith<_$_TimesModel> get copyWith =>
      __$$_TimesModelCopyWithImpl<_$_TimesModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TimesModelToJson(
      this,
    );
  }
}

abstract class _TimesModel implements TimesModel {
  factory _TimesModel(
      {required String key,
      required String value,
      @JsonKey(defaultValue: false, nullable: true)
          required bool activeItem}) = _$_TimesModel;

  factory _TimesModel.fromJson(Map<String, dynamic> json) =
      _$_TimesModel.fromJson;

  @override
  String get key;
  set key(String value);
  @override
  String get value;
  set value(String value);
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool get activeItem;
  @JsonKey(defaultValue: false, nullable: true)
  set activeItem(bool value);
  @override
  @JsonKey(ignore: true)
  _$$_TimesModelCopyWith<_$_TimesModel> get copyWith =>
      throw _privateConstructorUsedError;
}
