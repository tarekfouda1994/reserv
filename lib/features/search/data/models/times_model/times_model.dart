import 'package:freezed_annotation/freezed_annotation.dart';

part 'times_model.freezed.dart';
part 'times_model.g.dart';

@unfreezed
class TimesModel with _$TimesModel {
  @JsonSerializable(explicitToJson: true)
  factory TimesModel({
    required String key,
    required String value,
    @JsonKey(defaultValue: false, nullable: true) required bool activeItem,
  }) = _TimesModel;

  factory TimesModel.fromJson(Map<String, dynamic> json) => _$TimesModelFromJson(json);
}
