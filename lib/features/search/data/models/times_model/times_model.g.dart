// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'times_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TimesModel _$$_TimesModelFromJson(Map<String, dynamic> json) =>
    _$_TimesModel(
      key: json['key'] as String,
      value: json['value'] as String,
      activeItem: json['activeItem'] as bool? ?? false,
    );

Map<String, dynamic> _$$_TimesModelToJson(_$_TimesModel instance) =>
    <String, dynamic>{
      'key': instance.key,
      'value': instance.value,
      'activeItem': instance.activeItem,
    };
