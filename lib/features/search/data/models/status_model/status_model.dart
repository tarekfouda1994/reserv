import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'status_model.freezed.dart';
part 'status_model.g.dart';

@freezed
class StatusModel with _$StatusModel {
  StatusModel._();

  @JsonSerializable(explicitToJson: true)
  factory StatusModel({
    @JsonKey(name: "englishName", defaultValue: "") required String text,
    @JsonKey(name: "arabicName", defaultValue: "") required String textAR,
    @JsonKey(name: "id", defaultValue: 0) required int value,
  }) = _StatusModel;

  factory StatusModel.fromJson(Map<String, dynamic> json) => _$StatusModelFromJson(json);

  String getStatus() {
    return getIt<Utilities>().getLocalizedValue(textAR, text);
  }
}
