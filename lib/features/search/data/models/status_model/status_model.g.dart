// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StatusModel _$$_StatusModelFromJson(Map<String, dynamic> json) =>
    _$_StatusModel(
      text: json['englishName'] as String? ?? '',
      textAR: json['arabicName'] as String? ?? '',
      value: json['id'] as int? ?? 0,
    );

Map<String, dynamic> _$$_StatusModelToJson(_$_StatusModel instance) =>
    <String, dynamic>{
      'englishName': instance.text,
      'arabicName': instance.textAR,
      'id': instance.value,
    };
