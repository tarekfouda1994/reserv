// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'status_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StatusModel _$StatusModelFromJson(Map<String, dynamic> json) {
  return _StatusModel.fromJson(json);
}

/// @nodoc
mixin _$StatusModel {
  @JsonKey(name: "englishName", defaultValue: "")
  String get text => throw _privateConstructorUsedError;
  @JsonKey(name: "arabicName", defaultValue: "")
  String get textAR => throw _privateConstructorUsedError;
  @JsonKey(name: "id", defaultValue: 0)
  int get value => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StatusModelCopyWith<StatusModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StatusModelCopyWith<$Res> {
  factory $StatusModelCopyWith(
          StatusModel value, $Res Function(StatusModel) then) =
      _$StatusModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: "englishName", defaultValue: "") String text,
      @JsonKey(name: "arabicName", defaultValue: "") String textAR,
      @JsonKey(name: "id", defaultValue: 0) int value});
}

/// @nodoc
class _$StatusModelCopyWithImpl<$Res> implements $StatusModelCopyWith<$Res> {
  _$StatusModelCopyWithImpl(this._value, this._then);

  final StatusModel _value;
  // ignore: unused_field
  final $Res Function(StatusModel) _then;

  @override
  $Res call({
    Object? text = freezed,
    Object? textAR = freezed,
    Object? value = freezed,
  }) {
    return _then(_value.copyWith(
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      textAR: textAR == freezed
          ? _value.textAR
          : textAR // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_StatusModelCopyWith<$Res>
    implements $StatusModelCopyWith<$Res> {
  factory _$$_StatusModelCopyWith(
          _$_StatusModel value, $Res Function(_$_StatusModel) then) =
      __$$_StatusModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: "englishName", defaultValue: "") String text,
      @JsonKey(name: "arabicName", defaultValue: "") String textAR,
      @JsonKey(name: "id", defaultValue: 0) int value});
}

/// @nodoc
class __$$_StatusModelCopyWithImpl<$Res> extends _$StatusModelCopyWithImpl<$Res>
    implements _$$_StatusModelCopyWith<$Res> {
  __$$_StatusModelCopyWithImpl(
      _$_StatusModel _value, $Res Function(_$_StatusModel) _then)
      : super(_value, (v) => _then(v as _$_StatusModel));

  @override
  _$_StatusModel get _value => super._value as _$_StatusModel;

  @override
  $Res call({
    Object? text = freezed,
    Object? textAR = freezed,
    Object? value = freezed,
  }) {
    return _then(_$_StatusModel(
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      textAR: textAR == freezed
          ? _value.textAR
          : textAR // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_StatusModel extends _StatusModel {
  _$_StatusModel(
      {@JsonKey(name: "englishName", defaultValue: "") required this.text,
      @JsonKey(name: "arabicName", defaultValue: "") required this.textAR,
      @JsonKey(name: "id", defaultValue: 0) required this.value})
      : super._();

  factory _$_StatusModel.fromJson(Map<String, dynamic> json) =>
      _$$_StatusModelFromJson(json);

  @override
  @JsonKey(name: "englishName", defaultValue: "")
  final String text;
  @override
  @JsonKey(name: "arabicName", defaultValue: "")
  final String textAR;
  @override
  @JsonKey(name: "id", defaultValue: 0)
  final int value;

  @override
  String toString() {
    return 'StatusModel(text: $text, textAR: $textAR, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StatusModel &&
            const DeepCollectionEquality().equals(other.text, text) &&
            const DeepCollectionEquality().equals(other.textAR, textAR) &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(text),
      const DeepCollectionEquality().hash(textAR),
      const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  _$$_StatusModelCopyWith<_$_StatusModel> get copyWith =>
      __$$_StatusModelCopyWithImpl<_$_StatusModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StatusModelToJson(
      this,
    );
  }
}

abstract class _StatusModel extends StatusModel {
  factory _StatusModel(
      {@JsonKey(name: "englishName", defaultValue: "")
          required final String text,
      @JsonKey(name: "arabicName", defaultValue: "")
          required final String textAR,
      @JsonKey(name: "id", defaultValue: 0)
          required final int value}) = _$_StatusModel;
  _StatusModel._() : super._();

  factory _StatusModel.fromJson(Map<String, dynamic> json) =
      _$_StatusModel.fromJson;

  @override
  @JsonKey(name: "englishName", defaultValue: "")
  String get text;
  @override
  @JsonKey(name: "arabicName", defaultValue: "")
  String get textAR;
  @override
  @JsonKey(name: "id", defaultValue: 0)
  int get value;
  @override
  @JsonKey(ignore: true)
  _$$_StatusModelCopyWith<_$_StatusModel> get copyWith =>
      throw _privateConstructorUsedError;
}
