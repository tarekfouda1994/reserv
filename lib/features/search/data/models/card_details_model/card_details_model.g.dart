// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CardDetailsModel _$$_CardDetailsModelFromJson(Map<String, dynamic> json) =>
    _$_CardDetailsModel(
      id: json['id'] as String,
      holderName: json['holderName'] as String,
      cardNumber: json['cardNumber'] as String,
      expiryDate: json['expiryDate'] as String,
      cvv: json['cvv'] as String,
      isDefault: json['isDefault'] as bool,
    );

Map<String, dynamic> _$$_CardDetailsModelToJson(_$_CardDetailsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'holderName': instance.holderName,
      'cardNumber': instance.cardNumber,
      'expiryDate': instance.expiryDate,
      'cvv': instance.cvv,
      'isDefault': instance.isDefault,
    };
