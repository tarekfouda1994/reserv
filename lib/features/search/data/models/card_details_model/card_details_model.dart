import 'package:freezed_annotation/freezed_annotation.dart';

part 'card_details_model.freezed.dart';
part 'card_details_model.g.dart';

@freezed
class CardDetailsModel with _$CardDetailsModel {
  @JsonSerializable(explicitToJson: true)
  factory CardDetailsModel({
    required String id,
    required String holderName,
    required String cardNumber,
    required String expiryDate,
    required String cvv,
    required bool isDefault,
  }) = _CardDetailsModel;

  factory CardDetailsModel.fromJson(Map<String, dynamic> json) => _$CardDetailsModelFromJson(json);
}
