// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'card_details_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CardDetailsModel _$CardDetailsModelFromJson(Map<String, dynamic> json) {
  return _CardDetailsModel.fromJson(json);
}

/// @nodoc
mixin _$CardDetailsModel {
  String get id => throw _privateConstructorUsedError;
  String get holderName => throw _privateConstructorUsedError;
  String get cardNumber => throw _privateConstructorUsedError;
  String get expiryDate => throw _privateConstructorUsedError;
  String get cvv => throw _privateConstructorUsedError;
  bool get isDefault => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CardDetailsModelCopyWith<CardDetailsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CardDetailsModelCopyWith<$Res> {
  factory $CardDetailsModelCopyWith(
          CardDetailsModel value, $Res Function(CardDetailsModel) then) =
      _$CardDetailsModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String holderName,
      String cardNumber,
      String expiryDate,
      String cvv,
      bool isDefault});
}

/// @nodoc
class _$CardDetailsModelCopyWithImpl<$Res>
    implements $CardDetailsModelCopyWith<$Res> {
  _$CardDetailsModelCopyWithImpl(this._value, this._then);

  final CardDetailsModel _value;
  // ignore: unused_field
  final $Res Function(CardDetailsModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? holderName = freezed,
    Object? cardNumber = freezed,
    Object? expiryDate = freezed,
    Object? cvv = freezed,
    Object? isDefault = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      holderName: holderName == freezed
          ? _value.holderName
          : holderName // ignore: cast_nullable_to_non_nullable
              as String,
      cardNumber: cardNumber == freezed
          ? _value.cardNumber
          : cardNumber // ignore: cast_nullable_to_non_nullable
              as String,
      expiryDate: expiryDate == freezed
          ? _value.expiryDate
          : expiryDate // ignore: cast_nullable_to_non_nullable
              as String,
      cvv: cvv == freezed
          ? _value.cvv
          : cvv // ignore: cast_nullable_to_non_nullable
              as String,
      isDefault: isDefault == freezed
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_CardDetailsModelCopyWith<$Res>
    implements $CardDetailsModelCopyWith<$Res> {
  factory _$$_CardDetailsModelCopyWith(
          _$_CardDetailsModel value, $Res Function(_$_CardDetailsModel) then) =
      __$$_CardDetailsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String holderName,
      String cardNumber,
      String expiryDate,
      String cvv,
      bool isDefault});
}

/// @nodoc
class __$$_CardDetailsModelCopyWithImpl<$Res>
    extends _$CardDetailsModelCopyWithImpl<$Res>
    implements _$$_CardDetailsModelCopyWith<$Res> {
  __$$_CardDetailsModelCopyWithImpl(
      _$_CardDetailsModel _value, $Res Function(_$_CardDetailsModel) _then)
      : super(_value, (v) => _then(v as _$_CardDetailsModel));

  @override
  _$_CardDetailsModel get _value => super._value as _$_CardDetailsModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? holderName = freezed,
    Object? cardNumber = freezed,
    Object? expiryDate = freezed,
    Object? cvv = freezed,
    Object? isDefault = freezed,
  }) {
    return _then(_$_CardDetailsModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      holderName: holderName == freezed
          ? _value.holderName
          : holderName // ignore: cast_nullable_to_non_nullable
              as String,
      cardNumber: cardNumber == freezed
          ? _value.cardNumber
          : cardNumber // ignore: cast_nullable_to_non_nullable
              as String,
      expiryDate: expiryDate == freezed
          ? _value.expiryDate
          : expiryDate // ignore: cast_nullable_to_non_nullable
              as String,
      cvv: cvv == freezed
          ? _value.cvv
          : cvv // ignore: cast_nullable_to_non_nullable
              as String,
      isDefault: isDefault == freezed
          ? _value.isDefault
          : isDefault // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_CardDetailsModel implements _CardDetailsModel {
  _$_CardDetailsModel(
      {required this.id,
      required this.holderName,
      required this.cardNumber,
      required this.expiryDate,
      required this.cvv,
      required this.isDefault});

  factory _$_CardDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$$_CardDetailsModelFromJson(json);

  @override
  final String id;
  @override
  final String holderName;
  @override
  final String cardNumber;
  @override
  final String expiryDate;
  @override
  final String cvv;
  @override
  final bool isDefault;

  @override
  String toString() {
    return 'CardDetailsModel(id: $id, holderName: $holderName, cardNumber: $cardNumber, expiryDate: $expiryDate, cvv: $cvv, isDefault: $isDefault)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CardDetailsModel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.holderName, holderName) &&
            const DeepCollectionEquality()
                .equals(other.cardNumber, cardNumber) &&
            const DeepCollectionEquality()
                .equals(other.expiryDate, expiryDate) &&
            const DeepCollectionEquality().equals(other.cvv, cvv) &&
            const DeepCollectionEquality().equals(other.isDefault, isDefault));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(holderName),
      const DeepCollectionEquality().hash(cardNumber),
      const DeepCollectionEquality().hash(expiryDate),
      const DeepCollectionEquality().hash(cvv),
      const DeepCollectionEquality().hash(isDefault));

  @JsonKey(ignore: true)
  @override
  _$$_CardDetailsModelCopyWith<_$_CardDetailsModel> get copyWith =>
      __$$_CardDetailsModelCopyWithImpl<_$_CardDetailsModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CardDetailsModelToJson(
      this,
    );
  }
}

abstract class _CardDetailsModel implements CardDetailsModel {
  factory _CardDetailsModel(
      {required final String id,
      required final String holderName,
      required final String cardNumber,
      required final String expiryDate,
      required final String cvv,
      required final bool isDefault}) = _$_CardDetailsModel;

  factory _CardDetailsModel.fromJson(Map<String, dynamic> json) =
      _$_CardDetailsModel.fromJson;

  @override
  String get id;
  @override
  String get holderName;
  @override
  String get cardNumber;
  @override
  String get expiryDate;
  @override
  String get cvv;
  @override
  bool get isDefault;
  @override
  @JsonKey(ignore: true)
  _$$_CardDetailsModelCopyWith<_$_CardDetailsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
