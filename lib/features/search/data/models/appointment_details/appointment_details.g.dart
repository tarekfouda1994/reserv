// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppointmentDetails _$$_AppointmentDetailsFromJson(
        Map<String, dynamic> json) =>
    _$_AppointmentDetails(
      id: json['id'] as String,
      firstName: json['nameEN'] as String,
      lastName: json['nameAR'] as String,
      distanceText: json['distance'] as String,
      businessGender: json['businessGender'] as String,
      bookingDateTime: DateTime.parse(json['bookingDateTime'] as String),
      promoCode: json['promoCode'] as String? ?? '',
      rate: (json['businessRating'] as num).toDouble(),
      netTotAmount: (json['netTotalAmount'] as num).toDouble(),
      totalDiscAmount: (json['totalDiscountAmount'] as num).toDouble(),
      totAmount: (json['totalAmount'] as num).toDouble(),
      contactNumber: json['contactNumber'] as String? ?? '',
      email: json['email'] as String,
      orderNumber: json['orderNumber'] as String,
      currencyEN: json['currencyEN'] as String? ?? '',
      currencyAR: json['currencyAR'] as String? ?? '',
      appointmentType: json['appointmentType'] as int,
      manageVisitListItems: (json['manageVisitListItems'] as List<dynamic>)
          .map((e) => AppontmentServices.fromJson(e as Map<String, dynamic>))
          .toList(),
      internalNote: json['internalNote'] as String? ?? '',
      messageToClient: json['messageToClient'] as String? ?? '',
      avatar: json['avatar'] as String? ?? '',
      isSuccess: json['isSuccess'] as bool,
      message: json['message'] as String,
      businessID: json['businessID'] as String,
      detail: json['detail'] as String,
      cardDetails: json['cardDetails'] == null
          ? null
          : CardDetailsModel.fromJson(
              json['cardDetails'] as Map<String, dynamic>),
      bookingOrderGift: json['bookingOrderGift'] == null
          ? null
          : OrderGiftModel.fromJson(
              json['bookingOrderGift'] as Map<String, dynamic>),
      noteListItems: (json['noteListItems'] as List<dynamic>)
          .map((e) => NotesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AppointmentDetailsToJson(
        _$_AppointmentDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nameEN': instance.firstName,
      'nameAR': instance.lastName,
      'distance': instance.distanceText,
      'businessGender': instance.businessGender,
      'bookingDateTime': instance.bookingDateTime.toIso8601String(),
      'promoCode': instance.promoCode,
      'businessRating': instance.rate,
      'netTotalAmount': instance.netTotAmount,
      'totalDiscountAmount': instance.totalDiscAmount,
      'totalAmount': instance.totAmount,
      'contactNumber': instance.contactNumber,
      'email': instance.email,
      'orderNumber': instance.orderNumber,
      'currencyEN': instance.currencyEN,
      'currencyAR': instance.currencyAR,
      'appointmentType': instance.appointmentType,
      'manageVisitListItems':
          instance.manageVisitListItems.map((e) => e.toJson()).toList(),
      'internalNote': instance.internalNote,
      'messageToClient': instance.messageToClient,
      'avatar': instance.avatar,
      'isSuccess': instance.isSuccess,
      'message': instance.message,
      'businessID': instance.businessID,
      'detail': instance.detail,
      'cardDetails': instance.cardDetails?.toJson(),
      'bookingOrderGift': instance.bookingOrderGift?.toJson(),
      'noteListItems': instance.noteListItems.map((e) => e.toJson()).toList(),
    };
