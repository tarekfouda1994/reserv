// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'appointment_details.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppointmentDetails _$AppointmentDetailsFromJson(Map<String, dynamic> json) {
  return _AppointmentDetails.fromJson(json);
}

/// @nodoc
mixin _$AppointmentDetails {
  String get id => throw _privateConstructorUsedError;
  set id(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "nameEN")
  String get firstName => throw _privateConstructorUsedError;
  @JsonKey(name: "nameEN")
  set firstName(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "nameAR")
  String get lastName => throw _privateConstructorUsedError;
  @JsonKey(name: "nameAR")
  set lastName(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  String get distanceText => throw _privateConstructorUsedError;
  @JsonKey(name: "distance")
  set distanceText(String value) => throw _privateConstructorUsedError;
  String get businessGender => throw _privateConstructorUsedError;
  set businessGender(String value) => throw _privateConstructorUsedError;
  DateTime get bookingDateTime => throw _privateConstructorUsedError;
  set bookingDateTime(DateTime value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get promoCode => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set promoCode(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "businessRating")
  double get rate => throw _privateConstructorUsedError;
  @JsonKey(name: "businessRating")
  set rate(double value) => throw _privateConstructorUsedError;
  @JsonKey(name: "netTotalAmount")
  double get netTotAmount => throw _privateConstructorUsedError;
  @JsonKey(name: "netTotalAmount")
  set netTotAmount(double value) => throw _privateConstructorUsedError;
  @JsonKey(name: "totalDiscountAmount")
  double get totalDiscAmount => throw _privateConstructorUsedError;
  @JsonKey(name: "totalDiscountAmount")
  set totalDiscAmount(double value) => throw _privateConstructorUsedError;
  @JsonKey(name: "totalAmount")
  double get totAmount => throw _privateConstructorUsedError;
  @JsonKey(name: "totalAmount")
  set totAmount(double value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get contactNumber => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set contactNumber(String value) => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  set email(String value) => throw _privateConstructorUsedError;
  String get orderNumber => throw _privateConstructorUsedError;
  set orderNumber(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyEN => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyEN(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyAR => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyAR(String value) => throw _privateConstructorUsedError;
  int get appointmentType => throw _privateConstructorUsedError;
  set appointmentType(int value) => throw _privateConstructorUsedError;
  List<AppontmentServices> get manageVisitListItems =>
      throw _privateConstructorUsedError;
  set manageVisitListItems(List<AppontmentServices> value) =>
      throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get internalNote => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set internalNote(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get messageToClient => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set messageToClient(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get avatar => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set avatar(String value) => throw _privateConstructorUsedError;
  bool get isSuccess => throw _privateConstructorUsedError;
  set isSuccess(bool value) => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;
  set message(String value) => throw _privateConstructorUsedError;
  String get businessID => throw _privateConstructorUsedError;
  set businessID(String value) => throw _privateConstructorUsedError;
  String get detail => throw _privateConstructorUsedError;
  set detail(String value) => throw _privateConstructorUsedError;
  CardDetailsModel? get cardDetails => throw _privateConstructorUsedError;
  set cardDetails(CardDetailsModel? value) =>
      throw _privateConstructorUsedError;
  OrderGiftModel? get bookingOrderGift => throw _privateConstructorUsedError;
  set bookingOrderGift(OrderGiftModel? value) =>
      throw _privateConstructorUsedError;
  List<NotesModel> get noteListItems => throw _privateConstructorUsedError;
  set noteListItems(List<NotesModel> value) =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppointmentDetailsCopyWith<AppointmentDetails> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppointmentDetailsCopyWith<$Res> {
  factory $AppointmentDetailsCopyWith(
          AppointmentDetails value, $Res Function(AppointmentDetails) then) =
      _$AppointmentDetailsCopyWithImpl<$Res>;
  $Res call(
      {String id,
      @JsonKey(name: "nameEN") String firstName,
      @JsonKey(name: "nameAR") String lastName,
      @JsonKey(name: "distance") String distanceText,
      String businessGender,
      DateTime bookingDateTime,
      @JsonKey(nullable: true, defaultValue: "") String promoCode,
      @JsonKey(name: "businessRating") double rate,
      @JsonKey(name: "netTotalAmount") double netTotAmount,
      @JsonKey(name: "totalDiscountAmount") double totalDiscAmount,
      @JsonKey(name: "totalAmount") double totAmount,
      @JsonKey(nullable: true, defaultValue: "") String contactNumber,
      String email,
      String orderNumber,
      @JsonKey(nullable: true, defaultValue: "") String currencyEN,
      @JsonKey(nullable: true, defaultValue: "") String currencyAR,
      int appointmentType,
      List<AppontmentServices> manageVisitListItems,
      @JsonKey(nullable: true, defaultValue: "") String internalNote,
      @JsonKey(nullable: true, defaultValue: "") String messageToClient,
      @JsonKey(nullable: true, defaultValue: "") String avatar,
      bool isSuccess,
      String message,
      String businessID,
      String detail,
      CardDetailsModel? cardDetails,
      OrderGiftModel? bookingOrderGift,
      List<NotesModel> noteListItems});

  $CardDetailsModelCopyWith<$Res>? get cardDetails;
  $OrderGiftModelCopyWith<$Res>? get bookingOrderGift;
}

/// @nodoc
class _$AppointmentDetailsCopyWithImpl<$Res>
    implements $AppointmentDetailsCopyWith<$Res> {
  _$AppointmentDetailsCopyWithImpl(this._value, this._then);

  final AppointmentDetails _value;
  // ignore: unused_field
  final $Res Function(AppointmentDetails) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? distanceText = freezed,
    Object? businessGender = freezed,
    Object? bookingDateTime = freezed,
    Object? promoCode = freezed,
    Object? rate = freezed,
    Object? netTotAmount = freezed,
    Object? totalDiscAmount = freezed,
    Object? totAmount = freezed,
    Object? contactNumber = freezed,
    Object? email = freezed,
    Object? orderNumber = freezed,
    Object? currencyEN = freezed,
    Object? currencyAR = freezed,
    Object? appointmentType = freezed,
    Object? manageVisitListItems = freezed,
    Object? internalNote = freezed,
    Object? messageToClient = freezed,
    Object? avatar = freezed,
    Object? isSuccess = freezed,
    Object? message = freezed,
    Object? businessID = freezed,
    Object? detail = freezed,
    Object? cardDetails = freezed,
    Object? bookingOrderGift = freezed,
    Object? noteListItems = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      bookingDateTime: bookingDateTime == freezed
          ? _value.bookingDateTime
          : bookingDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      promoCode: promoCode == freezed
          ? _value.promoCode
          : promoCode // ignore: cast_nullable_to_non_nullable
              as String,
      rate: rate == freezed
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as double,
      netTotAmount: netTotAmount == freezed
          ? _value.netTotAmount
          : netTotAmount // ignore: cast_nullable_to_non_nullable
              as double,
      totalDiscAmount: totalDiscAmount == freezed
          ? _value.totalDiscAmount
          : totalDiscAmount // ignore: cast_nullable_to_non_nullable
              as double,
      totAmount: totAmount == freezed
          ? _value.totAmount
          : totAmount // ignore: cast_nullable_to_non_nullable
              as double,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
      currencyEN: currencyEN == freezed
          ? _value.currencyEN
          : currencyEN // ignore: cast_nullable_to_non_nullable
              as String,
      currencyAR: currencyAR == freezed
          ? _value.currencyAR
          : currencyAR // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentType: appointmentType == freezed
          ? _value.appointmentType
          : appointmentType // ignore: cast_nullable_to_non_nullable
              as int,
      manageVisitListItems: manageVisitListItems == freezed
          ? _value.manageVisitListItems
          : manageVisitListItems // ignore: cast_nullable_to_non_nullable
              as List<AppontmentServices>,
      internalNote: internalNote == freezed
          ? _value.internalNote
          : internalNote // ignore: cast_nullable_to_non_nullable
              as String,
      messageToClient: messageToClient == freezed
          ? _value.messageToClient
          : messageToClient // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      isSuccess: isSuccess == freezed
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      detail: detail == freezed
          ? _value.detail
          : detail // ignore: cast_nullable_to_non_nullable
              as String,
      cardDetails: cardDetails == freezed
          ? _value.cardDetails
          : cardDetails // ignore: cast_nullable_to_non_nullable
              as CardDetailsModel?,
      bookingOrderGift: bookingOrderGift == freezed
          ? _value.bookingOrderGift
          : bookingOrderGift // ignore: cast_nullable_to_non_nullable
              as OrderGiftModel?,
      noteListItems: noteListItems == freezed
          ? _value.noteListItems
          : noteListItems // ignore: cast_nullable_to_non_nullable
              as List<NotesModel>,
    ));
  }

  @override
  $CardDetailsModelCopyWith<$Res>? get cardDetails {
    if (_value.cardDetails == null) {
      return null;
    }

    return $CardDetailsModelCopyWith<$Res>(_value.cardDetails!, (value) {
      return _then(_value.copyWith(cardDetails: value));
    });
  }

  @override
  $OrderGiftModelCopyWith<$Res>? get bookingOrderGift {
    if (_value.bookingOrderGift == null) {
      return null;
    }

    return $OrderGiftModelCopyWith<$Res>(_value.bookingOrderGift!, (value) {
      return _then(_value.copyWith(bookingOrderGift: value));
    });
  }
}

/// @nodoc
abstract class _$$_AppointmentDetailsCopyWith<$Res>
    implements $AppointmentDetailsCopyWith<$Res> {
  factory _$$_AppointmentDetailsCopyWith(_$_AppointmentDetails value,
          $Res Function(_$_AppointmentDetails) then) =
      __$$_AppointmentDetailsCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      @JsonKey(name: "nameEN") String firstName,
      @JsonKey(name: "nameAR") String lastName,
      @JsonKey(name: "distance") String distanceText,
      String businessGender,
      DateTime bookingDateTime,
      @JsonKey(nullable: true, defaultValue: "") String promoCode,
      @JsonKey(name: "businessRating") double rate,
      @JsonKey(name: "netTotalAmount") double netTotAmount,
      @JsonKey(name: "totalDiscountAmount") double totalDiscAmount,
      @JsonKey(name: "totalAmount") double totAmount,
      @JsonKey(nullable: true, defaultValue: "") String contactNumber,
      String email,
      String orderNumber,
      @JsonKey(nullable: true, defaultValue: "") String currencyEN,
      @JsonKey(nullable: true, defaultValue: "") String currencyAR,
      int appointmentType,
      List<AppontmentServices> manageVisitListItems,
      @JsonKey(nullable: true, defaultValue: "") String internalNote,
      @JsonKey(nullable: true, defaultValue: "") String messageToClient,
      @JsonKey(nullable: true, defaultValue: "") String avatar,
      bool isSuccess,
      String message,
      String businessID,
      String detail,
      CardDetailsModel? cardDetails,
      OrderGiftModel? bookingOrderGift,
      List<NotesModel> noteListItems});

  @override
  $CardDetailsModelCopyWith<$Res>? get cardDetails;
  @override
  $OrderGiftModelCopyWith<$Res>? get bookingOrderGift;
}

/// @nodoc
class __$$_AppointmentDetailsCopyWithImpl<$Res>
    extends _$AppointmentDetailsCopyWithImpl<$Res>
    implements _$$_AppointmentDetailsCopyWith<$Res> {
  __$$_AppointmentDetailsCopyWithImpl(
      _$_AppointmentDetails _value, $Res Function(_$_AppointmentDetails) _then)
      : super(_value, (v) => _then(v as _$_AppointmentDetails));

  @override
  _$_AppointmentDetails get _value => super._value as _$_AppointmentDetails;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? distanceText = freezed,
    Object? businessGender = freezed,
    Object? bookingDateTime = freezed,
    Object? promoCode = freezed,
    Object? rate = freezed,
    Object? netTotAmount = freezed,
    Object? totalDiscAmount = freezed,
    Object? totAmount = freezed,
    Object? contactNumber = freezed,
    Object? email = freezed,
    Object? orderNumber = freezed,
    Object? currencyEN = freezed,
    Object? currencyAR = freezed,
    Object? appointmentType = freezed,
    Object? manageVisitListItems = freezed,
    Object? internalNote = freezed,
    Object? messageToClient = freezed,
    Object? avatar = freezed,
    Object? isSuccess = freezed,
    Object? message = freezed,
    Object? businessID = freezed,
    Object? detail = freezed,
    Object? cardDetails = freezed,
    Object? bookingOrderGift = freezed,
    Object? noteListItems = freezed,
  }) {
    return _then(_$_AppointmentDetails(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      bookingDateTime: bookingDateTime == freezed
          ? _value.bookingDateTime
          : bookingDateTime // ignore: cast_nullable_to_non_nullable
              as DateTime,
      promoCode: promoCode == freezed
          ? _value.promoCode
          : promoCode // ignore: cast_nullable_to_non_nullable
              as String,
      rate: rate == freezed
          ? _value.rate
          : rate // ignore: cast_nullable_to_non_nullable
              as double,
      netTotAmount: netTotAmount == freezed
          ? _value.netTotAmount
          : netTotAmount // ignore: cast_nullable_to_non_nullable
              as double,
      totalDiscAmount: totalDiscAmount == freezed
          ? _value.totalDiscAmount
          : totalDiscAmount // ignore: cast_nullable_to_non_nullable
              as double,
      totAmount: totAmount == freezed
          ? _value.totAmount
          : totAmount // ignore: cast_nullable_to_non_nullable
              as double,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
      currencyEN: currencyEN == freezed
          ? _value.currencyEN
          : currencyEN // ignore: cast_nullable_to_non_nullable
              as String,
      currencyAR: currencyAR == freezed
          ? _value.currencyAR
          : currencyAR // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentType: appointmentType == freezed
          ? _value.appointmentType
          : appointmentType // ignore: cast_nullable_to_non_nullable
              as int,
      manageVisitListItems: manageVisitListItems == freezed
          ? _value.manageVisitListItems
          : manageVisitListItems // ignore: cast_nullable_to_non_nullable
              as List<AppontmentServices>,
      internalNote: internalNote == freezed
          ? _value.internalNote
          : internalNote // ignore: cast_nullable_to_non_nullable
              as String,
      messageToClient: messageToClient == freezed
          ? _value.messageToClient
          : messageToClient // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      isSuccess: isSuccess == freezed
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      detail: detail == freezed
          ? _value.detail
          : detail // ignore: cast_nullable_to_non_nullable
              as String,
      cardDetails: cardDetails == freezed
          ? _value.cardDetails
          : cardDetails // ignore: cast_nullable_to_non_nullable
              as CardDetailsModel?,
      bookingOrderGift: bookingOrderGift == freezed
          ? _value.bookingOrderGift
          : bookingOrderGift // ignore: cast_nullable_to_non_nullable
              as OrderGiftModel?,
      noteListItems: noteListItems == freezed
          ? _value.noteListItems
          : noteListItems // ignore: cast_nullable_to_non_nullable
              as List<NotesModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_AppointmentDetails extends _AppointmentDetails {
  _$_AppointmentDetails(
      {required this.id,
      @JsonKey(name: "nameEN") required this.firstName,
      @JsonKey(name: "nameAR") required this.lastName,
      @JsonKey(name: "distance") required this.distanceText,
      required this.businessGender,
      required this.bookingDateTime,
      @JsonKey(nullable: true, defaultValue: "") required this.promoCode,
      @JsonKey(name: "businessRating") required this.rate,
      @JsonKey(name: "netTotalAmount") required this.netTotAmount,
      @JsonKey(name: "totalDiscountAmount") required this.totalDiscAmount,
      @JsonKey(name: "totalAmount") required this.totAmount,
      @JsonKey(nullable: true, defaultValue: "") required this.contactNumber,
      required this.email,
      required this.orderNumber,
      @JsonKey(nullable: true, defaultValue: "") required this.currencyEN,
      @JsonKey(nullable: true, defaultValue: "") required this.currencyAR,
      required this.appointmentType,
      required this.manageVisitListItems,
      @JsonKey(nullable: true, defaultValue: "") required this.internalNote,
      @JsonKey(nullable: true, defaultValue: "") required this.messageToClient,
      @JsonKey(nullable: true, defaultValue: "") required this.avatar,
      required this.isSuccess,
      required this.message,
      required this.businessID,
      required this.detail,
      required this.cardDetails,
      required this.bookingOrderGift,
      required this.noteListItems})
      : super._();

  factory _$_AppointmentDetails.fromJson(Map<String, dynamic> json) =>
      _$$_AppointmentDetailsFromJson(json);

  @override
  String id;
  @override
  @JsonKey(name: "nameEN")
  String firstName;
  @override
  @JsonKey(name: "nameAR")
  String lastName;
  @override
  @JsonKey(name: "distance")
  String distanceText;
  @override
  String businessGender;
  @override
  DateTime bookingDateTime;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String promoCode;
  @override
  @JsonKey(name: "businessRating")
  double rate;
  @override
  @JsonKey(name: "netTotalAmount")
  double netTotAmount;
  @override
  @JsonKey(name: "totalDiscountAmount")
  double totalDiscAmount;
  @override
  @JsonKey(name: "totalAmount")
  double totAmount;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String contactNumber;
  @override
  String email;
  @override
  String orderNumber;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String currencyEN;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String currencyAR;
  @override
  int appointmentType;
  @override
  List<AppontmentServices> manageVisitListItems;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String internalNote;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String messageToClient;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String avatar;
  @override
  bool isSuccess;
  @override
  String message;
  @override
  String businessID;
  @override
  String detail;
  @override
  CardDetailsModel? cardDetails;
  @override
  OrderGiftModel? bookingOrderGift;
  @override
  List<NotesModel> noteListItems;

  @override
  String toString() {
    return 'AppointmentDetails(id: $id, firstName: $firstName, lastName: $lastName, distanceText: $distanceText, businessGender: $businessGender, bookingDateTime: $bookingDateTime, promoCode: $promoCode, rate: $rate, netTotAmount: $netTotAmount, totalDiscAmount: $totalDiscAmount, totAmount: $totAmount, contactNumber: $contactNumber, email: $email, orderNumber: $orderNumber, currencyEN: $currencyEN, currencyAR: $currencyAR, appointmentType: $appointmentType, manageVisitListItems: $manageVisitListItems, internalNote: $internalNote, messageToClient: $messageToClient, avatar: $avatar, isSuccess: $isSuccess, message: $message, businessID: $businessID, detail: $detail, cardDetails: $cardDetails, bookingOrderGift: $bookingOrderGift, noteListItems: $noteListItems)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_AppointmentDetailsCopyWith<_$_AppointmentDetails> get copyWith =>
      __$$_AppointmentDetailsCopyWithImpl<_$_AppointmentDetails>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppointmentDetailsToJson(
      this,
    );
  }
}

abstract class _AppointmentDetails extends AppointmentDetails {
  factory _AppointmentDetails(
      {required String id,
      @JsonKey(name: "nameEN")
          required String firstName,
      @JsonKey(name: "nameAR")
          required String lastName,
      @JsonKey(name: "distance")
          required String distanceText,
      required String businessGender,
      required DateTime bookingDateTime,
      @JsonKey(nullable: true, defaultValue: "")
          required String promoCode,
      @JsonKey(name: "businessRating")
          required double rate,
      @JsonKey(name: "netTotalAmount")
          required double netTotAmount,
      @JsonKey(name: "totalDiscountAmount")
          required double totalDiscAmount,
      @JsonKey(name: "totalAmount")
          required double totAmount,
      @JsonKey(nullable: true, defaultValue: "")
          required String contactNumber,
      required String email,
      required String orderNumber,
      @JsonKey(nullable: true, defaultValue: "")
          required String currencyEN,
      @JsonKey(nullable: true, defaultValue: "")
          required String currencyAR,
      required int appointmentType,
      required List<AppontmentServices> manageVisitListItems,
      @JsonKey(nullable: true, defaultValue: "")
          required String internalNote,
      @JsonKey(nullable: true, defaultValue: "")
          required String messageToClient,
      @JsonKey(nullable: true, defaultValue: "")
          required String avatar,
      required bool isSuccess,
      required String message,
      required String businessID,
      required String detail,
      required CardDetailsModel? cardDetails,
      required OrderGiftModel? bookingOrderGift,
      required List<NotesModel> noteListItems}) = _$_AppointmentDetails;
  _AppointmentDetails._() : super._();

  factory _AppointmentDetails.fromJson(Map<String, dynamic> json) =
      _$_AppointmentDetails.fromJson;

  @override
  String get id;
  set id(String value);
  @override
  @JsonKey(name: "nameEN")
  String get firstName;
  @JsonKey(name: "nameEN")
  set firstName(String value);
  @override
  @JsonKey(name: "nameAR")
  String get lastName;
  @JsonKey(name: "nameAR")
  set lastName(String value);
  @override
  @JsonKey(name: "distance")
  String get distanceText;
  @JsonKey(name: "distance")
  set distanceText(String value);
  @override
  String get businessGender;
  set businessGender(String value);
  @override
  DateTime get bookingDateTime;
  set bookingDateTime(DateTime value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get promoCode;
  @JsonKey(nullable: true, defaultValue: "")
  set promoCode(String value);
  @override
  @JsonKey(name: "businessRating")
  double get rate;
  @JsonKey(name: "businessRating")
  set rate(double value);
  @override
  @JsonKey(name: "netTotalAmount")
  double get netTotAmount;
  @JsonKey(name: "netTotalAmount")
  set netTotAmount(double value);
  @override
  @JsonKey(name: "totalDiscountAmount")
  double get totalDiscAmount;
  @JsonKey(name: "totalDiscountAmount")
  set totalDiscAmount(double value);
  @override
  @JsonKey(name: "totalAmount")
  double get totAmount;
  @JsonKey(name: "totalAmount")
  set totAmount(double value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get contactNumber;
  @JsonKey(nullable: true, defaultValue: "")
  set contactNumber(String value);
  @override
  String get email;
  set email(String value);
  @override
  String get orderNumber;
  set orderNumber(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyEN;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyEN(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get currencyAR;
  @JsonKey(nullable: true, defaultValue: "")
  set currencyAR(String value);
  @override
  int get appointmentType;
  set appointmentType(int value);
  @override
  List<AppontmentServices> get manageVisitListItems;
  set manageVisitListItems(List<AppontmentServices> value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get internalNote;
  @JsonKey(nullable: true, defaultValue: "")
  set internalNote(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get messageToClient;
  @JsonKey(nullable: true, defaultValue: "")
  set messageToClient(String value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get avatar;
  @JsonKey(nullable: true, defaultValue: "")
  set avatar(String value);
  @override
  bool get isSuccess;
  set isSuccess(bool value);
  @override
  String get message;
  set message(String value);
  @override
  String get businessID;
  set businessID(String value);
  @override
  String get detail;
  set detail(String value);
  @override
  CardDetailsModel? get cardDetails;
  set cardDetails(CardDetailsModel? value);
  @override
  OrderGiftModel? get bookingOrderGift;
  set bookingOrderGift(OrderGiftModel? value);
  @override
  List<NotesModel> get noteListItems;
  set noteListItems(List<NotesModel> value);
  @override
  @JsonKey(ignore: true)
  _$$_AppointmentDetailsCopyWith<_$_AppointmentDetails> get copyWith =>
      throw _privateConstructorUsedError;
}
