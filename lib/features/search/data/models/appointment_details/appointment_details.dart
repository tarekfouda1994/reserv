import 'package:flutter/cupertino.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/search/data/models/appontment_services/appontment_services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';
import '../card_details_model/card_details_model.dart';
import '../note_model/notes_model.dart';
import '../order_gift_model/order_gift_model.dart';

part 'appointment_details.freezed.dart';

part 'appointment_details.g.dart';

@unfreezed
class AppointmentDetails with _$AppointmentDetails {
  AppointmentDetails._();

  @JsonSerializable(explicitToJson: true)
  factory AppointmentDetails({
    required String id,
    @JsonKey(name: "nameEN") required String firstName,
    @JsonKey(name: "nameAR") required String lastName,
    @JsonKey(name: "distance") required String distanceText,
    required String businessGender,
    required DateTime bookingDateTime,
    @JsonKey(nullable: true, defaultValue: "") required String promoCode,
    @JsonKey(name: "businessRating") required double rate,
    @JsonKey(name: "netTotalAmount") required double netTotAmount,
    @JsonKey(name: "totalDiscountAmount") required double totalDiscAmount,
    @JsonKey(name: "totalAmount") required double totAmount,
    @JsonKey(nullable: true, defaultValue: "") required String contactNumber,
    required String email,
    required String orderNumber,
    @JsonKey(nullable: true, defaultValue: "") required String currencyEN,
    @JsonKey(nullable: true, defaultValue: "") required String currencyAR,
    required int appointmentType,
    required List<AppontmentServices> manageVisitListItems,
    @JsonKey(nullable: true, defaultValue: "") required String internalNote,
    @JsonKey(nullable: true, defaultValue: "") required String messageToClient,
    @JsonKey(nullable: true, defaultValue: "") required String avatar,
    required bool isSuccess,
    required String message,
    required String businessID,
    required String detail,
    required CardDetailsModel? cardDetails,
    required OrderGiftModel? bookingOrderGift,
    required List<NotesModel> noteListItems,
  }) = _AppointmentDetails;

  factory AppointmentDetails.fromJson(Map<String, dynamic> json) =>
      _$AppointmentDetailsFromJson(json);

  String getUserName() {
    return "$firstName $lastName";
  }

  String currencyName() {
    return getIt<Utilities>().getLocalizedValue(currencyAR, currencyEN);
  }

  String get businessRating {
    BuildContext context = getIt<GlobalContext>().context();
    if (rate <= 0) {
      return "0";
    }
    return getIt<Utilities>().convertNumToAr(context: context, value: rate.toString());
  }

  String get netTotalAmount {
    BuildContext context = getIt<GlobalContext>().context();
    return getIt<Utilities>().convertNumToAr(context: context, value: netTotAmount.toString());
  }

  String get totalDiscountAmount {
    BuildContext context = getIt<GlobalContext>().context();
    return getIt<Utilities>().convertNumToAr(context: context, value: totalDiscAmount.toString());
  }

  String get totalAmount {
    BuildContext context = getIt<GlobalContext>().context();
    return getIt<Utilities>().convertNumToAr(context: context, value: totAmount.toString());
  }

  String get distance {
    BuildContext context = getIt<GlobalContext>().context();
    return getIt<Utilities>().convertNumToAr(
      context: context,
      value: "${distanceText.toString().split(" ").first} ${tr("km")}",
      isDistance: true,
    );
  }
}
