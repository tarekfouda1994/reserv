// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'reviews_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ReviewsModel _$ReviewsModelFromJson(Map<String, dynamic> json) {
  return _ReviewsModel.fromJson(json);
}

/// @nodoc
mixin _$ReviewsModel {
  List<ServiceReviewsModel> get companyItemReviews =>
      throw _privateConstructorUsedError;
  List<RatingModel> get ratingSummary => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ReviewsModelCopyWith<ReviewsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReviewsModelCopyWith<$Res> {
  factory $ReviewsModelCopyWith(
          ReviewsModel value, $Res Function(ReviewsModel) then) =
      _$ReviewsModelCopyWithImpl<$Res>;
  $Res call(
      {List<ServiceReviewsModel> companyItemReviews,
      List<RatingModel> ratingSummary});
}

/// @nodoc
class _$ReviewsModelCopyWithImpl<$Res> implements $ReviewsModelCopyWith<$Res> {
  _$ReviewsModelCopyWithImpl(this._value, this._then);

  final ReviewsModel _value;
  // ignore: unused_field
  final $Res Function(ReviewsModel) _then;

  @override
  $Res call({
    Object? companyItemReviews = freezed,
    Object? ratingSummary = freezed,
  }) {
    return _then(_value.copyWith(
      companyItemReviews: companyItemReviews == freezed
          ? _value.companyItemReviews
          : companyItemReviews // ignore: cast_nullable_to_non_nullable
              as List<ServiceReviewsModel>,
      ratingSummary: ratingSummary == freezed
          ? _value.ratingSummary
          : ratingSummary // ignore: cast_nullable_to_non_nullable
              as List<RatingModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_ReviewsModelCopyWith<$Res>
    implements $ReviewsModelCopyWith<$Res> {
  factory _$$_ReviewsModelCopyWith(
          _$_ReviewsModel value, $Res Function(_$_ReviewsModel) then) =
      __$$_ReviewsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<ServiceReviewsModel> companyItemReviews,
      List<RatingModel> ratingSummary});
}

/// @nodoc
class __$$_ReviewsModelCopyWithImpl<$Res>
    extends _$ReviewsModelCopyWithImpl<$Res>
    implements _$$_ReviewsModelCopyWith<$Res> {
  __$$_ReviewsModelCopyWithImpl(
      _$_ReviewsModel _value, $Res Function(_$_ReviewsModel) _then)
      : super(_value, (v) => _then(v as _$_ReviewsModel));

  @override
  _$_ReviewsModel get _value => super._value as _$_ReviewsModel;

  @override
  $Res call({
    Object? companyItemReviews = freezed,
    Object? ratingSummary = freezed,
  }) {
    return _then(_$_ReviewsModel(
      companyItemReviews: companyItemReviews == freezed
          ? _value._companyItemReviews
          : companyItemReviews // ignore: cast_nullable_to_non_nullable
              as List<ServiceReviewsModel>,
      ratingSummary: ratingSummary == freezed
          ? _value._ratingSummary
          : ratingSummary // ignore: cast_nullable_to_non_nullable
              as List<RatingModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ReviewsModel implements _ReviewsModel {
  _$_ReviewsModel(
      {required final List<ServiceReviewsModel> companyItemReviews,
      required final List<RatingModel> ratingSummary})
      : _companyItemReviews = companyItemReviews,
        _ratingSummary = ratingSummary;

  factory _$_ReviewsModel.fromJson(Map<String, dynamic> json) =>
      _$$_ReviewsModelFromJson(json);

  final List<ServiceReviewsModel> _companyItemReviews;
  @override
  List<ServiceReviewsModel> get companyItemReviews {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_companyItemReviews);
  }

  final List<RatingModel> _ratingSummary;
  @override
  List<RatingModel> get ratingSummary {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_ratingSummary);
  }

  @override
  String toString() {
    return 'ReviewsModel(companyItemReviews: $companyItemReviews, ratingSummary: $ratingSummary)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ReviewsModel &&
            const DeepCollectionEquality()
                .equals(other._companyItemReviews, _companyItemReviews) &&
            const DeepCollectionEquality()
                .equals(other._ratingSummary, _ratingSummary));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_companyItemReviews),
      const DeepCollectionEquality().hash(_ratingSummary));

  @JsonKey(ignore: true)
  @override
  _$$_ReviewsModelCopyWith<_$_ReviewsModel> get copyWith =>
      __$$_ReviewsModelCopyWithImpl<_$_ReviewsModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ReviewsModelToJson(
      this,
    );
  }
}

abstract class _ReviewsModel implements ReviewsModel {
  factory _ReviewsModel(
      {required final List<ServiceReviewsModel> companyItemReviews,
      required final List<RatingModel> ratingSummary}) = _$_ReviewsModel;

  factory _ReviewsModel.fromJson(Map<String, dynamic> json) =
      _$_ReviewsModel.fromJson;

  @override
  List<ServiceReviewsModel> get companyItemReviews;
  @override
  List<RatingModel> get ratingSummary;
  @override
  @JsonKey(ignore: true)
  _$$_ReviewsModelCopyWith<_$_ReviewsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
