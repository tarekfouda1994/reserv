import 'package:flutter_tdd/features/search/data/models/service_reviews_model/service_reviews_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../rating_model/rating_model.dart';

part 'reviews_model.freezed.dart';
part 'reviews_model.g.dart';

@freezed
class ReviewsModel with _$ReviewsModel {
  @JsonSerializable(explicitToJson: true)
  factory ReviewsModel({
    required List<ServiceReviewsModel> companyItemReviews,
    required List<RatingModel> ratingSummary,
  }) = _ReviewsModel;

  factory ReviewsModel.fromJson(Map<String, dynamic> json) => _$ReviewsModelFromJson(json);
}
