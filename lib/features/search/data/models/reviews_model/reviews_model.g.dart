// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reviews_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ReviewsModel _$$_ReviewsModelFromJson(Map<String, dynamic> json) =>
    _$_ReviewsModel(
      companyItemReviews: (json['companyItemReviews'] as List<dynamic>)
          .map((e) => ServiceReviewsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      ratingSummary: (json['ratingSummary'] as List<dynamic>)
          .map((e) => RatingModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ReviewsModelToJson(_$_ReviewsModel instance) =>
    <String, dynamic>{
      'companyItemReviews':
          instance.companyItemReviews.map((e) => e.toJson()).toList(),
      'ratingSummary': instance.ratingSummary.map((e) => e.toJson()).toList(),
    };
