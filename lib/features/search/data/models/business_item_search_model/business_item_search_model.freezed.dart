// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'business_item_search_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BusinessItemSearchModel _$BusinessItemSearchModelFromJson(
    Map<String, dynamic> json) {
  return _BusinessItemSearchModel.fromJson(json);
}

/// @nodoc
mixin _$BusinessItemSearchModel {
  String get id => throw _privateConstructorUsedError;
  set id(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: '')
  String get picture => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: '')
  set picture(String value) => throw _privateConstructorUsedError;
  String get businessNameEN => throw _privateConstructorUsedError;
  set businessNameEN(String value) => throw _privateConstructorUsedError;
  String get businessNameAR => throw _privateConstructorUsedError;
  set businessNameAR(String value) => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  set longitude(double value) => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  set latitude(double value) => throw _privateConstructorUsedError;
  int get serviceByGenderId => throw _privateConstructorUsedError;
  set serviceByGenderId(int value) => throw _privateConstructorUsedError;
  String get serviceByGenderEN => throw _privateConstructorUsedError;
  set serviceByGenderEN(String value) => throw _privateConstructorUsedError;
  String get serviceByGenderAR => throw _privateConstructorUsedError;
  set serviceByGenderAR(String value) => throw _privateConstructorUsedError;
  String get location => throw _privateConstructorUsedError;
  set location(String value) => throw _privateConstructorUsedError;
  double get distance => throw _privateConstructorUsedError;
  set distance(double value) => throw _privateConstructorUsedError;
  String get serviceDistance => throw _privateConstructorUsedError;
  set serviceDistance(String value) => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;
  set rating(double value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: '')
  String get areaNameEN => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: '')
  set areaNameEN(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: '')
  String get areaNameAR => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: '')
  set areaNameAR(String value) => throw _privateConstructorUsedError;
  int get servicesCount => throw _privateConstructorUsedError;
  set servicesCount(int value) => throw _privateConstructorUsedError;
  int get totalReviews => throw _privateConstructorUsedError;
  set totalReviews(int value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get businessImage => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set businessImage(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: false)
  bool get hasWishItem => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: false)
  set hasWishItem(bool value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BusinessItemSearchModelCopyWith<BusinessItemSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BusinessItemSearchModelCopyWith<$Res> {
  factory $BusinessItemSearchModelCopyWith(BusinessItemSearchModel value,
          $Res Function(BusinessItemSearchModel) then) =
      _$BusinessItemSearchModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      @JsonKey(nullable: true, defaultValue: '') String picture,
      String businessNameEN,
      String businessNameAR,
      double longitude,
      double latitude,
      int serviceByGenderId,
      String serviceByGenderEN,
      String serviceByGenderAR,
      String location,
      double distance,
      String serviceDistance,
      double rating,
      @JsonKey(nullable: true, defaultValue: '') String areaNameEN,
      @JsonKey(nullable: true, defaultValue: '') String areaNameAR,
      int servicesCount,
      int totalReviews,
      @JsonKey(nullable: true, defaultValue: "") String businessImage,
      @JsonKey(nullable: true, defaultValue: false) bool hasWishItem});
}

/// @nodoc
class _$BusinessItemSearchModelCopyWithImpl<$Res>
    implements $BusinessItemSearchModelCopyWith<$Res> {
  _$BusinessItemSearchModelCopyWithImpl(this._value, this._then);

  final BusinessItemSearchModel _value;
  // ignore: unused_field
  final $Res Function(BusinessItemSearchModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? picture = freezed,
    Object? businessNameEN = freezed,
    Object? businessNameAR = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
    Object? serviceByGenderId = freezed,
    Object? serviceByGenderEN = freezed,
    Object? serviceByGenderAR = freezed,
    Object? location = freezed,
    Object? distance = freezed,
    Object? serviceDistance = freezed,
    Object? rating = freezed,
    Object? areaNameEN = freezed,
    Object? areaNameAR = freezed,
    Object? servicesCount = freezed,
    Object? totalReviews = freezed,
    Object? businessImage = freezed,
    Object? hasWishItem = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      picture: picture == freezed
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameEN: businessNameEN == freezed
          ? _value.businessNameEN
          : businessNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameAR: businessNameAR == freezed
          ? _value.businessNameAR
          : businessNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      serviceByGenderId: serviceByGenderId == freezed
          ? _value.serviceByGenderId
          : serviceByGenderId // ignore: cast_nullable_to_non_nullable
              as int,
      serviceByGenderEN: serviceByGenderEN == freezed
          ? _value.serviceByGenderEN
          : serviceByGenderEN // ignore: cast_nullable_to_non_nullable
              as String,
      serviceByGenderAR: serviceByGenderAR == freezed
          ? _value.serviceByGenderAR
          : serviceByGenderAR // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      distance: distance == freezed
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as double,
      serviceDistance: serviceDistance == freezed
          ? _value.serviceDistance
          : serviceDistance // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      areaNameEN: areaNameEN == freezed
          ? _value.areaNameEN
          : areaNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      areaNameAR: areaNameAR == freezed
          ? _value.areaNameAR
          : areaNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      servicesCount: servicesCount == freezed
          ? _value.servicesCount
          : servicesCount // ignore: cast_nullable_to_non_nullable
              as int,
      totalReviews: totalReviews == freezed
          ? _value.totalReviews
          : totalReviews // ignore: cast_nullable_to_non_nullable
              as int,
      businessImage: businessImage == freezed
          ? _value.businessImage
          : businessImage // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_BusinessItemSearchModelCopyWith<$Res>
    implements $BusinessItemSearchModelCopyWith<$Res> {
  factory _$$_BusinessItemSearchModelCopyWith(_$_BusinessItemSearchModel value,
          $Res Function(_$_BusinessItemSearchModel) then) =
      __$$_BusinessItemSearchModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      @JsonKey(nullable: true, defaultValue: '') String picture,
      String businessNameEN,
      String businessNameAR,
      double longitude,
      double latitude,
      int serviceByGenderId,
      String serviceByGenderEN,
      String serviceByGenderAR,
      String location,
      double distance,
      String serviceDistance,
      double rating,
      @JsonKey(nullable: true, defaultValue: '') String areaNameEN,
      @JsonKey(nullable: true, defaultValue: '') String areaNameAR,
      int servicesCount,
      int totalReviews,
      @JsonKey(nullable: true, defaultValue: "") String businessImage,
      @JsonKey(nullable: true, defaultValue: false) bool hasWishItem});
}

/// @nodoc
class __$$_BusinessItemSearchModelCopyWithImpl<$Res>
    extends _$BusinessItemSearchModelCopyWithImpl<$Res>
    implements _$$_BusinessItemSearchModelCopyWith<$Res> {
  __$$_BusinessItemSearchModelCopyWithImpl(_$_BusinessItemSearchModel _value,
      $Res Function(_$_BusinessItemSearchModel) _then)
      : super(_value, (v) => _then(v as _$_BusinessItemSearchModel));

  @override
  _$_BusinessItemSearchModel get _value =>
      super._value as _$_BusinessItemSearchModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? picture = freezed,
    Object? businessNameEN = freezed,
    Object? businessNameAR = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
    Object? serviceByGenderId = freezed,
    Object? serviceByGenderEN = freezed,
    Object? serviceByGenderAR = freezed,
    Object? location = freezed,
    Object? distance = freezed,
    Object? serviceDistance = freezed,
    Object? rating = freezed,
    Object? areaNameEN = freezed,
    Object? areaNameAR = freezed,
    Object? servicesCount = freezed,
    Object? totalReviews = freezed,
    Object? businessImage = freezed,
    Object? hasWishItem = freezed,
  }) {
    return _then(_$_BusinessItemSearchModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      picture: picture == freezed
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameEN: businessNameEN == freezed
          ? _value.businessNameEN
          : businessNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameAR: businessNameAR == freezed
          ? _value.businessNameAR
          : businessNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      serviceByGenderId: serviceByGenderId == freezed
          ? _value.serviceByGenderId
          : serviceByGenderId // ignore: cast_nullable_to_non_nullable
              as int,
      serviceByGenderEN: serviceByGenderEN == freezed
          ? _value.serviceByGenderEN
          : serviceByGenderEN // ignore: cast_nullable_to_non_nullable
              as String,
      serviceByGenderAR: serviceByGenderAR == freezed
          ? _value.serviceByGenderAR
          : serviceByGenderAR // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
      distance: distance == freezed
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as double,
      serviceDistance: serviceDistance == freezed
          ? _value.serviceDistance
          : serviceDistance // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      areaNameEN: areaNameEN == freezed
          ? _value.areaNameEN
          : areaNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      areaNameAR: areaNameAR == freezed
          ? _value.areaNameAR
          : areaNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      servicesCount: servicesCount == freezed
          ? _value.servicesCount
          : servicesCount // ignore: cast_nullable_to_non_nullable
              as int,
      totalReviews: totalReviews == freezed
          ? _value.totalReviews
          : totalReviews // ignore: cast_nullable_to_non_nullable
              as int,
      businessImage: businessImage == freezed
          ? _value.businessImage
          : businessImage // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_BusinessItemSearchModel extends _BusinessItemSearchModel {
  _$_BusinessItemSearchModel(
      {required this.id,
      @JsonKey(nullable: true, defaultValue: '') required this.picture,
      required this.businessNameEN,
      required this.businessNameAR,
      required this.longitude,
      required this.latitude,
      required this.serviceByGenderId,
      required this.serviceByGenderEN,
      required this.serviceByGenderAR,
      required this.location,
      required this.distance,
      required this.serviceDistance,
      required this.rating,
      @JsonKey(nullable: true, defaultValue: '') required this.areaNameEN,
      @JsonKey(nullable: true, defaultValue: '') required this.areaNameAR,
      required this.servicesCount,
      required this.totalReviews,
      @JsonKey(nullable: true, defaultValue: "") required this.businessImage,
      @JsonKey(nullable: true, defaultValue: false) required this.hasWishItem})
      : super._();

  factory _$_BusinessItemSearchModel.fromJson(Map<String, dynamic> json) =>
      _$$_BusinessItemSearchModelFromJson(json);

  @override
  String id;
  @override
  @JsonKey(nullable: true, defaultValue: '')
  String picture;
  @override
  String businessNameEN;
  @override
  String businessNameAR;
  @override
  double longitude;
  @override
  double latitude;
  @override
  int serviceByGenderId;
  @override
  String serviceByGenderEN;
  @override
  String serviceByGenderAR;
  @override
  String location;
  @override
  double distance;
  @override
  String serviceDistance;
  @override
  double rating;
  @override
  @JsonKey(nullable: true, defaultValue: '')
  String areaNameEN;
  @override
  @JsonKey(nullable: true, defaultValue: '')
  String areaNameAR;
  @override
  int servicesCount;
  @override
  int totalReviews;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String businessImage;
  @override
  @JsonKey(nullable: true, defaultValue: false)
  bool hasWishItem;

  @override
  String toString() {
    return 'BusinessItemSearchModel(id: $id, picture: $picture, businessNameEN: $businessNameEN, businessNameAR: $businessNameAR, longitude: $longitude, latitude: $latitude, serviceByGenderId: $serviceByGenderId, serviceByGenderEN: $serviceByGenderEN, serviceByGenderAR: $serviceByGenderAR, location: $location, distance: $distance, serviceDistance: $serviceDistance, rating: $rating, areaNameEN: $areaNameEN, areaNameAR: $areaNameAR, servicesCount: $servicesCount, totalReviews: $totalReviews, businessImage: $businessImage, hasWishItem: $hasWishItem)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_BusinessItemSearchModelCopyWith<_$_BusinessItemSearchModel>
      get copyWith =>
          __$$_BusinessItemSearchModelCopyWithImpl<_$_BusinessItemSearchModel>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BusinessItemSearchModelToJson(
      this,
    );
  }
}

abstract class _BusinessItemSearchModel extends BusinessItemSearchModel {
  factory _BusinessItemSearchModel(
      {required String id,
      @JsonKey(nullable: true, defaultValue: '')
          required String picture,
      required String businessNameEN,
      required String businessNameAR,
      required double longitude,
      required double latitude,
      required int serviceByGenderId,
      required String serviceByGenderEN,
      required String serviceByGenderAR,
      required String location,
      required double distance,
      required String serviceDistance,
      required double rating,
      @JsonKey(nullable: true, defaultValue: '')
          required String areaNameEN,
      @JsonKey(nullable: true, defaultValue: '')
          required String areaNameAR,
      required int servicesCount,
      required int totalReviews,
      @JsonKey(nullable: true, defaultValue: "")
          required String businessImage,
      @JsonKey(nullable: true, defaultValue: false)
          required bool hasWishItem}) = _$_BusinessItemSearchModel;
  _BusinessItemSearchModel._() : super._();

  factory _BusinessItemSearchModel.fromJson(Map<String, dynamic> json) =
      _$_BusinessItemSearchModel.fromJson;

  @override
  String get id;
  set id(String value);
  @override
  @JsonKey(nullable: true, defaultValue: '')
  String get picture;
  @JsonKey(nullable: true, defaultValue: '')
  set picture(String value);
  @override
  String get businessNameEN;
  set businessNameEN(String value);
  @override
  String get businessNameAR;
  set businessNameAR(String value);
  @override
  double get longitude;
  set longitude(double value);
  @override
  double get latitude;
  set latitude(double value);
  @override
  int get serviceByGenderId;
  set serviceByGenderId(int value);
  @override
  String get serviceByGenderEN;
  set serviceByGenderEN(String value);
  @override
  String get serviceByGenderAR;
  set serviceByGenderAR(String value);
  @override
  String get location;
  set location(String value);
  @override
  double get distance;
  set distance(double value);
  @override
  String get serviceDistance;
  set serviceDistance(String value);
  @override
  double get rating;
  set rating(double value);
  @override
  @JsonKey(nullable: true, defaultValue: '')
  String get areaNameEN;
  @JsonKey(nullable: true, defaultValue: '')
  set areaNameEN(String value);
  @override
  @JsonKey(nullable: true, defaultValue: '')
  String get areaNameAR;
  @JsonKey(nullable: true, defaultValue: '')
  set areaNameAR(String value);
  @override
  int get servicesCount;
  set servicesCount(int value);
  @override
  int get totalReviews;
  set totalReviews(int value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get businessImage;
  @JsonKey(nullable: true, defaultValue: "")
  set businessImage(String value);
  @override
  @JsonKey(nullable: true, defaultValue: false)
  bool get hasWishItem;
  @JsonKey(nullable: true, defaultValue: false)
  set hasWishItem(bool value);
  @override
  @JsonKey(ignore: true)
  _$$_BusinessItemSearchModelCopyWith<_$_BusinessItemSearchModel>
      get copyWith => throw _privateConstructorUsedError;
}
