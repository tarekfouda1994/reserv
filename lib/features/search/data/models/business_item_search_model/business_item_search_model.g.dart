// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_item_search_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BusinessItemSearchModel _$$_BusinessItemSearchModelFromJson(
        Map<String, dynamic> json) =>
    _$_BusinessItemSearchModel(
      id: json['id'] as String,
      picture: json['picture'] as String? ?? '',
      businessNameEN: json['businessNameEN'] as String,
      businessNameAR: json['businessNameAR'] as String,
      longitude: (json['longitude'] as num).toDouble(),
      latitude: (json['latitude'] as num).toDouble(),
      serviceByGenderId: json['serviceByGenderId'] as int,
      serviceByGenderEN: json['serviceByGenderEN'] as String,
      serviceByGenderAR: json['serviceByGenderAR'] as String,
      location: json['location'] as String,
      distance: (json['distance'] as num).toDouble(),
      serviceDistance: json['serviceDistance'] as String,
      rating: (json['rating'] as num).toDouble(),
      areaNameEN: json['areaNameEN'] as String? ?? '',
      areaNameAR: json['areaNameAR'] as String? ?? '',
      servicesCount: json['servicesCount'] as int,
      totalReviews: json['totalReviews'] as int,
      businessImage: json['businessImage'] as String? ?? '',
      hasWishItem: json['hasWishItem'] as bool? ?? false,
    );

Map<String, dynamic> _$$_BusinessItemSearchModelToJson(
        _$_BusinessItemSearchModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'picture': instance.picture,
      'businessNameEN': instance.businessNameEN,
      'businessNameAR': instance.businessNameAR,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'serviceByGenderId': instance.serviceByGenderId,
      'serviceByGenderEN': instance.serviceByGenderEN,
      'serviceByGenderAR': instance.serviceByGenderAR,
      'location': instance.location,
      'distance': instance.distance,
      'serviceDistance': instance.serviceDistance,
      'rating': instance.rating,
      'areaNameEN': instance.areaNameEN,
      'areaNameAR': instance.areaNameAR,
      'servicesCount': instance.servicesCount,
      'totalReviews': instance.totalReviews,
      'businessImage': instance.businessImage,
      'hasWishItem': instance.hasWishItem,
    };
