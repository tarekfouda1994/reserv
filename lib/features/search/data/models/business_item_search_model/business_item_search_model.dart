import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'business_item_search_model.freezed.dart';
part 'business_item_search_model.g.dart';

@unfreezed
class BusinessItemSearchModel with _$BusinessItemSearchModel {
  BusinessItemSearchModel._();

  @JsonSerializable(explicitToJson: true)
  factory BusinessItemSearchModel({
    required String id,
    @JsonKey(nullable: true, defaultValue: '') required String picture,
    required String businessNameEN,
    required String businessNameAR,
    required double longitude,
    required double latitude,
    required int serviceByGenderId,
    required String serviceByGenderEN,
    required String serviceByGenderAR,
    required String location,
    required double distance,
    required String serviceDistance,
    required double rating,
    @JsonKey(nullable: true, defaultValue: '') required String areaNameEN,
    @JsonKey(nullable: true, defaultValue: '') required String areaNameAR,
    required int servicesCount,
    required int totalReviews,
    @JsonKey(nullable: true, defaultValue: "") required String businessImage,
    @JsonKey(nullable: true, defaultValue: false) required bool hasWishItem,
  }) = _BusinessItemSearchModel;

  factory BusinessItemSearchModel.fromJson(Map<String, dynamic> json) =>
      _$BusinessItemSearchModelFromJson(json);

  String getSearchName() {
    return getIt<Utilities>().getLocalizedValue(businessNameAR, businessNameEN);
  }

  String getAreaName() {
    return getIt<Utilities>().getLocalizedValue(areaNameAR, areaNameEN);
  }
}
