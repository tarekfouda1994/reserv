// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_search_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceSearchModel _$$_ServiceSearchModelFromJson(
        Map<String, dynamic> json) =>
    _$_ServiceSearchModel(
      totalRecords: json['totalRecords'] as int,
      pageSize: json['pageSize'] as int,
      itemsList: (json['itemsList'] as List<dynamic>)
          .map(
              (e) => ServiceSearchItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ServiceSearchModelToJson(
        _$_ServiceSearchModel instance) =>
    <String, dynamic>{
      'totalRecords': instance.totalRecords,
      'pageSize': instance.pageSize,
      'itemsList': instance.itemsList.map((e) => e.toJson()).toList(),
    };
