// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'service_search_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceSearchModel _$ServiceSearchModelFromJson(Map<String, dynamic> json) {
  return _ServiceSearchModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceSearchModel {
  int get totalRecords => throw _privateConstructorUsedError;
  int get pageSize => throw _privateConstructorUsedError;
  List<ServiceSearchItemModel> get itemsList =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceSearchModelCopyWith<ServiceSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceSearchModelCopyWith<$Res> {
  factory $ServiceSearchModelCopyWith(
          ServiceSearchModel value, $Res Function(ServiceSearchModel) then) =
      _$ServiceSearchModelCopyWithImpl<$Res>;
  $Res call(
      {int totalRecords, int pageSize, List<ServiceSearchItemModel> itemsList});
}

/// @nodoc
class _$ServiceSearchModelCopyWithImpl<$Res>
    implements $ServiceSearchModelCopyWith<$Res> {
  _$ServiceSearchModelCopyWithImpl(this._value, this._then);

  final ServiceSearchModel _value;
  // ignore: unused_field
  final $Res Function(ServiceSearchModel) _then;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_value.copyWith(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value.itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<ServiceSearchItemModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceSearchModelCopyWith<$Res>
    implements $ServiceSearchModelCopyWith<$Res> {
  factory _$$_ServiceSearchModelCopyWith(_$_ServiceSearchModel value,
          $Res Function(_$_ServiceSearchModel) then) =
      __$$_ServiceSearchModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {int totalRecords, int pageSize, List<ServiceSearchItemModel> itemsList});
}

/// @nodoc
class __$$_ServiceSearchModelCopyWithImpl<$Res>
    extends _$ServiceSearchModelCopyWithImpl<$Res>
    implements _$$_ServiceSearchModelCopyWith<$Res> {
  __$$_ServiceSearchModelCopyWithImpl(
      _$_ServiceSearchModel _value, $Res Function(_$_ServiceSearchModel) _then)
      : super(_value, (v) => _then(v as _$_ServiceSearchModel));

  @override
  _$_ServiceSearchModel get _value => super._value as _$_ServiceSearchModel;

  @override
  $Res call({
    Object? totalRecords = freezed,
    Object? pageSize = freezed,
    Object? itemsList = freezed,
  }) {
    return _then(_$_ServiceSearchModel(
      totalRecords: totalRecords == freezed
          ? _value.totalRecords
          : totalRecords // ignore: cast_nullable_to_non_nullable
              as int,
      pageSize: pageSize == freezed
          ? _value.pageSize
          : pageSize // ignore: cast_nullable_to_non_nullable
              as int,
      itemsList: itemsList == freezed
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<ServiceSearchItemModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceSearchModel implements _ServiceSearchModel {
  _$_ServiceSearchModel(
      {required this.totalRecords,
      required this.pageSize,
      required final List<ServiceSearchItemModel> itemsList})
      : _itemsList = itemsList;

  factory _$_ServiceSearchModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceSearchModelFromJson(json);

  @override
  final int totalRecords;
  @override
  final int pageSize;
  final List<ServiceSearchItemModel> _itemsList;
  @override
  List<ServiceSearchItemModel> get itemsList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'ServiceSearchModel(totalRecords: $totalRecords, pageSize: $pageSize, itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServiceSearchModel &&
            const DeepCollectionEquality()
                .equals(other.totalRecords, totalRecords) &&
            const DeepCollectionEquality().equals(other.pageSize, pageSize) &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(totalRecords),
      const DeepCollectionEquality().hash(pageSize),
      const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  _$$_ServiceSearchModelCopyWith<_$_ServiceSearchModel> get copyWith =>
      __$$_ServiceSearchModelCopyWithImpl<_$_ServiceSearchModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceSearchModelToJson(
      this,
    );
  }
}

abstract class _ServiceSearchModel implements ServiceSearchModel {
  factory _ServiceSearchModel(
          {required final int totalRecords,
          required final int pageSize,
          required final List<ServiceSearchItemModel> itemsList}) =
      _$_ServiceSearchModel;

  factory _ServiceSearchModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceSearchModel.fromJson;

  @override
  int get totalRecords;
  @override
  int get pageSize;
  @override
  List<ServiceSearchItemModel> get itemsList;
  @override
  @JsonKey(ignore: true)
  _$$_ServiceSearchModelCopyWith<_$_ServiceSearchModel> get copyWith =>
      throw _privateConstructorUsedError;
}
