import 'package:freezed_annotation/freezed_annotation.dart';

import '../sevice_search_item_model/sevice_search_item_model.dart';

part 'service_search_model.freezed.dart';
part 'service_search_model.g.dart';

@freezed
class ServiceSearchModel with _$ServiceSearchModel {
  @JsonSerializable(explicitToJson: true)
  factory ServiceSearchModel({
    required int totalRecords,
    required int pageSize,
    required List<ServiceSearchItemModel> itemsList,
  }) = _ServiceSearchModel;

  factory ServiceSearchModel.fromJson(Map<String, dynamic> json) =>
      _$ServiceSearchModelFromJson(json);
}
