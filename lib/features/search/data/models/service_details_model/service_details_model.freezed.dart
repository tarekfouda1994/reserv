// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'service_details_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceDetailsModel _$ServiceDetailsModelFromJson(Map<String, dynamic> json) {
  return _ServiceDetailsModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceDetailsModel {
  String get id => throw _privateConstructorUsedError;
  set id(String value) => throw _privateConstructorUsedError;
  String get businessNameEN => throw _privateConstructorUsedError;
  set businessNameEN(String value) => throw _privateConstructorUsedError;
  String get businessNameAR => throw _privateConstructorUsedError;
  set businessNameAR(String value) => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;
  set rating(double value) => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  set longitude(double value) => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  set latitude(double value) => throw _privateConstructorUsedError;
  @JsonKey(name: "serviceDistance")
  String get distanceText => throw _privateConstructorUsedError;
  @JsonKey(name: "serviceDistance")
  set distanceText(String value) => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  set email(String value) => throw _privateConstructorUsedError;
  String get contactNumber => throw _privateConstructorUsedError;
  set contactNumber(String value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  int get officeNumber => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: 0)
  set officeNumber(int value) => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  String get aboutUs => throw _privateConstructorUsedError;
  @JsonKey(nullable: true, defaultValue: "")
  set aboutUs(String value) => throw _privateConstructorUsedError;
  String get addressEN => throw _privateConstructorUsedError;
  set addressEN(String value) => throw _privateConstructorUsedError;
  String get addressAR => throw _privateConstructorUsedError;
  set addressAR(String value) => throw _privateConstructorUsedError;
  String get todayStatus => throw _privateConstructorUsedError;
  set todayStatus(String value) => throw _privateConstructorUsedError;
  String get todayTiming => throw _privateConstructorUsedError;
  set todayTiming(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get businessGender => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set businessGender(String value) => throw _privateConstructorUsedError;
  bool get hasWishItem => throw _privateConstructorUsedError;
  set hasWishItem(bool value) => throw _privateConstructorUsedError;
  List<String> get pictures => throw _privateConstructorUsedError;
  set pictures(List<String> value) => throw _privateConstructorUsedError;
  List<TimesModel> get timing => throw _privateConstructorUsedError;
  set timing(List<TimesModel> value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceDetailsModelCopyWith<ServiceDetailsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceDetailsModelCopyWith<$Res> {
  factory $ServiceDetailsModelCopyWith(
          ServiceDetailsModel value, $Res Function(ServiceDetailsModel) then) =
      _$ServiceDetailsModelCopyWithImpl<$Res>;
  $Res call(
      {String id,
      String businessNameEN,
      String businessNameAR,
      double rating,
      double longitude,
      double latitude,
      @JsonKey(name: "serviceDistance") String distanceText,
      String email,
      String contactNumber,
      @JsonKey(nullable: true, defaultValue: 0) int officeNumber,
      @JsonKey(nullable: true, defaultValue: "") String aboutUs,
      String addressEN,
      String addressAR,
      String todayStatus,
      String todayTiming,
      @JsonKey(defaultValue: "", nullable: true) String businessGender,
      bool hasWishItem,
      List<String> pictures,
      List<TimesModel> timing});
}

/// @nodoc
class _$ServiceDetailsModelCopyWithImpl<$Res>
    implements $ServiceDetailsModelCopyWith<$Res> {
  _$ServiceDetailsModelCopyWithImpl(this._value, this._then);

  final ServiceDetailsModel _value;
  // ignore: unused_field
  final $Res Function(ServiceDetailsModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? businessNameEN = freezed,
    Object? businessNameAR = freezed,
    Object? rating = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
    Object? distanceText = freezed,
    Object? email = freezed,
    Object? contactNumber = freezed,
    Object? officeNumber = freezed,
    Object? aboutUs = freezed,
    Object? addressEN = freezed,
    Object? addressAR = freezed,
    Object? todayStatus = freezed,
    Object? todayTiming = freezed,
    Object? businessGender = freezed,
    Object? hasWishItem = freezed,
    Object? pictures = freezed,
    Object? timing = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameEN: businessNameEN == freezed
          ? _value.businessNameEN
          : businessNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameAR: businessNameAR == freezed
          ? _value.businessNameAR
          : businessNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      officeNumber: officeNumber == freezed
          ? _value.officeNumber
          : officeNumber // ignore: cast_nullable_to_non_nullable
              as int,
      aboutUs: aboutUs == freezed
          ? _value.aboutUs
          : aboutUs // ignore: cast_nullable_to_non_nullable
              as String,
      addressEN: addressEN == freezed
          ? _value.addressEN
          : addressEN // ignore: cast_nullable_to_non_nullable
              as String,
      addressAR: addressAR == freezed
          ? _value.addressAR
          : addressAR // ignore: cast_nullable_to_non_nullable
              as String,
      todayStatus: todayStatus == freezed
          ? _value.todayStatus
          : todayStatus // ignore: cast_nullable_to_non_nullable
              as String,
      todayTiming: todayTiming == freezed
          ? _value.todayTiming
          : todayTiming // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      pictures: pictures == freezed
          ? _value.pictures
          : pictures // ignore: cast_nullable_to_non_nullable
              as List<String>,
      timing: timing == freezed
          ? _value.timing
          : timing // ignore: cast_nullable_to_non_nullable
              as List<TimesModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceDetailsModelCopyWith<$Res>
    implements $ServiceDetailsModelCopyWith<$Res> {
  factory _$$_ServiceDetailsModelCopyWith(_$_ServiceDetailsModel value,
          $Res Function(_$_ServiceDetailsModel) then) =
      __$$_ServiceDetailsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      String businessNameEN,
      String businessNameAR,
      double rating,
      double longitude,
      double latitude,
      @JsonKey(name: "serviceDistance") String distanceText,
      String email,
      String contactNumber,
      @JsonKey(nullable: true, defaultValue: 0) int officeNumber,
      @JsonKey(nullable: true, defaultValue: "") String aboutUs,
      String addressEN,
      String addressAR,
      String todayStatus,
      String todayTiming,
      @JsonKey(defaultValue: "", nullable: true) String businessGender,
      bool hasWishItem,
      List<String> pictures,
      List<TimesModel> timing});
}

/// @nodoc
class __$$_ServiceDetailsModelCopyWithImpl<$Res>
    extends _$ServiceDetailsModelCopyWithImpl<$Res>
    implements _$$_ServiceDetailsModelCopyWith<$Res> {
  __$$_ServiceDetailsModelCopyWithImpl(_$_ServiceDetailsModel _value,
      $Res Function(_$_ServiceDetailsModel) _then)
      : super(_value, (v) => _then(v as _$_ServiceDetailsModel));

  @override
  _$_ServiceDetailsModel get _value => super._value as _$_ServiceDetailsModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? businessNameEN = freezed,
    Object? businessNameAR = freezed,
    Object? rating = freezed,
    Object? longitude = freezed,
    Object? latitude = freezed,
    Object? distanceText = freezed,
    Object? email = freezed,
    Object? contactNumber = freezed,
    Object? officeNumber = freezed,
    Object? aboutUs = freezed,
    Object? addressEN = freezed,
    Object? addressAR = freezed,
    Object? todayStatus = freezed,
    Object? todayTiming = freezed,
    Object? businessGender = freezed,
    Object? hasWishItem = freezed,
    Object? pictures = freezed,
    Object? timing = freezed,
  }) {
    return _then(_$_ServiceDetailsModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameEN: businessNameEN == freezed
          ? _value.businessNameEN
          : businessNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      businessNameAR: businessNameAR == freezed
          ? _value.businessNameAR
          : businessNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: longitude == freezed
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      latitude: latitude == freezed
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      distanceText: distanceText == freezed
          ? _value.distanceText
          : distanceText // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      contactNumber: contactNumber == freezed
          ? _value.contactNumber
          : contactNumber // ignore: cast_nullable_to_non_nullable
              as String,
      officeNumber: officeNumber == freezed
          ? _value.officeNumber
          : officeNumber // ignore: cast_nullable_to_non_nullable
              as int,
      aboutUs: aboutUs == freezed
          ? _value.aboutUs
          : aboutUs // ignore: cast_nullable_to_non_nullable
              as String,
      addressEN: addressEN == freezed
          ? _value.addressEN
          : addressEN // ignore: cast_nullable_to_non_nullable
              as String,
      addressAR: addressAR == freezed
          ? _value.addressAR
          : addressAR // ignore: cast_nullable_to_non_nullable
              as String,
      todayStatus: todayStatus == freezed
          ? _value.todayStatus
          : todayStatus // ignore: cast_nullable_to_non_nullable
              as String,
      todayTiming: todayTiming == freezed
          ? _value.todayTiming
          : todayTiming // ignore: cast_nullable_to_non_nullable
              as String,
      businessGender: businessGender == freezed
          ? _value.businessGender
          : businessGender // ignore: cast_nullable_to_non_nullable
              as String,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      pictures: pictures == freezed
          ? _value.pictures
          : pictures // ignore: cast_nullable_to_non_nullable
              as List<String>,
      timing: timing == freezed
          ? _value.timing
          : timing // ignore: cast_nullable_to_non_nullable
              as List<TimesModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceDetailsModel extends _ServiceDetailsModel {
  _$_ServiceDetailsModel(
      {required this.id,
      required this.businessNameEN,
      required this.businessNameAR,
      required this.rating,
      required this.longitude,
      required this.latitude,
      @JsonKey(name: "serviceDistance") required this.distanceText,
      required this.email,
      required this.contactNumber,
      @JsonKey(nullable: true, defaultValue: 0) required this.officeNumber,
      @JsonKey(nullable: true, defaultValue: "") required this.aboutUs,
      required this.addressEN,
      required this.addressAR,
      required this.todayStatus,
      required this.todayTiming,
      @JsonKey(defaultValue: "", nullable: true) required this.businessGender,
      required this.hasWishItem,
      required this.pictures,
      required this.timing})
      : super._();

  factory _$_ServiceDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceDetailsModelFromJson(json);

  @override
  String id;
  @override
  String businessNameEN;
  @override
  String businessNameAR;
  @override
  double rating;
  @override
  double longitude;
  @override
  double latitude;
  @override
  @JsonKey(name: "serviceDistance")
  String distanceText;
  @override
  String email;
  @override
  String contactNumber;
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  int officeNumber;
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String aboutUs;
  @override
  String addressEN;
  @override
  String addressAR;
  @override
  String todayStatus;
  @override
  String todayTiming;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String businessGender;
  @override
  bool hasWishItem;
  @override
  List<String> pictures;
  @override
  List<TimesModel> timing;

  @override
  String toString() {
    return 'ServiceDetailsModel(id: $id, businessNameEN: $businessNameEN, businessNameAR: $businessNameAR, rating: $rating, longitude: $longitude, latitude: $latitude, distanceText: $distanceText, email: $email, contactNumber: $contactNumber, officeNumber: $officeNumber, aboutUs: $aboutUs, addressEN: $addressEN, addressAR: $addressAR, todayStatus: $todayStatus, todayTiming: $todayTiming, businessGender: $businessGender, hasWishItem: $hasWishItem, pictures: $pictures, timing: $timing)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_ServiceDetailsModelCopyWith<_$_ServiceDetailsModel> get copyWith =>
      __$$_ServiceDetailsModelCopyWithImpl<_$_ServiceDetailsModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceDetailsModelToJson(
      this,
    );
  }
}

abstract class _ServiceDetailsModel extends ServiceDetailsModel {
  factory _ServiceDetailsModel(
      {required String id,
      required String businessNameEN,
      required String businessNameAR,
      required double rating,
      required double longitude,
      required double latitude,
      @JsonKey(name: "serviceDistance") required String distanceText,
      required String email,
      required String contactNumber,
      @JsonKey(nullable: true, defaultValue: 0) required int officeNumber,
      @JsonKey(nullable: true, defaultValue: "") required String aboutUs,
      required String addressEN,
      required String addressAR,
      required String todayStatus,
      required String todayTiming,
      @JsonKey(defaultValue: "", nullable: true) required String businessGender,
      required bool hasWishItem,
      required List<String> pictures,
      required List<TimesModel> timing}) = _$_ServiceDetailsModel;
  _ServiceDetailsModel._() : super._();

  factory _ServiceDetailsModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceDetailsModel.fromJson;

  @override
  String get id;
  set id(String value);
  @override
  String get businessNameEN;
  set businessNameEN(String value);
  @override
  String get businessNameAR;
  set businessNameAR(String value);
  @override
  double get rating;
  set rating(double value);
  @override
  double get longitude;
  set longitude(double value);
  @override
  double get latitude;
  set latitude(double value);
  @override
  @JsonKey(name: "serviceDistance")
  String get distanceText;
  @JsonKey(name: "serviceDistance")
  set distanceText(String value);
  @override
  String get email;
  set email(String value);
  @override
  String get contactNumber;
  set contactNumber(String value);
  @override
  @JsonKey(nullable: true, defaultValue: 0)
  int get officeNumber;
  @JsonKey(nullable: true, defaultValue: 0)
  set officeNumber(int value);
  @override
  @JsonKey(nullable: true, defaultValue: "")
  String get aboutUs;
  @JsonKey(nullable: true, defaultValue: "")
  set aboutUs(String value);
  @override
  String get addressEN;
  set addressEN(String value);
  @override
  String get addressAR;
  set addressAR(String value);
  @override
  String get todayStatus;
  set todayStatus(String value);
  @override
  String get todayTiming;
  set todayTiming(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get businessGender;
  @JsonKey(defaultValue: "", nullable: true)
  set businessGender(String value);
  @override
  bool get hasWishItem;
  set hasWishItem(bool value);
  @override
  List<String> get pictures;
  set pictures(List<String> value);
  @override
  List<TimesModel> get timing;
  set timing(List<TimesModel> value);
  @override
  @JsonKey(ignore: true)
  _$$_ServiceDetailsModelCopyWith<_$_ServiceDetailsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
