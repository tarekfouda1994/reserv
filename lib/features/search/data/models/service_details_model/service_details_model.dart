import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';
import '../times_model/times_model.dart';

part 'service_details_model.freezed.dart';
part 'service_details_model.g.dart';

@unfreezed
class ServiceDetailsModel with _$ServiceDetailsModel {
  ServiceDetailsModel._();

  @JsonSerializable(explicitToJson: true)
  factory ServiceDetailsModel({
    required String id,
    required String businessNameEN,
    required String businessNameAR,
    required double rating,
    required double longitude,
    required double latitude,
    @JsonKey(name: "serviceDistance") required String distanceText,
    required String email,
    required String contactNumber,
    @JsonKey(nullable: true, defaultValue: 0) required int officeNumber,
    @JsonKey(nullable: true, defaultValue: "") required String aboutUs,
    required String addressEN,
    required String addressAR,
    required String todayStatus,
    required String todayTiming,
    @JsonKey(defaultValue: "", nullable: true) required String businessGender,
    required bool hasWishItem,
    required List<String> pictures,
    required List<TimesModel> timing,
  }) = _ServiceDetailsModel;

  factory ServiceDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$ServiceDetailsModelFromJson(json);

  String get serviceDistance {
    BuildContext context = getIt<GlobalContext>().context();
    var distance = getIt<Utilities>().convertNumToAr(
       context:  context, value: distanceText.toString().split(" ").first, isDistance: true);
    if (distanceText == "" || distanceText == "0" || distanceText == "0.00") {
      return "";
    } else {
      return "$distance ${tr("km")}";
    }
  }

  String getBusinessName() {
    return getIt<Utilities>().getLocalizedValue(businessNameAR, businessNameEN);
  }

  String getAddressName() {
    return getIt<Utilities>().getLocalizedValue(addressAR, addressEN);
  }

  String getBusinessGender() {
    switch (businessGender) {
      case "Male":
        return tr("men");
      case "Female":
        return tr("women");
      default:
        return tr("menWomen");
    }
  }
}
