// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceDetailsModel _$$_ServiceDetailsModelFromJson(
        Map<String, dynamic> json) =>
    _$_ServiceDetailsModel(
      id: json['id'] as String,
      businessNameEN: json['businessNameEN'] as String,
      businessNameAR: json['businessNameAR'] as String,
      rating: (json['rating'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      latitude: (json['latitude'] as num).toDouble(),
      distanceText: json['serviceDistance'] as String,
      email: json['email'] as String,
      contactNumber: json['contactNumber'] as String,
      officeNumber: json['officeNumber'] as int? ?? 0,
      aboutUs: json['aboutUs'] as String? ?? '',
      addressEN: json['addressEN'] as String,
      addressAR: json['addressAR'] as String,
      todayStatus: json['todayStatus'] as String,
      todayTiming: json['todayTiming'] as String,
      businessGender: json['businessGender'] as String? ?? '',
      hasWishItem: json['hasWishItem'] as bool,
      pictures:
          (json['pictures'] as List<dynamic>).map((e) => e as String).toList(),
      timing: (json['timing'] as List<dynamic>)
          .map((e) => TimesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ServiceDetailsModelToJson(
        _$_ServiceDetailsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'businessNameEN': instance.businessNameEN,
      'businessNameAR': instance.businessNameAR,
      'rating': instance.rating,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'serviceDistance': instance.distanceText,
      'email': instance.email,
      'contactNumber': instance.contactNumber,
      'officeNumber': instance.officeNumber,
      'aboutUs': instance.aboutUs,
      'addressEN': instance.addressEN,
      'addressAR': instance.addressAR,
      'todayStatus': instance.todayStatus,
      'todayTiming': instance.todayTiming,
      'businessGender': instance.businessGender,
      'hasWishItem': instance.hasWishItem,
      'pictures': instance.pictures,
      'timing': instance.timing.map((e) => e.toJson()).toList(),
    };
