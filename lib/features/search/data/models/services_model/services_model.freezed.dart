// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'services_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServicesModel _$ServicesModelFromJson(Map<String, dynamic> json) {
  return _ServicesModel.fromJson(json);
}

/// @nodoc
mixin _$ServicesModel {
  String get serviceNameEN => throw _privateConstructorUsedError;
  String get serviceNameAR => throw _privateConstructorUsedError;
  String get appointmentId => throw _privateConstructorUsedError;
  String get businessServiceID => throw _privateConstructorUsedError;
  String get staffID => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;
  int get statusID => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServicesModelCopyWith<ServicesModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServicesModelCopyWith<$Res> {
  factory $ServicesModelCopyWith(
          ServicesModel value, $Res Function(ServicesModel) then) =
      _$ServicesModelCopyWithImpl<$Res>;
  $Res call(
      {String serviceNameEN,
      String serviceNameAR,
      String appointmentId,
      String businessServiceID,
      String staffID,
      double rating,
      int statusID});
}

/// @nodoc
class _$ServicesModelCopyWithImpl<$Res>
    implements $ServicesModelCopyWith<$Res> {
  _$ServicesModelCopyWithImpl(this._value, this._then);

  final ServicesModel _value;
  // ignore: unused_field
  final $Res Function(ServicesModel) _then;

  @override
  $Res call({
    Object? serviceNameEN = freezed,
    Object? serviceNameAR = freezed,
    Object? appointmentId = freezed,
    Object? businessServiceID = freezed,
    Object? staffID = freezed,
    Object? rating = freezed,
    Object? statusID = freezed,
  }) {
    return _then(_value.copyWith(
      serviceNameEN: serviceNameEN == freezed
          ? _value.serviceNameEN
          : serviceNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      serviceNameAR: serviceNameAR == freezed
          ? _value.serviceNameAR
          : serviceNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentId: appointmentId == freezed
          ? _value.appointmentId
          : appointmentId // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      staffID: staffID == freezed
          ? _value.staffID
          : staffID // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      statusID: statusID == freezed
          ? _value.statusID
          : statusID // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_ServicesModelCopyWith<$Res>
    implements $ServicesModelCopyWith<$Res> {
  factory _$$_ServicesModelCopyWith(
          _$_ServicesModel value, $Res Function(_$_ServicesModel) then) =
      __$$_ServicesModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String serviceNameEN,
      String serviceNameAR,
      String appointmentId,
      String businessServiceID,
      String staffID,
      double rating,
      int statusID});
}

/// @nodoc
class __$$_ServicesModelCopyWithImpl<$Res>
    extends _$ServicesModelCopyWithImpl<$Res>
    implements _$$_ServicesModelCopyWith<$Res> {
  __$$_ServicesModelCopyWithImpl(
      _$_ServicesModel _value, $Res Function(_$_ServicesModel) _then)
      : super(_value, (v) => _then(v as _$_ServicesModel));

  @override
  _$_ServicesModel get _value => super._value as _$_ServicesModel;

  @override
  $Res call({
    Object? serviceNameEN = freezed,
    Object? serviceNameAR = freezed,
    Object? appointmentId = freezed,
    Object? businessServiceID = freezed,
    Object? staffID = freezed,
    Object? rating = freezed,
    Object? statusID = freezed,
  }) {
    return _then(_$_ServicesModel(
      serviceNameEN: serviceNameEN == freezed
          ? _value.serviceNameEN
          : serviceNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      serviceNameAR: serviceNameAR == freezed
          ? _value.serviceNameAR
          : serviceNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      appointmentId: appointmentId == freezed
          ? _value.appointmentId
          : appointmentId // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      staffID: staffID == freezed
          ? _value.staffID
          : staffID // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      statusID: statusID == freezed
          ? _value.statusID
          : statusID // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServicesModel extends _ServicesModel {
  _$_ServicesModel(
      {required this.serviceNameEN,
      required this.serviceNameAR,
      required this.appointmentId,
      required this.businessServiceID,
      required this.staffID,
      required this.rating,
      required this.statusID})
      : super._();

  factory _$_ServicesModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServicesModelFromJson(json);

  @override
  final String serviceNameEN;
  @override
  final String serviceNameAR;
  @override
  final String appointmentId;
  @override
  final String businessServiceID;
  @override
  final String staffID;
  @override
  final double rating;
  @override
  final int statusID;

  @override
  String toString() {
    return 'ServicesModel(serviceNameEN: $serviceNameEN, serviceNameAR: $serviceNameAR, appointmentId: $appointmentId, businessServiceID: $businessServiceID, staffID: $staffID, rating: $rating, statusID: $statusID)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServicesModel &&
            const DeepCollectionEquality()
                .equals(other.serviceNameEN, serviceNameEN) &&
            const DeepCollectionEquality()
                .equals(other.serviceNameAR, serviceNameAR) &&
            const DeepCollectionEquality()
                .equals(other.appointmentId, appointmentId) &&
            const DeepCollectionEquality()
                .equals(other.businessServiceID, businessServiceID) &&
            const DeepCollectionEquality().equals(other.staffID, staffID) &&
            const DeepCollectionEquality().equals(other.rating, rating) &&
            const DeepCollectionEquality().equals(other.statusID, statusID));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(serviceNameEN),
      const DeepCollectionEquality().hash(serviceNameAR),
      const DeepCollectionEquality().hash(appointmentId),
      const DeepCollectionEquality().hash(businessServiceID),
      const DeepCollectionEquality().hash(staffID),
      const DeepCollectionEquality().hash(rating),
      const DeepCollectionEquality().hash(statusID));

  @JsonKey(ignore: true)
  @override
  _$$_ServicesModelCopyWith<_$_ServicesModel> get copyWith =>
      __$$_ServicesModelCopyWithImpl<_$_ServicesModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServicesModelToJson(
      this,
    );
  }
}

abstract class _ServicesModel extends ServicesModel {
  factory _ServicesModel(
      {required final String serviceNameEN,
      required final String serviceNameAR,
      required final String appointmentId,
      required final String businessServiceID,
      required final String staffID,
      required final double rating,
      required final int statusID}) = _$_ServicesModel;
  _ServicesModel._() : super._();

  factory _ServicesModel.fromJson(Map<String, dynamic> json) =
      _$_ServicesModel.fromJson;

  @override
  String get serviceNameEN;
  @override
  String get serviceNameAR;
  @override
  String get appointmentId;
  @override
  String get businessServiceID;
  @override
  String get staffID;
  @override
  double get rating;
  @override
  int get statusID;
  @override
  @JsonKey(ignore: true)
  _$$_ServicesModelCopyWith<_$_ServicesModel> get copyWith =>
      throw _privateConstructorUsedError;
}
