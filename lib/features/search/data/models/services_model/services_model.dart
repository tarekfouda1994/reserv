import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'services_model.freezed.dart';
part 'services_model.g.dart';

@freezed
class ServicesModel with _$ServicesModel {
  ServicesModel._();

  @JsonSerializable(explicitToJson: true)
  factory ServicesModel({
    required String serviceNameEN,
    required String serviceNameAR,
    required String appointmentId,
    required String businessServiceID,
    required String staffID,
    required double rating,
    required int statusID,
  }) = _ServicesModel;

  factory ServicesModel.fromJson(Map<String, dynamic> json) => _$ServicesModelFromJson(json);

  String getServiceName() {
    return getIt<Utilities>().getLocalizedValue(serviceNameAR, serviceNameEN);
  }
}
