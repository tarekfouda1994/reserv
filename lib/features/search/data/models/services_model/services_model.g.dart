// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'services_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServicesModel _$$_ServicesModelFromJson(Map<String, dynamic> json) =>
    _$_ServicesModel(
      serviceNameEN: json['serviceNameEN'] as String,
      serviceNameAR: json['serviceNameAR'] as String,
      appointmentId: json['appointmentId'] as String,
      businessServiceID: json['businessServiceID'] as String,
      staffID: json['staffID'] as String,
      rating: (json['rating'] as num).toDouble(),
      statusID: json['statusID'] as int,
    );

Map<String, dynamic> _$$_ServicesModelToJson(_$_ServicesModel instance) =>
    <String, dynamic>{
      'serviceNameEN': instance.serviceNameEN,
      'serviceNameAR': instance.serviceNameAR,
      'appointmentId': instance.appointmentId,
      'businessServiceID': instance.businessServiceID,
      'staffID': instance.staffID,
      'rating': instance.rating,
      'statusID': instance.statusID,
    };
