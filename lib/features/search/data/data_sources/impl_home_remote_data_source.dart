import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/http/generic_http/generic_http.dart';
import 'package:flutter_tdd/core/http/models/http_request_model.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_details/appointment_details.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_orders/appointment_orders.dart';
import 'package:flutter_tdd/features/search/data/models/business_staff_model/business_staff_model.dart';
import 'package:flutter_tdd/features/search/data/models/count_chat_model/count_chat_model.dart';
import 'package:flutter_tdd/features/search/data/models/hot_deals_model/hot_deals_model.dart';
import 'package:flutter_tdd/features/search/data/models/note_model/notes_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_cat_model/popular_cat_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_search_model/popular_search_model.dart';
import 'package:flutter_tdd/features/search/data/models/reviews_model/reviews_model.dart';
import 'package:flutter_tdd/features/search/data/models/service_details_model/service_details_model.dart';
import 'package:flutter_tdd/features/search/data/models/service_search_model/service_search_model.dart';
import 'package:flutter_tdd/features/search/data/models/status_model/status_model.dart';
import 'package:flutter_tdd/features/search/data/models/top_businesses_model/top_businesses_model.dart';
import 'package:flutter_tdd/features/search/data/models/trinding_model/trinding_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/add_msg_chat_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/appointment_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/banners_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_image_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_staff_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/cancel_order_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/edit_note_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/get_notes_params.dart';
import 'package:flutter_tdd/features/search/domain/entites/hot_deals_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/msg_count_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/note_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/search_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/services_rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:flutter_tdd/features/search/domain/entites/trinding_entity.dart';
import 'package:injectable/injectable.dart';

import '../../domain/entites/appointment_order_entity.dart';
import '../../domain/entites/service_details_entity.dart';
import '../../domain/entites/sevice_search_entity.dart';
import '../models/business_search_model/business_search_model.dart';
import '../models/chat_model/chat_model.dart';
import '../models/home_banners/home_banners_model.dart';
import '../models/popular_services_model/popular_services_model.dart';
import 'home_remote_data_source.dart';

@lazySingleton
class ImplHomeRemoteDataSource extends HomeRemoteDataSource {
  @override
  Future<Either<Failure, HotDealsModel>> getHotDeals(
      HotDealsPrams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getHotDeals,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      refresh: param.refresh,
      requestBody: param.toJson(),
      toJsonFunc: (json) => HotDealsModel.fromJson(json),
    );
    return await GenericHttpImpl<HotDealsModel>()(model);
  }

  @override
  Future<Either<Failure, HomeBannersModel>> getMainBanners(
      BannersParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getMainBanners,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      refresh: param.refresh,
      requestBody: param.toJson(),
      toJsonFunc: (json) => HomeBannersModel.fromJson(json),
    );
    return await GenericHttpImpl<HomeBannersModel>()(model);
  }

  @override
  Future<Either<Failure, TrindingModel>> getTrending(
      TrendingPrams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: param.filter.isEmpty
          ? ApiNames.getTrending
          : ApiNames.getSearchTrending,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      refresh: param.refresh,
      requestBody: param.toJson(),
      toJsonFunc: (json) => TrindingModel.fromMap(json),
    );
    return await GenericHttpImpl<TrindingModel>()(model);
  }

  @override
  Future<Either<Failure, List<PopularServicesModel>>> getPopular(
      bool param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.popularServices,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: param,
      toJsonFunc: (json) => List<PopularServicesModel>.from(
          json.map((e) => PopularServicesModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<PopularServicesModel>>()(model);
  }

  @override
  Future<Either<Failure, List<PopularSearchModel>>> getPopularSearch(
      bool param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.popularSearch,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: param,
      toJsonFunc: (json) => List<PopularSearchModel>.from(
          json.map((e) => PopularSearchModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<PopularSearchModel>>()(model);
  }

  @override
  Future<Either<Failure, List<PopularSearchModel>>> getRecentSearch(
      bool param) async {
    var deviceId = await FirebaseMessaging.instance.getToken();
    var params = "?deviceId=$deviceId";

    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.recentSearch + params,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: param,
      toJsonFunc: (json) => List<PopularSearchModel>.from(
          json.map((e) => PopularSearchModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<PopularSearchModel>>()(model);
  }

  @override
  Future<Either<Failure, BusinessSearchModel>> getAllSearch(
      SearchPrams param) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.searchGetAll+ "#${json.encode(param.toJson())}",
        requestMethod: RequestMethod.post,
        responseType: ResType.model,
        requestBody: param.toJson(),
        refresh: param.refresh ?? false,
        responseKey: (data) => data,
        toJsonFunc: (json) => BusinessSearchModel.fromJson(json));
    return await GenericHttpImpl<BusinessSearchModel>()(model);
  }

  @override
  Future<Either<Failure, ServiceDetailsModel>> getServiceDetails(
      ServiceDetailsPrams prams) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getServicesDetails,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      requestBody: prams.toJson(),
      refresh: prams.refresh,
      toJsonFunc: (json) => ServiceDetailsModel.fromJson(json),
    );
    return await GenericHttpImpl<ServiceDetailsModel>()(model);
  }

  @override
  Future<Either<Failure, ReviewsModel>> getServiceReviews(
      ServiceDetailsPrams prams) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getServicesReviews,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      requestBody: prams.toJson(),
      refresh: prams.refresh,
      responseKey: (data) => data,
      toJsonFunc: (json) => ReviewsModel.fromJson(json),
    );
    return await GenericHttpImpl<ReviewsModel>()(model);
  }

  @override
  Future<Either<Failure, ServiceSearchModel>> getAllServiceSearch(
      ServiceSearchParams param) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.searchServices,
        requestMethod: RequestMethod.post,
        responseType: ResType.model,
        requestBody: param.toJson(),
        refresh: param.refresh ?? false,
        responseKey: (data) => data,
        toJsonFunc: (json) => ServiceSearchModel.fromJson(json));
    return await GenericHttpImpl<ServiceSearchModel>()(model);
  }

  @override
  Future<Either<Failure, List<AppointmentOrdersModel>>> getDashboardData(
      AppointmentOrderParams prams) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllOrders+  "#${json.encode(prams.toJson())}",
      requestMethod: RequestMethod.post,
      showLoader: false,
      responseType: ResType.list,
      refresh: prams.refresh ?? true,
      responseKey: (data) => data["itemsList"],
      requestBody: prams.toJson(),
      toJsonFunc: (json) => List<AppointmentOrdersModel>.from(
          json.map((e) => AppointmentOrdersModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<AppointmentOrdersModel>>()(model);
  }

  @override
  Future<Either<Failure, List<StatusModel>>> getStatus(bool prams) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllOrdersStatus,
      requestMethod: RequestMethod.get,
      responseType: ResType.list,
      refresh: prams,
      toJsonFunc: (json) =>
          List<StatusModel>.from(json.map((e) => StatusModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<StatusModel>>()(model);
  }

  @override
  Future<Either<Failure, AppointmentDetails>> getAppointmentDetails(
      AppointmentDetailsParams prams) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.getAppointmentDetails,
        requestMethod: RequestMethod.post,
        showLoader: false,
        responseType: ResType.model,
        requestBody: prams.toJson(),
        toJsonFunc: (json) => AppointmentDetails.fromJson(json));
    return await GenericHttpImpl<AppointmentDetails>()(model);
  }

  @override
  Future<Either<Failure, List<BusinessStaffModel>>> getBusinessStaff(
      BusinessStaffParams prams) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getBusinessStaff,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: prams.refresh ?? true,
      responseKey: (data) => data["itemsList"],
      requestBody: prams.toJson(),
      toJsonFunc: (json) => List<BusinessStaffModel>.from(
          json.map((e) => BusinessStaffModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<BusinessStaffModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> cancelOrder(CancelOrderParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.cancelOrder,
      requestBody: param.toJson(),
      requestMethod: RequestMethod.put,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> cancelOrderDetails(
      CancelOrderParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.cancelOrderDetails,
      requestBody: param.toJson(),
      requestMethod: RequestMethod.put,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<dynamic>>> getAppointmentImage(
      BusinessImagesParams param) async {
    var params = "?businessId=${param.businessId}&firstOnly=${param.firstOnly}";
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.businessImage + params,
        requestMethod: RequestMethod.get,
        responseType: ResType.type,
        toJsonFunc: (json) =>
            List<dynamic>.from(json.decode(json).map((x) => x)),
        refresh: param.refresh ?? true);
    return await GenericHttpImpl<List<dynamic>>()(model);
  }

  @override
  Future<Either<Failure, bool>> addNote(AddNoteParams param) async {
    var params = "?orderID=${param.orderID}";

    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.addNote + params,
      requestDynamicBody: param.noteTitle,
      requestMethod: RequestMethod.post,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> editNote(EditNoteParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.editNote,
      requestBody: param.toJson(),
      requestMethod: RequestMethod.post,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<NotesModel>>> getNotes(
      GetNoteParams param) async {
    var params = "?orderID=${param.orderID}";
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllNotes + params,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: param.refresh ?? false,
      toJsonFunc: (json) =>
          List<NotesModel>.from(json.map((e) => NotesModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<NotesModel>>()(model);
  }

  @override
  Future<Either<Failure, bool>> removeAllNote(String param) async {
    var params = "?orderID=$param";
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.removeAllNotes + params,
      requestMethod: RequestMethod.post,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> removeNote(String param) async {
    var params = "?noteID=$param";
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.removeOneNote + params,
      requestMethod: RequestMethod.post,
      showLoader: true,
      responseKey: (data) => data["isSuccess"],
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, TopBusinessesModel>> getTopBusinesses(
      TopBusinessParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getTopBusinesses,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      refresh: param.refresh,
      requestBody: param.toJson(),
      toJsonFunc: (json) => TopBusinessesModel.fromJson(json),
    );
    return await GenericHttpImpl<TopBusinessesModel>()(model);
  }

  @override
  Future<Either<Failure, TrindingModel>> getHomeServices(
      TrendingPrams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.apiNamesHome(param.homeApisType!),
      responseType: ResType.model,
      requestMethod: RequestMethod.post,
      refresh: param.refresh,
      requestBody: param.toJson(),
      toJsonFunc: (json) => TrindingModel.fromJson(json),
    );
    return await GenericHttpImpl<TrindingModel>()(model);
  }

  @override
  Future<Either<Failure, bool>> rating(RateParams param) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.createRate,
        requestMethod: RequestMethod.post,
        showLoader: true,
        requestBody: param.toJson(),
        responseType: ResType.type,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> ratingServices(ServicesRateParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.createServicesRate,
      requestMethod: RequestMethod.post,
      showLoader: true,
      requestBody: param.toJson(),
      responseType: ResType.type,
    );
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, bool>> addMsgChat(AddMsgChatParams param) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.addMsgToChat,
        requestMethod: RequestMethod.post,
        showLoader: true,
        requestBody: param.toJson(),
        responseType: ResType.type,
        responseKey: (data) => data["isSuccess"]);
    return await GenericHttpImpl<bool>()(model);
  }

  @override
  Future<Either<Failure, List<ChatModel>>> getAllMsgChat(String param) async {
    var params = "?orderNumber=$param";
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getAllMessages + params,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: true,
      toJsonFunc: (json) =>
          List<ChatModel>.from(json.map((e) => ChatModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<ChatModel>>()(model);
  }

  @override
  Future<Either<Failure, List<CountChatModel>>> getMsgCount(
      MsgCountParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getMsgCount,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      showLoader: false,
      requestBody: param.toJson(),
      refresh: true,
      responseKey: (data) => data["itemsList"],
      toJsonFunc: (json) => List<CountChatModel>.from(
          json.map((e) => CountChatModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<CountChatModel>>()(model);
  }

  @override
  Future<Either<Failure, List<PopularCatModel>>> getPopularCat(
      TopBusinessParams param) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getPopularCat,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      requestBody: param.toJson(),
      refresh: param.refresh,
      responseKey: (data) => data["itemsList"],
      toJsonFunc: (json) => List<PopularCatModel>.from(
          json.map((e) => PopularCatModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<PopularCatModel>>()(model);
  }
}
