part of 'widgets_imports.dart';

class BuildDeptHeader extends StatelessWidget {
  final String title;
  final String? subTitle;
  final int count;
  final double? paddingHorizontal;
  final bool hasMore;
  final bool isHotDeal;
  final Widget? moreWidget;
  final Color? bockGroundColorCount;
  final Color? counterColor;
  final Function()? onTap;

  const BuildDeptHeader({
    Key? key,
    required this.title,
    this.subTitle,
    this.count = 0,
    this.hasMore = true,
    this.isHotDeal = false,
    this.onTap,
    this.bockGroundColorCount,
    this.moreWidget,
    this.paddingHorizontal,
    this.counterColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap: hasMore ? onTap : null,
      child: Container(
        margin: const EdgeInsetsDirectional.only(start: 1, bottom: 3),
        padding: EdgeInsets.symmetric(horizontal: paddingHorizontal ?? 16).r,
        child: Row(
          children: [
            if (isHotDeal)
              Padding(
                padding: EdgeInsetsDirectional.only(end: 6.r, bottom: 3.r),
                child: SvgPicture.asset(
                  Res.hotDeals,
                  width: 20.sp,
                  height: 19.sp,
                  color: Color(0xffED3A57),
                ),
              ),
            Flexible(
              child: Row(
                children: [
                  Flexible(
                    child: RichText(
                      textScaleFactor: 1.2,
                      text: TextSpan(
                        text: title,
                        style: TextStyle(
                          fontFamily: CustomFonts.primaryFont,
                          color: isHotDeal ? Color(0xffED3A57) : MyColors.black,
                          fontSize: device.isTablet ? 8.5.sp : 13.5.sp,
                          fontWeight: FontWeight.bold,
                        ),
                        children: [
                          TextSpan(
                            text: subTitle ?? "",
                            style: TextStyle(
                              fontFamily: CustomFonts.primaryFont,
                              color: isHotDeal
                                  ? Color(0xffED3A57)
                                  : MyColors.black,
                              fontSize: device.isTablet ? 8.5.sp : 13.5.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text: count == 0 ? "" : " ($count)",
                            style: TextStyle(
                              fontFamily: CustomFonts.primaryFont,
                              color: isHotDeal
                                  ? Color(0xffED3A57)
                                  : MyColors.black,
                              fontSize: device.isTablet ? 8.5.sp : 13.5.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 10.w),
                ],
              ),
            ),
            moreWidget ??
                Offstage(
                  offstage: !hasMore,
                  child: Row(
                    children: [
                      MyText(
                        title: tr("see_all"),
                        color: MyColors.primary,
                        fontFamily: CustomFonts.primaryFont,
                        size: device.isTablet ? 6.sp : 9.5.sp,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: device.isTablet ? 8.sp : 10.sp,
                        color: MyColors.primary,
                      )
                    ],
                  ),
                ),
          ],
        ),
      ),
    );
  }
}
