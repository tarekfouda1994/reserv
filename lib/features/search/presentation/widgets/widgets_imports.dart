import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../core/localization/localization_methods.dart';

part 'build_dept_header.dart';
part 'build_icon_item.dart';
