part of 'widgets_imports.dart';

class BuildIconItem extends StatelessWidget {
  final Widget? img;
  final IconData? icon;
  final Color? iconColor;
  final Color? backGroundColor;
  final Color? titleColor;
  final Color? borderColor;
  final double? sizeIcon;
  final double? paddingIcon;
  final bool? showTitle;
  final String? title;
  final Function()? onTap;

  const BuildIconItem({
    Key? key,
    this.icon,
    this.iconColor,
    this.sizeIcon,
    this.backGroundColor,
    this.paddingIcon,
    this.onTap,
    this.img,
    this.showTitle,
    this.title,
    this.titleColor, this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        GestureDetector(
          onTap: onTap,
          child: Container(
            height:device.isTablet?35.h: 38.h,
            width: device.isTablet?35.w: 38.w,
            padding: EdgeInsets.all(paddingIcon ?? 8.r),
            decoration: BoxDecoration(
              border: Border.all(color:borderColor??MyColors.grey.withOpacity(.5),width: .5 ),
              shape: BoxShape.circle,
              color: backGroundColor ?? MyColors.primary.withOpacity(.04),
            ),
            child: img ??
                Icon(
                  icon,
                  size: sizeIcon ?? (device.isTablet ? 15.sp : 19.sp),
                  color: iconColor ?? MyColors.primary,
                ),
          ),
        ),
        if (showTitle == true)
          Container(
            width:
                device.isTablet ? null : MediaQuery.of(context).size.width * .2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 3),
                FittedBox(
                  child: MyText(
                    title: title ?? "",
                    color: titleColor ?? MyColors.black,
                    size: device.isTablet ? 6.sp : 9.sp,
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }
}
