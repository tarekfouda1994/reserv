import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../core/localization/localization_methods.dart';
import '../../../../core/routes/router_imports.gr.dart';

class BuildRescheduleItem extends StatelessWidget {
  final Color? backGroundColor;
  final String businessID;
  final String?serviceId;

  const BuildRescheduleItem({Key? key, this.backGroundColor, required this.businessID, this.serviceId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return InkWell(
      onTap: () => AutoRouter.of(context).push(ReservationRootRoute(businessID: businessID,serviceId: serviceId)),
      borderRadius: BorderRadius.circular(40.r),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18.w, vertical: 6.h),
        margin: EdgeInsets.symmetric(vertical: 8.h, horizontal: 5.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.r), color: backGroundColor ?? MyColors.white),
        child: Visibility(
          visible: device.locale == Locale('en', 'US'),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MyText(
                title: "R",
                color: MyColors.secondary,
                size: device.isTablet ? 7.sp : 11,
                fontWeight: FontWeight.bold,
              ),
              MyText(
                  title: "eschedule",
                  color: MyColors.primary,
                  size: device.isTablet ? 6.sp : 9.5.sp,
                  fontWeight: FontWeight.bold)
            ],
          ),
          replacement: MyText(
              alien: TextAlign.center,
              size: device.isTablet ? 6.sp : 9.sp,
              title: tr("reschedule"),
              color: MyColors.primary,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
