import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_tdd/features/general/domain/entities/location_entity.dart';

part 'location_state.dart';

class HomeLocationCubit extends Cubit<HomeLocationState> {
  HomeLocationCubit() : super(LocationInitial());

  onLocationUpdated(LocationEntity? model) {
    emit(LocationUpdated(model, state.changed));
  }
}
