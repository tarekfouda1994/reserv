part of 'location_cubit.dart';

abstract class HomeLocationState extends Equatable {
  final LocationEntity? model;
  final bool? changed;

  const HomeLocationState({this.model, this.changed});
}

class LocationInitial extends HomeLocationState {
  LocationInitial() : super(model: null, changed: false);

  @override
  List<Object?> get props => [model, changed];
}

class LocationUpdated extends HomeLocationState {
  LocationUpdated(LocationEntity? model, bool? changed) : super(model: model, changed: changed);

  @override
  List<Object?> get props => [model, changed];
}
