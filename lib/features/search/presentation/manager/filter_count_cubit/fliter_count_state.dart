part of 'filter_count_cubit.dart';

abstract class FilterCountState extends Equatable {
  final List<int> list;
  final bool changed;

  const FilterCountState({required this.list, required this.changed});
}

class FilterCountInitial extends FilterCountState {
   FilterCountInitial() : super(list: [],changed: false);

  @override
  List<Object> get props => [changed];
}

class FilterCountUpdateState extends FilterCountState {
  const FilterCountUpdateState({required List<int> list, required bool changed})
      : super(list: list, changed: changed);

  @override
  List<Object> get props => [changed];
}
