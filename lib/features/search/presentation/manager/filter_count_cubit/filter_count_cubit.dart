import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'fliter_count_state.dart';

class FilterCountCubit extends Cubit<FilterCountState> {
  FilterCountCubit() : super(FilterCountInitial());

  onUpdateFilterCount(List<int> list) {
    emit(FilterCountUpdateState(list: list, changed: !state.changed));
  }
}
