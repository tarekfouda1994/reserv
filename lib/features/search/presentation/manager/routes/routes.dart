import 'package:auto_route/auto_route.dart';
import 'package:flutter_tdd/features/search/presentation/pages/all_banners_page/all_banners_page_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/dashboard_details/dashboard_details_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/product_details/product_details_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/search_screan/search_screen_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/select_date_and_time_page/date_time_page_Imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/top_parters/top_parters_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/tracking_map/tracking_map_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/trending_services_page/trending_services_imports.dart';

import '../../pages/business_saerch/business_search_imports.dart';
import '../../pages/chat_room/chat_room_imports.dart';
import '../../pages/filter/filter_import.dart';
import '../../pages/home/home_imports.dart';
import '../../pages/hot_deals_page/hot_deals_page_imports.dart';
import '../../pages/servicees_search/services_search_imports.dart';

const baseRoute = [
  CustomRoute(
    page: Home,
    durationInMilliseconds: 800,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: DashboardDetails,
    durationInMilliseconds: 800,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: TrackingMap,
    durationInMilliseconds: 800,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: DateTimePage,
    durationInMilliseconds: 800,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: SearchScreen,
    durationInMilliseconds: 800,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: TrendingServices,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: HotDealsPage,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: ProductDetails,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: BusinessSearchPage,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: ServicesSearchPage,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: ChatRoom,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: Filter,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
  CustomRoute(
    page: TopPartners,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),CustomRoute(
    page: DashboardDetails,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),CustomRoute(
    page: AllBannerPage,
    durationInMilliseconds: 100,
    transitionsBuilder: TransitionsBuilders.fadeIn,
  ),
];
