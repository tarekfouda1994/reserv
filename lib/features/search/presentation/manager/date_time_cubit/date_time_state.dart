part of 'date_time_cubit.dart';

abstract class DateTimeState extends Equatable {
  final SelectedDateTimeEntity? model;
  final bool changed;

  const DateTimeState({this.model, required this.changed});
}

class DateTimeInitial extends DateTimeState {
  const DateTimeInitial() : super(changed: false);

  @override
  List<Object> get props => [changed];
}

class DateTimeUserUpdateState extends DateTimeState {
  const DateTimeUserUpdateState({required SelectedDateTimeEntity? model, required bool changed})
      : super(model: model, changed: changed);

  @override
  List<Object> get props => [changed];
}
