import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entites/selected_date_time_entity.dart';

part 'date_time_state.dart';

class DateTimeCubit extends Cubit<DateTimeState> {
  DateTimeCubit() : super(const DateTimeInitial());

  onUpdateDateTimeData(SelectedDateTimeEntity model) {
    emit(DateTimeUserUpdateState(model: model, changed: !state.changed));
  }
}
