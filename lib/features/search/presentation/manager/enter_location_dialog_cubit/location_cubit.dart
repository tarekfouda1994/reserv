import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'location_state.dart';

class SelectLocDialogCubit extends Cubit<SelectLocDialogState> {
  SelectLocDialogCubit() : super(SelectLocDialogInitial());

  onLocationUpdated(bool show) {
    emit(SelectLocDialogUpdated(show, state.changed));
  }
}
