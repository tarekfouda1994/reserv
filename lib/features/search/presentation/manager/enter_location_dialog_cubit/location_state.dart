part of 'location_cubit.dart';

abstract class SelectLocDialogState extends Equatable {
  final bool show;
  final bool? changed;

  const SelectLocDialogState({required this.show, this.changed});
}

class SelectLocDialogInitial extends SelectLocDialogState {
  SelectLocDialogInitial() : super(show: true, changed: false);

  @override
  List<Object?> get props => [show, changed];
}

class SelectLocDialogUpdated extends SelectLocDialogState {
  SelectLocDialogUpdated(bool show, bool? changed) : super(show: show, changed: changed);

  @override
  List<Object?> get props => [show, changed];
}
