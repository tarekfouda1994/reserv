part of 'dashboard_details_imports.dart';

class DashboardDetails extends StatefulWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final int status;

  const DashboardDetails(
      {Key? key, required this.appointmentOrdersModel, required this.status})
      : super(key: key);

  @override
  State<DashboardDetails> createState() => _DashboardDetailsState();
}

class _DashboardDetailsState extends State<DashboardDetails> {
  final DashboardDetailsData dashboardDetailsData = DashboardDetailsData();

  @override
  void initState() {
    dashboardDetailsData.status = widget.status;
    dashboardDetailsData.ficheData(context, widget.appointmentOrdersModel);
    dashboardDetailsData.getMsgCount(widget.appointmentOrdersModel.orderNumber,
        widget.appointmentOrdersModel.businessID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return GradientScaffold(
      appBar: DefaultAppBar(
          title: tr("appointment"), backgroundBack: MyColors.white),
      body: BlocBuilder<GenericBloc<AppointmentDetails?>,
              GenericState<AppointmentDetails?>>(
          bloc: dashboardDetailsData.detailsCubit,
          builder: (context, state) {
            if (state is GenericUpdateState) {
              return Column(
                children: [
                  Flexible(
                    child: ListView(
                      children: [
                        BuildHeaderDetails(
                          model: device,
                          appointmentOrdersModel: widget.appointmentOrdersModel,
                          appointmentDetailsModel: state.data!,
                          dashboardDetailsData: dashboardDetailsData,
                        ),
                        BuildBookedServices(
                          businessID: widget.appointmentOrdersModel.businessID,
                          appointmentDetails: state.data!,
                          dashboardDetailsData: dashboardDetailsData,
                        ),
                        BuildOwnerCardDetails(
                          appointmentDetails: state.data!,
                          model: device,
                        ),
                        BuildPaymentDetails(
                          dashboardDetailsData: dashboardDetailsData,
                          model: device,
                          appointmentDetails: state.data!,
                        ),
                        BuildNotesBody(
                          listNotes: state.data!.noteListItems,
                          deviceModel: device,
                          model: widget.appointmentOrdersModel,
                          dashboardDetailsData: dashboardDetailsData,
                        )
                      ],
                    ),
                  ),
                  BuildBottomIcons(
                    appointmentOrdersModel: widget.appointmentOrdersModel,
                    deviceModel: device,
                    dashboardDetailsData: dashboardDetailsData,
                    appointmentDetails: state.data!,
                  ),
                ],
              );
            }
            return BuildDashboardDetailsLoadingView(
              appointmentOrdersModel: widget.appointmentOrdersModel,
              appointmentDetailsModel: state.data,
              dashboardDetailsData: dashboardDetailsData,
            );
          }),
    );
  }
}
