part of 'dashboard_details_imports.dart';

class DashboardDetailsData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GenericBloc<bool> checkBoxBloc = GenericBloc(true);
  final GenericBloc<AppointmentDetails?> detailsCubit = GenericBloc(null);
  final GenericBloc<double> rateCubit = GenericBloc(0);
  final GenericBloc<List<CountChatModel>> msgCountCubit = GenericBloc([]);
  final TextEditingController note = TextEditingController();
  final TextEditingController editNoteController = TextEditingController();
  final TextEditingController rateMsg = TextEditingController();
  final GenericBloc<String> imageCubit = GenericBloc("");
  int status = 0;

  Future<void> ficheData(
      BuildContext context, AppointmentOrdersModel model) async {
    var laLong =
        await getIt<Utilities>().getCurrentLocation(context, showLoader: false);
    AppointmentDetailsParams params = _appointmentDetailsParams(model, laLong);
    var data = await GetAppointmentDetails()(params);
    if (data != null) {
      _handleServicesList(data);
      detailsCubit.onUpdateData(data);
      context.read<ReservationDetailsCubit>().onUpdateReservationData(null);
    }
  }

  AppointmentDetailsParams _appointmentDetailsParams(
      AppointmentOrdersModel model, LocationData? laLong) {
    var params = AppointmentDetailsParams(
      businessID: model.businessID,
      longitude: laLong?.longitude ?? 0,
      latitude: laLong?.latitude ?? 0,
      id: model.bookingOrderID,
      appointmentType: model.appointmentType,
    );
    return params;
  }

  void _handleServicesList(AppointmentDetails data) {
    data.manageVisitListItems = data.manageVisitListItems
        .where((element) => element.cancelled == false)
        .toList();
    data.noteListItems.reversed.toList();
    if (status == 3) {
      data.manageVisitListItems.removeWhere((e) => e.statusId != 3);
    } else {
      data.manageVisitListItems.removeWhere((e) => e.statusId == 3);
    }
  }

  void bottomSheetAddNote(BuildContext context,
      DashboardDetailsData dashboardDetailsData, AppointmentOrdersModel model) {
    Widget chile = BuildBottomSheet(
      model: model,
      dashboardDetailsData: dashboardDetailsData,
    );
    _showBottomSheet(context, chile);
  }

  void bottomSheetEditNote(BuildContext context,
      DashboardDetailsData dashboardDetailsData, NotesModel notesModel) {
    editNoteController.text = notesModel.note;
    Widget child = BuildBottomSheetEditNote(
      dashboardDetailsData: dashboardDetailsData,
      notesModel: notesModel,
    );
    _showBottomSheet(context, child);
  }

  void _showBottomSheet(BuildContext context, Widget child) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) {
        return child;
      },
    );
  }

  Future<void> updateWish(
      BuildContext context, AppontmentServices model) async {
    if (checkUserAuth(context)) {
      getIt<LoadingHelper>().showLoadingDialog();
      var params = WishParams(
          serviceID: model.serviceId,
          businessServiceID: model.businessServiceID);
      if (model.hasWishItem) {
        var update = await RemoveWishService()(params);
        if (update) {
          _updateWishCubit(model);
        }
      } else {
        var update = await UpdateWishService()(params);
        if (update) {
          _updateWishCubit(model);
        }
      }
      AutoRouter.of(context).pop();
      getIt<LoadingHelper>().dismissDialog();
    }
  }

  bool checkUserAuth(BuildContext context) {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      return true;
    } else {
      CustomToast.customAuthDialog(isSalon: false);
    }
    return false;
  }

  void _updateWishCubit(AppontmentServices model) {
    int index = detailsCubit.state.data!.manageVisitListItems.indexOf(model);
    detailsCubit.state.data!.manageVisitListItems[index].hasWishItem =
        !detailsCubit.state.data!.manageVisitListItems[index].hasWishItem;
    detailsCubit.onUpdateData(detailsCubit.state.data);
  }

  void bottomSheetDeleteCard(
      BuildContext context,
      DashboardDetailsData dashboardDetailsData,
      String id,
      int appointmentType) {
    Widget child = BuildClearBottomSheet(
        id: id,
        dashboardDetailsData: dashboardDetailsData,
        appointmentType: appointmentType);
    _showBottomSheet(context, child);
  }

  Future<void> cancelReservationItem(
    BuildContext context,
    String eDetailsId,
    String eId,
    int appointmentType,
    int index,
  ) async {
    var cubit = detailsCubit.state.data!.manageVisitListItems;
    if (cubit.length == 1) {
      _confirmationCancelOrder(context, eId, appointmentType);
    } else {
      var data = await CancelOrderDetails()(
          _cancelOrderParams(eDetailsId, appointmentType));
      if (data) {
        cubit.removeAt(index);
        cubit = cubit.where((element) => element.cancelled == false).toList();
        detailsCubit.onUpdateData(detailsCubit.state.data);
        Navigator.pop(context);
        CustomToast.showSimpleToast(
          msg: tr("success_cancel"),
          type: ToastType.success,
          title: tr("success_toast"),
        );
      }
    }
  }

  CancelOrderParams _cancelOrderParams(
          String eDetailsId, int appointmentType) =>
      CancelOrderParams(
        id: eDetailsId,
        appointmentType: appointmentType,
      );

  bottomSheetServiceOptions(
      BuildContext context,
      AppointmentDetails appointmentDetails,
      int index,
      DashboardDetailsData dashboardDetailsData,
      AppontmentServices appointmentServices) {
    Widget child = BuildBottomSheetAppointmentServiceOptions(
      appointmentDetails: appointmentDetails,
      dashboardDetailsData: dashboardDetailsData,
      appointmentServices: appointmentServices,
      index: index,
    );
    _showBottomSheet(context, child);
  }

  void _confirmationCancelOrder(
      BuildContext context, String id, int appointmentType) {
    var device = context.read<DeviceCubit>().state.model;
    CustomToast.customConfirmDialog(
      onConfirm: () async {
        await _onConfirmCancelOrder(id, appointmentType);
      },
      height: device.isTablet ? 170.h : 150.h,
      recordItem: MyText(
          title: tr("cancel_order"),
          color: MyColors.black,
          size: device.isTablet ? 5.5.sp : 10.sp),
      content: tr("remove_last_service"),
      title: tr("Cancel_Order"),
      btnName: tr("cancel"),
    );
  }

  Future<void> _onConfirmCancelOrder(String id, int appointmentType) async {
    BuildContext context = getIt<GlobalContext>().context();
    await _cancelOrder(id, appointmentType);
    await AutoRouter.of(context).pop();
    await AutoRouter.of(context).pop();
    AutoRouter.of(context).pop(id);
    CustomToast.showSimpleToast(
      msg: tr("success_cancel_order"),
      type: ToastType.success,
      title: tr("success_toast"),
    );
  }

  Future<void> cancelOrder(
      BuildContext context, String id, int appointmentType) async {
    bool result = await _cancelOrder(id, appointmentType);
    if (result) {
      AutoRouter.of(context)
          .pop()
          .then((value) => AutoRouter.of(context).pop(id));
      CustomToast.showSimpleToast(
        msg: tr("success_cancel_order"),
        type: ToastType.success,
        title: tr("success_toast"),
      );
    }
  }

  // void successToast(String msg) {
  //   CustomToast.showSimpleToast(
  //     msg: msg,
  //     type: ToastType.success,
  //     title: tr("success_toast"),
  //   );
  // }

  Future<bool> _cancelOrder(String id, int appointmentType) async {
    var params = _cancelOrderParams(id, appointmentType);
    var result = await CancelOrder()(params);
    if (result) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> getAppointmentImage(String businessId,
      {bool refresh = true}) async {
    var params = BusinessImagesParams(
        businessId: businessId, refresh: refresh, firstOnly: true);
    var data = await GetBusinessImage()(params);
    imageCubit.onUpdateData(data);
  }

  Future<void> addNote(
      BuildContext context, AppointmentOrdersModel model) async {
    if (note.text.isEmpty) {
      return;
    }
    var params =
        AddNoteParams(orderID: model.bookingOrderID, noteTitle: [note.text]);
    var data = await AddNote()(params);
    if (data) {
      AutoRouter.of(context).pop();
      ficheData(context, model);
      note.clear();
      CustomToast.showSimpleToast(
        msg: tr("success_add_note"),
        type: ToastType.success,
        title: tr("success_toast"),
      );
    }
  }

  Future<void> editNote(BuildContext context, NotesModel notesModel) async {
    var params =
        EditNoteParams(id: notesModel.id, note: editNoteController.text);
    var result = await EditNote()(params);
    if (result) {
      _updateListNote(notesModel);
      AutoRouter.of(context).pop();
      CustomToast.showSimpleToast(
          msg: tr("success_add_note"),
          type: ToastType.success,
          title: tr("success_toast"));
    }
  }

  void _updateListNote(NotesModel notesModel) {
    int index = detailsCubit.state.data!.noteListItems.indexOf(notesModel);
    detailsCubit.state.data!.noteListItems[index].note =
        editNoteController.text;
    detailsCubit.onUpdateData(detailsCubit.state.data);
  }

  removeNote(String notId, int index) async {
    var data = await RemoveNote()(notId);
    if (data) {
      detailsCubit.state.data!.noteListItems.removeAt(index);
      detailsCubit.onUpdateData(detailsCubit.state.data);
      CustomToast.showSimpleToast(
        msg: tr("success_remove"),
        type: ToastType.success,
        title: tr("success_toast"),
      );
    }
  }

  removeAllNote(BuildContext context, String orderId) async {
    var data = await RemoveAllNote()(orderId);
    if (data) {
      detailsCubit.state.data!.noteListItems = [];
      detailsCubit.onUpdateData(detailsCubit.state.data);
      AutoRouter.of(context).pop();
      CustomToast.showSimpleToast(
        msg: tr("success_remove"),
        type: ToastType.success,
        title: tr("success_toast"),
      );
    }
  }

  clearAllAlert(BuildContext context, DashboardDetailsData dashboardDetailsData,
      String orderId) {
    var device = context.read<DeviceCubit>().state.model;
    showDialog(
        context: context,
        builder: (cxt) {
          return BuildAlertClearNote(
            dashboardDetailsData: dashboardDetailsData,
            orderId: orderId,
            device: device,
          );
        });
  }

  String timeFromTo(AppointmentDetails? appointmentDetailsModel) {
    if (appointmentDetailsModel != null &&
        appointmentDetailsModel.manageVisitListItems.isNotEmpty) {
      return "${appointmentDetailsModel.manageVisitListItems.first.fromTime.split(":").sublist(0, 2).join(":")} - ${appointmentDetailsModel.manageVisitListItems.last.toTime.split(":").sublist(0, 2).join(":")}";
    } else if (appointmentDetailsModel != null &&
        appointmentDetailsModel.manageVisitListItems.isEmpty) {
      return "";
    } else {
      return "00:00 - 00:00";
    }
  }

  showConfirmCancelDialog(
      BuildContext context,
      String id,
      int appointmentType,
      AppointmentOrdersModel appointmentOrdersModel,
      AppointmentDetails appointmentDetails,
      DashboardDetailsData dashboardDetailsData) {
    var device = context.read<DeviceCubit>().state.model;
    CustomToast.customConfirmDialog(
      height: device.isTablet ? 210.h : 200.h,
      recordItem: BuildDeletedReservationItem(
        appointmentOrdersModel: appointmentOrdersModel,
        dashboardDetailsData: dashboardDetailsData,
        appointmentDetails: appointmentDetails,
      ),
      onConfirm: () => cancelOrder(context, id, appointmentType),
      content: tr("sure_cancel"),
      btnName: tr("cancel"),
      title: tr("Cancel_appointment"),
    );
  }

  showContactDialog(
      BuildContext context,
      AppointmentOrdersModel appointmentOrdersModel,
      DashboardDetailsData dashboardDetailsData) {
    Widget _child = BuildContactBottomSheet(
      appointmentOrdersModel: appointmentOrdersModel,
      dashboardDetailsData: dashboardDetailsData,
    );
    _showBottomSheet(context, _child);
  }

  String formatDate(
      DateTime? dateTime, Locale locale, String format, DateFormat formatter) {
    String date = "";
    if (locale == Locale('en', 'US')) {
      date = DateFormat(format).format(dateTime ?? DateTime(0));
    } else {
      date = formatter.format(dateTime ?? DateTime(0));
    }
    return date;
  }

  void showRateSheet(AppontmentServices appointmentServices) {
    BuildContext context = getIt<GlobalContext>().context();
    showModalBottomSheet(
        context: context,
        backgroundColor: MyColors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15),
          ),
        ),
        builder: (cxt) {
          return BuildServicesRateDialog(
            appointmentServices: appointmentServices,
            dashboardDetailsData: this,
            email: detailsCubit.state.data!.email,
          );
        });
  }

  Future<void> createRateServices(
      AppontmentServices service, String email) async {
    if (formKey.currentState!.validate()) {
      await _applyRating(_buildRateParams(service, email));
    }
  }

  Future<void> _applyRating(ServicesRateParams params) async {
    BuildContext context = getIt<GlobalContext>().context();
    var result = await AddServicesRate()(params);
    if (result) {
      rateMsg.clear();
      AutoRouter.of(context).pop();
      CustomToast.showSimpleToast(
        msg: tr("Success_Rating"),
        type: ToastType.success,
        title: tr("success_toast"),
      );
    }
  }

  ServicesRateParams _buildRateParams(
      AppontmentServices appontmentServices, String email) {
    return ServicesRateParams(
      ratingID: rateCubit.state.data.toInt(),
      ratingComments: rateMsg.text,
      email: email,
      appointmentId: appontmentServices.appointmentId,
      businessServiceId: appontmentServices.businessServiceID,
      isHidden: true,
      id: appontmentServices.serviceId,
    );
  }

  Future<void> getMsgCount(String orderNumber, String businessID) async {
    BuildContext context = getIt<GlobalContext>().context();
    var device = context.read<DeviceCubit>().state.model;
    if (device.auth) {
      var params = MsgCountParams(
        orderNumber: orderNumber,
        businessID: businessID,
      );
      var data = await GetMsgCount()(params);
      msgCountCubit.onUpdateData(data);
    }
  }

  void navigateToRescheduleScreen(AppointmentDetails model) {
    BuildContext context = getIt<GlobalContext>().context();
    var services = model.manageVisitListItems
        .map((e) => _rescheduleParams(e, model))
        .where((element) => element.businessServiceId.isNotEmpty)
        .toList();
    if (services.isNotEmpty) {
      AutoRouter.of(context).push(
        RescheduleAppointmentRoute(
            businessID: model.businessID,
            services: services,
            orderNumber: model.orderNumber,
            lastName: model.lastName),
      );
    } else {
      CustomToast.showSimpleToast(
        msg: tr("can't_reschedule"),
        type: ToastType.info,
        title: tr("reschedule_info"),
      );
      return;
    }
  }

  RescheduleParams _rescheduleParams(
      AppontmentServices e, AppointmentDetails model) {
    if (e.statusId != 1) {
      return RescheduleParams(
        id: e.appointmentId,
        businessId: model.businessID,
        appointmentType: model.appointmentType,
        staffID: e.staffID,
        businessServiceId: e.businessServiceID,
        // businessServiceId: e.serviceId,
        appointmentDate: "",
        businessServiceName: e.getServiceName(),
        fromTime: "",
      );
    } else {
      return RescheduleParams(
        id: e.appointmentId,
        businessId: "",
        appointmentType: 0,
        staffID: "",
        businessServiceId: "",
        appointmentDate: "",
        businessServiceName: "",
        fromTime: "",
      );
    }
  }
}
