part of 'dashboard_details_widget_imports.dart';

class BuildChatItem extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final DashboardDetailsData dashboardDetailsData;
  final DeviceModel deviceModel;

  const BuildChatItem(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.dashboardDetailsData,
      required this.deviceModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (deviceModel.auth) {
          AutoRouter.of(context).push(
              ChatRoomRoute(appointmentOrdersModel: appointmentOrdersModel));
        } else {
          CustomToast.customAuthDialog();
        }
      },
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            BuildChatIcon(
                deviceModel: deviceModel,
                appointmentOrdersModel: appointmentOrdersModel),
            SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    MyText(
                        title: tr("Chat_With_Us"),
                        color: MyColors.black,
                        size: deviceModel.isTablet ? 7.sp : 11.sp),
                    BlocBuilder<GenericBloc<List<CountChatModel>>,
                            GenericState<List<CountChatModel>>>(
                        bloc: dashboardDetailsData.msgCountCubit,
                        builder: (context, state) {
                          if (state is GenericUpdateState &&
                              state.data.isNotEmpty &&
                              state.data.firstOrNull
                                      ?.serverUnReadMessageCount !=
                                  0) {
                            return Container(
                              padding: EdgeInsets.all(3.5.sp),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: MyColors.errorColor),
                              child: MyText(
                                color: MyColors.white,
                                size: deviceModel.isTablet ? 6.sp : 9.sp,
                                title:
                                    "${state.data.firstOrNull?.serverUnReadMessageCount ?? 0}",
                              ),
                            );
                          } else {
                            return Container();
                          }
                        }),
                  ],
                ),
                MyText(
                    title: tr("Direct_Conversation"),
                    color: MyColors.grey,
                    size: deviceModel.isTablet ? 5.5.sp : 9.sp),
              ],
            ),
            Spacer(),
            Icon(
              Icons.arrow_forward,
              color: MyColors.primary,
              size: deviceModel.isTablet ? 12.sp : 17.sp,
            )
          ],
        ),
      ),
    );
  }
}
