part of 'dashboard_details_widget_imports.dart';

class BuildHeaderOrderDetails extends StatelessWidget {
  final DeviceModel model;
  final DashboardDetailsData dashboardDetailsData;
  final AppointmentOrdersModel appointmentOrdersModel;

  const BuildHeaderOrderDetails(
      {Key? key,
      required this.model,
      required this.appointmentOrdersModel,
      required this.dashboardDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15).r,
      decoration: BoxDecoration(
        border: Border.all(
            color: appointmentOrdersModel.statusId.getStatusBgDateColor()),
        borderRadius: BorderRadius.vertical(top: Radius.circular(11.r)),
        color: appointmentOrdersModel.statusId.getStatusBgDateColor(),
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsetsDirectional.only(start: 25.r),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(Res.receipt,
                        color: appointmentOrdersModel.statusId
                            .getStatusTextColor(),
                        height: 12.h,
                        width: 12.h),
                    SizedBox(width: 5),
                    MyText(
                        title: tr("ref"),
                        color: appointmentOrdersModel.statusId
                            .getStatusTextColor(),
                        size: model.isTablet ? 5.5.sp : 9.sp),
                  ],
                ),
                GestureDetector(
                  onTap: () => getIt<Utilities>()
                      .copyToClipBoard(appointmentOrdersModel.orderNumber),
                  child: MyText(
                    title: appointmentOrdersModel.orderNumber,
                    color: appointmentOrdersModel.statusId.getStatusTextColor(),
                    size: model.isTablet ? 8.sp : 11.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 7),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.access_time_rounded,
                      color:
                          appointmentOrdersModel.statusId.getStatusTextColor(),
                      size: model.isTablet ? 10.sp : 15.sp,
                    ),
                    SizedBox(width: 5),
                    MyText(
                      title: tr("reserved_on"),
                      color:
                          appointmentOrdersModel.statusId.getStatusTextColor(),
                      size: model.isTablet ? 5.5.sp : 9.sp,
                    ),
                  ],
                ),
                Row(
                  children: [
                    MyText(
                      title: dashboardDetailsData.formatDate(
                          appointmentOrdersModel.bookingDate,
                          model.locale,
                          "d MMM yyyy",
                          DateFormat.yMMMd('ar_SA')),
                      color:
                          appointmentOrdersModel.statusId.getStatusTextColor(),
                      size: model.isTablet ? 7.sp : 9.sp,
                    ),
                    MyText(
                      title: " ${tr("at").toLowerCase()} ",
                      color:
                          appointmentOrdersModel.statusId.getStatusTextColor(),
                      size: model.isTablet ? 7.sp : 9.sp,
                    ),
                    MyText(
                      title: DateFormat(
                              "HH:mm",
                              model.locale == Locale('en', 'US')
                                  ? "en_US"
                                  : 'ar_SA')
                          .format(appointmentOrdersModel.bookingDate),
                      color:
                          appointmentOrdersModel.statusId.getStatusTextColor(),
                      size: model.isTablet ? 7.sp : 9.sp,
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
