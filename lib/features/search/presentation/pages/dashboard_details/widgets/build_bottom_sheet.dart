part of 'dashboard_details_widget_imports.dart';

class BuildBottomSheet extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
   final AppointmentOrdersModel model;

  const BuildBottomSheet({Key? key, required this.dashboardDetailsData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.sm,horizontal: 16),
        decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.r),),),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              margin: const EdgeInsets.only(bottom: 15).r,
              ladingIcon: SvgPicture.asset(
                Res.comment,
                width: 13.h,
                height: 13.h,
              ),
              title: "  ${tr("add_note_for_salon")}",
            ),
            CustomTextField(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              max: 4,
              fillColor: Color(0xffF6F9F9),
              controller: dashboardDetailsData.note,
              fieldTypes: FieldTypes.rich,
              type: TextInputType.multiline,
              action: TextInputAction.done,
              contentPadding: EdgeInsets.all(10),
              validate: (value) => value?.validateEmpty(),
            ),
            Row(
              children: [
                Expanded(
                  child: DefaultButton(
                    onTap: ()=> AutoRouter.of(context).pop(),
                    title: tr("cancel"),
                    color: MyColors.black.withOpacity(.05),
                    textColor: MyColors.blackOpacity,
                    borderColor: MyColors.white,
                    borderRadius: BorderRadius.circular(30.r),
                    margin: EdgeInsets.zero,
                    fontSize: device.isTablet ? 7.sp : 11.sp,

                    height:device.isTablet?60.sm: 50.sm,
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: DefaultButton(
                    onTap: () => dashboardDetailsData.addNote(context,model),
                    title: tr("add_note"),
                    color: MyColors.primary,
                    textColor: MyColors.white,
                    borderColor: MyColors.white,
                    borderRadius: BorderRadius.circular(30.r),
                    margin: EdgeInsets.zero,
                    fontSize: device.isTablet ? 7.sp : 11.sp,

                    height:device.isTablet?60.sm: 50.sm,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
