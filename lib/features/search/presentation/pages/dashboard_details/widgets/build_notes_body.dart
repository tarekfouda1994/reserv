part of 'dashboard_details_widget_imports.dart';

class BuildNotesBody extends StatelessWidget {
  final DeviceModel deviceModel;
  final DashboardDetailsData dashboardDetailsData;
  final AppointmentOrdersModel model;
  final List<NotesModel> listNotes;

  const BuildNotesBody(
      {Key? key,
      required this.deviceModel,
      required this.dashboardDetailsData,
      required this.listNotes, required this.model,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsetsDirectional.only(start: 5.0, end: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              BuildHeaderTitle(title: tr("notes"), count: 0),
              InkWell(
                onTap: () => dashboardDetailsData.bottomSheetAddNote(context,
                    dashboardDetailsData, model),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: MyText(
                    title: "+ ${tr("add_note_for_salon")}",
                    color: MyColors.black,
                    size: deviceModel.isTablet ? 5.sp : 9.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          ),
        ),
        Visibility(
          visible: listNotes.isNotEmpty,
          child: Container(
            margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 16.h),
            padding: EdgeInsets.all(16.0.r),
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.circular(13.r),
              border:
                  Border.all(width: .1, color: MyColors.grey.withOpacity(.5)),
            ),
            child: Column(children: [
              ...List.generate(listNotes.length, (index) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 16),
                        child: MyText(
                            title: listNotes[index].note,
                            color: MyColors.blackOpacity,
                            size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 25.w),
                      child: InkWell(
                          onTap: () => dashboardDetailsData.removeNote(
                              listNotes[index].id, index),
                          child: SvgPicture.asset(Res.trash,
                              color: MyColors.black)),
                    ),
                    InkWell(
                        onTap: () => dashboardDetailsData.bottomSheetEditNote(
                            context, dashboardDetailsData, listNotes[index]),
                        child:
                            SvgPicture.asset(Res.edit, color: MyColors.black)),
                  ],
                );
              }),
            ]),
          ),
        ),
      ],
    );
  }
}
