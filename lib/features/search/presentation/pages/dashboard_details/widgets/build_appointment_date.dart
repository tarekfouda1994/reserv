part of 'dashboard_details_widget_imports.dart';

class BuildAppointmentDate extends StatelessWidget {
  final DeviceModel model;
  final AppointmentOrdersModel appointmentOrdersModel;
  final AppointmentDetails? appointmentDetailsModel;
  final DashboardDetailsData dashboardDetailsData;

  const BuildAppointmentDate(
      {Key? key,
      required this.model,
      required this.appointmentOrdersModel,
      this.appointmentDetailsModel,
      required this.dashboardDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Transform.scale(
            scale: .9,
            child: SvgPicture.asset(
                appointmentOrdersModel.statusId.getGroupIcon()),
          ),
          SizedBox(width: 7),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  title: dashboardDetailsData.formatDate(
                    appointmentOrdersModel.activeAppDateTime,
                    model.locale,
                    "EEEE, d MMM yyyy",
                    DateFormat.yMMMMEEEEd('ar_SA'),
                  ),
                  color: MyColors.black,
                  size: model.isTablet ? 11.sp : 14.5.sp,
                  fontWeight: FontWeight.bold),
              SizedBox(height: 3.h),
              Visibility(
                visible: dashboardDetailsData
                    .timeFromTo(appointmentDetailsModel)
                    .isNotEmpty,
                child: MyText(
                  title:getIt<Utilities>().convertNumToAr(context: context, value: dashboardDetailsData.timeFromTo(appointmentDetailsModel)),
                  color: appointmentOrdersModel.statusId.getStatusBgColor(),
                  size: model.isTablet ? 8.5.sp : 13.sp,
                  fontFamily: CustomFonts.primarySemiBoldFont,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
