part of 'dashboard_details_widget_imports.dart';

class BuildFrontCard extends StatelessWidget {
  final DeviceModel model;
  final String currencyName;
  final int index;
  final AppontmentServices appontmentServices;
  final DashboardDetailsData dashboardDetailsData;
  final String businessID;
  final AppointmentDetails appointmentDetails;

  const BuildFrontCard({
    Key? key,
    required this.model,
    required this.appontmentServices,
    required this.currencyName,
    required this.index,
    required this.dashboardDetailsData,
    required this.businessID,
    required this.appointmentDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 16.h, top: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () => dashboardDetailsData.bottomSheetServiceOptions(
                    context,
                    appointmentDetails,
                    index,
                    dashboardDetailsData,
                    appontmentServices),
                child: Padding(
                  padding: EdgeInsets.only(left: 8.w, right: 8.w, top: 8),
                  child: SvgPicture.asset(Res.dots),
                ),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  if (appontmentServices.businessServiceImage.isNotEmpty)
                    CachedImage(
                      url: appontmentServices.businessServiceImage
                          .replaceAll("\\", "/"),
                      width: model.isTablet ? 35.w : 50.w,
                      height: model.isTablet ? 35.w : 50.w,
                      fit: BoxFit.cover,
                      bgColor: MyColors.defaultImgBg,
                      placeHolder: ServicePlaceholder(),
                      borderRadius: BorderRadius.circular(12.r),
                    ),
                  SizedBox(width: 10.w),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                            title: appontmentServices.getServiceName(),
                            color: MyColors.primary,
                            size: model.isTablet ? 7.sp : 11.sp,
                            fontWeight: FontWeight.bold),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 5.h),
                          child: Row(
                            children: [
                              MyText(
                                title:
                                    "$currencyName ${getIt<Utilities>().convertNumToAr(context: context, value: "${appontmentServices.amount}")}",
                                color: MyColors.black,
                                size: model.isTablet ? 5.5.sp : 9.sp,
                                fontFamily: CustomFonts.primarySemiBoldFont,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.access_time,
                                    color: MyColors.greyLight,
                                    size: model.isTablet ? 14.sp : 17.sp,
                                  ),
                                  MyText(
                                    title:
                                      getIt<Utilities>().convertNumToAr(context: context, value: " ${appontmentServices.serviceDuration} ${tr("min")} "),
                                    color: MyColors.grey,
                                    size: model.isTablet ? 5.5.sp : 9.sp,
                                  ),
                                ],
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsetsDirectional.only(
                                          end: 3, start: 25.w),
                                      padding: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: MyColors.greyLight,
                                      ),
                                    ),
                                    Expanded(
                                      child: MyText(
                                        title: appontmentServices
                                                    .businessServiceGender ==
                                                "Male"
                                            ? tr("men")
                                            : tr("women"),
                                        color: MyColors.grey,
                                        size: model.isTablet ? 5.5.sp : 9.sp,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Visibility(
                                visible: appontmentServices
                                    .getStaffName()
                                    .isNotEmpty,
                                child: Row(
                                  children: [
                                    SvgPicture.asset(Res.user_icon,
                                        color: MyColors.grey.withOpacity(.4),
                                        width: 10.h,
                                        height: 10.h),
                                    MyText(
                                      title:
                                          " ${appontmentServices.getStaffName()}",
                                      color: MyColors.grey,
                                      size: model.isTablet ? 5.5.sp : 9.sp,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // Row(
                            //   children: [
                            //     Container(
                            //       margin:
                            //       EdgeInsetsDirectional.only(end: 5),
                            //       height: 5.h,
                            //       width: 5.h,
                            //       decoration: BoxDecoration(
                            //         color: appontmentServices.statusId
                            //             .getDashboardStatusTextColor(),
                            //         shape: BoxShape.circle,
                            //       ),
                            //       alignment: Alignment.center,
                            //     ),
                            //     MyText(
                            //       alien: TextAlign.center,
                            //       size: model.isTablet ? 5.5.sp : 9.sp,
                            //       fontWeight: FontWeight.bold,
                            //       title: appontmentServices
                            //           .getStatus(),
                            //       color: appontmentServices.statusId
                            //           .getDashboardStatusTextColor(),
                            //     )
                            //   ],
                            // ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
