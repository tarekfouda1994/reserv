part of 'dashboard_details_widget_imports.dart';

class BuildServicesRateDialog extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final AppontmentServices appointmentServices;
  final String email;

  const BuildServicesRateDialog(
      {Key? key,
      required this.dashboardDetailsData,
      required this.appointmentServices,
      required this.email})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    dashboardDetailsData.rateCubit.onUpdateData(4);
    return Container(
      padding: EdgeInsets.all(14),
      margin: MediaQuery.of(context).viewInsets,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        BuildHeaderBottomSheet(title: tr("addRating")),
          SizedBox(height: 12),
        BlocBuilder<GenericBloc<num>, GenericState<num>>(
            bloc: dashboardDetailsData.rateCubit,
            builder: (context, state) {
              return RatingBar.builder(
                glowColor: MyColors.primary,
                itemSize: 25.sp,
                initialRating: state.data.toDouble(),
                minRating: 1,
                unratedColor: MyColors.grey.withOpacity(.2),
                direction: Axis.horizontal,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 3),
                itemBuilder: (context, _) =>
                    SvgPicture.asset(Res.star, color: MyColors.amber),
                onRatingUpdate: (rating) {
                  dashboardDetailsData.rateCubit.onUpdateData(rating);
                },
              );
            }),
        Form(
          key: dashboardDetailsData.formKey,
          child: CustomTextField(
            controller: dashboardDetailsData.rateMsg,
            max: 3,
            fieldTypes: FieldTypes.rich,
            type: TextInputType.text,
            action: TextInputAction.done,
            validate: (value) => value?.validateEmpty(),
            hint: "${tr("ex")} ${tr("any_message")}",
            autoValidateMode: AutovalidateMode.onUserInteraction,
            margin: EdgeInsets.only(bottom: 15, top: 20),
          ),
        ),
        DefaultButton(
          margin: EdgeInsets.zero,
          title: tr("confirm"),
          onTap: () => dashboardDetailsData.createRateServices(
            appointmentServices,
            email,
          ),
        )
      ]),
    );
  }
}
