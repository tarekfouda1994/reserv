part of 'dashboard_details_widget_imports.dart';

class BuildReservedItem extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final String currencyName;
  final int index;
  final String businessID;
  final DeviceModel model;
  final AppointmentDetails appointmentDetails;

  const BuildReservedItem(
      {Key? key,
      required this.dashboardDetailsData,
      required this.model,
      required this.appointmentDetails,
      required this.currencyName,
      required this.index,
      required this.businessID})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 3.h),
      child: BuildFrontCard(
        index: index,
        model: model,
        dashboardDetailsData: dashboardDetailsData,
        businessID: businessID,
        appointmentDetails: appointmentDetails,
        appontmentServices: appointmentDetails.manageVisitListItems[index],
        currencyName: currencyName,
      ),
    );
  }
}
