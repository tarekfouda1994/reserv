part of 'dashboard_details_widget_imports.dart';

class BuildDeletedReservationItem extends StatefulWidget {
  final DashboardDetailsData dashboardDetailsData;
  final AppointmentOrdersModel appointmentOrdersModel;
  final AppointmentDetails appointmentDetails;

  const BuildDeletedReservationItem(
      {Key? key,
      required this.dashboardDetailsData,
      required this.appointmentOrdersModel,
      required this.appointmentDetails})
      : super(key: key);

  @override
  State<BuildDeletedReservationItem> createState() =>
      _BuildDeletedReservationItemState();
}

class _BuildDeletedReservationItemState
    extends State<BuildDeletedReservationItem> {
  @override
  void initState() {
    widget.dashboardDetailsData.getAppointmentImage(
        widget.appointmentOrdersModel.businessID,
        refresh: false);
    widget.dashboardDetailsData
        .getAppointmentImage(widget.appointmentOrdersModel.businessID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 8.h, horizontal: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: MyColors.blackOpacity.withOpacity(.02)),
      child: Row(
        children: [
          BlocBuilder<GenericBloc<String>, GenericState<String>>(
              bloc: widget.dashboardDetailsData.imageCubit,
              builder: (context, state) {
                if (state is GenericUpdateState) {
                  return CachedImage(
                      url: "".replaceAll("\\", "/"),
                      width: deviceModel.isTablet ? 35.w : 50.w,
                      height: deviceModel.isTablet ? 35.w : 50.w,
                      borderRadius: BorderRadius.circular(12.r),
                      fit: BoxFit.cover,
                      bgColor: MyColors.defaultImgBg,
                      placeHolder: BusinessPlaceholder());
                } else {
                  return BuildShimmerView(
                      width: 90.w, height: deviceModel.isTablet ? 35.w : 50.w);
                }
              }),
          SizedBox(width: 10.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Flexible(
                      child: MyText(
                        title: widget.appointmentOrdersModel.getBusinessName(),
                        color: MyColors.black,
                        fontWeight: FontWeight.bold,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: deviceModel.isTablet ? 6.w : 8.w,
                          vertical: deviceModel.isTablet ? 5.h : 4.5.h),
                      margin: EdgeInsetsDirectional.only(start: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: widget.appointmentOrdersModel.statusId
                            .getStatusBgDateColor(),
                        border: Border.all(
                          color: widget.appointmentOrdersModel.statusId
                              .getStatusBgDateColor(),
                        ),
                      ),
                      child: MyText(
                        alien: TextAlign.center,
                        size: deviceModel.isTablet ? 6.sp : 9.sp,
                        title: widget.appointmentOrdersModel.getStatus(),
                        color: widget.appointmentOrdersModel.statusId
                            .getStatusTextColor(),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 2.h),
                Row(
                  children: [
                    Icon(
                      Icons.access_time,
                      color: MyColors.grey,
                      size: deviceModel.isTablet ? 12.sp : 15.sp,
                    ),
                    MyText(
                      title: getIt<Utilities>().convertNumToAr(
                        context: context,
                        value: " " +
                            widget.dashboardDetailsData
                                .timeFromTo(widget.appointmentDetails),
                      ),
                      color: MyColors.blackOpacity,
                      size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
