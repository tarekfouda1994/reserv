part of 'dashboard_details_widget_imports.dart';

class BuildActionsAppBarr extends StatelessWidget {
  final DeviceModel model;
  final AppointmentOrdersModel appointmentOrdersModel;

  const BuildActionsAppBarr(
      {Key? key, required this.model, required this.appointmentOrdersModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: model.isTablet ? 6.w : 8.w,
          vertical: model.isTablet ? 5.h : 4.5.h),
      margin: EdgeInsetsDirectional.only(
          top: model.isTablet ? 13.h : 11.h,
          bottom: model.isTablet ? 4.h : 6.h,
          end: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: appointmentOrdersModel.statusId.getStatusBgColor(),
          border: Border.all(
              color: appointmentOrdersModel.statusId.getStatusTextColor())),
      child: MyText(
          alien: TextAlign.center,
          size: model.isTablet ? 6.sp : 9.sp,
          title: appointmentOrdersModel.getStatus(),
          color: appointmentOrdersModel.statusId.getStatusTextColor()),
    );
  }
}
