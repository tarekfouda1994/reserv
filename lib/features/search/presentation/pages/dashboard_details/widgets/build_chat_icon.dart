part of 'dashboard_details_widget_imports.dart';

class BuildChatIcon extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final DeviceModel deviceModel;

  const BuildChatIcon(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.deviceModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildIconItem(
        onTap: () {
          if (deviceModel.auth) {
            AutoRouter.of(context).push(ChatRoomRoute(
                appointmentOrdersModel: appointmentOrdersModel));
          } else {
            CustomToast.customAuthDialog();
          }
        },
        img: Transform.scale(
          scale: .8,
          child: SvgPicture.asset(Res.comment_alt,
              width: deviceModel.isTablet ? 20.sp : 21.h,
              height: deviceModel.isTablet ? 20.sp : 21.h),
        ));
  }
}
