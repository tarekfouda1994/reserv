part of 'dashboard_details_widget_imports.dart';

class BuildOwnerCardDetails extends StatelessWidget {
  final DeviceModel model;
  final AppointmentDetails appointmentDetails;

  const BuildOwnerCardDetails(
      {Key? key, required this.model, required this.appointmentDetails})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = context.watch<UserCubit>().state.model;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: BuildHeaderTitle(title: tr("person_details"), count: 0),
        ),
        Container(
          margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 10.h),
          padding: EdgeInsets.all(16.0.r),
          decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(13.r),
            border: Border.all(width: .1, color: MyColors.grey.withOpacity(.5)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  if (user?.photo.isNotEmpty ?? false)
                    CachedImage(
                      url: user!.photo.replaceAll("\\", "/"),
                      width: model.isTablet ? 28.w : 35.w,
                      height: model.isTablet ? 28.w : 35.w,
                      fit: BoxFit.cover,
                      bgColor: MyColors.defaultImgBg,
                      placeHolder: StaffPlaceholder(),
                      boxShape: BoxShape.circle,
                      haveRadius: false,
                    ),
                  if (user?.photo.isEmpty ?? true)
                    SvgPicture.asset(
                      Res.staff_default_img,
                      width: model.isTablet ? 25.w : 28.w,
                      height: model.isTablet ? 25.w : 28.w,
                    ),
                  SizedBox(width: 8.h),
                  MyText(
                      title: appointmentDetails.getUserName(),
                      color: MyColors.black,
                      size: model.isTablet ? 7.sp : 11.sp),
                ],
              ),
              SizedBox(height: 5.h),
              if (appointmentDetails.contactNumber.isNotEmpty)
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 9),
                      child: SvgPicture.asset(
                        Res.smartphone,
                        width: 16.h,
                        height: 16.h,
                      ),
                    ),
                    SizedBox(width: 8.h),
                    Directionality(
                      textDirection: TextDirection.ltr,
                      child: MyText(
                        title: appointmentDetails.contactNumber,
                        color: MyColors.blackOpacity,
                        size: model.isTablet ? 5.5.sp : 9.sp,
                      ),
                    ),
                  ],
                ),
              SizedBox(height: 5.h),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 9),
                    child: SvgPicture.asset(
                      Res.envelope,
                      width: 13.h,
                      height: 13.h,
                    ),
                  ),
                  SizedBox(width: 8.h),
                  MyText(
                    title: appointmentDetails.email,
                    color: MyColors.blackOpacity,
                    size: model.isTablet ? 5.5.sp : 9.sp,
                  ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
