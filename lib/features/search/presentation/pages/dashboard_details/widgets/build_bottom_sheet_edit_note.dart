part of 'dashboard_details_widget_imports.dart';

class BuildBottomSheetEditNote extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final NotesModel notesModel;

  const BuildBottomSheetEditNote(
      {Key? key, required this.dashboardDetailsData, required this.notesModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16,vertical: 20.sm),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              margin: const EdgeInsets.only(bottom: 15).r,
              ladingIcon: SvgPicture.asset(
                Res.comment,
                width: 13.h,
                height: 13.h,
              ),
              title: "  ${tr("edit_note")}",
            ),
            CustomTextField(
              label: tr("notes"),
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              max: 3,
              controller: dashboardDetailsData.editNoteController,
              fieldTypes: FieldTypes.rich,
              type: TextInputType.text,
              action: TextInputAction.done,
              validate: (value) => value?.validateEmpty(),
            ),
            Row(
              children: [
                Expanded(
                  child: DefaultButton(
                    color: MyColors.black.withOpacity(.05),
                    textColor: MyColors.blackOpacity,
                    borderColor: MyColors.white,
                    borderRadius: BorderRadius.circular(30.r),
                    margin: EdgeInsets.zero,
                    fontSize: device.isTablet ? 7.sp : 11.sp,

                    height:device.isTablet?60.sm: 50.sm,
                    title: tr("keep"),
                    onTap: ()=> AutoRouter.of(context).pop(),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: DefaultButton(
                    color: MyColors.primary,
                    textColor: MyColors.white,
                    borderColor: MyColors.white,
                    borderRadius: BorderRadius.circular(30.r),
                    margin: EdgeInsets.zero,
                    fontSize: device.isTablet ? 7.sp : 11.sp,

                    height:device.isTablet?60.sm: 50.sm,
                    title: tr("Edit_note"),
                    onTap: () => dashboardDetailsData.editNote(context,notesModel),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
