part of 'dashboard_details_widget_imports.dart';

class BuildAlertClearNote extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final String orderId;
  final DeviceModel device;

  const BuildAlertClearNote(
      {Key? key, required this.dashboardDetailsData, required this.orderId, required this.device})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(14),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.r)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(children: [
            MyText(
                title: " ${tr("clear_all_salon")}",
                color: MyColors.black,
                size: 11.sp,
                fontWeight: FontWeight.bold),
            Spacer(),
            BuildIconItem(
              onTap: () => AutoRouter.of(context).pop(),
              icon: Icons.clear,
              paddingIcon: 5,
              sizeIcon: device.isTablet ? 12.sp : 17.sp,
              backGroundColor: MyColors.black,
              iconColor: MyColors.white,
            )
          ]),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 25),
            child: MyText(
              title: tr("alert_before_removing"),
              color: MyColors.black,
              size: 11.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () => AutoRouter.of(context).pop(),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: MyColors.primary.withOpacity(.05),
                  ),
                  child: MyText(
                      alien: TextAlign.center,
                      size: 12,
                      title: tr("cancel"),
                      color: MyColors.primary),
                ),
              ),
              InkWell(
                onTap: () => dashboardDetailsData.removeAllNote(context,orderId),
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30), color: MyColors.primary),
                  child: MyText(
                      alien: TextAlign.center,
                      size: 12,
                      title: tr("clear_all"),
                      color: MyColors.white),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
