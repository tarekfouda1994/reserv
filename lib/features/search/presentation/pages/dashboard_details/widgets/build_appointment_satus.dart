part of 'dashboard_details_widget_imports.dart';

class BuildAppointmentStatus extends StatelessWidget {
  final DeviceModel model;
  final AppointmentOrdersModel appointmentOrdersModel;

  const BuildAppointmentStatus(
      {Key? key, required this.model, required this.appointmentOrdersModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsetsDirectional.only(start: 26.h),
      height: 33.h,
      width: MediaQuery.of(context).size.width,
      color:
          appointmentOrdersModel.statusId.getStatusBgColor().withOpacity(.08),
      child: Row(
        children: [
          Container(
            margin: EdgeInsetsDirectional.only(end: 10.r),
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: appointmentOrdersModel.statusId.getStatusBgColor()),
            child: Transform.scale(
              scale: model.isTablet ? .5 : .7,
              child: SvgPicture.asset(
                appointmentOrdersModel.statusId.getStatusIcon(),
                height: 15.w,
                width: 15,
                color: MyColors.white,
              ),
            ),
          ),
          MyText(
            title: appointmentOrdersModel.getStatus(),
            color: appointmentOrdersModel.statusId.getStatusBgColor(),
            size: model.isTablet ? 6.sp : 10.sp,
          )
        ],
      ),
    );
  }
}
