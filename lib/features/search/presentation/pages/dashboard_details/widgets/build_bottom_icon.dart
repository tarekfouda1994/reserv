part of 'dashboard_details_widget_imports.dart';

class BuildBottomIcons extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final DashboardDetailsData dashboardDetailsData;
  final DeviceModel deviceModel;
  final AppointmentDetails appointmentDetails;

  const BuildBottomIcons(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.dashboardDetailsData,
      required this.deviceModel,
      required this.appointmentDetails})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool rescheduleStatus =
        appointmentOrdersModel.services.any((element) => element.statusID == 3);
    int statusId = appointmentOrdersModel.statusId;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Divider(height: 0),
        Container(
          color: MyColors.white,
          padding: EdgeInsets.symmetric(vertical: 8.h),
          child: Row(
            mainAxisAlignment: _mainAlignCheck()
                ? MainAxisAlignment.end
                : MainAxisAlignment.spaceAround,
            children: [
              if ((statusId == 0 && !rescheduleStatus) || statusId == 5)
                BuildIconItem(
                  onTap: () => dashboardDetailsData.showConfirmCancelDialog(
                    context,
                    appointmentDetails.id,
                    appointmentOrdersModel.appointmentType,
                    appointmentOrdersModel,
                    appointmentDetails,
                    dashboardDetailsData,
                  ),
                  icon: Icons.clear,
                  backGroundColor: MyColors.white,
                  iconColor: Colors.red,
                  showTitle: true,
                  titleColor: MyColors.errorColor,
                  borderColor: MyColors.grey.withOpacity(.2),
                  title: tr("cancel"),
                ),
              BuildIconItem(
                backGroundColor: MyColors.white,
                onTap: () => dashboardDetailsData.showContactDialog(
                  context,
                  appointmentOrdersModel,
                  dashboardDetailsData,
                ),
                img: Transform.scale(
                  scale: .8,
                  child: SvgPicture.asset(Res.call_outgoing_no_background,
                      color: MyColors.black,
                      width: deviceModel.isTablet ? 20.sp : 18.h,
                      height: deviceModel.isTablet ? 20.sp : 18.h),
                ),
                showTitle: true,
                title: tr("contact"),
              ),
              if ((statusId == 0 && !rescheduleStatus) || statusId == 5)
                BuildIconItem(
                  onTap: () => dashboardDetailsData
                      .navigateToRescheduleScreen(appointmentDetails),
                  iconColor: MyColors.white,
                  backGroundColor: MyColors.white,
                  img: Transform.scale(
                    scale: .9,
                    child: Transform.scale(
                      scale: .9,
                      child: SvgPicture.asset(Res.ReserveAgain,
                          width: 16.w, height: 16.h),
                    ),
                  ),
                  paddingIcon: 6,
                  showTitle: true,
                  title: tr("reschedule"),
                ),
            ],
          ),
        )
      ],
    );
  }

  bool _mainAlignCheck() {
    int statusId = appointmentOrdersModel.statusId;
    return statusId == 1 || statusId == 4 || statusId == 3;
  }
}
