part of 'dashboard_details_widget_imports.dart';

class BuildContactBottomSheet extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final DashboardDetailsData dashboardDetailsData;

  const BuildContactBottomSheet(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.dashboardDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              title: tr("contact"),
            ),
            BuildChatItem(
                appointmentOrdersModel: appointmentOrdersModel,
                dashboardDetailsData: dashboardDetailsData,
                deviceModel: deviceModel),
            Divider(height: 0),
            BuildContactItem(
              deviceModel: deviceModel,
              onTap: () => getIt<Utilities>().callPhone(
                  phone: appointmentOrdersModel.businessContactNumber),
              title: tr("Call_Us"),
              icon: Transform.scale(
                scale: .8,
                child: SvgPicture.asset(Res.call_outgoing_no_background,
                    color: MyColors.black,
                    width: deviceModel.isTablet ? 20.sp : 21.h,
                    height: deviceModel.isTablet ? 20.sp : 21.h),
              ),
              subTitle: getIt<Utilities>().convertNumToAr(
                context: context, value: "+${appointmentOrdersModel.businessContactNumber}",
              ),
            ),
            Divider(height: 0),
            BuildContactItem(
              deviceModel: deviceModel,
              onTap: () => getIt<Utilities>()
                  .launchWhatsApp(appointmentOrdersModel.businessContactNumber),
              title: tr("Whatsapp"),
              backGroundColor: Colors.green.withOpacity(.9),
              icon: Image.asset(
                Res.whatsapp,
                width: (deviceModel.isTablet ? 15.sp : 19.sp),
                height: (deviceModel.isTablet ? 15.sp : 19.sp),
                color: MyColors.white,
              ),
              subTitle: tr("Chat_With_Us"),
            )
          ]),
    );
  }
}
