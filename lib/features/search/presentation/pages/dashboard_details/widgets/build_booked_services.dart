part of 'dashboard_details_widget_imports.dart';

class BuildBookedServices extends StatelessWidget {
  final AppointmentDetails appointmentDetails;
  final DashboardDetailsData dashboardDetailsData;
  final String businessID;

  const BuildBookedServices({
    Key? key,
    required this.appointmentDetails,
    required this.dashboardDetailsData,
    required this.businessID,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Visibility(
      visible: appointmentDetails.manageVisitListItems.isNotEmpty,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: BuildHeaderTitle(
                title: tr("Booked_Services"),
                count: appointmentDetails.manageVisitListItems.length),
          ),
          Container(
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.circular(13.r),
              border:
                  Border.all(width: .1, color: MyColors.grey.withOpacity(.5)),
            ),
            margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 10.h),
            child: ListView.separated(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemBuilder: (cxt, index) {
                return BuildReservedItem(
                  businessID: businessID,
                  currencyName: appointmentDetails.currencyName(),
                  dashboardDetailsData: dashboardDetailsData,
                  model: device,
                  appointmentDetails: appointmentDetails,
                  index: index,
                );
              },
              separatorBuilder: (cxt, i) {
                return Divider(height: 0);
              },
              itemCount: appointmentDetails.manageVisitListItems.length,
            ),
          )
        ],
      ),
    );
  }
}
