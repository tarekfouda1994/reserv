part of 'dashboard_details_widget_imports.dart';

class BuildPaymentDetails extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final DeviceModel model;
  final AppointmentDetails appointmentDetails;

  const BuildPaymentDetails(
      {Key? key,
      required this.dashboardDetailsData,
      required this.model,
      required this.appointmentDetails})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var total = appointmentDetails.manageVisitListItems.isNotEmpty
        ? appointmentDetails.manageVisitListItems
            .fold(0, (num prev, e) => prev + e.amount)
        : 0;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: BuildHeaderTitle(title: tr("payment"), count: 0),
        ),
        Container(
          margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 16.h),
          padding: EdgeInsets.all(16.0.r),
          decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(13.r),
            border: Border.all(width: .1, color: MyColors.grey.withOpacity(.5)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  MyText(
                    title: "${tr("payment_methods")} :",
                    color: MyColors.black,
                    size: model.isTablet ? 7.sp : 11.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  Spacer(),
                  SvgPicture.asset(Res.credit_card,
                      color: MyColors.grey.withOpacity(.4)),
                  const SizedBox(width: 2),
                  Visibility(
                    visible:
                        appointmentDetails.cardDetails == null && model.auth,
                    child: MyText(
                        title: tr("cash"),
                        color: MyColors.black,
                        size: model.isTablet ? 6.sp : 9.sp),
                  ),
                  Visibility(
                    visible:
                        appointmentDetails.cardDetails != null || !model.auth,
                    child: MyText(
                        title: tr("visa_card"),
                        color: MyColors.black,
                        size: model.isTablet ? 6.sp : 9.sp),
                  ),
                ],
              ),
              Visibility(
                  visible: appointmentDetails.promoCode.isNotEmpty,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20.h),
                      Row(
                        children: [
                          MyText(
                            title: "${tr("promo_code")} :",
                            color: MyColors.black,
                            size: model.isTablet ? 7.sp : 11.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          Spacer(),
                          MyText(
                              title: appointmentDetails.promoCode,
                              color: MyColors.primary,
                              size: model.isTablet ? 6.sp : 9.sp),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10, bottom: 30),
                        padding: EdgeInsets.symmetric(
                            horizontal: model.isTablet ? 14.w : 16.w),
                        decoration: BoxDecoration(
                            color: Color(0xffD6ECEC),
                            borderRadius: BorderRadius.circular(14.r)),
                        child: Row(
                          children: [
                            Transform.scale(
                              scale: model.isTablet ? 1.7 : 1,
                              child: Checkbox(
                                  splashRadius: 100,
                                  activeColor: MyColors.primary,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100)),
                                  value: true,
                                  onChanged: (value) {}),
                            ),
                            MyText(
                                title: tr("congrats_saved"),
                                color: MyColors.primary,
                                size: model.isTablet ? 6.sp : 9.sp),
                            MyText(
                                title:
                                    " ${appointmentDetails.currencyName()} ${getIt<Utilities>().convertNumToAr(context: context, value: "${appointmentDetails.netTotAmount - appointmentDetails.totAmount}")}",
                                color: MyColors.primary,
                                size: model.isTablet ? 6.sp : 9.sp,
                                fontWeight: FontWeight.bold),
                          ],
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            title: "${tr("Total_Amount")} :",
                            color: MyColors.black,
                            size: model.isTablet ? 7.sp : 11.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          Spacer(),
                          Column(
                            children: [
                              MyText(
                                  title:
                                      "${appointmentDetails.currencyName()} ${appointmentDetails.netTotalAmount}",
                                  color: Color(0xffD9D9D9),
                                  decoration: TextDecoration.lineThrough,
                                  size: model.isTablet ? 8.sp : 12.sp,
                                  fontWeight: FontWeight.bold),
                              MyText(
                                  title:
                                      "${appointmentDetails.currencyName()} ${appointmentDetails.totalAmount}",
                                  color: MyColors.black,
                                  size: model.isTablet ? 8.sp : 12.sp,
                                  fontWeight: FontWeight.bold),
                            ],
                          ),
                        ],
                      ),
                    ],
                  )),
              Visibility(
                visible: appointmentDetails.promoCode.isEmpty,
                child: Padding(
                  padding: EdgeInsets.only(top: 8.h),
                  child: Row(
                    children: [
                      MyText(
                        title: "${tr("Total_Amount")} :",
                        color: MyColors.black,
                        size: model.isTablet ? 7.sp : 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      Spacer(),
                      MyText(
                          title:
                              "${appointmentDetails.currencyName()} ${appointmentDetails.totalAmount}",
                          color: MyColors.black,
                          size: model.isTablet ? 8.sp : 12.sp,
                          fontWeight: FontWeight.bold),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
