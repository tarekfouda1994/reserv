part of 'dashboard_details_widget_imports.dart';

class BuildHeaderDetails extends StatelessWidget {
  final DeviceModel model;
  final AppointmentOrdersModel appointmentOrdersModel;
  final AppointmentDetails? appointmentDetailsModel;
  final DashboardDetailsData dashboardDetailsData;

  const BuildHeaderDetails(
      {Key? key,
      required this.model,
      required this.appointmentOrdersModel,
      required this.appointmentDetailsModel,
      required this.dashboardDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(13.r),
        border: Border.all(
          width: 1.2,
          color: appointmentOrdersModel.statusId.getStatusBgDateColor(),
        ),
      ),
      margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 10.h, top: 16.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BuildHeaderOrderDetails(
            appointmentOrdersModel: appointmentOrdersModel,
            dashboardDetailsData: dashboardDetailsData,
            model: model,
          ),
          BuildAppointmentDate(
            model: model,
            appointmentDetailsModel: appointmentDetailsModel,
            dashboardDetailsData: dashboardDetailsData,
            appointmentOrdersModel: appointmentOrdersModel,
          ),
          BuildAppointmentStatus(
            appointmentOrdersModel: appointmentOrdersModel,
            model: model,
          ),
          BuildAboutAppointmentDetails(
            appointmentDetailsModel: appointmentDetailsModel!,
            appointmentOrdersModel: appointmentOrdersModel,
            model: model,
          )
        ],
      ),
    );
  }
}
