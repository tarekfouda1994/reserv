part of 'dashboard_details_widget_imports.dart';

class BuildBottomSheetAppointmentServiceOptions extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final int index;
  final AppointmentDetails appointmentDetails;
  final AppontmentServices appointmentServices;

  const BuildBottomSheetAppointmentServiceOptions(
      {Key? key,
      required this.dashboardDetailsData,
      required this.appointmentDetails,
      required this.index,
      required this.appointmentServices})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        BuildHeaderBottomSheet(
          title: appointmentServices.getServiceName(),
          titleColor: MyColors.primary,
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          child: Row(
            children: [
              Icon(
                Icons.access_time,
                color: MyColors.greyLight,
                size: device.isTablet ? 14.sp : 17.sp,
              ),
              MyText(
                title:
                    " ${getIt<Utilities>().convertNumToAr(context: context, value: "${appointmentServices.serviceDuration}")} ${tr("min")}",
                color: MyColors.grey,
                size: device.isTablet ? 6.sp : 10.sp,
              ),
            ],
          ),
        ),
        BuildServiceOptionsItem(
          title: appointmentServices.hasWishItem
              ? tr("remove_from_favorites")
              : tr("move_to_favorites"),
          subTitle: appointmentServices.hasWishItem
              ? tr("remove_favorite_and_appointment")
              : tr("add_favorite_and_appointment"),
          onTap: () =>
              dashboardDetailsData.updateWish(context, appointmentServices),
          image: appointmentServices.hasWishItem
              ? Res.favoritesActive
              : Res.Favorites,
          imageColor: MyColors.primary,
        ),
        Divider(height: 5),
        BuildServiceOptionsItem(
          title: tr("Remove"),
          subTitle: tr("definitely_remove_appointment"),
          onTap: () => dashboardDetailsData.cancelReservationItem(
            context,
            appointmentDetails.manageVisitListItems[index].detailID,
            appointmentDetails.id,
            appointmentDetails.appointmentType,
            index,
          ),
          icon: Icon(Icons.clear,
              color: MyColors.errorColor,
              size: device.isTablet ? 12.sp : 17.sp),
          imageColor: MyColors.primary,
        ),
        if(appointmentServices.statusId ==5)
        Column(
          children: [
            Divider(height: 5),
            BuildServiceOptionsItem(
              title: tr("review"),
              subTitle: tr("addReviewService"),
              onTap: () => dashboardDetailsData.showRateSheet(appointmentServices),
              image: Res.star,
              imageColor: MyColors.amber,
            ),
          ],
        ),
      ]),
    );
  }
}
