part of'dashboard_details_widget_imports.dart';

class BuildContactItem extends StatelessWidget {
  final DeviceModel deviceModel;
  final Widget?  icon;
  final String  title;
  final String  subTitle;
  final Color?  backGroundColor;
  final void Function() onTap;
  const BuildContactItem({Key? key, required this.deviceModel,  this.icon, required this.title, required this.subTitle, required this.onTap, this.backGroundColor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap:onTap,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
             BuildIconItem( img:icon?? SvgPicture.asset(Res.edit,
                width: deviceModel.isTablet ? 20.sp : 21.h,
                height: deviceModel.isTablet ? 20.sp : 21.h),backGroundColor: backGroundColor),
            SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                    title: title,
                    color: MyColors.black,
                    size: deviceModel.isTablet ? 7.sp : 11.sp),
                MyText(
                    title: subTitle,
                    color: MyColors.grey,
                    size: deviceModel.isTablet ? 5.5.sp : 9.sp),
              ],
            ),
            Spacer(),
            Icon(
              Icons.arrow_forward,
              color: MyColors.primary,
              size: deviceModel.isTablet ? 12.sp : 17.sp,
            )
          ],
        ),
      ),
    );
  }
}
