part of'dashboard_details_widget_imports.dart';

class BuildDashboardDetailsLoadingView extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final AppointmentDetails? appointmentDetailsModel;
  final DashboardDetailsData dashboardDetailsData;
  const BuildDashboardDetailsLoadingView({Key? key, required this.appointmentOrdersModel,  this.appointmentDetailsModel, required this.dashboardDetailsData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return ListView(
      children: [
        BuildShimmerView(height: device.isTablet ? 250.h : 230.h),
        Divider(height: 5.h),
        ...List.generate(5, (index) {
          return BuildShimmerView(
              height: device.isTablet ? 100.h : 80.h);
        }),
        BuildShimmerView(height: device.isTablet ? 80.h : 60.h),
      ],
    );
  }
}
