part of 'dashboard_details_widget_imports.dart';

class BuildAboutAppointmentDetails extends StatelessWidget {
  final DeviceModel model;
  final AppointmentOrdersModel appointmentOrdersModel;
  final AppointmentDetails appointmentDetailsModel;

  const BuildAboutAppointmentDetails(
      {Key? key,
      required this.model,
      required this.appointmentOrdersModel,
      required this.appointmentDetailsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 25).r,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset(Res.building, height: 18.r, width: 18.r),
          SizedBox(width: 10.sm),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 2.r,
                ),
                MyText(
                  fontFamily: CustomFonts.primarySemiBoldFont,
                  title: appointmentOrdersModel.getBusinessName(),
                  color: MyColors.black,
                  size: model.isTablet ? 7.sp : 11.sp,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0).r,
                  child: Row(
                    children: [
                      if(int.tryParse(appointmentDetailsModel.businessRating) != 0 && int.tryParse(appointmentDetailsModel.businessRating) != null)
                      Row(
                        children: [
                          MyText(
                            title: "${appointmentDetailsModel.businessRating} ",
                            color: MyColors.amber,
                            size: model.isTablet ? 5.sp : 8.sp,
                          ),
                          SvgPicture.asset(
                            Res.star,
                            color: MyColors.amber,
                            height: 12.sp,
                            width: 12.sp,
                          ),
                          Container(
                            margin: EdgeInsetsDirectional.only(end: 3, start: 25.w),
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: MyColors.greyLight,
                            ),
                          ),
                        ],
                      ),

                      MyText(
                        title: appointmentDetailsModel.businessGender == "Male"
                            ? tr("men")
                            : tr("women"),
                        color: MyColors.grey,
                        size: model.isTablet ? 5.5.sp : 9.sp,
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () => AutoRouter.of(context).push(
                    TrackingMapRoute(
                      businessName: appointmentOrdersModel.getBusinessName(),
                      address: appointmentOrdersModel.getAddressName(),
                      lat: appointmentOrdersModel.latitude,
                      lng: appointmentOrdersModel.longitude,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 2.0),
                            child: SvgPicture.asset(
                              Res.location_arrow_icon,
                              width: model.isTablet ? 5.5.w : null,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.only(
                                end: 8.0, start: 5),
                            child: MyText(
                              decoration: TextDecoration.underline,
                              title: tr("See_Direction"),
                              color: MyColors.blue,
                              size: model.isTablet ? 5.5.sp : 9.sp,
                            ),
                          ),
                        ],
                      ),
                      MyText(
                        title: appointmentDetailsModel.distance,
                        color: MyColors.grey,
                        size: model.isTablet ? 5.5.sp : 9.sp,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
