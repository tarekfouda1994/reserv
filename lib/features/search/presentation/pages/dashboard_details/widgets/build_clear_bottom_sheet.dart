part of 'dashboard_details_widget_imports.dart';

class BuildClearBottomSheet extends StatelessWidget {
  final DashboardDetailsData dashboardDetailsData;
  final String id;
  final int appointmentType;

  const BuildClearBottomSheet(
      {Key? key,
      required this.dashboardDetailsData,
      required this.id,
      required this.appointmentType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.all(15.h),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              ladingIcon: SvgPicture.asset(
                Res.trash,
                width: deviceModel.isTablet ? 10.w : 15.w,
                height: deviceModel.isTablet ? 10.w : 15.w,
              ),
              title: " ${tr("Cancel_appointment")}",
              titleColor: Colors.red,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: " ${tr("like_cancel_appointment")}",
                      color: Colors.black,
                      size: deviceModel.isTablet ? 7.sp : 10.sp,
                    ),
                    SizedBox(height: 5),
                    MyText(
                      title: " ${tr("you_will_cancel_all")}",
                      color: Colors.black,
                      size: deviceModel.isTablet ? 7.sp : 10.sp,
                    ),
                  ]),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () => AutoRouter.of(context).pop(),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    margin: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.r),
                      color: MyColors.primary.withOpacity(.05),
                    ),
                    child: MyText(
                        alien: TextAlign.center,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                        title: tr("keep"),
                        color: MyColors.black),
                  ),
                ),
                InkWell(
                  onTap: () => dashboardDetailsData.cancelOrder(
                      context, id, appointmentType),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    margin: const EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.r),
                        color: Colors.red.withOpacity(.2)),
                    child: Row(
                      children: [
                        MyText(
                            alien: TextAlign.center,
                            size: deviceModel.isTablet ? 7.sp : 11.sp,
                            title: ' ${tr("cancel_appointment")}',
                            color: Colors.red),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
