part of 'business_search_widgets_imports.dart';

class BuildBusinessSearchCard extends StatelessWidget {
  final BusinessItemSearchModel businessItemSearchModel;
  final BusinessSearchData businessSearchData;

  const BuildBusinessSearchCard(
      {Key? key,
      required this.businessItemSearchModel,
      required this.businessSearchData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return GestureDetector(
      onTap: () => AutoRouter.of(context)
          .push(ProductDetailsRoute(id: businessItemSearchModel.id)),
      child: Container(
        margin: EdgeInsets.all(15.r),
        decoration: BoxDecoration(boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, .1),
            offset: Offset(0.0, 2.0),
            blurRadius: 20.0,
          ),
        ], borderRadius: BorderRadius.circular(10), color: MyColors.white),
        child: Column(
          children: [
            CachedImage(
              url: businessItemSearchModel.businessImage.replaceAll("\\", "/"),
              height: device.isTablet ? 240.sm : 170.sm,
              borderRadius: BorderRadius.vertical(top: Radius.circular(12.r)),
              fit: BoxFit.cover,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        children: [
                          InkWell(
                            onTap: () => businessSearchData.shareSinglePage(
                                context,
                                businessItemSearchModel.id,
                                businessItemSearchModel.getSearchName()),
                            child: Container(
                              width: 30.r,
                              height: 30.r,
                              margin: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 10),
                              decoration: BoxDecoration(
                                  border: Border.all(color: MyColors.white),
                                  shape: BoxShape.circle,
                                  color: MyColors.favouriteBg),
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.share_outlined,
                                size: device.isTablet ? 12.sp : 17.sp,
                                color: MyColors.white,
                              ),
                            ),
                          ),
                          BuildFavouriteIcon(
                              onTap: () => businessSearchData.updateWish(
                                  context,
                                  businessItemSearchModel,
                                  device.auth),
                              hasWish: businessItemSearchModel.hasWishItem),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: 6.h,
                          horizontal: device.isTablet ? 6.w : 10.w,
                        ),
                        decoration: BoxDecoration(
                          color: MyColors.black,
                          borderRadius: BorderRadius.only(
                            topLeft: isEn
                                ? Radius.circular(14.r)
                                : Radius.circular(0),
                            topRight: isEn
                                ? Radius.circular(0)
                                : Radius.circular(14.r),
                          ),
                        ),
                        child: MyText(
                          title:
                              "${businessItemSearchModel.servicesCount} ${tr("services")}",
                          color: MyColors.white,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Transform.scale(
                          scale: .8,
                          child: SvgPicture.asset(Res.building,
                              width: device.isTablet ? 12.w : null)),
                      Expanded(
                        child: MyText(
                          title: " " + businessItemSearchModel.getSearchName(),
                          color: MyColors.black,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 11.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      MyText(
                        title: businessItemSearchModel.rating.toString(),
                        color: MyColors.amber,
                        size: device.isTablet ? 7.sp : 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(width: 4.sp),
                      SvgPicture.asset(
                        Res.star,
                        color: MyColors.amber,
                        width: 14.r,
                        height: 14.r,
                      ),
                    ],
                  ),
                  Divider(color: MyColors.white, height: 10),
                  Row(
                    children: [
                      Transform.scale(
                          scale: .9,
                          child: SvgPicture.asset(Res.marker_input,
                              width: 14.sp, height: 15.sp)),
                      Expanded(
                        child: MyText(
                          title: " ${businessItemSearchModel.getAreaName()}",

                          color: MyColors.black,
                          overflow: TextOverflow.ellipsis,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Transform.scale(
                          scale: .9,
                          child: SvgPicture.asset(
                            Res.marker_input,
                            width: 14.sp,
                            height: 15.sp,
                            color: MyColors.white,
                          )),
                      MyText(
                        title: businessItemSearchModel.distance != 0
                            ? "  ${businessItemSearchModel.distance.toStringAsFixed(2)} ${tr("away")}"
                            : "",
                        color: MyColors.black,
                        size: device.isTablet ? 6.sp : 9.sp,
                        fontWeight: FontWeight.w600,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 4.w),
                      InkWell(
                        onTap: () => getIt<Utilities>().trackingBusinessOnMap(
                          businessItemSearchModel.latitude,
                          businessItemSearchModel.longitude,
                          businessItemSearchModel.getSearchName(),
                          businessItemSearchModel.getAreaName(),
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(Res.location_arrow_icon),
                            SizedBox(width: 2),
                            MyText(
                                decoration: TextDecoration.underline,
                                title: tr("direction"),
                                color: Color(0xff4278F6),
                                size: device.isTablet ? 5.5.sp : 9.sp),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
