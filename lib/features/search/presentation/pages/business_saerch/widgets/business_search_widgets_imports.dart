import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/search/data/models/business_item_search_model/business_item_search_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/bloc/device_cubit/device_cubit.dart';
import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../core/routes/router_imports.gr.dart';
import '../business_search_imports.dart';

part 'build_business_search_card.dart';
