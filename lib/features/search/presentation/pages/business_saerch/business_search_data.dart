part of 'business_search_imports.dart';

class BusinessSearchData {
  final PagingController<int, BusinessItemSearchModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  ficheData(BuildContext context, int currentPage, String filter,
      {bool refresh = true}) async {
    var lotLong = context.read<HomeLocationCubit>().state.model;
    var deviceId = await FirebaseMessaging.instance.getToken();
    var dateTime = context.read<SearchCubit>().state.dateTime.dateList;
    dateTime.sort((a, b) => a.compareTo(b));
    var data = await GetAllSearch().call(SearchPrams(
        pageNumber: currentPage,
        pageSize: pageSize,
        deviceId: deviceId!,
        latitude: lotLong?.lat ?? 0,
        longitude: lotLong?.lng ?? 0,
        from: getIt<Utilities>().formatDate(dateTime.firstOrNull),
        to: getIt<Utilities>().formatDate(dateTime.lastOrNull),
        filter: filter));
    final isLastPage = data!.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
  }

  updateWish(
      BuildContext context, BusinessItemSearchModel model, bool auth) async {
    if (auth) {
      getIt<LoadingHelper>().showLoadingDialog();
      if (model.hasWishItem == false) {
        var update = await UpdateWishBusiness()(model.id);
        if (update) {
          int index = pagingController.itemList!.indexOf(model);
          pagingController.itemList![index].hasWishItem =
              !pagingController.itemList![index].hasWishItem;
          pagingController.notifyListeners();
        }
      } else {
        var remove = await RemoveWishBusiness()(model.id);
        if (remove) {
          int index = pagingController.itemList!.indexOf(model);
          pagingController.itemList![index].hasWishItem =
              !pagingController.itemList![index].hasWishItem;
          pagingController.notifyListeners();
        }
      }
      getIt<LoadingHelper>().dismissDialog();
    } else {
      CustomToast.customAuthDialog(isSalon: true);
    }
  }

  void shareSinglePage(
      BuildContext context, String id, String businessName) async {
    return getIt<Utilities>().shareSinglePage(context, id, businessName);
  }
}
