part of 'business_search_imports.dart';

class BusinessSearchPage extends StatefulWidget {
  final String filter;

  const BusinessSearchPage({Key? key, required this.filter}) : super(key: key);

  @override
  State<BusinessSearchPage> createState() => _BusinessSearchPageState();
}

class _BusinessSearchPageState extends State<BusinessSearchPage> {
  final BusinessSearchData businessSearchData = BusinessSearchData();

  @override
  void initState() {
    businessSearchData.pagingController.addPageRequestListener((pageKey) {
      businessSearchData.ficheData(context, pageKey, widget.filter);
    });
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Scaffold(
      appBar: DefaultAppBar(title: tr("business_search"), centerTitle: true),
      body: RefreshIndicator(
              color: MyColors.primary,
        onRefresh: () => businessSearchData.ficheData(context, 1, widget.filter),
        child: PagedListView<int, BusinessItemSearchModel>(
          padding: const EdgeInsets.symmetric(vertical: 25),
          pagingController: businessSearchData.pagingController,
          builderDelegate: PagedChildBuilderDelegate<BusinessItemSearchModel>(
              firstPageProgressIndicatorBuilder: (context) => Column(
                    children: List.generate(
                      10,
                      (index) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: BuildShimmerView(
                          height: device.isTablet ? 190.h : 160.h,
                        ),
                      ),
                    ),
                  ),
              itemBuilder: (context, item, index) => SizedBox(
                    child: BuildBusinessSearchCard(
                      businessItemSearchModel: item,
                      businessSearchData: businessSearchData,
                    ),
                  ),
              newPageProgressIndicatorBuilder: (con) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CupertinoActivityIndicator(),
                  ),
              firstPageErrorIndicatorBuilder: (context) => BuildPageError(
                    onTap: () => businessSearchData.pagingController.refresh(),
                  )),
        ),
      ),
    );
  }
}
