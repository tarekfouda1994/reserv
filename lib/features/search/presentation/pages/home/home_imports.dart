import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/bloc/search_cubit/search_cubit.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/gradient_scaffold.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/favourite_imports.dart';
import 'package:flutter_tdd/features/search/domain/entites/tab_icons_entity.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/dashbaord/dashboard_imports.dart';
import 'package:flutter_tdd/features/versioning/presentation/controller/versioning_controller.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'tabs/home_main/home_main_imports.dart';
import 'tabs/profile/profile_imports.dart';
import 'widgets/home_widgets_imports.dart';

part 'home.dart';
part 'home_data.dart';
