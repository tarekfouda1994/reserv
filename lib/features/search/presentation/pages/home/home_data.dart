part of 'home_imports.dart';

class HomeData {
  final GenericBloc<int> homeTabCubit = GenericBloc(0);
  final GenericBloc<ProfilePages> pagesCubit = GenericBloc(ProfilePages.intro);
  final GenericBloc<int> homeMainPagesCubit = GenericBloc(0);
  late AnimationController animationController;
  late TabController tabController;
  late Animation<double> animation;
  late CurvedAnimation curve;
  DateTime? conTime;
  Function()? restSearch;

  List<TabIconsEntity> tabs = [];

  void initTabs(BuildContext context) {
    tabs = [
      TabIconsEntity(
          icon: Res.dashboardInactive,
          activeIcon: Res.dashboardActive,
          title: tr("home")),
      TabIconsEntity(
          icon: Res.appointmentInactive,
          activeIcon: Res.appointmentActive,
          title: tr("appointments")),
      if (checkUserAuth(context))
        TabIconsEntity(
            icon: Res.Favorites,
            activeIcon: Res.favoritesActive,
            title: tr("favorites")),
      TabIconsEntity(
          icon: Res.profileInactive,
          activeIcon: Res.profileActive,
          title: (!checkUserAuth(context)) ? tr("Log_Reg") : tr("profile")),
    ];
  }

  void initBottomNavigation(TickerProvider ticker, int index) {
    tabController = TabController(
      length: tabs.length,
      vsync: ticker,
      initialIndex: index,
    );
  }

  bool checkUserAuth(BuildContext context) {
    return context.read<DeviceCubit>().state.model.auth;
  }

  void animateTabsPages(int index, BuildContext context) {
    if (index != homeTabCubit.state.data) {
      homeTabCubit.onUpdateData(index);
      tabController.animateTo(index);
    }
  }

  Future<bool> onBackPressed(BuildContext context) async {
    if (homeMainPagesCubit.state.data == 1) {
      homeMainPagesCubit.onUpdateData(0);
      return false;
    }
    DateTime now = DateTime.now();
    if (conTime == null || now.difference(conTime!) > Duration(seconds: 2)) {
      conTime = now;
      CustomToast.showSimpleToast(
        msg: tr("exitAlert"),
        title: tr("exitAlertTitle"),
        type: ToastType.info,
      );
      return Future.value(false);
    }
    conTime = null;
    SystemNavigator.pop();
    return Future.value(true);
  }
}
