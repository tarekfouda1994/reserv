part of 'home_imports.dart';

class Home extends StatefulWidget {
  final int index;

  const Home({Key? key, this.index = 0}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  final HomeData homeData = HomeData();

  @override
  void initState() {
    homeData.initTabs(context);
    homeData.homeTabCubit.onUpdateData(widget.index);
    homeData.initBottomNavigation(this, widget.index);
    Future.delayed(const Duration(seconds: 1), () {
      VersioningController.instance.checkVersion();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String filter = context.watch<SearchCubit>().state.filter;
    if (filter.isNotEmpty) {
      return _buildDefaultTabController(context);
    } else {
      return WillPopScope(
        onWillPop: () => homeData.onBackPressed(context),
        child: _buildDefaultTabController(context),
      );
    }
  }

  DefaultTabController _buildDefaultTabController(BuildContext context) {
    return DefaultTabController(
      length: homeData.tabs.length,
      initialIndex: widget.index,
      child: GradientScaffold(
        body: TabBarView(
          controller: homeData.tabController,
          physics: NeverScrollableScrollPhysics(),
          children: [
            HomeMain(homeData: homeData),
            Dashboard(homeData: homeData),
            if (homeData.checkUserAuth(context))
              Favourite(fromHome: true, homeData: homeData),
            Profile(pagesCubit: homeData.pagesCubit),
          ],
        ),
        bottomNavigationBar: BuildBottomNavigationBar(homeData: homeData),
      ),
    );
  }
}
