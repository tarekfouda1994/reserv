part of 'home_widgets_imports.dart';

class BuildTabIcon extends StatelessWidget {
  final int index;
  final bool active;
  final HomeData homeData;

  const BuildTabIcon(
      {required this.index, required this.active, required this.homeData});

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.only(top: 6).r,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Transform.scale(
            scale: .8,
            child: SvgPicture.asset(
                active
                    ? homeData.tabs[index].activeIcon
                    : homeData.tabs[index].icon,
                height: 26.h,
                width: 26.h),
          ),
          SizedBox(height: 2.5.h),
          MyText(
            title: tr(homeData.tabs[index].title, context: context),
            color: active ? MyColors.primary : MyColors.grey,
            size: device.isTablet? 6.sp : 8.sp,
          ),
        ],
      ),
    );
  }
}
