part of 'home_widgets_imports.dart';

class BuildBottomNavigationBar extends StatelessWidget {
  final HomeData homeData;

  const BuildBottomNavigationBar({required this.homeData});

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: homeData.homeTabCubit,
      builder: (context, state) {
        return AnimatedBottomNavigationBar.builder(
          itemCount: homeData.tabs.length,
          tabBuilder: (int index, bool isActive) {
            return BuildTabIcon(
              index: index,
              active: isActive,
              homeData: homeData,
            );
          },
          elevation: 8,
          backgroundColor: MyColors.white,
          splashColor: MyColors.primary,
          activeIndex: state.data,
          gapLocation: GapLocation.none,
          splashSpeedInMilliseconds: 300,
          height: (device.isTablet ? 60.h : (Platform.isIOS? 50.h : 55.h)),
          onTap: (index) => homeData.animateTabsPages(index, context),
        );
      },
    );
  }
}
