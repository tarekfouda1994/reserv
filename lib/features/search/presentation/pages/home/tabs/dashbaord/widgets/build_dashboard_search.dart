part of 'dashbaord_widgets_imports.dart';

class BuildDashboardSearch extends StatelessWidget {
  final DashboardData dashboardData;

  const BuildDashboardSearch({Key? key, required this.dashboardData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var lastName = context.read<UserCubit>().state.model?.lastName;
    final device = context.watch<DeviceCubit>().state.model;
    return SliverToBoxAdapter(
      child: BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
          bloc: dashboardData.searchButtonCubit,
          builder: (context, state) {
            return Container(
              color: MyColors.white,
              padding: EdgeInsets.symmetric(
                  horizontal: device.isTablet ? 12.sp : 20.sp, vertical: 8.sp),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      title: tr("reference_no"),
                      color: MyColors.black,
                      size: device.isTablet ? 6.sp : 9.5.sp),
                  Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: GenericTextField(
                          // onChange: (v) => dashboardData.onChangeSearch(context,v),
                          controller: dashboardData.search,
                          fieldTypes: FieldTypes.normal,
                          onSubmit: () => dashboardData.getOrders(
                            null,
                            orderNumber: dashboardData.search.text,
                            lastName: lastName,
                            refresh: false,
                          ),
                          type: TextInputType.text,
                          onChange: (v) {
                            if (v.trim().isNotEmpty) {
                              dashboardData.searchButtonCubit
                                  .onUpdateData(true);
                            } else {
                              dashboardData.searchButtonCubit
                                  .onUpdateData(false);
                            }
                          },
                          action: TextInputAction.search,
                          validate: (value) => value?.noValidate(),
                          hint: "${tr("ex")} 2210055",
                          fillColor: MyColors.white,
                          contentPadding: EdgeInsets.symmetric(
                            vertical: device.isTablet ? 10.sp : 8.sp,
                            horizontal: device.isTablet ? 8.sp : 12.w,
                          ),
                          margin: EdgeInsets.symmetric(vertical: 6.h),
                        ),
                      ),
                      SizedBox(width: 10.w),
                      Expanded(
                        flex: 1,
                        child: DisableWidget(
                          isDisable: !state.data,
                          child: InkWell(
                            onTap: () => dashboardData.submitSearch(context),
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: device.isTablet ? 11.sp : 14.sp),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25.r),
                                color: MyColors.primary,
                              ),
                              child: MyText(
                                alien: TextAlign.center,
                                color: MyColors.white,
                                size: device.isTablet ? 7.sp : 11.sp,

                                title: tr("search"),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            );
          }),
    );
  }

  void clearInput() {
    dashboardData.search.text = "";
    dashboardData.viewCubit.onUpdateData("");
    dashboardData.getOrders(dashboardData.status);
  }
}
