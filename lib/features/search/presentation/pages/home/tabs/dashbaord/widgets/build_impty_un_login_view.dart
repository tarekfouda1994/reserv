part of 'dashbaord_widgets_imports.dart';

class BuildUnLoginView extends StatelessWidget {

  const BuildUnLoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      height: MediaQuery.of(context).size.height,
      color: MyColors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(Res.empty_group),
            Container(
              width: MediaQuery.of(context).size.width*.8,
              padding: EdgeInsets.symmetric(vertical: 15.h),
              child: MyText(
                alien: TextAlign.center,
                  color: MyColors.black,
                  title: tr("to_find_your_appointments"),
                  size: device.isTablet ? 7.sp : 11.sp),
            ),
          ],
        ),
      ),
    );
  }
}
