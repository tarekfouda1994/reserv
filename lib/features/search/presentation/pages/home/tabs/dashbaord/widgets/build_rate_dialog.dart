part of 'dashbaord_widgets_imports.dart';

class BuildRateDialog extends StatelessWidget {
  final DashboardData dashboardData;
  final AppointmentOrdersModel appointmentOrdersModel;

  const BuildRateDialog(
      {Key? key,
      required this.dashboardData,
      required this.appointmentOrdersModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      insetPadding: EdgeInsets.all(16),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.r)),
      content: Container(
        padding: EdgeInsets.all(14.h),
        width: MediaQuery.of(context).size.width,
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          BlocBuilder<GenericBloc<num>, GenericState<num>>(
              bloc: dashboardData.rateCubit,
              builder: (context, state) {
                return RatingBar.builder(
                  glowColor: MyColors.primary,
                  itemSize: 25.sp,
                  initialRating: state.data.toDouble(),
                  minRating: 1,
                  unratedColor: MyColors.grey.withOpacity(.2),
                  direction: Axis.horizontal,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 3),
                  itemBuilder: (context, _) =>
                      SvgPicture.asset(Res.star, color: MyColors.amber),
                  onRatingUpdate: (rating) {
                    dashboardData.rateCubit.onUpdateData(rating);
                  },
                );
              }),
          Form(
            key: dashboardData.formKey,
            child: CustomTextField(
              controller: dashboardData.rateMsg,
              max: 3,
              fieldTypes: FieldTypes.rich,
              type: TextInputType.text,
              action: TextInputAction.done,
              validate: (value) => value?.validateEmpty(),
              hint: "${tr("ex")} ${tr("any_message")}",
              autoValidateMode: AutovalidateMode.onUserInteraction,
              margin: EdgeInsets.only(bottom: 15, top: 15),
            ),
          ),
          DefaultButton(
            margin: EdgeInsets.zero,
            title: tr("Confirm"),
            onTap: () => dashboardData.createRate(context,appointmentOrdersModel),
          )
        ]),
      ),
    );
  }
}
