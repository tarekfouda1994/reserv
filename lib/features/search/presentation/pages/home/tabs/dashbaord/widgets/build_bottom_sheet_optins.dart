part of 'dashbaord_widgets_imports.dart';

class BuildBottomSheetOptions extends StatelessWidget {
  final DashboardData dashboardData;
  final AppointmentOrdersModel appointmentOrdersModel;
  final int index;
  final String image;

  const BuildBottomSheetOptions({
    Key? key,
    required this.appointmentOrdersModel,
    required this.dashboardData,
    required this.index,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: const EdgeInsets.only(left: 16, right: 16.0, top: 16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(15.r),
              ),
              color: MyColors.white),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            BuildHeaderBottomSheet(
              margin: EdgeInsets.only(bottom: 10, left: 10, right: 10).r,
              title: tr("options"),
            ),
            InkWell(
              onTap: () {
                if (deviceModel.auth) {
                  AutoRouter.of(context).push(ChatRoomRoute(
                      appointmentOrdersModel: appointmentOrdersModel));
                } else {
                  CustomToast.customAuthDialog();
                }
              },
              child: Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  children: [
                    BuildChatIcon(
                        deviceModel: deviceModel,
                        appointmentOrdersModel: appointmentOrdersModel),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            MyText(
                                title: tr("Chat_With_Us"),
                                color: MyColors.black,
                                size: deviceModel.isTablet ? 7.sp : 11.sp),
                            BlocBuilder<GenericBloc<List<CountChatModel>>,
                                    GenericState<List<CountChatModel>>>(
                                bloc: dashboardData.msgCountCubit,
                                builder: (context, state) {
                                  if (state is GenericUpdateState &&
                                      state.data.isNotEmpty &&
                                      state.data.firstOrNull
                                              ?.serverUnReadMessageCount !=
                                          0) {
                                    return Container(
                                      padding: EdgeInsets.all(3.5.sp),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: MyColors.errorColor),
                                      child: MyText(
                                        color: MyColors.white,
                                        size:
                                            deviceModel.isTablet ? 6.sp : 9.sp,
                                        title:
                                            "${state.data.firstOrNull?.serverUnReadMessageCount ?? 0}",
                                      ),
                                    );
                                  } else {
                                    return Container();
                                  }
                                }),
                          ],
                        ),
                        MyText(
                            title: tr("Direct_Conversation"),
                            color: MyColors.grey,
                            size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                      ],
                    ),
                    Spacer(),
                    Icon(
                      Icons.arrow_forward,
                      color: MyColors.primary,
                      size: deviceModel.isTablet ? 12.sp : 17.sp,
                    )
                  ],
                ),
              ),
            ),
            Divider(height: 0),
            BuildContactItem(
              deviceModel: deviceModel,
              onTap: () => getIt<Utilities>().callPhone(
                  phone: appointmentOrdersModel.businessContactNumber),
              title: tr("Call_Us"),
              icon: Transform.scale(
                scale: .8,
                child: SvgPicture.asset(Res.call_outgoing_no_background,
                    color: MyColors.black,
                    width: deviceModel.isTablet ? 20.sp : 21.h,
                    height: deviceModel.isTablet ? 20.sp : 21.h),
              ),
              subTitle: getIt<Utilities>().convertNumToAr(
                context: context,
                value: "+${appointmentOrdersModel.businessContactNumber}",
              ),
            ),
            Divider(height: 0),
            BuildContactItem(
              deviceModel: deviceModel,
              onTap: () => getIt<Utilities>()
                  .launchWhatsApp(appointmentOrdersModel.businessContactNumber),
              title: tr("Whatsapp"),
              backGroundColor: Colors.green.withOpacity(.9),
              icon: Image.asset(
                Res.whatsapp,
                width: (deviceModel.isTablet ? 15.sp : 19.sp),
                height: (deviceModel.isTablet ? 15.sp : 19.sp),
                color: MyColors.white,
              ),
              subTitle: tr("Chat_With_Us"),
            ),
          ]),
        ),
        BuildCardOptions(
          appointmentOrdersModel: appointmentOrdersModel,
          dashboardData: dashboardData,
          index: index,
          image: image,
        ),
      ],
    );
  }
}
