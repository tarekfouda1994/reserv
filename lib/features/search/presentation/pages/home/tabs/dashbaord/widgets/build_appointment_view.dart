part of 'dashbaord_widgets_imports.dart';

class BuildAppointmentView extends StatelessWidget {
  final DashboardData dashboardData;
  final List<AppointmentOrdersModel> listAppointment;

  const BuildAppointmentView(
      {Key? key, required this.dashboardData, required this.listAppointment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (listAppointment.isEmpty)
            BuildEmptyBody(dashboardData: dashboardData),
          ...List.generate(
            listAppointment.length,
            (index) {
              return BuildDashboardCard(
                appointmentOrdersModel: listAppointment[index],
                index: index,
                dashboardData: dashboardData,
              );
            },
          ),
        ],
      ),
    );
  }
}
