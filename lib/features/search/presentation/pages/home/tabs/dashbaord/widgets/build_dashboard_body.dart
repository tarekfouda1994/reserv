part of 'dashbaord_widgets_imports.dart';

class BuildDashBoardBody extends StatelessWidget {
  final DashboardData dashboardData;

  const BuildDashBoardBody({Key? key, required this.dashboardData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return (device.auth)
        ? BlocBuilder<GenericBloc<String>, GenericState<String>>(
            bloc: dashboardData.viewCubit,
            builder: (context, viewState) {
              return CustomScrollView(
                slivers: [
                  BuildDashboardSearch(dashboardData: dashboardData),
                  SliverStickyHeader(
                    header: BuildDashboardTabs(
                      dashboardData: dashboardData,
                      filter: viewState.data,
                    ),
                    sliver: BlocBuilder<
                        GenericBloc<List<AppointmentOrdersModel>>,
                        GenericState<List<AppointmentOrdersModel>>>(
                      bloc: dashboardData.ordersCubit,
                      builder: (context, state) {
                        if (state is GenericUpdateState) {
                          return BuildAppointmentView(
                            listAppointment: state.data,
                            dashboardData: dashboardData,
                          );
                        }
                        return BuildLoadingView();
                      },
                    ),
                  ),
                ],
              );
            })
        : BuildWithoutLoginView(dashboardData: dashboardData);
  }
}
