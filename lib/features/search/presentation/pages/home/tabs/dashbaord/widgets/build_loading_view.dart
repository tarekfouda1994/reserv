part of 'dashbaord_widgets_imports.dart';

class BuildLoadingView extends StatelessWidget {
  const BuildLoadingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Column(
        children: List.generate(6, (index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: BuildShimmerView(
              height: 145.h,
            ),
          );
        }),
      ),
    );
  }
}
