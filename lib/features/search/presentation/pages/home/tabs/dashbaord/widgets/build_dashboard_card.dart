part of 'dashbaord_widgets_imports.dart';

class BuildDashboardCard extends StatefulWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final int index;
  final DashboardData dashboardData;

  const BuildDashboardCard(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.index,
      required this.dashboardData})
      : super(key: key);

  @override
  State<BuildDashboardCard> createState() => _BuildDashboardCardState();
}

class _BuildDashboardCardState extends State<BuildDashboardCard> {
  final GenericBloc<String> imageCubit = GenericBloc("");

  @override
  void initState() {
    widget.dashboardData
        .getAppointmentImage(widget.appointmentOrdersModel.businessID)
        .then((value) {
      imageCubit.onUpdateData(value);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int bookedServiceLength = widget.appointmentOrdersModel.services
        .map((e) => e.businessServiceID)
        .toSet()
        .length;
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: EdgeInsets.only(
        top: device.isTablet ? 6.h : 8.h,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomCard(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                              title: tr("ref"),
                              color: MyColors.black,

                              size: device.isTablet ? 5.5.sp : 9.sp),
                          SizedBox(height: 5),
                          MyText(
                              title: widget.appointmentOrdersModel.orderNumber,
                              color: MyColors.primary,
                              fontFamily: CustomFonts.primarySemiBoldFont,
                              size: device.isTablet ? 5.5.sp : 9.sp),
                        ],
                      ),
                      GestureDetector(
                        onTap: () => widget.dashboardData.bottomSheetSOptions(
                            context,
                            widget.dashboardData,
                            widget.appointmentOrdersModel,
                            widget.index,
                            imageCubit.state.data),
                        child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 3.0),
                            child: SvgPicture.asset(Res.dots)),
                      ),
                    ],
                  ),
                ),
                Divider(),
                InkWell(
                  onTap: () async {
                    var result = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => DashboardDetails(
                                status: widget.dashboardData.status,
                                appointmentOrdersModel:
                                    widget.appointmentOrdersModel)));
                    widget.dashboardData.ordersCubit.state.data.removeWhere(
                        (element) => element.bookingOrderID == result);
                    widget.dashboardData.ordersCubit.onUpdateData(
                        widget.dashboardData.ordersCubit.state.data);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BlocBuilder<GenericBloc<String>, GenericState<String>>(
                            bloc: imageCubit,
                            builder: (context, state) {
                              if (state is GenericUpdateState) {
                                return CachedImage(
                                  borderRadius: BorderRadius.circular(8.r),
                                  url: state.data.replaceAll("\\", "/"),
                                  width: device.isTablet ? 35.w : 50.w,
                                  height: device.isTablet ? 35.w : 50.w,
                                  fit: BoxFit.cover,
                                  bgColor: MyColors.defaultImgBg,
                                  placeHolder: BusinessPlaceholder(),
                                );
                              } else {
                                return BuildShimmerView(
                                  width: device.isTablet ? 35.w : 50.w,
                                  height: device.isTablet ? 35.w : 50.w,
                                );
                              }
                            }),
                        SizedBox(width: 8),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              MyText(
                                title: widget.appointmentOrdersModel
                                    .getBusinessName(),
                                color: MyColors.grey,
                                size: device.isTablet ? 7.sp : 11.sp,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 10.h),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      Res.appointmentInactive,
                                      width: 12.h,
                                      height: 12.h,
                                      color: MyColors.black,
                                    ),
                                    const SizedBox(width: 8),
                                    MyText(
                                      title: widget.dashboardData.formatDate(
                                        widget.appointmentOrdersModel
                                            .activeAppDateTime,
                                        device.locale,
                                      ),
                                      color: MyColors.black,
                                      fontWeight: FontWeight.bold,
                                      size: device.isTablet ? 7.sp : 11.sp,
                                    ),
                                    MyText(
                                      title: " ${tr("at")} ",
                                      color: MyColors.grey,
                                      size: device.isTablet ? 5.5.sp : 9.sp,
                                    ),
                                    MyText(
                                      title: DateFormat(
                                              "HH:mm",
                                              device.locale ==
                                                      Locale('en', 'US')
                                                  ? "en_US"
                                                  : 'ar_SA')
                                          .format(
                                        widget.appointmentOrdersModel
                                            .activeAppDateTime,
                                      ),
                                      color: MyColors.black,
                                      fontWeight: FontWeight.bold,
                                      size: device.isTablet ? 7.sp : 11.sp,
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        margin:
                                            EdgeInsetsDirectional.only(end: 5),
                                        height: 5.h,
                                        width: 5.h,
                                        decoration: BoxDecoration(
                                          color: widget
                                              .appointmentOrdersModel.statusId
                                              .getDashboardStatusTextColor(),
                                          shape: BoxShape.circle,
                                        ),
                                        alignment: Alignment.center,
                                      ),
                                      MyText(
                                        alien: TextAlign.center,
                                        size: device.isTablet ? 5.5.sp : 9.sp,
                                        fontWeight: FontWeight.bold,
                                        title: widget.appointmentOrdersModel
                                            .getStatus(),
                                        color: widget
                                            .appointmentOrdersModel.statusId
                                            .getDashboardStatusTextColor(),
                                      )
                                    ],
                                  ),
                                  MyText(
                                    alien: TextAlign.center,
                                    size: device.isTablet ? 5.5.sp : 9.sp,
                                    title:
                                        '$bookedServiceLength ${bookedServiceLength>1? tr("Booked_Services") : tr("booked_service")}',
                                    color: MyColors.black,
                                  ),
                                  SizedBox(),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
