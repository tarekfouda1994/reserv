part of 'dashbaord_widgets_imports.dart';

class BuildDeletedItem extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final String picture;
  final DashboardData dashboardData;

  const BuildDeletedItem(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.dashboardData,
      required this.picture})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 8.h, horizontal: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: MyColors.white),
      child: Row(
        children: [
          CachedImage(
              url: picture.replaceAll("\\", "/"),
              width: deviceModel.isTablet ? 35.w : 50.w,
              height: deviceModel.isTablet ? 35.w : 50.w,
              borderRadius: BorderRadius.circular(12.r),
              fit: BoxFit.cover,
              bgColor: MyColors.defaultImgBg,
              placeHolder: BusinessPlaceholder()),
          SizedBox(width: 10.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    MyText(
                      title: dashboardData.formatDate(
                          appointmentOrdersModel.activeAppDateTime,
                          deviceModel.locale),
                      color: MyColors.black,
                      fontWeight: FontWeight.bold,
                      size: deviceModel.isTablet ? 7.sp : 11.sp,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: deviceModel.isTablet ? 6.w : 8.w,
                          vertical: deviceModel.isTablet ? 5.h : 4.5.h),
                      margin: EdgeInsetsDirectional.only(start: 20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: appointmentOrdersModel.statusId
                              .getStatusBgColor(),
                          border: Border.all(
                              color: appointmentOrdersModel.statusId
                                  .getStatusTextColor())),
                      child: MyText(
                          alien: TextAlign.center,
                          size: deviceModel.isTablet ? 6.sp : 9.sp,
                          title: appointmentOrdersModel.getStatus(),
                          color: MyColors.white),
                    ),
                  ],
                ),
                SizedBox(height: 2.h),
                Row(
                  children: [
                    Icon(
                      Icons.access_time,
                      color: MyColors.grey,
                      size: deviceModel.isTablet ? 12.sp : 15.sp,
                    ),
                    MyText(
                        title: " " +
                            DateFormat(
                              "HH:mm",
                              deviceModel.locale == Locale('en', 'US')
                                  ? "en_US"
                                  : 'ar_SA',
                            ).format(appointmentOrdersModel.activeAppDateTime),
                        color: MyColors.blackOpacity,
                        size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
