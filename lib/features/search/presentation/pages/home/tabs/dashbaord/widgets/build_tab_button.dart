part of 'dashbaord_widgets_imports.dart';

class BuildTabButton extends StatelessWidget {
  final DashboardData dashboardData;
  final int index;
  final StatusModel statusModel;

  const BuildTabButton({
    Key? key,
    required this.index,
    required this.dashboardData,
    required this.statusModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: dashboardData.tabButtonBloc,
      builder: (cxt, state) {
        return InkWell(
          onTap: () => dashboardData.onChangeStatus(index),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 18.r, vertical: 6.h),
            margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 5.r),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.r),
              color: state.data == index ? MyColors.primary : MyColors.primary.withOpacity(.05),
            ),
            alignment: Alignment.center,
            child: MyText(
              alien: TextAlign.center,
              size: device.isTablet ? 7.sp : 9.sp,
              title:
                  statusModel.getStatus().isNotEmpty ? statusModel.getStatus() : statusModel.text,
              color: state.data == index ? MyColors.white : MyColors.primary,
            ),
          ),
        );
      },
    );
  }
}
