part of 'dashbaord_widgets_imports.dart';

class BuildWithoutLoginView extends StatelessWidget {
  final DashboardData dashboardData;

  const BuildWithoutLoginView({Key? key, required this.dashboardData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(children: [
      BuildSearchWithoutLogin(dashboardData: dashboardData),
      Flexible(
        child: BlocBuilder<GenericBloc<List<AppointmentOrdersModel>>,
                GenericState<List<AppointmentOrdersModel>>>(
            bloc: dashboardData.ordersCubit,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                return ListView(children: [
                  if (state.data.isEmpty)
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 130.h),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SvgPicture.asset(Res.empty_group),
                          Container(
                            width: MediaQuery.of(context).size.width * .8,
                            padding: EdgeInsets.only(top: 15.h, bottom: 8.h),
                            child: MyText(
                                alien: TextAlign.center,
                                color: MyColors.black,
                                title: tr("un_found_appointment"),
                                size: device.isTablet ? 7.sp : 11.sp),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * .6,
                            child: MyText(
                                alien: TextAlign.center,
                                color: MyColors.grey,
                                title: tr("validation_appointment"),
                                size: device.isTablet ? 7.sp : 11.sp),
                          ),
                        ],
                      ),
                    ),
                  ...List.generate(
                    state.data.length,
                    (index) {
                      return BuildDashboardCard(
                        appointmentOrdersModel: state.data[index],
                        index: index,
                        dashboardData: dashboardData,
                      );
                    },
                  ),
                ]);
              } else {
                return BuildUnLoginView();
              }
            }),
      )
    ]);
  }
}
