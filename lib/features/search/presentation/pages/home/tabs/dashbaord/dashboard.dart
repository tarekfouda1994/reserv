part of 'dashboard_imports.dart';

class Dashboard extends StatefulWidget {
  final HomeData homeData;

  const Dashboard({Key? key, required this.homeData}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final DashboardData dashboardData = DashboardData();

  @override
  void initState() {
    dashboardData.getStatus(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: GradientScaffold(
        appBar: DefaultAppBar(
          onBack: () => widget.homeData.animateTabsPages(0, context),
          title: tr("my_appointment"),
        ),
        body: BuildDashBoardBody(dashboardData: dashboardData),
      ),
    );
  }
}
