part of 'dashbaord_widgets_imports.dart';

class BuildEmptyBody extends StatelessWidget {
  final DashboardData dashboardData;
  const BuildEmptyBody({Key? key, required this.dashboardData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    String status = dashboardData.statusCubit.state.data[dashboardData.tabButtonBloc.state.data].getStatus();
    return Container(
      color: MyColors.white,
      height: MediaQuery.of(context).size.height - 200.h,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(Res.empty_group),
            Container(
              width: MediaQuery.of(context).size.width * .8,
              padding: EdgeInsets.symmetric(vertical: 15.h),
              child: MyText(
                  alien: TextAlign.center,
                  color: MyColors.black,
                  title:device.locale.languageCode=="en"?  "${tr("no")} $status${tr("no_appointment")}":"${tr("no_appointment")} $status",
                  size: device.isTablet ? 7.sp : 11.sp),
            ),
          ],
        ),
      ),
    );
  }
}
