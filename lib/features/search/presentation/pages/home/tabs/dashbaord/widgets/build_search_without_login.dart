part of 'dashbaord_widgets_imports.dart';

class BuildSearchWithoutLogin extends StatelessWidget {
  final DashboardData dashboardData;

  const BuildSearchWithoutLogin({Key? key, required this.dashboardData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Visibility(
      visible: device.auth == false,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(
            horizontal: device.isTablet ? 12.sp : 20.sp, vertical: 8.sp),
        color: MyColors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: CustomTextField(
                          radius: BorderRadius.circular(15),
                          label: tr("reference_no"),
                          controller: dashboardData.searchWithoutLogin,
                          fieldTypes: FieldTypes.normal,
                          type: TextInputType.name,
                          action: TextInputAction.done,
                          validate: (value) {},
                          hint: "${tr("ex")} 2210055",
                          contentPadding: EdgeInsets.symmetric(
                              vertical: device.isTablet ? 10.sp : 8.sp,
                              horizontal: device.isTablet ? 8.sp : 12.w),
                          margin: EdgeInsets.symmetric(vertical: 6.h,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: device.isTablet ? 15.sp : 10),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: CustomTextField(
                          radius: BorderRadius.circular(15),
                          label: tr("last_name"),
                          controller: dashboardData.lastName,
                          fieldTypes: FieldTypes.normal,
                          type: TextInputType.name,
                          action: TextInputAction.done,
                          validate: (value) {},
                          hint: "${tr("ex")} ${tr("ex_name")}",
                          contentPadding: EdgeInsets.symmetric(
                              vertical: device.isTablet ? 10.sp : 8.sp,
                              horizontal: device.isTablet ? 8.sp : 12.w),
                          margin: EdgeInsets.symmetric(vertical: 6.h,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            InkWell(
              onTap: () => dashboardData.submitSearchWithoutLogin(),
              borderRadius: BorderRadius.circular(10),
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 6.r, bottom: 3.r),
                width: MediaQuery.of(context).size.width,
                height: 35.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.r),
                  color: MyColors.primary,
                ),
                child: MyText(
                  alien: TextAlign.center,
                  color: MyColors.white,

                  size: device.isTablet ? 8.sp : 13.sp,
                  title: tr("search"),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
