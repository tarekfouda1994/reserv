part of 'dashboard_imports.dart';

class DashboardData {
  final GlobalKey<FormState> formKey = GlobalKey();
  final GlobalKey<FormState> searchFormKey = GlobalKey();
  TextEditingController search = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController rateMsg = TextEditingController();
  TextEditingController searchWithoutLogin = TextEditingController();
  final GenericBloc<int> tabButtonBloc = GenericBloc(0);
  final GenericBloc<double> rateCubit = GenericBloc(0);
  final GenericBloc<String> viewCubit = GenericBloc('');
  final GenericBloc<bool> searchButtonCubit = GenericBloc(false);
  final GenericBloc<List<StatusModel>> statusCubit = GenericBloc([]);
  final GenericBloc<List<AppointmentOrdersModel>> ordersCubit = GenericBloc([]);
  final GenericBloc<List<CountChatModel>> msgCountCubit = GenericBloc([]);
  int status = 0;

  getStatus(BuildContext context, {bool refresh = true}) async {
    var data = await GetOrderStatus()(refresh);
    statusCubit.onUpdateData(data);
    if (data.isNotEmpty) {
      var auth = context.read<DeviceCubit>().state.model.auth;
      if (auth) {
        onChangeStatus(0);
      }
    }
  }

  getOrders(int? status,
      {bool refresh = true,
      String? lastName,
      String? searchRef,
      String? orderNumber}) async {
    ordersCubit.onUpdateToInitState([]);
    AppointmentOrderParams params = _appointmentOrderParams(
        status, refresh, searchRef, lastName, orderNumber);
    var data = await GetDashboardData()(params);
    ordersCubit.onUpdateData(data.reversed.toList());
  }

  AppointmentOrderParams _appointmentOrderParams(int? status, bool refresh,
      String? searchRef, String? lastName, String? orderNumber) {
    var params = AppointmentOrderParams(
        status: status,
        refresh: refresh,
        filter: searchRef,
        lastName: lastName,
        orderNumber: orderNumber);
    return params;
  }

  Future<String> getAppointmentImage(String businessId,
      {bool refresh = true}) async {
    BusinessImagesParams params = _businessImagesParams(businessId, refresh);
    var data = await GetBusinessImage()(params);
    return data;
  }

  BusinessImagesParams _businessImagesParams(String businessId, bool refresh) {
    var params = BusinessImagesParams(
        businessId: businessId, refresh: refresh, firstOnly: true);
    return params;
  }

  cancelOrder(
      BuildContext context, String id, int appointmentType, int index) async {
    var params = CancelOrderParams(id: id, appointmentType: appointmentType);
    var result = await CancelOrder()(params);
    if (result) {
      _onHandleCancelOrder(context, index);
    }
  }

  void _onHandleCancelOrder(BuildContext context, int index) {
    ordersCubit.state.data.removeAt(index);
    ordersCubit.onUpdateData(ordersCubit.state.data);
    Navigator.of(context).pop();
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        CustomToast.showSimpleToast(
          msg: tr("success_cancel_order"),
          type: ToastType.success,
          title: tr("success_toast"),
        );
      },
    );
  }

  showConfirmCancelDialog(
    BuildContext context,
    int index,
    AppointmentOrdersModel appointmentOrdersModel,
    String picture,
    DashboardData dashboardData,
  ) {
    var device = context.read<DeviceCubit>().state.model;

    CustomToast.customConfirmDialog(
      height: device.isTablet ? 220.h : 200.h,
      recordItem: BuildDeletedItem(
          appointmentOrdersModel: appointmentOrdersModel,
          picture: picture,
          dashboardData: dashboardData),
      onConfirm: () => cancelOrder(
        context,
        appointmentOrdersModel.bookingOrderID,
        appointmentOrdersModel.appointmentType,
        index,
      ),
      content: tr("sure_cancel"),
      btnName: tr("cancel"),
      title: tr("Cancel_appointment"),
    );
  }

  showRateDialog(BuildContext context, DashboardData dashboardData,
      AppointmentOrdersModel appointmentOrdersModel) {
    showDialog(
      context: context,
      builder: (context) {
        return BuildRateDialog(
            dashboardData: dashboardData,
            appointmentOrdersModel: appointmentOrdersModel);
      },
    );
  }

  onChangeSearch(BuildContext context, String value) {
    var lastName = context.read<UserCubit>().state.model?.lastName;
    viewCubit.onUpdateData(value);
    Future.delayed(Duration(milliseconds: 2000)).then((value) {
      if (search.text.isNotEmpty) {
        getOrders(null,
            orderNumber: search.text, lastName: lastName, refresh: false);
      } else {
        getOrders(0);
      }
    });
  }

  submitSearch(BuildContext context) async {
    var lastName = context.read<UserCubit>().state.model?.lastName;
    if (search.text.trim().isNotEmpty) {
      getIt<LoadingHelper>().showLoadingDialog();
      await getOrders(status,
          refresh: false, lastName: lastName, orderNumber: search.text);
      getIt<LoadingHelper>().dismissDialog();
    }
  }

  createRate(BuildContext context,
      AppointmentOrdersModel appointmentOrdersModel) async {
    if (formKey.currentState!.validate()) {
      await _applyRating(context, _buildRateParams(appointmentOrdersModel));
    }
  }

  Future<void> _applyRating(BuildContext context, RateParams params) async {
    var result = await RatingService()(params);
    if (result) {
      rateMsg.clear();
      AutoRouter.of(context).pop();
      CustomToast.showSimpleToast(
          msg: tr("Success_Rating"),
          type: ToastType.success,
          title: tr("success_toast"));
    }
  }

  RateParams _buildRateParams(AppointmentOrdersModel appointmentOrdersModel) {
    return RateParams(
        ratingID: rateCubit.state.data.toInt(),
        ratingComments: rateMsg.text,
        bookingOrderID: appointmentOrdersModel.bookingOrderID,
        businessID: appointmentOrdersModel.businessID,
        orderNumber: appointmentOrdersModel.orderNumber,
        email: appointmentOrdersModel.email);
  }

  submitSearchWithoutLogin() async {
    if (searchWithoutLogin.text.isEmpty) {
      return;
    }
    if (lastName.text.isEmpty) {
      CustomToast.showSimpleToast(
        msg: tr("name_to_continue"),
        type: ToastType.warning,
        title: tr("Warning_Toast"),
      );
      return;
    }
    if (searchWithoutLogin.text.isNotEmpty && lastName.text.isNotEmpty) {
      getIt<LoadingHelper>().showLoadingDialog();
      await getOrders(null,
          lastName: lastName.text,
          orderNumber: searchWithoutLogin.text,
          refresh: false);
      getIt<LoadingHelper>().dismissDialog();
    }
  }

  String formatDate(DateTime? dateTime, Locale locale) {
    String date = "";
    var formatter = DateFormat.yMMMd('ar_SA');
    if (locale == Locale('en', 'US')) {
      date = DateFormat("d-MMM-yyyy").format(dateTime ?? DateTime(0));
    } else {
      date = formatter.format(dateTime ?? DateTime(0));
    }
    return date;
  }

  onChangeStatus(int index) {
    tabButtonBloc.onUpdateData(index);
    status = statusCubit.state.data[index].value;
    getOrders(status);
  }

  bottomSheetSOptions(BuildContext context, DashboardData dashboardData,
      AppointmentOrdersModel appointmentOrdersModel, int index, String image) {
    showModalBottomSheet(
      elevation: 10,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      isScrollControlled: true,
      builder: (cxt) {
        return BuildBottomSheetOptions(
          image: image,
          index: index,
          appointmentOrdersModel: appointmentOrdersModel,
          dashboardData: dashboardData,
        );
      },
    );
  }

  void navigateToRescheduleScreen(
      BuildContext context, AppointmentOrdersModel model) async {
    final auth = context.read<DeviceCubit>().state.model.auth;
    var services = model.services.map((e) {
      return _rescheduleParams(e, model);
    }).toList();
    if (services
        .where((element) => element.businessServiceId.isNotEmpty)
        .isNotEmpty) {
      await _navToReschedule(context, model, services);
      if (auth) getOrders(status);
    } else {
      CustomToast.showSimpleToast(
          msg: tr("can't_reschedule"),
          type: ToastType.info,
          title: tr("reschedule_info"));
      return;
    }
  }

  Future<void> _navToReschedule(
    BuildContext context,
    AppointmentOrdersModel model,
    List<RescheduleParams> services,
  ) async {
    Navigator.of(context).pop();
    await AutoRouter.of(context).push(RescheduleAppointmentRoute(
      businessID: model.businessID,
      orderNumber: model.orderNumber,
      lastName: model.lastName,
      services: _rescheduleServices(services),
    ));
  }

  List<RescheduleParams> _rescheduleServices(List<RescheduleParams> services) {
    return services
        .where((element) => element.businessServiceId.isNotEmpty)
        .toList();
  }

  RescheduleParams _rescheduleParams(
      ServicesModel e, AppointmentOrdersModel model) {
    if (e.statusID == 0 || e.statusID == 6) {
      return RescheduleParams(
        id: e.appointmentId,
        businessId: model.businessID,
        appointmentType: model.appointmentType,
        staffID: e.staffID,
        businessServiceId: e.businessServiceID,
        appointmentDate: "",
        businessServiceName: e.serviceNameEN,
        fromTime: "",
      );
    } else {
      return RescheduleParams(
        id: "",
        businessId: "",
        appointmentType: 0,
        staffID: "",
        businessServiceId: "",
        appointmentDate: "",
        businessServiceName: "",
        fromTime: "",
      );
    }
  }
}
