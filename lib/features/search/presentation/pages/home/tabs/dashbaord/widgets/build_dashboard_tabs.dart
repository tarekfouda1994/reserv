part of 'dashbaord_widgets_imports.dart';

class BuildDashboardTabs extends StatelessWidget {
  final DashboardData dashboardData;
  final String filter;

  const BuildDashboardTabs(
      {Key? key, required this.dashboardData, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        Offstage(
          offstage: filter.isNotEmpty,
          child: BlocBuilder<GenericBloc<List<StatusModel>>,
                  GenericState<List<StatusModel>>>(
              bloc: dashboardData.statusCubit,
              builder: (context, statusState) {
                if (statusState is GenericUpdateState) {
                  if (statusState.data.isEmpty) {
                    return Container();
                  }
                  return Container(
                    height: device.isTablet ? 55.h : 50.h,
                    color: MyColors.white,
                    // margin: EdgeInsets.symmetric(vertical: 5.h),
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(
                        start: device.isTablet ? 8.w : 10.w,
                      ),
                      child: ListView.builder(
                        itemCount: statusState.data.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (cxt, index) {
                          return BuildTabButton(
                            statusModel: statusState.data[index],
                            index: index,
                            dashboardData: dashboardData,
                          );
                        },
                      ),
                    ),
                  );
                }
                return Container(
                  padding: const EdgeInsetsDirectional.only(start: 16.0),
                  height: device.isTablet ? 60.h : 50.h,
                  child: ListView.builder(
                    itemCount: 10,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (cxt, index) {
                      return Padding(
                        padding: const EdgeInsetsDirectional.only(
                          end: 10,
                          top: 4,
                          bottom: 4,
                        ),
                        child: BuildShimmerView(
                          height: 30.h,
                          width: 100,
                          borderRadius: 50,
                        ),
                      );
                    },
                  ),
                );
              }),
        ),
        Divider(height: 0),
      ],
    );
  }
}
