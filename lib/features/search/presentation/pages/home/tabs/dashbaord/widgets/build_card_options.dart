part of 'dashbaord_widgets_imports.dart';

class BuildCardOptions extends StatelessWidget {
  final AppointmentOrdersModel appointmentOrdersModel;
  final DashboardData dashboardData;
  final String image;
  final int index;

  const BuildCardOptions(
      {Key? key,
      required this.appointmentOrdersModel,
      required this.dashboardData,
      required this.index,
      required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool rescheduleStatus =
        appointmentOrdersModel.services.any((element) => element.statusID == 3);
    var device = context.watch<DeviceCubit>().state.model;
    var statusId = appointmentOrdersModel.statusId;
    return Container(
      margin: EdgeInsets.only(top: 2),
      padding: EdgeInsets.symmetric(horizontal: 16),
      height: device.isTablet ? 70.h : 80.sm,
      color: Color(0xffD6ECEC),
      child: Row(
        mainAxisAlignment: (statusId == 0 || statusId == 5 || statusId == 3)
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          BuildIconItem(
            onTap: () => getIt<Utilities>().navigateToMapWithDirection(
              lat: appointmentOrdersModel.latitude,
              lng: appointmentOrdersModel.longitude,
              title: appointmentOrdersModel.businessAddressEN,
              context: context,
            ),
            backGroundColor: MyColors.white,
            img: Transform.scale(
                scale: device.isTablet ? .6 : 1,
                child: SvgPicture.asset(Res.location)),
            showTitle: true,
            paddingIcon: 10,
            title: tr("Direction"),
          ),
          if ((statusId == 0 && !rescheduleStatus) || statusId == 5)
            BuildIconItem(
                onTap: () => dashboardData.showConfirmCancelDialog(context,
                    index, appointmentOrdersModel, image, dashboardData),
                backGroundColor: MyColors.white,
                img: Padding(
                  padding: EdgeInsets.all(3.0.r),
                  child: Icon(
                    Icons.clear,
                    size: device.isTablet ? 16.sp : 20.sp,
                    color: MyColors.errorColor,
                  ),
                ),
                paddingIcon: 6,
                titleColor: MyColors.errorColor,
                showTitle: true,
                title: tr("cancel")),
          if ((statusId == 0 && !rescheduleStatus) || statusId == 6)
            BuildIconItem(
                onTap: () => dashboardData.navigateToRescheduleScreen(
                    context, appointmentOrdersModel),
                iconColor: MyColors.white,
                backGroundColor: MyColors.white,
                img: Transform.scale(
                  scale: device.isTablet ? .6 : .8,
                  child: SvgPicture.asset(
                    Res.ReserveAgain,
                    width: 16.w,
                    height: 16.h,
                  ),
                ),
                paddingIcon: 6,
                showTitle: true,
                title: tr("reschedule")),
          InkWell(
            onTap: () async {
              var result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => DashboardDetails(
                          status: dashboardData.status,
                          appointmentOrdersModel: appointmentOrdersModel)));
              dashboardData.ordersCubit.state.data
                  .removeWhere((element) => element.bookingOrderID == result);
              dashboardData.ordersCubit
                  .onUpdateData(dashboardData.ordersCubit.state.data);
            },
            child: BuildIconItem(
              titleColor: MyColors.primary,
              iconColor: MyColors.white,
              backGroundColor: MyColors.primary,
              icon: Icons.visibility_outlined,
              paddingIcon: 6,
              showTitle: true,
              title: tr("View"),
            ),
          ),
        ],
      ),
    );
  }
}
