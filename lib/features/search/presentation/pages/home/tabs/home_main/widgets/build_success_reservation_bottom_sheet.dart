part of 'home_main_widgets_imports.dart';

class BuildSuccessReservationBottomSheet extends StatelessWidget {
  final CancelFunc toastCancel;

  const BuildSuccessReservationBottomSheet(
      {Key? key, required this.toastCancel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<ReservationDetailsCubit, ReservationDetailsState>(
        builder: (context, reserveState) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 100.h),
            padding: EdgeInsets.symmetric(vertical: 5),
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: MyColors.blackOpacity.withOpacity(.7),
                  offset: Offset(0, 15),
                  spreadRadius: -15,
                  blurRadius: 20.0,
                ),
              ],
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(12.r),
              ),
              color: Colors.white,
            ),
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0.w, vertical: 5),
              padding: EdgeInsets.symmetric(horizontal: 5.0.w, vertical: 12.h),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(12.sp),
                    bottom: Radius.circular(12.sp),
                  ),
                  color: Colors.white),
              child: Column(
                children: [
                  BuildReservationDetailsHeader(
                    toastCancel: toastCancel,
                    device: device,
                    model: reserveState.model!,
                  ),
                  BuildSuccessDetails(
                    toastCancel: toastCancel,
                    device: device,
                    model: reserveState.model!,
                  ),
                ],
              ),
            ),
          ),
        ],
      );
    });
  }
}
