part of 'home_main_widgets_imports.dart';

class BuildHomeMainBody extends StatelessWidget {
  final HomeMainData mainData;
  final GenericBloc<int> pagesCubit;
  final String filter;
  final SelectedDateTimeEntity dateTime;

  const BuildHomeMainBody({
    Key? key,
    required this.mainData,
    required this.filter,
    required this.dateTime, required this.pagesCubit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: pagesCubit,
      builder: (_, state) {
        return AnimatedSwitcher(
          duration: Duration(milliseconds: 500),
          reverseDuration: Duration(milliseconds: 500),
          child: mainData.getPages(mainData, state.data, filter, dateTime),
          transitionBuilder: (child, animation) {
            return FadeTransition(
              child: child,
              opacity: animation,
            );
          },
        );
      },
    );
  }
}
