part of 'home_main_widgets_imports.dart';

class BuildPopularCategories extends StatelessWidget {
  final HomeMainData data;
  final VoidCallback? callback;

  const BuildPopularCategories({Key? key, required this.data, this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<PopularCatModel>>,
        GenericState<List<PopularCatModel>>>(
      bloc: data.popularCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BuildDeptHeader(title: tr("popular_cat"), hasMore: false),
              SizedBox(height: 8),
              SizedBox(
                height: device.isTablet ? 90.h : 75.h,
                child: ListView.separated(
                  controller: data.catScrollController,
                  padding: EdgeInsets.symmetric(horizontal: 15.w),
                  scrollDirection: Axis.horizontal,
                  itemCount: state.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return BuildCategoryItem(
                      model: state.data[index],
                      homeMainData: data,
                      callback: callback,
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(width: 10.w);
                  },
                ),
              ),
              Divider(),
            ],
          );
        }
        return Padding(
          padding: EdgeInsets.only(top: 6.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BuildDeptHeader(title: tr("popular_cat"), hasMore: false),
              SizedBox(
                height: device.isTablet ? 90.h : 85.h,
                child: ListView.separated(
                  padding: EdgeInsets.symmetric(
                      horizontal: device.isTablet ? 18.w : 15.w),
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        BuildShimmerView(
                          width: 100.w,
                          height: device.isTablet ? 60.h : 53.h,
                        ),
                        BuildShimmerView(
                          width: 100.w,
                          height: device.isTablet ? 12.h : 8.h,
                        ),
                      ],
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(width: 10.w);
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
