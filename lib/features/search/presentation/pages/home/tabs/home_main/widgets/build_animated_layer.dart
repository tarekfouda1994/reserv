part of 'home_main_widgets_imports.dart';

class BuildAnimatedLayer extends StatelessWidget {
  final HomeMainData homeMainData;
  final Widget child;

  const BuildAnimatedLayer({Key? key, required this.homeMainData, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: homeMainData.scrollCubit,
        builder: (context, state) {
          return BlocBuilder<GenericBloc<int>, GenericState<int>>(
            bloc: homeMainData.filterCountCubit,
            builder: (context, filterState) {
              return AnimatedSize(
                duration: Duration(milliseconds: 500),
                child: SizedBox(
                  child: child,
                  height: state.data ? 45.h : 0,
                ),
              );
            },
          );
        });
  }
}
