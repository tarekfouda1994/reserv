part of 'home_main_widgets_imports.dart';

class BuildListView extends StatefulWidget {
  final HomeMainData homeMainData;
  final String filter;
  final SelectedDateTimeEntity dateTime;

  const BuildListView({
    Key? key,
    required this.homeMainData,
    required this.filter,
    required this.dateTime,
  }) : super(key: key);

  @override
  State<BuildListView> createState() => _BuildListViewState();
}

class _BuildListViewState extends State<BuildListView> {
  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarBrightness: Brightness.dark),
    );
    widget.homeMainData.initScrollListener();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: widget.homeMainData.emptyListCubit,
      builder: (context, state) {
        return BlocBuilder<SearchCubit, SearchState>(
          builder: (context, state) {
            return Container(
              color: MyColors.white,
              child: Column(
                children: [
                  BuildHeaderHome(
                    homeMainData: widget.homeMainData,
                  ),
                  Expanded(
                    child: RefreshIndicator(
                      color: MyColors.primary,
                      onRefresh: () => widget.homeMainData.fitchHomeData(
                        context,
                        widget.homeMainData,
                      ),
                      backgroundColor: MyColors.white,
                      child: ScrollConfiguration(
                        behavior: MyBehavior(),
                        child: _viewBody(state),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  Widget _viewBody(SearchState state) {
    if (state.filters.isEmpty || state.filters.length == 0) {
      return CustomMainListView(
        homeMainData: widget.homeMainData,
        dateTime: widget.dateTime,
        filter: widget.filter,
      );
    } else {
      return BuildOnSearchListView(
        homeMainData: widget.homeMainData,
        onNoResultListView: CustomMainListView(
          homeMainData: widget.homeMainData,
          dateTime: widget.dateTime,
          filter: widget.filter,
          hasScrolling: false,
        ),
      );
    }
  }
}
