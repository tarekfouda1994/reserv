part of "home_main_widgets_imports.dart";

class CustomMainListView extends StatelessWidget {
  final HomeMainData homeMainData;
  final String filter;
  final SelectedDateTimeEntity dateTime;
  final bool hasScrolling;

  const CustomMainListView({
    Key? key,
    required this.homeMainData,
    required this.filter,
    required this.dateTime,
    this.hasScrolling = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var auth = context.watch<DeviceCubit>().state.model.auth;
    var search = context.watch<SearchCubit>().state;
    return Visibility(
      visible: _checkVisibleEmptyView(),
      replacement: BuildEmptyServicesView(),
      child: ListView(
        physics: hasScrolling
            ? AlwaysScrollableScrollPhysics()
            : NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        controller: homeMainData.scrollController,
        padding: EdgeInsets.only(top: 8),
        children: [
          if (_onNoResultSearchCheck(search))
            BuildPopularCategories(data: homeMainData),
          if (_onNoResultSearchCheck(search))
            BuildMainBannersView(
              homeMainData: homeMainData,
            ),
          if (_onNoResultSearchCheck(search))
            BuildSliderView(
              homeMainData: homeMainData,
              filter: filter,
            ),
          BuildServicesStickyHeader(
            homeApisType: HomeApisType.offers,
            homeDepartments: HomeDepartments.offers,
            auth: auth,
            title: HomeDepartments.offers.getTitle(filter),
            homeMainData: homeMainData,
            bloc: homeMainData.servicesOffersCubit,
          ),
          BuildServicesStickyHeader(
            homeApisType: HomeApisType.recommended,
            homeDepartments: HomeDepartments.recommended,
            auth: auth,
            title: HomeDepartments.recommended.getTitle(filter),
            homeMainData: homeMainData,
            bloc: homeMainData.recommendedCubit,
          ),
          BuildServicesStickyHeader(
            homeApisType: HomeApisType.recently,
            homeDepartments: HomeDepartments.recently,
            auth: auth,
            title: HomeDepartments.recently.getTitle(filter),
            homeMainData: homeMainData,
            bloc: homeMainData.recentlyCubit,
          ),
          BuildFavoriteStickyHeaders(
            title: tr("favorite_services"),
            homeMainData: homeMainData,
          ),
          BuildServicesStickyHeader(
            homeApisType: HomeApisType.popular,
            homeDepartments: HomeDepartments.popular,
            auth: true,
            title: HomeDepartments.popular.getTitle(filter),
            homeMainData: homeMainData,
            bloc: homeMainData.popularServicesCubit,
          ),
          if (homeMainData.checkFiltration(filter, dateTime))
            BuildTopBusinessesStickyHeader(
              title: tr("top_partners"),
              homeMainData: homeMainData,
            ),
          BuildServicesStickyHeader(
            homeApisType: HomeApisType.trending,
            homeDepartments: HomeDepartments.trending,
            auth: true,
            title: HomeDepartments.trending.getTitle(filter),
            homeMainData: homeMainData,
            bloc: homeMainData.trendingServiceCubit,
          ),
          SizedBox(height: 25.h),
        ],
      ),
    );
  }

  bool _onNoResultSearchCheck(SearchState search) =>
      homeMainData.checkFiltration(filter, dateTime) && !search.noSearchResult;

  bool _checkVisibleEmptyView() {
    var state = homeMainData.emptyListCubit.state;
    return state.data ||
        state is! GenericUpdateState ||
        homeMainData.allFinishLoading == 0;
  }
}
