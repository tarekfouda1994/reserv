import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/bloc/search_cubit/search_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/enums.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/core/widgets/custom_slider_widget.dart';
import 'package:flutter_tdd/features/search/data/models/trinding_model/trinding_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/home_main/views/all_search_text_result/all_search_text_result_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/trending_services_page/widgets/trending_services_widgets_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_service_card.dart';
import 'package:flutter_tdd/features/search/presentation/widgets/widgets_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../../../../../res.dart';

part 'build_empty_services_view_on_filter.dart';
part 'build_services_result_item.dart';
part 'build_vertical_list_view.dart';