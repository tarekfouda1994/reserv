part of 'home_main_widgets_imports.dart';

class BuildMainBannersView extends StatelessWidget {
  final HomeMainData homeMainData;
  final bool showBottomSpase;

  const BuildMainBannersView(
      {Key? key, required this.homeMainData, this.showBottomSpase = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<HomeBannersModel?>,
            GenericState<HomeBannersModel?>>(
        bloc: homeMainData.mainBannersCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data!.itemsList.isEmpty) {
              return Container();
            }
            return Padding(
              padding: const EdgeInsets.only(top: 18),
              child: Column(
                children: [
                  BuildDeptHeader(
                    title: tr("main_banners"),
                    bockGroundColorCount: MyColors.primary,
                    counterColor: MyColors.white,
                    count: state.data!.totalRecords,
                    onTap: () => AutoRouter.of(context).push(
                      AllBannerPageRoute(),
                    ),
                    hasMore: true,
                  ),
                  SizedBox(height: 8),
                  CustomSliderWidget(
                    height: device.isTablet ? 190.sm : 100.sm,
                    items: List.generate(state.data!.itemsList.length, (index) {
                      return BuildBannerItem(
                        model: state.data!.itemsList[index],
                      );
                    }),
                  ),
                ],
              ),
            );
          }
          return Column(
            children: [
              BuildDeptHeader(title: tr("main_banners"), hasMore: false),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: BuildShimmerView(
                  height: device.isTablet ? 130.h : 100.h,
                ),
              ),
              if (showBottomSpase) SizedBox(height: 30.h),
            ],
          );
        });
  }
}
