part of 'home_main_widgets_imports.dart';

class BuildBannerItem extends StatelessWidget {
  final BannerModel model;

  const BuildBannerItem({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => AutoRouter.of(context).push(
        ProductDetailsRoute(id: model.businessID),
      ),
      child: Container(
        margin: EdgeInsetsDirectional.only(end: 12, bottom: 18),
        child: CachedImage(
          url: model.avatar.replaceAll("\\", "/"),
          borderRadius: BorderRadius.circular(10.r),
          fit: BoxFit.fill,
          bgColor: MyColors.defaultImgBg,
          placeHolder: ServicePlaceholder(),
        ),
      ),
    );
  }
}
