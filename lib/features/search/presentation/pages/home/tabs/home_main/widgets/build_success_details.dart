part of 'home_main_widgets_imports.dart';

class BuildSuccessDetails extends StatelessWidget {
  final DeviceModel device;
  final ReservationDetailsWithoutLoginModel model;
  final CancelFunc toastCancel;

  const BuildSuccessDetails(
      {Key? key,
      required this.device,
      required this.model,
      required this.toastCancel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 16.h),
      child: GestureDetector(
        onTap: () => _goToDetails(),
        child: Container(
          padding: EdgeInsets.all(12.h),
          decoration: BoxDecoration(
            color: Color(0xffF2F6F6),
            borderRadius: BorderRadius.circular(12.r),
          ),
          child: Row(
            children: [
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            title: _convertNumToAr(context, model.date),
                            color: MyColors.black,
                            size: device.isTablet ? 7.5.sp : 11.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(width: 10.h),
                          Expanded(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.access_time_rounded,
                                  color: MyColors.black,
                                  size: device.isTablet ? 12.sp : 16.sp,
                                ),
                                Expanded(
                                  child: MyText(
                                    title: _convertNumToAr(
                                      context,
                                      " ${model.reservationTimes.join(" - ")}",
                                    ),
                                    color: MyColors.black,
                                    size: device.isTablet ? 7.sp : 11.sp,

                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 7.h),
                      MyText(
                        title: model.businessName,
                        color: MyColors.black,

                        size: device.isTablet ? 7.sp : 11.sp,
                      ),
                    ]),
              ),
              Icon(
                device.locale.languageCode != "en" ? Icons.west : Icons.east,
                color: MyColors.black,
                size: device.isTablet ? 10.sp : 18.sp,
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _convertNumToAr(BuildContext context, String text) {
    return getIt<Utilities>().convertNumToAr(context: context, value: text);
  }

  void _goToDetails() {
    var cxt = getIt<GlobalContext>().context();
    toastCancel();
    Future.delayed(
      Duration(milliseconds: 200),
      () {
        AutoRouter.of(cxt).push(DashboardDetailsRoute(
            status: model.appointmentOrdersModel.statusId,
            appointmentOrdersModel: model.appointmentOrdersModel));
      },
    ).then((value) =>
        cxt.read<ReservationDetailsCubit>().onUpdateReservationData(null));
  }
}
