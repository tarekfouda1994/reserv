part of 'home_main_widgets_imports.dart';

class BuildHomeAppBar extends StatelessWidget implements PreferredSizeWidget {
  final HomeMainData homeMainData;
  final GenericBloc<int> pagesCubit;
  final DeviceModel model;
  final TrendingServicesItemModel? trendingServicesItemModel;

  const BuildHomeAppBar(
      {Key? key,
      required this.homeMainData,
      required this.model,
      this.trendingServicesItemModel,
      required this.pagesCubit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: MyColors.primary,
      automaticallyImplyLeading: false,
      elevation: 0,
      toolbarHeight: 60.h,
      title: BlocBuilder<SearchCubit, SearchState>(
        builder: (context, state) {
          return GestureDetector(
            onTap: () => AutoRouter.of(context).push(SearchScreenRoute()),
            child: Container(
              height: 36.h,
              margin: EdgeInsets.only(top: 4).r,
              padding: EdgeInsets.symmetric(vertical: 9, horizontal: 10).r,
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.circular(10.r)),
              child: Row(children: [
                SvgPicture.asset(Res.Search,
                    width: model.isTablet ? 10.w : null),
                SizedBox(width: 6),
                Expanded(
                  child: MyText(
                    title: _getTitle(state),
                    color:
                        _filterCheck(state) ? MyColors.primary : Colors.black54,
                    overflow: TextOverflow.ellipsis,
                    fontWeight: _filterCheck(state) ? FontWeight.bold : null,
                    size: model.isTablet ? 8.sp : 11.sp,
                    letterSpace: model.locale == "en" ? 0 : .22,
                  ),
                ),
                Offstage(
                  offstage: !_filterCheck(state),
                  child: InkWell(
                    onTap: () => homeMainData.resetFilter(
                      context,
                      state.dateTime,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 8.0,
                        vertical: 3,
                      ),
                      child: Icon(
                        Icons.close,
                        size: model.isTablet ? 15.sp : 18.sp,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ]),
            ),
          );
        },
      ),
    );
  }

  bool _filterCheck(SearchState state) =>
      state.filter.isNotEmpty || state.filters.isNotEmpty;

  String _getTitle(SearchState state) {
    if (state.filters.isNotEmpty) {
      return state.filters.join(",");
    } else if (state.filter.isNotEmpty) {
      return state.filter;
    } else {
      return tr("find_your_favourite");
    }
  }

  @override
  Size get preferredSize => Size.fromHeight(50.h);
}
