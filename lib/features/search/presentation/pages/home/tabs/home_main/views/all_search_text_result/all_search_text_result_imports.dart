import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/bloc/search_cubit/search_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/enums.dart';
import 'package:flutter_tdd/core/helpers/loading_helper.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/profile/domain/entities/wish_entity.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/remove_wish_service.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/update_wishe_item.dart';
import 'package:flutter_tdd/features/search/data/models/trending_services_item_model/trending_services_item_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/trinding_entity.dart';
import 'package:flutter_tdd/features/search/domain/use_cases/get_home_services.dart';
import 'package:flutter_tdd/features/search/presentation/manager/location_cubit/location_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/home_main/home_main_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/home_main/views/all_search_text_result/widgets/all_services_widgets_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/home_main/widgets/home_main_widgets_imports.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

import '../../../../../../../data/models/trinding_model/trinding_model.dart';

part 'all_search_text_result.dart';
part 'all_search_text_result_controller.dart';