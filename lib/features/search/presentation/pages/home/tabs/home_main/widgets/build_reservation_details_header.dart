part of 'home_main_widgets_imports.dart';

class BuildReservationDetailsHeader extends StatelessWidget {
  final DeviceModel device;
  final ReservationDetailsWithoutLoginModel model;
  final CancelFunc toastCancel;

  const BuildReservationDetailsHeader(
      {Key? key,
      required this.device,
      required this.model,
      required this.toastCancel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyText(
                title: tr("successful_appointment"),
                color: MyColors.primary,

                fontWeight: FontWeight.bold,
                size: device.isTablet ? 8.sp : 12.sp),
            Row(
              children: [
                SvgPicture.asset(
                  Res.ticket,
                  color: MyColors.greyLight,
                  height: 15.h,
                  width: 15.w,
                ),
                SizedBox(width: 5),
                MyText(
                  title: model.refNumber,
                  color: MyColors.black,

                  size: device.isTablet ? 5.5.sp : 9.sp,
                ),
              ],
            )
          ],
        ),
        Spacer(),
        BuildIconContainer(
          onTap: () => _pop(context),
          borderColor: MyColors.black,
          width: device.isTablet ? 30.r : 35,
          height: device.isTablet ? 30.r : 35,
          bgColor: MyColors.white,
          child: Icon(
            Icons.close,
            size: device.isTablet ? 10.sp : 20,
            color: MyColors.black,
          ),
        ),
      ],
    );
  }

  void _pop(BuildContext context) {
    toastCancel();
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        context.read<ReservationDetailsCubit>().onUpdateReservationData(null);
      },
    );
  }
}
