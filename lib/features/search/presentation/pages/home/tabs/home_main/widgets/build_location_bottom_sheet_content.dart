part of 'home_main_widgets_imports.dart';

class BuildLocationBottomSheetContent extends StatelessWidget {
  final HomeMainData homeMainData;

  const BuildLocationBottomSheetContent({Key? key, required this.homeMainData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    LocationEntity? loc = context.read<HomeLocationCubit>().state.model;
    var device = context.watch<DeviceCubit>().state.model;
    return WillPopScope(
      onWillPop: () => homeMainData.willPopCallback(context),
      child: Container(
        height: device.isTablet ? 255.sm : 205.sm,
        padding: EdgeInsets.symmetric(vertical: 20.sm, horizontal: 16),
        decoration: BoxDecoration(
          color: MyColors.primaryDark,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                BuildIconContainer(
                  width: device.isTablet ? 30.r : 40,
                  height: device.isTablet ? 30.r : 40,
                  bgColor: MyColors.primary,
                  child: SvgPicture.asset(Res.location,
                      color: MyColors.white, width: 14.r, height: 14.r),
                ),
                SizedBox(width: 10.w),
                MyText(
                    title: tr("select_location"),
                    color: MyColors.white,
                    size: device.isTablet ? 10.sp : 14.sp),
                Spacer(),
                Visibility(
                  visible: loc != null,
                  child: BuildIconContainer(
                    onTap: () => AutoRouter.of(context).pop(),
                    width: device.isTablet ? 30.r : 40,
                    height: device.isTablet ? 30.r : 40,
                    bgColor: MyColors.white,
                    child: Icon(
                      Icons.close,
                      size: device.isTablet ? 10.sp : 20,
                      color: MyColors.primary,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20.h),
            MyText(
              title: tr("pleas_select_location"),
              color: MyColors.offWhite,
              size: device.isTablet ? 7.sp : 11.sp,
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: BuildHomeButton(
                    height: device.isTablet ? 60.sm : 50.sm,
                    bg: MyColors.primary,
                    textColor: MyColors.white,
                    text: tr("enter_location"),
                    alignment: MainAxisAlignment.center,
                    radius: 40.r,
                    onTap: () => homeMainData.enterLocation(context),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: BuildHomeButton(
                    height: device.isTablet ? 60.sm : 50.sm,
                    bg: MyColors.black,
                    textColor: MyColors.white,
                    text: tr("near_me"),
                    alignment: MainAxisAlignment.center,
                    radius: 40.r,
                    onTap: () => homeMainData.nearLocation(context),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
