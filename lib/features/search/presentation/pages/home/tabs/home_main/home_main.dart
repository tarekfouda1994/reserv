part of 'home_main_imports.dart';

class HomeMain extends StatefulWidget {
  final HomeData homeData;

  const HomeMain({Key? key, required this.homeData}) : super(key: key);

  @override
  _HomeMainState createState() => _HomeMainState();
}

class _HomeMainState extends State<HomeMain> {
  final HomeMainData homeMainData = HomeMainData();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver;
    homeMainData.getAllAddresses(context, refresh: false);
    homeMainData.getAllAddresses(context);
    homeMainData.fitchHomeData(context, homeMainData);
    homeMainData.dynamicLinkService.retrieveDynamicLink(context);
    super.initState();
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Timer(
        Duration(milliseconds: 2000),
        () {
          homeMainData.dynamicLinkService.retrieveDynamicLink(context);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return WillPopScope(
      onWillPop: () => homeMainData.willPopResetFilter(context),
      child: BlocBuilder<ReservationDetailsCubit, ReservationDetailsState>(
          builder: (context, reserveState) {
        return Theme(
          data: AppThem.instance
              .themeData(lang: device.locale.languageCode)
              .copyWith(canvasColor: Colors.transparent),
          child: Scaffold(
            backgroundColor: MyColors.primary,
            appBar: BuildHomeAppBar(
              model: device,
              homeMainData: homeMainData,
              pagesCubit: widget.homeData.homeMainPagesCubit,
            ),
            body: Column(
              children: [
                Expanded(
                  child: BlocBuilder<SearchCubit, SearchState>(
                    builder: (_, state) {
                      return AnimatedSwitcher(
                        duration: Duration(milliseconds: 500),
                        reverseDuration: Duration(milliseconds: 500),
                        child: homeMainData.getSelectView(
                          homeMainData,
                          state.filter,
                          state.dateTime,
                          widget.homeData.homeMainPagesCubit,
                        ),
                        transitionBuilder: (child, animation) {
                          return FadeTransition(
                            child: child,
                            opacity: animation,
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
            floatingActionButton:
                BlocBuilder<GenericBloc<int>, GenericState<int>>(
              bloc: widget.homeData.homeMainPagesCubit,
              builder: (context, state) {
                return BuildFloatFilterView(
                  homeMainData: homeMainData,
                  show: true,
                  currentPage: state.data,
                  pagesCubit: widget.homeData.homeMainPagesCubit,
                );
              },
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          ),
        );
      }),
    );
  }
}
