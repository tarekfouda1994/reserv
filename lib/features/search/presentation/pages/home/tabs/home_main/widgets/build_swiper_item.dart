part of 'home_main_widgets_imports.dart';

class BuildSwiperItem extends StatelessWidget {
  final HotDealsItemModel model;
  final bool fromHome;

  const BuildSwiperItem({Key? key, required this.model, required this.fromHome})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    String image = model.avatar;
    if (image.isEmpty) {
      image = model.serviceAvatar;
    }
    return InkWell(
      onTap: () => AutoRouter.of(context).push(ReservationRootRoute(
          businessID: model.businessID, serviceId: model.serviceID)),
      child: Container(
        margin: EdgeInsetsDirectional.only(
            bottom: 16.r, end: fromHome ? 12.r : 0, top: 5.r),
        decoration: BoxDecoration(
          border: Border.all(color: MyColors.borderCard),
          color: MyColors.white,
          borderRadius: BorderRadius.circular(10.r),
          // boxShadow: <BoxShadow>[
          //   BoxShadow(
          //     color: Color.fromRGBO(0, 0, 0, .1),
          //     offset: Offset(0.0, 2.0),
          //     blurRadius: 20.0,
          //   ),
          // ],
        ),
        child: Row(
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Visibility(
                  visible: device.locale == Locale('en', 'US'),
                  child: CustomPaint(
                    foregroundPainter: ImageSwiperCurvePainter(),
                    child: CachedImage(
                      url: image.replaceAll("\\", "/"),
                      width: 118.w,
                      borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(10.r)),
                      bgColor: MyColors.defaultImgBg,
                      placeHolder: ServicePlaceholder(),
                      alignment: Alignment.bottomCenter,
                      fit: BoxFit.cover,
                      child: Visibility(
                        visible: model.businessGender != "Both",
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 15),
                          padding: EdgeInsets.symmetric(
                              vertical: 6, horizontal: 8),
                          decoration: BoxDecoration(
                              color: MyColors.white,
                              border: Border.all(
                                  color: MyColors.primary, width: 1),
                              borderRadius: BorderRadius.circular(30)),
                          child: MyText(
                            title: model.businessGender == "Male"
                                ? tr("men")
                                : tr("women"),
                            color: MyColors.black,
                            size: device.isTablet ? 6.sp : 9.sp,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding:
                    EdgeInsets.only(left: 10.0.r, right: 10.0.r, top: 30.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title: model.getPromoteAdsTitle(),
                      color: MyColors.black,
                      size: device.isTablet ? 7.sp : 11.sp,
                      fontWeight: FontWeight.w600,
                    ),
                     SizedBox(height: 10.h),
                    MyText(
                      title: model.getServiceName(),
                      color: MyColors.blackOpacity,
                      size: device.isTablet ? 5.5.sp : 9.sp,
                      overflow: TextOverflow.ellipsis,
                    ),
                    FittedBox(
                      child: Container(
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.symmetric(
                              horizontal: 15.w, vertical: 4.h),
                          decoration: BoxDecoration(
                            color: model.hasActiveSlot
                                ? MyColors.primary.withOpacity(.1)
                                : MyColors.black,
                            borderRadius: BorderRadius.circular(15.r),
                          ),
                          alignment: Alignment.center,
                          child: Visibility(
                              visible: device.locale == Locale('en', 'US'),
                              child: Visibility(
                                visible: model.hasActiveSlot,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    MyText(
                                      title: "R",
                                      color: MyColors.secondary,
                                      size: device.isTablet ? 6.5.sp : 11,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    MyText(
                                        title: "eserve now",
                                        color: MyColors.primary,
                                        size: device.isTablet ? 5.5.sp : 9.sp,
                                        fontWeight: FontWeight.bold),
                                  ],
                                ),
                                replacement: MyText(
                                    title: tr("fully_reserved"),
                                    color: MyColors.white,
                                    size: device.isTablet ? 5.5.sp : 9.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                              replacement: MyText(
                                title: model.hasActiveSlot
                                    ? tr("reserveNow")
                                    : tr("fully_reserved"),
                                color: model.hasActiveSlot
                                    ? MyColors.primary
                                    : MyColors.white,
                                size: device.isTablet ? 8.sp : 10.sp,
                                fontWeight: FontWeight.bold,
                              ))),
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: device.locale != Locale('en', 'US'),
              child: CustomPaint(
                foregroundPainter: ImageSwiperCurvePainter(),
                child: CachedImage(
                  url: image.replaceAll("\\", "/"),
                  width: 118.w,
                  borderRadius:
                      BorderRadius.horizontal(left: Radius.circular(10.r)),
                  bgColor: MyColors.defaultImgBg,
                  placeHolder: ServicePlaceholder(),
                  alignment: Alignment.bottomCenter,
                  fit: BoxFit.cover,
                  child: Visibility(
                    visible: model.businessGender != "Both",
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      padding:
                          EdgeInsets.symmetric(vertical: 6, horizontal: 8),
                      decoration: BoxDecoration(
                          color: MyColors.white,
                          border:
                              Border.all(color: MyColors.primary, width: 1),
                          borderRadius: BorderRadius.circular(30)),
                      child: MyText(
                        title: model.businessGender == "Male"
                            ? tr("men")
                            : tr("women"),
                        color: MyColors.black,
                        size: device.isTablet ? 6.sp : 9.sp,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
