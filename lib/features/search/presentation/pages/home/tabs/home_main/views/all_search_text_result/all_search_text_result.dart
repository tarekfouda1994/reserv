part of 'all_search_text_result_imports.dart';

class BuildOnSearchListView extends StatefulWidget {
  final HomeMainData homeMainData;
  final Widget onNoResultListView;

  const BuildOnSearchListView({key, required this.homeMainData, required this.onNoResultListView});

  @override
  State<BuildOnSearchListView> createState() => _BuildOnSearchListViewState();
}

class _BuildOnSearchListViewState extends State<BuildOnSearchListView> {
  late AllSearchTextResultController controller;

  @override
  void initState() {
    widget.homeMainData.getMainBanners(context, refresh: false);
    widget.homeMainData.getMainBanners(context);
    controller = AllSearchTextResultController(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchCubit, SearchState>(builder: (context, state) {
      return RefreshIndicator(
        color: MyColors.primary,
        onRefresh: () => controller._getAllServices(context),
        child: ListView(
          padding: EdgeInsets.only(top: 8),
          children: [
            if (state.fromCat)
              BuildPopularCategories(
                data: widget.homeMainData,
                callback: () {
                  controller._getAllServices(context);
                },
              ),
            BuildMainBannersView(
              homeMainData: widget.homeMainData,
              showBottomSpase: false,
            ),
            ...List.generate(controller.servicesCubits.length, (index) {
              return Visibility(
                visible: _visibleVerticalListView(context),
                replacement: BuildVerticalListView(
                  controller: controller,
                  index: index,
                  onNoResultListView: widget.onNoResultListView,
                ),
                child: BuildServicesResultItem(
                  controller: controller,
                  index: index,
                ),
              );
            }),
            SizedBox(height: 25.h),
          ],
        ),
      );
    });
  }

  bool _visibleVerticalListView(BuildContext context) {
    return context.read<SearchCubit>().state.filters.length > 1;
  }
}
