part of 'home_main_widgets_imports.dart';

class BuildServicesStickyHeader extends StatelessWidget {
  final HomeMainData homeMainData;
  final HomeApisType homeApisType;
  final String title;
  final bool auth;
  final GenericBloc<TrindingModel?> bloc;
  final HomeDepartments homeDepartments;

  const BuildServicesStickyHeader(
      {Key? key,
      required this.title,
      required this.homeMainData,
      required this.bloc,
      required this.auth,
      required this.homeDepartments,
      required this.homeApisType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return BlocBuilder<GenericBloc<TrindingModel?>,
        GenericState<TrindingModel?>>(
      bloc: bloc,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          if ((state.data?.itemsList ?? []).isEmpty) {
            return SizedBox();
          }
          return Column(
            children: [
              BuildDeptHeader(
                title: title,
                count: state.data!.totalRecords,
                bockGroundColorCount: MyColors.primary,
                counterColor: MyColors.white,
                hasMore: true,
                onTap: () => AutoRouter.of(context).push(
                  TrendingServicesRoute(
                    homeApisType: homeApisType,
                    titleAppBar: title,
                  ),
                ),
              ),
              SizedBox(height: 2),
              CustomSliderWidget(
                height: device.isTablet ? 412.sm : (!isEn ? 295.sm : 280.sm),
                items: List.generate(state.data!.itemsList.length, (index) {
                  final item = state.data!.itemsList[index];
                  return BuildServiceCard(
                    homeDepartments: homeDepartments,
                    fromHome: true,
                    serviceModel: item,
                    onUpdateWish: () {
                      homeMainData.updateWishListItem(
                        context,
                        item,
                        homeDepartments,
                      );
                    },
                  );
                }),
              ),
            ],
          );
        } else {
          return Visibility(
            visible: auth,
            child: Column(
              children: [
                BuildDeptHeader(title: title, hasMore: false),
                BuildShimmerView(
                  height: !isEn ? 250.h : 220.h,
                  margin: EdgeInsets.all(10.sp),
                ),
                SizedBox(height: 36),
              ],
            ),
          );
        }
      },
    );
  }
}
