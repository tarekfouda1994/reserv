part of 'all_services_widgets_imports.dart';

class BuildVerticalListView extends StatelessWidget {
  final AllSearchTextResultController controller;
  final int index;
  final Widget onNoResultListView;

  const BuildVerticalListView({
    Key? key,
    required this.controller,
    required this.index,
    required this.onNoResultListView,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    final search = context.read<SearchCubit>().state;
    final String filter = search.filters[index];
    return BlocBuilder<GenericBloc<TrindingModel?>,
        GenericState<TrindingModel?>>(
      bloc: controller.servicesCubits[index],
      builder: (context, state) {
        if (state is GenericUpdateState) {
          final cubit = state.data!;
          return Column(
            children: [
              if (!search.noSearchResult)
                _buildBuildDeptHeader(isEn, filter, cubit, context),
              SizedBox(height: 10),
              _handleView(context, cubit),
              SizedBox(height: 20.h),
            ],
          );
        } else {
          var isEn = device.locale == Locale('en', 'US');
          return Column(
            children: [
              BuildDeptHeader(
                title:
                    isEn ? "$filter " : "${tr("services_headerTiTle")} $filter",
                subTitle: isEn ? tr("services") : "",
                hasMore: false,
              ),
              SizedBox(height: 10),
              ...List.generate(10, (index) {
                return BuildShimmerView(
                  height: !isEn ? 250.h : 220.h,
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10).r,
                );
              }),
              SizedBox(height: 30.h),
            ],
          );
        }
      },
    );
  }

  Widget _handleView(BuildContext context, TrindingModel cubit) {
    final filter = context.read<SearchCubit>().state.filters;
    final device = context.read<DeviceCubit>().state.model;
    if (cubit.itemsList.isEmpty) {
      context.read<SearchCubit>().updateResultStatus(noSearchResult: true);
      return Column(
        children: [
          BuildEmptyServicesViewOnFilter(filter: filter.first),
          onNoResultListView,
        ],
      );
    } else {
      context.read<SearchCubit>().updateResultStatus(noSearchResult: false);
      return Column(
        children: List.generate(
          cubit.itemsList.length,
          (index) {
            final item = cubit.itemsList[index];
            return BuildTrendingServicesCard(
              serviceModel: item,
              onUpdateWish: () => controller.updateWish(
                context,
                item,
                controller.servicesCubits[index],
              ),
            );
          },
        ),
      );
    }
  }

  BuildDeptHeader _buildBuildDeptHeader(
      bool isEn, String filter, TrindingModel cubit, BuildContext context) {
    return BuildDeptHeader(
      title: filter,
      count: cubit.totalRecords,
      bockGroundColorCount: MyColors.primary,
      counterColor: MyColors.white,
      hasMore: cubit.totalRecords > 2 ? true : false,
      onTap: () => AutoRouter.of(context).push(
        TrendingServicesRoute(
          homeApisType: HomeApisType.onSearch,
          filter: filter,
          titleAppBar: filter,
        ),
      ),
    );
  }
}
