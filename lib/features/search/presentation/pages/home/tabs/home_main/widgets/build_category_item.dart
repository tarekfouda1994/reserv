part of 'home_main_widgets_imports.dart';

class BuildCategoryItem extends StatelessWidget {
  final PopularCatModel model;
  final HomeMainData homeMainData;
  final VoidCallback? callback;

  const BuildCategoryItem(
      {Key? key,
      required this.model,
      required this.homeMainData,
      this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      width: 100.w,
      child: Column(
        children: [
          Expanded(
            child: InkWell(
              onTap: () => _onSelectCategory(context),
              child: Stack(
                alignment: AlignmentDirectional.topEnd,
                children: [
                  CachedImage(
                    url: model.avatar.replaceAll("\\", "/"),
                    fit: BoxFit.cover,
                    borderRadius: BorderRadius.circular(10.r),
                    bgColor: MyColors.defaultImgBg,
                    placeHolder: CategoryPlaceholder(),
                  ),
                  if (model.isSelected == true)
                    Container(
                      width: 100.w,
                      decoration: BoxDecoration(
                        border: Border.all(width: 2, color: MyColors.primary),
                        color: MyColors.primary.withOpacity(.2),
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                    ),
                  Visibility(
                    visible: model.isSelected == true,
                    child: Container(
                      margin: EdgeInsets.all(4),
                      alignment: Alignment.center,
                      width: 18,
                      height: 18,
                      decoration: BoxDecoration(
                        color: MyColors.primary,
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.check,
                        color: MyColors.white,
                        size: 15,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: _isEn(device) ? 0 : 3).r,
            child: MyText(
              title: model.getPopularCatName(),
              color: MyColors.black,
              overflow: TextOverflow.ellipsis,
              size: device.isTablet ? 6.sp : 9.sp,
            ),
          ),
        ],
      ),
    );
  }

  bool _isEn(DeviceModel device) => device.locale.languageCode == "en";

  Future<void> _onSelectCategory(BuildContext context) async {
    final dateTime = context.read<SearchCubit>().state.dateTime;
    if (model.isSelected == true) {
      model.isSelected = false;
      context.read<SearchCubit>().onUpdateFilterType(fromCat: false);
      homeMainData.resetFilter(context, dateTime);
    } else {
      _onMakeItemSelected(context);
      if (callback != null) callback!();
    }
    homeMainData.fitchHomeData(context, homeMainData);
    homeMainData.popularCubit.onUpdateData(
      homeMainData.popularCubit.state.data,
    );
  }

  void _onMakeItemSelected(BuildContext context) {
    String name = model.getPopularCatName();
    homeMainData.allFinishLoading = 0;
    homeMainData.popularCubit.state.data
        .map((e) => e.isSelected = false)
        .toList();
    model.isSelected = true;
    homeMainData.saveStringListValue(name);
    context.read<SearchCubit>().onUpdateFiltersList(filters: [name]);
    context.read<SearchCubit>().onUpdateFilterType(fromCat: true);
    Future.delayed(
      Duration(milliseconds: 100),
      () => homeMainData.scrollToSelectedCat(),
    );
  }
}
