part of 'home_main_widgets_imports.dart';

class BuildHomeButton extends StatelessWidget {
  final Color? bg;
  final Color? textColor;
  final IconData? iconData;
  final Widget? child;
  final EdgeInsetsGeometry? margin;
  final double? radius;
  final double? height;
  final MainAxisAlignment? alignment;
  final String? text;
  final BoxConstraints? constraints;
  final Function() onTap;

  const BuildHomeButton({
    Key? key,
    this.bg,
    this.textColor,
    this.iconData,
    this.radius,
    this.alignment,
    this.text,
    required this.onTap,
    this.child,
    this.constraints,
    this.margin, this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap: onTap,
      child: Container(
        constraints: constraints,
        height: height??40.h,
        margin: margin,
        width:constraints==null? (MediaQuery.of(context).size.width * (device.isTablet? .47:.44)):null,
        padding: EdgeInsets.symmetric(horizontal: 8.w),
        decoration: BoxDecoration(color: bg, borderRadius: BorderRadius.circular(radius ?? 40.r)),
        child: child ??
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: alignment ?? MainAxisAlignment.spaceAround,
              children: [
                if (iconData != null)
                  Icon(
                    iconData,
                    size: device.isTablet ? 12.sp : 20.sp,
                    color: textColor,
                  ),
                Expanded(
                  child: MyText(
                    title: text ?? "",
                    color: textColor,
                    size: device.isTablet ? 7.sp : 10.sp,
                    overflow: TextOverflow.ellipsis,
                    alien: TextAlign.center,
                  ),
                ),
              ],
            ),
      ),
    );
  }
}
