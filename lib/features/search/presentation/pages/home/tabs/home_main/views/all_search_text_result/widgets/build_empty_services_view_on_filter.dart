part of 'all_services_widgets_imports.dart';

class BuildEmptyServicesViewOnFilter extends StatelessWidget {
  final String filter;

  const BuildEmptyServicesViewOnFilter({key, required this.filter});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 35),
      padding: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            MyColors.reserveDateBg,
            MyColors.reserveDateBg,
            MyColors.white,
          ],
        ),
        borderRadius: BorderRadius.circular(11).r,
      ),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              Res.emptyServicesImage,
            ),
            SizedBox(height: 6),
            _buildServiceTitle(context),
            SizedBox(height: 4),
            MyText(
              alien: TextAlign.center,
              title: tr("emptyServicesHint"),
              color: MyColors.blackOpacity,
              size: 10,
            ),
          ]),
    );
  }

  Widget _buildServiceTitle(BuildContext context) {
    final locale = context.read<DeviceCubit>().state.model.locale;
    final isAr = locale.languageCode == "ar";
    var words = (isAr?"${tr("unfortunately")} '$filter'": "${tr("unfortunately")} '$filter' found").split(" ");
    var timeSpans = words.map((e) {
      var currentSearchKeywords =
          filter.split(" ").map((e) => e.toLowerCase()).toList() ?? [];
      if (currentSearchKeywords.any((word) => e.toLowerCase().contains(word))) {
        return TextSpan(
          text: e,
          style: TextStyle(
            color: MyColors.black,
            fontFamily: CustomFonts.primarySemiBoldFont,
            fontSize: isAr?10.5:11,
            fontWeight: FontWeight.bold,
          ),
          children: [
            TextSpan(
              text: " ",
              style: TextStyle(
                color: MyColors.black,
                fontFamily: CustomFonts.primarySemiBoldFont,
                fontSize: isAr?10.5:11,
              ),
            ),
          ],
        );
      }
      return TextSpan(
        text: e,
        style: TextStyle(
          color: MyColors.black,
          fontFamily: CustomFonts.primarySemiBoldFont,
          fontWeight: FontWeight.w400,
          fontSize: isAr?10.5:11,
        ),
        children: [
          TextSpan(
            text: " ",
            style: TextStyle(
              fontFamily: CustomFonts.primarySemiBoldFont,
              color: MyColors.black,
              fontSize: isAr?10.5:11,
            ),
          ),
        ],
      );
    }).toList();
    return RichText(
      textScaleFactor: 1.2,
      textAlign: TextAlign.center,
      textWidthBasis: TextWidthBasis.longestLine,
      text: TextSpan(
        style: TextStyle(
          fontFamily: CustomFonts.primarySemiBoldFont,
        ),
        children: timeSpans,
      ),
    );
  }
}
