part of 'home_main_widgets_imports.dart';

class BuildTopBusinessesStickyHeader extends StatelessWidget {
  final HomeMainData homeMainData;
  final String title;

  const BuildTopBusinessesStickyHeader(
      {Key? key, required this.homeMainData, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var filter = context.read<SearchCubit>().state.filter;
    var isEn = device.locale == Locale('en', 'US');

    return BlocBuilder<GenericBloc<TopBusinessesModel?>,
        GenericState<TopBusinessesModel?>>(
      bloc: homeMainData.topBusinessesCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return filter.isEmpty
              ? Visibility(
                  visible: state.data!.itemsList.isNotEmpty,
                  child: Column(
                    children: [
                      BuildDeptHeader(
                        title: title,
                        bockGroundColorCount: MyColors.primary,
                        counterColor: MyColors.white,
                        count: state.data!.totalRecords,
                        onTap: () =>
                            AutoRouter.of(context).push(TopPartnersRoute()),
                        hasMore: state.data!.totalRecords > 2 ? true : false,
                      ),
                      SizedBox(height: 8),
                      CustomSliderWidget(
                        height:
                            device.isTablet ? 420.sm : (isEn ? 280.sm : 290.sm),
                        items: List.generate(state.data!.itemsList.length,
                            (index) {
                          return BuildTopBusinessCard(
                            shareLink: () => homeMainData.shareSinglePage(
                                context,
                                state.data!.itemsList[index].id,
                                state.data!.itemsList[index].getBusinessName()),
                            updateWish: () =>
                                homeMainData.applyWishOperationsInTopBusiness(
                              context,
                              state.data!.itemsList[index],
                            ),
                            businessModel: state.data!.itemsList[index],
                          );
                        }),
                      ),
                    ],
                  ),
                )
              : SizedBox();
        } else {
          return Column(
            children: [
              BuildDeptHeader(title: title, hasMore: false),
              BuildShimmerView(
                height: device.isTablet ? 180.h : 150.h,
                margin: EdgeInsets.all(10.sp),
              ),
            ],
          );
        }
      },
    );
  }
}
