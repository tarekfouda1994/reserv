part of 'home_main_widgets_imports.dart';

class BuildDateTimeAppBar extends StatelessWidget {
  final TrendingServicesItemModel? trendingServicesItemModel;
  final String filter;
  final HomeMainData homeMainData;
  final SelectedDateTimeEntity dateTime;

  const BuildDateTimeAppBar(
      {Key? key,
      required this.trendingServicesItemModel,
      required this.filter,
      required this.dateTime,
      required this.homeMainData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return BuildHomeButton(
      constraints: BoxConstraints(
        minWidth: MediaQuery.of(context).size.width * .44,
      ),
      bg: MyColors.primaryDark,
      iconData: Icons.access_time,
      onTap: () => homeMainData.goDateTimeFilterPage(context),
      child: Row(
        children: [
          /// Date Time Icon
          Icon(Icons.access_time_outlined,
              color: MyColors.white, size: isTablet ? 14.sp : 19.sp),

          SizedBox(width: 6),

          /// Dates Text
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                title: dateTime.dateList.length != 0
                    ? buildFormat(
                        context,
                        dateTime.dateList.firstOrNull ?? DateTime(0),
                      )
                    : '',
                color: MyColors.white,
                size: isTablet ? 6.sp : 9.sp,
              ),
              Visibility(
                visible: dateTime.dateList.length > 1,
                child: MyText(
                  title: dateTime.dateList.length > 1
                      ? buildFormat(context, dateTime.dateList.last)
                      : '',
                  color: MyColors.white,
                  size: isTablet ? 6.sp : 9.sp,
                ),
              ),
            ],
          ),

          /// Selected Dates Count
          Visibility(
            visible: dateTime.dateList.length > 2,
            child: Container(
              height: 22.h,
              width: 22.w,
              padding: EdgeInsets.all(3),
              margin: EdgeInsets.symmetric(horizontal: 6),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: MyColors.white,
                border: Border.all(
                  color: MyColors.primary,
                ),
              ),
              alignment: Alignment.center,
              child: FittedBox(
                child: MyText(
                  title: "${dateTime.dateList.length - 2}",
                  color: MyColors.primaryDark,
                  size: isTablet ? 6.sp : 9.sp,
                ),
              ),
            ),
          ),

          /// container spacing
          Offstage(
            offstage:
                !(dateTime.dateList.isNotEmpty && dateTime.timeList.isNotEmpty),
            child: SizedBox(width: 20),
          ),

          /// Times Text
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  title: _localRange(
                      context, dateTime.timeList.firstOrNull?.title ?? ""),
                  color: MyColors.white,
                  size: isTablet ? 6.sp : 9.sp),
              Visibility(
                visible: dateTime.timeList.length > 1,
                child: MyText(
                    title: dateTime.timeList.length > 1
                        ? _localRange(context, dateTime.timeList.last.title)
                        : '',
                    color: MyColors.white,
                    size: isTablet ? 6.sp : 9.sp),
              ),
            ],
          ),

          /// Selected Times Count
          Visibility(
            visible: dateTime.timeList.length > 2,
            child: Container(
              height: 22.h,
              width: 22.w,
              padding: EdgeInsets.all(3),
              margin: EdgeInsets.symmetric(horizontal: 6),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: MyColors.white,
                border: Border.all(
                  color: MyColors.primary,
                ),
              ),
              alignment: Alignment.center,
              child: FittedBox(
                child: MyText(
                  title: "${dateTime.timeList.length - 2}",
                  color: MyColors.primaryDark,
                  size: isTablet ? 6.sp : 9.sp,
                ),
              ),
            ),
          ),

          /// Clear Filter Button
          InkWell(
            onTap: () {
              context.read<SearchCubit>().onUpdate(
                  filter: filter,
                  dateTime: SelectedDateTimeEntity(timeList: [], dateList: []));
              homeMainData.ficheServices(context);
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              padding: const EdgeInsets.all(2.5),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: MyColors.primary),
                  color: MyColors.primary),
              child: Icon(
                Icons.close,
                color: MyColors.white,
                size: isTablet ? 10.sp : 12.sp,
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _localRange(BuildContext context, String value) {
    if (value.isNotEmpty) {
      return getIt<Utilities>().localizedRangeNumber(context, value);
    } else {
      return "";
    }
  }

  String buildFormat(BuildContext context, DateTime date) {
    final locale = context.read<DeviceCubit>().state.model.locale;
    return DateFormat("dd MMM yyyy", locale.languageCode).format(
      date,
    );
  }
}
