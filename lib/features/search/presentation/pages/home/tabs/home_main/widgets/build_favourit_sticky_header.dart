part of 'home_main_widgets_imports.dart';

class BuildFavoriteStickyHeaders extends StatelessWidget {
  final HomeMainData homeMainData;
  final String title;

  const BuildFavoriteStickyHeaders(
      {Key? key, required this.homeMainData, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return BlocBuilder<GenericBloc<List<ServicesFavouriteModel>>,
        GenericState<List<ServicesFavouriteModel>>>(
      bloc: homeMainData.favouriteCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          if (state.data.isNotEmpty)
            return Column(
              children: [
                BuildDeptHeader(
                    onTap: () => AutoRouter.of(context)
                        .push(FavouriteRoute(fromHome: false)),
                    title: title,
                    bockGroundColorCount: MyColors.primary,
                    counterColor: MyColors.white,
                    count: state.data.length,
                    hasMore: state.data.length > 2 ? true : false),
                CustomSliderWidget(
                  height: device.isTablet ? 415.sm : (!isEn ? 295.sm : 280.sm),
                  items: List.generate(state.data.length, (index) {
                    return BuildServicesCardFavourite(
                      shareLink: () => homeMainData.shareSinglePage(
                          context,
                          state.data[index].businessID,
                          state.data[index].getServiceName()),
                      updateServiceWish: () => homeMainData.updateWishListItem(
                          context, state.data[index], HomeDepartments.favorite),
                      fromHome: true,
                      sevicesModel: state.data[index],
                    );
                  }),
                ),
              ],
            );
          else {
            return SizedBox();
          }
        } else {
          return device.auth
              ? Column(
                  children: [
                    BuildDeptHeader(title: title, hasMore: false),
                    BuildShimmerView(
                      height: device.isTablet ? 180.h : 150.h,
                      margin: EdgeInsets.all(10.sp),
                    ),
                    SizedBox(height: 30.h),
                  ],
                )
              : SizedBox();
        }
      },
    );
  }
}
