part of 'home_main_imports.dart';

class HomeMainData {
  Completer<GoogleMapController> mapController = Completer();
  final ScrollController scrollController = ScrollController();
  final ScrollController catScrollController = ScrollController();
  final GenericBloc<bool> scrollCubit = GenericBloc(true);
  final GenericBloc<bool> emptyListCubit = GenericBloc(false);
  final GenericBloc<int> filterCountCubit = GenericBloc(0);
  final GenericBloc<HotDealsModel?> hotDealsCubit = GenericBloc(null);
  final GenericBloc<HomeBannersModel?> mainBannersCubit = GenericBloc(null);
  final GenericBloc<TrindingModel?> recommendedCubit = GenericBloc(null);
  final GenericBloc<TrindingModel?> recentlyCubit = GenericBloc(null);
  final GenericBloc<TrindingModel?> trendingServiceCubit = GenericBloc(null);
  final GenericBloc<LocationData?> locBusinessesBloc = GenericBloc(null);
  final GenericBloc<TrindingModel?> servicesOffersCubit = GenericBloc(null);
  final GenericBloc<TrindingModel?> popularServicesCubit = GenericBloc(null);
  final GenericBloc<TrindingModel?> onSearchServicesCubit = GenericBloc(null);
  final GenericBloc<TopBusinessesModel?> topBusinessesCubit = GenericBloc(null);
  final GenericBloc<List<Marker>> markersCubit = GenericBloc([]);
  final GenericBloc<List<PopularCatModel>> popularCubit = GenericBloc([]);
  final GenericBloc<List<ServicesFavouriteModel>> favouriteCubit =
      GenericBloc([]);
  GenericBloc<TopBusinessItemModel?> placeCubit = GenericBloc(null);
  final DynamicLinkService dynamicLinkService = DynamicLinkService();
  final GenericBloc<List<AddressModel>> addressCubit = GenericBloc([]);
  List<Widget> _pages = [];
  int allFinishLoading = 0;
  FilterEntity? filterEntity;
  late GoogleMapController controllerMap;
  double lat = 31.0414217;
  double? lng = 31.3653301;
  double? zoom;

  initScrollListener() {
    retrieveStringListValue();
    scrollController.addListener(scrollListener);
  }

  void scrollListener() {
    var userScroll = scrollController.position.userScrollDirection;
    if (userScroll != ScrollDirection.forward && !_checkBottomList()) {
      if (scrollCubit.state.data) {
        scrollCubit.onUpdateData(false);
      }
    } else {
      scrollCubit.onUpdateData(true);
    }
  }

  void scrollToSelectedCat() {
    final cubit = popularCubit.state.data;
    final int index = cubit.indexOf(
      cubit.singleWhere((element) => element.isSelected == true),
    );
    final double value =
        (catScrollController.position.maxScrollExtent / cubit.length - 3) *
            index.toDouble();
    catScrollController.position.jumpTo(value);
  }

  bool _checkBottomList() {
    return (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent);
  }

  getPages(HomeMainData mainData, int index, String filter,
      SelectedDateTimeEntity dateTime) {
    _pages = [
      BuildListView(homeMainData: mainData, filter: filter, dateTime: dateTime),
      BuildMapView(homeMainData: mainData),
    ];
    return _pages[index];
  }

  getSelectView(
    HomeMainData mainData,
    String filter,
    SelectedDateTimeEntity dateTime,
    GenericBloc<int> pagesCubit,
  ) {
    var views = [
      BuildHomeMainBody(
        pagesCubit: pagesCubit,
        mainData: mainData,
        filter: "",
        dateTime: SelectedDateTimeEntity(
          timeList: [],
          dateList: [],
        ),
      ),
      BuildHomeMainBody(
        mainData: mainData,
        filter: filter,
        dateTime: dateTime,
        pagesCubit: pagesCubit,
      ),
    ];
    // return views;
    return views[
        filter.isEmpty && dateTime.timeList == [] && dateTime.dateList == []
            ? 0
            : 1];
  }

  Future<void> getAllAddresses(BuildContext context,
      {bool refresh = true}) async {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      var data = await GetAllAddresses()(refresh);
      for (AddressModel e in data) {
        e.isDefault = false;
      }
      addressCubit.onUpdateData(data);
    }
  }

  void showLocationBottomSheet(BuildContext context, HomeMainData data) {
    LocationEntity? loc = context.read<HomeLocationCubit>().state.model;
    showModalBottomSheet(
      isDismissible: loc != null,
      enableDrag: loc != null,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      builder: (context) {
        return BuildLocationBottomSheetContent(
          homeMainData: data,
        );
      },
    );
  }

  bool checkFiltration(String filter, SelectedDateTimeEntity dateTime) {
    return filter.isEmpty &&
        dateTime.dateList.isEmpty &&
        dateTime.timeList.isEmpty;
  }

  Future<void> enterLocation(BuildContext context) async {
    var laLong = await getIt<Utilities>().getCurrentLocation(context);
    if (laLong != null) {
      LocationEntity result =
          await Navigator.push(context, MaterialPageRoute(builder: (cxt) {
        return LocationAddress(
            location: LocationEntity(
                lat: laLong.latitude ?? 0, lng: laLong.longitude ?? 0));
      }));
      if (result.lat != 0 && result.lng != 0) {
        saveLocNearMe(LatLng(result.lat, result.lng), result.title);
        ficheServices(context);
      }
    }
  }

  Future<void> nearLocation(BuildContext context) async {
    var laLong = await getIt<Utilities>().getCurrentLocation(context);
    if (laLong != null) {
      context.read<HomeLocationCubit>().onLocationUpdated(LocationEntity(
          lng: laLong.longitude!, lat: laLong.latitude!, title: "null"));
      saveLocNearMe(
          LatLng(laLong.latitude ?? 0, laLong.longitude ?? 0), "null");
      AutoRouter.of(context).pop();
      ficheServices(context);
    }
  }

  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(json.encode(MapStyle.server));
    if (!mapController.isCompleted) {
      controllerMap = controller;
      mapController.complete(controller);
    }
  }

  setMarkerWidgets(BuildContext context, List<TopBusinessItemModel> services) {
    MarkerGenerator(markerWidgets(), (bitmaps) {
      var markers = mapBitmapsToMarkers(bitmaps, services);
      markersCubit.onUpdateData(markers);
    }).generate(context);
    markersCubit.onUpdateData(markersCubit.state.data);
  }

  List<Marker> mapBitmapsToMarkers(
      List<Uint8List> bitmaps, List<TopBusinessItemModel> services) {
    List<Marker> markersList = [];
    bitmaps.asMap().forEach((i, bmp) {
      final model = services[i];
      markersList.add(
        Marker(
          onTap: () => onMarkerClick(model),
          markerId: MarkerId(model.distance.toString()),
          position: LatLng(model.latitude, model.longitude),
          icon: BitmapDescriptor.fromBytes(bmp),
        ),
      );
    });
    return markersList;
  }

  Widget _getMarkerWidget(TopBusinessItemModel model) {
    return BuildMapMarker(model: model);
  }

  List<Widget> markerWidgets() {
    return topBusinessesCubit.state.data?.itemsList
            .map((c) => _getMarkerWidget(c))
            .toList() ??
        [];
  }

  void onMarkerClick(TopBusinessItemModel model) {
    placeCubit.onUpdateData(model);
  }

  Future<void> getFavourite(BuildContext context, {bool refresh = true}) async {
    var data = await GetFavouriteServices()(
        _businessParams(context, refresh: refresh));
    favouriteCubit.onUpdateData(data.reversed.toList());
    if (data.isNotEmpty) {
      emptyListCubit.onUpdateData(true);
    }
    _checkLoading(context);
  }

  void _checkLoading(BuildContext context) {
    final cxt = getIt<GlobalContext>().context();
    var auth = cxt.read<DeviceCubit>().state.model.auth;
    var filter = cxt.read<SearchCubit>().state.filter;
    var cubit = emptyListCubit.state.data;
    allFinishLoading = allFinishLoading + 1;
    bool unAuth = allFinishLoading == 6 && filter.isNotEmpty && !cubit && !auth;
    bool isAuth = allFinishLoading == 12 && filter.isNotEmpty && !cubit && auth;
    if (isAuth || unAuth) {
      emptyListCubit.onUpdateData(false);
    }
  }

  Future<void> getHotDeals(BuildContext context, {bool refresh = true}) async {
    var filter = context.read<SearchCubit>().state.filter;
    var latLong = context.read<HomeLocationCubit>().state.model;
    HotDealsPrams params = _hotDealsPrams(filter, latLong, refresh);
    var data = await GetHotDeals()(params);
    hotDealsCubit.onUpdateData(data);
  }

  HotDealsPrams _hotDealsPrams(
      String filter, LocationEntity? latLong, bool refresh) {
    return HotDealsPrams(
      pageNumber: 1,
      pageSize: 10,
      filter: filter,
      longitude: latLong?.lng ?? 0,
      refresh: refresh,
      latitude: latLong?.lat ?? 0,
    );
  }

  Future<void> getMainBanners(BuildContext context,
      {bool refresh = true}) async {
    var filters = context.read<SearchCubit>().state.filters;
    // var filter = _filterBanner(filters);
    var latLong = context.read<HomeLocationCubit>().state.model;
    BannersParams params = _bannersParams("", latLong);
    params.refresh = refresh;
    var data = await GetMainBanners()(params);
    mainBannersCubit.onUpdateData(data);
    if (data.itemsList.isNotEmpty) {
      emptyListCubit.onUpdateData(true);
    }
  }

  // String _filterBanner(List<String> filters) => filters.isNotEmpty
  //     ? filters.length == 1
  //         ? filters.first
  //         : filters.join(",")
  //     : "";

  BannersParams _bannersParams(String filter, LocationEntity? latLong) {
    return BannersParams(
      pageNumber: 1,
      pageSize: 10,
      refresh: true,
      loadImages: true,
      filter: filter,
      latitude: latLong?.lat ?? 0,
      longitude: latLong?.lng ?? 0,
    );
  }

  TrendingPrams _businessAndServiceParams(BuildContext context,
      {HomeApisType? homeApisType, int? pageSize, required bool refresh}) {
    var dateTime = context.read<SearchCubit>().state.dateTime;
    dateTime.dateList.sort((a, b) => a.compareTo(b));
    dateTime.timeList.sort((a, b) => a.title.compareTo(b.title));
    var filterState = context.read<SearchCubit>().state;
    var lotLong = context.read<HomeLocationCubit>().state.model;
    final params = TrendingPrams(
        homeApisType: homeApisType,
        pageNumber: 1,
        pageSize: pageSize ?? 10,
        filter: filterState.filter,
        refresh: refresh,
        fromScheduleDate:
            getIt<Utilities>().formatDate(dateTime.dateList.firstOrNull),
        fromTime: dateTime.timeList.firstOrNull?.title,
        toScheduleDate:
            getIt<Utilities>().formatDate(dateTime.dateList.lastOrNull),
        toTime: dateTime.timeList.lastOrNull?.title,
        latitude: lotLong?.lat ?? 0,
        longitude: lotLong?.lng ?? 0,
        rating: filterEntity?.rating,
        sortColumn: filterEntity?.sortColumn,
        genderID: filterEntity?.genderID != null
            ? (filterEntity?.genderID == 1 ? "1" : "2")
            : null,
        minServiceAmount: filterEntity?.maxServiceAmount != null &&
                filterEntity?.minServiceAmount == null
            ? 0
            : filterEntity?.minServiceAmount,
        maxServiceAmount: filterEntity?.maxServiceAmount == null &&
                filterEntity?.minServiceAmount != null
            ? 700
            : filterEntity?.maxServiceAmount,
        minDuration: filterEntity?.maxDuration?.toInt() != null &&
                filterEntity?.minDuration?.toInt() == null
            ? 30
            : filterEntity?.minDuration?.toInt(),
        maxDuration: filterEntity?.maxDuration?.toInt() == null &&
                filterEntity?.minDuration?.toInt() != null
            ? 480
            : filterEntity?.maxDuration?.toInt(),
        sortDirection: filterEntity?.sortDirection);
    return params;
  }

  Future<void> getPopular(BuildContext context, {bool refresh = true}) async {
    final filter = context.read<SearchCubit>().state;
    final params = _businessParams(context, pageSize: 150, refresh: refresh);
    var data = await GetPopularCat()(params);
    popularCubit.onUpdateData(data);
    _setInitialSelected(filter);
  }

  void _setInitialSelected(SearchState filter) {
    final data = popularCubit.state.data;
    if (filter.fromCat) {
      final item = data.singleWhere(
          (element) => element.getPopularCatName() == filter.filters.first);
      item.isSelected = true;
      popularCubit.onUpdateData(popularCubit.state.data);
      scrollToSelectedCat();
    }
  }

  Future<void> getTopBusiness(BuildContext context,
      {int pageSize = 10, bool refresh = true}) async {
    TopBusinessParams params = _businessParams(context, refresh: refresh);
    var data = await GetTopBusinesses()(params);
    if (data != null) {
      topBusinessesCubit.onUpdateData(data);
      setMarkerWidgets(context, data.itemsList);
    }
  }

  Future<void> getRecommendedServices(BuildContext context,
      {bool refresh = true}) async {
    TrindingModel? data = await _getHomeServices(
        context, HomeApisType.recommended,
        refresh: refresh);
    if (data != null) {
      recommendedCubit.onUpdateData(data);
      if (data.itemsList.isNotEmpty) {
        emptyListCubit.onUpdateData(true);
      }
    }
    _checkLoading(context);
  }

  Future<void> getRecentlyServices(BuildContext context,
      {bool refresh = true}) async {
    TrindingModel? data = await _getHomeServices(context, HomeApisType.recently,
        refresh: refresh);
    if (data != null) {
      recentlyCubit.onUpdateData(data);
      if (data.itemsList.isNotEmpty) {
        emptyListCubit.onUpdateData(true);
      }
    }
    _checkLoading(context);
  }

  Future<void> getTrendingServices(BuildContext context,
      {bool refresh = true}) async {
    TrindingModel? data = await _getHomeServices(context, HomeApisType.trending,
        refresh: refresh);
    if (data != null) {
      trendingServiceCubit.onUpdateData(data);
      if (data.itemsList.isNotEmpty) {
        emptyListCubit.onUpdateData(true);
      }
    }
    _checkLoading(context);
  }

  Future<void> getServicesOffers(BuildContext context,
      {bool refresh = true}) async {
    TrindingModel? data =
        await _getHomeServices(context, HomeApisType.offers, refresh: refresh);
    if (data != null) {
      servicesOffersCubit.onUpdateData(data);
      if (data.itemsList.isNotEmpty) {
        emptyListCubit.onUpdateData(true);
      }
    }
    _checkLoading(context);
  }

  Future<void> getPopularServices(BuildContext context,
      {bool refresh = true}) async {
    TrindingModel? data =
        await _getHomeServices(context, HomeApisType.popular, refresh: refresh);
    if (data != null) {
      popularServicesCubit.onUpdateData(data);
      if (data.itemsList.isNotEmpty) {
        emptyListCubit.onUpdateData(true);
      }
      _checkLoading(context);
    }
  }

  Future<void> getSearchServices(BuildContext context) async {
    await _getHomeServices(context, HomeApisType.onSearch, refresh: true)
        .then((data) {
      if (data != null) {
        onSearchServicesCubit.onUpdateData(data);
        if (data.itemsList.isNotEmpty) {
          emptyListCubit.onUpdateData(true);
        }
        _checkLoading(context);
      }
    });
  }

  Future<TrindingModel?> _getHomeServices(
      BuildContext context, HomeApisType homeApisType,
      {required bool refresh}) async {
    TrendingPrams params = _businessAndServiceParams(context,
        homeApisType: homeApisType, refresh: refresh);
    var data = await GetHomeServices()(params);
    return data;
  }

  TopBusinessParams _businessParams(BuildContext context,
      {HomeApisType? homeApisType, int? pageSize, required bool refresh}) {
    var filter = context.read<SearchCubit>().state.filter;
    var lotLong = context.read<HomeLocationCubit>().state.model;
    var params = TopBusinessParams(
      loadImages: true,
      longitude: lotLong?.lng,
      latitude: lotLong?.lat,
      pageSize: pageSize ?? 10,
      pageNumber: 1,
      filter: filter,
      refresh: refresh,
      homeApisType: homeApisType,
    );
    return params;
  }

  returnFilterData(BuildContext context, FilterEntity? result) {
    if (result != null) {
      ficheServices(context);
      filterCountCubit.onUpdateData(filterEntity?.filterCount!.length ?? 0);
      filterEntity = result;
    }
  }

  bool checkUserAuth(BuildContext context, bool isSalon) {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      return true;
    } else {
      CustomToast.customAuthDialog(isSalon: isSalon);
    }
    return false;
  }

  goToFilter(BuildContext context) async {
    FilterEntity result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (cxt) => Filter(filterEntity: filterEntity)));
    if (result != FilterEntity()) {
      filterEntity = result;
      returnFilterData(context, result);
    }
  }

  Future<void> updateWishListItem(
      BuildContext context, dynamic model, HomeDepartments departments) async {
    if (checkUserAuth(context, false)) {
      switch (departments) {
        case HomeDepartments.trending:
          await _applyWishOperationsInTrendingServices(model);
          break;
        case HomeDepartments.favorite:
          await _applyWishOperationsInFavorite(model);
          break;
        case HomeDepartments.recommended:
          await _applyWishOperationsInRecommendedServices(model);
          break;
        case HomeDepartments.recently:
          await _applyWishOperationsInRecentlyServices(model);
          break;
        case HomeDepartments.offers:
          await _applyWishOperationsInServicesOffers(model);
          break;
        case HomeDepartments.popular:
          await _applyWishOperationsInPopularServices(model);
          break;
        case HomeDepartments.onSearch:
          break;
      }
      getFavourite(context);
    }
  }

  Future<void> _applyWishOperationsInTrendingServices(
      TrendingServicesItemModel model) async {
    int index = trendingServiceCubit.state.data!.itemsList.indexOf(model);
    bool result = false;
    result =
        await _updateWishServices(index, result, model, trendingServiceCubit);
    if (result) {
      _updateTrendingCubit(index);
    }
  }

  Future<void> _applyWishOperationsInServicesOffers(
      TrendingServicesItemModel model) async {
    int index = servicesOffersCubit.state.data!.itemsList.indexOf(model);
    bool result = false;
    result =
        await _updateWishServices(index, result, model, servicesOffersCubit);
    if (result) {
      _updateServiceOffersCubit(index);
    }
  }

  Future<void> _applyWishOperationsInPopularServices(
      TrendingServicesItemModel model) async {
    int index = popularServicesCubit.state.data!.itemsList.indexOf(model);
    bool result = false;
    result =
        await _updateWishServices(index, result, model, popularServicesCubit);
    if (result) {
      _updatePopularServicesCubit(index);
    }
  }

  Future<void> _applyWishOperationsInRecommendedServices(
      TrendingServicesItemModel model) async {
    int index = recommendedCubit.state.data!.itemsList.indexOf(model);
    bool result = false;
    result = await _updateWishServices(index, result, model, recommendedCubit);
    if (result) {
      _updateRecommendedCubit(index);
    }
  }

  Future<void> _applyWishOperationsInRecentlyServices(
      TrendingServicesItemModel model) async {
    int index = recentlyCubit.state.data!.itemsList.indexOf(model);
    bool result = false;
    result = await _updateWishServices(index, result, model, recentlyCubit);
    if (result) {
      _updateRecentlyCubit(index);
    }
  }

  Future<void> _applyWishOperationsInFavorite(
      ServicesFavouriteModel model) async {
    bool result = false;
    result =
        await _removeFromWishList(model.serviceID, model.eBusinessServiceID);
    _updateFavoriteCubit(result, model.serviceID, model.eBusinessServiceID);
  }

  void _updateFavoriteCubit(bool result, String serviceId, String businessId) {
    if (result) {
      favouriteCubit.state.data.removeWhere((element) =>
          element.serviceID == serviceId &&
          element.eBusinessServiceID == businessId);
      favouriteCubit.onUpdateData(favouriteCubit.state.data);
    }
  }

  void _updateTrendingCubit(int index) {
    trendingServiceCubit.state.data!.itemsList[index].hasWishItem =
        !trendingServiceCubit.state.data!.itemsList[index].hasWishItem!;
    trendingServiceCubit.onUpdateData(trendingServiceCubit.state.data);
  }

  void _updateServiceOffersCubit(int index) {
    servicesOffersCubit.state.data!.itemsList[index].hasWishItem =
        !servicesOffersCubit.state.data!.itemsList[index].hasWishItem!;
    servicesOffersCubit.onUpdateData(servicesOffersCubit.state.data);
  }

  void _updatePopularServicesCubit(int index) {
    popularServicesCubit.state.data!.itemsList[index].hasWishItem =
        !popularServicesCubit.state.data!.itemsList[index].hasWishItem!;
    popularServicesCubit.onUpdateData(popularServicesCubit.state.data);
  }

  void _updateRecommendedCubit(int index) {
    recommendedCubit.state.data!.itemsList[index].hasWishItem =
        !recommendedCubit.state.data!.itemsList[index].hasWishItem!;
    recommendedCubit.onUpdateData(recommendedCubit.state.data);
  }

  void _updateRecentlyCubit(int index) {
    recentlyCubit.state.data!.itemsList[index].hasWishItem =
        !recentlyCubit.state.data!.itemsList[index].hasWishItem!;
    recentlyCubit.onUpdateData(recentlyCubit.state.data);
  }

  Future<bool> _updateWishServices(int index, bool result,
      TrendingServicesItemModel model, GenericBloc<TrindingModel?> bloc) async {
    if (bloc.state.data!.itemsList[index].hasWishItem!) {
      result =
          await _removeFromWishList(model.serviceID, model.eBusinessServiceID);
    } else {
      result = await _addToWishList(model.serviceID, model.eBusinessServiceID);
    }
    return result;
  }

  Future<bool> _addToWishList(String serviceId, String businessId) async {
    getIt<LoadingHelper>().showLoadingDialog();
    var result = await UpdateWishService()(
        WishParams(serviceID: serviceId, businessServiceID: businessId));
    getIt<LoadingHelper>().dismissDialog();
    return result;
  }

  Future<bool> _removeFromWishList(String serviceId, String businessId) async {
    getIt<LoadingHelper>().showLoadingDialog();
    var result = await RemoveWishService()(
        WishParams(serviceID: serviceId, businessServiceID: businessId));
    getIt<LoadingHelper>().dismissDialog();
    return result;
  }

  Future<void> applyWishOperationsInTopBusiness(
      BuildContext context, TopBusinessItemModel model) async {
    if (checkUserAuth(context, true)) {
      getIt<LoadingHelper>().showLoadingDialog();
      bool result = false;
      result = await _updateWishBusiness(result, model);
      if (result) {
        _updateTopPartnerCubit(model);
      }
      getIt<LoadingHelper>().dismissDialog();
    }
  }

  void _updateTopPartnerCubit(TopBusinessItemModel model) {
    int index = topBusinessesCubit.state.data!.itemsList.indexOf(model);
    topBusinessesCubit.state.data!.itemsList[index].hasWishItem =
        !topBusinessesCubit.state.data!.itemsList[index].hasWishItem;
    topBusinessesCubit.onUpdateData(topBusinessesCubit.state.data);
  }

  Future _updateWishBusiness(bool result, TopBusinessItemModel model) async {
    if (model.hasWishItem) {
      result = await _removeBusinessWish(model.id);
    } else {
      result = await _addBusinessWish(model.id);
    }
    return result;
  }

  Future _removeBusinessWish(String id) async {
    var result = await RemoveWishBusiness()(id);
    return result;
  }

  Future _addBusinessWish(String id) async {
    var result = await UpdateWishBusiness()(id);
    return result;
  }

  void clearLocation(BuildContext context) {
    context.read<HomeLocationCubit>().onLocationUpdated(null);
    ficheServices(context);
  }

  GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: CustomFonts.googleMapKey);

  Future<void> displayPrediction(BuildContext context, Prediction? p) async {
    if (p != null) {
      var deviceModel = context.read<DeviceCubit>().state.model;
      PlacesDetailsResponse detail = await places.getDetailsByPlaceId(
          p.placeId!,
          language: deviceModel.locale.languageCode);
      lat = detail.result.geometry!.location.lat;
      lng = detail.result.geometry!.location.lng;
      moveSearchCamera(context, lat, lng ?? 0);
      // zoom = 10;
    }
  }

  void addressListBottomSheet(
    BuildContext context,
  ) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (cxt) {
          return BuildMyAddressesList(
            bloc: addressCubit,
            onSelectLocation: (model) => selectAddress(context, model),
          );
        });
  }

  void selectAddress(BuildContext context, AddressModel model) async {
    addressCubit.state.data.map((e) {
      e.isDefault = false;
      if (e == model) {
        e.isDefault = true;
      }
      return e;
    }).toList();
    addressCubit.onUpdateData(addressCubit.state.data);
    Navigator.of(context).pop();
    moveSearchCamera(context, model.latitude, model.longitude);
  }

  moveSearchCamera(BuildContext context, double lat, double lng) {
    controllerMap.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(lat, lng),
          zoom: 14.0,
          bearing: 30.0,
          tilt: 45.0,
        ),
      ),
    );
  }

  void shareSinglePage(
      BuildContext context, String id, String businessName) async {
    return getIt<Utilities>().shareSinglePage(context, id, businessName);
  }

  void resetFilter(BuildContext context, SelectedDateTimeEntity dateTime) {
    context.read<SearchCubit>().onUpdate(filter: "", dateTime: dateTime);
    context.read<SearchCubit>().onUpdateFiltersList(filters: []);
    context.read<SearchCubit>().onUpdateFilterType(fromCat: false);
    context.read<SearchCubit>().updateResultStatus(noSearchResult: false);
    popularCubit.state.data.map((e) => e.isSelected = false).toList();
    popularCubit.onUpdateToInitState(popularCubit.state.data);
    ficheServices(context);
  }

  void ficheServices(BuildContext context) {
    var filter = context.read<SearchCubit>().state.filter;
    var auth = context.read<DeviceCubit>().state.model.auth;
    updateCubitsToInitialState();
    getPopular(context);
    getHotDeals(context);
    getMainBanners(context);
    getServicesOffers(context);
    getTrendingServices(context);
    getPopularServices(context);
    if (auth) {
      getFavourite(context);
      getRecentlyServices(context);
      getRecommendedServices(context);
    }
    if (filter.isEmpty) {
      getTopBusiness(context);
    } else if (filter.isNotEmpty) {
      getSearchServices(context);
    }
  }

  void updateCubitsToInitialState() {
    popularServicesCubit.onUpdateToInitState(popularServicesCubit.state.data);
    onSearchServicesCubit.onUpdateToInitState(onSearchServicesCubit.state.data);
    hotDealsCubit.onUpdateToInitState(hotDealsCubit.state.data);
    mainBannersCubit.onUpdateToInitState(mainBannersCubit.state.data);
    servicesOffersCubit.onUpdateToInitState(servicesOffersCubit.state.data);
    trendingServiceCubit.onUpdateToInitState(trendingServiceCubit.state.data);
    recommendedCubit.onUpdateToInitState(recommendedCubit.state.data);
    recentlyCubit.onUpdateToInitState(recentlyCubit.state.data);
  }

  Future<void> fitchHomeData(
      BuildContext context, HomeMainData homeMainData) async {
    var auth = context.read<DeviceCubit>().state.model.auth;
    var filterState = context.read<SearchCubit>().state;
    getPopular(context, refresh: false);
    getPopular(context);
    if (filterState.filters.isEmpty || filterState.noSearchResult == true) {
      await getNearMeLoc(context);
      checkLocationDialogStatus(context, homeMainData);
      if (filterState.filter.isEmpty) {
        getMainBanners(context, refresh: false);
        getMainBanners(context);
        getHotDeals(context, refresh: false);
        getHotDeals(context);
        getTopBusiness(context, refresh: false);
        getTopBusiness(context);
      } else {
        getSearchServices(context);
      }
      getTrendingServices(context, refresh: false);
      getTrendingServices(context);
      getPopularServices(context, refresh: false);
      getPopularServices(context);
      getServicesOffers(context, refresh: false);
      getServicesOffers(context);
      if (auth) {
        getFavourite(context, refresh: false);
        getFavourite(context);
        getRecentlyServices(context, refresh: false);
        getRecentlyServices(context);
        getRecommendedServices(context, refresh: false);
        getRecommendedServices(context);
      }
    }
  }

  Future<void> goDateTimeFilterPage(BuildContext context) async {
    var dateTime = context.read<SearchCubit>().state.dateTime;
    final timesLength = dateTime.timeList.length;
    final datesLength = dateTime.dateList.length;
    await AutoRouter.of(context).push(DateTimePageRoute()).then((value) {
      if (datesLength != dateTime.dateList.length ||
          timesLength != dateTime.timeList.length) {
        ficheServices(context);
      }
    });
  }

  List<String> recentSearchList = [];

  saveStringListValue(String text) async {
    recentSearchList.add(text);
    var prefs = await SharedPreferences.getInstance();
    prefs.setStringList("recentSearch", recentSearchList.toSet().toList());
  }

  retrieveStringListValue() async {
    var prefs = await SharedPreferences.getInstance();
    List<String>? value = prefs.getStringList("recentSearch");
    if (value != null && value.isNotEmpty) {
      recentSearchList = value;
    }
  }

  String durationToString(int minutes) {
    if (minutes < 60) {
      return '$minutes ${tr("min")}';
    } else if (minutes == 60) {
      return '1 ${tr("h")}';
    }
    var duration = Duration(minutes: minutes);
    List<String> parts = duration.toString().split(':');
    return '${parts[0].padRight(2, ' ${tr("h")}')}: ${parts[1].padLeft(2, '0')} ${tr("min")}';
  }

  checkLocationDialogStatus(
      BuildContext context, HomeMainData homeMainData) async {
    LocationEntity? loc = context.read<HomeLocationCubit>().state.model;
    if (loc == null) {
      Future.delayed(
        Duration(seconds: 1),
        () => showLocationBottomSheet(context, homeMainData),
      );
    }
  }

  moveCamera(BuildContext context) {
    controllerMap.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            locBusinessesBloc.state.data?.latitude ?? 25.276987,
            locBusinessesBloc.state.data?.longitude ?? 55.296249,
          ),
          zoom: 14,
          bearing: 30.0,
          tilt: 45.0,
        ),
      ),
    );
  }

  void saveLocNearMe(LatLng loc, String title) async {
    var prefs = await SharedPreferences.getInstance();
    String data = jsonEncode(
        NearMeLocEntity(lng: loc.longitude, lat: loc.latitude, title: title));
    prefs.setString("nearMeLoc", data);
  }

  Future<void> getNearMeLoc(BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    var resultData = prefs.getString('nearMeLoc');
    if (resultData != null) {
      NearMeLocEntity loc = NearMeLocEntity.fromJson(json.decode(resultData));
      context.read<HomeLocationCubit>().onLocationUpdated(
          LocationEntity(lng: loc.lng, lat: loc.lat, title: loc.title));
    }
  }

  Future<bool> willPopCallback(BuildContext context) async {
    LocationEntity? loc = context.read<HomeLocationCubit>().state.model;
    if (loc == null) {
      return false;
    } else {
      return true;
    }
  }

  Future<bool> willPopResetFilter(BuildContext context) async {
    var searchCubit = context.read<SearchCubit>().state;
    if (searchCubit.filter.isNotEmpty) {
      resetFilter(context, SelectedDateTimeEntity(dateList: [], timeList: []));
      return false;
    } else {
      return false;
    }
  }
}
