part of 'home_main_widgets_imports.dart';

class BuildMapMarker extends StatelessWidget {
  final TopBusinessItemModel model;

  const BuildMapMarker({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110.h,
      // width: 100.w,
      padding: EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(25.r),
        image: DecorationImage(
          image: AssetImage(Res.markerActiveBg),
          fit: BoxFit.fill,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: MyText(
              title: model.distanceText,
              size: 12.sp,
              color: MyColors.black,
              fontWeight: FontWeight.bold,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Image.asset(
            Res.markerActive,
            fit: BoxFit.fill,
          ),
        ],
      ),
    );
  }
}
