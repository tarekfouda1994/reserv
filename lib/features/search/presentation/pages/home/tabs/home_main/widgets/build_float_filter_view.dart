part of 'home_main_widgets_imports.dart';

class BuildFloatFilterView extends StatelessWidget {
  final HomeMainData homeMainData;
  final GenericBloc<int> pagesCubit;
  final bool show;
  final int currentPage;

  const BuildFloatFilterView(
      {Key? key,
      required this.homeMainData,
      required this.show,
      required this.currentPage,
      required this.pagesCubit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    if (!show) return SizedBox();
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
        bloc: homeMainData.scrollCubit,
        builder: (context, state) {
          return BlocBuilder<GenericBloc<int>, GenericState<int>>(
            bloc: homeMainData.filterCountCubit,
            builder: (context, countState) {
              return AnimatedOpacity(
                opacity: (state.data ? 1 : 0),
                duration: Duration(milliseconds: 500),
                child: Container(
                  height: 36.h,
                  width: device.isTablet ? 185.w : 234.w,
                  decoration: BoxDecoration(
                      color: MyColors.white,
                      borderRadius: BorderRadius.circular(30.r),
                      border: Border.all(color: MyColors.primary)),
                  child: Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () =>pagesCubit
                              .onUpdateData(currentPage == 1 ? 0 : 1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                  state.data == 0 ? Res.map : Res.apps_sort,
                                  width: device.isTablet ? 30 : 19,
                                  height: device.isTablet ? 30 : 19,
                                  color: Color(0xffABC8C8)),
                              SizedBox(width: 5.w),
                              MyText(
                                  title: currentPage == 0
                                      ? tr("map")
                                      : tr("list"),
                                  color: MyColors.primary,
                                  size: device.isTablet ? 7.sp : 11.5.sp,
                                  ),
                            ],
                          ),
                        ),
                      ),
                      VerticalDivider(thickness: 1.2),
                      Expanded(
                        child: InkWell(
                          onTap: () => homeMainData.goToFilter(context),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(Res.filter,
                                  width: device.isTablet ? 30 : 19,
                                  height: device.isTablet ? 30 : 19,
                                  color: Color(0xffABC8C8)),
                              SizedBox(width: 5.w),
                              MyText(
                                  title: tr("filters"),
                                  color: MyColors.primary,
                                  size: device.isTablet ? 7.sp : 11.5.sp,
                                  ),
                              SizedBox(width: 5.w),
                              Visibility(
                                visible: countState.data != 0,
                                child: BuildIconContainer(
                                  width: 25.r,
                                  height: 25.r,
                                  bgColor: MyColors.primary,
                                  child: MyText(
                                    title: "${countState.data}",
                                    color: MyColors.white,
                                    size: device.isTablet ? 8.sp : 12.sp,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }
}
