
import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/bloc/search_cubit/search_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/extensions/home_departments_extension.dart';
import 'package:flutter_tdd/core/helpers/enums.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/helpers/scroll_behavior.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/core/widgets/custom_slider_widget.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/business_placeholder.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/category_placeholder.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/service_placeholder.dart';
import 'package:flutter_tdd/features/general/domain/entities/location_entity.dart';
import 'package:flutter_tdd/features/general/presentation/pages/location_address/widgets/LocationWidgetsImports.dart';
import 'package:flutter_tdd/features/profile/data/model/services_favourit_model/services_favourite_model.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/widgets/favourite_widget_imports.dart';
import 'package:flutter_tdd/features/reservation/data/models/reservation_details_without_login_model/reservation_details_without_login_model.dart';
import 'package:flutter_tdd/features/reservation/presentation/manager/reservation_details_cubit/reservation_details_cubit.dart';
import 'package:flutter_tdd/features/search/data/models/hot_deals_item_model/hot_deals_item_model.dart';
import 'package:flutter_tdd/features/search/data/models/hot_deals_model/hot_deals_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_cat_model/popular_cat_model.dart';
import 'package:flutter_tdd/features/search/data/models/top_business_item_model/top_business_item_model.dart';
import 'package:flutter_tdd/features/search/data/models/top_businesses_model/top_businesses_model.dart';
import 'package:flutter_tdd/features/search/data/models/trending_services_item_model/trending_services_item_model.dart';
import 'package:flutter_tdd/features/search/data/models/trinding_model/trinding_model.dart';
import 'package:flutter_tdd/features/search/presentation/manager/location_cubit/location_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/home_main/views/all_search_text_result/all_search_text_result_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_icon_container.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_service_card.dart';
import 'package:flutter_tdd/features/search/presentation/widgets/widgets_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../../../../../../../core/helpers/di.dart';
import '../../../../../../../../../../core/helpers/utilities.dart';
import '../../../../../../../../core/localization/localization_methods.dart';
import '../../../../../../data/models/home_banners/banner_model.dart';
import '../../../../../../data/models/home_banners/home_banners_model.dart';
import '../../../../../../domain/entites/selected_date_time_entity.dart';
import '../home_main_imports.dart';

part 'build_animated_layer.dart';
part 'build_banner_item.dart';
part 'build_category_item.dart';
part 'build_date_time_app_bar.dart';
part 'build_empty_services_view.dart';
part 'build_favourit_sticky_header.dart';
part 'build_float_filter_view.dart';
part 'build_header_home.dart';
part 'build_home_app_bar.dart';
part 'build_home_button.dart';
part 'build_home_main_body.dart';
part 'build_list_view.dart';
part 'build_location_bottom_sheet_content.dart';
part 'build_main_banners_view.dart';
part 'build_map_marker.dart';
part 'build_map_view.dart';
part 'build_popular_categories.dart';
part 'build_reservation_details_header.dart';
part 'build_selected_marker.dart';
part 'build_services_sticky_header.dart';
part 'build_slider_view.dart';
part 'build_success_details.dart';
part 'build_success_reservation_bottom_sheet.dart';
part 'build_swiper_item.dart';
part 'build_top_business_card.dart';
part 'build_top_businesses_stickey_header.dart';
part 'custom_main_list_view.dart';
part 'image_swiper_curve_painter.dart';