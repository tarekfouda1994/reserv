part of 'home_main_widgets_imports.dart';

class ImageSwiperCurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.white;
    paint.style = PaintingStyle.fill; // Change this to fill
    var path = Path();

    path.moveTo(size.width, 0);
    path.quadraticBezierTo(size.width / 1.5, size.height / 1.7, size.width, size.height);
    path.lineTo(size.height.h, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
