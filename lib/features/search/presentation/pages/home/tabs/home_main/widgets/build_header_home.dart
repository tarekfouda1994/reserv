part of 'home_main_widgets_imports.dart';

class BuildHeaderHome extends StatelessWidget {
  final HomeMainData homeMainData;

  const BuildHeaderHome({Key? key, required this.homeMainData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return BlocBuilder<HomeLocationCubit, HomeLocationState>(
        builder: (context, locationState) {
      return BlocBuilder<SearchCubit, SearchState>(
        builder: (context, state) {
          return BuildAnimatedLayer(
            homeMainData: homeMainData,
            child: Container(
              color: MyColors.primary,
              child: Column(
                children: [
                  Container(
                    height: 35.h,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    margin: EdgeInsets.only(
                      top: 2.h,
                      bottom: 8.h,
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          Visibility(
                            visible: locationState.model == null,
                            child: BuildHomeButton(
                              onTap: () => homeMainData.showLocationBottomSheet(
                                  context, homeMainData),
                              bg: MyColors.bgRed,
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                    Res.location,
                                    color: Colors.red,
                                    width: 14.r,
                                    height: 14.r,
                                  ),
                                  Expanded(
                                    child: MyText(
                                      title: tr("select_location"),
                                      color: MyColors.errorColor,
                                      size: isTablet ? 7.sp : 11.sp,
                                      overflow: TextOverflow.ellipsis,
                                      alien: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            replacement: BuildHomeButton(
                              onTap: () => homeMainData.showLocationBottomSheet(
                                  context, homeMainData),
                              textColor: MyColors.errorColor,
                              text: tr("select_location"),
                              bg: MyColors.primaryDark,
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                          Res.location,
                                          color: MyColors.white,
                                          width: 14.r,
                                          height: 14.r,
                                        ),
                                        SizedBox(width: 6),
                                        Flexible(
                                          child: MyText(
                                            title:
                                                _locationTitle(locationState),
                                            color: MyColors.white,
                                            size: isTablet ? 7.sp : 11.sp,
                                            overflow: TextOverflow.ellipsis,
                                            alien: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () =>
                                        homeMainData.clearLocation(context),
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      padding: const EdgeInsets.all(2.5),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border:
                                            Border.all(color: MyColors.primary),
                                        color: MyColors.primary,
                                      ),
                                      child: Icon(
                                        Icons.close,
                                        color: MyColors.white,
                                        size: isTablet ? 8.sp : 12.sp,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(width: 15),
                          Visibility(
                            visible: state.dateTime.timeList.isEmpty &&
                                state.dateTime.dateList.isEmpty,
                            child: BuildHomeButton(
                              onTap: () =>
                                  homeMainData.goDateTimeFilterPage(context),
                              textColor: MyColors.white,
                              text: tr("select_dateTime"),
                              bg: MyColors.primaryDark,
                              iconData: Icons.access_time,
                            ),
                            replacement: BuildDateTimeAppBar(
                              trendingServicesItemModel: null,
                              filter: state.filter,
                              dateTime: state.dateTime,
                              homeMainData: homeMainData,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    });
  }

  String _locationTitle(HomeLocationState locationState) {
    return locationState.model?.title == "null"
        ? tr("near_me")
        : locationState.model?.title ?? "";
  }
}
