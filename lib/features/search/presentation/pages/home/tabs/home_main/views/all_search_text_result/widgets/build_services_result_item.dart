part of 'all_services_widgets_imports.dart';

class BuildServicesResultItem extends StatelessWidget {
  final AllSearchTextResultController controller;
  final int index;

  const BuildServicesResultItem(
      {Key? key, required this.controller, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    final String filter = context.read<SearchCubit>().state.filters[index];
    return BlocBuilder<GenericBloc<TrindingModel?>,
            GenericState<TrindingModel?>>(
        bloc: controller.servicesCubits[index],
        builder: (cxt, state) {
          if (state is GenericUpdateState) {
            return Visibility(
              visible: state.data!.itemsList.isNotEmpty,
              replacement: BuildEmptyServicesViewOnFilter(filter: filter),
              child: Column(
                children: [
                  _buildBuildDeptHeader(isEn, filter, state, context),
                  CustomSliderWidget(
                    height:
                        device.isTablet ? 412.sm : (!isEn ? 295.sm : 280.sm),
                    items: List.generate(state.data!.itemsList.length, (index) {
                      final item = state.data!.itemsList[index];
                      return BuildServiceCard(
                        homeDepartments: HomeDepartments.onSearch,
                        fromHome: true,
                        serviceModel: item,
                        onUpdateWish: () {
                          controller.updateWish(
                            context,
                            item,
                            controller.servicesCubits[index],
                          );
                        },
                      );
                    }),
                  ),
                ],
              ),
            );
          } else {
            return Column(
              children: [
                BuildDeptHeader(
                  title: isEn
                      ? "$filter "
                      : "${tr("services_headerTiTle")} $filter",
                  subTitle: isEn ? tr("services") : "",
                  hasMore: false,
                ),
                BuildShimmerView(
                  height: !isEn ? 250.h : 220.h,
                  margin: EdgeInsets.all(10.sp),
                ),
                SizedBox(height: 30.h),
              ],
            );
          }
        });
  }

  BuildDeptHeader _buildBuildDeptHeader(bool isEn, String filter,
      GenericState<TrindingModel?> state, BuildContext context) {
    return BuildDeptHeader(
      title: filter,
      count: state.data!.totalRecords,
      bockGroundColorCount: MyColors.primary,
      counterColor: MyColors.white,
      hasMore: state.data!.totalRecords > 2 ? true : false,
      onTap: () => AutoRouter.of(context).push(
        TrendingServicesRoute(
          homeApisType: HomeApisType.onSearch,
          filter: filter,
          titleAppBar: filter,
        ),
      ),
    );
  }
}
