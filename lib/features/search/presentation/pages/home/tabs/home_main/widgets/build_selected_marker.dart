part of 'home_main_widgets_imports.dart';

class BuildSelectedMarker extends StatelessWidget {
  final HomeMainData mainData;

  const BuildSelectedMarker({Key? key, required this.mainData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<TopBusinessItemModel?>,
        GenericState<TopBusinessItemModel?>>(
      bloc: mainData.placeCubit,
      builder: (context, state) {
        if (state.data == null) {
          return Container();
        }
        return Container(
          height: 140,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(10),
          color: MyColors.primary,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: MyText(
                      title: state.data?.getBusinessName() ?? "",
                      color: MyColors.white,
                      size: 14.sp,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  if((state.data?.rating??0) > 0)
                  Row(
                    children: [
                      Icon(
                        Icons.star,
                        size: 18,
                        color: MyColors.secondary,
                      ),
                      MyText(
                        title: state.data?.rating.toString()??"0",
                        size: 12,
                        color: MyColors.secondary,
                      ),
                      SizedBox(width: 5),
                    ],
                  ),

                  BuildIconContainer(
                    onTap: () => mainData.placeCubit.onUpdateData(null),
                    width: 32,
                    height: 32,
                    bgColor: MyColors.black,
                    child: Icon(
                      Icons.close,
                      size: 18,
                      color: MyColors.white,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: MyText(
                      title: state.data?.getAddressName() ?? "",
                      color: MyColors.offWhite,
                      size: 12,
                    ),
                  ),
                  MyText(
                    title: state.data?.distanceText??"",
                    size: 12,
                    color: MyColors.secondary,
                  ),
                ],
              ),
              Spacer(),
              InkWell(
                onTap: () => AutoRouter.of(context).push(ReservationRootRoute(
                  businessID: state.data?.id ?? "",
                )),
                child: Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                    color: MyColors.primaryDark,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Visibility(
                      visible: device.locale == Locale('en', 'US'),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MyText(
                            title: "R",
                            color: MyColors.secondary,
                            size: device.isTablet ? 9.sp : 14,
                            fontWeight: FontWeight.bold,
                          ),
                          MyText(
                            title: "eserve now",
                            color: MyColors.white,
                            size: device.isTablet ? 8.sp : 10.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ],
                      ),
                      replacement: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MyText(
                            title: "احجز الآن",
                            color: MyColors.white,
                            size: device.isTablet ? 8.sp : 10.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ],
                      )),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
