part of 'home_main_widgets_imports.dart';

class BuildTopBusinessCard extends StatelessWidget {
  final TopBusinessItemModel businessModel;
  final void Function() updateWish;
  final void Function() shareLink;
  final EdgeInsetsGeometry? margin;

  const BuildTopBusinessCard(
      {Key? key,
      required this.businessModel,
      required this.updateWish,
      required this.shareLink,
      this.margin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return InkWell(
      onTap: () => AutoRouter.of(context)
          .push(ProductDetailsRoute(id: businessModel.id)),
      child: Container(
        margin: margin ??
            EdgeInsetsDirectional.only(end: 12.w, bottom: 15.w),
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.borderCard),
            // boxShadow: <BoxShadow>[
            //   BoxShadow(
            //     color: Color.fromRGBO(0, 0, 0, .1),
            //     offset: Offset(0.0, 2.0),
            //     blurRadius: 20.0,
            //   ),
            // ],
            borderRadius: BorderRadius.circular(11),
            color: MyColors.white),
        child: Column(
          children: [
            Center(
              child: CachedImage(
                url: businessModel.businesssImage.replaceAll("\\", "/"),
                height: device.isTablet ? 240.sm : 160.sm,
                fit: BoxFit.cover,
                bgColor: MyColors.defaultImgBg,
                borderWidth: 0,
                placeHolder: BusinessPlaceholder(),
                borderRadius: BorderRadius.vertical(top: Radius.circular(10.r)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            InkWell(
                              onTap: shareLink,
                              child: Container(
                                width: 30.r,
                                height: 30.r,
                                margin: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: MyColors.white),
                                  shape: BoxShape.circle,
                                  color: MyColors.favouriteBg,
                                ),
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.share_outlined,
                                  size: device.isTablet ? 12.sp : 17.sp,
                                  color: MyColors.white,
                                ),
                              ),
                            ),
                            BuildFavouriteIcon(
                              onTap: updateWish,
                              hasWish: businessModel.hasWishItem,
                            ),
                          ],
                        ),
                        if(businessModel.numberOfService!=0)
                        Container(
                          padding: EdgeInsets.symmetric(
                            vertical: 6.h,
                            horizontal: device.isTablet ? 6.w : 10.w,
                          ),
                          decoration: BoxDecoration(
                            color: MyColors.black,
                            borderRadius: BorderRadius.only(
                              topLeft: isEn
                                  ? Radius.circular(14.r)
                                  : Radius.circular(0),
                              topRight: isEn
                                  ? Radius.circular(0)
                                  : Radius.circular(14.r),
                            ),
                          ),
                          child: MyText(
                            title:
                                "${getIt<Utilities>().convertNumToAr(context: context, value: "${businessModel.numberOfService}")}  ${tr("availableServices")}",
                            color: MyColors.white,
                            size: device.isTablet ? 7.sp : 11.sp,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Transform.scale(
                        scale: .8,
                        child: SvgPicture.asset(
                          Res.building,
                          width: device.isTablet ? 12.w : null,
                        ),
                      ),
                      Expanded(
                        child: MyText(
                          title: " " + businessModel.getBusinessName(),
                          color: MyColors.black,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 11.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Offstage(
                        offstage: businessModel.rating == 0,
                        child: Row(
                          children: [
                            MyText(
                              title: getIt<Utilities>().convertNumToAr(
                                context: context,
                                value: businessModel.rating.toString(),
                              ),
                              color: MyColors.amber,
                              size: device.isTablet ? 7.sp : 11.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(width: 4.sp),
                            SvgPicture.asset(
                              Res.star,
                              color: MyColors.amber,
                              width: 14.r,
                              height: 14.r,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5).r,
                    child: Row(
                      children: [
                        Transform.scale(
                            scale: .9,
                            child: SvgPicture.asset(Res.marker_input,
                                width: 14.sp, height: 15.sp)),
                        Expanded(
                          child: MyText(
                            title: " ${businessModel.getAddressName()}",
                            color: MyColors.black,
                            overflow: TextOverflow.ellipsis,
                            size: device.isTablet ? 7.sp : 11.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Transform.scale(
                        scale: .9,
                        child: SvgPicture.asset(
                          Res.marker_input,
                          width: 14.sp,
                          height: 15.sp,
                          color: MyColors.white,
                        ),
                      ),
                      MyText(
                        title: businessModel.distance(context).isNotEmpty
                            ? "  ${businessModel.distance(context)}"
                            : "",
                        color: MyColors.black,
                        size: device.isTablet ? 5.5.sp : 9.sp,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 4.w),
                      InkWell(
                        onTap: () => getIt<Utilities>().trackingBusinessOnMap(
                          businessModel.latitude,
                          businessModel.longitude,
                          businessModel.getBusinessName(),
                          businessModel.getAddressName(),
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(Res.location_arrow_icon),
                            SizedBox(width: 2),
                            MyText(
                              decoration: TextDecoration.underline,
                              title: tr("direction"),
                              color: Color(0xff4278F6),
                              size: device.isTablet ? 5.5.sp : 9.sp,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
