part of 'home_main_widgets_imports.dart';

class BuildMapView extends StatefulWidget {
  final HomeMainData homeMainData;

  const BuildMapView({Key? key, required this.homeMainData}) : super(key: key);

  @override
  State<BuildMapView> createState() => _BuildMapViewState();
}

class _BuildMapViewState extends State<BuildMapView> {
  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    getIt<Utilities>().getCurrentLocation(context).then((value) {
      widget.homeMainData.locBusinessesBloc.onUpdateData(value);
      widget.homeMainData.moveCamera(context);
      widget.homeMainData.getTopBusiness(context, pageSize: 200);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return Container(
      color: MyColors.white,
      padding: EdgeInsets.fromLTRB(16.r, 16.r, 16.r, isTablet ? 100 : 70),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12).r,
        child: BlocBuilder<GenericBloc<LocationData?>,
                GenericState<LocationData?>>(
            bloc: widget.homeMainData.locBusinessesBloc,
            builder: (_, locState) {
              return BlocBuilder<GenericBloc<List<Marker>>,
                  GenericState<List<Marker>>>(
                bloc: widget.homeMainData.markersCubit,
                builder: (_, state) {
                  CameraPosition _initialPosition =
                      _buildCameraPosition(locState);
                  return Stack(
                    children: [
                      GoogleMap(
                        compassEnabled: true,
                        myLocationEnabled: true,
                        mapToolbarEnabled: true,
                        zoomControlsEnabled: false,
                        zoomGesturesEnabled: true,
                        buildingsEnabled: false,
                        trafficEnabled: false,
                        tiltGesturesEnabled: true,
                        myLocationButtonEnabled: true,
                        initialCameraPosition: _initialPosition,
                        markers: state.data.toSet(),
                        onMapCreated: widget.homeMainData.onMapCreated,
                        onCameraIdle: () {},
                        onCameraMove: (position) {},
                      ),
                      Positioned(
                        child: BuildSelectedMarker(
                          mainData: widget.homeMainData,
                        ),
                      ),
                      Positioned(
                        bottom: 12.r,
                        right: 5,
                        left: 5,
                        child: BuildSearchField(
                          displayPrediction: (cxt, p) {
                            return widget.homeMainData
                                .displayPrediction(cxt, p);
                          },
                          onTapMyAddress: _buildIsNotEmpty()
                              ? () => widget.homeMainData
                                  .addressListBottomSheet(context)
                              : null,
                        ),
                      )
                    ],
                  );
                },
              );
            }),
      ),
    );
  }

  bool _buildIsNotEmpty() {
    return widget.homeMainData.addressCubit.state.data.isNotEmpty;
  }

  CameraPosition _buildCameraPosition(GenericState<LocationData?> locState) {
    return CameraPosition(
      target: LatLng(locState.data?.latitude ?? 25.276987,
          locState.data?.longitude ?? 55.296249),
      zoom: 14,
    );
  }
}
