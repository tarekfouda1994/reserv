part of 'home_main_widgets_imports.dart';

class BuildEmptyServicesView extends StatelessWidget {

  const BuildEmptyServicesView({Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = context.watch<DeviceCubit>().state.model;
    return Center(
      child: Padding(
        padding: EdgeInsets.only(bottom: 25, top: 10).r,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          SvgPicture.asset(
            Res.EmptyServices,
            width: 40.r,
            height: 40.r,
          ),
          MyText(
              title: " ${tr("noServicesFound")} ",
              color: MyColors.grey,
              size: model.isTablet ? 8.sp : 11.sp),
        ]),
      ),
    );
  }
}
