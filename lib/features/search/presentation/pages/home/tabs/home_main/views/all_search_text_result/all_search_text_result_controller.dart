part of 'all_search_text_result_imports.dart';

class AllSearchTextResultController {
  List<GenericBloc<TrindingModel?>> servicesCubits = [];

  AllSearchTextResultController(BuildContext context) {
    _getAllServices(context);
  }

  Future<void> _getAllServices(BuildContext context) async {
    final filters = context.read<SearchCubit>().state.filters;
    _setInitialBlocs(filters);
    for (var element in filters) {
      _getServices(context, element);
    }
  }

  List<GenericBloc<TrindingModel?>> _setInitialBlocs(List<String> filters) =>
      servicesCubits = List.generate(
        filters.length,
        (index) => GenericBloc(null),
      );

  Future<void> _getServices(BuildContext context, String filter) async {
    final int index = context.read<SearchCubit>().state.filters.indexOf(filter);
    final bloc = servicesCubits[index];
    TrendingPrams params = _trendingPrams(filter, context);
    var data = await GetHomeServices()(params);
    bloc.onUpdateData(data);
  }

  TrendingPrams _trendingPrams(String filter, BuildContext context) {
    var dateTime = context.read<SearchCubit>().state.dateTime;
    dateTime.dateList.sort((a, b) => a.compareTo(b));
    dateTime.timeList.sort((a, b) => a.title.compareTo(b.title));
    var lotLong = context.read<HomeLocationCubit>().state.model;
    return TrendingPrams(
        pageNumber: 1,
        pageSize: 10,
        homeApisType: HomeApisType.onSearch,
        refresh: true,
        filter: filter,
        fromScheduleDate:
            getIt<Utilities>().formatDate(dateTime.dateList.firstOrNull),
        fromTime: dateTime.timeList.firstOrNull?.title,
        toScheduleDate:
            getIt<Utilities>().formatDate(dateTime.dateList.lastOrNull),
        toTime: dateTime.timeList.lastOrNull?.title,
        longitude: lotLong?.lng ?? 0,
        latitude: lotLong?.lat ?? 0);
  }

  Future<void> updateWish(BuildContext context, TrendingServicesItemModel model,
      GenericBloc<TrindingModel?> bloc) async {
    final auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      getIt<LoadingHelper>().showLoadingDialog();
      if (!model.hasWishItem!) {
        var update = await UpdateWishService()(WishParams(
            businessServiceID: model.eBusinessServiceID,
            serviceID: model.serviceID));
        if (update) {
          model.hasWishItem = !model.hasWishItem!;
          bloc.onUpdateData(bloc.state.data!);
        }
      } else {
        var update = await RemoveWishService()(WishParams(
            serviceID: model.serviceID,
            businessServiceID: model.eBusinessServiceID));
        if (update) {
          model.hasWishItem = !model.hasWishItem!;
          bloc.onUpdateData(bloc.state.data!);
        }
      }
      getIt<LoadingHelper>().dismissDialog();
    } else {
      CustomToast.customAuthDialog();
    }
  }

// void shareSinglePage(
//     BuildContext context, String id, String businessName) async {
//   return getIt<Utilities>().shareSinglePage(context, id, businessName);
// }
}
