part of 'home_main_widgets_imports.dart';

class BuildSliderView extends StatelessWidget {
  final HomeMainData homeMainData;
  final String filter;

  const BuildSliderView(
      {Key? key, required this.homeMainData, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<HotDealsModel?>,
            GenericState<HotDealsModel?>>(
        bloc: homeMainData.hotDealsCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data!.itemsList.isEmpty) {
              return Container();
            }
            return Column(
              children: [
                BuildDeptHeader(
                  title: tr("hot_deals"),
                  isHotDeal: true,
                  bockGroundColorCount: Color(0xffED3A57),
                  counterColor: MyColors.white,
                  count: state.data!.totalRecords,
                  onTap: () => AutoRouter.of(context).push(HotDealsPageRoute()),
                ),
                CustomSliderWidget(
                  height: device.isTablet ? 260.sm : 170.sm,
                  items: List.generate(state.data!.itemsList.length, (index) {
                    return BuildSwiperItem(
                      model: state.data!.itemsList[index],
                      fromHome: true,
                    );
                  }),
                ),
              ],
            );
          }
          return Column(
            children: [
              BuildDeptHeader(title: tr("hot_deals"), hasMore: false),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: BuildShimmerView(
                  height: device.isTablet ? 180.h : 145.h,
                ),
              ),
              SizedBox(height: 30.h),
            ],
          );
        });
  }
}
