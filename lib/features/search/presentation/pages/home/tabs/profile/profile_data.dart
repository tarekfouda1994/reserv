part of 'profile_imports.dart';

class ProfileData {
  final _googleSignIn = GoogleSignIn();
  List<Widget> pages = [];
  String? email;
  late GenericBloc<ProfilePages> pagesCubit;

  ProfileData(GenericBloc<ProfilePages> cubit) {
    pagesCubit = cubit;
  }

  initPage(ProfileData profileData, GenericBloc<ProfilePages> cubit) {
    pagesCubit = cubit;
    var context = getIt<GlobalContext>().context();
    pages = [
      IntroScreen(profileData: profileData),
      MyProfile(),
    ];
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      cubit.onUpdateData(ProfilePages.profile);
    }
  }

  Future<GoogleSignInAccount?> loginGoogle() => _googleSignIn.signIn();

  Future<GoogleSignInAccount?> logoutGoogle() => _googleSignIn.signOut();

  Future<void> googleLogin(BuildContext context) async {
    var data = await loginGoogle();
    if (data != null) {
      var name = data.displayName.toString().split(" ").toList();
      socialMediaLogin(
        firstName: name.length == 1
            ? data.displayName ?? ""
            : name.sublist(0, name.length - 1).join(' ').toString(),
        email: data.email,
        userPhoto: data.photoUrl ?? "",
        externalUserType: 1,
        lastName: _lastName(name),
      );
    }
  }

  Future<void> faceBookLogin() async {
    signInWithFacebook().then((userCredential) {
      User? user = userCredential!.user;
      if (user?.email != null && user != null) {
        _callSocialMediaLogin(user);
      } else {
        CustomToast.showSimpleToast(
          msg: tr("pleaseLoginWithAnotherMethod"),
          title: tr("login"),
          type: ToastType.error,
        );
      }
    });
  }

  void _callSocialMediaLogin(User user) {
    var name = user.displayName.toString().split(" ").toList();
    socialMediaLogin(
      externalUserType: 2,
      lastName: _lastName(name),
      firstName: _firstName(name, user),
      userPhoto: user.photoURL!,
      email: user.email,
    );
  }

  String _lastName(List<String> name) => name.length == 1 ? "" : name.last;

  String _firstName(List<String> name, User user) {
    return name.length == 1
        ? user.displayName!
        : name.sublist(0, name.length - 1).join(' ').toString();
  }

  Future<UserCredential?> signInWithFacebook() async {
    try {
      final LoginResult loginResult =
          await FacebookAuth.instance.login().then((value) {
        return value;
      });
      // Create a credential from the access token
      final OAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(loginResult.accessToken!.token);
      // Once signed in, return the UserCredential
      return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
    } catch (e) {
      return null;
    }
  }

  Future<void> socialMediaLogin({
    required String firstName,
    required String userPhoto,
    required int externalUserType,
    required String lastName,
    required String? email,
  }) async {
    var context = getIt<GlobalContext>().context();
    ExternalLoginParams params = _externalLoginParams(
      firstName,
      userPhoto,
      externalUserType,
      email,
      lastName,
    );
    final result = await MediaLogin()(params);
    if (result != null) {
      var data = await GetUserData().call(result.userId);
      if (data != null) {
        context.read<DeviceCubit>().updateUserAuth(true);
        data = data.copyWith(
            token: result.authenticationToken, userId: result.userId);
        getIt<Utilities>().saveUserData(data);
        GlobalState.instance.set("token", result.authenticationToken);
        context.read<UserCubit>().onUpdateUserData(data);
        _onSuccess(context, result, data.phoneNumber.isNotEmpty);
      }
    }
  }

  void _onSuccess(BuildContext context, LoginModel result, bool hasPhone) {
    if (hasPhone) {
      AutoRouter.of(context).pushAndPopUntil(
        HomeRoute(),
        predicate: (cxt) => false,
      );
    } else {
      AutoRouter.of(context).push(
        EditProfileRoute(notCompletedData: true),
      );
    }
    _handleToast(hasPhone, result);
  }

  void _handleToast(bool hasPhone, LoginModel result) {
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        _successLoginToast(result);
        if (!hasPhone) {
          Future.delayed(
            Duration(seconds: 2),
            () {
              CustomToast.showSimpleToast(
                title: tr("Warning_Toast"),
                msg: tr("completeYourData"),
                type: ToastType.warning,
              );
            },
          );
        }
      },
    );
  }

  void _successLoginToast(LoginModel result) {
    CustomToast.showSimpleToast(
      title: tr("success_login"),
      type: ToastType.success,
      msg: "${tr("welcome_back")} ${result.firstName}",
    );
  }

  ExternalLoginParams _externalLoginParams(String firstName, String userPhoto,
      int externalUserType, String? email, String lastName) {
    return ExternalLoginParams(
      firstName: firstName,
      userPhoto: userPhoto,
      externalUserType: externalUserType,
      email: email,
      lastName: lastName.isNotEmpty ? lastName : firstName,
    );
  }
}

enum ProfilePages {
  intro,
  // login,
  // confirmationEmail,
  // sentSuccess,
  // register,
  profile
}
