part of 'profile_imports.dart';

class Profile extends StatefulWidget {
  final GenericBloc<ProfilePages> pagesCubit;

  const Profile({Key? key, required this.pagesCubit}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late ProfileData profileData;

  @override
  void initState() {
    profileData = ProfileData(widget.pagesCubit);
    profileData.initPage(profileData, widget.pagesCubit);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<ProfilePages>, GenericState<ProfilePages>>(
      bloc: widget.pagesCubit,
      builder: (_, state) {
        return AnimatedSwitcher(
          duration: Duration(milliseconds: 500),
          reverseDuration: Duration(milliseconds: 500),
          child: profileData.pages[state.data.index],
          transitionBuilder: (child, animation) {
            return FadeTransition(
              child: child,
              opacity: animation,
            );
          },
        );
      },
    );
  }
}
