import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/features/auth/data/model/login_model/login_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/external_login_params.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/media_login.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/intro_screen/intro_screen_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/pages/my_profile/my_profile_imports.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';

import '../../../../../../../core/helpers/custom_toast.dart';
import '../../../../../../../core/helpers/di.dart';
import '../../../../../../../core/helpers/global_state.dart';
import '../../../../../../../core/helpers/utilities.dart';
import '../../../../../../../core/localization/localization_methods.dart';
import '../../../../../../auth/presentation/manager/user_cubit/user_cubit.dart';
import '../../../../../../profile/domain/use_cases/get_user_data.dart';

part 'profile.dart';
part 'profile_data.dart';
