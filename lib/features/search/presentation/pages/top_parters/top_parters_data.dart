part of 'top_parters_imports.dart';

class TopPartnerData {
  final PagingController<int, TopBusinessItemModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  ficheData(BuildContext context, int currentPage,
      {bool refresh = true}) async {
    var filter = context.read<SearchCubit>().state.filter;
    var params = _topBusinessParams(currentPage, filter);
    var data = await GetTopBusinesses()(params);
    final isLastPage = data!.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
  }

  TopBusinessParams _topBusinessParams(int currentPage, String filter) {
    return TopBusinessParams(
      pageNumber: currentPage,
      pageSize: pageSize,
      loadImages: true,
      filter: filter,
      refresh: true,
    );
  }

  void shareSinglePage(
      BuildContext context, String id, String businessName) async {
    return getIt<Utilities>().shareSinglePage(context, id, businessName);
  }

  Future<void> applyWishOperationsInTopBusiness(
      BuildContext context, TopBusinessItemModel model) async {
    if (checkUserAuth(context, true)) {
      getIt<LoadingHelper>().showLoadingDialog();
      bool result = false;
      result = await _updateWishBusiness(result, model);
      if (result) {
        _updateTopPartnerCubit(model);
      }
      getIt<LoadingHelper>().dismissDialog();
    }
  }

  void _updateTopPartnerCubit(TopBusinessItemModel model) {
    int index = pagingController.itemList!.indexOf(model);
    pagingController.itemList![index].hasWishItem =
        !pagingController.itemList![index].hasWishItem;
    pagingController.notifyListeners();
  }

  bool checkUserAuth(BuildContext context, bool isSalon) {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      return true;
    } else {
      CustomToast.customAuthDialog(isSalon: isSalon);
    }
    return false;
  }

  _updateWishBusiness(bool result, TopBusinessItemModel model) async {
    if (model.hasWishItem) {
      result = await _removeBusinessWish(model.id);
    } else {
      result = await _addBusinessWish(model.id);
    }
    return result;
  }

  _removeBusinessWish(String id) async {
    var result = await RemoveWishBusiness()(id);
    return result;
  }

  _addBusinessWish(String id) async {
    var result = await UpdateWishBusiness()(id);
    return result;
  }
}
