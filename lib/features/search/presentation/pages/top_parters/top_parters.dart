part of 'top_parters_imports.dart';

class TopPartners extends StatefulWidget {
  const TopPartners({Key? key}) : super(key: key);

  @override
  State<TopPartners> createState() => _TopPartnersState();
}

class _TopPartnersState extends State<TopPartners> {
  TopPartnerData topPartnerData = TopPartnerData();

  @override
  void initState() {
    topPartnerData.pagingController.addPageRequestListener((pageKey) {
      topPartnerData.ficheData(context, pageKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      appBar: DefaultAppBar(
        title: tr("top_partners"),
      ),
      body: RefreshIndicator(
        color: MyColors.primary,
        onRefresh: () => topPartnerData.ficheData(context, 1),
        child: PagedListView<int, TopBusinessItemModel>(
          padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 16),
          pagingController: topPartnerData.pagingController,
          builderDelegate: PagedChildBuilderDelegate<TopBusinessItemModel>(
            firstPageProgressIndicatorBuilder: (context) => Column(
                children: List.generate(
              5,
              (index) {
                return BuildShimmerView(
                  height: device.isTablet ? 180.h : 150.h,
                );
              },
            )),
            itemBuilder: (context, item, index) {
              return BuildTopBusinessCard(
                margin: EdgeInsetsDirectional.only(bottom: 7.h),
                businessModel: item,
                shareLink: () {
                  topPartnerData.shareSinglePage(
                      context, item.id, item.getBusinessName());
                },
                updateWish: () {
                  topPartnerData.applyWishOperationsInTopBusiness(
                      context, item);
                },
              );
            },
            newPageProgressIndicatorBuilder: (con) => Padding(
                padding: const EdgeInsets.all(8.0),
                child: CupertinoActivityIndicator()),
            firstPageErrorIndicatorBuilder: (context) => BuildPageError(
              onTap: () => topPartnerData.pagingController.refresh(),
            ),
          ),
        ),
      ),
    );
  }
}
