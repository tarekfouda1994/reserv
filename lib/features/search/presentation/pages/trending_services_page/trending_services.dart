part of 'trending_services_imports.dart';

class TrendingServices extends StatefulWidget {
  final HomeApisType homeApisType;
  final String titleAppBar;
  final String? filter;

  const TrendingServices({
    Key? key,
    required this.homeApisType,
    required this.titleAppBar, this.filter,
  }) : super(key: key);

  @override
  State<TrendingServices> createState() => _TrendingServicesState();
}

class _TrendingServicesState extends State<TrendingServices> {
  final TrendingServicesData trendingServicesData = TrendingServicesData();

  @override
  void initState() {
    trendingServicesData.pagingController.addPageRequestListener((pageKey) {
      trendingServicesData.ficheData(
        context,
        pageKey,
        filterPage: widget.filter,
        homeApisType: widget.homeApisType,

      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool auth = context.watch<DeviceCubit>().state.model.auth!;
    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(title: widget.titleAppBar, size: 65),
      body: RefreshIndicator(
        color: MyColors.primary,
        onRefresh: () => trendingServicesData.ficheData(context, 1,
            homeApisType: widget.homeApisType),
        child: PagedListView<int, TrendingServicesItemModel>(
          pagingController: trendingServicesData.pagingController,
          builderDelegate: PagedChildBuilderDelegate<TrendingServicesItemModel>(
            firstPageProgressIndicatorBuilder: (context) =>
                BuildTrendingLoadingView(),
            itemBuilder: (context, item, index) {
              return BuildTrendingServicesCard(
                serviceModel: item,
                onUpdateWish: () =>
                    trendingServicesData.updateWish(context, item, auth),
              );
            },
            newPageProgressIndicatorBuilder: (con) => Padding(
              padding: const EdgeInsets.all(8.0),
              child: CupertinoActivityIndicator(),
            ),
            firstPageErrorIndicatorBuilder: (context) => BuildPageError(
              onTap: () => trendingServicesData.pagingController.refresh(),
            ),
          ),
        ),
      ),
    );
  }
}
