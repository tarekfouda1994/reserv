import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/build_shemer.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/service_placeholder.dart';
import 'package:flutter_tdd/features/search/data/models/trending_services_item_model/trending_services_item_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../res.dart';

part 'build_trending_loading_view.dart';
part 'build_trending_services_card.dart';
