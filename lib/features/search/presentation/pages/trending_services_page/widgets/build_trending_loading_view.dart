part of'trending_services_widgets_imports.dart';

class BuildTrendingLoadingView extends StatelessWidget {
  const BuildTrendingLoadingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
        children: List.generate(
          5,
              (index) {
            return BuildShimmerView(
                height: device.isTablet ? 180.h : 160.h,
                width: MediaQuery.of(context).size.width * .9);
          },
        ));
  }
}
