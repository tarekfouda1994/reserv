part of 'trending_services_widgets_imports.dart';

class BuildTrendingServicesCard extends StatelessWidget {
  final TrendingServicesItemModel serviceModel;
  final void Function() onUpdateWish;

  const BuildTrendingServicesCard({
    Key? key,
    required this.serviceModel,
    required this.onUpdateWish,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return Container(
      margin: EdgeInsetsDirectional.only(
          end: 16.w, start: 16.w, bottom: 15.h, top: 5.h),
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.borderCard),
          // boxShadow: <BoxShadow>[
          //   BoxShadow(
          //     color: Color.fromRGBO(0, 0, 0, .1),
          //     offset: Offset(0.0, 2.0),
          //     blurRadius: 20.0,
          //   ),
          // ],
          borderRadius: BorderRadius.circular(11.r),
          color: MyColors.white),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () => AutoRouter.of(context).push(ReservationRootRoute(
                businessID: serviceModel.businessID,
                serviceId: serviceModel.serviceID)),
            child: Column(
              children: [
                Center(
                  child: CachedImage(
                    url: serviceModel.serviceAvatar.replaceAll("\\", "/"),
                    height: device.isTablet ? 180.h : 150.h,
                    fit: BoxFit.cover,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(10.r)),
                    bgColor: MyColors.defaultImgBg,
                    borderWidth: 0,
                    placeHolder: ServicePlaceholder(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 8),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 6, horizontal: 16),
                                  decoration: BoxDecoration(
                                      color: MyColors.white,
                                      border: Border.all(
                                          color: MyColors.primary, width: 1),
                                      borderRadius: BorderRadius.circular(30)),
                                  child: MyText(
                                    title: serviceModel.businessGender == "Male"
                                        ? tr("men")
                                        : tr("women"),
                                    color: MyColors.black,
                                    size: device.isTablet ? 6.sp : 9.sp,
                                  ),
                                ),
                                Visibility(
                                  visible: serviceModel.discount != 0,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 6, horizontal: 12),
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(30)),
                                    child: MyText(
                                      title: "${serviceModel.discount}% OFF",
                                      color: MyColors.white,
                                      size: device.isTablet ? 6.sp : 9.sp,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                // InkWell(
                                //   onTap: () => homeMainData.shareSinglePage(
                                //       context,
                                //       trendingServicesItemModel.businessID,
                                //       trendingServicesItemModel
                                //           .getServiceName(),
                                //       true),
                                //   borderRadius: BorderRadius.circular(100),
                                //   child: Container(
                                //     width: 30.r,
                                //     height: 30.r,
                                //     margin: EdgeInsets.symmetric(
                                //         vertical: 5, horizontal: 10),
                                //     decoration: BoxDecoration(
                                //       shape: BoxShape.circle,
                                //       color: MyColors.primary.withOpacity(.4),
                                //     ),
                                //     alignment: Alignment.center,
                                //     child: Icon(Icons.share_outlined,
                                //         size: device.isTablet ? 12.sp : 18.sp,
                                //         color: MyColors.white),
                                //   ),
                                // ),
                                if (serviceModel.hasWishItem != null)
                                  BuildFavouriteIcon(
                                    onTap: onUpdateWish,
                                    hasWish: serviceModel.hasWishItem!,
                                  ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: device.isTablet ? 5.sp : 8.sp,
                              horizontal: device.isTablet ? 8.sp : 10.sp),
                          decoration: BoxDecoration(
                            color: MyColors.black,
                            borderRadius: BorderRadius.only(
                              topLeft: isEn
                                  ? Radius.circular(14.r)
                                  : Radius.circular(0),
                              topRight: isEn
                                  ? Radius.circular(0)
                                  : Radius.circular(14.r),
                            ),
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              MyText(
                                  title:
                                      "${serviceModel.getCurrencyName()} ${serviceModel.servicePrice.round()}",
                                  color: MyColors.white,
                                  size: device.isTablet ? 7.sp : 11.sp),
                              SizedBox(width: device.isTablet ? 8.w : 10.w),
                              Icon(
                                Icons.access_time_outlined,
                                size: device.isTablet ? 12.sp : 15.sp,
                                color: MyColors.white,
                              ),
                              SizedBox(width: 3.w),
                              MyText(
                                title: serviceModel.durationToString,
                                color: MyColors.white,
                                size: device.isTablet ? 6.sp : 9.sp,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    children: [
                      Expanded(
                        child: MyText(
                          title: serviceModel.getServiceName(),
                          color: MyColors.black,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                      ),
                      Offstage(
                        offstage: serviceModel.serviceRating == 0,
                        child: Row(
                          children: [
                            MyText(
                              title: serviceModel.serviceRating.toString(),
                              color: Color(0xffF1C800),
                              size: device.isTablet ? 7.sp : 11.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(width: 4.sp),
                            SvgPicture.asset(
                              Res.star,
                              color: Color(0xffF1C800),
                              width: 14.r,
                              height: 14.r,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => AutoRouter.of(context)
                      .push(ProductDetailsRoute(id: serviceModel.businessID)),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Transform.scale(
                                    scale: .8,
                                    child: SvgPicture.asset(Res.building,
                                        width: device.isTablet ? 12.w : null)),
                                Expanded(
                                  child: MyText(
                                    title: serviceModel.getBusiness(),
                                    color: MyColors.black,
                                    size: device.isTablet ? 7.sp : 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Offstage(
                                  offstage:
                                      serviceModel.distance(context).isEmpty,
                                  child: MyText(
                                    title: serviceModel.distance(context),
                                    color: MyColors.black,
                                    size: device.isTablet ? 5.5.sp : 9.sp,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4.h),
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                children: [
                                  Transform.scale(
                                    scale: .9,
                                    child: SvgPicture.asset(
                                      Res.marker_input,
                                      width: 14.sp,
                                      height: 15.sp,
                                    ),
                                  ),
                                  Expanded(
                                    child: MyText(
                                      title:
                                          "  ${serviceModel.getServicesAddress()}",
                                      color: MyColors.blackOpacity,
                                      overflow: TextOverflow.ellipsis,
                                      size: device.isTablet ? 5.5.sp : 9.sp,
                                    ),
                                  ),
                                  SvgPicture.asset(Res.location_arrow_icon),
                                  SizedBox(width: 2),
                                  InkWell(
                                    onTap: () => getIt<Utilities>()
                                        .trackingBusinessOnMap(
                                      serviceModel.latitude,
                                      serviceModel.longitude,
                                      serviceModel.getBusiness(),
                                      serviceModel.getServicesAddress(),
                                    ),
                                    child: MyText(
                                        decoration: TextDecoration.underline,
                                        title: tr("direction"),
                                        color: Color(0xff4278F6),
                                        size: device.isTablet ? 5.5.sp : 9.sp),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
