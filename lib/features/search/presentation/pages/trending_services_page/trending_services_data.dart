part of 'trending_services_imports.dart';

class TrendingServicesData {
  final PagingController<int, TrendingServicesItemModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  ficheData(BuildContext context, int currentPage,
      {bool refresh = true,
      required HomeApisType homeApisType,
      String? filterPage}) async {
    var dateTime = context.read<SearchCubit>().state.dateTime;
    dateTime.dateList.sort((a, b) => a.compareTo(b));
    dateTime.timeList.sort((a, b) => a.title.compareTo(b.title));
    var filter = context.read<SearchCubit>().state.filter;
    var lotLong = context.read<HomeLocationCubit>().state.model;
    TrendingPrams params = _trendingPrams(
      currentPage,
      homeApisType,
      filterPage ?? filter,
      dateTime,
      lotLong,
    );
    var data = await GetHomeServices()(params);
    final isLastPage = data!.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
  }

  TrendingPrams _trendingPrams(int currentPage, HomeApisType homeApisType,
      String filter, SelectedDateTimeEntity dateTime, LocationEntity? lotLong) {
    return TrendingPrams(
        pageNumber: currentPage,
        pageSize: pageSize,
        homeApisType: homeApisType,
        refresh: true,
        filter: filter,
        fromScheduleDate:
            getIt<Utilities>().formatDate(dateTime.dateList.firstOrNull),
        fromTime: dateTime.timeList.firstOrNull?.title,
        toScheduleDate:
            getIt<Utilities>().formatDate(dateTime.dateList.lastOrNull),
        toTime: dateTime.timeList.lastOrNull?.title,
        longitude: lotLong?.lng ?? 0,
        latitude: lotLong?.lat ?? 0);
  }

  Future<void> updateWish(
      BuildContext context, TrendingServicesItemModel model, bool auth) async {
    if (auth) {
      getIt<LoadingHelper>().showLoadingDialog();

      int index = pagingController.itemList!.indexOf(model);
      if (!pagingController.itemList![index].hasWishItem!) {
        var update = await UpdateWishService()(WishParams(
            businessServiceID: model.eBusinessServiceID,
            serviceID: model.serviceID));
        if (update) {
          pagingController.itemList![index].hasWishItem =
              !pagingController.itemList![index].hasWishItem!;
          pagingController.notifyListeners();
        }
      } else {
        var update = await RemoveWishService()(WishParams(
            serviceID: model.serviceID,
            businessServiceID: model.eBusinessServiceID));
        if (update) {
          pagingController.itemList![index].hasWishItem =
              !pagingController.itemList![index].hasWishItem!;
          pagingController.notifyListeners();
        }
      }
      getIt<LoadingHelper>().dismissDialog();
    } else {
      CustomToast.customAuthDialog();
    }
  }

  void shareSinglePage(
      BuildContext context, String id, String businessName) async {
    return getIt<Utilities>().shareSinglePage(context, id, businessName);
  }
}
