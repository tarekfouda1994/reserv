part of 'all_banners_page_imports.dart';

class AllBannerPage extends StatefulWidget {
  const AllBannerPage({key}) : super(key: key);

  @override
  State<AllBannerPage> createState() => _AllBannerPageState();
}

class _AllBannerPageState extends State<AllBannerPage> {
  final AllBannersPageController controller = AllBannersPageController();

  @override
  void initState() {
    controller.pagingController.addPageRequestListener((pageKey) {
      controller.ficheData(context, pageKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bool isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return Scaffold(
      appBar: DefaultAppBar(size: 65, title: tr("main_banners")),
      body: RefreshIndicator(
        color: MyColors.primary,
        onRefresh: () => controller.ficheData(context, 1),
        child: PagedListView<int, BannerModel>(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          pagingController: controller.pagingController,
          builderDelegate: PagedChildBuilderDelegate<BannerModel>(
              firstPageProgressIndicatorBuilder: (context) => Column(
                    children: List.generate(
                      10,
                      (index) => BuildShimmerView(
                        height: isTablet ? 160.h : 120.h,
                        width: MediaQuery.of(context).size.width * .9,
                      ),
                    ),
                  ),
              itemBuilder: (context, item, index) => SizedBox(
                    child: BuildBannerItem(model: item),
                    height: isTablet ? 160.h : 120.h,
                  ),
              newPageProgressIndicatorBuilder: (con) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CupertinoActivityIndicator(),
                  ),
              firstPageErrorIndicatorBuilder: (context) => BuildPageError(
                    onTap: () => controller.pagingController.refresh(),
                  )),
        ),
      ),
    );
  }
}
