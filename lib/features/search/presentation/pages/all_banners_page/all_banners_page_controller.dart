part of 'all_banners_page_imports.dart';

class AllBannersPageController {
  final PagingController<int, BannerModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  ficheData(BuildContext context, int currentPage, {bool refresh = true}) async {
    var location = context.read<HomeLocationCubit>().state.model;
    var filter = context.read<SearchCubit>().state.filter;
    BannersParams params =
        _bannerParams(refresh, filter, currentPage, location);
    var data = await GetMainBanners()(params);
    final isLastPage = data.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
  }

  BannersParams _bannerParams(
      bool refresh, String filter, int currentPage, LocationEntity? location) {
    return BannersParams(
      refresh: refresh,
      filter: filter,
      pageNumber: currentPage,
      pageSize: pageSize,
      longitude: location?.lng ?? 0,
      latitude: location?.lat ?? 0,
      loadImages: true,
    );
  }
}
