part of 'filter_widget_imports.dart';

class BuildBottom extends StatelessWidget {
  final DeviceModel device;
  final FilterData filterData;

  const BuildBottom({Key? key, required this.device, required this.filterData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<int>>, GenericState<List<int>>>(
      bloc: filterData.filterCountCubit,
        builder: (context, state) {
      return Column(mainAxisSize: MainAxisSize.min, children: [
        Divider(height: 0, thickness: 1),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: device.isTablet ? 8.sp : 16, vertical: 20.sp),
          child: Row(
            children: [
              SizedBox(width: 10),
              Expanded(
                flex: 4,
                child: DefaultButton(
                  onTap: () => filterData.clearAllFilter(),
                  title: tr("clear_all"),
                  color: MyColors.primary.withOpacity(.05),
                  textColor: MyColors.primary,
                  borderColor: MyColors.white,
                  borderRadius: BorderRadius.circular(30.r),
                  margin: EdgeInsets.zero,
                  fontSize: device.isTablet ? 7.sp : 11.sp,
                  height: device.isTablet ? 60.sm : 50.sm,
                ),
              ),
              const SizedBox(width: 8),
              Expanded(
                flex: 7,
                child: DefaultButton(
                  onTap: () => filterData.confirmFilter(context),
                  title: "${tr("apply_filters")} ${state.data.length}",
                  color: MyColors.primary,
                  textColor: MyColors.white,
                  borderColor: MyColors.white,
                  borderRadius: BorderRadius.circular(30.r),
                  margin: EdgeInsets.zero,
                  fontSize: device.isTablet ? 7.sp : 11.sp,
                  height: device.isTablet ? 60.sm : 50.sm,
                ),
              ),
            ],
          ),
        )
      ]);
    });
  }
}
