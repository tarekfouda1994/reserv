part of 'filter_widget_imports.dart';

class BuildDuration extends StatelessWidget {
  final DeviceModel deviceModel;
  final FilterData filterData;

  const BuildDuration(
      {Key? key, required this.deviceModel, required this.filterData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<SfRangeValues>, GenericState<SfRangeValues>>(
        bloc: filterData.durationCubit,
        builder: (cxt, state) {
          return Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: MyText(
                        title: tr("duration"),
                        color: MyColors.black,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                        fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  Visibility(
                    visible: _visibleUnSelect(state),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(100),
                      onTap: () {
                        filterData
                            .removeFilterConteItem(FilterOptions.duration);
                        filterData.durationCubit.onUpdateData(
                            SfRangeValues(.308 / .6.toDouble(), 8.0));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 8),
                              padding: const EdgeInsets.all(2.5),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(color: MyColors.primary),
                                  color: MyColors.primary),
                              child: Icon(Icons.close,
                                  color: MyColors.white,
                                  size: deviceModel.isTablet ? 7.sp : 10.sp),
                            ),
                            MyText(
                                title: "${tr("unselect_duration")} ",
                                color: MyColors.black,
                                size: deviceModel.isTablet ? 6.sp : 9.sp),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              Transform.scale(
                scale: 1.08,
                child: Container(
                  child: SfRangeSlider(
                    min: .308 / .6.toDouble(),
                    max: 8.toDouble(),
                    values: state.data,
                    labelPlacement: LabelPlacement.betweenTicks,
                    interval: .01,
                    inactiveColor: MyColors.grey.withOpacity(.7),
                    activeColor: MyColors.primary,
                    dateIntervalType: DateIntervalType.hours,
                    dateFormat: DateFormat.y(),
                    onChanged: (v) => filterData.onChangeDuration(v),
                    showTicks: false,
                    showLabels: false,
                    enableTooltip: true,
                    tooltipTextFormatterCallback: (v, val) {
                      List<String> value = val.split(".");
                      int last = (int.parse(value.last) * .6).toInt();
                      int first = int.parse(value.first);
                      return first == 0
                          ? "$last" " ${tr("min")}"
                          : "$first" "${tr("h")}:" "$last" " ${tr("min")}";
                    },
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MyText(
                    title: "30 ${tr("min")}",
                    color: MyColors.primary,
                    size: deviceModel.isTablet ? 7.sp : 11.sp,
                  ),
                  MyText(
                    title: "8 ${tr("hour")}",
                    color: MyColors.primary,
                    size: deviceModel.isTablet ? 7.sp : 11.sp,
                  )
                ],
              )
            ],
          );
        });
  }

  bool _visibleUnSelect(GenericState<SfRangeValues> state) {
    return state.data.start != .308 / .6.toDouble() || state.data.end != 8.0;
  }
}
