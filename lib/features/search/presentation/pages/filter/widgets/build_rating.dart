part of 'filter_widget_imports.dart';

class BuildRating extends StatelessWidget {
  final DeviceModel deviceModel;
  final FilterData filterData;

  const BuildRating(
      {Key? key, required this.deviceModel, required this.filterData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<RatingEntity>>,
            GenericState<List<RatingEntity>>>(
        bloc: filterData.ratingCubit,
        builder: (context, state) {
          return Column(children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: MyText(
                      title: tr("rating"),
                      color: MyColors.black,
                      size: deviceModel.isTablet ? 8.sp : 11.sp,
                      fontWeight: FontWeight.bold),
                ),
                Spacer(),
                Visibility(
                  visible:
                      state.data.any((element) => element.selected == true),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(100),
                    onTap: () => filterData.removeAllRate(),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 8),
                            padding: const EdgeInsets.all(2.5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: MyColors.primary),
                                color: MyColors.primary),
                            child: Icon(
                              Icons.close,
                              color: MyColors.white,
                              size: deviceModel.isTablet ? 7.sp : 10.sp,
                            ),
                          ),
                          MyText(
                            title: "${tr("unselect_rate")} ",
                            color: MyColors.black,
                            size: deviceModel.isTablet ? 7.sp : 9.sp,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              ...List.generate(state.data.length, (index) {
                return InkWell(
                  onTap: () => filterData.selectRate(index),
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 12.sp, vertical: 8),
                    decoration: BoxDecoration(
                      color: state.data[index].selected!
                          ? MyColors.primary
                          : MyColors.white,
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(
                        color: state.data[index].selected!
                            ? MyColors.white
                            : MyColors.grey.withOpacity(.5),
                      ),
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        MyText(
                          title: state.data[index].ratingVal.toString(),
                          color: state.data[index].selected!
                              ? MyColors.white
                              : MyColors.primary,
                          size: deviceModel.isTablet ? 7.sp : 11.sp,
                        ),
                        SizedBox(width: 4.sp),
                        SvgPicture.asset(
                          Res.star_outline,
                          width: 12.r,
                          height: 12.r,
                          color: state.data[index].selected!
                              ? MyColors.white
                              : MyColors.primary,
                        ),
                      ],
                    ),
                  ),
                );
              }),
            ])
          ]);
        });
  }
}
