part of 'filter_widget_imports.dart';

class BuildPaymentType extends StatelessWidget {
  final DeviceModel deviceModel;

  const BuildPaymentType({Key? key, required this.deviceModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: MyText(
                title: tr("payment_type"),
                color: MyColors.black,
                size: deviceModel.isTablet ? 8.sp : 12.sp,
                fontWeight: FontWeight.bold),
          ),
          Spacer(),
          Visibility(
            // visible:
            // state.data.any((element) => element.selected == true),
            child: InkWell(
              borderRadius: BorderRadius.circular(100),
              // onTap: () => filterData.removeAllRate(),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      padding: const EdgeInsets.all(2.5),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: MyColors.primary),
                          color: MyColors.primary),
                      child: Icon(Icons.close, color: MyColors.white, size: 10),
                    ),
                    MyText(
                        title: "${tr("unselect")} ",
                        color: MyColors.black,
                        size: deviceModel.isTablet ? 7.sp : 11.sp),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 16),
      Wrap(
        runSpacing: 10,
        spacing: 10,
        alignment: WrapAlignment.center,
        children: List.generate(
          2,
          (index) => Container(
            width: MediaQuery.of(context).size.width * .44,
            padding: EdgeInsets.symmetric(vertical: 7),
            decoration: BoxDecoration(
              border: Border.all(color: MyColors.primary),
              borderRadius: BorderRadius.circular(30.r),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  padding: const EdgeInsets.all(2.5),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: MyColors.primary),
                      color: MyColors.white),
                  child: Icon(Icons.close, color: MyColors.primary, size: 10),
                ),
                MyText(
                    alien: TextAlign.center,
                    color: MyColors.grey,
                    size: deviceModel.isTablet ? 7.sp : 10.sp,
                    title: tr("credit_card")),
              ],
            ),
          ),
        ),
      ),
    ]);
  }
}
