part of 'filter_widget_imports.dart';

class BuildBudget extends StatelessWidget {
  final FilterData filterData;

  const BuildBudget({Key? key, required this.filterData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return BlocBuilder<GenericBloc<SfRangeValues>, GenericState<SfRangeValues>>(
        bloc: filterData.priceCubit,
        builder: (cxt, state) {
          return BlocBuilder<GenericBloc<List<RangePriceEntity>>,
                  GenericState<List<RangePriceEntity>>>(
              bloc: filterData.rangePriceCubit,
              builder: (cxt, budgetState) {
                return Wrap(
                  runSpacing: 10,
                  spacing: 10,
                  // alignment: WrapAlignment.center,
                  children: List.generate(
                    budgetState.data.length,
                    (index) => InkWell(
                      onTap: () => filterData.selectBudget(index),
                      child: Container(
                        width: MediaQuery.of(context).size.width * .44,
                        padding: EdgeInsets.symmetric(vertical: 8),
                        decoration: BoxDecoration(
                            color:
                                state.data.end == budgetState.data[index].to &&
                                        state.data.start ==
                                            budgetState.data[index].from
                                    ? MyColors.primary
                                    : MyColors.white,
                            border: Border.all(color: MyColors.primary),
                            borderRadius: BorderRadius.circular(30.r)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Visibility(
                              visible: state.data.end ==
                                      budgetState.data[index].to &&
                                  state.data.start ==
                                      budgetState.data[index].from,
                              child: InkWell(
                                onTap: () {
                                  filterData.deletePriceItem(index);
                                  filterData.removeFilterConteItem(
                                      FilterOptions.budget);
                                },
                                child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 5),
                                  padding: const EdgeInsets.all(2.5),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border:
                                          Border.all(color: MyColors.primary),
                                      color: MyColors.white),
                                  child: Icon(Icons.close,
                                      color: MyColors.primary, size: 10),
                                ),
                              ),
                            ),
                            MyText(
                                alien: TextAlign.center,
                                color: state.data.end ==
                                            budgetState.data[index].to &&
                                        state.data.start ==
                                            budgetState.data[index].from
                                    ? MyColors.white
                                    : MyColors.grey,
                                size: isTablet ? 7.sp : 10.sp,
                                title: filterData.budgetData(index)),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              });
        });
  }
}
