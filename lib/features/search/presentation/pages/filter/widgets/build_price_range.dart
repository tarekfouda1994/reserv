part of 'filter_widget_imports.dart';

class BuildPriceRange extends StatelessWidget {
  final DeviceModel deviceModel;
  final FilterData filterData;

  const BuildPriceRange(
      {Key? key, required this.deviceModel, required this.filterData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<SfRangeValues>, GenericState<SfRangeValues>>(
      bloc: filterData.priceCubit,
      builder: (cxt, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: MyText(
                    title: tr("budget"),
                    color: MyColors.black,
                    size: deviceModel.isTablet ? 7.sp : 11.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                Visibility(
                  visible: state.data.start != 0 || state.data.end != 700,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(100),
                    onTap: () {
                      filterData.priceCubit.onUpdateData(SfRangeValues(0, 700));
                      filterData.removeFilterConteItem(FilterOptions.budget);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 8),
                            padding: const EdgeInsets.all(2.5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: MyColors.primary),
                                color: MyColors.primary),
                            child: Icon(Icons.close,
                                color: MyColors.white,
                                size: deviceModel.isTablet ? 7.sp : 10.sp),
                          ),
                          MyText(
                              title: "${tr("unselect_price")} ",
                              color: MyColors.black,
                              size: deviceModel.isTablet ? 6.sp : 9.sp),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 16),
            BuildBudget(filterData: filterData),
            Transform.scale(
              scale: 1.08,
              child: SfRangeSlider(
                min: 0,
                max: 700,
                values: state.data,
                interval: 1,
                inactiveColor: MyColors.grey.withOpacity(.7),
                activeColor: MyColors.primary,
                onChanged: (v) => filterData.onChangePrice(v),
                showTicks: false,
                showLabels: false,
                enableTooltip: true,
                tooltipTextFormatterCallback: (v, value) {
                  double val = double.parse(value);
                  return "${val.toInt()}" " ${tr("AED")}";
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: "0 ${tr("AED")}",
                  color: MyColors.primary,
                  size: deviceModel.isTablet ? 7.sp : 11.sp,
                ),
                MyText(
                  title: "700 ${tr("AED")}",
                  color: MyColors.primary,
                  size: deviceModel.isTablet ? 7.sp : 11.sp,
                )
              ],
            )
          ],
        );
      },
    );
  }
}
