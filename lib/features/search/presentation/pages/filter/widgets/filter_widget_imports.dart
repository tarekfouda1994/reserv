import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/widgets/build_radio_item.dart';
import 'package:flutter_tdd/features/search/domain/entites/range_price_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/rateing_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/sort_entity.dart';
import 'package:flutter_tdd/features/search/presentation/pages/filter/filter_import.dart';
import 'package:flutter_tdd/res.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../core/models/device_model/device_model.dart';

part 'Build_sort_body.dart';
part 'build_bottom.dart';
part 'build_budget.dart';
part 'build_duration.dart';
part 'build_gender_body.dart';
part 'build_payment_type.dart';
part 'build_price_range.dart';
part 'build_rating.dart';
