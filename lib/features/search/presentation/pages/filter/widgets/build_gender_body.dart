part of 'filter_widget_imports.dart';

class BuildGenderBody extends StatelessWidget {
  final DeviceModel device;
  final FilterData filterData;

  const BuildGenderBody(
      {Key? key, required this.device, required this.filterData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
        bloc: filterData.genderCubit,
        builder: (cxt, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  title: tr("gender"),
                  color: MyColors.black,
                  size: device.isTablet ? 7.sp : 11,
                  fontWeight: FontWeight.bold),
              SizedBox(height: 10),
              BuildRadioItem(
                value: 0,
                changeValue: state.data,
                title: tr("both"),
                onTap: () => filterData.selectGender(0),
              ),
              BuildRadioItem(
                value: 1,
                changeValue: state.data,
                title: tr("men"),
                onTap: () => filterData.selectGender(1),
              ),
              BuildRadioItem(
                value: 2,
                changeValue: state.data,
                title: tr("women"),
                onTap: () => filterData.selectGender(2),
              ),
            ],
          );
        });
  }
}
