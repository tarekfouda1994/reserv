part of 'filter_widget_imports.dart';

class BuildSortBody extends StatelessWidget {
  final DeviceModel device;
  final FilterData filterData;

  const BuildSortBody(
      {Key? key, required this.device, required this.filterData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<SortParams>>,
            GenericState<List<SortParams>>>(
        bloc: filterData.sortCubit,
        builder: (cxt, state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyText(
                  title: tr("sort_by"),
                  color: MyColors.black,
                  size: device.isTablet ? 7.sp : 11,
                  fontWeight: FontWeight.bold),
              SizedBox(height: 10),
              ...List.generate(state.data.length, (index) {
                return Container(
                  margin: const EdgeInsets.symmetric(vertical: 6),
                  child: InkWell(
                    onTap: () => filterData.selectSortItem(index),
                    child: Row(children: [
                      Container(
                        padding: EdgeInsets.all(2.sp),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: MyColors.primary),
                          color: state.data[index].selected == true
                              ? MyColors.primary
                              : MyColors.white,
                        ),
                        child: Icon(
                          Icons.done,
                          color: MyColors.white,
                          size: device.isTablet ? 8.sp : 16,
                        ),
                      ),
                      SizedBox(width: 10),
                      MyText(
                        title: state.data[index].title,
                        color: state.data[index].selected == true
                            ? MyColors.primary
                            : MyColors.grey,
                        size: device.isTablet ? 7.sp : 11.sp,
                      ),
                    ]),
                  ),
                );
              })
            ],
          );
        });
  }
}
