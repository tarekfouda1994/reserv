part of 'filter_import.dart';

class FilterData {
  GenericBloc<List<int>> filterCountCubit = GenericBloc([]);
  GenericBloc<List<RatingEntity>> ratingCubit = GenericBloc([]);
  GenericBloc<List<SortParams>> sortCubit = GenericBloc([]);
  GenericBloc<List<RangePriceEntity>> rangePriceCubit = GenericBloc([]);
  GenericBloc<int> genderCubit = GenericBloc(0);
  GenericBloc<SfRangeValues> durationCubit =
      GenericBloc(SfRangeValues(.308 / .6.toDouble(), 8.0));
  GenericBloc<SfRangeValues> priceCubit = GenericBloc(SfRangeValues(0, 700));
  String? sort;
  bool? sortDirection;
  FilterEntity? filterEntity;

  void saveRangePriceList(List<RangePriceEntity> list) async {
    var prefs = await SharedPreferences.getInstance();
    var jsonData = list.map((e) => e.toJson()).toList();
    prefs.setString("rangePrice", json.encode(jsonData));
  }

  void retrieveRangePriceValue() async {
    var prefs = await SharedPreferences.getInstance();
    var value = prefs.getString("rangePrice");
    if (value != null) {
      var items = List<RangePriceEntity>.from(
          json.decode(value).map((e) => RangePriceEntity.fromJson(e)));
      rangePriceCubit.onUpdateData(items.take(6).toList());
    }
  }

  void deletePriceItem(int index) async {
    rangePriceCubit.state.data.removeAt(index);
    rangePriceCubit.onUpdateData(rangePriceCubit.state.data);
    var prefs = await SharedPreferences.getInstance();
    var jsonData = rangePriceCubit.state.data.map((e) => e.toJson()).toList();
    prefs.setString("rangePrice", json.encode(jsonData));
  }

  String budgetData(int index) {
    String title = "";
    if (rangePriceCubit.state.data[index].from != 0 ||
        rangePriceCubit.state.data[index].to != 700) {
      if (rangePriceCubit.state.data[index].from == 0) {
        title =
            "${tr("Less_than")} ${rangePriceCubit.state.data[index].to.toInt()}";
      } else if (rangePriceCubit.state.data[index].to == 700) {
        title =
            "${tr("More_than")} ${rangePriceCubit.state.data[index].from.toInt()}";
      } else {
        title =
            "${tr("From")} ${rangePriceCubit.state.data[index].from.toInt()} ${tr("to")} ${rangePriceCubit.state.data[index].to.toInt()}";
      }
    }
    return title;
  }

  void selectBudget(int index) {
    priceCubit.onUpdateData(SfRangeValues(
        rangePriceCubit.state.data[index].from,
        rangePriceCubit.state.data[index].to));
    addFilterCountItem(FilterOptions.budget);
  }

  void selectRate(int index) {
    ratingCubit.state.data.map((e) => e.selected = false).toList();
    ratingCubit.state.data[index].selected = true;
    ratingCubit.onUpdateData(ratingCubit.state.data);
    addFilterCountItem(FilterOptions.rating);
  }

  void removeAllRate() {
    ratingCubit.state.data.map((e) => e.selected = false).toList();
    ratingCubit.onUpdateData(ratingCubit.state.data);
    removeFilterConteItem(FilterOptions.rating);
  }

  void selectSortItem(int index) {
    if (sortCubit.state.data[index].selected == true) {
      sortCubit.state.data[index].selected = false;
      sortCubit.onUpdateData(sortCubit.state.data);
      removeFilterConteItem(FilterOptions.sortBy);
    } else {
      sortCubit.state.data.map((e) => e.selected = false).toList();
      addFilterCountItem(FilterOptions.sortBy);
      sortCubit.state.data[index].selected = true;
      sortCubit.onUpdateData(sortCubit.state.data);
      for (SortParams e in sortCubit.state.data) {
        if (e.selected == true) {
          sort = e.id.toString();
          sortDirection = e.sortDirection;
        }
      }
    }
  }

  void addRangePrice() {
    if (_checkPriceCubitNotDefault()) {
      rangePriceCubit.state.data.insert(
          0,
          RangePriceEntity(
              from: double.parse(priceCubit.state.data.start.toString()),
              to: double.parse(priceCubit.state.data.end.toString())));
      saveRangePriceList(rangePriceCubit.state.data);
    }
  }

  bool _checkPriceCubitNotDefault() {
    return (priceCubit.state.data.start != 0 ||
            priceCubit.state.data.end != 700) &&
        rangePriceCubit.state.data
            .where((element) =>
                element.from == priceCubit.state.data.start &&
                element.to == priceCubit.state.data.end)
            .toList()
            .isEmpty;
  }

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Initial Filter Data
  void initialData(FilterEntity filterEntity) {
    _initialSortBy(filterEntity);
    genderCubit.onUpdateData(filterEntity.genderID ?? 0);
    _initialRating(filterEntity);
    durationCubit.onUpdateData(SfRangeValues(
        filterEntity.minDuration ?? .308 / .6,
        filterEntity.maxDuration != null ? filterEntity.maxDuration! : 8));
    priceCubit.onUpdateData(SfRangeValues(filterEntity.minServiceAmount ?? 0,
        filterEntity.maxServiceAmount ?? 500));
    retrieveRangePriceValue();
    filterCountCubit.onUpdateData(filterEntity.filterCount ?? []);
  }

  void _initialSortBy(FilterEntity filterEntity) {
    sortDirection = filterEntity.sortDirection;
    sort = filterEntity.sortColumn;
    for (SortParams e in sortCubit.state.data) {
      if (e.id == filterEntity.sortColumn &&
          e.sortDirection == filterEntity.sortDirection) {
        e.selected = true;
      }
    }
  }

  void _initialRating(FilterEntity filterEntity) {
    for (RatingEntity e in ratingCubit.state.data) {
      if (e.ratingVal == filterEntity.rating) {
        e.selected = true;
      }
    }
    ratingCubit.onUpdateData(ratingCubit.state.data);
  }

  void selectGender(int val) {
    genderCubit.onUpdateData(val);
    _genderSwitch(val);
  }

  void _genderSwitch(int val) {
    switch (val) {
      case 0:
        removeFilterConteItem(FilterOptions.gender);
        break;
      default:
        addFilterCountItem(FilterOptions.gender);
    }
  }

  void onChangeDuration(SfRangeValues val) {
    durationCubit.onUpdateData(val);
    addFilterCountItem(FilterOptions.duration);
    if (val.end == 8.0 && val.start == 0.5133333333333333) {
      removeFilterConteItem(FilterOptions.duration);
    }
  }

  void onChangePrice(SfRangeValues val) {
    priceCubit.onUpdateData(val);
    addFilterCountItem(FilterOptions.budget);
    if (val.start == 0 && val.end == 700.0) {
      removeFilterConteItem(FilterOptions.budget);
    }
  }

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Submit Filter
  void confirmFilter(BuildContext context) {
    addRangePrice();
    filterEntity = FilterEntity(
        filterCount: filterCountCubit.state.data,
        genderID: genderCubit.state.data != 0 ? genderCubit.state.data : null,
        maxDuration: durationCubit.state.data.end != 8
            ? durationCubit.state.data.end
            : null,
        minDuration: durationCubit.state.data.start != .308 / .6
            ? durationCubit.state.data.start
            : null,
        maxServiceAmount:
            priceCubit.state.data.end != 500 ? priceCubit.state.data.end : null,
        minServiceAmount: priceCubit.state.data.start != 0
            ? priceCubit.state.data.start
            : null,
        rating:
            ratingCubit.state.data.any((element) => element.selected == true)
                ? ratingCubit.state.data
                    .firstWhere((element) => element.selected == true)
                    .ratingVal
                : null,
        sortColumn: sort != "" ? sort : null,
        sortDirection: sortDirection);
    AutoRouter.of(context).pop(filterEntity);
  }

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Clear Data Filter
  void clearAllFilter() {
    filterCountCubit.onUpdateToInitState([]);
    removeAllRate();
    filterEntity = null;
    sort = "";
    genderCubit.onUpdateData(0);
    sortCubit.state.data.map((e) => e.selected = false).toList();
    sortCubit.onUpdateData(sortCubit.state.data);
    durationCubit.onUpdateData(SfRangeValues(.308 / .6, 8));
    priceCubit.onUpdateData(SfRangeValues(0, 700));
  }

  void removeFilterConteItem(FilterOptions filterOptions) {
    filterCountCubit.state.data.removeWhere((e) => e == filterOptions.index);
    filterCountCubit.onUpdateData(filterCountCubit.state.data);
  }

  void addFilterCountItem(FilterOptions filterOptions) {
    filterCountCubit.state.data.add(filterOptions.index);
    filterCountCubit.onUpdateData(filterCountCubit.state.data.toSet().toList());
  }

  List<RatingEntity> ratingVal = [
    RatingEntity(ratingVal: 5),
    RatingEntity(ratingVal: 4),
    RatingEntity(ratingVal: 3),
    RatingEntity(ratingVal: 2),
    RatingEntity(ratingVal: 1),
  ];

  List<SortParams> sortVal = [
    SortParams(
        id: "businessRating",
        title: tr("RatingHighToLow"),
        sortDirection: false),
    SortParams(
      id: "businessRating",
      title: tr("RatingLowToHigh"),
      sortDirection: true, /**/
    ),
    SortParams(id: "", title: tr("newest")),
    SortParams(
      id: "servicePrice",
      title: tr("PriceHighToLow"),
      sortDirection: false,
    ),
    SortParams(
      id: "servicePrice",
      title: tr("PriceLowToHigh"),
      sortDirection: true,
    ),
  ];
}

enum FilterOptions { sortBy, gender, rating, duration, budget }
