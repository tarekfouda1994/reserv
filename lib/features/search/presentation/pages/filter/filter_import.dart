import 'dart:convert';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/search/domain/entites/filter_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/rateing_entity.dart';
import 'package:flutter_tdd/features/search/presentation/pages/filter/widgets/filter_widget_imports.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../core/bloc/device_cubit/device_cubit.dart';
import '../../../../../core/localization/localization_methods.dart';
import '../../../domain/entites/range_price_entity.dart';
import '../../../domain/entites/sort_entity.dart';

part 'filter.dart';
part 'filter_data.dart';
