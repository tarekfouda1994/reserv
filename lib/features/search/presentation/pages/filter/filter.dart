part of 'filter_import.dart';

class Filter extends StatefulWidget {
  final FilterEntity? filterEntity;

  const Filter({Key? key, this.filterEntity}) : super(key: key);

  @override
  State<Filter> createState() => _FilterState();
}

class _FilterState extends State<Filter> {
  FilterData filterData = FilterData();

  @override
  void initState() {
    filterData.ratingVal;
    filterData.ratingCubit.onUpdateData(filterData.ratingVal);
    filterData.sortCubit.onUpdateData(filterData.sortVal);
    if (widget.filterEntity != null) {
      filterData.initialData(widget.filterEntity!);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Scaffold(
      appBar: DefaultAppBar(title: tr("filters")),
      body: ListView(
          padding: EdgeInsets.only(
            right: 16,
            left: 16,
            top: 20,
            bottom: 30,
          ),
          children: [
            BuildSortBody(device: device, filterData: filterData),
            SizedBox(height: device.isTablet ? 40.h : 30.h),
            BuildGenderBody(device: device, filterData: filterData),
            SizedBox(height: device.isTablet ? 40.h : 30.h),
            BuildRating(filterData: filterData, deviceModel: device),
            SizedBox(height: device.isTablet ? 40.h : 30.h),
            BuildDuration(deviceModel: device, filterData: filterData),
            SizedBox(height: device.isTablet ? 40.h : 30.h),
            BuildPriceRange(deviceModel: device, filterData: filterData),
            SizedBox(height: device.isTablet ? 40.h : 30.h),
            // BuildPaymentType(deviceModel: device)
          ]),
      bottomNavigationBar: BuildBottom(device: device, filterData: filterData),
    );
  }
}
