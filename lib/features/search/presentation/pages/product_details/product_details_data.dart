part of 'product_details_imports.dart';

class ProductDetailsData {
  final GenericBloc<int> swiperCubit = GenericBloc(0);
  final GenericBloc<int> tabsCubit = GenericBloc(1);
  final GenericBloc<int> indicatorBloc = GenericBloc(0);
  GenericBloc<DetailsViews> pagesCubit = GenericBloc(DetailsViews.services);
  final GenericBloc<List<ServiceListModel>> servicesBloc = GenericBloc([]);
  GenericBloc<ServiceDetailsModel?> detailsCubit = GenericBloc(null);
  GenericBloc<ReviewsModel?> reviewsCubit = GenericBloc(null);
  final GenericBloc<List<ServiceTypesModel>> companyServicesBloc =
      GenericBloc([]);
  final PagingController<int, BusinessStaffModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  List<Widget> pages = [];

  initPage(BuildContext context, ProductDetailsData data, String businessId) {
    pages = [
      BuildServicesView(data: data),
      BuildReviewsView(data: data),
      StaffView(productDetailsData: data, businessId: businessId),
      DetailsView(data: data)
    ];
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    pagesCubit.onUpdateData(DetailsViews.services);
  }

  void fetchData(BuildContext context, ProductDetailsData data, String id) {
    initPage(context, data, id);
    getBusinessesDetails(context, id, refresh: false);
    getBusinessesDetails(context, id);
    // getCompanyServices(context, id, refresh: false);
    getCompanyServices(context, id);
    getServiceReviews(context, id, refresh: false);
    getServiceReviews(context, id);
    pagingController.addPageRequestListener((pageKey) {
      ficheBusinessStaff(context, pageKey, id);
    });
  }

  ficheBusinessStaff(BuildContext context, int currentPage, String businessID,
      {bool refresh = true}) async {
    var params = _businessStaffParams(currentPage, businessID);
    var data = await GetBusinessStaff()(params);
    final isLastPage = data.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data, nextPageKey);
    }
  }

  BusinessStaffParams _businessStaffParams(int currentPage, String businessID) {
    return BusinessStaffParams(
        pageNumber: currentPage, businessID: businessID, pageSize: pageSize);
  }

  void DetailsBottomSheet(
      BuildContext context,
      ProductDetailsData productDetailsData,
      ServiceListModel serviceListModel) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) {
        return BuildDetailsItemBottomSheet(
          productDetailsData: productDetailsData,
          serviceListModel: serviceListModel,
        );
      },
    );
  }

  void addStaff(ServiceListModel model, int index) {
    var data = servicesBloc.state.data.map((e) {
      if (e.businessServiceID == model.businessServiceID) {
        e.staffId = model.staffDetail[index].staffID;
        e.staffName = model.staffDetail[index].getStaffName();
      }
      return e;
    }).toList();
    servicesBloc.onUpdateData(data);
  }

  void removeStaff(ServiceListModel model) {
    servicesBloc.state.data.map((e) {
      if (e == model) {
        e.staffId = "";
        e.staffName = "";
      }
      return e;
    }).toList();
    servicesBloc.onUpdateData(servicesBloc.state.data);
  }

  void _bottomSheet(BuildContext context, Widget widget) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) => widget,
    );
  }

  void allServicesBottomSheet(
    BuildContext context,
    ProductDetailsData productDetailsData,
  ) {
    Widget widget = BuildSelectedServicesSheet(
      bloc: productDetailsData.servicesBloc,
      onRemoveItem: (index) =>
          _onRemoveItem(productDetailsData, index, context),
      onDeleteAll: () => _onDeleteAllSheetServices(productDetailsData, context),
    );
    _bottomSheet(context, widget);
  }

  void _onRemoveItem(
      ProductDetailsData productDetailsData, int index, BuildContext context) {
    var cubit = productDetailsData.servicesBloc.state.data;
    productDetailsData.onSelectService(cubit[index], context);
    if (cubit.isEmpty) AutoRouter.of(context).pop();
  }

  void _onDeleteAllSheetServices(
      ProductDetailsData productDetailsData, BuildContext context) {
    productDetailsData.servicesBloc.onUpdateData([]);
    productDetailsData.companyServicesBloc
        .onUpdateData(productDetailsData.companyServicesBloc.state.data);
    AutoRouter.of(context).pop();
  }

  void onSelectService(ServiceListModel model, BuildContext context) {
    if (servicesBloc.state.data.any((element) => element == model)) {
      servicesBloc.state.data.remove(model);
    } else {
      model.staffId = "0";
      model.staffName = tr("any_one");
      servicesBloc.state.data.add(model);
    }
    // companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
    //This is the last update here
    goToReservationScreen(context, detailsCubit.state.data!.id);
    // servicesBloc.onUpdateData(servicesBloc.state.data);
  }

  Future<void> getBusinessesDetails(BuildContext context, String id,
      {bool refresh = true}) async {
    var data =
        await GetServiceDetails().call(_serviceDetailsPrams(id, refresh));
    if (data != null) {
      detailsCubit.onUpdateData(data);
    }
  }

  ServiceDetailsPrams _serviceDetailsPrams(String id, bool refresh) =>
      ServiceDetailsPrams(id: id, refresh: refresh);

  Future<void> getServiceReviews(BuildContext context, String id,
      {bool refresh = true}) async {
    ServiceDetailsPrams params = _serviceDetailsPrams(id, refresh);
    var data = await GetServiceReviews()(params);
    if (data != null) {
      reviewsCubit.onUpdateData(data);
    }
  }

  Future<void> getCompanyServices(BuildContext context, String id,
      {bool refresh = true}) async {
    BusinessServicePrams params = _businessServicePrams(id, refresh);
    var data = await GetCompanyServices()(params);
    if (data.isNotEmpty) {
      data.first.selected = true;
      for (var element in data) {
        for (var service in element.servicesList) {
          _addDefaultStaff(service);
          // _checkSelectedServices(service);
        }
      }
    }
    companyServicesBloc.onUpdateData(data);
  }

  // void _checkSelectedServices(ServiceListModel service) {
  //   if (servicesBloc.state.data
  //       .any((ele) => ele.businessServiceID == service.businessServiceID)) {
  //     service.selected = true;
  //   }
  // }

  BusinessServicePrams _businessServicePrams(String id, bool refresh) =>
      BusinessServicePrams(id: id, loadImages: true, refresh: refresh);

  void _addDefaultStaff(ServiceListModel service) {
    if (service.staffDetail.isNotEmpty) {
      service.staffDetail = service.staffDetail.toSet().toList();
      service.staffDetail.sort((a, b) => a.days
          .where((day) => day.isDateAvailable)
          .length
          .compareTo(b.days.where((day) => day.isDateAvailable).length));
      service.staffDetail.insert(
        0,
        service.staffDetail.first.copyWith(
          staffNameArabic: tr("any_one"),
          staffNameEnglish: tr("any_one"),
          staffID: "0",
        ),
      );
    }
  }

  num getSelectedServicePrice() {
    var price =
        servicesBloc.state.data.fold(0, (num prev, e) => prev + e.price);
    return price;
  }

  updateServiceWish(BuildContext context, ServiceListModel model) async {
    var params = WishParams(
        serviceID: model.serviceId, businessServiceID: model.businessServiceID);
    if (model.hasWishItem) {
      getIt<LoadingHelper>().showLoadingDialog();
      var update = await RemoveWishService()(params);
      if (update) {
        _updateCubitToWish(context, model);
      }
    } else {
      getIt<LoadingHelper>().showLoadingDialog();
      var update = await UpdateWishService()(params);
      if (update) {
        _updateCubitToWish(context, model);
      }
    }
  }

  _updateCubitToWish(BuildContext context, ServiceListModel model) {
    model.hasWishItem = !model.hasWishItem;
    servicesBloc.onUpdateData(servicesBloc.state.data);
    AutoRouter.of(context).pop();
    getIt<LoadingHelper>().dismissDialog();
  }

  updateBusinessWish(
    BuildContext context,
    ServiceDetailsModel model,
    bool auth,
  ) async {
    if (auth) {
      getIt<LoadingHelper>().showLoadingDialog();
      if (model.hasWishItem == false) {
        var update = await UpdateWishBusiness()(model.id);
        if (update) {
          _updateWishView(model, true);
        }
      } else {
        var remove = await RemoveWishBusiness()(model.id);
        if (remove) {
          _updateWishView(model, false);
        }
      }
      getIt<LoadingHelper>().dismissDialog();
    } else {
      CustomToast.customAuthDialog();
    }
  }

  void _updateWishView(ServiceDetailsModel model, bool hasWish) {
    model.hasWishItem = hasWish;
    detailsCubit.onUpdateData(detailsCubit.state.data);
  }

  void shareSinglePage(
      BuildContext context, String id, String businessName) async {
    return getIt<Utilities>().shareSinglePage(context, id, businessName);
  }

  goToReservationScreen(BuildContext context, String businessId) {
    if (servicesBloc.state.data.length == 0) {
      CustomToast.showSimpleToast(
        msg: tr(
          "worn_to_select_service",
        ),
        title: tr("Warning_Toast"),
        type: ToastType.warning,
      );
      return;
    }
    for (var e in servicesBloc.state.data) {
      _checkServiceStaff(e);
      if (e.staffId.isEmpty) {
        CustomToast.showSimpleToast(
          msg: "${tr("select")} ${e.getServiceName()} ${tr("staff")}",
          title: tr("Warning_Toast"),
        );
        return;
      }
    }
    _navToReservation(context, businessId);
  }

  void _navToReservation(BuildContext context, String businessId) async {
    if (!servicesBloc.state.data
        .any((element) => element.staffDetail.isEmpty)) {
      await AutoRouter.of(context).push(
        ReservationRootRoute(
          businessID: businessId,
          serviceId:servicesBloc.state.data.first.serviceId,
          // services: servicesBloc.state.data,
        ),
      );
      servicesBloc.onUpdateData([]);
      companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
    }
  }

  void _checkServiceStaff(ServiceListModel e) {
    if (e.staffDetail.isEmpty) {
      CustomToast.showSimpleToast(
          msg:
              "${e.getServiceName()} ${tr("staff")} ${tr("empty_staff_alert")}",
          title: tr("Missing_reservation_staff"),
          type: ToastType.info);
      return;
    }
    if (e.staffId == "0" && e.staffDetail.isNotEmpty) {
      e.staffName = tr("any_one");
    }
  }

  showContactDialog(
    BuildContext context,
    ServiceDetailsModel serviceDetailsModel,
  ) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) {
        return BuildDetailsContactBottomSheet(
            serviceDetailsModel: serviceDetailsModel);
      },
    );
  }
}

enum DetailsViews { services, reviews, staff, details }
