part of 'product_details_imports.dart';

class ProductDetails extends StatefulWidget {
  final String id;

  const ProductDetails({Key? key, required this.id}) : super(key: key);

  @override
  State<ProductDetails> createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final ProductDetailsData productDetailsData = ProductDetailsData();

  @override
  void initState() {
    productDetailsData.fetchData(context, productDetailsData, widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return SafeArea(
      top: true,
      child: GradientScaffold(
        body: BlocBuilder<GenericBloc<ServiceDetailsModel?>,
                GenericState<ServiceDetailsModel?>>(
            bloc: productDetailsData.detailsCubit,
            builder: (context, state) {
              if (state is GenericUpdateState) {
                return CustomScrollView(
                  slivers: [
                    BuildSwiper(
                      data: productDetailsData,
                      model: device,
                      serviceDetailsModel: state.data!,
                    ),
                    BuildDetails(
                      model: device,
                      serviceDetailsModel: state.data!,
                    ),
                    SliverStickyHeader(
                      header: BuildTabs(data: productDetailsData),
                      sliver: SliverToBoxAdapter(
                        child: BlocBuilder<GenericBloc<DetailsViews>,
                            GenericState<DetailsViews>>(
                          bloc: productDetailsData.pagesCubit,
                          builder: (_, state) {
                            return AnimatedSwitcher(
                              duration: Duration(milliseconds: 200),
                              reverseDuration: Duration(milliseconds: 200),
                              child: productDetailsData.pages[state.data.index],
                              transitionBuilder: (child, animation) {
                                return FadeTransition(
                                  child: child,
                                  opacity: animation,
                                );
                              },
                            );
                          },
                        ),
                      ),
                    )
                  ],
                );
              }
              return BuildDetailsLoadingView(
                productDetailsData: productDetailsData,
              );
            }),
        bottomNavigationBar: BuildBottomProductDetails(
          data: productDetailsData,
          businessId: widget.id,
        ),
      ),
    );
  }
}
