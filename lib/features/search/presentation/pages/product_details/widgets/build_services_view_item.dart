part of 'product_details_widgets_imports.dart';

class BuildServicesViewItem extends StatelessWidget {
  final Function()? action;
  final Color? dividerColor;
  final ProductDetailsData productDetailsData;
  final DeviceModel deviceModel;
  final ServiceListModel serviceListModel;
  final int index;
  final int length;

  const BuildServicesViewItem(
      {Key? key,
      this.action,
      this.dividerColor,
      required this.productDetailsData,
      required this.deviceModel,
      required this.serviceListModel,
      required this.index,
      required this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final selected = _isSelected();
    final unSelected = _unSelected();
    return InkWell(
      onTap: () => productDetailsData.DetailsBottomSheet(
          context, productDetailsData, serviceListModel),
      child: Container(
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(13.r),
          border: Border.all(
            color: selected ? MyColors.primary : MyColors.white,
            width: 1.5,
          ),
        ),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(13, index ==0? 15 : 0, 13, 12).r,
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: serviceListModel.getServiceName(),
                        color: MyColors.primary,
                        fontFamily: CustomFonts.primarySemiBoldFont,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                      ),
                      SizedBox(height: 8.h),
                      Container(
                        width: MediaQuery.of(context).size.width * .65,
                        child: Row(
                          children: [
                            MyText(
                              title: getIt<Utilities>().convertNumToAr(
                                context: context,
                                value: "${serviceListModel.getCurrencyName()} ${serviceListModel.price.round()}",
                              ),
                              color: MyColors.black,
                              size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(width: 10.h),
                            Icon(
                              Icons.access_time,
                              color: MyColors.greyLight,
                              size: deviceModel.isTablet ? 12.sp : 15.sp,
                            ),
                            Row(
                              children: [
                                MyText(
                                  title: getIt<Utilities>().convertNumToAr(
                                    context: context,
                                    value: " ${serviceListModel.serviceDuration}",
                                  ),
                                  color: MyColors.blackOpacity,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                                ),
                                SizedBox(width: 2),
                                MyText(
                                  title: tr("min"),
                                  color: MyColors.blackOpacity,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                                ),
                              ],
                            ),
                            if (serviceListModel.getServiceGender().isNotEmpty)
                              Container(
                                margin: EdgeInsetsDirectional.only(
                                    end: 3, start: 20.w),
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: MyColors.greyLight,
                                ),
                              ),
                            Expanded(
                              child: MyText(
                                title: serviceListModel.getServiceGender(),
                                color: MyColors.blackOpacity,
                                size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  InkWell(
                    onTap: () => productDetailsData.onSelectService(
                        serviceListModel, context),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          height: 59.h,
                          child: Column(
                            children: [
                              CachedImage(
                                url: serviceListModel.picture.firstOrNull ??
                                    "".replaceAll("\\", "/"),
                                width: deviceModel.isTablet ? 35.w : 50.w,
                                height: deviceModel.isTablet ? 35.w : 50.w,
                                borderRadius: BorderRadius.circular(6.r),
                                fit: BoxFit.cover,
                                bgColor: MyColors.defaultImgBg,
                                placeHolder: ServicePlaceholder(),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 31.h,
                          width: 31.w,
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: selected ? MyColors.primary : MyColors.white,
                            border: Border.all(
                              color:
                                  selected ? MyColors.white : MyColors.primary,
                              width: selected ? 1.5 : 1,
                            ),
                          ),
                          child: Icon(
                            selected ? Icons.done : Icons.add,
                            color: selected ? MyColors.white : MyColors.primary,
                            size: deviceModel.isTablet ? 12.sp : 17.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: selected && serviceListModel.staffDetail.length != 0,
              child: Container(
                height: deviceModel.isTablet ? 65.h : 60.h,
                decoration:
                    BoxDecoration(color: MyColors.primary.withOpacity(.04)),
                child: BuildScrollableBodyOfItems(
                  productDetailsData: productDetailsData,
                  serviceListModel: serviceListModel,
                ),
              ),
            ),
            Visibility(
              visible: !selected && index != length - 1 && !unSelected,
              child: Divider(
                height: 0,
                thickness: selected ? 1 : .5,
                color: MyColors.reserveDateBg,
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool _unSelected() {
    return productDetailsData.servicesBloc.state.data.any((element) {
      return element.serviceId != serviceListModel.serviceId;
    });
  }

  bool _isSelected() {
    return productDetailsData.servicesBloc.state.data
        .any((element) => element.serviceId == serviceListModel.serviceId);
  }
}
