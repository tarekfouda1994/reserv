part of'product_details_widgets_imports.dart';

class BuildDetailsLoadingView extends StatelessWidget {
  final ProductDetailsData productDetailsData;
  const BuildDetailsLoadingView({Key? key, required this.productDetailsData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return ListView(
      children: [
        Stack(
          alignment: Alignment.topCenter,
          children: [
            BuildShimmerView(
              height: device.isTablet
                  ? MediaQuery.of(context).size.height * .3.h
                  : MediaQuery.of(context).size.height * .35.h,
            ),
            Padding(
              padding: EdgeInsets.all(25.h),
              child: Row(
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(100),
                    onTap: () => AutoRouter.of(context).pop(),
                    child: Container(
                      padding: const EdgeInsets.all(6),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: MyColors.primary.withOpacity(.5)),
                      child: Icon(
                          device.locale == Locale('en', 'US')
                              ? Icons.west
                              : Icons.east,
                          color: MyColors.white,
                          size: 19.sp),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        BuildShimmerView(height:40.h),
        BlocBuilder<GenericBloc<DetailsViews>,
            GenericState<DetailsViews>>(
          bloc: productDetailsData.pagesCubit,
          builder: (_, state) {
            return AnimatedSwitcher(
              duration: Duration(milliseconds: 200),
              reverseDuration: Duration(milliseconds: 200),
              child: productDetailsData.pages[state.data.index],
              transitionBuilder: (child, animation) {
                return FadeTransition(
                  child: child,
                  opacity: animation,
                );
              },
            );
          },
        ),
      ],
    );
  }
}
