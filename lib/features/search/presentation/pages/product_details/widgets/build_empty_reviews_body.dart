part of 'product_details_widgets_imports.dart';

class BuildEmptyReviewsBody extends StatelessWidget {
  const BuildEmptyReviewsBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      margin: const EdgeInsets.all(16).r,
      height: 300.sm,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.r),
        color: MyColors.white,
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(Res.emptyFavorites),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: MyText(
              title: tr("noReviews"),
              color: MyColors.grey,
              size: device.isTablet ? 7.sp : 11.sp,
            ),
          ),
        ],
      ),
    );
  }
}
