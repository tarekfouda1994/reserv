import 'package:auto_route/auto_route.dart';
import 'package:carousel_indicator/carousel_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/widgets/build_my_arrow_icon.dart';
import 'package:flutter_tdd/core/widgets/custom_card.dart';
import 'package:flutter_tdd/core/widgets/disable/disable_widget.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/business_placeholder.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/service_placeholder.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/staff_placeholder.dart';
import 'package:flutter_tdd/features/reservation/data/models/staff_model/staff_model.dart';
import 'package:flutter_tdd/features/search/data/models/service_details_model/service_details_model.dart';
import 'package:flutter_tdd/features/search/data/models/service_reviews_model/service_reviews_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/dashboard_details/widgets/dashboard_details_widget_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/product_details/product_details_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/bloc/device_cubit/device_cubit.dart';
import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/helpers/di.dart';
import '../../../../../../core/helpers/utilities.dart';
import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../../core/models/device_model/device_model.dart';
import '../../../../../../core/routes/router_imports.gr.dart';
import '../../../../../../core/widgets/build_page_error.dart';
import '../../../../../../core/widgets/build_shemer.dart';
import '../../../../../../res.dart';
import '../../../../../reservation/data/models/service_list_model/service_list_model.dart';
import '../../../../../reservation/data/models/service_types_model/service_types_model.dart';
import '../../../../../reservation/presentation/pages/select_services/widgets/select_services_widgets_imports.dart';
import '../../../../data/models/business_staff_model/business_staff_model.dart';
import '../../../../data/models/reviews_model/reviews_model.dart';
import '../../../widgets/widgets_imports.dart';

part 'Build_item_tabs.dart';
part 'Build_rate_view.dart';
part 'build_all_selected_services.dart';
part 'build_bottom_product_details.dart';
part 'build_bottom_service_view.dart';
part 'build_chat_icon.dart';
part 'build_contact_bottom_sheet.dart';
part 'build_details.dart';
part 'build_details_bottom_sheet.dart';
part 'build_details_loading_view.dart';
part 'build_details_time_details_view.dart';
part 'build_details_view.dart';
part 'build_empty_reviews_body.dart';
part 'build_header_details_view.dart';
part 'build_header_title.dart';
part 'build_product_details_empty_services.dart';
part 'build_rate_item_bar.dart';
part 'build_review_details.dart';
part 'build_reviews_view.dart';
part 'build_scrollable_body_of_item.dart';
part 'build_selected_services_item.dart';
part 'build_selected_staff.dart';
part 'build_service_staff_sheet.dart';
part 'build_services_sheet_body.dart';
part 'build_services_view.dart';
part 'build_services_view_item.dart';
part 'build_staff_item.dart';
part 'build_staff_view.dart';
part 'build_swiper.dart';
part 'build_swiper_bottom_sheet.dart';
part 'build_tabs.dart';
part 'build_unselected_staff.dart';
