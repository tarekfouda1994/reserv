part of 'product_details_widgets_imports.dart';

class BuildDetailsItemBottomSheet extends StatelessWidget {
  final ProductDetailsData productDetailsData;
  final ServiceListModel serviceListModel;

  const BuildDetailsItemBottomSheet(
      {Key? key,
      required this.productDetailsData,
      required this.serviceListModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: productDetailsData.servicesBloc,
        builder: (context, state) {
          return Container(
            height: device.isTablet
                ? MediaQuery.of(context).size.height * .35.h
                : MediaQuery.of(context).size.height * .55.h,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: ListView(
                        children: [
                          BuildSwiperBottomSheet(
                            productDetailsData: productDetailsData,
                            model: device,
                            pictures: serviceListModel.picture,
                          ),
                          BuildBottomSheetDetails(
                            model: device,
                            serviceListModel: serviceListModel,
                          ),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        productDetailsData.onSelectService(
                            serviceListModel, context);
                        AutoRouter.of(context).pop();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        height: device.isTablet ? 60.sm : 50.sm,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.all(device.isTablet ? 10.sp : 16),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.r),
                          color: MyColors.primary,
                        ),
                        child: Visibility(
                          visible: state.data
                              .any((element) => element == serviceListModel),
                          child: MyText(
                              alien: TextAlign.center,
                              size: device.isTablet ? 8.sp : 11.sp,
                              title: tr("remove_service"),
                              color: MyColors.white),
                          replacement: MyText(
                              alien: TextAlign.center,
                              size: device.isTablet ? 8.sp : 11.sp,
                              title: tr("add_service"),
                              color: MyColors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      BuildFavouriteIcon(
                        onTap: () => productDetailsData.updateServiceWish(
                            context, serviceListModel),
                        hasWish: serviceListModel.hasWishItem,
                      ),
                      InkWell(
                        borderRadius: BorderRadius.circular(100),
                        onTap: () => AutoRouter.of(context).pop(),
                        child: Container(
                          padding: const EdgeInsets.all(6),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: MyColors.black),
                          child: Icon(Icons.clear,
                              color: MyColors.white, size: 19.sp),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }
}
