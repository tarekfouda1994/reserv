part of 'product_details_widgets_imports.dart';

class BuildRateItemBar extends StatelessWidget {
  final ReviewsModel model;
  final int value;

  const BuildRateItemBar({Key? key, required this.model, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Row(
      children: [
        MyText(title: "$value", color: MyColors.grey, size: device.isTablet ? 7.sp : 11.sp),
        Expanded(
          child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: device.isTablet ? 3.sp : 5.sp,
                  vertical: device.isTablet ? 4.sp : 5.sp),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.r),
                child: LinearProgressIndicator(
                  value: model.ratingSummary.firstWhereOrNull((e) => e.key == value)?.value ?? 0,
                  color: MyColors.amber,
                  minHeight: device.isTablet ? 4.w : 4.h,
                  backgroundColor: MyColors.primary.withOpacity(.08),
                ),
              )),
        ),
        MyText(
            title: "${model.companyItemReviews.where((e) => e.rating.toInt() == value).length}",
            color: MyColors.grey,
            size: device.isTablet ? 7.sp : 11.sp),
      ],
    );
  }
}
