part of "product_details_widgets_imports.dart";

class BuildSwiper extends StatelessWidget {
  final ProductDetailsData data;
  final ServiceDetailsModel serviceDetailsModel;
  final DeviceModel model;

  const BuildSwiper(
      {Key? key,
      required this.data,
      required this.model,
      required this.serviceDetailsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: data.swiperCubit,
      builder: (cxt, state) {
        return SliverToBoxAdapter(
          child: Stack(
            children: [
              if (serviceDetailsModel.pictures.isEmpty)
                CachedImage(
                  height: model.isTablet
                      ? MediaQuery.of(context).size.height * .245.sm
                      : MediaQuery.of(context).size.height * .245.h,
                  width: MediaQuery.of(context).size.width,
                  url:"",
                  fit: BoxFit.cover,
                  bgColor: MyColors.defaultImgBg,
                  placeHolder: BusinessPlaceholder(),
                  alignment: Alignment.bottomCenter,
                ),
              CarouselSlider(
                options: CarouselOptions(
                    onPageChanged: (index, carousel) {
                      data.swiperCubit.onUpdateData(index);
                    },
                    autoPlay: serviceDetailsModel.pictures.length > 1,
                    height: model.isTablet
                        ? MediaQuery.of(context).size.height * .3.sm
                        : MediaQuery.of(context).size.height * .245.h,
                    autoPlayAnimationDuration: Duration(milliseconds: 5000),
                    viewportFraction: 1),
                items:
                    List.generate(serviceDetailsModel.pictures.length, (index) {
                  return CachedImage(
                    height: model.isTablet
                        ? MediaQuery.of(context).size.height * .3.sm
                        : MediaQuery.of(context).size.height * .26.h,
                    width: MediaQuery.of(context).size.width,
                    url: serviceDetailsModel.pictures[index]
                        .replaceAll("\\", "/"),
                    fit: BoxFit.cover,
                    bgColor: MyColors.defaultImgBg,
                    placeHolder: BusinessPlaceholder(),
                    alignment: Alignment.bottomCenter,
                  );
                }),
              ),
              Container(
                height: model.isTablet
                    ? MediaQuery.of(context).size.height * .3.sm
                    : MediaQuery.of(context).size.height * .245.h,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        InkWell(
                          borderRadius: BorderRadius.circular(100),
                          onTap: () => AutoRouter.of(context).pop(),
                          child: Container(
                            padding: const EdgeInsets.all(6),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: MyColors.white,width: 1),
                                color: Color(0xffABC8C8)),
                            child: Icon(
                              model.locale == Locale('en', 'US')
                                  ? Icons.west
                                  : Icons.east,
                              color: MyColors.white,
                              size: model.isTablet ? 12.sp : 18.sp,
                            ),
                          ),
                        ),
                        Spacer(),
                        InkWell(
                            onTap: () => data.shareSinglePage(
                                context, serviceDetailsModel.id,serviceDetailsModel.getBusinessName()),
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              width: 30.r,
                              height: 30.r,
                              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                              decoration: BoxDecoration(
                                  border: Border.all(color: MyColors.white),
                                  shape: BoxShape.circle,
                                  color:  MyColors.favouriteBg),
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.share_outlined,
                                size: model.isTablet ? 12.sp : 17.sp,
                                color: MyColors.white,
                              ),
                            ),),
                        BuildFavouriteIcon(
                            onTap: () => data.updateBusinessWish(
                                context, serviceDetailsModel, model.auth),
                            hasWish:
                            serviceDetailsModel.hasWishItem),
                      ],
                    ),
                    if (serviceDetailsModel.pictures.length!=1)
                      CarouselIndicator(
                        color: MyColors.white.withOpacity(.8),
                        activeColor: MyColors.primary,
                        count: serviceDetailsModel.pictures.length,
                        index: state.data,
                      ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
