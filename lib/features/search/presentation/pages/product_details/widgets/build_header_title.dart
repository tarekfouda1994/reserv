part of 'product_details_widgets_imports.dart';

class BuildHeaderTitle extends StatelessWidget {
  final String title;
  final int count;

  const BuildHeaderTitle({Key? key, required this.title, required this.count}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Container(
      padding: const EdgeInsets.only(bottom: 12, top: 24).r,
      margin: EdgeInsets.symmetric(horizontal: 14).r,
      child: Row(children: [
        RichText(
          textScaleFactor: 1.2,
          text: TextSpan(
            text: title,
            style: TextStyle(
              color: MyColors.black,
              fontFamily: CustomFonts.primaryFont,
              fontSize: device.isTablet ? 8.5.sp : 13.5.sp,
              fontWeight: FontWeight.bold,
            ),
            children: [
              TextSpan(
                text: " ",
              ),
              if (count > 0)
                TextSpan(
                  text: "(${getIt<Utilities>().convertNumToAr(context: context, value: "$count")})",
                  style: TextStyle(
                    color: MyColors.black,
                    fontFamily: CustomFonts.primaryFont,
                    fontSize: device.isTablet ? 8.5.sp : 13.5.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
            ],
          )
        ),
      ]),
    );
  }
}
