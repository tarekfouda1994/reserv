part of 'product_details_widgets_imports.dart';

class BuildDetails extends StatelessWidget {
  final DeviceModel model;
  final ServiceDetailsModel serviceDetailsModel;

  const BuildDetails({Key? key, required this.model, required this.serviceDetailsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        margin: EdgeInsets.only(bottom: 3.h),
        padding: const EdgeInsets.all(16),
        color: MyColors.white,
        child: Column(children: [
          Row(
            children: [
              SvgPicture.asset(Res.building, height: 18.r, width: 18.r),
              MyText(
                title: " ${serviceDetailsModel.getBusinessName()}",
                color: MyColors.black,
                size: model.isTablet ? 9.sp : 13.sp,
                fontFamily: CustomFonts.primarySemiBoldFont,
              ),
            ],
          ),
          SizedBox(height: 15.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Offstage(
                    offstage: serviceDetailsModel.rating == 0,
                    child: Row(
                      children: [
                        MyText(
                          title: getIt<Utilities>().convertNumToAr(
                            context: context,
                            value: "${serviceDetailsModel.rating.toString()} ",
                          ),
                          color: MyColors.amber,
                          size: model.isTablet ? 6.sp : 9.sp,
                        ),
                        Icon(Icons.star,
                            color: MyColors.amber, size: model.isTablet ? 12.sp : 16.sp),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsetsDirectional.only(end: 3, top: 2),
                        padding: EdgeInsets.all(4),
                        decoration:
                            BoxDecoration(shape: BoxShape.circle, color: MyColors.greyLight),
                      ),
                      MyText(
                        title: serviceDetailsModel.getBusinessGender(),
                        color: MyColors.grey,
                        size: model.isTablet ? 5.5.sp : 9.sp,
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () => getIt<Utilities>().trackingBusinessOnMap(
                      serviceDetailsModel.latitude,
                      serviceDetailsModel.longitude,
                      serviceDetailsModel.getBusinessName(),
                      serviceDetailsModel.getAddressName(),
                    ),
                    borderRadius: BorderRadius.circular(10.r),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SvgPicture.asset(Res.location_arrow_icon,
                            width: model.isTablet ? 5.5.w : null),
                        SizedBox(width: 3),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Offstage(
                              offstage: serviceDetailsModel.serviceDistance.isEmpty,
                              child: MyText(
                                title: serviceDetailsModel.serviceDistance,
                                color: MyColors.grey,
                                size: model.isTablet ? 5.5.sp : 9.sp,
                              ),
                            ),
                            MyText(
                                decoration: TextDecoration.underline,
                                title: tr("See_Direction"),
                                color: MyColors.blue,
                                size: model.isTablet ? 5.5.sp : 9.sp),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Icon(Icons.access_time_outlined,
                      color: MyColors.grey.withOpacity(.7), size: model.isTablet ? 12.sp : 16.sp),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                          title: tr("open"),
                          color: MyColors.primary,
                          size: model.isTablet ? 5.sp : 9.sp),
                      MyText(
                          title: getIt<Utilities>().convertNumToAr(
                            context: context,
                            value: " ${serviceDetailsModel.todayTiming}",
                          ),
                          color: MyColors.black,
                          size: model.isTablet ? 5.sp : 9.sp)
                    ],
                  ),
                ],
              )
            ],
          ),
        ]),
      ),
    );
  }
}
