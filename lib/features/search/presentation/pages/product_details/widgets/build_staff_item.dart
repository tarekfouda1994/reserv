part of 'product_details_widgets_imports.dart';

class BuildStaffItem extends StatelessWidget {
  final DeviceModel model;
  final BusinessStaffModel businessStaffModel;

  const BuildStaffItem(
      {Key? key, required this.model, required this.businessStaffModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CachedImage(
                url: businessStaffModel.avatar.replaceAll("\\", "/"),
                width: model.isTablet ? 30.w : 40.w,
                height: model.isTablet ? 30.h : 40.h,
                boxShape: BoxShape.circle,
                haveRadius: false,
                fit: BoxFit.cover,
                bgColor: MyColors.defaultImgBg,
                placeHolder: StaffPlaceholder(),
              ),
              SizedBox(width: 10.w),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    MyText(
                      title:
                          "${businessStaffModel.getFirstName()} ${businessStaffModel.getLastName()}",
                      color: MyColors.black,
                      size: model.isTablet ? 7.sp : 11.sp,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      children: [
                        Icon(Icons.star, color: MyColors.primaryDark, size: 14),
                        MyText(
                          title: businessStaffModel.rating.toString(),
                          color: MyColors.primaryDark,

                          size: model.isTablet ? 5.sp : 8.sp,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: MyText(
                title: businessStaffModel.getJopTitle(),
                color: MyColors.black,

                size: model.isTablet ? 6.sp : 9.5.sp),
          ),
          InkWell(
            onTap: () => AutoRouter.of(context).push(ReservationRootRoute(
                businessID: businessStaffModel.businessID)),
            borderRadius: BorderRadius.circular(10.r),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.r),
                  color: MyColors.primaryDark.withOpacity(.03)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyText(
                      title:
                          "${tr("reserveWith")} ${businessStaffModel.getFirstName()}  ",
                      color: MyColors.primaryDark,
                      size: model.isTablet ? 7.sp : 11.sp),
                  Icon(
                    model.locale.languageCode == "en" ? Icons.east : Icons.west,
                    color: MyColors.primary,
                    size: 17,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
