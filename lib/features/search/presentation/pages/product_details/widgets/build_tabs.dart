part of 'product_details_widgets_imports.dart';

class BuildTabs extends StatelessWidget {
  final ProductDetailsData data;

  const BuildTabs({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: data.tabsCubit,
      builder: (cxt, state) {
        return Container(
          color: MyColors.white,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal:10).r,
                child: Row(children: [
                  BuildItemTabs(
                    val: 1,
                    changeVal: state.data,
                    title: tr("services"),
                    onTap: () {
                      data.pagesCubit.onUpdateData(DetailsViews.services);
                      data.tabsCubit.onUpdateData(1);
                    },
                  ),
                  SizedBox(width: 5),
                  BuildItemTabs(
                      val: 2,
                      changeVal: state.data,
                      title: tr("reviews"),
                      onTap: () {
                        data.pagesCubit.onUpdateData(DetailsViews.reviews);
                        data.tabsCubit.onUpdateData(2);
                      }),
                    SizedBox(width: 5),
                  BuildItemTabs(
                      val: 3,
                      changeVal: state.data,
                      title: tr("staff"),
                      onTap: () {
                        data.pagesCubit.onUpdateData(DetailsViews.staff);
                        data.tabsCubit.onUpdateData(3);
                      }),
                  SizedBox(width: 5),
                  BuildItemTabs(
                      val:4,
                      changeVal: state.data,
                      title: tr("details"),
                      onTap: () {
                        data.pagesCubit.onUpdateData(DetailsViews.details);
                        data.tabsCubit.onUpdateData(4);
                      }),
                ]),
              ),
            ],
          ),
        );
      },
    );
  }
}
