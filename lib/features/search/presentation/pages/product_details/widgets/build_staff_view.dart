part of 'product_details_widgets_imports.dart';

class StaffView extends StatelessWidget {
  final ProductDetailsData productDetailsData;
  final String businessId;

  const StaffView(
      {Key? key, required this.productDetailsData, required this.businessId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return RefreshIndicator(
      color: MyColors.primary,
      onRefresh: () =>
          productDetailsData.ficheBusinessStaff(context, 1, businessId),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: BuildHeaderTitle(
              title: tr("staff"),
              count: productDetailsData.pagingController.itemList?.length ?? 0,
            ),
          ),
          PagedListView<int, BusinessStaffModel>(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            pagingController: productDetailsData.pagingController,
            builderDelegate: PagedChildBuilderDelegate<BusinessStaffModel>(
                firstPageProgressIndicatorBuilder: (context) => Column(
                        children: List.generate(
                      5,
                      (index) {
                        return BuildShimmerView(
                            height: device.isTablet ? 120.h : 100.h,
                            width: MediaQuery.of(context).size.width * .9);
                      },
                    )),
                itemBuilder: (context, item, index) {
                  return BuildStaffItem(
                      model: device, businessStaffModel: item);
                },
                newPageProgressIndicatorBuilder: (con) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CupertinoActivityIndicator()),
                firstPageErrorIndicatorBuilder: (context) => BuildPageError(
                    onTap: () =>
                        productDetailsData.pagingController.refresh())),
          ),
        ],
      ),
    );
  }
}
