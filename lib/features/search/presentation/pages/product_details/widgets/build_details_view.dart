part of 'product_details_widgets_imports.dart';

class DetailsView extends StatelessWidget {
  final ProductDetailsData data;

  const DetailsView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return BlocBuilder<GenericBloc<ServiceDetailsModel?>,
            GenericState<ServiceDetailsModel?>>(
        bloc: data.detailsCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BuildHeaderDetailsView(
                    model: device,
                    serviceDetailsModel: state.data!,
                    productDetailsData: data),
                BuildDetailsTime(
                    model: device,
                    data: data,
                    serviceDetailsModel: state.data!),
              ],
            );
          }
          return Column(
            children: [
              BuildShimmerView(
                  width: MediaQuery.of(context).size.width * .8,
                  height: device.isTablet
                      ? MediaQuery.of(context).size.height * .1.h
                      : MediaQuery.of(context).size.height * .16.h),
              Divider(height: 30.h),
              BuildShimmerView(
                  width: MediaQuery.of(context).size.width * .8,
                  height: device.isTablet
                      ? MediaQuery.of(context).size.height * .1.h
                      : MediaQuery.of(context).size.height * .16.h)
            ],
          );
        });
  }
}
