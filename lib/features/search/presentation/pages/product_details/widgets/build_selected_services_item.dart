part of 'product_details_widgets_imports.dart';

class BuildSelectedServicesItem extends StatelessWidget {
  final void Function() onRemove;
  final ServiceListModel serviceListModel;
  final GenericBloc<List<ServiceListModel>> bloc;

  const BuildSelectedServicesItem({
    Key? key,
    required this.serviceListModel,
    required this.onRemove,
    required this.bloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 24, left: 10, right: 10),
          color: MyColors.white,
          child: Row(
            children: [
                CachedImage(
                  url: serviceListModel.picture.firstOrNull?.replaceAll("\\", "/")??"",
                  width: deviceModel.isTablet ? 75 : 70,
                  height: deviceModel.isTablet ? 75 : 70,
                  borderRadius: BorderRadius.circular(6.r),
                  fit: BoxFit.cover,
                  bgColor: MyColors.defaultImgBg,
                  placeHolder: ServicePlaceholder(),
                ),
              SizedBox(width: 10.w),
              Expanded(
                child: SizedBox(
                  height: deviceModel.isTablet ? 75 : 70,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MyText(
                        title: serviceListModel.getServiceName(),
                        color: MyColors.primary,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                      ),
                      Row(
                        children: [
                          MyText(
                            title:
                                "${serviceListModel.getCurrencyName()} ${serviceListModel.price}",
                            color: MyColors.black,
                            fontFamily: CustomFonts.primarySemiBoldFont,
                            size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(width: 12.5),
                          Row(
                            children: [
                              Icon(
                                Icons.access_time,
                                color: MyColors.grey,
                                size: deviceModel.isTablet ? 12.sp : 15.sp,
                              ),
                              MyText(
                                title:
                                    " " + serviceListModel.serviceDurationText,
                                color: MyColors.darkGrey,
                                size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          serviceListModel.staffName == tr("any_one") ||
                                  serviceListModel.staffName.isEmpty
                              ? SvgPicture.asset(Res.defaultProfile,
                                  height: 24, width: 24, fit: BoxFit.cover)
                              : CachedImage(
                                  height: 24,
                                  width: 24,
                                  url: _selectedStaffImage(),
                                  boxShape: BoxShape.circle,
                                  haveRadius: false,
                                ),
                          SizedBox(width: 3.5),
                          MyText(
                            title: serviceListModel.staffName.isNotEmpty
                                ? serviceListModel.staffName
                                : tr("any_one"),
                            color: MyColors.black,
                            size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                          ),
                          SizedBox(width: 16),
                          InkWell(
                            onTap: () => _onChangeStaff(context),
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  Res.refresh,
                                  width: 11,
                                  height: 11,
                                  color: MyColors.darkGrey,
                                ),
                                SizedBox(width: 3.5),
                                MyText(
                                  title: tr("change"),
                                  color: MyColors.darkGrey,
                                  fontWeight: FontWeight.w400,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  onRemove();
                },
                child: Container(
                  padding: EdgeInsets.all(8.r),
                  child: SvgPicture.asset(
                    Res.trash,
                    color: MyColors.black,
                    height: 20,
                    width: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
        // Visibility(
        //   visible: serviceListModel.staffDetail.length != 0,
        //   child: Container(
        //     height: deviceModel.isTablet ? 65.h : 70,
        //     padding: const EdgeInsets.symmetric(vertical: 5).r,
        //     decoration: BoxDecoration(
        //       color: MyColors.primary.withOpacity(.04),
        //     ),
        //     child: BuildScrollableBodyOfItems(
        //       productDetailsData: productDetailsData,
        //       serviceListModel: serviceListModel,
        //     ),
        //   ),
        // ),
        SizedBox(height: 24),
      ],
    );
  }

  String _selectedStaffImage() {
    return serviceListModel.staffDetail
        .firstWhere((element) => element.staffID == serviceListModel.staffId)
        .avatar;
  }

  void _onChangeStaff(BuildContext context) {
    final widget = BuildServiceStaffSheet(
      serviceListModel: serviceListModel,
      bloc: bloc,
    );
    _bottomSheet(context, widget);
  }

  void _bottomSheet(BuildContext context, Widget widget) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) => widget,
    );
  }
}
