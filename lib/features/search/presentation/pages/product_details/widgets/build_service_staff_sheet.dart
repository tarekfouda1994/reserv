part of 'product_details_widgets_imports.dart';

class BuildServiceStaffSheet extends StatelessWidget {
  final ServiceListModel serviceListModel;
  final GenericBloc<List<ServiceListModel>> bloc;

  const BuildServiceStaffSheet({
    Key? key,
    required this.serviceListModel,
    required this.bloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    final staffList = serviceListModel.staffDetail;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: bloc,
        builder: (context, state) {
          return BuildServicesSheetBody(
            title: tr("selectStaff"),
            sheetHeight: _sheetHeight(context),
            children: [
              Flexible(
                child: ListView.separated(
                  itemCount: staffList.length,
                  itemBuilder: (cxt, index) {
                    final item = staffList[index];
                    final isSelected = item.staffID == serviceListModel.staffId;
                    return GestureDetector(
                      onTap: () => _addStaff(context, index),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 16,
                        ).r,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6).r,
                          color:
                              isSelected ? Color(0xffD2F1F1) : MyColors.white,
                          border: Border.all(
                            color: isSelected
                                ? MyColors.primary
                                : Color(0xffF2F2F0),
                          ),
                        ),
                        child: Row(
                          children: [
                            Visibility(
                              visible:
                                  item.avatar.isNotEmpty && item.staffID != "0",
                              child: CachedImage(
                                url: item.avatar.replaceAll("\\", "/"),
                                width: 36.h,
                                height: 36.h,
                                borderColor: MyColors.grey.withOpacity(.5),
                                placeHolder: StaffPlaceholder(),
                                bgColor: MyColors.defaultImgBg,
                                haveRadius: false,
                                boxShape: BoxShape.circle,
                                // boxShape: BoxShape.circle,
                                fit: BoxFit.cover,
                              ),
                              replacement: SvgPicture.asset(
                                Res.defaultProfile,
                                width: isTablet ? 65.sm : 36.sm,
                                height: isTablet ? 65.sm : 36.sm,
                              ),
                            ),
                            SizedBox(width: 10.w),
                            Expanded(
                              child: SizedBox(
                                height: isTablet ? 65.sm : 36.sm,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      item.getStaffName() != tr("any_one")
                                          ? MainAxisAlignment.spaceBetween
                                          : MainAxisAlignment.center,
                                  children: [
                                    MyText(
                                      title: item.getStaffName(),
                                      color: MyColors.black,
                                      size: isTablet ? 6.sp : 9.sp,
                                    ),
                                    if (item.getStaffName() != tr("any_one"))
                                      Row(
                                        children: [
                                          SvgPicture.asset(
                                            Res.star,
                                            color: MyColors.amber,
                                            height: 12.sp,
                                            width: 12.sp,
                                          ),
                                          MyText(
                                            title: " ${item.rating}",
                                            color: MyColors.amber,
                                            size: isTablet ? 7.sp : 8.sp,
                                          ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                            ),
                            if(item.getStaffName() != tr("any_one"))
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                MyText(
                                  title: tr("available_days"),
                                  color: MyColors.blackOpacity,
                                  size: isTablet ? 5.sp : 8.sp,
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children: List.generate(
                                      serviceListModel.staffDetail[index].days
                                          .length, (dayIndex) {
                                    bool isAvailable = serviceListModel
                                        .staffDetail[index]
                                        .days[dayIndex]
                                        .isDateAvailable;
                                    return Container(
                                      padding: EdgeInsets.all(4.sp),
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 2.w),
                                      decoration: BoxDecoration(
                                        color: isAvailable
                                            ? Color(0xffD2F1F1)
                                            : Color(0xffF3F3F3),
                                        shape: BoxShape.circle,
                                      ),
                                      child: MyText(
                                        title: serviceListModel
                                            .staffDetail[index]
                                            .days[dayIndex]
                                            .day,
                                        color: isAvailable
                                            ? MyColors.primary
                                            : MyColors.blackOpacity,
                                        size: isTablet ? 6.sp : 8.sp,
                                      ),
                                    );
                                  }),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(height: 6);
                  },
                ),
              ),
            ],
          );
        });
  }

  void _addStaff(BuildContext context, int index) {
    var data = bloc.state.data.map((e) {
      if (e.businessServiceID == serviceListModel.businessServiceID) {
        e.staffId = serviceListModel.staffDetail[index].staffID;
        e.staffName = serviceListModel.staffDetail[index].getStaffName();
      }
      return e;
    }).toList();
    bloc.onUpdateData(data);
    Navigator.pop(context);
  }

  double _sheetHeight(BuildContext context) {
    final staff = serviceListModel.staffDetail;
    bool isTablet = context.read<DeviceCubit>().state.model.isTablet;
    return staff.length > 7
        ? MediaQuery.of(context).size.height * .7
        : (isTablet ? staff.length * 120.sm : staff.length * 70.h) + 90.h;
  }
}
