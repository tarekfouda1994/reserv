part of 'product_details_widgets_imports.dart';

class BuildUnselectedStaff extends StatelessWidget {
  final ProductDetailsData productDetailsData;
  final DeviceModel model;
  final int index;
  final ServiceListModel serviceListModel;

  const BuildUnselectedStaff(
      {Key? key,
      required this.productDetailsData,
      required this.index,
      required this.model,
      required this.serviceListModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => productDetailsData.addStaff(serviceListModel, index),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.h, horizontal: 3.w),
        padding: EdgeInsets.symmetric(
            vertical: model.isTablet ? 2.h : 5.h, horizontal: 8.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.r), color: MyColors.white),
        child: Row(
          children: [
            Visibility(
              visible: serviceListModel.staffDetail[index].avatar.isNotEmpty &&
                  serviceListModel.staffDetail[index].staffID != "0",
              child: CachedImage(
                url: serviceListModel.staffDetail[index].avatar
                    .replaceAll("\\", "/"),
                width: 30.w,
                height: 30.h,
                borderColor: MyColors.grey.withOpacity(.5),
                placeHolder: StaffPlaceholder(),
                bgColor: MyColors.defaultImgBg,
                haveRadius: false,
                boxShape: BoxShape.circle,
                // boxShape: BoxShape.circle,
                fit: BoxFit.cover,
              ),
              replacement: SvgPicture.asset(Res.defaultProfile,
                  width: 30.w, height: 30.h),
            ),
            SizedBox(
              width: 10.w,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(
                    title: serviceListModel.staffDetail[index].getStaffName(),
                    color: MyColors.black,

                    size: model.isTablet ? 6.sp : 9.sp),
                Visibility(
                  visible: serviceListModel.staffDetail[index].getStaffName()!=tr("any_one"),
                  child: Row(
                    children: [
                      SvgPicture.asset(Res.star,
                          color: MyColors.primary, height: 12.sp, width: 12.sp),
                      MyText(
                          title: " ${serviceListModel.staffDetail[index].rating}",
                          color: MyColors.primary,

                          size: model.isTablet ? 7.sp : 8.sp),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
