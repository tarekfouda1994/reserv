part of 'product_details_widgets_imports.dart';

class BuildServicesSheetBody extends StatelessWidget {
  final double sheetHeight;
  final String? title;
  final List<Widget> children;

  const BuildServicesSheetBody({
    Key? key,
    required this.sheetHeight,
    required this.children,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10.r),
          ),
        ),
        height: sheetHeight,
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 14).r,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              MyText(
                title: title ?? tr("selected_services"),
                color: MyColors.black,
                size: deviceModel.isTablet ? 8.sp : 12.sp,
                fontWeight: FontWeight.bold,
              ),
              InkWell(
                onTap: () => AutoRouter.of(context).pop(),
                child: BuildIconItem(
                  icon: Icons.clear,
                  backGroundColor: MyColors.black,
                  iconColor: MyColors.white,
                ),
              )
            ]),
            const SizedBox(height: 20),
            ...children,
          ],
        ),
      ),
    );
  }
}
