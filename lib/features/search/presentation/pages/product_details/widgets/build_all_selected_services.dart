part of 'product_details_widgets_imports.dart';

class BuildSelectedServicesSheet extends StatelessWidget {
  final GenericBloc<List<ServiceListModel>> bloc;
  final void Function(int index) onRemoveItem;
  final void Function() onDeleteAll;

  const BuildSelectedServicesSheet({
    Key? key,
    required this.bloc,
    required this.onRemoveItem,
    required this.onDeleteAll,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: bloc,
        builder: (context, state) {
          return BuildServicesSheetBody(
              sheetHeight: _sheetHeight(state, context),
              children: [
                Flexible(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.r),
                      border: Border.all(color: MyColors.reserveDateBg),
                    ),
                    child: ListView.separated(
                      itemBuilder: (BuildContext context, int index) {
                        return BuildSelectedServicesItem(
                          bloc: bloc,
                          serviceListModel: state.data[index],
                          onRemove: () => onRemoveItem(index),
                        );
                      },
                      itemCount: state.data.length,
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(
                          height: 0,
                          thickness: .5,
                          color: MyColors.grey.withOpacity(.8),
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(height: 24),
                DefaultButton(
                  borderColor: MyColors.reserveDateBg,
                  height: deviceModel.isTablet ? 46.sm : 36.sm,
                  borderRadius: BorderRadius.circular(50.sp),
                  margin: EdgeInsets.zero,
                  color: MyColors.white,
                  textColor: MyColors.errorColor,
                  fontSize: deviceModel.isTablet ? 7.sp : 11.sp,
                  title: tr("deleteAllServices"),
                  onTap: () {
                    onDeleteAll();
                  },
                ),
                SizedBox(height: 12),
                DefaultButton(
                  borderColor: Colors.transparent,
                  height: deviceModel.isTablet ? 46.sm : 36.sm,
                  borderRadius: BorderRadius.circular(50.sp),
                  margin: EdgeInsets.zero,
                  color: MyColors.black,
                  textColor: MyColors.white,
                  fontSize: deviceModel.isTablet ? 7.sp : 11.sp,
                  title: tr("keepServices"),
                  onTap: () => AutoRouter.of(context).pop(),
                ),
              ]);
        });
  }

  double _sheetHeight(
      GenericState<List<ServiceListModel>> state, BuildContext context) {
    bool isTablet = context.read<DeviceCubit>().state.model.isTablet;
    return state.data.length > 4
        ? MediaQuery.of(context).size.height * .9
        : (isTablet ? state.data.length * 107.sm : state.data.length * 105.h) +
            180.h;
  }
}
