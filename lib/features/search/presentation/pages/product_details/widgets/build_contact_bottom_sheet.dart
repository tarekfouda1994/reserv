part of 'product_details_widgets_imports.dart';

class BuildDetailsContactBottomSheet extends StatelessWidget {
  final ServiceDetailsModel serviceDetailsModel;

  const BuildDetailsContactBottomSheet(
      {Key? key, required this.serviceDetailsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                MyText(
                  title: "Contact",
                  color: MyColors.black,
                  size: device.isTablet ? 8.5.sp : 13.5.sp,
                  fontWeight: FontWeight.bold,
                ),
                Spacer(),
                InkWell(
                  onTap: () => AutoRouter.of(context).pop(),
                  child: BuildIconItem(
                    icon: Icons.clear,
                    backGroundColor: MyColors.black,
                    iconColor: MyColors.white,
                  ),
                )
              ],
            ),
            BuildChatIcon(),
            Divider(height: 0),
            BuildContactItem(
              deviceModel: device,
              onTap: () => getIt<Utilities>().callPhone(
                phone: "${_checkPlus()}${serviceDetailsModel.contactNumber}",
              ),
              title: tr("Call_Us"),
              icon: Transform.scale(
                scale: .8,
                child: SvgPicture.asset(Res.call_outgoing_no_background,
                    color: MyColors.black,
                    width: device.isTablet ? 20.sp : 21.h,
                    height: device.isTablet ? 20.sp : 21.h),
              ),
              subTitle: "${_checkPlus()}${serviceDetailsModel.contactNumber}",
            ),
            Divider(height: 0),
            BuildContactItem(
              deviceModel: device,
              onTap: () => getIt<Utilities>().launchWhatsApp(
                "${_checkPlus()}${serviceDetailsModel.contactNumber}",
              ),
              title: tr("Whatsapp"),
              backGroundColor: Colors.green.withOpacity(.9),
              icon: Image.asset(
                Res.whatsapp,
                width: (device.isTablet ? 15.sp : 19.sp),
                height: (device.isTablet ? 15.sp : 19.sp),
                color: MyColors.white,
              ),
              subTitle: tr("Chat_With_Us"),
            )
          ]),
    );
  }

  String _checkPlus() =>
      serviceDetailsModel.contactNumber.contains("+") ? "" : "+";
}
