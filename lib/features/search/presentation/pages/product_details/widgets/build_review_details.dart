part of 'product_details_widgets_imports.dart';

class BuildReviewDetails extends StatelessWidget {
  final ServiceReviewsModel model;

  const BuildReviewDetails({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return CustomCard(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  CachedImage(
                    url: model.picture.replaceAll("\\", "/"),
                    width: device.isTablet ? 35.w : 50.w,
                    height: device.isTablet ? 35.w : 50.w,
                    borderRadius: BorderRadius.circular(12.r),
                    placeHolder: StaffPlaceholder(),
                    bgColor: MyColors.defaultImgBg,
                  ),
                  SizedBox(width: 10.w),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          title: model.getUserName(),
                          color: MyColors.black,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          children: [
                            Icon(Icons.star, color: MyColors.primaryDark, size: 12),
                            MyText(
                              title: model.rating.toString(),
                              color: MyColors.primaryDark,
                              size: device.isTablet ? 4.sp : 8.sp,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.access_time,
                        color: MyColors.grey,
                        size: device.isTablet ? 14.sp : 17.sp,
                      ),
                      MyText(
                        title:
                            " ${DateFormat("dd MMM yyyy",device.locale.languageCode).format(model.remarksDate)}",
                        color: MyColors.grey,
                        size: device.isTablet ? 6.sp : 9.sp,
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 10.h,
              ),
              MyText(
                  title: model.remarks,
                  color: MyColors.black,
                  size: device.isTablet ? 6.sp : 9.sp),
            ],
          ),
        ),
        Divider(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  MyText(
                      title: "${tr("service")}: ",
                      color: MyColors.grey,
                      size: device.isTablet ? 6.sp : 9.sp,
                  ),
                  SizedBox(width: 34),
                  Expanded(
                    child: MyText(
                        title: model.getServiceName(),
                        color: MyColors.black,
                        size: device.isTablet ? 6.sp : 9.sp),
                  ),
                ],
              ),

              Row(children: [
                MyText(
                    title: "${tr("servicedBy")}: ",
                    color: MyColors.grey,
                    size: device.isTablet ? 6.sp : 9.sp),
                SizedBox(width: 10),
                CachedImage(
                url: model.picture.replaceAll("\\", "/"),
                width: device.isTablet ? 20.r : 25.r,
                height: device.isTablet ? 20.r : 25.r,
                boxShape: BoxShape.circle,
                haveRadius: false,
                placeHolder: StaffPlaceholder(),
                bgColor: MyColors.defaultImgBg,
                fit: BoxFit.cover,
              ),
                MyText(
                    title: " ${model.getUserName()}",
                    color: MyColors.black,
                    size: device.isTablet ? 6.sp : 9.sp),],),
            ],
          ),
        ),
      ]),
    );
  }
}
