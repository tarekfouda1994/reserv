part of 'product_details_widgets_imports.dart';

class BuildScrollableBodyOfItems extends StatelessWidget {
  final ProductDetailsData productDetailsData;
  final ServiceListModel serviceListModel;

  const BuildScrollableBodyOfItems(
      {Key? key,
      required this.serviceListModel,
      required this.productDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
        GenericState<List<ServiceListModel>>>(
      bloc: productDetailsData.servicesBloc,
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(13.r)),
            color: MyColors.primaryLight,
          ),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: serviceListModel.staffDetail.length,
            itemBuilder: (cxt, index) {
              return AnimatedSwitcher(
                duration: Duration(milliseconds: 700),
                reverseDuration: Duration(milliseconds: 50),
                child: _isSelected(index)
                    ? BuildSelectedStaff(
                        index: index,
                        model: model,
                        serviceListModel: serviceListModel,
                        productDetailsData: productDetailsData)
                    : BuildUnselectedStaff(
                        productDetailsData: productDetailsData,
                        index: index,
                        model: model,
                        serviceListModel: serviceListModel),
              );
            },
          ),
        );
      },
    );
  }

  bool _isSelected(int index) {
    return serviceListModel.staffName ==
            serviceListModel.staffDetail[index].getStaffName() &&
        serviceListModel.staffId == serviceListModel.staffDetail[index].staffID;
  }
}
