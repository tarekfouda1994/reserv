part of 'product_details_widgets_imports.dart';

class BuildItemTabs extends StatelessWidget {
  final String title;
  final int val;
  final int changeVal;
  final void Function() onTap;

  const BuildItemTabs(
      {Key? key,
      required this.title,
      required this.onTap,
      required this.val,
      required this.changeVal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Expanded(
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(100.r),
        child: Container(
          margin: EdgeInsets.symmetric(
            vertical: device.isTablet ? 4.sp : 6.sp,
          ),
          padding: EdgeInsets.symmetric(vertical: 10).r,
          decoration: BoxDecoration(
              color: val == changeVal
                  ? MyColors.primary
                  : MyColors.primary.withOpacity(.05),
              borderRadius: BorderRadius.circular(100)),
          child: MyText(
            color: val == changeVal ? MyColors.white : MyColors.primary,
            title: title,
            size: device.isTablet ? 5.sp : 9.sp,
            alien: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
