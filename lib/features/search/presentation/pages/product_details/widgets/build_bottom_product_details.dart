part of 'product_details_widgets_imports.dart';

class BuildBottomProductDetails extends StatelessWidget {
  final ProductDetailsData data;
  final String businessId;

  const BuildBottomProductDetails(
      {Key? key, required this.data, required this.businessId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<DetailsViews>, GenericState<DetailsViews>>(
        bloc: data.pagesCubit,
        builder: (_, stateViews) {
          return Visibility(
            visible: stateViews.data != DetailsViews.services,
            child: Container(
              decoration: BoxDecoration(color: MyColors.white, boxShadow: [
                BoxShadow(
                    color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
              ]),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          BlocBuilder<GenericBloc<List<ServiceTypesModel>>,
                              GenericState<List<ServiceTypesModel>>>(
                            bloc: data.companyServicesBloc,
                            builder: (context, state) {
                              var total = 0;
                              int totalLength = _totalLength(state, total);
                              return Container(
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    color: MyColors.primary,
                                    shape: BoxShape.circle),
                                child: MyText(
                                  title: "$totalLength",
                                  color: MyColors.white,
                                  size: device.isTablet ? 6.sp : 10.sp,
                                ),
                              );
                            },
                          ),
                          MyText(
                              title: " ${tr("available_service")}",
                              color: MyColors.black,
                              size: device.isTablet ? 6.sp : 9.sp)
                        ],
                      ),
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () => AutoRouter.of(context)
                            .push(ReservationRootRoute(businessID: businessId)),
                        child: Container(
                          alignment: Alignment.center,
                          height: device.isTablet ? 50.h : 40.h,
                          decoration: BoxDecoration(
                              color: MyColors.primary,
                              borderRadius: BorderRadius.circular(30.r)),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10.w, vertical: 10.h),
                          child: Visibility(
                            visible: device.locale == Locale('en', 'US'),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                MyText(
                                  title: "R",
                                  color: MyColors.secondary,
                                  size: device.isTablet ? 9.sp : 13,
                                  fontWeight: FontWeight.bold,
                                ),
                                MyText(
                                  title: "eserve now",
                                  color: MyColors.white,
                                  size: device.isTablet ? 8.sp : 11.sp,
                                ),
                              ],
                            ),
                            replacement: MyText(
                              title: "احجز الآن",
                              color: MyColors.white,
                              size: device.isTablet ? 8.sp : 11.sp,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            replacement: BuildBottomServiceReview(
              data: data,
              businessId: businessId,
            ),
          );
        });
  }

  int _totalLength(GenericState<List<ServiceTypesModel>> state, int total) {
    var totalLength = state.data.map((element) {
      int length = element.servicesList.length;
      return total = total + length;
    }).toList();
    return totalLength.lastOrNull ?? 0;
  }
}
