part of 'product_details_widgets_imports.dart';

class BuildReviewsView extends StatelessWidget {
  final ProductDetailsData data;

  const BuildReviewsView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<ReviewsModel?>, GenericState<ReviewsModel?>>(
      bloc: data.reviewsCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Column(
            children: [
              Visibility(
                visible: state.data!.companyItemReviews.isNotEmpty,
                replacement: BuildEmptyReviewsBody(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BuildRateView(reviewsModel: state.data!),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: BuildHeaderTitle(
                          title: tr("reviews"),
                          count: state.data?.companyItemReviews.length ?? 0),
                    ),
                  ],
                ),
              ),
              Column(
                children: state.data!.companyItemReviews.map(
                  (e) {
                    return BuildReviewDetails(
                      model: e,
                    );
                  },
                ).toList(),
              )
            ],
          );
        }
        return Column(
          children: [
            BuildShimmerView(
              width: MediaQuery.of(context).size.width * .8.w,
              height: MediaQuery.of(context).size.height * .17.h,
            ),
            Divider(),
            BuildShimmerView(
              width: MediaQuery.of(context).size.width * .8.w,
              height: MediaQuery.of(context).size.height * .17.h,
            ),
          ],
        );
      },
    );
  }
}
