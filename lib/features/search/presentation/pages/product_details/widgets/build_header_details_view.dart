part of 'product_details_widgets_imports.dart';

class BuildHeaderDetailsView extends StatelessWidget {
  final DeviceModel model;
  final ProductDetailsData productDetailsData;
  final ServiceDetailsModel serviceDetailsModel;

  const BuildHeaderDetailsView(
      {Key? key,
      required this.model,
      required this.serviceDetailsModel,
      required this.productDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
        padding: const EdgeInsetsDirectional.only(start: 5.0),
        child: BuildHeaderTitle(title: tr("about_us"), count: 0),
      ),
      CustomCard(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * .6,
              child: MyText(
                title: serviceDetailsModel.aboutUs,
                color: MyColors.blackOpacity,

                size: model.isTablet ? 6.sp : 9.sp,
              ),
            ),
            InkWell(
              onTap: () => productDetailsData.showContactDialog(
                  context, serviceDetailsModel),
              child: Column(
                children: [
                  Transform.scale(
                    scale: .8,
                    child: BuildIconItem(
                        paddingIcon: model.isTablet ? 15 : 12,
                        backGroundColor: MyColors.primary,
                        img: SvgPicture.asset(
                          Res.call_outgoing,
                          color: MyColors.white,
                        )),
                  ),
                  MyText(
                      title: tr("contact"),
                      color: MyColors.primary,
                      size: model.isTablet ? 5.5.sp : 9.sp),
                ],
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
