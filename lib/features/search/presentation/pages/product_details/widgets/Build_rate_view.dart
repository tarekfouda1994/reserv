part of 'product_details_widgets_imports.dart';

class BuildRateView extends StatelessWidget {
  final ReviewsModel reviewsModel;

  const BuildRateView({Key? key, required this.reviewsModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    var rate = (reviewsModel.companyItemReviews.fold(0, (num prev, e) => prev + e.rating) /
            reviewsModel.companyItemReviews.length)
        .toStringAsFixed(1);
    return Padding(
      padding:  EdgeInsets.only(top: 15.h),
      child: CustomCard(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                    title: "$rate/ 5",
                    color: MyColors.amber,
                    size: device.isTablet ? 8.5.sp : 13.sp,
                    fontWeight: FontWeight.bold,
                  ),
                  RatingBarIndicator(
                    itemSize: device.isTablet ? 10.sp : 16.sp,
                    itemCount: 5,
                    rating: 1,
                    unratedColor: MyColors.greyLight,
                    itemPadding:
                        EdgeInsets.symmetric(horizontal: 0, vertical: device.isTablet ? 1.5.sp : 2),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: MyColors.amber,
                    ),
                  ),
                  MyText(
                    title: "${reviewsModel.companyItemReviews.length} ${tr("reviewers")}",
                    color: MyColors.primaryDark,
                    size: device.isTablet ? 6.sp : 9.sp,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  BuildRateItemBar(model: reviewsModel, value: 5),
                  BuildRateItemBar(model: reviewsModel, value: 4),
                  BuildRateItemBar(model: reviewsModel, value: 3),
                  BuildRateItemBar(model: reviewsModel, value: 2),
                  BuildRateItemBar(model: reviewsModel, value: 1),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
