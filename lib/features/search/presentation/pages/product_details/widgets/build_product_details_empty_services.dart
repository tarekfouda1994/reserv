part of 'product_details_widgets_imports.dart';

class BuildProductDetailsEmptyServices extends StatelessWidget {
  const BuildProductDetailsEmptyServices({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 60.h),
        child: MyText(
            title: tr("no_item_found"),
            color: MyColors.black,
            size: device.isTablet ? 9.sp : 13.sp),
      ),
    );
  }
}
