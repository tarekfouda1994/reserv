part of 'product_details_widgets_imports.dart';

class BuildDetailsTime extends StatelessWidget {
  final DeviceModel model;
  final ProductDetailsData data;
  final ServiceDetailsModel serviceDetailsModel;

  const BuildDetailsTime(
      {Key? key,
      required this.model,
      required this.data,
      required this.serviceDetailsModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: BuildHeaderTitle(title: tr("working_hours"), count: 0),
      ),
      CustomCard(
          child: Container(
        margin: EdgeInsets.symmetric(vertical: 8.h),
        child: Column(
          children: List.generate(
            serviceDetailsModel.timing.length,
            (index) {
              return Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: MyText(
                            alien: TextAlign.start,
                            size: model.isTablet ? 8.sp : 10.sp,

                            title: serviceDetailsModel.timing[index].key,
                            color: MyColors.black),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: MyText(
                            alien: TextAlign.start,
                            size: model.isTablet ? 8.sp : 10.sp,
                            title: serviceDetailsModel.timing[index].value,
                            color: MyColors.primary),
                      ),
                    ],
                  ),
                  if (serviceDetailsModel.timing.length != index + 1) Divider(),
                ],
              );
            },
          ),
        ),
      )),
    ]);
  }
}
