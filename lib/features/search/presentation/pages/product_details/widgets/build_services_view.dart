part of 'product_details_widgets_imports.dart';

class BuildServicesView extends StatelessWidget {
  final ProductDetailsData data;

  const BuildServicesView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceTypesModel>>,
        GenericState<List<ServiceTypesModel>>>(
      bloc: data.companyServicesBloc,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return state.data.isNotEmpty
              ? Column(
                  children: List.generate(
                    state.data.length,
                    (index) {
                      return Column(
                        children: [
                          BuildHeaderTitle(
                            title: state.data[index].getServiceTypeName(),
                            count: state.data[index].servicesList.length,
                          ),
                          CustomCard(
                            padding: EdgeInsets.zero,
                            child: ListView.separated(
                              physics: BouncingScrollPhysics(),
                              shrinkWrap: true,
                              separatorBuilder: (BuildContext context, int i) {
                                return Container(height: 3);
                              },
                              itemBuilder:
                                  (BuildContext context, int indexItem) {
                                return DisableWidget(
                                  isDisable:
                                      _isDisable(state, index, indexItem),
                                  child: BuildServicesViewItem(
                                    productDetailsData: data,
                                    deviceModel: device,
                                    serviceListModel: state
                                        .data[index].servicesList[indexItem],
                                    action: () => data.onSelectService(
                                      state.data[index].servicesList[indexItem],
                                      context,
                                    ),
                                    index: indexItem,
                                    length:
                                        state.data[index].servicesList.length,
                                  ),
                                );
                              },
                              itemCount: state.data[index].servicesList.length,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                )
              : const BuildProductDetailsEmptyServices();
        }
        return Column(
          children: List.generate(
            5,
            (index) => BuildShimmerView(
              height: device.isTablet ? 110.h : 100.h,
              width: MediaQuery.of(context).size.width * .94.w,
            ),
          ),
        );
      },
    );
  }

  bool _isDisable(
      GenericState<List<ServiceTypesModel>> state, int index, int indexItem) {
    return state.data[index].servicesList[indexItem].staffDetail.isEmpty;
  }
}
