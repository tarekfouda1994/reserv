part of 'product_details_widgets_imports.dart';

class BuildBottomServiceReview extends StatelessWidget {
  final ProductDetailsData data;
  final String businessId;

  const BuildBottomServiceReview(
      {Key? key, required this.data, required this.businessId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
        GenericState<List<ServiceListModel>>>(
      bloc: data.servicesBloc,
      builder: (context, state) {
        return Visibility(
          visible: state.data.isNotEmpty,
          child: Container(
            decoration: BoxDecoration(color: MyColors.white, boxShadow: [
              BoxShadow(
                  color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
            ]),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            MyText(
                                title:
                                    "${state.data.lastOrNull?.getCurrencyName() ?? ""} ${data.getSelectedServicePrice()} ",
                                color: MyColors.black,
                                fontWeight: FontWeight.bold,
                                size: device.isTablet ? 8.sp : 12.sp),
                            MyText(
                                title: tr("including_vat"),
                                color: MyColors.grey,
                                size: device.isTablet ? 6.sp : 8.sp),
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            if (state.data.length != 0)
                              data.allServicesBottomSheet(context, data);
                          },
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(10),
                                margin:
                                    const EdgeInsetsDirectional.only(end: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: MyColors.primary),
                                  color: MyColors.primary,
                                  shape: BoxShape.circle,
                                ),
                                child: MyText(
                                    title: "${state.data.length}",
                                    color: MyColors.white,
                                    size: device.isTablet ? 7.sp : 11.sp),
                              ),
                              MyText(
                                  title: tr("View_items"),
                                  color: MyColors.primary,
                                  size: device.isTablet ? 6.sp : 9.sp),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () =>
                          data.goToReservationScreen(context, businessId),
                      child: Container(
                        height: device.isTablet ? 50.h : 40.h,
                        decoration: BoxDecoration(
                          color: MyColors.primary,
                          borderRadius: BorderRadius.circular(40.r),
                        ),
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 10.h),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              MyText(
                                  title: "${tr("proceed")} ",
                                  color: MyColors.white,
                                  size: device.isTablet ? 7.sp : 11.sp),
                              SizedBox(width: 5.w),
                              BuildMyArrowIcon(),
                            ]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
