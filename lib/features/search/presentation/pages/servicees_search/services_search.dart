part of 'services_search_imports.dart';

class ServicesSearchPage extends StatefulWidget {
  final String filter;

  const ServicesSearchPage({Key? key, required this.filter}) : super(key: key);

  @override
  State<ServicesSearchPage> createState() => _ServicesSearchPageState();
}

class _ServicesSearchPageState extends State<ServicesSearchPage> {
  ServiceSearchData searchData = ServiceSearchData();

  @override
  void initState() {
    searchData.pagingController.addPageRequestListener((pageKey) {
      searchData.ficheData(context, pageKey, widget.filter);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Scaffold(
      appBar: DefaultAppBar(title: tr("service_search"), centerTitle: true),
      body: RefreshIndicator(
        color: MyColors.primary,
        onRefresh: () => searchData.ficheData(context, 1, widget.filter),
        child: PagedListView<int, ServiceSearchItemModel>(
          padding: const EdgeInsets.symmetric(vertical: 25),
          pagingController: searchData.pagingController,
          builderDelegate: PagedChildBuilderDelegate<ServiceSearchItemModel>(
              firstPageProgressIndicatorBuilder: (context) => Column(
                  children: List.generate(
                      10,
                      (index) => BuildShimmerView(
                          height: device.isTablet ? 60.h : 40.h,
                          width: MediaQuery.of(context).size.width * .8.w))),
              itemBuilder: (context, item, index) => SizedBox(
                      child: BuildServiceSearchItem(
                    searchItemModel: item,
                    model: device,
                    currentSearchKeyword: widget.filter,
                  )),
              newPageProgressIndicatorBuilder: (con) =>
                  Padding(padding: const EdgeInsets.all(8.0), child: CupertinoActivityIndicator()),
              firstPageErrorIndicatorBuilder: (context) =>
                  BuildPageError(onTap: () => searchData.pagingController.refresh())),
        ),
      ),
    );
  }
}
