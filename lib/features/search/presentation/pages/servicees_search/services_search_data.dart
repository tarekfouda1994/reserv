part of 'services_search_imports.dart';

class ServiceSearchData {
  final PagingController<int, ServiceSearchItemModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  ficheData(BuildContext context, int currentPage, String filter,
      {bool refresh = true}) async {
    ServiceSearchParams params =
        _serviceSearchParams(filter, currentPage, refresh);
    var data = await GetAllServiceSearch()(params);
    final isLastPage = data!.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
  }

  ServiceSearchParams _serviceSearchParams(
      String filter, int currentPage, bool refresh) {
    return ServiceSearchParams(
        filter: filter,
        pageSize: pageSize,
        pageNumber: currentPage,
        refresh: refresh);
  }
}
