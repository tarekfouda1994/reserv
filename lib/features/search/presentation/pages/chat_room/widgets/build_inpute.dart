part of 'chat_room_widgets_imports.dart';

class BuildInput extends StatelessWidget {
  final DeviceModel device;
  final ChatRoomData chatRoomData;
  final String  businessID;
  final String  orderNumber;
  final String  bookingOrderID;

  const BuildInput({Key? key, required this.device, required this.chatRoomData, required this.businessID, required this.orderNumber, required this.bookingOrderID}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GenericTextField(
      controller: chatRoomData.msgController,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        hint: tr("message"),
        fieldTypes: FieldTypes.chat,
        type: TextInputType.text,
        focusBorderColor: MyColors.primary.withOpacity(.3),
        suffixIcon: Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.horizontal(right: Radius.circular(12)),
              color: MyColors.primaryLight.withOpacity(.4)),
          child: InkWell(
            onTap: ()=> chatRoomData.addMsg(businessID: businessID, orderNumber: orderNumber, bookingOrderID: bookingOrderID),
            child: MyText(
              color: MyColors.primary,
              title: tr('send'),
              size: device.isTablet ? 8.sp : 11.sp,
            ),
          ),
        ),
        action: TextInputAction.send,
        validate: (v) {});
  }
}
