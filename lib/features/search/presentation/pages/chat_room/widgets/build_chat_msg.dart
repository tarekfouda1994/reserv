part of 'chat_room_widgets_imports.dart';

class BuildChatMassages extends StatelessWidget {
  final List<ChatModel> chatModels;
  final ChatRoomData chatRoomData;

  const BuildChatMassages(
      {Key? key, required this.chatModels, required this.chatRoomData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: chatRoomData.controller,
      addAutomaticKeepAlives: true,
      physics: Platform.isIOS
          ? BouncingScrollPhysics(parent: NeverScrollableScrollPhysics())
          : null,
      padding: EdgeInsets.only(bottom: 100.h, top: 16.h, right: 16, left: 16),
      itemCount: chatModels.length,
      itemBuilder: (cxt, index) {
        return Visibility(
          visible: chatModels[index].isClient,
          child: BuildRightMsg(
              chatModelList: chatModels,
              chatRoomData: chatRoomData,
              index: index),
          replacement: BuildLeftMsg(
              chatModelList: chatModels,
              index: index,
              chatRoomData: chatRoomData),
        );
      },
    );
  }
}
