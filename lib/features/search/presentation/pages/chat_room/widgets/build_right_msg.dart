part of 'chat_room_widgets_imports.dart';

class BuildRightMsg extends StatelessWidget {
  final int index;
  final ChatRoomData chatRoomData;
  final List<ChatModel> chatModelList;

  const BuildRightMsg(
      {Key? key,
      required this.chatRoomData,
      required this.index,
      required this.chatModelList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = context.watch<UserCubit>().state.model;
    ChatModel chatModel = chatModelList[index];
    var device = context.watch<DeviceCubit>().state.model;
    var prevItem =
        index == 0 ? chatModelList[index] : chatModelList[index - 1];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: prevItem.insertedDate != chatModel.insertedDate ||
                  index == 0,
              child: MyText(
                  title: chatModel.insertedDate + "  ",
                  color: MyColors.blackOpacity,
                  size: device.isTablet ? 6.sp : 9.sp),
            ),
            Visibility(
            visible: prevItem.insertedTime != chatModel.insertedTime ||
            index == 0,
              child: MyText(
                  title: chatModel.insertedTime,
                  color: MyColors.blackOpacity,
                  size: device.isTablet ? 6.sp : 9.sp),
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(width: MediaQuery.of(context).size.width*.1),
            Flexible(
              child: Container(
                child: Bubble(
                  padding: BubbleEdges.all(8),
                  radius: Radius.circular(5.r),
                  elevation: 0,
                  borderColor: MyColors.white,
                  color: Color(0xffF6F9F9),
                  nip: BubbleNip.rightBottom,
                  child: MyText(
                      alien: TextAlign.end,

                      title: chatModel.message,
                      color: MyColors.black,
                      size: device.isTablet ? 6.sp : 10.sp),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: CachedImage(
                  url: user!.photo.replaceAll("\\", "/"),
                  placeHolder: StaffPlaceholder(),
                  fit: BoxFit.cover,
                  haveRadius: false,
                  bgColor: MyColors.defaultImgBg,
                  boxShape: BoxShape.circle,
                  width: device.isTablet ? 26.w : 31.w,
                  height: device.isTablet ? 26.w : 31.w),
            ),
          ],
        ),
      ],
    );
  }
}
