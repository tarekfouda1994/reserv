part of 'chat_room_widgets_imports.dart';

class BuildAction extends StatelessWidget {
  final DeviceModel deviceModel;

  const BuildAction({Key? key, required this.deviceModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => AutoRouter.of(context).pop(),
        child: Container(
            margin: const EdgeInsetsDirectional.only(end: 14),
            padding: EdgeInsets.all(deviceModel.isTablet? 3.h:6.r),
            decoration: BoxDecoration(shape: BoxShape.circle, color: MyColors.black),
            child: Icon(Icons.clear,
                size: deviceModel.isTablet ? 14.sp : 17.sp, color: MyColors.white)));
  }
}
