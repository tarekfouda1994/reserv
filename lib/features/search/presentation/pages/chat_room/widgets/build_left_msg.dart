part of 'chat_room_widgets_imports.dart';

class BuildLeftMsg extends StatelessWidget {
  final List<ChatModel> chatModelList;
  final int index;
  final ChatRoomData chatRoomData;

  const BuildLeftMsg(
      {Key? key,
      required this.chatRoomData, required this.chatModelList, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ChatModel chatModel = chatModelList[index];
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyText(
                title: chatModel.insertedTime,
                color: MyColors.blackOpacity,
                size: device.isTablet ? 6.sp : 9.sp),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            BlocBuilder<GenericBloc<String>, GenericState<String>>(
                bloc: chatRoomData.imageCubit,
                builder: (context, state) {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: CachedImage(
                        url: state.data.replaceAll("\\", "/"),
                        fit: BoxFit.cover,
                        placeHolder: StaffPlaceholder(),
                        bgColor: MyColors.defaultImgBg,
                        borderRadius: BorderRadius.circular(100),
                        width: device.isTablet ? 26.w : 31.w,
                        height: device.isTablet ? 26.w : 31.w),
                  );
                }),
            SizedBox(width: 5),
            Flexible(
              child: MyText(
                  title: chatModel.message,
                  color: MyColors.blackOpacity,
                  size: device.isTablet ? 6.sp : 9.sp),
            ),
            SizedBox(width: MediaQuery.of(context).size.width*.1),
          ],
        ),
      ],
    );
  }
}
