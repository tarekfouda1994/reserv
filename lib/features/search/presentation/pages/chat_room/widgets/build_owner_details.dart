part of 'chat_room_widgets_imports.dart';

class BuildOwnerDetails extends StatelessWidget {
  final DeviceModel device;
  final String businessName;
  final ChatRoomData chatRoomData;

  const BuildOwnerDetails(
      {Key? key,
      required this.device,
      required this.businessName,
      required this.chatRoomData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<String>, GenericState<String>>(
        bloc: chatRoomData.imageCubit,
        builder: (context, state) {
          return Container(
            color: Color(0xffF6F9F9),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: Row(
              children: [
                Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    CachedImage(
                        url: state.data.replaceAll("\\", "/"),
                        fit: BoxFit.cover,
                        borderRadius: BorderRadius.circular(100),
                        placeHolder: StaffPlaceholder(),
                        bgColor: MyColors.defaultImgBg,
                        width: device.isTablet ? 28.w : 33.w,
                        height: device.isTablet ? 28.w : 33.w),
                    Visibility(
                      visible: state.data.isNotEmpty,
                      child: Container(
                        height: 9.sp,
                        width: 9.sp,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: MyColors.white,
                                width: device.isTablet ? 1.r : .5),
                            shape: BoxShape.circle,
                            color: MyColors.primary),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    MyText(
                        title: businessName,
                        color: MyColors.black,
                        size: device.isTablet ? 7.sp : 11.sp),
                    Visibility(
                      visible: state.data.isNotEmpty,
                      child: MyText(
                          title: tr("online"),
                          color: MyColors.grey,
                          size: device.isTablet ? 5.5.sp : 9.sp),
                    ),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
