import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/staff_placeholder.dart';
import 'package:flutter_tdd/features/auth/presentation/manager/user_cubit/user_cubit.dart';
import 'package:flutter_tdd/features/search/data/models/chat_model/chat_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/chat_room/chat_room_imports.dart';
import 'package:tf_custom_widgets/Inputs/GenericTextField.dart';
import 'package:tf_custom_widgets/utils/generic_cubit/generic_cubit.dart';
import 'package:tf_custom_widgets/widgets/CachedImage.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import '../../../../../../core/constants/my_colors.dart';

part 'build_action.dart';
part 'build_chat_msg.dart';
part 'build_inpute.dart';
part 'build_left_msg.dart';
part 'build_owner_details.dart';
part 'build_right_msg.dart';
