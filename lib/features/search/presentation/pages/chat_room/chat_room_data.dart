part of 'chat_room_imports.dart';

class ChatRoomData {
  GenericBloc<List<ChatModel>> chatCubit = GenericBloc([]);
  TextEditingController msgController = TextEditingController();
  final GenericBloc<String> imageCubit = GenericBloc("");
  ScrollController controller = ScrollController();

  bool goToLastMsg = true;

  getAllMsg(String orderNumber, String businessId,
      {bool refresh = true}) async {
    var data = await GetAllChatMsg()(orderNumber);
    await _getAppointmentImage(businessId, refresh: refresh);
    await chatCubit.onUpdateData(data);
    if (goToLastMsg) {
      Future.delayed(
        Duration(milliseconds: 500),
        () {
          _navigateToLastMsg(milliseconds: 1000);
          goToLastMsg = false;
        },
      );
    }
  }

  addMsg(
      {required String bookingOrderID,
      required String orderNumber,
      required String businessID}) async {
    if (msgController.text.isNotEmpty) {
      var result = await AddMsgChat()(_chatMsgParams(orderNumber, businessID));
      if (result) {
        await _updateChatCubit(bookingOrderID, orderNumber, businessID);
        msgController.clear();
        _navigateToLastMsg();
      }
    }
  }

  _updateChatCubit(
      String bookingOrderID, String orderNumber, String businessID) {
    chatCubit.state.data
        .add(_chatModel(bookingOrderID, orderNumber, businessID));
    chatCubit.onUpdateData(chatCubit.state.data);
  }

  _navigateToLastMsg({int? milliseconds}) {
    double end = controller.position.maxScrollExtent;
    controller.animateTo(end + 90,
        duration: Duration(milliseconds: milliseconds ?? 300),
        curve: Curves.easeIn);
  }

  ChatModel _chatModel(
      String bookingOrderID, String orderNumber, String businessID) {
    ChatModel chatModel = ChatModel(
        hasReadMessage: true,
        isActive: true,
        businessUserPhotoPath: '',
        insertedTime: DateFormat("hh:mm").format(DateTime.now()),
        orderNumber: orderNumber,
        chatStatus: 3,
        isClient: true,
        message: msgController.text,
        businessID: businessID,
        bookingOrderID: bookingOrderID,
        insertedDate: DateFormat("dd-MM-yyyy").format(DateTime.now()));
    return chatModel;
  }

  AddMsgChatParams _chatMsgParams(String orderNumber, String businessID) {
    var params = AddMsgChatParams(
        orderNumber: orderNumber,
        businessID: businessID,
        chatStatus: 0,
        message: msgController.text);
    return params;
  }

  _getAppointmentImage(String businessId, {bool refresh = true}) async {
    var params = BusinessImagesParams(
        businessId: businessId, refresh: refresh, firstOnly: true);
    var data = await GetBusinessImage()(params);
    imageCubit.onUpdateData(data);
  }
}
