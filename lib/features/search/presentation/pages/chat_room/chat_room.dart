part of 'chat_room_imports.dart';

class ChatRoom extends StatefulWidget {
  final AppointmentOrdersModel appointmentOrdersModel;

  const ChatRoom({
    Key? key,
    required this.appointmentOrdersModel,
  }) : super(key: key);

  @override
  State<ChatRoom> createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  ChatRoomData chatRoomData = ChatRoomData();
  late Timer _timer;

  @override
  void initState() {
    chatRoomData.getAllMsg(widget.appointmentOrdersModel.orderNumber,
        widget.appointmentOrdersModel.businessID,
        refresh: false);
    _timer = Timer.periodic(
        Duration(seconds: 10),
        (Timer timer) => chatRoomData.getAllMsg(widget.appointmentOrdersModel.orderNumber,
            widget.appointmentOrdersModel.businessID));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: MyColors.white,
        appBar: DefaultAppBar(
            titleSize:device.isTablet?7.sp: 11.sp,
            showBack: false,
            title: tr("chat_with"),
            actions: [BuildAction(deviceModel: device)]),
        body: Column(children: [
          BuildOwnerDetails(
              device: device,
              businessName: widget.appointmentOrdersModel.getBusinessName(),
              chatRoomData: chatRoomData),
          Flexible(
            child: BlocBuilder<GenericBloc<List<ChatModel>>,
                    GenericState<List<ChatModel>>>(
                bloc: chatRoomData.chatCubit,
                builder: (cxt, state) {
                  if (state is GenericUpdateState) {
                    return BuildChatMassages(
                        chatModels: state.data, chatRoomData: chatRoomData);
                  } else {
                    return LinearProgressIndicator(
                        color: MyColors.primary,
                        backgroundColor: MyColors.grey.withOpacity(.3));
                  }
                }),
          )
        ]),
        bottomSheet: BuildInput(
          device: device,
          chatRoomData: chatRoomData,
          bookingOrderID: widget.appointmentOrdersModel.bookingOrderID,
          orderNumber: widget.appointmentOrdersModel.orderNumber,
          businessID: widget.appointmentOrdersModel.businessID,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
