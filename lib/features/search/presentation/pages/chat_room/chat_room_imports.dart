import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_orders/appointment_orders.dart';
import 'package:flutter_tdd/features/search/data/models/chat_model/chat_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/add_msg_chat_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_image_entity.dart';
import 'package:flutter_tdd/features/search/domain/use_cases/add_msg_chat.dart';
import 'package:flutter_tdd/features/search/domain/use_cases/get_all_chat_msg.dart';
import 'package:flutter_tdd/features/search/domain/use_cases/get_business_image.dart';
import 'package:flutter_tdd/features/search/presentation/pages/chat_room/widgets/chat_room_widgets_imports.dart';
import 'package:intl/src/intl/date_format.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../core/bloc/device_cubit/device_cubit.dart';

part 'chat_room.dart';
part 'chat_room_data.dart';
