part of 'search_screen_imports.dart';

class SearchScreenData {
  TextEditingController searchController = TextEditingController();

  GenericBloc<bool> searchViewBloc = GenericBloc(false);
  GenericBloc<List<PopularSearchModel>> popularSearchBloc = GenericBloc([]);
  GenericBloc<List<PopularSearchModel>> recentSearchBloc = GenericBloc([]);
  GenericBloc<BusinessSearchModel?> businessSearchBloc = GenericBloc(null);
  GenericBloc<ServiceSearchModel?> serviceSearchBloc = GenericBloc(null);
  GenericBloc<List<String>> recentSearchCubit = GenericBloc([]);
  GenericBloc<String> filterBloc = GenericBloc("");

  List<String> recentSearchList = [];
  List<PopularSearchModel> popularSearchList = [];
  List<DateTime> dateList = [];

  String? currentSearchKeyword;


  changeView() {
    if (searchController.text.isNotEmpty) {
      searchViewBloc.onUpdateData(true);
    } else {
      searchViewBloc.onUpdateData(false);
    }
  }

  getPopularSearch(BuildContext context, {bool refresh = true}) async {
    var data = await GetPopularSearch()(refresh);
    popularSearchBloc.onUpdateData(data.take(5).toList());
    popularSearchList = data;
  }

  getRecentSearch(BuildContext context, {bool refresh = true}) async {
    var data = await GteRecentSearch()(refresh);
    recentSearchBloc.onUpdateData(data);
  }

  getBusinessSearch(BuildContext context, String filter,
      {bool refresh = false}) async {
    var dateTime = context.read<SearchCubit>().state.dateTime.dateList;
    dateTime.sort((a, b) => a.compareTo(b));
    businessSearchBloc.onUpdateToInitState(null);
    var deviceId = await FirebaseMessaging.instance.getToken();
    var params =
        _businessSearchPrams(context, deviceId, filter, dateTime, refresh);
    var data = await GetAllSearch()(params);
    if (data != null && refresh == false) {
      businessSearchBloc.onUpdateData(data);
    }
  }

  SearchPrams _businessSearchPrams(BuildContext context, String? deviceId,
      String filter, List<DateTime> dateTime, bool refresh) {
    var lotLong = context.read<HomeLocationCubit>().state.model;
    return SearchPrams(
        deviceId: deviceId!,
        filter: filter,
        pageNumber: 1,
        longitude: lotLong?.lng ?? 0,
        latitude: lotLong?.lat ?? 0,
        pageSize: 5,
        from: getIt<Utilities>().formatDate(dateTime.firstOrNull),
        to: getIt<Utilities>().formatDate(dateTime.lastOrNull),
        refresh: refresh);
  }

  getServiceSearch(BuildContext context, String filter,
      {bool refresh = false}) async {
    serviceSearchBloc.onUpdateToInitState(null);
    var params = _serviceSearchParams(filter, refresh);
    var data = await GetAllServiceSearch()(params);
    if (data != null && refresh == false) {
      serviceSearchBloc.onUpdateData(data);
    }
  }

  ServiceSearchParams _serviceSearchParams(String filter, bool refresh) {
    return ServiceSearchParams(
      filter: filter,
      pageSize: 10,
      pageNumber: 1,
      refresh: refresh,
    );
  }

  Future<void> doSearch(
      String filter, BuildContext context) async {
    if (filter.isNotEmpty) {
      currentSearchKeyword = filter;
      filterBloc.onUpdateData(filter);
      searchViewBloc.onUpdateData(filter.isNotEmpty);
      getServiceSearch(context, filter, refresh: false);
      getServiceSearch(context, filter, refresh: true);
      getBusinessSearch(context, filter, refresh: false);
      getBusinessSearch(context, filter, refresh: true);
    }
  }

  void onSearchTextChanged(String filter, BuildContext context){
   if (filter.isEmpty) {
     searchViewBloc.onUpdateData(false);
     serviceSearchBloc.onUpdateToInitState(null);
     businessSearchBloc.onUpdateToInitState(null);
   }
  }

  onSelectSearchStr(BuildContext context, String filter) async {
    await doSearch(filter, context);
    if (serviceSearchBloc.state.data!.itemsList.isNotEmpty ||
        businessSearchBloc.state.data!.itemsList.isNotEmpty) {
      saveMySearchString(context, filter);
    }
  }

  onCloseSearch(BuildContext context) {
    searchViewBloc.onUpdateData(false);
    filterBloc.onUpdateData('');
    searchController.clear();
  }

  Future<void> saveMySearchString(BuildContext context, String filter) async {
    await saveStringListValue(filter);
    await retrieveStringListValue();
    FocusScope.of(context).requestFocus(FocusNode());
  }

  saveStringListValue(String filter) async {
    if (filter.isEmpty) {
      return;
    }
    recentSearchList.insert(0, filter);
    var prefs = await SharedPreferences.getInstance();
    prefs.setStringList("recentSearch", recentSearchList.toSet().toList());
  }

  retrieveStringListValue() async {
    var prefs = await SharedPreferences.getInstance();
    List<String>? value = prefs.getStringList("recentSearch");
    if (value!.isNotEmpty) {
      recentSearchList = value;
      recentSearchCubit.onUpdateData(value.take(5).toList());
    }
  }

  removeItem(int index) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove("recentSearch"[index]);
    recentSearchList.removeAt(index);
    recentSearchCubit.onUpdateData(recentSearchList);
    prefs.setStringList("recentSearch", recentSearchList.toSet().toList());
  }

  removeAllItem(BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove("recentSearch");
    recentSearchList.clear();
    recentSearchCubit.onUpdateData(recentSearchList);
    Navigator.pop(context);
  }

  onSubmitFilter(BuildContext context, String filter) async {
    onSelectSearchStr(context, filter);
    WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
    // var dateTime = context.read<SearchCubit>().state.dateTime;
    // getIt<LoadingHelper>().showLoadingDialog();
    // var deviceId = await FirebaseMessaging.instance.getToken();
    // var getBusiness = await GetAllSearch()(SearchPrams(
    //     deviceId: deviceId!,
    //     filter: filter,
    //     pageNumber: 1,
    //     pageSize: 5,
    //     from: getIt<Utilities>().formatDate(dateTime.dateList.firstOrNull),
    //     to: getIt<Utilities>().formatDate(dateTime.dateList.lastOrNull),
    //     refresh: true));
    // var serviceSearch = await GetAllServiceSearch()(ServiceSearchParams(
    //     filter: filter, pageSize: 5, pageNumber: 1, refresh: true));
    // if (getBusiness!.itemsList.isNotEmpty ||
    //     serviceSearch!.itemsList.isNotEmpty) {
    //   // getIt<LoadingHelper>().dismissDialog();
    // } else {
    //   // WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
    //   // getIt<LoadingHelper>().dismissDialog();
    //   CustomToast.showSimpleToast(
    //       msg: "${tr("alert_search")} $filter",
    //       type: ToastType.info,
    //       title: tr("no_result"));
    //   return;
    // }
    // else {
    //   // WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
    //   CustomToast.showSimpleToast(
    //       msg: tr("kindly_text"),
    //       type: ToastType.info,
    //       title: tr("Warning_Toast"));
    //   return;
    // }
  }

  showConfirmCancelDialog(BuildContext context) {
    var device = context.read<DeviceCubit>().state.model;
    CustomToast.customConfirmDialog(
        recordItem: MyText(
          title: tr("sure_remove_search"),
          color: MyColors.black,
          size: device.isTablet ? 5.5.sp : 9.sp,
          fontWeight: FontWeight.bold,
        ),
        onConfirm: () => removeAllItem(context),
        content: "",
        title: tr("Removing_Search"),
        btnName: tr("Remove"));
  }
}
