part of 'search_screen_widgets_imports.dart';

class BuildOnSearchView extends StatelessWidget {
  final DeviceModel model;
  final SearchScreenData data;

  const BuildOnSearchView({Key? key, required this.model, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        BuildServiceSearch(model: model, searchScreenData: data),
        BuildBusinesses(model: model, data: data),
      ],
    );
  }
}
