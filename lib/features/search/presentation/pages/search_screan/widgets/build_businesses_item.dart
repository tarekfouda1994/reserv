part of 'search_screen_widgets_imports.dart';

class BuildBusinessesItem extends StatelessWidget {
  final DeviceModel model;
  final BusinessItemSearchModel businessItemSearchModel;

  const BuildBusinessesItem({Key? key, required this.model, required this.businessItemSearchModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => AutoRouter.of(context).push(ProductDetailsRoute(id: businessItemSearchModel.id)),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        Padding(
                          padding: const EdgeInsetsDirectional.only(bottom: 3, end: 3),
                          child: CachedImage(
                            url: businessItemSearchModel.businessImage.replaceAll("\\", "/"),
                            width: model.isTablet ? 30.w : 40.w,
                            height: model.isTablet ? 30.w : 40.w,
                            borderRadius: BorderRadius.circular(8.r),
                            fit: BoxFit.cover,
                            bgColor: MyColors.defaultImgBg,
                            placeHolder: BusinessPlaceholder(),
                          ),
                        ),
                        // Container(
                        //   decoration: BoxDecoration(
                        //       shape: BoxShape.circle, color: MyColors.white),
                        //   child: Container(
                        //     margin: EdgeInsets.all(1.h),
                        //     height: 8.h,
                        //     width: 8.h,
                        //     decoration: BoxDecoration(
                        //         shape: BoxShape.circle, color: MyColors.primary),
                        //   ),
                        // )
                      ],
                    ),
                    if (businessItemSearchModel.totalReviews != 0)
                      SizedBox(height: model.isTablet ? 30.h : 23.h)
                  ],
                ),
                SizedBox(width: model.isTablet ? 6.w : 8.w),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Container(
                              width: MediaQuery.of(context).size.width * .45,
                              child: MyText(
                                  overflow: TextOverflow.ellipsis,
                                  title: businessItemSearchModel.getSearchName(),
                                  color: MyColors.black,
                                  size: model.isTablet ? 8.sp : 11.sp),
                            ),
                          ),
                          Offstage(
                            offstage: businessItemSearchModel.rating == 0,
                            child: MyText(
                                title: businessItemSearchModel.rating.toString(),
                                color: MyColors.amber,
                                size: model.isTablet ? 6.sp : 9.sp),
                          ),
                          Offstage(
                            offstage: businessItemSearchModel.rating == 0,
                            child: Icon(Icons.star, color: MyColors.amber, size: 16),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: Row(
                          children: [
                            Expanded(
                                child: MyText(
                                    title: businessItemSearchModel.getAreaName(),
                                    color: MyColors.grey,
                                    size: model.isTablet ? 6.sp : 9.sp),
                            ),
                            Offstage(
                              offstage: businessItemSearchModel.distance == 0,
                              child: MyText(
                                  title:
                                      "${businessItemSearchModel.distance.toStringAsFixed(2)} ${tr("km")}",
                                  color: MyColors.black,
                                  size: model.isTablet ? 6.sp : 9.sp),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: businessItemSearchModel.totalReviews != 0,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                margin: const EdgeInsets.all(3),
                                padding: const EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    color: MyColors.primary),
                                child: Icon(Icons.thumb_up,
                                    color: MyColors.white, size: model.isTablet ? 9.sp : 12.sp)),
                            MyText(
                                overflow: TextOverflow.ellipsis,
                                title: tr("recommended"),
                                color: MyColors.primary,
                                size: model.isTablet ? 6.sp : 9.5.sp),
                            MyText(
                                title: " +${businessItemSearchModel.totalReviews} ${tr("reviews")}",
                                color: MyColors.redColor,
                                size: model.isTablet ? 7.sp : 9.5.sp),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Divider(
              height: model.isTablet ? 10.sp : 14.h,
              endIndent: 16,
              indent: 16,
            ),
          ],
        ),
      ),
    );
  }
}
