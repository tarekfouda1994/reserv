part of 'search_screen_widgets_imports.dart';

class BuildServiceSearchItem extends StatelessWidget {
  final DeviceModel model;
  final ServiceSearchItemModel searchItemModel;

  final String? currentSearchKeyword;

  const BuildServiceSearchItem({
    Key? key,
    required this.model,
    required this.searchItemModel,
    required this.currentSearchKeyword,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => _onSelectItem(context),
      contentPadding: EdgeInsets.symmetric(
          horizontal: model.isTablet ? 12.w : 16.w,
          vertical: model.isTablet ? 8.h : 0),
      horizontalTitleGap: 5.w,
      leading: Container(
        padding: EdgeInsets.symmetric(
            vertical: model.isTablet ? 6.h : 8.h, horizontal: 10.h),
        child: MyText(
          alien: TextAlign.center,
          title: searchItemModel.totalResult.toString(),
          size: model.isTablet ? 8.sp : 10.sp,
          color: MyColors.primary,
        ),
        decoration: BoxDecoration(
            color: MyColors.primary.withOpacity(.05),
            borderRadius: BorderRadius.circular(6.r)),
      ),
      title: _buildServiceTitle(searchItemModel.getServiceTypeName()),
      trailing: Icon(
        model.locale == Locale('en', 'US') ? Icons.east : Icons.west,
        color: MyColors.primary,
        size: model.isTablet ? 12.sp : 17.sp,
      ),
    );
  }


  Widget _buildServiceTitle(String title){
    var words = title.split(" ");
    var timeSpans = words.map((e) {
      var currentSearchKeywords = currentSearchKeyword?.split(" ").map((e) => e.toLowerCase()).toList()??[];
      if (currentSearchKeywords.any((word)=>e.toLowerCase().contains(word)) ) {
        return TextSpan(
          text: e,
          style: TextStyle(
            color: MyColors.black,
            fontFamily: CustomFonts.primaryFont,
            fontSize: model.isTablet ? 7.sp : 11.sp,
            fontWeight: FontWeight.bold,
          ),
          children: [
            TextSpan(
              text: " ",
              style: TextStyle(
                color: MyColors.blackOpacity,
                fontFamily: CustomFonts.primaryFont,
                fontSize: model.isTablet ? 7.sp : 11.sp,
              ),
            ),
          ],
        );
      }
      return TextSpan(
        text: e,
        style: TextStyle(
          color: MyColors.blackOpacity,
          fontFamily: CustomFonts.primaryFont,
          fontSize: model.isTablet ? 7.sp : 11.sp,
        ),
        children: [
          TextSpan(
            text: " ",
            style: TextStyle(
              fontFamily: CustomFonts.primaryFont,
              color: MyColors.blackOpacity,
              fontSize: model.isTablet ? 7.sp : 11.sp,
            ),
          ),
        ],
      );
    }).toList();
    return RichText(
      textScaleFactor: 1.2,
      textWidthBasis: TextWidthBasis.longestLine,
      text: TextSpan(
        children: timeSpans,
      ),
    );
  }

  void _onSelectItem(BuildContext context) {
    context.read<SearchCubit>().onUpdateFiltersList(
      filters: [searchItemModel.getServiceTypeName()],
    );
    context.read<SearchCubit>().onUpdateFilterType(fromCat: false);
    AutoRouter.of(context).pushAndPopUntil(
      HomeRoute(),
      predicate: (cxt) => false,
    );
  }
}
