part of 'search_screen_widgets_imports.dart';

class BuildSearchScreenAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  final SearchScreenData searchScreenData;
  final DeviceModel model;
  final bool isFocus;

  const BuildSearchScreenAppBar(
      {Key? key,
      required this.searchScreenData,
      required this.model,
      required this.isFocus})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.primary,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(5).r,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 6).r,
                child: DefaultAppBar(
                  backgroundBack: Colors.transparent,
                  titleWidget: MyText(
                    title: tr("find_your_favorite_services"),
                    color: MyColors.white,

                    size: model.isTablet ? 10.sp : 14.sp,
                  ),
                  leading: Container(
                    margin: EdgeInsetsDirectional.only(
                      start: model.isTablet ? 2.sp : 14.sp,
                    ),
                    decoration: BoxDecoration(
                        color: MyColors.white.withOpacity(.2.r),
                        shape: BoxShape.circle),
                    child: IconButton(
                      icon: Icon(
                        model.locale == Locale('en', 'US')
                            ? Icons.west
                            : Icons.east,
                        color: MyColors.white,
                        size: model.isTablet ? 10.sp : 18.sp,
                      ),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5),
              BuildSearchInput(
                searchScreenData: searchScreenData,
                isFocus: isFocus,
              ),
              SizedBox(height: 10),
              BuildDateTimeFilterInput(
                searchScreenData: searchScreenData,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(model.isTablet ? 160.h : (Platform.isIOS? 210 : 195));
}
