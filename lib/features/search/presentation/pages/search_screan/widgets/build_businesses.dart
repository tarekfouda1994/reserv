part of 'search_screen_widgets_imports.dart';

class BuildBusinesses extends StatelessWidget {
  final DeviceModel model;
  final SearchScreenData data;

  const BuildBusinesses({Key? key, required this.model, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<BusinessSearchModel?>, GenericState<BusinessSearchModel?>>(
      bloc: data.businessSearchBloc,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return BlocBuilder<GenericBloc<String>, GenericState<String>>(
            bloc: data.filterBloc,
            builder: (context, filterState) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  BuildDeptHeader(
                    count: state.data!.totalRecords,
                    title: tr("businesses"),
                    moreWidget: Row(
                      children: [
                        InkWell(
                          onTap: () => AutoRouter.of(context).push(
                            BusinessSearchPageRoute(
                                filter: data.searchController.text.isNotEmpty
                                    ? data.searchController.text
                                    : filterState.data),
                          ),
                          child: Visibility(
                            visible: state.data!.itemsList.length != 0,
                            child: Row(
                              children: [
                                MyText(
                                    title: "${tr("View_all")} ",
                                    color: MyColors.blackOpacity,

                                    size: model.isTablet ? 8.sp : 11.sp),
                                Icon(
                                  model.locale == Locale('en', 'US') ? Icons.east : Icons.west,
                                  color: MyColors.primary,
                                  size: model.isTablet ? 13.sp : 18.sp,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: model.isTablet ? 8.h : 6.h),
                  Visibility(
                    visible: state.data!.itemsList.isNotEmpty,
                    replacement: Padding(
                      padding: EdgeInsets.only(bottom: 25, top: 10).r,
                      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                        SvgPicture.asset(
                          Res.EmptyBusinesses,
                          width: 40.r,
                          height: 40.r,
                        ),
                        MyText(
                            title: " ${tr("noBusinessesFound")} ",
                            color: MyColors.grey,
                            size: model.isTablet ? 8.sp : 11.sp),
                      ]),
                    ),
                    child: Column(
                      children: List.generate(
                        state.data!.itemsList.length,
                        (index) => BuildBusinessesItem(
                          model: model,
                          businessItemSearchModel: state.data!.itemsList[index],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        } else {
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              BuildDeptHeader(
                count: 0,
                title: tr("businesses"),
                hasMore: false,
              ),
              SizedBox(height: model.isTablet ? 8.h : 6.h),
              ...List.generate(
                5,
                (index) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: BuildShimmerView(
                      height: model.isTablet ? 45 : 35,
                      width: MediaQuery.of(context).size.width * 28),
                ),
              ),
            ],
          );
        }
      },
    );
  }
}
