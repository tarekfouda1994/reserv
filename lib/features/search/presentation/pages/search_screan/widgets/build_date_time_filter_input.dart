part of 'search_screen_widgets_imports.dart';

class BuildDateTimeFilterInput extends StatelessWidget {
  final SearchScreenData searchScreenData;

  const BuildDateTimeFilterInput({Key? key, required this.searchScreenData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<SearchCubit, SearchState>(builder: (context, state) {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 11),
        constraints: BoxConstraints(
          minHeight: 50,
        ),
        padding: EdgeInsetsDirectional.only(start: 15, end: 6),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: MyColors.white,
        ),
        child: InkWell(
          onTap: () async {
            await AutoRouter.of(context).push(DateTimePageRoute()).then((value) {
              if (searchScreenData.searchController.text.isNotEmpty) {
                searchScreenData.getBusinessSearch(
                  context,
                  searchScreenData.searchController.text,
                );
              }
            });
          },
          child: Visibility(
            visible: state.dateTime.timeList.isEmpty && state.dateTime.dateList.isEmpty,
            child: Row(children: [
              SvgPicture.asset(
                Res.clock_five,
                color: MyColors.grey.withOpacity(.5),
                height: 16.sp,
                width: 16.sp,
              ),
              SizedBox(width: 6),
              MyText(
                title: "  ${tr("select_dateTime")}",
                color: Colors.black54,
                size: device.isTablet ? 7.sp : 9.sp,
              )
            ]),
            replacement: Row(children: [
              SvgPicture.asset(
                Res.clock_five,
                color: MyColors.grey.withOpacity(.5),
                height: 16.sp,
                width: 16.sp,
              ),
              SizedBox(width: 12),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      title: state.dateTime.dateList.length != 0
                          ? buildFormat(
                              context,
                              state.dateTime.dateList.firstOrNull ?? DateTime(0),
                            )
                          : '',
                      color: MyColors.primary,
                      size: device.isTablet ? 7.sp : 10.sp),
                  Visibility(
                    visible: state.dateTime.dateList.length > 1,
                    child: MyText(
                      title: state.dateTime.dateList.length > 1
                          ? buildFormat(context, state.dateTime.dateList.last)
                          : '',
                      color: MyColors.primary,
                      size: device.isTablet ? 7.sp : 10.sp,
                    ),
                  ),
                ],
              ),
              Visibility(
                visible: state.dateTime.dateList.length > 2,
                child: Container(
                  height: 25,
                  width: 25,
                  padding: EdgeInsets.all(3).r,
                  margin: EdgeInsets.symmetric(horizontal: 6),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: MyColors.primaryLight,
                    ),
                  ),
                  alignment: Alignment.center,
                  child: FittedBox(
                    child: MyText(
                      title: "${state.dateTime.dateList.length - 2}",
                      color: MyColors.primaryDark,
                      size: device.isTablet ? 7.sp : 11.sp,
                    ),
                  ),
                ),
              ),
              Spacer(),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      title: _localRange(context, state.dateTime.timeList.firstOrNull?.title ?? ""),
                      color: MyColors.primary,
                      size: device.isTablet ? 7.sp : 10.sp),
                  Visibility(
                    visible: state.dateTime.timeList.length > 1,
                    child: MyText(
                        title: state.dateTime.timeList.length > 1
                            ? _localRange(context, state.dateTime.timeList.last.title)
                            : '',
                        color: MyColors.primary,
                        size: device.isTablet ? 7.sp : 10.sp),
                  ),
                ],
              ),
              Visibility(
                visible: state.dateTime.timeList.length > 2,
                child: Container(
                  height: 25,
                  width: 25,
                  padding: EdgeInsets.all(3).r,
                  margin: EdgeInsets.symmetric(horizontal: 6),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: MyColors.primaryLight,
                    ),
                  ),
                  alignment: Alignment.center,
                  child: FittedBox(
                    child: MyText(
                      title: "${state.dateTime.timeList.length - 2}",
                      color: MyColors.primaryDark,
                      size: device.isTablet ? 7.sp : 11.sp,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 18.w),
              InkWell(
                onTap: () => context.read<SearchCubit>().onUpdate(
                    filter: searchScreenData.searchController.text,
                    dateTime: SelectedDateTimeEntity(timeList: [], dateList: [])),
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 8),
                  padding: const EdgeInsets.all(2.5),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: MyColors.primary),
                      color: MyColors.primary),
                  child: Icon(Icons.close,
                      color: MyColors.white, size: device.isTablet ? 10.sp : 12.sp),
                ),
              ),
            ]),
          ),
        ),
      );
    });
  }

  String _localRange(BuildContext context, String value) {
    if (value.isNotEmpty) {
      return getIt<Utilities>().localizedRangeNumber(context, value);
    } else {
      return "";
    }
  }

  String buildFormat(BuildContext context, DateTime date) {
    String locale = context.watch<DeviceCubit>().state.model.locale.languageCode;
    return DateFormat("dd MMM yyyy", locale).format(
      date,
    );
  }
}
