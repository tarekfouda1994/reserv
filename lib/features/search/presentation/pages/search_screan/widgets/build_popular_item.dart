part of 'search_screen_widgets_imports.dart';

class BuildPopularItem extends StatelessWidget {
  final DeviceModel model;
  final PopularSearchModel searchModel;
  final int index;
  final SearchScreenData data;

  const BuildPopularItem(
      {Key? key,
      required this.model,
      required this.data,
      required this.searchModel,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          borderRadius: BorderRadius.circular(100),
          onTap: () {
            data.searchController.text = searchModel.getPopularSearchName();
            data.onSelectSearchStr(context, searchModel.getPopularSearchName());
          },
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: model.isTablet ? 10.w : 15.w, vertical: 8.h),
            decoration: BoxDecoration(
              border: Border.all(
                color: MyColors.primary,
              ),
              borderRadius: BorderRadius.circular(40.r),
            ),
            child: MyText(
              title: searchModel.getPopularSearchName(),
              color: MyColors.grey,

              size: model.isTablet ? 6.sp : 10.sp,
            ),
          ),
        ),
        Visibility(
          visible: index == 4 &&
              data.popularSearchBloc.state.data.length == 5 &&
              data.popularSearchList.length - 5 != 0,
          child: InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () =>
                data.popularSearchBloc.onUpdateData(data.popularSearchList),
            child: Container(
              margin: EdgeInsetsDirectional.only(start: 10),
              padding: EdgeInsets.symmetric(
                  horizontal: model.isTablet ? 15.w : 21.w, vertical: 7.h),
              decoration: BoxDecoration(
                border: Border.all(color: MyColors.primary),
                color: MyColors.primary.withOpacity(.05),
                borderRadius: BorderRadius.circular(40.r),
              ),
              child: MyText(
                  title: "+${data.popularSearchList.length - 5} ${tr("more")}",
                  color: MyColors.primary,
                  size: model.isTablet ? 6.sp : 10.sp),
            ),
          ),
        ),
        Visibility(
          visible: index > 4 && data.popularSearchList.length - 1 == index,
          child: InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () => data.popularSearchBloc
                .onUpdateData(data.popularSearchList.take(5).toList()),
            child: Container(
              margin: EdgeInsetsDirectional.only(start: 10),
              padding: EdgeInsets.symmetric(
                  horizontal: model.isTablet ? 20.w : 26.w, vertical: 7.h),
              decoration: BoxDecoration(
                border: Border.all(color: MyColors.primary),
                color: MyColors.primary.withOpacity(.05),
                borderRadius: BorderRadius.circular(40.r),
              ),
              child: MyText(
                title: tr("less"),
                color: MyColors.primary,
                size: model.isTablet ? 6.sp : 10.sp,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
