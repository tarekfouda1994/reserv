part of 'search_screen_widgets_imports.dart';

class BuildSuffixIconWidget extends StatelessWidget {
  final SearchScreenData searchScreenData;
  final DeviceModel model;

  const BuildSuffixIconWidget(
      {Key? key, required this.searchScreenData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<String>, GenericState<String>>(
        bloc: searchScreenData.filterBloc,
        builder: (context, filterState) {
          return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: searchScreenData.searchViewBloc,
              builder: (cxt, stateView) {
                return Visibility(
                  visible: (filterState.data.isNotEmpty || stateView.data),
                  child: InkWell(
                    onTap: () => searchScreenData.onCloseSearch(context),
                    child: Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 8, vertical: 12.h),
                      padding: const EdgeInsets.all(2.5),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: MyColors.primary),
                          color: MyColors.primary),
                      child: Icon(Icons.close,
                          color: MyColors.white,
                          size: model.isTablet ? 10.sp : 12.sp),
                    ),
                  ),
                );
              });
        });
  }
}
