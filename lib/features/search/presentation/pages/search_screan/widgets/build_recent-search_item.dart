part of 'search_screen_widgets_imports.dart';

class BuildRecentSearchItem extends StatelessWidget {
  final DeviceModel model;
  final String title;
  final int index;
  final bool recentOrPopular;
  final SearchScreenData searchScreenData;

  const BuildRecentSearchItem(
      {Key? key,
      required this.model,
      required this.title,
      required this.recentOrPopular,
      required this.searchScreenData,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          borderRadius: BorderRadius.circular(100),
          onTap: () {
            searchScreenData.searchController.text = title;
            searchScreenData.onSelectSearchStr(context, title);
          },
          child: Chip(
            backgroundColor: MyColors.white,
            deleteIconColor: MyColors.primary,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: MyColors.primary),
              borderRadius: BorderRadius.circular(30.r),
            ),
            label: Padding(
              padding: EdgeInsets.all(model.isTablet ? 8.0.r : 0),
              child: MyText(
                title: title,
                color: MyColors.grey,
                size: model.isTablet ? 6.sp : 10.sp,
              ),
            ),
            onDeleted: () => searchScreenData.removeItem(index),
            deleteIcon: Icon(Icons.close, size: model.isTablet ? 10.sp : 14),
          ),
        ),
        Visibility(
          visible: index == 4 &&
              searchScreenData.recentSearchCubit.state.data.length == 5 &&
              searchScreenData.recentSearchList.length - 5 != 0,
          child: InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () => searchScreenData.recentSearchCubit
                .onUpdateData(searchScreenData.recentSearchList),
            child: Container(
              margin: EdgeInsetsDirectional.only(start: 10),
              padding: EdgeInsets.symmetric(
                  horizontal: model.isTablet ? 15.w : 21.w, vertical: 7.h),
              decoration: BoxDecoration(
                border: Border.all(color: MyColors.primary),
                color: MyColors.primary.withOpacity(.05),
                borderRadius: BorderRadius.circular(40.r),
              ),
              child: MyText(
                title:
                    "+${searchScreenData.recentSearchList.length - 5} ${tr("more")}",
                color: MyColors.primary,
                size: model.isTablet ? 6.sp : 10.sp,
              ),
            ),
          ),
        ),
        Visibility(
          visible: index > 4 &&
              searchScreenData.recentSearchList.length - 1 == index,
          child: InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () => searchScreenData.recentSearchCubit.onUpdateData(
              searchScreenData.recentSearchList.take(5).toList(),
            ),
            child: Container(
              margin: EdgeInsetsDirectional.only(start: 10),
              padding: EdgeInsets.symmetric(
                  horizontal: model.isTablet ? 20.w : 26.w, vertical: 7.h),
              decoration: BoxDecoration(
                border: Border.all(color: MyColors.primary),
                color: MyColors.primary.withOpacity(.05),
                borderRadius: BorderRadius.circular(40.r),
              ),
              child: MyText(
                title: tr("less"),
                color: MyColors.primary,
                size: model.isTablet ? 6.sp : 10.sp,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
