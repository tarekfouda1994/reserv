part of 'search_screen_widgets_imports.dart';

class BuildRecentView extends StatelessWidget {
  final DeviceModel model;
  final SearchScreenData data;

  const BuildRecentView({Key? key, required this.model, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      children: [
        BuildRecentCatSearch(catName: tr("recent_searches"), model: model, data: data),
        const SizedBox(height: 20),
        BuildPopularSearch(catName: tr("popular_searches"), model: model, data: data),
      ],
    );
  }
}
