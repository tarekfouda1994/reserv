part of 'search_screen_widgets_imports.dart';

class BuildSearchInput extends StatelessWidget {
  final SearchScreenData searchScreenData;
  final bool isFocus;

  const BuildSearchInput(
      {Key? key, required this.searchScreenData, required this.isFocus})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<SearchCubit, SearchState>(builder: (context, state) {
      return GenericTextField(
        autoFocus: true,
        textDirection: Directionality.of(context),
        controller: searchScreenData.searchController,
        onSubmit: () => searchScreenData.onSubmitFilter(
            context, searchScreenData.searchController.text),
        margin: const EdgeInsets.symmetric(horizontal: 11),
        suffixIcon: BuildSuffixIconWidget(
          model: device,
          searchScreenData: searchScreenData,
        ),
        hintTextSize: device.isTablet ? 7.sp : 9.sp,
        fieldTypes: FieldTypes.normal,
        radius: BorderRadius.circular(8),
        type: TextInputType.text,
        action: TextInputAction.search,
        validate: (value) {},
        onChange: (v) => searchScreenData.onSearchTextChanged(v, context),
        hint: state.filter.isEmpty
            ? tr("find_your_favorite_services_around")
            : state.filter,
        prefixIcon: Transform.scale(
          scale: device.isTablet ? .5 : .35,
          child: SvgPicture.asset(Res.Search),
        ),
        fillColor: MyColors.white,
        contentPadding: const EdgeInsets.symmetric(
          vertical: 15,
          horizontal: 10,
        ).r,
      );
    });
  }
}
