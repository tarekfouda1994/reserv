part of 'search_screen_widgets_imports.dart';

class BuildServiceSearch extends StatelessWidget {
  final DeviceModel model;
  final SearchScreenData searchScreenData;

  const BuildServiceSearch({
    Key? key,
    required this.model,
    required this.searchScreenData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<ServiceSearchModel?>,
            GenericState<ServiceSearchModel?>>(
        bloc: searchScreenData.serviceSearchBloc,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            return BlocBuilder<GenericBloc<String>, GenericState<String>>(
                bloc: searchScreenData.filterBloc,
                builder: (context, filterState) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BuildDeptHeader(
                        count: state.data!.totalRecords,
                        title: tr("services"),
                        moreWidget: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                var cubit = context.read<SearchCubit>();
                                cubit.onUpdateFiltersList(
                                    filters: _filtersNames());
                                cubit.onUpdateFilterType(fromCat: false);
                                AutoRouter.of(context).pushAndPopUntil(
                                  HomeRoute(),
                                  predicate: (route) => false,
                                );
                              },
                              child: Visibility(
                                visible: state.data!.itemsList.length != 0,
                                child: Row(
                                  children: [
                                    MyText(
                                        title: "${tr("View_all")} ",
                                        color: MyColors.grey,
                                        size: model.isTablet ? 8.sp : 11.sp),
                                    Icon(
                                      model.locale == Locale('en', 'US')
                                          ? Icons.east
                                          : Icons.west,
                                      color: MyColors.primary,
                                      size: model.isTablet ? 13.sp : 18.sp,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: model.isTablet ? 8.h : 6.h),
                      Visibility(
                        visible: state.data!.itemsList.isNotEmpty,
                        replacement: Padding(
                          padding: EdgeInsets.only(bottom: 25, top: 10).r,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  Res.EmptyServices,
                                  width: 40.r,
                                  height: 40.r,
                                ),
                                MyText(
                                    title: " ${tr("noServicesFound")} ",
                                    color: MyColors.grey,
                                    size: model.isTablet ? 8.sp : 11.sp),
                              ]),
                        ),
                        child: Column(
                          children: List.generate(
                            state.data!.itemsList.length,
                            (index) => Visibility(
                              visible: index < 5,
                              child: BuildServiceSearchItem(
                                model: model,
                                searchItemModel: state.data!.itemsList[index],
                                currentSearchKeyword:
                                    searchScreenData.currentSearchKeyword,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  );
                });
          } else {
            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BuildDeptHeader(
                    count: 0, title: tr("services"), hasMore: false),
                SizedBox(height: model.isTablet ? 8.h : 6.h),
                ...List.generate(
                  5,
                  (index) => BuildShimmerView(
                      height: model.isTablet ? 45 : 35,
                      width: MediaQuery.of(context).size.width * .9),
                ),
              ],
            );
          }
        });
  }

  List<String> _filtersNames() =>
      searchScreenData.serviceSearchBloc.state.data!.itemsList
          .map((e) => e.getServiceTypeName())
          .toList();
}
