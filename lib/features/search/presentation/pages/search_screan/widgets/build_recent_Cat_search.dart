part of 'search_screen_widgets_imports.dart';

class BuildRecentCatSearch extends StatelessWidget {
  final String catName;
  final DeviceModel model;
  final SearchScreenData data;

  const BuildRecentCatSearch(
      {Key? key,
      required this.catName,
      required this.model,
      required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<String>>, GenericState<List<String>>>(
      bloc: data.recentSearchCubit,
      builder: (context, state) {
        if (state.data.isEmpty) {
          return Container();
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildDeptHeader(
                title: catName,
                paddingHorizontal: 0,
                count: data.recentSearchList.length,
                hasMore: true,
                moreWidget: GestureDetector(
                  onTap: () => data.showConfirmCancelDialog(context),
                  child: Row(
                    children: [
                      MyText(
                        title: tr("remove_all") + " ",
                        color: MyColors.errorColor,
                        size: model.isTablet ? 6.sp : 9.5.sp,
                      ),
                      Icon(
                        Icons.clear,
                        size: model.isTablet ? 10.sp : 12.sp,
                        color: MyColors.errorColor,
                      )
                    ],
                  ),
                )),
            const SizedBox(height: 3),
            Wrap(
              spacing: model.isTablet ? 3.w : 5.w,
              children: List.generate(
                state.data.length,
                (index) {
                  return BuildRecentSearchItem(
                    searchScreenData: data,
                    recentOrPopular: true,
                    model: model,
                    title: state.data[index],
                    index: index,
                  );
                },
              ),
            ),
          ],
        );
      },
    );
  }
}
