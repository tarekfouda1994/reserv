part of 'search_screen_widgets_imports.dart';

class BuildPopularSearch extends StatelessWidget {
  final String catName;
  final DeviceModel model;
  final SearchScreenData data;

  const BuildPopularSearch(
      {Key? key,
      required this.catName,
      required this.model,
      required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<PopularSearchModel>>,
        GenericState<List<PopularSearchModel>>>(
      bloc: data.popularSearchBloc,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          if (state.data.isEmpty) {
            return Container();
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BuildDeptHeader(
                  paddingHorizontal: 0,
                  title: catName,
                  count: data.popularSearchList.length,
                  hasMore: false),
              const SizedBox(height: 3),
              Wrap(
                runSpacing: model.isTablet ? 9.h : 8.h,
                spacing: model.isTablet ? 5.w : 5.w,
                children: List.generate(
                  state.data.length,
                  (index) {
                    return BuildPopularItem(
                      data: data,
                      model: model,
                      searchModel: state.data[index],
                      index: index,
                    );
                  },
                ),
              ),
            ],
          );
        }
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildDeptHeader(title: catName, hasMore: false),
            const SizedBox(
              height: 10,
            ),
            ...List.generate(
              5,
              (index) => BuildShimmerView(
                  height: model.isTablet ? 45 : 35,
                  width: MediaQuery.of(context).size.width * .9),
            )
          ],
        );
      },
    );
  }
}
