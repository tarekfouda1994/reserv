part of 'search_screen_imports.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final SearchScreenData searchScreenData = SearchScreenData();
  Timer? _debounce;

  @override
  void initState() {
    searchScreenData.getPopularSearch(context, refresh: false);
    searchScreenData.getPopularSearch(context);
    searchScreenData.retrieveStringListValue();
    searchScreenData.searchController.addListener(_onSearchChanged);
    super.initState();
  }

  _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(Duration(milliseconds: 500), () {
      if (searchScreenData.searchController.text != "") {
        ///here you perform your search
        searchScreenData.doSearch(searchScreenData.searchController.text, context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var focusScope = FocusScope.of(context);
    var device = context.watch<DeviceCubit>().state.model;
    return GestureDetector(
      onTap: () => focusScope.unfocus(),
      child: Scaffold(
        backgroundColor: MyColors.white,
        appBar: BuildSearchScreenAppBar(
          searchScreenData: searchScreenData,
          model: device,
          isFocus: focusScope.hasFocus,
        ),
        body: BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
          bloc: searchScreenData.searchViewBloc,
          builder: (cxt, state) {
            return Padding(
              padding: EdgeInsets.only(top: 10),
              child: Visibility(
                visible: !state.data,
                child: BuildRecentView(model: device, data: searchScreenData),
                replacement: BuildOnSearchView(
                  model: device,
                  data: searchScreenData,
                ),
              ),
            );
          },
        ),
      ),
    );
  }


  @override
  void dispose() {
    searchScreenData.searchController.removeListener(_onSearchChanged);
    searchScreenData.searchController.dispose();
    super.dispose();
  }
}
