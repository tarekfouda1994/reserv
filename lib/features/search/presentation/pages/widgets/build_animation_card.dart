import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class BuildAnimationCard extends StatelessWidget {
  final Widget child;
  final Widget screen;

  const BuildAnimationCard({
    Key? key,
    required this.child,
    required this.screen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedElevation: 0,
      openElevation: 0,
      transitionDuration: Duration(milliseconds: 800),
      closedColor: Colors.transparent,
      middleColor: Colors.transparent,
      openColor: Colors.white,
      closedBuilder: (_, action) => child,
      openBuilder: (_, action) => screen,
    );
  }
}
