import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';

class BuildIconContainer extends StatelessWidget {
  final Widget child;
  final Color bgColor;
  final Color? borderColor;
  final double? width, height;
  final Function()? onTap;
  final bool hasDecoration;

  const BuildIconContainer({
    Key? key,
    required this.child,
    required this.bgColor,
    this.onTap,
    this.height,
    this.width,
    this.borderColor,
    this.hasDecoration = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width ?? (device.isTablet ? 22.r : 25.r),
        height: height ?? (device.isTablet ? 22.r : 25.r),
        decoration: hasDecoration
            ? BoxDecoration(
                color: bgColor,
                shape: BoxShape.circle,
                border: Border.all(
                  color: borderColor ?? MyColors.primary,
                ),
              )
            : null,
        alignment: Alignment.center,
        child: child,
      ),
    );
  }
}
