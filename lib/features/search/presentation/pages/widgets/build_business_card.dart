import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/business_placeholder.dart';
import 'package:flutter_tdd/features/profile/data/model/businesse_favourite_model/businesse_favourite_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildBusinessCard extends StatelessWidget {
  final BusinesseFavouriteModel businesseFavouriteModel;
  final void Function() updateWish;
  final void Function() shareLink;

  const BuildBusinessCard(
      {Key? key,
      required this.businesseFavouriteModel,
      required this.updateWish,
      required this.shareLink})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return InkWell(
      onTap: () => AutoRouter.of(context)
          .push(ProductDetailsRoute(id: businesseFavouriteModel.id)),
      child: Container(
        margin: EdgeInsets.all(15),
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.borderCard),
            borderRadius: BorderRadius.circular(11.r),
            color: MyColors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, .1),
                offset: Offset(0.0, 2.0),
                blurRadius: 20.0,
              ),
            ]),
        child: Column(
          children: [
            Center(
              child: CachedImage(
                url: businesseFavouriteModel.businesssImage
                    .replaceAll("\\", "/"),
                height: device.isTablet ? 240.sm : 170.sm,
                fit: BoxFit.cover,
                bgColor: MyColors.defaultImgBg,
                borderWidth: 0,
                placeHolder: BusinessPlaceholder(),
                borderRadius: BorderRadius.vertical(top: Radius.circular(10.r)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            InkWell(
                              onTap: shareLink,
                              child: Container(
                                width: 30.r,
                                height: 30.r,
                                margin: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: MyColors.white),
                                  shape: BoxShape.circle,
                                  color: MyColors.favouriteBg,
                                ),
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.share_outlined,
                                  size: device.isTablet ? 12.sp : 17.sp,
                                  color: MyColors.white,
                                ),
                              ),
                            ),
                            BuildFavouriteIcon(
                              onTap: updateWish,
                              hasWish: true,
                            )
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                            vertical: 6.h,
                            horizontal: device.isTablet ? 6.w : 10.w,
                          ),
                          decoration: BoxDecoration(
                            color: MyColors.black,
                            borderRadius: BorderRadius.only(
                              topLeft: isEn
                                  ? Radius.circular(14.r)
                                  : Radius.circular(0),
                              topRight: isEn
                                  ? Radius.circular(0)
                                  : Radius.circular(14.r),
                            ),
                          ),
                          child: MyText(
                            title:
                                "${businesseFavouriteModel.numberOfService} ${tr("services")}",
                            color: MyColors.white,
                            fontFamily: CustomFonts.primarySemiBoldFont,
                            size: device.isTablet ? 7.sp : 11.sp,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Transform.scale(
                        scale: .8,
                        child: SvgPicture.asset(
                          Res.building,
                          width: device.isTablet ? 12.w : null,
                        ),
                      ),
                      Expanded(
                        child: MyText(
                          title: " " + businesseFavouriteModel.getServiceName(),
                          color: MyColors.black,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 11.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(width: 5),
                      MyText(
                        title: getIt<Utilities>().convertNumToAr(
                          context: context,
                          value: businesseFavouriteModel.rating.toString(),
                        ),
                        color: MyColors.amber,
                        size: device.isTablet ? 7.sp : 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(width: 4.sp),
                      SvgPicture.asset(
                        Res.star,
                        color: MyColors.amber,
                        width: 14.r,
                        height: 14.r,
                      ),
                    ],
                  ),
                  Divider(color: MyColors.white, height: 10),
                  Row(
                    children: [
                      Transform.scale(
                          scale: .9,
                          child: SvgPicture.asset(Res.marker_input,
                              width: 14.sp, height: 15.sp)),
                      Expanded(
                        child: MyText(
                          title: " ${businesseFavouriteModel.getAddressName()}",

                          color: MyColors.black,
                          overflow: TextOverflow.ellipsis,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Transform.scale(
                          scale: .9,
                          child: SvgPicture.asset(
                            Res.marker_input,
                            width: 14.sp,
                            height: 15.sp,
                            color: MyColors.white,
                          )),
                      MyText(
                        title: businesseFavouriteModel.distance.isNotEmpty
                            ? "  ${businesseFavouriteModel.distance} ${tr("away")}"
                            : "",
                        color: MyColors.black,
                        size: device.isTablet ? 6.sp : 9.sp,
                        fontWeight: FontWeight.w600,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(width: 4.w),
                      InkWell(
                        onTap: () => getIt<Utilities>().trackingBusinessOnMap(
                          businesseFavouriteModel.latitude,
                          businesseFavouriteModel.longitude,
                          businesseFavouriteModel.getBusinessName(),
                          businesseFavouriteModel.getAddressName(),
                        ),
                        child: Row(
                          children: [
                            SvgPicture.asset(Res.location_arrow_icon),
                            SizedBox(width: 2),
                            MyText(
                                decoration: TextDecoration.underline,
                                title: tr("direction"),
                                color: Color(0xff4278F6),
                                size: device.isTablet ? 5.5.sp : 9.sp),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
