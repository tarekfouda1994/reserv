import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';

class BuildFavouriteIcon extends StatelessWidget {
  final void Function() onTap;
  final bool hasWish;

  const BuildFavouriteIcon(
      {Key? key, required this.onTap, required this.hasWish})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(100),
      child: Container(
        width: 30.r,
        height: 30.r,
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.white),
            shape: BoxShape.circle,
            color: hasWish ? MyColors.activeFavouriteBg : MyColors.favouriteBg),
        alignment: Alignment.center,
        child: Icon(
          Icons.favorite,
          size: device.isTablet ? 12.sp : 17.sp,
          color: MyColors.white,
        ),
      ),
    );
  }
}
