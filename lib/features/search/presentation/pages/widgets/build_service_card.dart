import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/enums.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/service_placeholder.dart';
import 'package:flutter_tdd/features/search/data/models/trending_services_item_model/trending_services_item_model.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_favourit_icon.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildServiceCard extends StatelessWidget {
  final TrendingServicesItemModel serviceModel;
  final bool fromHome;
  final void Function() onUpdateWish;
  final HomeDepartments homeDepartments;

  const BuildServiceCard(
      {Key? key,
      this.fromHome = false,
      required this.serviceModel,
      required this.onUpdateWish,
      required this.homeDepartments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    var isEn = device.locale == Locale('en', 'US');
    return Container(
      margin: EdgeInsetsDirectional.only(end: 12.w, bottom: 15.h, top: 5),
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.borderCard),
          // boxShadow: <BoxShadow>[
          //   BoxShadow(
          //     color: Color.fromRGBO(0, 0, 0, .1),
          //     offset: Offset(0.0, 2.0),
          //     blurRadius: 20.0,
          //   ),
          // ],
          borderRadius: BorderRadius.circular(11.r),
          color: MyColors.white),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () => AutoRouter.of(context).push(ReservationRootRoute(
                businessID: serviceModel.businessID,
                serviceId: serviceModel.serviceID)),
            child: Column(
              children: [
                Center(
                  child: CachedImage(
                    url: serviceModel.serviceAvatar.replaceAll("\\", "/"),
                    height: device.isTablet ? 240.sm : 160.sm,
                    fit: BoxFit.cover,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(10.r)),
                    bgColor: MyColors.defaultImgBg,
                    borderWidth: 0,
                    placeHolder: ServicePlaceholder(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Visibility(
                                  visible: serviceModel
                                      .getServiceGender()
                                      .isNotEmpty,
                                  child: Container(
                                    margin: EdgeInsets.symmetric(horizontal: 8),
                                    padding: EdgeInsets.symmetric(
                                        vertical: 6, horizontal: 16),
                                    decoration: BoxDecoration(
                                      color: MyColors.white,
                                      border: Border.all(
                                          color: MyColors.primary, width: 1),
                                      borderRadius: BorderRadius.circular(30),
                                    ),
                                    child: MyText(
                                      title: serviceModel.getServiceGender(),
                                      color: MyColors.black,
                                      size: device.isTablet ? 6.sp : 9.sp,
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: serviceModel.discount != 0,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 6, horizontal: 12),
                                    decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(30)),
                                    child: MyText(
                                      title: "${serviceModel.discount}% OFF",
                                      color: MyColors.white,
                                      size: device.isTablet ? 6.sp : 9.sp,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                // InkWell(
                                //   onTap: () => homeMainData.shareSinglePage(
                                //       context,
                                //       trendingServicesItemModel.businessID,
                                //       trendingServicesItemModel
                                //           .getServiceName(),
                                //       true),
                                //   borderRadius: BorderRadius.circular(100),
                                //   child: Container(
                                //     width: 30.r,
                                //     height: 30.r,
                                //     margin: EdgeInsets.symmetric(
                                //         vertical: 5, horizontal: 10),
                                //     decoration: BoxDecoration(
                                //       shape: BoxShape.circle,
                                //       color: MyColors.primary.withOpacity(.4),
                                //     ),
                                //     alignment: Alignment.center,
                                //     child: Icon(Icons.share_outlined,
                                //         size: device.isTablet ? 12.sp : 18.sp,
                                //         color: MyColors.white),
                                //   ),
                                // ),
                                if (serviceModel.hasWishItem != null)
                                  BuildFavouriteIcon(
                                    onTap: onUpdateWish,
                                    hasWish: serviceModel.hasWishItem!,
                                  ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: device.isTablet ? 5.sp : 8.sp,
                              horizontal: device.isTablet ? 8.sp : 10.sp),
                          decoration: BoxDecoration(
                            color: MyColors.black,
                            borderRadius: BorderRadius.only(
                              topLeft: isEn
                                  ? Radius.circular(14.r)
                                  : Radius.circular(0),
                              topRight: isEn
                                  ? Radius.circular(0)
                                  : Radius.circular(14.r),
                            ),
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              MyText(
                                  title:
                                      "${serviceModel.getCurrencyName()} ${getIt<Utilities>().convertNumToAr(context: context, value: "${serviceModel.servicePrice.round()}")}",
                                  color: MyColors.white,
                                  size: device.isTablet ? 7.sp : 11.sp),
                              SizedBox(
                                width: device.isTablet ? 8.w : 10.w,
                              ),
                              Icon(
                                Icons.access_time_outlined,
                                size: device.isTablet ? 12.sp : 15.sp,
                                color: MyColors.white,
                              ),
                              SizedBox(width: 3.w),
                              MyText(
                                title: serviceModel.durationToString,
                                color: MyColors.white,
                                size: device.isTablet ? 6.sp : 9.sp,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    children: [
                      Expanded(
                        child: MyText(
                          title: serviceModel.getServiceName(),
                          color: MyColors.black,
                          fontWeight: FontWeight.bold,
                          size: device.isTablet ? 7.sp : 11.sp,
                        ),
                      ),
                      Offstage(
                        offstage: serviceModel.serviceRating == 0,
                        child: Row(
                          children: [
                            MyText(
                              title: getIt<Utilities>().convertNumToAr(
                                context: context,
                                value: serviceModel.serviceRating.toString(),
                              ),
                              color: Color(0xffF1C800),
                              size: device.isTablet ? 7.sp : 11.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(width: 4.sp),
                            SvgPicture.asset(
                              Res.star,
                              color: Color(0xffF1C800),
                              width: 14.r,
                              height: 14.r,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => AutoRouter.of(context)
                      .push(ProductDetailsRoute(id: serviceModel.businessID)),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Transform.scale(
                                    scale: .8,
                                    child: SvgPicture.asset(Res.building,
                                        width: device.isTablet ? 12.w : null)),
                                Expanded(
                                  child: MyText(
                                    title: serviceModel.getBusiness(),
                                    color: MyColors.black,
                                    size: device.isTablet ? 7.sp : 11.sp,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                SizedBox(width: 1),
                                Offstage(
                                  offstage: serviceModel.distance(context).isEmpty,
                                  child: MyText(
                                    title: serviceModel.distance(context),
                                    color: MyColors.black,
                                    size: device.isTablet ? 5.5.sp : 9.sp,
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 4.h),
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                children: [
                                  Transform.scale(
                                    scale: .9,
                                    child: SvgPicture.asset(
                                      Res.marker_input,
                                      width: 14.sp,
                                      height: 15.sp,
                                    ),
                                  ),
                                  Expanded(
                                    child: MyText(
                                      title:
                                          "  ${serviceModel.getServicesAddress()}",
                                      color: MyColors.blackOpacity,
                                      overflow: TextOverflow.ellipsis,
                                      size: device.isTablet ? 5.5.sp : 9.sp,
                                    ),
                                  ),
                                  SvgPicture.asset(Res.location_arrow_icon),
                                  SizedBox(width: 2),
                                  InkWell(
                                    onTap: () => getIt<Utilities>()
                                        .trackingBusinessOnMap(
                                      serviceModel.latitude,
                                      serviceModel.longitude,
                                      serviceModel.getBusiness(),
                                      serviceModel.getServicesAddress(),
                                    ),
                                    child: MyText(
                                        decoration: TextDecoration.underline,
                                        title: tr("direction"),
                                        color: Color(0xff4278F6),
                                        size: device.isTablet ? 5.sp : 9.sp),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
