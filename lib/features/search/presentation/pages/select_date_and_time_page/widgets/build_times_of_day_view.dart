part of 'date_time_widgets_imports.dart';

class BuildTimesOfDayView extends StatelessWidget {
  final DateTimePageData dateTimePageData;
  final DeviceModel model;

  const BuildTimesOfDayView(
      {Key? key, required this.dateTimePageData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<SelectedItemEntity>>,
        GenericState<List<SelectedItemEntity>>>(
      bloc: dateTimePageData.selectedTimesOfDayBloc,
      builder: (cxt, state) {
        return Container(
          height: 40.h,
          margin: EdgeInsets.symmetric(vertical: 5.h),
          child: SingleChildScrollView(
            controller: dateTimePageData.timesOfDayScrollController,
            padding: const EdgeInsetsDirectional.only(start: 10),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(state.data.length, (index) {
                return InkWell(
                  onTap: () => dateTimePageData.selectTimesOfDayTime(
                    index,
                    !(state.data[index].activeItem ?? false),
                  ),
                  child: Container(
                    key: state.data[index].globalKey,
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    margin: EdgeInsets.symmetric(
                        vertical: model.isTablet ? 2.h : 4.h, horizontal: 3.w),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: state.data[index].activeItem!
                          ? MyColors.primary
                          : MyColors.primary.withOpacity(.05),
                    ),
                    alignment: Alignment.center,
                    child: MyText(
                      alien: TextAlign.center,
                      size: model.isTablet ? 7.sp : 10.sp,
                      title: titleRange(state, index, context),
                      color: state.data[index].activeItem!
                          ? MyColors.white
                          : MyColors.primary,
                    ),
                  ),
                );
              }),
            ),
          ),
        );
      },
    );
  }

  String titleRange(GenericState<List<SelectedItemEntity>> state, int index,
          BuildContext context) =>
      "${state.data[index].title} ${_localRange(context, state.data[index].from!)} - ${_localRange(context, state.data[index].to!)}";

  String _localRange(BuildContext context, String value) {
    return getIt<Utilities>().localizedRangeNumber(context, value);
  }
}
