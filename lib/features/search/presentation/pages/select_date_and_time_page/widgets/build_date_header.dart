part of 'date_time_widgets_imports.dart';

class BuildDateHeader extends StatelessWidget {
  final DateTimePageData dateTimePageData;
  final DeviceModel model;

  const BuildDateHeader({Key? key, required this.dateTimePageData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          // Spacer(),
          InkWell(
            onTap: () => dateTimePageData.removeAllDate(),
            borderRadius: BorderRadius.circular(50),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                      vertical: model.isTablet ? 6.h : 8.h, horizontal: model.isTablet ? 3.w : 5.w),
                  padding: EdgeInsets.all(3.r),
                  decoration: BoxDecoration(color: MyColors.primary, shape: BoxShape.circle),
                  child:
                      Icon(Icons.clear, size: model.isTablet ? 8.sp : 10.sp, color: MyColors.white),
                ),
                MyText(
                    title: tr("unselected_all_dates"),
                    color: MyColors.blackOpacity,
                    size: model.isTablet ? 6.sp : 10.sp),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
