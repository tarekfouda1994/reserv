import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/widgets/custom_calender/custom_calender_imports.dart';
import 'package:flutter_tdd/features/search/domain/entites/selected_item_entites.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/constants/my_colors.dart';
import '../../../../../../core/localization/localization_methods.dart';
import '../date_time_page_Imports.dart';

part 'build_date_header.dart';
part 'build_selection_date_options.dart';
part 'build_selection_time_options.dart';
part 'build_time_header.dart';
part 'build_time_view.dart';
part 'build_times_of_day_view.dart';
