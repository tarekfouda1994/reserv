part of 'date_time_widgets_imports.dart';

class BuildSelectionDateOption extends StatelessWidget {
  final DateTimePageData dateTimePageData;
  final DeviceModel model;

  const BuildSelectionDateOption(
      {Key? key, required this.dateTimePageData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<SelectedItemEntity>>,
        GenericState<List<SelectedItemEntity>>>(
      bloc: dateTimePageData.selectedDateBloc,
      builder: (cxt, state) {
        return Column(
          children: [
            BlocBuilder<GenericBloc<List<DateTime>>,
                GenericState<List<DateTime>>>(
              bloc: dateTimePageData.dateCubit,
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 25),
                  child: Column(
                    children: [
                      Container(
                        height: 33.h,
                        child: Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsetsDirectional.only(start: 16),
                              child: SvgPicture.asset(Res.calendar,
                                  color: MyColors.grey.withOpacity(.5),
                                  height: 15.h,
                                  width: 15.w),
                            ),
                            MyText(
                              title: "  ${tr("select_date")}",
                              color: MyColors.black,
                              size: model.isTablet ? 7.sp : 11.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            Spacer(),
                            Offstage(
                              offstage: state.data.isEmpty,
                              child: BuildDateHeader(
                                dateTimePageData: dateTimePageData,
                                model: model,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: model.isTablet ? 10.h : 13.h),
                      CustomCalender(
                        controller: dateTimePageData.dateScrollController,
                        showRowTitle: false,
                        selectedDates: state.data,
                        disableDates: [],
                        onSelect: (disable, date) =>
                            dateTimePageData.onSelectDate(date),
                      ),
                    ],
                  ),
                );
              },
            ),
            Divider(height: 0),
            Container(
              height: 40.h,
              // margin: EdgeInsets.symmetric(vertical: 5.h),
              margin: EdgeInsets.symmetric(vertical: 16.h),
              child: ListView.builder(
                padding: const EdgeInsetsDirectional.only(start: 10),
                itemCount: state.data.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (cxt, index) {
                  return InkWell(
                    onTap: () =>
                        dateTimePageData.selectDatePeriodItem(context, index),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                      margin:
                          EdgeInsets.symmetric(vertical: 4.h, horizontal: 3.w),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: state.data[index].activeItem == true
                            ? MyColors.primary
                            : MyColors.primary.withOpacity(.05),
                      ),
                      alignment: Alignment.center,
                      child: MyText(
                          alien: TextAlign.center,
                          size: model.isTablet ? 7.sp : 10.sp,
                          title: state.data[index].title,
                          color: state.data[index].activeItem == true
                              ? MyColors.white
                              : MyColors.primary),
                    ),
                  );
                },
              ),
            )
          ],
        );
      },
    );
  }
}
