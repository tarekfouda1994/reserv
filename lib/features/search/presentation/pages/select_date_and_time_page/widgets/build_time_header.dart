part of 'date_time_widgets_imports.dart';

class BuildTimeHeader extends StatelessWidget {
  final DateTimePageData dateTimePageData;
  final DeviceModel model;

  const BuildTimeHeader({Key? key, required this.dateTimePageData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 33.h,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SvgPicture.asset(Res.clock_five,
              color: MyColors.grey.withOpacity(.5), height: 15.h, width: 15.w),
          MyText(
            title: "  ${tr("select_time_slot")}",
            color: MyColors.black,
            size: model.isTablet ? 7.sp : 11.sp,
            fontWeight: FontWeight.bold,
          ),
          Spacer(),
          Visibility(
            visible: dateTimePageData.selectedTimeBloc.state.data.isNotEmpty,
            child: InkWell(
              onTap: () => dateTimePageData.removeAllTime(),
              borderRadius: BorderRadius.circular(50),
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(color: MyColors.primary, shape: BoxShape.circle),
                    child: Icon(Icons.clear, size: 10.sp, color: MyColors.white),
                  ),
                  MyText(
                      title: tr("unselect_slot"),
                      color: MyColors.blackOpacity,
                      size: model.isTablet ? 8.sp : 10.sp),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
