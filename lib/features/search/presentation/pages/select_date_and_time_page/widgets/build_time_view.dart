part of 'date_time_widgets_imports.dart';

class BuildTimeView extends StatelessWidget {
  final DateTimePageData dateTimePageData;
  final DeviceModel model;
  final List<TimeEntity> listTime;

  const BuildTimeView(
      {Key? key,
      required this.dateTimePageData,
      required this.model,
      required this.listTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: model.isTablet ? 90.h : 80.h,
      margin: EdgeInsets.symmetric(vertical: 8.h),
      child: SingleChildScrollView(
        controller: dateTimePageData.timesScrollController,
        padding: const EdgeInsetsDirectional.only(start: 10),
        scrollDirection: Axis.horizontal,
        child: Row(
          children: List.generate(dateTimePageData.timesList.length, (index) {
            return Column(
              children: [
                GestureDetector(
                  onTap: () => dateTimePageData.selectItemTime(
                    dateTimePageData.timesList[index],
                  ),
                  child: Container(
                    key: dateTimePageData.timesList[index].globalKey,
                    padding: EdgeInsets.symmetric(
                      horizontal: model.isTablet ? 12.w : 14.w,
                      vertical: 8.h,
                    ),
                    margin: EdgeInsets.symmetric(
                      vertical: 4.h,
                      horizontal: 3.w,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(
                        color: elementSelectedAtHour(index)
                            ? MyColors.primary
                            : MyColors.grey.withOpacity(.3),
                      ),
                      color: elementSelectedAtHour(index)
                          ? MyColors.primary
                          : MyColors.white,
                    ),
                    alignment: Alignment.center,
                    child: MyText(
                      alien: TextAlign.center,
                      size: model.isTablet ? 8.sp : 10.sp,
                      title: _localRange(
                          context, dateTimePageData.timesList[index].title),
                      color: listTime.any(
                        (e) =>
                            e.title == dateTimePageData.timesList[index].title,
                      )
                          ? MyColors.white
                          : MyColors.black,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => dateTimePageData
                      .selectItemTime(dateTimePageData.halfTimesList[index]),
                  child: Container(
                    key: dateTimePageData.halfTimesList[index].globalKey,
                    padding: EdgeInsets.symmetric(
                      horizontal: model.isTablet ? 12.w : 14.w,
                      vertical: 8.h,
                    ),
                    margin:
                        EdgeInsets.symmetric(vertical: 4.h, horizontal: 3.w),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      border: Border.all(
                        color: elementSelectedAtHalfList(index)
                            ? MyColors.primary
                            : MyColors.grey.withOpacity(.3),
                      ),
                      color: elementSelectedAtHalfList(index)
                          ? MyColors.primary
                          : MyColors.white,
                    ),
                    alignment: Alignment.center,
                    child: MyText(
                      alien: TextAlign.center,
                      size: model.isTablet ? 8.sp : 10.sp,
                      title: _localRange(
                        context,
                        dateTimePageData.halfTimesList[index].title,
                      ),
                      color: listTime.any((e) =>
                              e.title ==
                              dateTimePageData.halfTimesList[index].title)
                          ? MyColors.white
                          : MyColors.black,
                    ),
                  ),
                ),
              ],
            );
          }),
        ),
      ),
    );
  }

  bool elementSelectedAtHalfList(int index) {
    return listTime.any(
      (e) => e.title == dateTimePageData.halfTimesList[index].title,
    );
  }

  bool elementSelectedAtHour(int index) {
    return listTime
        .any((e) => e.title == dateTimePageData.timesList[index].title);
  }

  String _localRange(BuildContext context, String value) {
    return getIt<Utilities>().localizedRangeNumber(context, value);
  }
}
