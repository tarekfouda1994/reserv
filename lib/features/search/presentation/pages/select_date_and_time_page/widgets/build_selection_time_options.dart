part of 'date_time_widgets_imports.dart';

class BuildSelectionTimeOptions extends StatelessWidget {
  final DateTimePageData dateTimePageData;
  final DeviceModel model;

  const BuildSelectionTimeOptions(
      {Key? key, required this.dateTimePageData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<TimeEntity>>,
            GenericState<List<TimeEntity>>>(
        bloc: dateTimePageData.selectedTimeBloc,
        builder: (cxt, state) {
          return Column(
            children: [
              BuildTimeHeader(
                dateTimePageData: dateTimePageData,
                model: model,
              ),
              BuildTimeView(
                dateTimePageData: dateTimePageData,
                model: model,
                listTime: state.data,
              ),
            ],
          );
        });
  }
}
