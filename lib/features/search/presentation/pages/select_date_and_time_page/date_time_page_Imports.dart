import 'dart:core';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/bloc/search_cubit/search_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/features/search/domain/entites/selected_date_time_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/selected_item_entites.dart';
import 'package:flutter_tdd/features/search/presentation/pages/select_date_and_time_page/widgets/date_time_widgets_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

part 'date_time_page.dart';
part 'date_time_page_data.dart';
