part of 'date_time_page_Imports.dart';

class DateTimePageData {
  final GenericBloc<List<SelectedItemEntity>> selectedDateBloc =
      GenericBloc([]);
  final GenericBloc<List<TimeEntity>> selectedTimeBloc = GenericBloc([]);
  final GenericBloc<List<SelectedItemEntity>> selectedTimesOfDayBloc =
      GenericBloc([]);
  GenericBloc<List<DateTime>> dateCubit = GenericBloc([]);
  final ScrollController timesOfDayScrollController = ScrollController();
  final ScrollController timesScrollController = ScrollController();
  final ScrollController dateScrollController = ScrollController();
  List<TimeEntity> timesList = [];
  List<TimeEntity> halfTimesList = [];

  initialDateAndTime(BuildContext context) {
    var dateTime = context.read<SearchCubit>().state.dateTime;
    dateCubit.onUpdateData(dateTime.dateList);
    selectedTimeBloc.onUpdateData(dateTime.timeList);
    initTimes();
  }

  initTimes() {
    for (int i = 1; i <= 24; i++) {
      var firstPart = i < 10 ? "0$i" : "$i";
      if (i == 24) {
        firstPart = "00";
      }
      timesList.add(TimeEntity(title: "$firstPart:00", globalKey: GlobalKey()));
    }
    for (int i = 1; i <= 24; i++) {
      var firstPart = i < 10 ? "0$i" : "$i";
      if (i == 24) {
        firstPart = "00";
      }
      halfTimesList
          .add(TimeEntity(title: "$firstPart:30", globalKey: GlobalKey()));
    }
  }

  scrollToSelectedTimesDay(GlobalKey key) {
    timesOfDayScrollController.position.ensureVisible(
        key.currentContext!.findRenderObject()!,
        duration: const Duration(milliseconds: 400),
        alignment: 0.1);
  }

  scrollToSelectedTime(GlobalKey key) {
    timesScrollController.position.ensureVisible(
      key.currentContext!.findRenderObject()!,
      duration: const Duration(milliseconds: 400),
      alignment: 0.1,
    );
  }

  scrollToSelectedDate() {
    dateScrollController.position.animateTo(
      0,
      duration: const Duration(milliseconds: 400),
      curve: Curves.bounceIn
    );
  }

  onSelectDate(DateTime dateTime) {
    if (dateCubit.state.data.any((element) => element == dateTime)) {
      dateCubit.state.data.remove(dateTime);
    } else {
      dateCubit.state.data.add(dateTime);
    }
    dateCubit.onUpdateData(dateCubit.state.data);
  }

  onConfirmDateTime(BuildContext context, String search) {
    dateCubit.state.data.sort((a, b) => a.compareTo(b));
    selectedTimeBloc.state.data.sort((a, b) => a.title.compareTo(b.title));
    _updateSearchCubit(context, search);
    AutoRouter.of(context).pop();
  }

  void _updateSearchCubit(BuildContext context, String search) {
    context.read<SearchCubit>().onUpdate(
          filter: search,
          dateTime: SelectedDateTimeEntity(
            timeList: selectedTimeBloc.state.data,
            dateList: dateCubit.state.data,
          ),
        );
  }

  selectDatePeriodItem(BuildContext context, int index) {
    if (selectedDateBloc.state.data[index].activeItem == false) {
      selectedDateBloc.state.data.map((e) => e.activeItem = false).toList();
      selectedDateBloc.state.data[index].activeItem = true;
      selectedDateBloc.onUpdateData(selectedDateBloc.state.data);
      if (selectedDateBloc.state.data[index].activeItem == true) {
        _datePeriodSwitch(index);
      }
      scrollToSelectedDate();
    } else {
      selectedDateBloc.state.data[index].activeItem = false;
      selectedDateBloc.onUpdateData(selectedDateBloc.state.data);
      dateCubit.state.data.clear();
    }
  }

  _datePeriodSwitch(int index) {
    switch (index) {
      case 0:
        dateCubit.state.data.clear();
        dateCubit.state.data.add(DateTime.now());
        dateCubit.onUpdateData(dateCubit.state.data);
        break;
      case 1:
        dateCubit.state.data.clear();
        dateCubit.state.data.add(DateTime.now().add(Duration(days: 1)));
        dateCubit.onUpdateData(dateCubit.state.data);
        break;
      case 2:
        dateCubit.state.data.clear();
        for (int i = 0; i < 7; i++) {
          dateCubit.state.data.add(DateTime.now().add(Duration(days: i)));
        }
        dateCubit.onUpdateData(dateCubit.state.data);
        break;
      case 3:
        dateCubit.state.data.clear();
        for (int i = 0; i < 30; i++) {
          dateCubit.state.data.add(DateTime.now().add(Duration(days: i)));
        }
        dateCubit.onUpdateData(dateCubit.state.data);
        break;
    }
  }

  selectItemTime(TimeEntity time) {
    if (!selectedTimeBloc.state.data.any((element) => element == time)) {
      selectedTimeBloc.state.data.add(time);
    } else {
      selectedTimeBloc.state.data.remove(time);
    }
    selectedTimeBloc.onUpdateData(selectedTimeBloc.state.data);
    if (selectedTimeBloc.state.data.isEmpty) {
      selectedTimesOfDayBloc.state.data
          .map((e) => e.activeItem = false)
          .toList();
      selectedTimesOfDayBloc.onUpdateData(selectedTimesOfDayBloc.state.data);
    }
  }

  selectTimesOfDayTime(int index, bool select) {
    if (select) {
      selectedTimesOfDayBloc.state.data[index].activeItem = true;
      selectedTimesOfDayBloc.onUpdateData(selectedTimesOfDayBloc.state.data);
      _addTimes(index);
      _scrollToEndTimes(index);
    } else {
      _updateSelectedTimes(index);
    }
  }

  _addTimes(int index) {
    selectedTimeBloc.state.data.addAll(timesList.getRange(
        timesList.indexWhere(
            (e) => e.title == selectedTimesOfDayBloc.state.data[index].from!),
        (timesList.indexWhere((e) =>
                e.title == selectedTimesOfDayBloc.state.data[index].to!)) +
            1));
    selectedTimeBloc.state.data.addAll(halfTimesList.getRange(
        halfTimesList.indexWhere((e) =>
            e.title.split(":").first ==
            selectedTimesOfDayBloc.state.data[index].from!.split(":").first),
        (halfTimesList.indexWhere((e) =>
            e.title.split(":").first ==
            selectedTimesOfDayBloc.state.data[index].to!.split(":").first))));
    selectedTimeBloc.onUpdateData(selectedTimeBloc.state.data);
  }

  _scrollToEndTimes(int index) {
    scrollToSelectedTimesDay(
        selectedTimesOfDayBloc.state.data[index].globalKey);
    if (selectedTimeBloc.state.data.isNotEmpty)
      scrollToSelectedTime(selectedTimeBloc
          .state.data[selectedTimeBloc.state.data.length - 3].globalKey);
  }

  _updateSelectedTimes(int index) {
    selectedTimeBloc.onUpdateData([]);
    selectedTimesOfDayBloc.state.data[index].activeItem = false;
    selectedTimesOfDayBloc.state.data
        .where((element) => element.activeItem ?? false)
        .forEach((element) {
      selectTimesOfDayTime(
          selectedTimesOfDayBloc.state.data.indexOf(element), true);
    });
    selectedTimesOfDayBloc.onUpdateData(selectedTimesOfDayBloc.state.data);
  }

  removeAllDate() {
    dateCubit.state.data.clear();
    selectedDateBloc.state.data.map((e) => e.activeItem = false).toList();
    selectedDateBloc.onUpdateData(selectedDateBloc.state.data);
  }

  removeAllTime() {
    selectedTimeBloc.state.data.clear();
    selectedTimesOfDayBloc.state.data.map((e) => e.activeItem = false).toList();
    selectedTimeBloc.onUpdateData(selectedTimeBloc.state.data);
    selectedTimesOfDayBloc.onUpdateData(selectedTimesOfDayBloc.state.data);
  }

  List<SelectedItemEntity> dateTabs = [
    SelectedItemEntity(title: tr("TODAY"), globalKey: GlobalKey()),
    SelectedItemEntity(title: tr("TOMORROW"), globalKey: GlobalKey()),
    SelectedItemEntity(title: tr("THIS_WEEK"), globalKey: GlobalKey()),
    SelectedItemEntity(title: tr("THIS_MONTH"), globalKey: GlobalKey()),
  ];

  List<SelectedItemEntity> timesOfDayTabs = [
    SelectedItemEntity(
        title: tr("MORNING"),
        from: '05:00',
        to: '09:00',
        globalKey: GlobalKey()),
    SelectedItemEntity(
        title: tr("Mid_MORNING"),
        to: '12:00',
        from: '09:00',
        globalKey: GlobalKey()),
    SelectedItemEntity(
        title: tr("AFTERNOON"),
        from: '12:00',
        to: '17:00',
        globalKey: GlobalKey()),
    SelectedItemEntity(
        title: tr("EVENING"),
        to: '21:00',
        from: '17:00',
        globalKey: GlobalKey()),
    SelectedItemEntity(
        title: tr("NIGHT"), from: '21:00', to: '00:00', globalKey: GlobalKey()),
  ];
}
