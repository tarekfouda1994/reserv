part of 'date_time_page_Imports.dart';

class DateTimePage extends StatefulWidget {
  const DateTimePage({Key? key}) : super(key: key);

  @override
  State<DateTimePage> createState() => _DateTimePageState();
}

class _DateTimePageState extends State<DateTimePage> {
  final DateTimePageData dateTimePageData = DateTimePageData();

  @override
  void initState() {
    dateTimePageData.initialDateAndTime(context);
    dateTimePageData.selectedDateBloc.onUpdateData(dateTimePageData.dateTabs);
    dateTimePageData.selectedTimesOfDayBloc.onUpdateData(dateTimePageData.timesOfDayTabs);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var search = context.watch<SearchCubit>().state.filter;
    var device = context.watch<DeviceCubit>().state.model;

    return Scaffold(
      backgroundColor: MyColors.white,
      appBar: DefaultAppBar(
        title: tr("select_reservation_dateTime"),
        onBack: () => AutoRouter.of(context).pop(),
      ),
      body: ListView(
        children: [
          BuildSelectionDateOption(
            dateTimePageData: dateTimePageData,
            model: device,
          ),
          SizedBox(
            height: device.isTablet
                ? MediaQuery.of(context).size.height * .09.h
                : MediaQuery.of(context).size.height * .08.h,
          ),
          BuildSelectionTimeOptions(
            dateTimePageData: dateTimePageData,
            model: device,
          ),
          BuildTimesOfDayView(
            dateTimePageData: dateTimePageData,
            model: device,
          ),
        ],
      ),
      bottomNavigationBar: DefaultButton(
        borderRadius: BorderRadius.circular(40.r),
        title: tr("confirm_dateTime"),
        height: device.isTablet?60.sm: 50.sm,
        fontSize: device.isTablet ? 7.sp : 11.sp,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 20.sm),
        color: MyColors.primary,
        borderColor: MyColors.white,
        textColor: MyColors.white,
        onTap: () => dateTimePageData.onConfirmDateTime(context, search),
      ),
    );
  }
}
