part of 'tracking_map_imports.dart';

class TrackingMapData {
  final GlobalKey<ScaffoldState> scaffold = GlobalKey<ScaffoldState>();
  final Completer<GoogleMapController> mapController = Completer();
  final GenericBloc<List<Marker>> markersCubit = GenericBloc([]);
  late CameraPosition initialLoc;

  List<PlaceEntity> servicesModels = [];

  getCurrentLocation(BuildContext context, double lat, double lng) async {
    var loc = await getIt<Utilities>().getCurrentLocation(context);
    if (loc != null) {
      var salon = PlaceEntity(
        lat: lat.toString(),
        lng: lng.toString(),
        distance: "5.7",
        name: Res.salon,
      );
      var client = PlaceEntity(
        lat: loc.latitude.toString(),
        lng: loc.longitude.toString(),
        distance: "5.7",
        name: Res.client,
      );
      servicesModels.add(salon);
      servicesModels.add(client);
      // final CameraPosition _client = CameraPosition(
      //   target: LatLng(loc.latitude ?? 0, loc.longitude ?? 0),
      //   zoom: 15.151926040649414,
      // );
      // final GoogleMapController controller = await mapController.future;
      // controller.animateCamera(CameraUpdate.newCameraPosition(_client));
      setMarkerWidgets(context);
    }
  }

  void onMapCreated(GoogleMapController controller) {
    controller.setMapStyle(json.encode(MapStyle.server));
    if (!mapController.isCompleted) {
      mapController.complete(controller);
    }
  }

  getIcons(String image) async {
    var icon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(120.h, 120.h)),
      image,
    );
    return icon;
  }

  setMarkerWidgets(context) async {
    for (var element in servicesModels) {
      markersCubit.state.data.add(
        Marker(
          markerId: MarkerId(element.name),
          position: LatLng(double.parse(element.lat), double.parse(element.lng)),
          icon: await getIcons(element.name),
        ),
      );
    }
    markersCubit.onUpdateData(markersCubit.state.data);
    // MarkerGenerator(markerWidgets(), (bitmaps) {
    //   var markers = mapBitmapsToMarkers(bitmaps, context);
    //   markersCubit.onUpdateData(markers);
    // }).generate(context);
    // markersCubit.onUpdateData(markersCubit.state.data);
  }

  List<Widget> markerWidgets() {
    return servicesModels.map((c) => BuildTrackingMarker(image: c.name)).toList();
  }

  List<Marker> mapBitmapsToMarkers(List<Uint8List> bitmaps, BuildContext context) {
    List<Marker> markersList = [];
    bitmaps.asMap().forEach((i, bmp) {
      final model = servicesModels[i];
      markersList.add(
        Marker(
          onTap: () => onMarkerClick(context, model),
          markerId: MarkerId(model.distance),
          position: LatLng(double.parse(model.lat), double.parse(model.lng)),
          icon: BitmapDescriptor.fromBytes(bmp),
        ),
      );
    });
    return markersList;
  }

  onMarkerClick(BuildContext context, PlaceEntity model) {}
}
