part of 'tracking_map_imports.dart';

class TrackingMap extends StatefulWidget {
  final String businessName;
  final double lat;
  final double lng;
  final String address;

  const TrackingMap({
    Key? key,
    required this.businessName,
    required this.address,
    required this.lat,
    required this.lng,
  }) : super(key: key);

  @override
  _TrackingMapState createState() => _TrackingMapState();
}

class _TrackingMapState extends State<TrackingMap> {
  final TrackingMapData trackingMapData = TrackingMapData();

  @override
  void initState() {
    trackingMapData.initialLoc = CameraPosition(
      target: LatLng(widget.lat, widget.lng),
      zoom: 15.4746,
    );
    trackingMapData.getCurrentLocation(context, widget.lat, widget.lng);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BuildMapAppBar(
        address: widget.address,
        businessName: widget.businessName,
      ),
      body: BuildGoogleMapView(mapData: trackingMapData),
    );
  }
}
