part of 'tracking_map_widgets_imports.dart';

class BuildTrackingMarker extends StatelessWidget {
  final String image;

  const BuildTrackingMarker({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      image,
      width: 100.w,
      height: 100.h,
    );
  }
}
