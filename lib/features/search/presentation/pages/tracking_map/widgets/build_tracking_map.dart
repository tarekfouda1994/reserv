part of 'tracking_map_widgets_imports.dart';

class BuildGoogleMapView extends StatelessWidget {
  final TrackingMapData mapData;

  const BuildGoogleMapView({Key? key, required this.mapData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<Marker>>, GenericState<List<Marker>>>(
      bloc: mapData.markersCubit,
      builder: (context, state) {
        return GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: mapData.initialLoc,
            onMapCreated: (GoogleMapController controller) {
              mapData.onMapCreated(controller);
            },
            compassEnabled: true,
            myLocationEnabled: true,
            mapToolbarEnabled: true,
            zoomControlsEnabled: true,
            zoomGesturesEnabled: true,
            buildingsEnabled: false,
            trafficEnabled: false,
            markers: state.data.toSet(),
            onCameraIdle: () {},
            onTap: (location) {},
            onCameraMove: (loc) {
              print("=========> ${loc.target.latitude} ==> ${loc.target.longitude}");
            });
      },
    );
  }
}
