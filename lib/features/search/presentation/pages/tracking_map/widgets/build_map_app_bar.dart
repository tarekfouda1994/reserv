part of 'tracking_map_widgets_imports.dart';

class BuildMapAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String businessName;
  final String address;

  const BuildMapAppBar({Key? key, required this.businessName, required this.address})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return AppBar(
      title: Row(
        children: [
          Container(
            padding: EdgeInsets.all(6.r),
            decoration:
                BoxDecoration(color: MyColors.white.withOpacity(.5), shape: BoxShape.circle),
            child: InkWell(
                child: Icon(
                  device.locale == Locale('en', 'US') ? Icons.west : Icons.east,
                  color: MyColors.primary,
                ),
                onTap: () => Navigator.of(context).pop()),
          ),
          SizedBox(width: 10.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(
                  title: businessName,
                  color: MyColors.white,
                  size: device.isTablet ? 8.sp : 12.sp,
                ),
                SizedBox(height: device.isTablet ? 2.h : 8.h),
                MyText(
                  title: address,
                  color: MyColors.offWhite,
                  size: device.isTablet ? 7.sp : 12.sp,
                ),
              ],
            ),
          )
        ],
      ),
      backgroundColor: MyColors.primary,
      systemOverlayStyle: SystemUiOverlayStyle(statusBarBrightness: Brightness.dark),
      automaticallyImplyLeading: false,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(80);
}
