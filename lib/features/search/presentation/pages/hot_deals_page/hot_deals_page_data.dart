part of 'hot_deals_page_imports.dart';

class HotDealsPageData {
  final PagingController<int, HotDealsItemModel> pagingController =
      PagingController(firstPageKey: 1);
  int pageSize = 10;

  ficheData(BuildContext context, int currentPage, {bool refresh = true}) async {
    var location = context.read<HomeLocationCubit>().state.model;
    var filter = context.read<SearchCubit>().state.filter;
    HotDealsPrams params = _hotDealsPrams(refresh, filter, currentPage, location);
    var data = await GetHotDeals()(params);
    final isLastPage = data.itemsList.length < pageSize;
    if (currentPage == 1) {
      pagingController.itemList = [];
    }
    if (isLastPage) {
      pagingController.appendLastPage(data.itemsList);
    } else {
      final nextPageKey = currentPage + 1;
      pagingController.appendPage(data.itemsList, nextPageKey);
    }
  }

  HotDealsPrams _hotDealsPrams(bool refresh, String filter, int currentPage, LocationEntity? location) {
    return HotDealsPrams(
      refresh: refresh,
      filter: filter,
      pageNumber: currentPage,
      pageSize: pageSize,
      longitude: location?.lng ?? 0,
      latitude: location?.lat ?? 0);
  }
}
