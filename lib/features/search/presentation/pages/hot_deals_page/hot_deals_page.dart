part of 'hot_deals_page_imports.dart';

class HotDealsPage extends StatefulWidget {
  const HotDealsPage({Key? key}) : super(key: key);

  @override
  State<HotDealsPage> createState() => _HotDealsPageState();
}

class _HotDealsPageState extends State<HotDealsPage> {
  final HotDealsPageData hotDealsPageData = HotDealsPageData();

  @override
  void initState() {
    super.initState();
    hotDealsPageData.pagingController.addPageRequestListener((pageKey) {
      hotDealsPageData.ficheData(context, pageKey);
    });
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Scaffold(
      appBar: DefaultAppBar(size: 65, title: tr("hot_deals")),
      body: RefreshIndicator(
              color: MyColors.primary,
        onRefresh: () => hotDealsPageData.ficheData(context, 1),
        child: PagedListView<int, HotDealsItemModel>(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          pagingController: hotDealsPageData.pagingController,
          builderDelegate: PagedChildBuilderDelegate<HotDealsItemModel>(
              firstPageProgressIndicatorBuilder: (context) => Column(
                    children: List.generate(
                      5,
                      (index) => BuildShimmerView(
                          height: device.isTablet ? 180.h : 150.h,
                          width: MediaQuery.of(context).size.width * .9),
                    ),
                  ),
              itemBuilder: (context, item, index) => SizedBox(
                    child: BuildSwiperItem(model: item,fromHome: false),
                    height: device.isTablet ? 180.h : 140.h,
                  ),
              newPageProgressIndicatorBuilder: (con) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CupertinoActivityIndicator(),
                  ),
              firstPageErrorIndicatorBuilder: (context) => BuildPageError(
                    onTap: () => hotDealsPageData.pagingController.refresh(),
                  )),
        ),
      ),
    );
  }
}
