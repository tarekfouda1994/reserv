import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/popular_search_model/popular_search_model.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GteRecentSearch implements UseCase<List<PopularSearchModel>, bool> {
  @override
  Future<List<PopularSearchModel>> call(bool params) async {
    var data = await getIt<BaseRepository>().getRecentSearch(params);
    return data.fold((l) => [], (r) => r);
  }
}
