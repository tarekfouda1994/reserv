import 'package:collection/collection.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_image_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetBusinessImage implements UseCase<String, BusinessImagesParams> {
  @override
  Future<String> call(BusinessImagesParams params) async {
    var data = await getIt<BaseRepository>().getAppointmentImage(params);
    return data.fold((l) => "", (r) => r.firstOrNull.toString());
  }
}
