import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/status_model/status_model.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetOrderStatus implements UseCase<List<StatusModel>, bool> {
  @override
  Future<List<StatusModel>> call(bool params) async {
    var data = await getIt<BaseRepository>().getStatus(params);
    return data.fold((l) => [], (r) => r);
  }
}
