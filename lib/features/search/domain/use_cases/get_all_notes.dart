import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/note_model/notes_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/get_notes_params.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetAllNotes implements UseCase<List<NotesModel>?, GetNoteParams> {
  @override
  Future<List<NotesModel>?> call(GetNoteParams params) async {
    var data = await getIt<BaseRepository>().getNotes(params);
    return data.fold((l) => null, (r) => r);
  }
}
