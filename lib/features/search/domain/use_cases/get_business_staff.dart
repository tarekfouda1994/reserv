import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/business_staff_model/business_staff_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/business_staff_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetBusinessStaff implements UseCase<List<BusinessStaffModel>, BusinessStaffParams> {
  @override
  Future<List<BusinessStaffModel>> call(BusinessStaffParams params) async {
    var data = await getIt<BaseRepository>().getBusinessStaff(params);
    return data.fold((l) => [], (r) => r);
  }
}
