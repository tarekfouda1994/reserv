import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/cancel_order_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class CancelOrder implements UseCase<bool, CancelOrderParams> {
  @override
  Future<bool> call(CancelOrderParams params) async {
    var data = await getIt<BaseRepository>().cancelOrder(params);
    return data.fold((l) => false, (r) => r);
  }
}
