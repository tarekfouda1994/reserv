import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/home_banners/home_banners_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/banners_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetMainBanners implements UseCase<HomeBannersModel?, BannersParams> {
  @override
  Future<HomeBannersModel> call(BannersParams params) async {
    var data = await getIt<BaseRepository>().getMainBanners(params);
    return data.fold(
        (l) => HomeBannersModel(totalRecords: 0, pageSize: 0, itemsList: []), (r) => r);
  }
}
