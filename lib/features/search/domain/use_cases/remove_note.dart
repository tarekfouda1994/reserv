import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

import '../../../../core/usecases/use_case.dart';

class RemoveNote implements UseCase<bool, String> {
  @override
  Future<bool> call(String params) async {
    var data = await getIt<BaseRepository>().removeNote(params);
    return data.fold((l) => false, (r) => r);
  }
}
