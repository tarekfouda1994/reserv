import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/add_msg_chat_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class AddMsgChat implements UseCase<bool, AddMsgChatParams> {
  @override
  Future<bool> call(AddMsgChatParams params) async {
    var result = await getIt<BaseRepository>().addMsgChat(params);
    return result.fold((l) => false, (r) => r);
  }
}
