import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/reviews_model/reviews_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/service_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetServiceReviews implements UseCase<ReviewsModel?, ServiceDetailsPrams> {
  @override
  Future<ReviewsModel?> call(ServiceDetailsPrams params) async {
    var data = await getIt<BaseRepository>().getServiceReviews(params);
    return data.fold((l) => null, (r) => r);
  }
}
