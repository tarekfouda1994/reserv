import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/chat_model/chat_model.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetAllChatMsg implements UseCase<List<ChatModel>, String> {
  @override
  Future<List<ChatModel>> call(String params) async {
    var data = await getIt<BaseRepository>().getAllMsgChat(params);
    return data.fold((l) => [], (r) => r);
  }
}
