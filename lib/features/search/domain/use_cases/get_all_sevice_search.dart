import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/service_search_model/service_search_model.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

import '../entites/sevice_search_entity.dart';

class GetAllServiceSearch implements UseCase<ServiceSearchModel?, ServiceSearchParams> {
  @override
  Future<ServiceSearchModel?> call(ServiceSearchParams params) async {
    var data = await getIt<BaseRepository>().getAllServiceSearch(params);
    return data.fold((l) => null, (r) => r);
  }
}
