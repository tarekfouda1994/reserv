import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_details/appointment_details.dart';
import 'package:flutter_tdd/features/search/domain/entites/appointment_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetAppointmentDetails implements UseCase<AppointmentDetails?, AppointmentDetailsParams> {
  @override
  Future<AppointmentDetails?> call(AppointmentDetailsParams params) async {
    var data = await getIt<BaseRepository>().getAppointmentDetails(params);
    return data.fold((l) => null, (r) => r);
  }
}
