import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/hot_deals_model/hot_deals_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/hot_deals_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetHotDeals implements UseCase<HotDealsModel?, HotDealsPrams> {
  @override
  Future<HotDealsModel> call(HotDealsPrams params) async {
    var data = await getIt<BaseRepository>().getHotDeals(params);
    return data.fold((l) => HotDealsModel(totalRecords: 0, pageSize: 0, itemsList: []), (r) => r);
  }
}
