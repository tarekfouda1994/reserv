import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/appointment_orders/appointment_orders.dart';
import 'package:flutter_tdd/features/search/domain/entites/appointment_order_entity.dart';

import '../repositories/base_repository.dart';

class GetDashboardData implements UseCase<List<AppointmentOrdersModel>, AppointmentOrderParams> {
  @override
  Future<List<AppointmentOrdersModel>> call(AppointmentOrderParams params) async {
    var data = await getIt<BaseRepository>().getDashboardData(params);
    return data.fold((l) => [], (r) => r);
  }
}
