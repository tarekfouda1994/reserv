import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/services_rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class AddServicesRate implements UseCase<bool, ServicesRateParams> {
  @override
  Future<bool> call(ServicesRateParams params) async {
    var result = await getIt<BaseRepository>().ratingServices(params);
    return result.fold((l) => false, (r) => r);
  }
}
