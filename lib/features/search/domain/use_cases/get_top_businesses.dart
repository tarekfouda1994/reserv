import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/top_businesses_model/top_businesses_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetTopBusinesses implements UseCase<TopBusinessesModel?, TopBusinessParams> {
  @override
  Future<TopBusinessesModel?> call(TopBusinessParams params) async {
    var data = await getIt<BaseRepository>().getTopBusinesses(params);
    return data.fold((l) => null, (r) => r);
  }
}
