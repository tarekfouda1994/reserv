import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/edit_note_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class EditNote implements UseCase<bool,EditNoteParams>{
  @override
  Future<bool> call(EditNoteParams params) async{
    var result = await getIt<BaseRepository>().editNote(params);
    return result.fold((l) => false, (r) => true);
  }
}