import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class RatingService implements UseCase<bool, RateParams> {
  @override
  Future<bool> call(RateParams params) async {
    var data = await getIt<BaseRepository>().rating(params);
    return data.fold((l) => false, (r) => r);
  }
}
