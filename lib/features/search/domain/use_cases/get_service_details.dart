import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/service_details_model/service_details_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/service_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

import '../../../../core/helpers/di.dart';

class GetServiceDetails implements UseCase<ServiceDetailsModel?, ServiceDetailsPrams> {
  @override
  Future<ServiceDetailsModel?> call(ServiceDetailsPrams params) async {
    var data = await getIt<BaseRepository>().getServiceDetails(params);
    return data.fold((l) => null, (r) => r);
  }
}
