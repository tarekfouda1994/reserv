import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/trinding_model/trinding_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/trinding_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetTrending implements UseCase<TrindingModel, TrendingPrams> {
  @override
  Future<TrindingModel> call(TrendingPrams params) async {
    var data = await getIt<BaseRepository>().getTrending(params);
    return data.fold((l) => TrindingModel(totalRecords: 0, itemsList: [], pageSize: 0), (r) => r);
  }
}
