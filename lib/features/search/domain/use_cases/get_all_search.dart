import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/domain/entites/search_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

import '../../data/models/business_search_model/business_search_model.dart';

class GetAllSearch implements UseCase<BusinessSearchModel?, SearchPrams> {
  @override
  Future<BusinessSearchModel?> call(SearchPrams params) async {
    var data = await getIt<BaseRepository>().getAllSearch(params);
    return data.fold((l) => null, (r) => r);
  }
}
