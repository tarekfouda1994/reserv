import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/count_chat_model/count_chat_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/msg_count_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetMsgCount implements UseCase<List<CountChatModel>, MsgCountParams> {
  @override
  Future<List<CountChatModel>> call(MsgCountParams params) async {
    var data = await getIt<BaseRepository>().getMsgCount(params);
    return data.fold((l) => [], (r) => r);
  }
}
