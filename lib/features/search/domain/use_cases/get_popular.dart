import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/popular_services_model/popular_services_model.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetPopular implements UseCase<List<PopularServicesModel>, bool> {
  @override
  Future<List<PopularServicesModel>> call(bool params) async {
    var data = await getIt<BaseRepository>().getPopular(params);
    return data.fold((l) => [], (r) => r);
  }
}
