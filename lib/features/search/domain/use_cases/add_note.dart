import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/features/search/domain/entites/note_entity.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

import '../../../../core/usecases/use_case.dart';

class AddNote implements UseCase<bool, AddNoteParams> {
  @override
  Future<bool> call(AddNoteParams params) async {
    var data = await getIt<BaseRepository>().addNote(params);
    return data.fold((l) => false, (r) => r);
  }
}
