import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/search/data/models/popular_cat_model/popular_cat_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:flutter_tdd/features/search/domain/repositories/base_repository.dart';

class GetPopularCat implements UseCase<List<PopularCatModel>,TopBusinessParams>{
  @override
  Future<List<PopularCatModel>> call(TopBusinessParams params) async{
   var data = await getIt<BaseRepository>().getPopularCat(params);
   return data.fold((l) => [], (r) => r);
  }
}