class BusinessStaffParams {
  final String businessID;
  final int pageNumber;
  final int pageSize;
  final bool? refresh;

  BusinessStaffParams(
      {this.refresh = true,
      required this.pageSize,
      required this.businessID,
      required this.pageNumber});

  Map<String, dynamic> toJson() => {
        "businessID": businessID,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
      };
}
