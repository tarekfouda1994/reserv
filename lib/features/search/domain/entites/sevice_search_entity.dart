class ServiceSearchParams {
  final int pageNumber;
  final int pageSize;
  final String filter;

  // final String? toTime;
  // final String? fromTime;
  // final DateTime? toScheduleDate;
  // final DateTime? fromScheduleDate;
  final bool? refresh;

  ServiceSearchParams({
    this.refresh = true,
    required this.pageNumber,
    required this.pageSize,
    required this.filter,
    // this.fromScheduleDate,
    // this.fromTime,
    // this.toScheduleDate,
    // this.toTime
  });

  Map<String, dynamic> toJson() => {
        "pageSize": pageSize,
        "pageNumber": pageNumber,
        "filter": filter,
        // if (toTime != null) "toTime": toTime,
        // if (toScheduleDate != null) "toScheduleDate": toScheduleDate,
        // if (fromScheduleDate != null) "fromScheduleDate": fromScheduleDate,
        // if (fromTime != null) "fromTime": fromTime,
      };
}
