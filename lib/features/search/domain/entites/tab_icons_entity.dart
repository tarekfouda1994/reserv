

class TabIconsEntity {
  final String icon;
  final String activeIcon;
  final String title;

  TabIconsEntity({required this.icon, required this.activeIcon,required this.title});
}
