import 'package:flutter_tdd/core/helpers/enums.dart';

class TopBusinessParams {
  final int pageNumber;
  final int pageSize;
  final int? totalRecords;
  final double? latitude;
  final double? longitude;
  final String? filter;
  final String? sortColumn;
  final int? userId;
  final bool? sortDirection;
  final bool refresh;
  final bool loadImages;
  final HomeApisType? homeApisType;

  TopBusinessParams({
    this.homeApisType,
    this.totalRecords,
    this.sortColumn,
    this.userId,
    this.sortDirection,
    required this.loadImages,
    required this.pageNumber,
    required this.pageSize,
    this.filter,
    this.latitude,
    this.longitude,
    required this.refresh,
  });

  Map<String, dynamic> toJson() => {
        "pageSize": pageSize,
        "pageNumber": pageNumber,
        if (sortDirection != null) "sortDirection": sortDirection,
        "loadImages": loadImages,
        if (sortColumn != null) "sortColumn": sortColumn,
        if (userId != null) "userId": userId,
        if (totalRecords != null) "totalRecords": totalRecords,
        if (filter != null) "filter": filter,
        if (latitude != null) "latitude": latitude,
        if (longitude != null) "longitude": longitude,
      };
}
