import 'package:flutter_tdd/core/helpers/enums.dart';

class TrendingPrams {
  String? genderID;
  String filter;
  String? sortColumn;
  num longitude;
  num latitude;
  num? minDuration;
  num? maxDuration;
  num? minServiceAmount;
  num? maxServiceAmount;
  int pageNumber;
  int pageSize;
  int? rating;
  bool? sortDirection;
  HomeApisType? homeApisType;

  String? fromScheduleDate;
  String? toScheduleDate;
  String? fromTime;
  String? toTime;
  bool refresh;

  TrendingPrams(
      {required this.pageNumber,
      required this.pageSize,
      required this.latitude,
      required this.longitude,
      required this.filter,
      required this.refresh,
      this.rating,
      this.homeApisType,
      this.minDuration,
      this.minServiceAmount,
      this.maxServiceAmount,
      this.maxDuration,
      this.sortColumn,
      this.sortDirection,
      this.fromTime,
      this.genderID,
      this.toTime,
      this.fromScheduleDate,
      this.toScheduleDate});

  Map<String, dynamic> toJson() => {
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "latitude": latitude,
        if (filter.isNotEmpty) "filter": filter,
        if (genderID != null) "serviceGender": genderID,
        // if (genderID != null) "businessGender": genderID,
        "longitude": longitude,
        if (rating != null) "businessRating": rating,
        if (rating != null) "businessRating": rating,
        if (minDuration != null) "minDuration": minDuration,
        if (minServiceAmount != null) "minServicePrice": minServiceAmount,
        if (maxServiceAmount != null) "maxServicePrice": maxServiceAmount,
        if (maxDuration != null) "maxDuration": maxDuration,
        if (sortColumn != null) "sortColumn": sortColumn,
        if (sortDirection != null) "sortDirection": sortDirection,
        if (toTime != null) "toTime": toTime,
        if (toScheduleDate != "") "toScheduleDate": toScheduleDate,
        if (fromScheduleDate != "") "fromScheduleDate": fromScheduleDate,
        if (fromTime != null) "fromTime": fromTime,
      };
}
