class ServicesRateParams {
  String id;
  String businessServiceId;
  String appointmentId;
  int ratingID;
  String ratingComments;
  String email;
  bool isHidden;

  ServicesRateParams(
      {required this.id,
        required this.email,
        required this.appointmentId,
        required this.ratingComments,
        required this.ratingID,
        required this.businessServiceId,
        required this.isHidden,
      });

  Map<String, dynamic> toJson() => {
    "id": id,
    "businessServiceId": businessServiceId,
    "appointmentId": appointmentId,
    "ratingId": ratingID,
    "ratingComments": ratingComments,
    "email": email,
    "isHidden": isHidden,
  };
}