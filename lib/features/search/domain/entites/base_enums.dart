enum SentOfferStatus {
  Accepted,
  Declined,
  Pending,
}
