

class FilterEntity {
  final int? genderID;
  final int? rating;
  final num? minDuration;
  final num? maxDuration;
  final num? minServiceAmount;
  final num? maxServiceAmount;
  final String? sortColumn;
  final List<int>? filterCount;
  final bool? sortDirection;

  FilterEntity(
      {this.filterCount,
      this.rating,
      this.genderID,
      this.maxDuration,
      this.maxServiceAmount,
      this.minDuration,
      this.minServiceAmount,
      this.sortDirection,
      this.sortColumn});

}
