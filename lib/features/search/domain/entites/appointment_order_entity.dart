class AppointmentOrderParams {
  final int? status;
  final bool? refresh;
  final String? filter;
  final String? lastName;
  final String? orderNumber;

  AppointmentOrderParams({
    this.refresh = true,
    this.status,
    this.filter,
    this.lastName,
    this.orderNumber,
  });

  Map<String, dynamic> toJson() => {
        "pageNumber": 1,
        "pageSize": 20,
        "sortColumn": "bookingDate",
        "sortDirection": false,
        if (status != null) "orderStatusID": status,
        if (filter != null && filter != "") "filter": filter,
        if (lastName != null) "lastName": lastName,
        if (orderNumber != null) "orderNumber": orderNumber,
      };
}
