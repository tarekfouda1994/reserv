class SearchPrams {
  final int pageNumber;
  final int pageSize;
  final String filter;
  final String deviceId;
  final String? from;
  final String? to;
  final double? longitude;
  final double? latitude;
  final bool? refresh;

  SearchPrams(
      {this.refresh = true,
      required this.pageNumber,
      required this.pageSize,
      required this.deviceId,
      required this.filter,
      this.latitude,
      this.longitude,
      this.from,
      this.to});

  Map<String, dynamic> toJson() => {
        "pageSize": pageSize,
        "pageNumber": pageNumber,
        "deviceId": deviceId,
        "filter": filter,
        "longitude": longitude,
        "latitude": latitude,
        "isLoadAllServices": true,
        "loadImages": true,
        if (from != "") "from": from,
        if (to != "") "to": to,
      };
}
