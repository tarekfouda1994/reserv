class MsgCountParams {
  final String orderNumber;
  final String businessID;

  MsgCountParams({
    required this.orderNumber,
    required this.businessID,
  });

  Map<String, dynamic> toJson() => {
        "filter": orderNumber,
        "businessID": businessID,
      };
}
