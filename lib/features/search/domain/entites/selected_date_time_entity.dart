import 'selected_item_entites.dart';

class SelectedDateTimeEntity {
  List<DateTime> dateList;
  List<TimeEntity> timeList;

  SelectedDateTimeEntity({required this.timeList, required this.dateList});
}
