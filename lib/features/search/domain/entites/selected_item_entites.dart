import 'package:flutter/material.dart';

class SelectedItemEntity {
  final String title;
  final String? from;
  final String? to;
  bool? activeItem;
  final GlobalKey globalKey;

  SelectedItemEntity(
      {required this.title, required this.globalKey, this.activeItem = false, this.from, this.to});
}

class TimeEntity {
  final String title;
  bool? activeItem;
  final GlobalKey globalKey;

  TimeEntity({required this.title, required this.globalKey, this.activeItem = false});
}
