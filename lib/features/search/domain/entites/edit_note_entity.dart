class EditNoteParams {
  String id;
  String note;

  EditNoteParams({required this.id, required this.note});

  Map<String, dynamic> toJson() => {
    "id": id,
    "note": note,
  };
}