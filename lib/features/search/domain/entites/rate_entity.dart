

class RateParams {
  String businessID;
  String bookingOrderID;
  String orderNumber;
  int ratingID;
  String ratingComments;
  String email;

  RateParams(
      {required this.businessID,
      required this.email,
      required this.orderNumber,
      required this.ratingComments,
      required this.ratingID,
      required this.bookingOrderID});

  Map<String, dynamic> toJson() => {
        "businessID": businessID,
        "bookingOrderID": bookingOrderID,
        "orderNumber": orderNumber,
        "ratingID": ratingID,
        "ratingComments": ratingComments,
        "email": email,
      };
}
