class BannersParams {
  int pageNumber;
  int pageSize;
  double? longitude;
  double? latitude;
  String? filter;
  String? fromDate;
  String? toDate;
  bool loadImages;
  bool refresh = false;

  BannersParams({
    required this.pageNumber,
    required this.pageSize,
    required this.refresh,
    required this.loadImages,
    this.longitude,
    this.latitude,
    this.filter,
    this.fromDate,
    this.toDate,
  });

  Map<String, dynamic> toJson() => {
        "pageSize": pageSize,
        "pageNumber": pageNumber,
        "loadImages": loadImages,
        "longitude": longitude,
        "latitude": latitude,
        if (filter != null) "filter": filter,
        if (fromDate != null) "fromDate": fromDate,
        if (fromDate != null) "toDate": toDate,
      };
}
