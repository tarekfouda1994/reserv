class SelectedTimeEntity {
  final String titleHours;
  bool? activeItemHours;
  final String titleHalf;
  bool? activeItemHalf;

  SelectedTimeEntity(
      {required this.titleHours,
      this.activeItemHours = false,
      required this.titleHalf,
      this.activeItemHalf = false});
}
