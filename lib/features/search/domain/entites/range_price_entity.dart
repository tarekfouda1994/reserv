class RangePriceEntity {
  final double from;
  final double to;

  RangePriceEntity({required this.from, required this.to});

  factory RangePriceEntity.fromJson(json) => RangePriceEntity(from: json["from"], to: json["to"]);

  Map<String, dynamic> toJson() => {
        "from": from,
        "to": to,
      };
}
