class AddNoteParams {
  String orderID;
  List<String> noteTitle;

  AddNoteParams({required this.orderID, required this.noteTitle});

  Map<String, dynamic> toJson() => {
        "data": noteTitle,
      };
}
