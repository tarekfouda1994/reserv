class CancelOrderParams {
  final String id;
  final int appointmentType;
  final bool? refresh;

  CancelOrderParams({this.refresh = true, required this.id, required this.appointmentType});

  Map<String, dynamic> toJson() => {
        "appointmentType": appointmentType,
        "id": id,
      };
}
