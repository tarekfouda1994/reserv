class PlaceEntity {
  final String lat;
  final String lng;
  final String distance;
  final String name;

  PlaceEntity({
    required this.lat,
    required this.lng,
    required this.distance,
    required this.name,
  });
}
