class NearMeLocEntity {
  final double lat;
  final double lng;
  final String title;

  NearMeLocEntity({required this.title,required this.lat, required this.lng});

  factory NearMeLocEntity.fromJson(json) => NearMeLocEntity(lat: json["lat"], lng: json["lng"],title: json["title"]);

  Map<String, dynamic> toJson() => {
    "lat": lat,
    "lng": lng,
    "title": title,
  };
}