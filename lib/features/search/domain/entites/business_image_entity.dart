class BusinessImagesParams {
  final String businessId;

  final bool? refresh;
  final bool? firstOnly;

  BusinessImagesParams({
    this.refresh = true,
    required this.businessId,
    this.firstOnly = true,
  });
}
