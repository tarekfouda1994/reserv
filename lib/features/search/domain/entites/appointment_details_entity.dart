class AppointmentDetailsParams {

  final String id;
  final String businessID;
  final int appointmentType;
  final double longitude;
  final double latitude;
  final bool? refresh;

  AppointmentDetailsParams(
      {this.refresh = true,
      required this.id,
      required this.appointmentType,
      required this.businessID,
      required this.longitude,
      required this.latitude});

  Map<String, dynamic> toJson() => {
        "appointmentType": appointmentType,
        "id": id,
        "businessID": businessID,
        "latitude": latitude,
        "longitude": longitude,
      };
}
