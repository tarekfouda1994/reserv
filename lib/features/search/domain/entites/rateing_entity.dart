class RatingEntity {
  final int ratingVal;
  bool? selected;

  RatingEntity({required this.ratingVal, this.selected = false});
}
