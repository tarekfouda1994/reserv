class AddMsgChatParams {
  final String orderNumber;
  final String message;
  final String businessID;
  final int chatStatus;

  AddMsgChatParams(
      {required this.orderNumber,
      required this.businessID,
      required this.chatStatus,
      required this.message});

  Map<String, dynamic> toJson() => {
        "orderNumber": orderNumber,
        "message": message,
        "businessID": businessID,
        "chatStatus": chatStatus,
      };
}
