class HotDealsPrams {
  String filter;
  int pageNumber;
  int pageSize;
  num longitude;
  num latitude;
  bool refresh = false;

  HotDealsPrams(
      {required this.filter,
      required this.pageNumber,
      required this.pageSize,
      required this.longitude,
      required this.latitude,
      required this.refresh});

  Map<String, dynamic> toJson() => {
        "filter": filter,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
        "latitude": latitude,
        "longitude": longitude,
        "loadImages": true,
      };
}
