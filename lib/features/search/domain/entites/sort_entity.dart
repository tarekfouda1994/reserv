class SortParams {
  String title;
  String id;
  bool? selected = false;
  bool? sortDirection = false;

  SortParams({required this.title, required this.id, this.selected, this.sortDirection});
}
