class GetNoteParams {
  String orderID;
  bool? refresh = false;

  GetNoteParams({required this.orderID, this.refresh});

  Map<String, dynamic> toJson() => {
        "orderID": orderID,
      };
}
