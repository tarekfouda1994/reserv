import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/search/data/models/count_chat_model/count_chat_model.dart';
import 'package:flutter_tdd/features/search/data/models/hot_deals_model/hot_deals_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_cat_model/popular_cat_model.dart';
import 'package:flutter_tdd/features/search/data/models/popular_services_model/popular_services_model.dart';
import 'package:flutter_tdd/features/search/data/models/top_businesses_model/top_businesses_model.dart';
import 'package:flutter_tdd/features/search/data/models/trinding_model/trinding_model.dart';
import 'package:flutter_tdd/features/search/domain/entites/add_msg_chat_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/banners_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/edit_note_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/hot_deals_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/service_details_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/services_rate_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/sevice_search_entity.dart';
import 'package:flutter_tdd/features/search/domain/entites/top_business_params.dart';
import 'package:flutter_tdd/features/search/domain/entites/trinding_entity.dart';

import '../../data/models/appointment_details/appointment_details.dart';
import '../../data/models/appointment_orders/appointment_orders.dart';
import '../../data/models/business_search_model/business_search_model.dart';
import '../../data/models/business_staff_model/business_staff_model.dart';
import '../../data/models/chat_model/chat_model.dart';
import '../../data/models/home_banners/home_banners_model.dart';
import '../../data/models/note_model/notes_model.dart';
import '../../data/models/popular_search_model/popular_search_model.dart';
import '../../data/models/reviews_model/reviews_model.dart';
import '../../data/models/service_details_model/service_details_model.dart';
import '../../data/models/service_search_model/service_search_model.dart';
import '../../data/models/status_model/status_model.dart';
import '../entites/appointment_details_entity.dart';
import '../entites/appointment_order_entity.dart';
import '../entites/business_image_entity.dart';
import '../entites/business_staff_entity.dart';
import '../entites/cancel_order_entity.dart';
import '../entites/get_notes_params.dart';
import '../entites/msg_count_entity.dart';
import '../entites/note_entity.dart';
import '../entites/search_entity.dart';

abstract class BaseRepository {

  Future<Either<Failure, HotDealsModel>> getHotDeals(HotDealsPrams param);

  Future<Either<Failure, HomeBannersModel>> getMainBanners(BannersParams param);

  Future<Either<Failure, TrindingModel>> getTrending(TrendingPrams param);

  Future<Either<Failure, List<PopularServicesModel>>> getPopular(bool param);

  Future<Either<Failure, List<PopularSearchModel>>> getPopularSearch(
      bool param);

  Future<Either<Failure, List<PopularSearchModel>>> getRecentSearch(bool param);

  Future<Either<Failure, BusinessSearchModel>> getAllSearch(SearchPrams param);

  Future<Either<Failure, ServiceDetailsModel>> getServiceDetails(
      ServiceDetailsPrams prams);

  Future<Either<Failure, ReviewsModel>> getServiceReviews(
      ServiceDetailsPrams prams);

  Future<Either<Failure, ServiceSearchModel>> getAllServiceSearch(
      ServiceSearchParams param);

  Future<Either<Failure, List<StatusModel>>> getStatus(bool prams);

  Future<Either<Failure, List<AppointmentOrdersModel>>> getDashboardData(
      AppointmentOrderParams prams);

  Future<Either<Failure, AppointmentDetails>> getAppointmentDetails(
      AppointmentDetailsParams prams);

  Future<Either<Failure, List<BusinessStaffModel>>> getBusinessStaff(
      BusinessStaffParams prams);

  Future<Either<Failure, bool>> cancelOrder(CancelOrderParams param);

  Future<Either<Failure, bool>> cancelOrderDetails(CancelOrderParams param);

  Future<Either<Failure, bool>> removeNote(String param);

  Future<Either<Failure, bool>> removeAllNote(String param);

  Future<Either<Failure, bool>> addNote(AddNoteParams param);

  Future<Either<Failure, List<NotesModel>>> getNotes(GetNoteParams param);

  Future<Either<Failure, List<dynamic>>> getAppointmentImage(
      BusinessImagesParams param);

  Future<Either<Failure, TopBusinessesModel>> getTopBusinesses(
      TopBusinessParams param);

  Future<Either<Failure, TrindingModel>> getHomeServices(
      TrendingPrams param);

  Future<Either<Failure, bool>> rating(RateParams param);

  Future<Either<Failure, bool>> ratingServices(ServicesRateParams param);

  Future<Either<Failure, bool>> addMsgChat(AddMsgChatParams param);

  Future<Either<Failure, List<ChatModel>>> getAllMsgChat(String param);

  Future<Either<Failure, List<CountChatModel>>> getMsgCount(
      MsgCountParams param);

  Future<Either<Failure, List<PopularCatModel>>> getPopularCat(
      TopBusinessParams param);

  Future<Either<Failure, bool>> editNote(EditNoteParams param);

}
