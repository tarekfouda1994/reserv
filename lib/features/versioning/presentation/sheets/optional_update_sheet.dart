import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class OptionalUpdateSheet extends StatelessWidget {
  final VersionInfo versionInfo;
  final VoidCallback onUpdatePressed;

  const OptionalUpdateSheet({Key? key, required this.versionInfo, required this.onUpdatePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 47),
          Image.asset(Res.optional_update_image, width: 72, height: 72),
          const SizedBox(height: 24),
          MyText(
            title: "New Update Available",
            color: MyColors.grey,
            size: 16,
            alien: TextAlign.center,
          ),
          const SizedBox(height: 4),
          MyText(
            title: "A new version ${versionInfo.currentVersion} of Reserva app is now available!",
            alien: TextAlign.center,
            size: 12,
            color: MyColors.grey,
          ),
          const SizedBox(height: 26),
          DefaultButton(
            title: "Update",
            onTap: onUpdatePressed,
          ),
          const SizedBox(height: 14),
          TextButton(
              onPressed: () => Navigator.of(context).pop(),
              style: TextButton.styleFrom(
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  padding: EdgeInsets.zero,
                  backgroundColor: Colors.transparent),
              child: MyText(
                 title: "Cancel",
                size: 14,
                color: MyColors.blackOpacity,
              ),
          ),
          const SizedBox(height: 38),
        ],
      ),
    );
  }
}
