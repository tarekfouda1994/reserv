import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class RequiredUpdateSheet extends StatelessWidget {
  final VoidCallback onUpdatePressed;

  const RequiredUpdateSheet({Key? key, required this.onUpdatePressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 47),
            Image.asset(Res.required_update_image, width: 72, height: 72),
            const SizedBox(height: 24),
            MyText(
              title: "Update Required",
              size:16 ,
                color: MyColors.grey
            ),
            const SizedBox(height: 4),
            MyText(
              title: "Your app is out of date.\nPlease update to the latest version",
              size: 12,
              color: MyColors.grey,
              alien: TextAlign.center,
            ),
            const SizedBox(height: 26),
            DefaultButton(
              title: "Update",
              onTap: onUpdatePressed,
            ),
            const SizedBox(height: 58),
          ],
        ),
      ),
    );
  }
}
