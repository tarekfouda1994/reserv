import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/helpers/helper_methods.dart';
import 'package:flutter_tdd/features/versioning/data/repository/i_versioning_repository.dart';
import 'package:flutter_tdd/features/versioning/domain/enums/current_version_state_enum.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';
import 'package:flutter_tdd/features/versioning/presentation/controller/versioning_bloc.dart';
import 'package:flutter_tdd/features/versioning/presentation/sheets/optional_update_sheet.dart';
import 'package:flutter_tdd/features/versioning/presentation/sheets/required_update_sheet.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class VersioningController {
  static VersioningController get instance => getIt.get<VersioningController>();

  BuildContext get _context => getIt.get<GlobalContext>().context();
  final versioningBloc = VersioningBloc();

  VersioningController();

  Future<void> preFetchVersioningInfo() async {
    versioningBloc.getVersionInfo();
  }

  Future<void> checkVersion() async {
    try {
      final fetchSuccess =
          versioningBloc.state.maybeWhen(success: (model) => true, orElse: () => false);

      /// Fetch not success
      if (fetchSuccess != true) {
        /// Re-Fetch and check again
        await versioningBloc.getVersionInfo();
        return checkVersion();
      } else {
        /// Fetch success
        final versionInfo = versioningBloc.data!;
        _handleVersioningState(versionInfo);
      }
    } catch (e, s) {
      /// TODO CrashReport
      log(e.toString());
      log(s.toString());
    }
  }

  void _handleVersioningState(VersionInfo versionInfo) {
    switch (versionInfo.versionState) {
      case CurrentVersionStateEnum.unSupported:
        _showRequiredUpdateSheet(versionInfo);
        break;
      case CurrentVersionStateEnum.stillSupported:
        if (versionInfo.shouldInform) {
          _showOptionalUpdateSheet(versionInfo);
        }
        break;
      case CurrentVersionStateEnum.upToDate:
      // Do nothing
    }

    /// Mark latest version as informed
    _setLastVersionUserInformedAbout();
  }

  Future<void> _showOptionalUpdateSheet(VersionInfo versionInfo) {
    return showModalBottomSheet(
        context: _context,
        elevation: 0.0,
        isDismissible: true,
        isScrollControlled: true,
        enableDrag: true,
        barrierColor: const Color.fromRGBO(0, 0, 0, 0.35),
        shape: const OutlineInputBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
            borderSide: BorderSide.none),
        builder: (context) => ClipRRect(
            borderRadius: const BorderRadius.vertical(top: Radius.circular(30)),
            child: OptionalUpdateSheet(
              onUpdatePressed: () {
                Navigator.of(_context).pop();
                goToAppInStore();
              },
              versionInfo: versionInfo,
            )));
  }

  Future<void> _showRequiredUpdateSheet(VersionInfo versionInfo) {
    return showModalBottomSheet(
        context: _context,
        elevation: 0.0,
        isDismissible: true,
        isScrollControlled: true,
        enableDrag: false,
        barrierColor: const Color.fromRGBO(0, 0, 0, 0.35),
        shape: const OutlineInputBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
            borderSide: BorderSide.none),
        builder: (context) => ClipRRect(
            borderRadius: const BorderRadius.vertical(top: Radius.circular(30)),
            child: RequiredUpdateSheet(onUpdatePressed: () {
              goToAppInStore();
            })));
  }

  Future<void> goToAppInStore() async {
    if (versioningBloc.hasData) {
      await HelperMethods.instance.customLaunchURL(url: versioningBloc.data!.value.updateURL);
    }
  }

  void refreshInfoTransparently() {
    versioningBloc.refreshInfoTransparently();
  }

  Future<void> _setLastVersionUserInformedAbout() async {
    try {
      if (versioningBloc.data != null) {
        return getIt.get<IVersioningRepository>()
            .setLastVersionUserInformedAbout(versioningBloc.data!.value.latestVersion);
      }
    } catch (_) {}
  }
}
