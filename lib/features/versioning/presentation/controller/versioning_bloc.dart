import 'dart:developer';

import 'package:flutter_tdd/core/bloc/base_bloc/base_bloc.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/features/versioning/data/repository/i_versioning_repository.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';

class VersioningBloc extends BaseBloc<VersionInfo> {
  final _repository = getIt.get<IVersioningRepository>();

  VersioningBloc();

  Future<void> getVersionInfo() async {
    try {
      loadingState();
      final result = await _repository.getVersionInfo();
      result.when(isSuccess: (data) {
        successState(data);
      }, isError: (error) {
        failedState(error, getVersionInfo);
      });
    } catch (e, s) {
      /// TODO CrashReport
      log(e.toString());
      log(s.toString());
    }
  }

  Future<void> refreshInfoTransparently() async {
    try {
      final result = await _repository.getVersionInfo();
      result.when(
          isSuccess: (data) {
            successState(data);
          },
          isError: (_) {});
    } catch (_) {}
  }
}
