import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

class TransparentButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;

  const TransparentButton({Key? key, required this.onPressed, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42,
      child: MaterialButton(
        onPressed: onPressed,
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        height: 42,
        elevation: 0.0,
        focusElevation: 0.0,
        highlightElevation: 0.0,
        hoverElevation: 0.0,
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8), side: BorderSide(color: MyColors.primary),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: MyText(
          title: text,
          size: 16,
          color: MyColors.primary,
          alien: TextAlign.center,
        ),
      ),
    );
  }
}
