import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/base_bloc/base_state.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/features/versioning/domain/enums/current_version_state_enum.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';
import 'package:flutter_tdd/features/versioning/presentation/controller/versioning_bloc.dart';
import 'package:flutter_tdd/features/versioning/presentation/controller/versioning_controller.dart';
import 'package:flutter_tdd/features/versioning/presentation/widgets/transparent_button.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

class VersionStateBuilder extends StatefulWidget {
  const VersionStateBuilder({Key? key}) : super(key: key);

  @override
  State<VersionStateBuilder> createState() => _VersionStateBuilderState();
}

class _VersionStateBuilderState extends State<VersionStateBuilder> {
  @override
  void initState() {
    super.initState();
    VersioningController.instance.refreshInfoTransparently();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VersioningBloc, BaseState<VersionInfo>>(
      bloc: VersioningController.instance.versioningBloc,
      builder: (context, state) {
        return state.maybeWhen(
          success: (data) {
            final versionInfo = data!;
            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                MyText(
                  title: "App Version ${versionInfo.currentVersion}",
                  size: 14,
                  color: MyColors.grey,
                ),
                if (versionInfo.versionState != CurrentVersionStateEnum.upToDate)
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: TransparentButton(
                      text: "Update to ${versionInfo.value.latestVersion}" ,
                      onPressed: () => VersioningController.instance.goToAppInStore(),
                    ),
                  ),
              ],
            );
          },
          orElse: () => const SizedBox(),
        );
      },
    );
  }
}
