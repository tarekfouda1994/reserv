import 'package:flutter_tdd/core/result/result.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';

abstract class IVersioningRepository {
  Future<MyResult<VersionInfo>> getVersionInfo();

  Future<MyResult<String>> getCurrentVersion();

  String? getLastVersionUserInformedAbout();

  Future<void> setLastVersionUserInformedAbout(String lastVersion);
}
