import 'package:flutter_tdd/core/result/result.dart';
import 'package:flutter_tdd/features/versioning/data/models/version_info_model.dart';

abstract class IVersioningRemoteDataSource {
  Future<MyResult<VersionInfoModel>> getVersionInfo(String hostAsDocId);
}
