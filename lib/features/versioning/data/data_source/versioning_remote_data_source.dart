import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_tdd/core/firebase/firebase_collections.dart';
import 'package:flutter_tdd/core/firebase/firebase_provider.dart';
import 'package:flutter_tdd/core/result/result.dart';
import 'package:flutter_tdd/features/versioning/data/data_source/i_versioning_remote_data_source.dart';
import 'package:flutter_tdd/features/versioning/data/models/version_info_model.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: IVersioningRemoteDataSource)
class VersioningRemoteDataSource implements IVersioningRemoteDataSource {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  @override
  Future<MyResult<VersionInfoModel>> getVersionInfo(String hostAsDocId) async {
    final result = await FirebaseProvider.requestDataObject<VersionInfoModel>(
      callback: () async {
        return _firestore
            .collection(FirebaseCollections.global)
            .doc(hostAsDocId)
            .get(const GetOptions(source: Source.server));
      },
      converter: (snapshot) {
        return VersionInfoModel.fromJson(snapshot.data() ?? <String, dynamic>{});
      },
    );
    return result;
  }
}
