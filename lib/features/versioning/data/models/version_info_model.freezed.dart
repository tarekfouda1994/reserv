// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'version_info_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

VersionInfoModel _$VersionInfoModelFromJson(Map<String, dynamic> json) {
  return _VersionInfoModel.fromJson(json);
}

/// @nodoc
mixin _$VersionInfoModel {
  PlatformVersionModel get android => throw _privateConstructorUsedError;
  PlatformVersionModel get ios => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $VersionInfoModelCopyWith<VersionInfoModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VersionInfoModelCopyWith<$Res> {
  factory $VersionInfoModelCopyWith(
          VersionInfoModel value, $Res Function(VersionInfoModel) then) =
      _$VersionInfoModelCopyWithImpl<$Res>;
  $Res call({PlatformVersionModel android, PlatformVersionModel ios});

  $PlatformVersionModelCopyWith<$Res> get android;
  $PlatformVersionModelCopyWith<$Res> get ios;
}

/// @nodoc
class _$VersionInfoModelCopyWithImpl<$Res>
    implements $VersionInfoModelCopyWith<$Res> {
  _$VersionInfoModelCopyWithImpl(this._value, this._then);

  final VersionInfoModel _value;
  // ignore: unused_field
  final $Res Function(VersionInfoModel) _then;

  @override
  $Res call({
    Object? android = freezed,
    Object? ios = freezed,
  }) {
    return _then(_value.copyWith(
      android: android == freezed
          ? _value.android
          : android // ignore: cast_nullable_to_non_nullable
              as PlatformVersionModel,
      ios: ios == freezed
          ? _value.ios
          : ios // ignore: cast_nullable_to_non_nullable
              as PlatformVersionModel,
    ));
  }

  @override
  $PlatformVersionModelCopyWith<$Res> get android {
    return $PlatformVersionModelCopyWith<$Res>(_value.android, (value) {
      return _then(_value.copyWith(android: value));
    });
  }

  @override
  $PlatformVersionModelCopyWith<$Res> get ios {
    return $PlatformVersionModelCopyWith<$Res>(_value.ios, (value) {
      return _then(_value.copyWith(ios: value));
    });
  }
}

/// @nodoc
abstract class _$$_VersionInfoModelCopyWith<$Res>
    implements $VersionInfoModelCopyWith<$Res> {
  factory _$$_VersionInfoModelCopyWith(
          _$_VersionInfoModel value, $Res Function(_$_VersionInfoModel) then) =
      __$$_VersionInfoModelCopyWithImpl<$Res>;
  @override
  $Res call({PlatformVersionModel android, PlatformVersionModel ios});

  @override
  $PlatformVersionModelCopyWith<$Res> get android;
  @override
  $PlatformVersionModelCopyWith<$Res> get ios;
}

/// @nodoc
class __$$_VersionInfoModelCopyWithImpl<$Res>
    extends _$VersionInfoModelCopyWithImpl<$Res>
    implements _$$_VersionInfoModelCopyWith<$Res> {
  __$$_VersionInfoModelCopyWithImpl(
      _$_VersionInfoModel _value, $Res Function(_$_VersionInfoModel) _then)
      : super(_value, (v) => _then(v as _$_VersionInfoModel));

  @override
  _$_VersionInfoModel get _value => super._value as _$_VersionInfoModel;

  @override
  $Res call({
    Object? android = freezed,
    Object? ios = freezed,
  }) {
    return _then(_$_VersionInfoModel(
      android: android == freezed
          ? _value.android
          : android // ignore: cast_nullable_to_non_nullable
              as PlatformVersionModel,
      ios: ios == freezed
          ? _value.ios
          : ios // ignore: cast_nullable_to_non_nullable
              as PlatformVersionModel,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_VersionInfoModel extends _VersionInfoModel {
  _$_VersionInfoModel({required this.android, required this.ios}) : super._();

  factory _$_VersionInfoModel.fromJson(Map<String, dynamic> json) =>
      _$$_VersionInfoModelFromJson(json);

  @override
  final PlatformVersionModel android;
  @override
  final PlatformVersionModel ios;

  @override
  String toString() {
    return 'VersionInfoModel(android: $android, ios: $ios)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_VersionInfoModel &&
            const DeepCollectionEquality().equals(other.android, android) &&
            const DeepCollectionEquality().equals(other.ios, ios));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(android),
      const DeepCollectionEquality().hash(ios));

  @JsonKey(ignore: true)
  @override
  _$$_VersionInfoModelCopyWith<_$_VersionInfoModel> get copyWith =>
      __$$_VersionInfoModelCopyWithImpl<_$_VersionInfoModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_VersionInfoModelToJson(
      this,
    );
  }
}

abstract class _VersionInfoModel extends VersionInfoModel {
  factory _VersionInfoModel(
      {required final PlatformVersionModel android,
      required final PlatformVersionModel ios}) = _$_VersionInfoModel;
  _VersionInfoModel._() : super._();

  factory _VersionInfoModel.fromJson(Map<String, dynamic> json) =
      _$_VersionInfoModel.fromJson;

  @override
  PlatformVersionModel get android;
  @override
  PlatformVersionModel get ios;
  @override
  @JsonKey(ignore: true)
  _$$_VersionInfoModelCopyWith<_$_VersionInfoModel> get copyWith =>
      throw _privateConstructorUsedError;
}
