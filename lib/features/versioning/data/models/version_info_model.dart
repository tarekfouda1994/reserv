import 'package:flutter_tdd/core/models/api_model/base_api_model.dart';
import 'package:flutter_tdd/features/versioning/data/models/platform_version_model.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'version_info_model.freezed.dart';
part 'version_info_model.g.dart';

@freezed
class VersionInfoModel extends BaseApiModel<VersionInfo> with _$VersionInfoModel {
  const VersionInfoModel._();

  @JsonSerializable(explicitToJson: true)
  factory VersionInfoModel({
    required PlatformVersionModel android,
    required PlatformVersionModel ios,
  }) = _VersionInfoModel;

  factory VersionInfoModel.fromJson(Map<String, dynamic> json) => _$VersionInfoModelFromJson(json);

  @override
  @protected
  VersionInfo toDomainModel() {
    assert(false, 'This method should not be used');
    return VersionInfo.fromMultiPlatforms(
      android: android.toDomainModel(),
      ios: ios.toDomainModel(),
      currentVersion: '0.0.1',
    );
  }

  VersionInfo toCompleteDomainModel(String current) {
    return VersionInfo.fromMultiPlatforms(
      android: android.toDomainModel(),
      ios: ios.toDomainModel(),
      currentVersion: current,
    );
  }
}
