// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'version_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_VersionInfoModel _$$_VersionInfoModelFromJson(Map<String, dynamic> json) =>
    _$_VersionInfoModel(
      android: PlatformVersionModel.fromJson(
          json['android'] as Map<String, dynamic>),
      ios: PlatformVersionModel.fromJson(json['ios'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_VersionInfoModelToJson(_$_VersionInfoModel instance) =>
    <String, dynamic>{
      'android': instance.android.toJson(),
      'ios': instance.ios.toJson(),
    };
