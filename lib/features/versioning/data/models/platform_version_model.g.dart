// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'platform_version_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PlatformVersionModel _$$_PlatformVersionModelFromJson(
        Map<String, dynamic> json) =>
    _$_PlatformVersionModel(
      latestVersion: json['latest_version'] as String,
      minVersionSupported: json['min_version_supported'] as String,
      updateURL: json['update_url'] as String,
    );

Map<String, dynamic> _$$_PlatformVersionModelToJson(
        _$_PlatformVersionModel instance) =>
    <String, dynamic>{
      'latest_version': instance.latestVersion,
      'min_version_supported': instance.minVersionSupported,
      'update_url': instance.updateURL,
    };
