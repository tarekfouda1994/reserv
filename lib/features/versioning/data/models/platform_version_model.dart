import 'package:flutter_tdd/core/models/api_model/base_api_model.dart';
import 'package:flutter_tdd/features/versioning/domain/models/platform_version.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'platform_version_model.freezed.dart';
part 'platform_version_model.g.dart';

@freezed
class PlatformVersionModel extends BaseApiModel<PlatformVersion> with _$PlatformVersionModel {
  const PlatformVersionModel._();

  @JsonSerializable(explicitToJson: true)
  factory PlatformVersionModel({
    @JsonKey(name: 'latest_version') required String latestVersion,
    @JsonKey(name: 'min_version_supported') required String minVersionSupported,
    @JsonKey(name: 'update_url') required String updateURL,
  }) = _PlatformVersionModel;

  factory PlatformVersionModel.fromJson(Map<String, dynamic> json) =>
      _$PlatformVersionModelFromJson(json);

  @override
  PlatformVersion toDomainModel() {
    return PlatformVersion(
        latestVersion: latestVersion,
        minVersionSupported: minVersionSupported,
        updateURL: updateURL);
  }
}
