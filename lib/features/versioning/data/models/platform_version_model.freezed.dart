// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'platform_version_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PlatformVersionModel _$PlatformVersionModelFromJson(Map<String, dynamic> json) {
  return _PlatformVersionModel.fromJson(json);
}

/// @nodoc
mixin _$PlatformVersionModel {
  @JsonKey(name: 'latest_version')
  String get latestVersion => throw _privateConstructorUsedError;
  @JsonKey(name: 'min_version_supported')
  String get minVersionSupported => throw _privateConstructorUsedError;
  @JsonKey(name: 'update_url')
  String get updateURL => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PlatformVersionModelCopyWith<PlatformVersionModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlatformVersionModelCopyWith<$Res> {
  factory $PlatformVersionModelCopyWith(PlatformVersionModel value,
          $Res Function(PlatformVersionModel) then) =
      _$PlatformVersionModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'latest_version') String latestVersion,
      @JsonKey(name: 'min_version_supported') String minVersionSupported,
      @JsonKey(name: 'update_url') String updateURL});
}

/// @nodoc
class _$PlatformVersionModelCopyWithImpl<$Res>
    implements $PlatformVersionModelCopyWith<$Res> {
  _$PlatformVersionModelCopyWithImpl(this._value, this._then);

  final PlatformVersionModel _value;
  // ignore: unused_field
  final $Res Function(PlatformVersionModel) _then;

  @override
  $Res call({
    Object? latestVersion = freezed,
    Object? minVersionSupported = freezed,
    Object? updateURL = freezed,
  }) {
    return _then(_value.copyWith(
      latestVersion: latestVersion == freezed
          ? _value.latestVersion
          : latestVersion // ignore: cast_nullable_to_non_nullable
              as String,
      minVersionSupported: minVersionSupported == freezed
          ? _value.minVersionSupported
          : minVersionSupported // ignore: cast_nullable_to_non_nullable
              as String,
      updateURL: updateURL == freezed
          ? _value.updateURL
          : updateURL // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PlatformVersionModelCopyWith<$Res>
    implements $PlatformVersionModelCopyWith<$Res> {
  factory _$$_PlatformVersionModelCopyWith(_$_PlatformVersionModel value,
          $Res Function(_$_PlatformVersionModel) then) =
      __$$_PlatformVersionModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'latest_version') String latestVersion,
      @JsonKey(name: 'min_version_supported') String minVersionSupported,
      @JsonKey(name: 'update_url') String updateURL});
}

/// @nodoc
class __$$_PlatformVersionModelCopyWithImpl<$Res>
    extends _$PlatformVersionModelCopyWithImpl<$Res>
    implements _$$_PlatformVersionModelCopyWith<$Res> {
  __$$_PlatformVersionModelCopyWithImpl(_$_PlatformVersionModel _value,
      $Res Function(_$_PlatformVersionModel) _then)
      : super(_value, (v) => _then(v as _$_PlatformVersionModel));

  @override
  _$_PlatformVersionModel get _value => super._value as _$_PlatformVersionModel;

  @override
  $Res call({
    Object? latestVersion = freezed,
    Object? minVersionSupported = freezed,
    Object? updateURL = freezed,
  }) {
    return _then(_$_PlatformVersionModel(
      latestVersion: latestVersion == freezed
          ? _value.latestVersion
          : latestVersion // ignore: cast_nullable_to_non_nullable
              as String,
      minVersionSupported: minVersionSupported == freezed
          ? _value.minVersionSupported
          : minVersionSupported // ignore: cast_nullable_to_non_nullable
              as String,
      updateURL: updateURL == freezed
          ? _value.updateURL
          : updateURL // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PlatformVersionModel extends _PlatformVersionModel {
  _$_PlatformVersionModel(
      {@JsonKey(name: 'latest_version') required this.latestVersion,
      @JsonKey(name: 'min_version_supported') required this.minVersionSupported,
      @JsonKey(name: 'update_url') required this.updateURL})
      : super._();

  factory _$_PlatformVersionModel.fromJson(Map<String, dynamic> json) =>
      _$$_PlatformVersionModelFromJson(json);

  @override
  @JsonKey(name: 'latest_version')
  final String latestVersion;
  @override
  @JsonKey(name: 'min_version_supported')
  final String minVersionSupported;
  @override
  @JsonKey(name: 'update_url')
  final String updateURL;

  @override
  String toString() {
    return 'PlatformVersionModel(latestVersion: $latestVersion, minVersionSupported: $minVersionSupported, updateURL: $updateURL)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlatformVersionModel &&
            const DeepCollectionEquality()
                .equals(other.latestVersion, latestVersion) &&
            const DeepCollectionEquality()
                .equals(other.minVersionSupported, minVersionSupported) &&
            const DeepCollectionEquality().equals(other.updateURL, updateURL));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(latestVersion),
      const DeepCollectionEquality().hash(minVersionSupported),
      const DeepCollectionEquality().hash(updateURL));

  @JsonKey(ignore: true)
  @override
  _$$_PlatformVersionModelCopyWith<_$_PlatformVersionModel> get copyWith =>
      __$$_PlatformVersionModelCopyWithImpl<_$_PlatformVersionModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PlatformVersionModelToJson(
      this,
    );
  }
}

abstract class _PlatformVersionModel extends PlatformVersionModel {
  factory _PlatformVersionModel(
      {@JsonKey(name: 'latest_version')
          required final String latestVersion,
      @JsonKey(name: 'min_version_supported')
          required final String minVersionSupported,
      @JsonKey(name: 'update_url')
          required final String updateURL}) = _$_PlatformVersionModel;
  _PlatformVersionModel._() : super._();

  factory _PlatformVersionModel.fromJson(Map<String, dynamic> json) =
      _$_PlatformVersionModel.fromJson;

  @override
  @JsonKey(name: 'latest_version')
  String get latestVersion;
  @override
  @JsonKey(name: 'min_version_supported')
  String get minVersionSupported;
  @override
  @JsonKey(name: 'update_url')
  String get updateURL;
  @override
  @JsonKey(ignore: true)
  _$$_PlatformVersionModelCopyWith<_$_PlatformVersionModel> get copyWith =>
      throw _privateConstructorUsedError;
}
