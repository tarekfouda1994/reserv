import 'dart:async';
import 'dart:developer';

import 'package:flutter_tdd/core/errors/base_error.dart';
import 'package:flutter_tdd/core/extensions/string_helper_extension.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/string_helper.dart';
import 'package:flutter_tdd/core/result/result.dart';
import 'package:flutter_tdd/features/versioning/data/data_source/i_versioning_remote_data_source.dart';
import 'package:flutter_tdd/features/versioning/data/models/version_info_model.dart';
import 'package:flutter_tdd/features/versioning/data/repository/i_versioning_repository.dart';
import 'package:flutter_tdd/features/versioning/domain/models/version_info.dart';
import 'package:injectable/injectable.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

@Injectable(as: IVersioningRepository)
class VersioningRepository extends IVersioningRepository {
  static const keyLastVersion = 'keyLastVersion';
  final _remoteDataSource = getIt.get<IVersioningRemoteDataSource>();
  int _getCurrentVersionRetryDelay = 2;
  int _getVersionRetryDelay = 2;
  String? _lastVersionInformedAbout;
  String? _currentVersion;
  String? _buildNumber;
  String? _appName;


  @override
  Future<MyResult<VersionInfo>> getVersionInfo() async {
    try {
      final hostAsDocId = StringHelper.getVersionFirebaseDocId("Reserva");
      final result = await _remoteDataSource.getVersionInfo(hostAsDocId);
      return result.when(
        isSuccess: _onVersionInfoSuccess,
        isError: _onVersionInfoError,
      );
    } catch (e, s) {
      log('$e\n$s');
      return MyResult.unknownError();
    }
  }

  @override
  Future<MyResult<String>> getCurrentVersion() async {
    final version = _currentVersion;
    if (version?.isNotEmpty == true) {
      return MyResult.isSuccess(version!);
    } else {
      try {
        await fetchVersionInfo();
        await Future.delayed(Duration(seconds: _getCurrentVersionRetryDelay));
        _getCurrentVersionRetryDelay += _getCurrentVersionRetryDelay;
        return getCurrentVersion();
      } catch (e, s) {
        log('$e\n$s');
        return MyResult.unknownError();
      }
    }
  }

  @override
  String? getLastVersionUserInformedAbout() {
    if (_lastVersionInformedAbout?.isNotEmpty == true) {
      return _lastVersionInformedAbout;
    } else {
      final res = getLastVersionInformedAbout();
      _lastVersionInformedAbout = res;
      return res;
    }
  }

  @override
  Future<void> setLastVersionUserInformedAbout(String lastVersion) {
    _lastVersionInformedAbout = lastVersion;
    return setLastVersionInformedAbout(lastVersion);
  }

  FutureOr<MyResult<VersionInfo>> _onVersionInfoSuccess(VersionInfoModel? data) async {
    final currentResult = await getCurrentVersion();
    return currentResult.when(
      isSuccess: (currentData) {
        return MyResult.isSuccess(data!.toCompleteDomainModel(currentData!));
      },
      isError: (error) {
        return MyResult.isError(error);
      },
    );
  }

  FutureOr<MyResult<VersionInfo>> _onVersionInfoError(BaseError error) async {
    await Future.delayed(Duration(seconds: _getVersionRetryDelay));
    _getVersionRetryDelay += _getVersionRetryDelay;
    return getVersionInfo();
  }


  Future<void> fetchVersionInfo() async {
    try {
      /// Return if we already have version info
      if (_currentVersion.isNotBlank && _buildNumber.isNotBlank && _appName.isNotBlank) return;
      final packageInfo = await PackageInfo.fromPlatform();
      _currentVersion = packageInfo.version;
      _buildNumber = packageInfo.buildNumber;
      _appName = packageInfo.appName;
    } catch (e, s) {
      /// TODO CrashReport
      log(e.toString());
      log(s.toString());
    }
  }

  String? getLastVersionInformedAbout() {
    final prefs = getIt.get<SharedPreferences>();
    return prefs.getString(keyLastVersion);
  }

  Future<void> setLastVersionInformedAbout(String lastVersion) {
    final prefs = getIt.get<SharedPreferences>();
    return prefs.setString(keyLastVersion, lastVersion);
  }
}
