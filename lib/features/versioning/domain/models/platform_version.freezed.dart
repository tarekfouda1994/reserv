// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'platform_version.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PlatformVersion {
  String get latestVersion => throw _privateConstructorUsedError;
  String get minVersionSupported => throw _privateConstructorUsedError;
  String get updateURL => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PlatformVersionCopyWith<PlatformVersion> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlatformVersionCopyWith<$Res> {
  factory $PlatformVersionCopyWith(
          PlatformVersion value, $Res Function(PlatformVersion) then) =
      _$PlatformVersionCopyWithImpl<$Res>;
  $Res call(
      {String latestVersion, String minVersionSupported, String updateURL});
}

/// @nodoc
class _$PlatformVersionCopyWithImpl<$Res>
    implements $PlatformVersionCopyWith<$Res> {
  _$PlatformVersionCopyWithImpl(this._value, this._then);

  final PlatformVersion _value;
  // ignore: unused_field
  final $Res Function(PlatformVersion) _then;

  @override
  $Res call({
    Object? latestVersion = freezed,
    Object? minVersionSupported = freezed,
    Object? updateURL = freezed,
  }) {
    return _then(_value.copyWith(
      latestVersion: latestVersion == freezed
          ? _value.latestVersion
          : latestVersion // ignore: cast_nullable_to_non_nullable
              as String,
      minVersionSupported: minVersionSupported == freezed
          ? _value.minVersionSupported
          : minVersionSupported // ignore: cast_nullable_to_non_nullable
              as String,
      updateURL: updateURL == freezed
          ? _value.updateURL
          : updateURL // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PlatformVersionCopyWith<$Res>
    implements $PlatformVersionCopyWith<$Res> {
  factory _$$_PlatformVersionCopyWith(
          _$_PlatformVersion value, $Res Function(_$_PlatformVersion) then) =
      __$$_PlatformVersionCopyWithImpl<$Res>;
  @override
  $Res call(
      {String latestVersion, String minVersionSupported, String updateURL});
}

/// @nodoc
class __$$_PlatformVersionCopyWithImpl<$Res>
    extends _$PlatformVersionCopyWithImpl<$Res>
    implements _$$_PlatformVersionCopyWith<$Res> {
  __$$_PlatformVersionCopyWithImpl(
      _$_PlatformVersion _value, $Res Function(_$_PlatformVersion) _then)
      : super(_value, (v) => _then(v as _$_PlatformVersion));

  @override
  _$_PlatformVersion get _value => super._value as _$_PlatformVersion;

  @override
  $Res call({
    Object? latestVersion = freezed,
    Object? minVersionSupported = freezed,
    Object? updateURL = freezed,
  }) {
    return _then(_$_PlatformVersion(
      latestVersion: latestVersion == freezed
          ? _value.latestVersion
          : latestVersion // ignore: cast_nullable_to_non_nullable
              as String,
      minVersionSupported: minVersionSupported == freezed
          ? _value.minVersionSupported
          : minVersionSupported // ignore: cast_nullable_to_non_nullable
              as String,
      updateURL: updateURL == freezed
          ? _value.updateURL
          : updateURL // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_PlatformVersion extends _PlatformVersion {
  _$_PlatformVersion(
      {required this.latestVersion,
      required this.minVersionSupported,
      required this.updateURL})
      : super._();

  @override
  final String latestVersion;
  @override
  final String minVersionSupported;
  @override
  final String updateURL;

  @override
  String toString() {
    return 'PlatformVersion(latestVersion: $latestVersion, minVersionSupported: $minVersionSupported, updateURL: $updateURL)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PlatformVersion &&
            const DeepCollectionEquality()
                .equals(other.latestVersion, latestVersion) &&
            const DeepCollectionEquality()
                .equals(other.minVersionSupported, minVersionSupported) &&
            const DeepCollectionEquality().equals(other.updateURL, updateURL));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(latestVersion),
      const DeepCollectionEquality().hash(minVersionSupported),
      const DeepCollectionEquality().hash(updateURL));

  @JsonKey(ignore: true)
  @override
  _$$_PlatformVersionCopyWith<_$_PlatformVersion> get copyWith =>
      __$$_PlatformVersionCopyWithImpl<_$_PlatformVersion>(this, _$identity);
}

abstract class _PlatformVersion extends PlatformVersion {
  factory _PlatformVersion(
      {required final String latestVersion,
      required final String minVersionSupported,
      required final String updateURL}) = _$_PlatformVersion;
  _PlatformVersion._() : super._();

  @override
  String get latestVersion;
  @override
  String get minVersionSupported;
  @override
  String get updateURL;
  @override
  @JsonKey(ignore: true)
  _$$_PlatformVersionCopyWith<_$_PlatformVersion> get copyWith =>
      throw _privateConstructorUsedError;
}
