import 'package:flutter_tdd/core/models/domain_model/base_domain_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'platform_version.freezed.dart';

@freezed
class PlatformVersion extends BaseDomainModel with _$PlatformVersion {
  const PlatformVersion._();

  factory PlatformVersion({
    required String latestVersion,
    required String minVersionSupported,
    required String updateURL,
  }) = _PlatformVersion;
}
