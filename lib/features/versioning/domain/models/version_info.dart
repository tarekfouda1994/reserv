import 'dart:io';

import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/models/domain_model/base_domain_model.dart';
import 'package:flutter_tdd/features/versioning/data/repository/i_versioning_repository.dart';
import 'package:flutter_tdd/features/versioning/domain/enums/current_version_state_enum.dart';
import 'package:flutter_tdd/features/versioning/domain/models/platform_version.dart';
import 'package:version/version.dart';


class VersionInfo extends BaseDomainModel {
  final PlatformVersion value;
  final String currentVersion;

  const VersionInfo({required this.value, required this.currentVersion});

  factory VersionInfo.fromMultiPlatforms({
    required PlatformVersion android,
    required PlatformVersion ios,
    required String currentVersion,
  }) {
    final ver = () {
      if (Platform.isAndroid) {
        return android;
      } else {
        return ios;
      }
    }();
    return VersionInfo(value: ver, currentVersion: currentVersion);
  }

  /// Determine weather the user has been informed about latest version before
  bool get shouldInform {
    if (versionState == CurrentVersionStateEnum.upToDate) return false;
    final repo = getIt.get<IVersioningRepository>();
    if (value.latestVersion == repo.getLastVersionUserInformedAbout()) {
      return false;
    } else {
      return true;
    }
  }

  CurrentVersionStateEnum get versionState {
    final current = Version.parse(currentVersion);
    final minimumSupported = Version.parse(value.minVersionSupported);
    final latest = Version.parse(value.latestVersion);

    if (current >= minimumSupported) {
      if (current >= latest) {
        /// Up to date
        return CurrentVersionStateEnum.upToDate;
      } else {
        /// Still supported
        return CurrentVersionStateEnum.stillSupported;
      }
    } else {
      /// Not supported
      return CurrentVersionStateEnum.unSupported;
    }
  }
}
