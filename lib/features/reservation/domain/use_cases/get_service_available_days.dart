import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_date_model/service_date_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/available_days_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

class GetServiceAvailableDays implements UseCase<List<ServiceDateModel>, AvailableDaysEntity> {
  @override
  Future<List<ServiceDateModel>> call(AvailableDaysEntity params) async {
    var data = await getIt<ReservationRepository>().getServiceAvailableDays(params);
    return data.fold((l) => [], (r) => r);
  }
}
