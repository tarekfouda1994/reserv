import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/data/models/promo_model/promo_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/get_promo_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

class GetPromo implements UseCase<List<PromoModel>, GetPromoParams> {
  @override
  Future<List<PromoModel>> call(GetPromoParams params) async {
    var data = await getIt<ReservationRepository>().getPromos(params);
    return data.fold((l) => [], (r) => r);
  }
}
