import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/data/models/services_times_models/services_times_models.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/together_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

class GetReserveTogetherData implements UseCase<ServicesTimesModels?, TogetherEntity> {
  @override
  Future<ServicesTimesModels?> call(TogetherEntity params) async {
    var data = await getIt<ReservationRepository>().getReserveTogetherTimes(params);
    return data.fold((l) => null, (r) => r);
  }
}
