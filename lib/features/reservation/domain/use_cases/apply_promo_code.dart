import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/data/models/apply_promo_code_model/apply_promo_code_model.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

import '../../../../core/helpers/di.dart';
import '../entities/reservation_entity.dart';

class ApplyPromoCode implements UseCase<ApplyPromoCodeModel?, ReservationEntity> {
  @override
  Future<ApplyPromoCodeModel?> call(ReservationEntity params) async {
    var result = await getIt<ReservationRepository>().applyPromoCode(params);
    return result.fold((l) => null, (r) => r);
  }
}
