import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

import '../../../../core/helpers/di.dart';
import '../../data/models/appointment_success_model/appointment_success_model.dart';
import '../entities/reservation_entity.dart';

class MakeReservation implements UseCase<AppointmentSuccessModel?, ReservationEntity> {
  @override
  Future<AppointmentSuccessModel?> call(ReservationEntity params) async {
    var result = await getIt<ReservationRepository>().makeReservation(params);
    return result.fold((l) => null, (r) => r);
  }
}
