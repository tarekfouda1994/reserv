import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_types_model/service_types_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/business_service_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

class GetCompanyServices implements UseCase<List<ServiceTypesModel>, BusinessServicePrams> {
  @override
  Future<List<ServiceTypesModel>> call(BusinessServicePrams params) async {
    var data = await getIt<ReservationRepository>().getCompanyService(params);
    return data.fold((l) => [], (r) => r);
  }
}
