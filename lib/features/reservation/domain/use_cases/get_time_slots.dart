import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/usecases/use_case.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/time_slot_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/repositories/reservation_repo.dart';

class GetTimeSlots implements UseCase<List<String>, TimeSlotEntity> {
  @override
  Future<List<String>> call(TimeSlotEntity params) async {
    var data = await getIt<ReservationRepository>().getTimesSlots(params);
    return data.fold((l) => [], (r) => r);
  }
}
