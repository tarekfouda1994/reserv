class ScrollableSelectedServiceItemsEntity {
  final String name;
  final double rate;
  final String img;
  bool? activeItem;

  ScrollableSelectedServiceItemsEntity(
      {required this.name, required this.rate, required this.img, this.activeItem = false});
}
