class GiftEntity {
  String applicantFirstName;
  String applicantLastName;
  String applicantEmail;
  String applicantMobile;
  String wishes;

  GiftEntity({
    required this.applicantFirstName,
    required this.applicantLastName,
    required this.applicantEmail,
    required this.applicantMobile,
    required this.wishes,
  });

  Map<String, dynamic> toJson() => {
        "applicantFirstName": applicantFirstName,
        "applicantLastName": applicantLastName,
        "applicantEmail": applicantEmail,
        "applicantMobile": applicantMobile,
        "wishes": wishes
      };
}
