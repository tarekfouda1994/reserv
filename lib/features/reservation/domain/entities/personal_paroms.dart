import 'package:flutter_tdd/features/reservation/data/enums/payment_enum.dart';

class PersonalParams {
  String firstName;
  String lastName;
  String email;
  String phone;
  String cardNumber;
  String cvv;
  String expiryDate;
  PaymentType paymentType;

  PersonalParams(
      {required this.firstName,
      required this.phone,
      required this.lastName,
      required this.expiryDate,
      required this.cardNumber,
      required this.cvv,
      required this.paymentType,
      required this.email});
}
