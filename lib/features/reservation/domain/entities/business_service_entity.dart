import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/features/search/presentation/manager/location_cubit/location_cubit.dart';

import '../../../../core/helpers/di.dart';

class BusinessServicePrams {
  String id;
  bool refresh;
  bool loadImages;

  BusinessServicePrams({
    required this.id,
    required this.loadImages,
    this.refresh = true,
  });

  Map<String, dynamic> toJson() {
    final context = getIt<GlobalContext>().context();
    var lotLong = context.read<HomeLocationCubit>().state.model;
    return {
      "id": id,
      "longitude": lotLong?.lng ?? 0,
      "latitude": lotLong?.lat ?? 0,
      "loadImages": loadImages,
    };
  }
}
