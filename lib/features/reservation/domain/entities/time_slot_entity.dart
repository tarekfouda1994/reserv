class TimeSlotEntity {
  String businessId;
  String serviceId;
  String dateTime;
  String staffId;

  TimeSlotEntity(
      {required this.businessId,
      required this.serviceId,
      required this.staffId,
      required this.dateTime});

  Map<String, dynamic> toJson() => {
        "businessId": businessId,
        "businessServiceId": serviceId,
        "selectedDate": dateTime,
        "staffId": staffId,
      };
}
