
import 'package:flutter_tdd/features/reservation/domain/entities/together_entity.dart';

class AvailableDaysEntity {
  final String businessId;
  final String dateFrom;
  final String dateTo;
  final List<RequestedServiceEntity> requestedServices;

  AvailableDaysEntity({
    required this.businessId,
    required this.dateFrom,
    required this.dateTo,
    required this.requestedServices,
  });

  Map<String, dynamic> toJson() => {
    "businessId": businessId,
    "dateFrom": dateFrom,
    "dateTo": dateTo,
    "requestedServices": requestedServices.map((e) => e.toJson()).toList(),
  };
}


