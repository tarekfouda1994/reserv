class GetPromoParams {
  String businessId;
  List<String> businessServiceIDs;
  int pageNumber;
  int pageSize;
  bool? loadImages;
  bool? refresh;

  GetPromoParams({
    this.loadImages = true,
    this.refresh = true,
    required this.pageSize,
    required this.pageNumber,
    required this.businessId,
    required this.businessServiceIDs,
  });

  Map<String, dynamic> toJson() => {
        "loadImages": loadImages,
        "businessServiceIDs": businessServiceIDs,
        "businessId": businessId,
        "pageNumber": pageNumber,
        "pageSize": pageSize,
      };
}
