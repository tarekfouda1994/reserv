// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_time_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceTimeEntity _$$_ServiceTimeEntityFromJson(Map<String, dynamic> json) =>
    _$_ServiceTimeEntity(
      service: json['service'] as String,
      date: DateTime.parse(json['date'] as String),
      duration: json['duration'] as int,
    );

Map<String, dynamic> _$$_ServiceTimeEntityToJson(
        _$_ServiceTimeEntity instance) =>
    <String, dynamic>{
      'service': instance.service,
      'date': instance.date.toIso8601String(),
      'duration': instance.duration,
    };
