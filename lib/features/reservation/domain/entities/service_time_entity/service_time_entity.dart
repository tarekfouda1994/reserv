import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';

part 'service_time_entity.freezed.dart';
part 'service_time_entity.g.dart';

@freezed
class ServiceTimeEntity with _$ServiceTimeEntity{
  const ServiceTimeEntity._();
  @JsonSerializable(explicitToJson: true)
  const factory ServiceTimeEntity({
    required String service,
    required DateTime date,
    required int duration,
  }) = _ServiceTimeEntity;


  factory ServiceTimeEntity.fromJson(Map<String, dynamic> json) =>
      _$ServiceTimeEntityFromJson(json);

  @override
  String toString({bool addDuration = false}) {
    return DateFormat("HH:mm").format(date.add(Duration(minutes: addDuration ? duration : 0)));
  }
}

// @freezed
// class ServiceTimeEntity extends _$ServiceTimeEntity {
//   const ServiceTimeEntity._();
//   @JsonSerializable(explicitToJson: true)
//   factory ServiceTimeEntity({
//
//   }) = _ServiceTimeEntity;
//
//
//   @override
//   String toString({bool addDuration = false}) {
//     return DateFormat("hh:mm a").format(date.add(Duration(minutes: addDuration ? duration : 0)));
//   }
//
//   factory ServiceTimeEntity.fromJson(Map<String, dynamic> json) => _$ServiceTimeEntityFromJson(json);
//
// }
