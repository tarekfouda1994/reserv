class TogetherEntity {
  final String businessID;
  final String reservationDate;
  final bool isReserveTogether;
  final List<RequestedServiceEntity> requestedServices;

  TogetherEntity({
    required this.businessID,
    required this.reservationDate,
    required this.isReserveTogether,
    required this.requestedServices,
  });

  Map<String, dynamic> toJson() => {
    "businessID": businessID,
    "reservationDate": reservationDate,
    "isReserveTogether": isReserveTogether,
    "requestedServices": requestedServices.map((e) => e.toJson()).toList(),
  };

}

class RequestedServiceEntity {
  final String businessServiceID;
  final String staffID;

  RequestedServiceEntity({required this.businessServiceID, required this.staffID});

  Map<String, dynamic> toJson() => {
        "businessServiceID": businessServiceID,
        "staffID": staffID,
      };
}
