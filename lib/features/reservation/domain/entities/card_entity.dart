class CardEntity {
  String holderName;
  String cardNumber;
  String expiryDate;
  String cvv;
  String? id;
  bool isDefault;

  CardEntity(
      {required this.holderName,
      required this.cardNumber,
      required this.expiryDate,
      required this.cvv,
      required this.isDefault,
      this.id});

  Map<String, dynamic> toJson() => {
        "id": id,
        "holderName": holderName,
        "cardNumber": cardNumber,
        "expiryDate": expiryDate,
        "cvv": cvv,
        "isDefault": isDefault
      };
}
