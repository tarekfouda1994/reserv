class ReservationServiceEntity {
  String businessId;
  String businessServiceName;
  String businessServiceId;
  String appointmentDate;
  String fromTime;
  String toTime;
  String staffID;

  ReservationServiceEntity(
      {required this.businessId,
      required this.businessServiceName,
      required this.businessServiceId,
      required this.appointmentDate,
      required this.fromTime,
      required this.staffID,
      required this.toTime});

  Map<String, dynamic> toJson() => {
        "businessId": businessId,
        "businessServiceName": businessServiceName,
        "businessServiceId": businessServiceId,
        "appointmentDate": appointmentDate,
        "staffID": staffID,
        "fromTime": fromTime,
        "toTime": toTime,
        "appointmentType": 1
      };
}
