import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';

import 'gift_entity.dart';
import 'reserv_service_entity.dart';

class ReservationEntity {
  String? userId;
  String? applicantFirstName;
  String? applicantLastName;
  String? applicantEmail;
  String? applicantMobile;
  String? promoCode;
  List<String>? notes;
  GiftEntity? giftDetail;
  PaymentModel? cardDetails;
  List<ReservationServiceEntity>? appointmentDetails;
  bool isReserveTogether;

  ReservationEntity(
      {this.userId,
      this.applicantFirstName,
      this.applicantLastName,
      this.applicantEmail,
      this.applicantMobile,
      this.promoCode,
      this.giftDetail,
      this.cardDetails,
      this.appointmentDetails,
      required this.isReserveTogether,
      this.notes});

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "notes": notes,
        "applicantFirstName": applicantFirstName,
        "applicantLastName": applicantLastName,
        "applicantEmail": applicantEmail,
        "applicantMobile": applicantMobile,
        "promoCode": promoCode,
        "isReserveTogether": isReserveTogether,
        "giftDetail": giftDetail?.toJson(),
        "cardDetails": cardDetails?.toJson(),
        "appointmentDetails": appointmentDetails?.map((e) => e.toJson()).toList(),
      };
}
