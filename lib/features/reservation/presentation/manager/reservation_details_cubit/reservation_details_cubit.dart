import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_tdd/features/reservation/data/models/reservation_details_without_login_model/reservation_details_without_login_model.dart';

part 'reservation_details_state.dart';

class ReservationDetailsCubit extends Cubit<ReservationDetailsState> {
  ReservationDetailsCubit() : super(const UserInitial());

  onUpdateReservationData(ReservationDetailsWithoutLoginModel? model) {
    emit(ReservationUpdateState(model: model, changed: !state.changed));
  }
}
