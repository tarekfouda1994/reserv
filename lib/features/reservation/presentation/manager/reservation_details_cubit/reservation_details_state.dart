part of 'reservation_details_cubit.dart';

abstract class ReservationDetailsState extends Equatable {
  final ReservationDetailsWithoutLoginModel? model;
  final bool changed;

  const ReservationDetailsState({this.model, required this.changed});
}

class UserInitial extends ReservationDetailsState {
  const UserInitial() : super(changed: false);

  @override
  List<Object> get props => [changed];
}

class ReservationUpdateState extends ReservationDetailsState {
  const ReservationUpdateState(
      {required ReservationDetailsWithoutLoginModel? model, required bool changed})
      : super(model: model, changed: changed);

  @override
  List<Object> get props => [changed];
}
