import 'package:auto_route/auto_route.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_date/reservation_date_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart';



const reservationRoutes = [
  CustomRoute(
    page: ReservationRoot,
    transitionsBuilder: TransitionsBuilders.fadeIn,
    durationInMilliseconds: 800,
  ),
  CustomRoute(page: ReservationDate),
];
