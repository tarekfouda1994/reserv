import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_date_model/service_date_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/available_days_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/best_time_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/time_slot_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/together_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/use_cases/get_reserve_together_data.dart';
import 'package:flutter_tdd/features/reservation/domain/use_cases/get_service_available_days.dart';
import 'package:flutter_tdd/features/reservation/domain/use_cases/get_time_slots.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_date/widgets/reservation_date_widgets_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart';
import 'package:intl/intl.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../core/bloc/device_cubit/device_cubit.dart';
import '../../../../../core/helpers/custom_toast.dart';
import '../../../../../core/localization/localization_methods.dart';
import '../../../domain/entities/select_service_time_entity.dart';

part 'reservation_date.dart';
part 'reservation_date_data.dart';
