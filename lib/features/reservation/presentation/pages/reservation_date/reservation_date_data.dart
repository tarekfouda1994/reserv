part of 'reservation_date_imports.dart';

class ReservationDateData {
  final GenericBloc<List<ServiceDateModel>> availableDatesCubit =
      GenericBloc([]);
  final GenericBloc<List<DateTime>> dateCubit = GenericBloc([]);
  final GenericBloc<bool> availableDateCubit = GenericBloc(true);
  final GenericBloc<BestTimeEntity> bestTimesCubit =
      GenericBloc(BestTimeEntity(index: -1, times: []));
  final GenericBloc<bool> switchedCubit = GenericBloc(true);
  final GenericBloc<List<SelectServiceTimeEntity>> selectedServiceCubit =
      GenericBloc([]);
  final GenericBloc<List<String>> timesCubit = GenericBloc([]);
  final GenericBloc<String> theSelectedTimeCubit = GenericBloc("");

  late String businessId;

  addSelectedService(BuildContext context, String id, String time, int index,
      ServiceListModel serviceListModel, ReservationRootData rootData) {
    var existItem =
        selectedServiceCubit.state.data.firstWhereOrNull((e) => e.id == id);
    if (existItem != null) {
      selectedServiceCubit.state.data.remove(existItem);
    }
    _updateItemTimes(id, time, serviceListModel, rootData);
    if (rootData.servicesData.servicesBloc.state.data.length == 1) {
      nextPage(rootData);
    } else {
      AutoRouter.of(context).pop();
    }
  }

  void _updateItemTimes(String id, String time,
      ServiceListModel serviceListModel, ReservationRootData rootData) {
    selectedServiceCubit.state.data
        .add(SelectServiceTimeEntity(id: id, time: time));
    selectedServiceCubit.onUpdateData(selectedServiceCubit.state.data);
    serviceListModel.selectedTimeForService = time;
    rootData.servicesData.servicesBloc
        .onUpdateData(rootData.servicesData.servicesBloc.state.data);
  }

  fetchServicesAvailableDays(ReservationRootData rootData) async {
    // getIt<LoadingHelper>().showLoadingDialog();
    // availableDatesCubit.onUpdateToInitState([]);
    timesCubit.onUpdateToInitState([]);
    var services = rootData.servicesData.servicesBloc.state.data;
    var requestedServices = services.map((e) {
      return RequestedServiceEntity(
        businessServiceID: e.businessServiceID,
        staffID: e.staffId == "0" ? "" : e.staffId,
      );
    }).toList();
    if (requestedServices.isNotEmpty) {
      var result = await fetchAvailableDays(requestedServices);
      availableDatesCubit.onUpdateData(result);
      if (dateCubit.state.data.isEmpty) {
        _selectFirstAvailableDate(result);
      }
      if (services.length == 1) {
        _getOnlyServiceTime(
            rootData, dateCubit.state.data.firstOrNull ?? DateTime.now());
      }
    }
    // getIt<LoadingHelper>().dismissDialog();
  }

  void _selectFirstAvailableDate(List<ServiceDateModel> data) {
    if (selectedServiceCubit.state.data.isEmpty) {
      var firstAvailableDate =
          data.firstWhereOrNull((element) => element.isServiceAvailable);
      if (firstAvailableDate != null) {
        dateCubit.onUpdateData([firstAvailableDate.date]);
      }
    }
  }

  fetchAvailableDays(List<RequestedServiceEntity> requestedServices) async {
    final date = DateTime.now();
    AvailableDaysEntity params =
        _serviceDateEntityParams(requestedServices, date);
    var data = await GetServiceAvailableDays()(params);
    return data;
  }

  AvailableDaysEntity _serviceDateEntityParams(
      List<RequestedServiceEntity> requestedServices, DateTime date) {
    final params = AvailableDaysEntity(
      businessId: businessId,
      requestedServices: requestedServices,
      dateFrom: getIt<Utilities>().formatDate(date),
      dateTo: getIt<Utilities>().formatDate(date.add(Duration(days: 60))),
    );
    return params;
  }

  Future<List<String>> fetchTimeSlots(
      String serviceId, String staffId, DateTime dateTime) async {
    final params = TimeSlotEntity(
      businessId: businessId,
      serviceId: serviceId,
      staffId: staffId == "0" ? "" : staffId,
      dateTime: DateFormat("yyyy-MM-dd").format(dateTime),
    );
    var data = await GetTimeSlots()(params);
    return data;
  }

  onChangeReserveTogether(ReservationRootData rootData, bool value,
      List<ServiceListModel> services) async {
    if (services.length >= 2) {
      switchedCubit.onUpdateData(value);
      if (value) {
        _doReserveTogether(services);
      } else {
        _changeToNotTogether(rootData);
      }
    }
  }

  void _changeToNotTogether(ReservationRootData rootData) {
    selectedServiceCubit.onUpdateData([]);
    bestTimesCubit.onUpdateToInitState(BestTimeEntity(index: -1, times: []));
    rootData.servicesData.servicesBloc.state.data
        .map((e) => e.selectedTimeForService = '-')
        .toList();
    rootData.servicesData.servicesBloc
        .onUpdateData(rootData.servicesData.servicesBloc.state.data);
    CustomToast.showSimpleToast(
        msg: tr("select_individually"),
        title: tr("Reservation_Slots"),
        type: ToastType.info);
  }

  void _doReserveTogether(List<ServiceListModel> services) {
    applyTogetherFunc(services);
    selectedServiceCubit.onUpdateData([]);
    bestTimesCubit.onUpdateToInitState(BestTimeEntity(index: -1, times: []));
    CustomToast.showSimpleToast(
      msg: tr("select_together"),
      title: tr("Reservation_Slots"),
      type: ToastType.info,
    );
  }

  Future<void> applyTogetherFunc(List<ServiceListModel> services) async {
    if (services.length == 1) {
      switchedCubit.onUpdateData(false);
      return;
    }
    var requestServices = services.map((e) {
      return RequestedServiceEntity(
        businessServiceID: e.businessServiceID,
        staffID: e.staffId == "0" ? "" : e.staffId,
      );
    }).toList();
    if (requestServices.isNotEmpty) {
      bestTimesCubit.onUpdateToInitState(BestTimeEntity(index: -1, times: []));
      DateTime dateTime = dateCubit.state.data.firstOrNull ?? DateTime.now();
      var params = TogetherEntity(
        businessID: businessId,
        isReserveTogether: true,
        reservationDate: DateFormat("yyyy-MM-dd").format(dateTime),
        requestedServices: requestServices,
      );

      var result = await GetReserveTogetherData()(params);
      if (result != null) {
        bestTimesCubit.state.data.times
            .addAll(result.servicesWithStaff.first.timeSlots);
        bestTimesCubit.onUpdateData(bestTimesCubit.state.data);
      }
    }
  }

  nextPage(ReservationRootData rootData) {
    if (_checkTimesLength(rootData)) rootData.goNextPage();
  }

  bool _checkTimesLength(ReservationRootData rootData) {
    if (switchedCubit.state.data && bestTimesCubit.state.data.index >= 0) {
      return true;
    }
    if (selectedServiceCubit.state.data.length !=
        rootData.servicesData.servicesBloc.state.data.length) {
      CustomToast.showSimpleToast(
          msg: tr("please_select_appointment_time"),
          title: tr("missing_appointment_time"));
      return false;
    } else {
      return true;
    }
  }

  bottomSheetSelectTime(BuildContext context, ServiceListModel serviceListModel,
      ReservationRootData rootData, DateTime? dateTime) {
    showModalBottomSheet(
      elevation: 10,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      isScrollControlled: false,
      builder: (cxt) {
        return BuildSelectTimeSheet(
          rootData: rootData,
          serviceListModel: serviceListModel,
          dateTime: dateTime,
        );
      },
    );
  }

  selectAvailableTimeItem(ReservationRootData rootData, index) {
    bestTimesCubit.state.data.index = index;
    bestTimesCubit.onUpdateData(bestTimesCubit.state.data);
    nextPage(rootData);
  }

  onSelectDate(bool disabled, DateTime dateTime,
      List<ServiceListModel> services, ReservationRootData rootData) async {
    dateCubit.onUpdateData([dateTime]);
    if (disabled) {
      availableDateCubit.onUpdateData(false);
    } else {
      availableDateCubit.onUpdateData(true);
      selectedServiceCubit.onUpdateData([]);
      if (switchedCubit.state.data) {
        applyTogetherFunc(services);
      }
      _getOnlyServiceTime(rootData, dateTime);
    }
    _removeOldServicesTimes(services, rootData);
  }

  Future<void> _getOnlyServiceTime(
      ReservationRootData rootData, DateTime dateTime) async {
    if (rootData.servicesData.servicesBloc.state.data.length == 1) {
      timesCubit.onUpdateToInitState([]);
      var serviceData = rootData.servicesData.servicesBloc.state.data.first;
      var data = await fetchTimeSlots(
          serviceData.businessServiceID, serviceData.staffId, dateTime);
      timesCubit.onUpdateData(data);
      theSelectedTimeCubit.onUpdateData("");
    }
  }

  _removeOldServicesTimes(
      List<ServiceListModel> services, ReservationRootData rootData) {
    if (!switchedCubit.state.data) {
      services.map((e) => e.selectedTimeForService = "-").toList();
      rootData.servicesData.servicesBloc
          .onUpdateData(rootData.servicesData.servicesBloc.state.data);
    }
  }
}
