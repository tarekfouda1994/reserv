part of 'reservation_date_widgets_imports.dart';

class BuildSelectTimeSheet extends StatefulWidget {
  final ServiceListModel serviceListModel;
  final ReservationRootData rootData;
  final DateTime? dateTime;

  const BuildSelectTimeSheet(
      {Key? key,
      required this.serviceListModel,
      required this.rootData,
      this.dateTime})
      : super(key: key);

  @override
  State<BuildSelectTimeSheet> createState() => _BuildSelectTimeSheetState();
}

class _BuildSelectTimeSheetState extends State<BuildSelectTimeSheet> {
  final GenericBloc<List<String>> timesCubit = GenericBloc([]);
  late ReservationDateData dateController;

  @override
  void initState() {
    dateController = widget.rootData.dateData;
    _fetchServiceTime();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: EdgeInsets.fromLTRB(16, 16, 16, 0),
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BuildHeaderBottomSheet(
            title: tr("Select_Service_Time"),
          ),
          MyText(
            title: widget.serviceListModel.getServiceName(),
            color: MyColors.grey,
            fontWeight: FontWeight.bold,
            size: device.isTablet ? 6.sp : 10.sp,
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            height: 250.h,
            child: BlocBuilder<GenericBloc<List<String>>,
                GenericState<List<String>>>(
              bloc: timesCubit,
              builder: (context, state) {
                if (state is GenericUpdateState) {
                  return Visibility(
                    visible: state.data.isNotEmpty,
                    replacement: Center(
                      child: MyText(
                        title: tr("noTimesAvailable"),
                        color: MyColors.black,
                        size: 12,
                      ),
                    ),
                    child: ListView.separated(
                      itemCount: state.data.length,
                      itemBuilder: (cxt, index) {
                        return InkWell(
                          onTap: () => dateController.addSelectedService(
                              context,
                              widget.serviceListModel.businessServiceID,
                              state.data[index],
                              index,
                              widget.serviceListModel,
                              widget.rootData),
                          child: Container(
                            height: 45.h,
                            padding: EdgeInsets.symmetric(horizontal: 14).r,
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Expanded(
                                  child: MyText(
                                    color: MyColors.black,
                                    title: getIt<Utilities>().convertNumToAr(
                                        context: context, value: state.data[index]),
                                    size: device.isTablet ? 7.sp : 10.sp,
                                    fontFamily: CustomFonts.primarySemiBoldFont,
                                  ),
                                ),
                                SvgPicture.asset(
                                  device.locale == Locale('en', 'US')
                                      ? Res.arrowForward
                                      : Res.arrow,
                                  height: device.locale == Locale('en', 'US')
                                      ? null
                                      : device.isTablet
                                          ? 9.sp
                                          : 15.sp,
                                  color: MyColors.white,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(height: 0);
                      },
                    ),
                  );
                }
                return ListView.builder(
                  itemCount: 5,
                  itemBuilder: (cxt, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: BuildShimmerView(
                        height: 50.h,
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _fetchServiceTime() async {
    final String servId = widget.serviceListModel.businessServiceID;
    final String staffId = widget.serviceListModel.staffId;
    final DateTime dateTime = widget.dateTime ?? DateTime.now();
    timesCubit.onUpdateToInitState([]);
    // _setBufferTimeToServiceDuration();
    await dateController
        .fetchTimeSlots(servId, staffId, dateTime)
        .then((value) {
      value.removeWhere((e) => _timesConditions(e, servId));
      timesCubit.onUpdateData(value.toSet().toList());
    });
  }

  bool _timesConditions(String e, String servId) {
    final selectedTimes = dateController.selectedServiceCubit.state.data;
    for (final time in selectedTimes) {
      bool validation = _rangeTimeCheck(time, e);
      if (validation) {
        if (time.id == servId) {
          return false;
        } else {
          return true;
        }
      }
    }
    return false;
  }

  bool _rangeTimeCheck(SelectServiceTimeEntity time, String e) {
    final selectedTime = _getEachSerDuration(time);
    final diffMinutes = inDifferenceMinutes(time, e);
    final int serviceDuration = widget.serviceListModel.serviceDuration;
    bool validation =
        !(diffMinutes >= serviceDuration || diffMinutes <= (0 - selectedTime!));
    return validation;
  }

  int? _getEachSerDuration(SelectServiceTimeEntity time) {
    final servCubit = widget.rootData.servicesData.servicesBloc.state.data;
    final currentService = servCubit.firstWhereOrNull(
      (element) => element.businessServiceID == time.id,
    );
    return currentService?.serviceDuration;
  }

  int inDifferenceMinutes(SelectServiceTimeEntity time, String e) {
    final timeDifference = convertStringSlot(time.time).difference(
      convertStringSlot(e),
    );
    return timeDifference.inMinutes;
  }

  DateTime convertStringSlot(String time) {
    final timeList = time.split(":");
    return DateTime(
      2000,
      1,
      1,
      int.parse(timeList.first),
      int.parse(timeList.last),
    );
  }

// void _setBufferTimeToServiceDuration() {
//   final servCubit = widget.rootData.servicesData.servicesBloc.state.data;
//   final serv = widget.serviceListModel;
//   for (var item in servCubit) {
//     if (item.staffId == serv.staffId &&
//         item.businessServiceID != serv.businessServiceID) {
//       int servDuration = item.serviceDuration;
//       servDuration = servDuration + item.getStaffBufferTime;
//     }
//   }
//   widget.rootData.servicesData.servicesBloc.onUpdateData(servCubit);
// }
}
