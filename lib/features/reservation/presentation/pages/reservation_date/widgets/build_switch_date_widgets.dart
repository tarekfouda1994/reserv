part of 'reservation_date_widgets_imports.dart';

class BuildSwitchDateWidgets extends StatelessWidget {
  final ReservationRootData rootData;
  final DateTime? data;

  const BuildSwitchDateWidgets(
      {Key? key, required this.rootData, required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
      bloc: rootData.dateData.switchedCubit,
      builder: (_, typeState) {
        return AnimatedSwitcher(
          duration: Duration(milliseconds: 500),
          reverseDuration: Duration(milliseconds: 500),
          child: typeState.data
              ? BuildReserveTogetherView(
                  rootData: rootData,
                  model: device,
                )
              : BuildNotReserveTogetherView(
                  rootData: rootData,
                  dateTime: data,
                ),
        );
      },
    );
  }
}
