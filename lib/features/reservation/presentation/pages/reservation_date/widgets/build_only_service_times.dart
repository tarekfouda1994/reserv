part of 'reservation_date_widgets_imports.dart';

class BuildOnlyServiceTimes extends StatelessWidget {
  final ServiceListModel serviceListModel;
  final ReservationRootData rootData;
  final DateTime? dateTime;
  final bool showTimes;

  const BuildOnlyServiceTimes(
      {Key? key,
      required this.serviceListModel,
      required this.rootData,
      this.dateTime,
      required this.showTimes})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      margin: EdgeInsets.only(bottom: 20, left: 15, right: 15),
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(13.r),
        border: Border.all(width: .1, color: MyColors.grey.withOpacity(.5)),
      ),
      // height: 230.h,
      child: BlocBuilder<GenericBloc<List<String>>, GenericState<List<String>>>(
        bloc: rootData.dateData.timesCubit,
        builder: (context, state) {
          if (state is GenericUpdateState) {
            if (state.data.isEmpty) {
              return BuildEmptyIndividualTimes(rootData: rootData);
            }
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12).r,
                  color: MyColors.white),
              child: ListView.separated(
                shrinkWrap: true,
                itemCount: state.data.length,
                physics: NeverScrollableScrollPhysics(),
                padding: EdgeInsets.zero,
                itemBuilder: (BuildContext context, int index) {
                  return BlocBuilder<GenericBloc<String>, GenericState<String>>(
                      bloc: rootData.dateData.theSelectedTimeCubit,
                      builder: (context, theSelectedState) {
                        return InkWell(
                          onTap: () => _onSelectTime(context, state, index),
                          child: Container(
                            height: 45.h,
                            color: theSelectedState.data == state.data[index]
                                ? MyColors.primary
                                : null,
                            padding: EdgeInsets.symmetric(horizontal: 10).r,
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Expanded(
                                  child: MyText(
                                    color: theSelectedState.data ==
                                            state.data[index]
                                        ? MyColors.white
                                        : MyColors.black,
                                    title: getIt<Utilities>().convertNumToAr(
                                      context: context,
                                      value: state.data[index].toString(),
                                    ),
                                    size: device.isTablet ? 7.sp : 10.sp,
                                    fontFamily: CustomFonts.primarySemiBoldFont,
                                  ),
                                ),
                                SvgPicture.asset(
                                  device.locale == Locale('en', 'US')
                                      ? Res.arrowForward
                                      : Res.arrow,
                                  height: device.locale == Locale('en', 'US')
                                      ? null
                                      : device.isTablet
                                          ? 9.sp
                                          : 15.sp,
                                  color:
                                      theSelectedState.data == state.data[index]
                                          ? MyColors.white
                                          : MyColors.black,
                                ),
                              ],
                            ),
                          ),
                        );
                      });
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    height: 1,
                  );
                },
              ),
            );
          } else {
            return ListView.builder(
              itemCount: 5,
              shrinkWrap: true,
              itemBuilder: (cxt, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: BuildShimmerView(
                    height: 50.h,
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }

  void _onSelectTime(
      BuildContext context, GenericState<List<String>> state, int index) {
    rootData.dateData.addSelectedService(
        context,
        serviceListModel.businessServiceID,
        state.data[index],
        index,
        serviceListModel,
        rootData);
    rootData.dateData.theSelectedTimeCubit.onUpdateData(state.data[index]);
  }
}
