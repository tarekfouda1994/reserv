part of 'reservation_date_widgets_imports.dart';

class BuildEmptyIndividualTimes extends StatelessWidget {
  final ReservationRootData rootData;

  const BuildEmptyIndividualTimes({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: MyColors.primary, width: 2),
        borderRadius: BorderRadius.circular(12).r,
        color: MyColors.headerBackGroundColor,
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20).r,
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(15).r,
              ),
            ),
            child: Row(
              children: [
                SvgPicture.asset(
                  Res.calendar_note,
                  height: device.isTablet ? 30.h : 40.h,
                  width: device.isTablet ? 30.w : 40.w,
                  fit: BoxFit.fill,
                ),
                SizedBox(width: 8.sm),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          SvgPicture.asset(Res.warning),
                          SizedBox(width: 8.sm),
                          MyText(
                            title: tr("only_reservation_unavailable"),
                            color: MyColors.infoColor,
                            size: device.isTablet ? 7.sp : 10.sp,
                          )
                        ],
                      ),
                      SizedBox(height: 8.sm),
                      MyText(
                        title: tr("select_these_service"),
                        color: MyColors.blackOpacity,
                        size: device.isTablet ? 6.sp : 8.sp,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15).r,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(
                  title: tr("Choose_different_date"),
                  color: MyColors.black,
                  size: device.isTablet ? 7.sp : 10.sp,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
