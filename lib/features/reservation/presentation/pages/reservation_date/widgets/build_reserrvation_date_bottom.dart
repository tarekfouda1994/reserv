part of 'reservation_date_widgets_imports.dart';

class BuildReservationDateBottom extends StatelessWidget {
  final ReservationRootData rootData;
  final DeviceModel model;

  const BuildReservationDateBottom(
      {Key? key, required this.rootData, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.vertical(top: Radius.circular(10).r),
        boxShadow: [
          BoxShadow(color: MyColors.greyWhite, spreadRadius: 5, blurRadius: 5)
        ],
      ),
      padding:
          EdgeInsetsDirectional.only(bottom: 10, start: 25, top: 10, end: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BuildCancelReservationButton(rootData: rootData),
          InkWell(
            onTap: () => rootData.dateData.nextPage(rootData),
            child: Container(
              height: 40.h,
              decoration: BoxDecoration(
                color: MyColors.primary,
                borderRadius: BorderRadius.circular(40.r),
              ),
              padding:
                  const EdgeInsets.symmetric(horizontal: 20, vertical: 10).r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  MyText(
                    title: tr("proceed"),
                    color: MyColors.white,
                    size: model.isTablet ? 7.sp : 13.sp,
                  ),
                  SizedBox(width: 5.sp),
                  BuildMyArrowIcon(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
