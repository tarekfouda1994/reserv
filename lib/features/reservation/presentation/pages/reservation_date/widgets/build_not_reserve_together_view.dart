part of 'reservation_date_widgets_imports.dart';

class BuildNotReserveTogetherView extends StatelessWidget {
  final ReservationRootData rootData;
  final DateTime? dateTime;

  const BuildNotReserveTogetherView({
    Key? key,
    required this.rootData,
    required this.dateTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<String>>, GenericState<List<String>>>(
        bloc: rootData.dateData.timesCubit,
        builder: (context, stateTimes) {
          return BlocBuilder<GenericBloc<List<ServiceListModel>>,
              GenericState<List<ServiceListModel>>>(
            bloc: rootData.servicesData.servicesBloc,
            builder: (context, state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20.h),
                  Row(
                    children: [
                      BuildAvailableTitle(),
                    ],
                  ),
                  if (state.data.length != 1)
                    CustomCard(
                      padding: EdgeInsets.symmetric(vertical: 10.h),
                      border: Border.all(color: MyColors.primary, width: 1.6),
                      child: Column(
                        children: List.generate(state.data.length, (index) {
                          return BuildServiceTimeItem(
                              index: index,
                              length: state.data.length,
                              serviceListModel: state.data[index],
                              dateTime: dateTime,
                              rootData: rootData);
                        }),
                      ),
                    ),
                  if (state.data.length == 1)
                    BuildOnlyServiceTimes(
                        serviceListModel: state.data.first,
                        showTimes: stateTimes.data.isNotEmpty,
                        rootData: rootData,
                        dateTime: dateTime,
                    ),
                ],
              );
            },
          );
        },
    );
  }
}
