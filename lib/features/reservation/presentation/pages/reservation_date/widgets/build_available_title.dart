part of 'reservation_date_widgets_imports.dart';

class BuildAvailableTitle extends StatelessWidget {
  const BuildAvailableTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: MyText(
        title: tr("Select_Service_Time"),
        color: MyColors.black,
        size: device.isTablet ? 7.sp : 11.sp,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
