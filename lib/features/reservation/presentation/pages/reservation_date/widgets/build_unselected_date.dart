part of "reservation_date_widgets_imports.dart";

class BuildUnselectedDate extends StatelessWidget {
  final ReservationDateData reservationDateData;

  const BuildUnselectedDate({Key? key, required this.reservationDateData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Row(
          children: [
            Spacer(),
            InkWell(
              borderRadius: BorderRadius.circular(50),
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                    padding: const EdgeInsets.all(3),
                    decoration: BoxDecoration(color: MyColors.primary, shape: BoxShape.circle),
                    child: Icon(Icons.clear, size: 10.sp, color: MyColors.white),
                  ),
                  MyText(title: tr("unselected_all_dates"), color: MyColors.grey, size: 10.sp),
                ],
              ),
            ),
          ],
        ));
  }
}
