part of 'reservation_date_widgets_imports.dart';

class BuildUnavailableDate extends StatelessWidget {
  final ReservationRootData rootData;

  const BuildUnavailableDate({Key? key, required this.rootData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 25.sp),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10).r,
          child: MyText(
            title: tr("available_times"),
            color: MyColors.black,
            size: device.isTablet ? 7.sp : 11.sp,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 15).r,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15).r,
            color: MyColors.white,
          ),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10,vertical: 20).r,
                decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(15).r,
                  ),
                ),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      Res.calendar_note,
                      height: device.isTablet ? 30.h : 40.h,
                      width: device.isTablet ? 30.w : 40.w,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(width: 8.sm),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SvgPicture.asset(Res.unavailable),
                              SizedBox(width: 8.sm),
                              MyText(
                                title: tr("Unavailable_Date"),
                                color: MyColors.errorColor,
                                size: device.isTablet ? 7.sp : 10.sp,
                              )
                            ],
                          ),
                          SizedBox(height: 8.sm),
                          MyText(
                            title: tr("Choose_different_date"),
                            color: MyColors.blackOpacity,
                            size: device.isTablet ? 6.sp : 8.sp,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Divider(height: 1),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20).r,
                child: MyText(
                  title: tr("Choose_different_date"),
                  color: MyColors.black,
                  size: device.isTablet ? 7.sp : 10.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
