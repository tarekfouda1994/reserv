part of 'reservation_date_imports.dart';

class ReservationDate extends StatefulWidget {
  final ReservationRootData rootData;

  const ReservationDate({Key? key, required this.rootData}) : super(key: key);

  @override
  State<ReservationDate> createState() => _ReservationDateState();
}

class _ReservationDateState extends State<ReservationDate> {
  late ReservationRootData rootData;
  late ReservationDateData dateData;

  @override
  void initState() {
    dateData = widget.rootData.dateData;
    rootData = widget.rootData;
    dateData.fetchServicesAvailableDays(rootData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        Flexible(
          child: BlocBuilder<GenericBloc<List<DateTime>>, GenericState<List<DateTime>>>(
            bloc: dateData.dateCubit,
            builder: (context, state) {
              return ListView(
                padding: EdgeInsets.zero,
                children: [
                  BuildReservationHeader(
                    rootData: rootData,
                    model: device,
                  ),
                  BuildSelectDate(selectedDates: state.data, rootData: rootData),
                  BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
                    bloc: rootData.dateData.availableDateCubit,
                    builder: (_, statusState) {
                      if (!statusState.data) {
                        return BuildUnavailableDate(
                          rootData: rootData,
                        );
                      }
                      return BuildSwitchDateWidgets(
                        data: state.data.firstOrNull,
                        rootData: rootData,
                      );
                    },
                  ),
                ],
              );
            },
          ),
        ),
        BuildReservationDateBottom(
          rootData: rootData,
          model: device,
        )
      ],
    );
  }
}
