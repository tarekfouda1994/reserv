part of 'select_services_widgets_imports.dart';

class BuildAllItemsBottomSheet extends StatelessWidget {
  final SelectServicesData servicesData;
  final DeviceModel deviceModel;

  const BuildAllItemsBottomSheet(
      {Key? key, required this.servicesData, required this.deviceModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: servicesData.servicesBloc,
        builder: (context, state) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              height: state.data.length > 4
                  ? MediaQuery.of(context).size.height * .9
                  : (state.data.length * 120.h) + 140.h,
              padding: EdgeInsets.symmetric(vertical: 16.r),
              decoration: BoxDecoration(
                  color: MyColors.white,
                  borderRadius:
                      BorderRadius.vertical(top: Radius.circular(10.r))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BuildHeaderBottomSheet(
                    margin: EdgeInsets.symmetric(horizontal: 15.h),
                    title: tr("selected_services"),
                  ),
                  const SizedBox(height: 15),
                  Flexible(
                    child: ListView.separated(
                      itemBuilder: (BuildContext context, int index) {
                        return BuildSelectedItem(
                          deviceModel: deviceModel,
                          serviceListModel: state.data[index],
                          servicesData: servicesData,
                        );
                      },
                      itemCount: state.data.length,
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(
                          height: 0,
                          thickness: .5,
                          color: MyColors.grey.withOpacity(.8),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () => AutoRouter.of(context).pop(),
                            child: Container(
                              alignment: Alignment.center,
                              height: deviceModel.isTablet ? 60.sm : 50.sm,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50.r),
                                color: MyColors.black,
                              ),
                              child: MyText(
                                  alien: TextAlign.center,
                                  size: deviceModel.isTablet ? 7.sp : 11.sp,
                                  title: tr("keep"),
                                  color: MyColors.white),
                            ),
                          ),
                        ),
                        SizedBox(width: 8),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              servicesData.servicesBloc.onUpdateData([]);
                              servicesData.companyServicesBloc.onUpdateData(
                                  servicesData.companyServicesBloc.state.data);
                              AutoRouter.of(context).pop();
                            },
                            child: Container(
                              height: deviceModel.isTablet ? 60.sm : 50.sm,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(top: 10, bottom: 5.sm),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                color: Colors.red.withOpacity(.2),
                              ),
                              child: MyText(
                                  alien: TextAlign.center,
                                  size: deviceModel.isTablet ? 8.sp : 12.sp,

                                  title: tr("delete"),
                                  color: MyColors.errorColor),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
