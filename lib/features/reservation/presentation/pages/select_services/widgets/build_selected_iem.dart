part of 'select_services_widgets_imports.dart';

class BuildSelectedItem extends StatelessWidget {
  final SelectServicesData servicesData;
  final DeviceModel deviceModel;
  final ServiceListModel serviceListModel;

  const BuildSelectedItem({
    Key? key,
    required this.servicesData,
    required this.deviceModel,
    required this.serviceListModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.h),
          color: MyColors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
            child: Row(
              children: [
                if (serviceListModel.picture.isNotEmpty)
                  CachedImage(
                    url: serviceListModel.picture.first.replaceAll("\\", "/"),
                    width: deviceModel.isTablet ? 35.w : 50.w,
                    height: deviceModel.isTablet ? 35.w : 50.w,
                    borderRadius: BorderRadius.circular(12.r),
                    fit: BoxFit.cover,
                    bgColor: MyColors.defaultImgBg,
                    placeHolder: ServicePlaceholder(),
                  ),
                SizedBox(width: 10.w),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: serviceListModel.getServiceName(),
                        color: MyColors.primary,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                      ),
                      SizedBox(height: 8.h),
                      Row(
                        children: [
                          MyText(
                            title:
                                "${serviceListModel.getCurrencyName()} ${serviceListModel.price}",
                            color: MyColors.black,
                            fontFamily: CustomFonts.primarySemiBoldFont,
                            size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.access_time,
                                color: MyColors.grey,
                                size: deviceModel.isTablet ? 12.sp : 15.sp,
                              ),
                              MyText(
                                  title: " " +
                                      serviceListModel.serviceDurationText,
                                  color: MyColors.grey,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap:  () {
                    servicesData.onSelectService(serviceListModel);
                    if (servicesData.servicesBloc.state.data.isEmpty) AutoRouter.of(context).pop();
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.r),
                    child: SvgPicture.asset(Res.trash,
                        color: MyColors.black,
                        height: 17.h,
                        width: 17.h),
                  ),
                ),
              ],
            ),
          ),
        ),
        Visibility(
          visible: serviceListModel.staffDetail.length != 0,
          child: Container(
            height: deviceModel.isTablet ? 65.h : 70,
            padding: const EdgeInsets.only(bottom: 5),
            decoration: BoxDecoration(
              color: MyColors.primary.withOpacity(.04),
            ),
            child: BuildScrollableBodyOfItems(
              servicesBloc: servicesData.servicesBloc,
              removeStaff:()=> servicesData.removeStaff(serviceListModel),
              addStaff: (m){},
              model: deviceModel,
              serviceListModel: serviceListModel,
            ),
          ),
        ),
      ],
    );
  }
}
