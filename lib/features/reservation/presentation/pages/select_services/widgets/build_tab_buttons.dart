part of 'select_services_widgets_imports.dart';

class BuildTabButtons extends StatelessWidget {
  final int index;
  final SelectServicesData servicesData;
  final ServiceTypesModel model;

  const BuildTabButtons(
      {Key? key,
      required this.servicesData,
      required this.index,
      required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return GestureDetector(
      onTap: () => servicesData.scrollToServiceType(model.key, index),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18.w),
        margin: EdgeInsets.symmetric(vertical: 4, horizontal: 3.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40.r),
          color: model.selected
              ? MyColors.primary
              : MyColors.headerBackGroundColor,
        ),
        alignment: Alignment.center,
        child: MyText(
          alien: TextAlign.center,
          size: isTablet ? 6.sp : 9.sp,
          title: model.getServiceTypeName(),
          color: model.selected
              ? MyColors.headerBackGroundColor
              : MyColors.primary,
        ),
      ),
    );
  }
}
