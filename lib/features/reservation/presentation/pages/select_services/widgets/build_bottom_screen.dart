part of 'select_services_widgets_imports.dart';

class BuildBottomScreen extends StatelessWidget {
  final ReservationRootData rootData;
  final SelectServicesData servicesData;
  final DeviceModel model;

  const BuildBottomScreen(
      {Key? key,
      required this.rootData,
      required this.servicesData,
      required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
        GenericState<List<ServiceListModel>>>(
      bloc: servicesData.servicesBloc,
      builder: (context, state) {
        return Visibility(
          visible: state.data.isNotEmpty &&
              state.data.any((element) => element.staffDetail.isNotEmpty),
          child: Container(
            padding: EdgeInsetsDirectional.only(start: 10, end: 5),
            decoration: BoxDecoration(color: MyColors.white, boxShadow: [
              BoxShadow(
                  color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
            ]),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                MyText(
                                  title: getIt<Utilities>().convertNumToAr(
                                   context:  context,
                                    value: "${state.data.lastOrNull?.getCurrencyName() ?? ""} ${state.data.fold(0, (num prev, e) => prev + e.price)} ",
                                  ),
                                  color: MyColors.black,
                                  fontWeight: FontWeight.bold,
                                  size: model.isTablet ? 8.sp : 12.sp,
                                ),
                                Expanded(
                                  child: MyText(
                                    title: tr("including_vat"),
                                    color: MyColors.grey,
                                    size: model.isTablet ? 5.sp : 8.5.sp,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 5.h),
                            InkWell(
                              onTap: () {
                                if (state.data.length != 0)
                                  servicesData.allServicesBottomSheet(
                                      context, servicesData);
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    height: 22.h,
                                    width: 22.w,
                                    alignment: Alignment.center,
                                    margin: const EdgeInsetsDirectional.only(
                                        end: 10),
                                    decoration: BoxDecoration(
                                        border:
                                            Border.all(color: MyColors.primary),
                                        color: MyColors.primary,
                                        shape: BoxShape.circle),
                                    child: FittedBox(
                                      child: AnimatedSwitcher(
                                        duration: Duration(milliseconds: 300),
                                        transitionBuilder: (Widget child,
                                            Animation<double> animation) {
                                          return ScaleTransition(
                                              scale: animation, child: child);
                                        },
                                        child: Text(
                                          "${state.data.length}",
                                          key: ValueKey(state.data.length),
                                          style: TextStyle(
                                              color: MyColors.white,
                                              fontSize: model.isTablet
                                                  ? 8.sp
                                                  : 10.sp),
                                        ),
                                      ),
                                    ),
                                  ),
                                  MyText(
                                    title: tr("View_items"),
                                    color: MyColors.primary,
                                    size: model.isTablet ? 5.5.sp : 9.sp,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () => servicesData.nextPage(rootData),
                          child: Container(
                            height: 40.h,
                            decoration: BoxDecoration(
                              color: MyColors.primary,
                              borderRadius: BorderRadius.circular(40.r),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.w, vertical: 10.h),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  MyText(
                                      title: tr("proceed"),
                                      color: MyColors.white,
                                      size: model.isTablet ? 7.sp : 11.sp),
                                  SizedBox(width: 5),
                                  BuildMyArrowIcon(),
                                ]),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
