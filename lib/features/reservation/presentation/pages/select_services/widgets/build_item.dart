part of 'select_services_widgets_imports.dart';

class BuildItem extends StatelessWidget {
  final Color? dividerColor;
  final SelectServicesData servicesData;
  final DeviceModel deviceModel;
  final int index;
  final int length;
  final String id;
  final ServiceListModel serviceListModel;
  final List<ServiceListModel> list;
  final Function() updateWish;

  const BuildItem({
    Key? key,
    required this.servicesData,
    this.dividerColor,
    required this.deviceModel,
    required this.serviceListModel,
    required this.index,
    required this.length,
    required this.id,
    required this.updateWish,
    required this.list,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => servicesData.DetailsBottomSheet(
          context, servicesData, serviceListModel, updateWish),
      child: Container(
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.circular(13.r),
          border: Border.all(
            color: selected() ? MyColors.primary : MyColors.white,
            width: 1.5,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 13.w, vertical: 12.h),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: serviceListModel.getServiceName(),
                        color: selected() ? MyColors.primary : MyColors.black,
                        fontFamily: CustomFonts.primarySemiBoldFont,
                        size: deviceModel.isTablet ? 7.sp : 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(height: 8.h),
                      Container(
                        width: MediaQuery.of(context).size.width * .65,
                        child: Row(
                          children: [
                            MyText(
                              title:
                                  "${serviceListModel.getCurrencyName()} ${getIt<Utilities>().convertNumToAr(context: context, value: "${serviceListModel.price.round()}")}",
                              color: selected()
                                  ? MyColors.primary
                                  : MyColors.black,
                              size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(width: 10.h),
                            Icon(
                              Icons.access_time,
                              color: MyColors.greyLight,
                              size: deviceModel.isTablet ? 12.sp : 15.sp,
                            ),
                            SizedBox(width: 3),
                            Row(
                              children: [
                                MyText(
                                  title: getIt<Utilities>().convertNumToAr(
                                    context: context,
                                   value:  " ${serviceListModel.serviceDuration}",
                                  ),
                                  color: MyColors.blackOpacity,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                                ),
                                SizedBox(width: 2),
                                MyText(
                                  title: tr("min"),
                                  color: MyColors.blackOpacity,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp,
                                ),
                              ],
                            ),
                            if (serviceListModel.getServiceGender().isNotEmpty)
                              Container(
                                margin: EdgeInsetsDirectional.only(
                                  end: 3,
                                  start: 20.w,
                                ),
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: MyColors.greyLight,
                                ),
                              ),
                            Expanded(
                              child: MyText(
                                  title: serviceListModel.getServiceGender(),
                                  color: MyColors.blackOpacity,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  InkWell(
                    onTap: () => servicesData.onSelectService(serviceListModel),
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          height: 59.h,
                          child: Column(
                            children: [
                              CachedImage(
                                url: serviceListModel.picture.firstOrNull ??
                                    "".replaceAll("\\", "/"),
                                width: deviceModel.isTablet ? 35.w : 50.w,
                                height: deviceModel.isTablet ? 35.w : 50.w,
                                borderRadius: BorderRadius.circular(6.r),
                                fit: BoxFit.cover,
                                bgColor: MyColors.defaultImgBg,
                                placeHolder: ServicePlaceholder(),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 31.h,
                          width: 31.w,
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color:
                                selected() ? MyColors.primary : MyColors.white,
                            border: Border.all(
                              color: selected()
                                  ? MyColors.white
                                  : MyColors.primary,
                              width: selected() ? 1.5 : 1,
                            ),
                          ),
                          child: Icon(
                            selected() ? Icons.done : Icons.add,
                            color:
                                selected() ? MyColors.white : MyColors.primary,
                            size: deviceModel.isTablet ? 12.sp : 17.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: selected() && serviceListModel.staffDetail.length != 0,
              child: Container(
                height: deviceModel.isTablet ? 65.h : 70.sm,
                decoration:
                    BoxDecoration(color: MyColors.primary.withOpacity(.04)),
                child: BuildScrollableBodyOfItems(
                  addStaff: (i) => servicesData.addStaff(serviceListModel, i),
                  removeStaff: () => servicesData.removeStaff(serviceListModel),
                  servicesBloc: servicesData.servicesBloc,
                  model: deviceModel,
                  serviceListModel: serviceListModel,
                ),
              ),
            ),
            Visibility(
              visible: !selected() && index != length - 1 && unSelected(),
              child: Divider(
                height: 0,
                thickness: 1,
                color: MyColors.indicatorBackgroundColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool unSelected() {
    if (index == length - 1) return true;
    return servicesData.servicesBloc.state.data
            .firstWhereOrNull(
                (element) => element.serviceId == list[index + 1].serviceId)
            ?.selected ??
        true;
  }

  bool selected() {
    return servicesData.servicesBloc.state.data.any((element) =>
            element.businessServiceID == serviceListModel.businessServiceID) &&
        serviceListModel.staffDetail.isNotEmpty;
  }
}
