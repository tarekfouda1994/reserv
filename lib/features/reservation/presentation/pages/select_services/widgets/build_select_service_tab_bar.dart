part of 'select_services_widgets_imports.dart';

class BuildSelectedServiceTabBar extends StatelessWidget {
  final SelectServicesData servicesData;
  final DeviceModel model;
  final List<ServiceTypesModel> serviceTypes;

  const BuildSelectedServiceTabBar({
    Key? key,
    required this.servicesData,
    required this.model,
    required this.serviceTypes,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: model.isTablet ? 44.h : 46.h,
      color: MyColors.white,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(vertical: 1.h),
      child: SingleChildScrollView(
        padding: const EdgeInsetsDirectional.only(start: 10,top: 4,bottom: 4),
        scrollDirection: Axis.horizontal,
        controller: servicesData.scrollTabsController,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: List.generate(serviceTypes.length, (index) {
            return BuildTabButtons(
              index: index,
              key: serviceTypes[index].tabKey,
              servicesData: servicesData,
              model: serviceTypes[index],
            );
          }),
        ),
      ),
    );
  }
}
