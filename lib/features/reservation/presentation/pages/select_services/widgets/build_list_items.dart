part of 'select_services_widgets_imports.dart';

class BuildListItems extends StatelessWidget {
  final List<ServiceTypesModel> serviceTypes;
  final ReservationRootData rootData;

  const BuildListItems(
      {Key? key, required this.serviceTypes, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Expanded(
      child: SingleChildScrollView(
        controller: rootData.servicesData.scrollController,
        child: Column(
          children: List.generate(serviceTypes.length, (index) {
            return Column(
              key: serviceTypes[index].key,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BuildCatTitle(
                  title: serviceTypes[index].getServiceTypeName(),
                  count: serviceTypes[index].servicesList.length,
                  model: device,
                ),
                CustomCard(
                  padding: EdgeInsets.zero,
                  child: ListView.separated(
                    itemCount: serviceTypes[index].servicesList.length,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                    itemBuilder: (context, indexItem) {
                      return DisableWidget(
                        isDisable: serviceTypes[index].servicesList[indexItem].staffDetail.isEmpty,
                        child: BuildItem(
                          list:serviceTypes[index].servicesList,
                          updateWish:()=> rootData.updateWish(context, serviceTypes[index].servicesList[indexItem], true),
                          servicesData: rootData.servicesData,
                          deviceModel: device,
                          length: serviceTypes[index].servicesList.length,
                          serviceListModel: serviceTypes[index].servicesList[indexItem],
                          index: indexItem,
                          id: serviceTypes[index].businessId,
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Container(height: 3);
                    },
                  ),
                )
              ],
            );
          }),
        ),
      ),
    );
  }
}
