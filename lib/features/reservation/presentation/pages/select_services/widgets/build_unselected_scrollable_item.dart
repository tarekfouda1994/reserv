part of 'select_services_widgets_imports.dart';

class BuildUnSelectedScrollableItem extends StatelessWidget {
  final Function(int) addStaff;
  final DeviceModel model;
  final int index;
  final ServiceListModel serviceListModel;

  const BuildUnSelectedScrollableItem(
      {Key? key,
      required this.index,
      required this.model,
      required this.serviceListModel,
      required this.addStaff})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => addStaff(index),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 9.h, horizontal: 3.w),
        padding: EdgeInsets.symmetric(
            vertical: model.isTablet ? 2.h : 5.h, horizontal: 8.w),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.r), color: MyColors.white),
        child: Row(
          children: [
            Visibility(
              visible: serviceListModel.staffDetail[index].avatar.isNotEmpty &&
                  serviceListModel.staffDetail[index].staffID != "0",
              child: CachedImage(
                url: serviceListModel.staffDetail[index].avatar
                    .replaceAll("\\", "/"),
                width: 30.w,
                height: 30.h,
                borderColor: MyColors.grey.withOpacity(.5),
                boxShape: BoxShape.circle,
                haveRadius: false,
                bgColor: MyColors.defaultImgBg,
                placeHolder: StaffPlaceholder(),
                fit: BoxFit.cover,
              ),
              replacement: SvgPicture.asset(
                Res.defaultProfile,
                width: 30.h,
                height: 30.h,
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MyText(
                    title: serviceListModel.staffDetail[index].getStaffName(),
                    color: MyColors.black,

                    size: model.isTablet ? 6.sp : 9.sp),
                Visibility(
                  visible: serviceListModel.staffDetail[index].getStaffName() !=
                      tr("any_one") && serviceListModel.staffDetail[index].rating != 0,
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        Res.star,
                        color: MyColors.primary,
                        height: 12.sp,
                        width: 12.sp,
                      ),
                      MyText(
                        title: getIt<Utilities>().convertNumToAr(
                          context: context,
                          value: " ${serviceListModel.staffDetail[index].rating}",
                        ),
                        color: MyColors.primary,

                        size: model.isTablet ? 5.sp : 8.sp,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
