part of 'select_services_widgets_imports.dart';

class BuildDetailsBottomSheet extends StatelessWidget {
  final SelectServicesData servicesData;
  final void Function() updateWish;
  final ServiceListModel serviceListModel;

  const BuildDetailsBottomSheet(
      {Key? key,
      required this.servicesData,
      required this.serviceListModel,
      required this.updateWish})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: servicesData.servicesBloc,
        builder: (context, state) {
          bool isSelected =
              state.data.any((element) => element == serviceListModel);
          return Container(
            height: device.isTablet
                ? MediaQuery.of(context).size.height * .35.h
                : MediaQuery.of(context).size.height * .55.h,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: ListView(
                        children: [
                          BuildSwiperBottomSheet(
                            servicesData: servicesData,
                            model: device,
                            pictures: serviceListModel.picture,
                          ),
                          BuildBottomSheetDetails(
                              model: device,
                              serviceListModel: serviceListModel),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        servicesData.onSelectService(serviceListModel);
                        AutoRouter.of(context).pop();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        height: device.isTablet ? 60.sm : 50.sm,
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          border: Border.all(
                              color: isSelected
                                  ? MyColors.errorColor
                                  : MyColors.white),
                          borderRadius: BorderRadius.circular(50.r),
                          color: isSelected ? MyColors.white : MyColors.primary,
                        ),
                        child: Visibility(
                          visible: isSelected,
                          child: MyText(
                              alien: TextAlign.center,
                              size: device.isTablet ? 8.sp : 11.sp,
                              title: tr("remove_service"),
                              color: MyColors.errorColor),
                          replacement: MyText(
                              alien: TextAlign.center,
                              size: device.isTablet ? 8.sp : 11.sp,
                              title: tr("add_service"),
                              color: MyColors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      BuildFavouriteIcon(
                          onTap: updateWish,
                          hasWish: serviceListModel.hasWishItem),
                      InkWell(
                        borderRadius: BorderRadius.circular(100),
                        onTap: () => AutoRouter.of(context).pop(),
                        child: Container(
                          padding: const EdgeInsets.all(6),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: MyColors.black),
                          child: Icon(
                            Icons.clear,
                            color: MyColors.white,
                            size: 19.sp,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }
}
