part of 'select_services_widgets_imports.dart';

class BuildBottomSheetDetails extends StatelessWidget {
  final DeviceModel model;
  final ServiceListModel serviceListModel;

  const BuildBottomSheetDetails(
      {Key? key, required this.model, required this.serviceListModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 25.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: serviceListModel.getServiceName(),
                color: MyColors.black,
                size: model.isTablet ? 9.sp : 13.sp,
                fontWeight: FontWeight.bold,
              ),
              if (serviceListModel.rating != 0)
                Row(
                  children: [
                    MyText(
                      title: _convertNumToAr(
                          context, "${serviceListModel.rating.toString()} "),
                      color: MyColors.amber,
                      fontFamily: CustomFonts.primarySemiBoldFont,
                      size: model.isTablet ? 5.5.sp : 9.sp,
                    ),
                    SvgPicture.asset(
                      Res.star,
                      color: MyColors.amber,
                      height: 12.sp,
                      width: 12.sp,
                    ),
                  ],
                ),
            ],
          ),
          Row(
            children: [
              Row(mainAxisSize: MainAxisSize.min, children: [
                MyText(
                  title: _convertNumToAr(context,
                      "${serviceListModel.getCurrencyName()} ${serviceListModel.price}"),
                  color: MyColors.primary,
                  size: model.isTablet ? 7.sp : 11.sp,
                  fontFamily: CustomFonts.primarySemiBoldFont,
                ),
                const SizedBox(
                  width: 12,
                ),
                Icon(
                  Icons.access_time_outlined,
                  color: MyColors.primary,
                  size: 17.sp,
                ),
                MyText(
                  title: _convertNumToAr(context,
                      " ${serviceListModel.serviceDuration} ${tr("min")}"),
                  color: MyColors.primary,
                  size: model.isTablet ? 7.sp : 11.sp,
                ),
              ]),
            ],
          ),
          const SizedBox(height: 10),
          MyText(
            title: serviceListModel.getServiceDescription(),
            color: MyColors.blackOpacity,
            size: model.isTablet ? 6.sp : 10.sp,
          ),
        ],
      ),
    );
  }

  String _convertNumToAr(BuildContext context, String text) {
    return getIt<Utilities>().convertNumToAr(context: context, value: text);
  }
}
