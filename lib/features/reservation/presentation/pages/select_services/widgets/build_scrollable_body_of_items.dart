part of 'select_services_widgets_imports.dart';

class BuildScrollableBodyOfItems extends StatelessWidget {
  final void Function(int) addStaff;
  final void Function() removeStaff;
  final GenericBloc<List<ServiceListModel>> servicesBloc;

  final DeviceModel model;
  final ServiceListModel serviceListModel;

  const BuildScrollableBodyOfItems(
      {Key? key,
      required this.model,
      required this.serviceListModel,
      required this.addStaff,
      required this.removeStaff,
      required this.servicesBloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
        GenericState<List<ServiceListModel>>>(
      bloc: servicesBloc,
      builder: (context, state) {
        return Visibility(
          child: Container(
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.vertical(bottom: Radius.circular(13.r)),
              color: MyColors.primaryLight,
            ),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: serviceListModel.staffDetail.length,
              itemBuilder: (cxt, index) {
                return AnimatedSwitcher(
                  duration: Duration(milliseconds: 700),
                  reverseDuration: Duration(milliseconds: 50),
                  child: _isSelected(index)
                      ? BuildSelectedScrollableItem(
                          index: index,
                          model: model,
                          serviceListModel: serviceListModel,
                          removeStaff: removeStaff,
                        )
                      : BuildUnSelectedScrollableItem(
                          addStaff: addStaff,
                          index: index,
                          model: model,
                          serviceListModel: serviceListModel,
                        ),
                );
              },
            ),
          ),
        );
      },
    );
  }

  bool _isSelected(int index) {
    return serviceListModel.staffName ==
            serviceListModel.staffDetail[index].getStaffName() &&
        serviceListModel.staffId == serviceListModel.staffDetail[index].staffID;
  }
}
