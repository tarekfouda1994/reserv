part of 'select_services_widgets_imports.dart';

class BuildSelectServiceShimmer extends StatelessWidget {
  final SelectServicesData servicesData;

  const BuildSelectServiceShimmer({Key? key, required this.servicesData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        Flexible(
          child: ListView(
            children: [
              SelectServicesDetails(
                model: device,
                servicesData: servicesData,
              ),
              BuildShimmerView(height: 30.h, width: MediaQuery.of(context).size.width * .9),
              ...List.generate(
                7,
                (index) =>
                    BuildShimmerView(height: 100.h, width: MediaQuery.of(context).size.width * .9),
              )
            ],
          ),
        ),
      ],
    );
  }
}
