part of 'select_services_widgets_imports.dart';

class BuildCatTitle extends StatelessWidget {
  final String title;
  final int count;
  final DeviceModel model;

  const BuildCatTitle({Key? key, required this.title, required this.count, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12, top: 24, right: 16, left: 16),
      child: Row(children: [
        RichText(
          textScaleFactor: 1.2,
          text: TextSpan(
            text: title,
            style: TextStyle(
              color: MyColors.black,
              fontFamily: CustomFonts.primaryFont,
              fontSize: model.isTablet ? 6.sp : 13.sp,
              fontWeight: FontWeight.bold,
            ),
            children: [
              TextSpan(
                text: " ",
              ),
              if(count>0)
              TextSpan(
                text: "(${getIt<Utilities>().convertNumToAr(context: context, value: "$count")})",
                style: TextStyle(
                  color: MyColors.black,
                  fontFamily: CustomFonts.primaryFont,
                  fontSize: model.isTablet ? 6.sp : 13.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
