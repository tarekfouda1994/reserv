part of 'select_services_widgets_imports.dart';

class BuildSelectedScrollableItem extends StatelessWidget {
  final DeviceModel model;
  final void Function() removeStaff;
  final int index;
  final ServiceListModel serviceListModel;

  const BuildSelectedScrollableItem(
      {Key? key,
      required this.index,
      required this.model,
      required this.serviceListModel,
      required this.removeStaff})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: removeStaff,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8.h, horizontal: 3.w),
        padding: EdgeInsets.symmetric(horizontal: 8.w),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.r),
          color: MyColors.primary,
        ),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Row(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: MyColors.white, shape: BoxShape.circle),
                child: Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      color: MyColors.primary, shape: BoxShape.circle),
                  child: Icon(
                    Icons.done,
                    color: MyColors.white,
                    size: model.isTablet ? 20.sp : 24.sp,
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MyText(
                    title: _staffDetail().getStaffName(),
                    color: MyColors.white,
                    size: model.isTablet ? 6.sp : 9.sp,
                  ),
                  SizedBox(height: 4.sm),
                  Visibility(
                    visible: !_isAnyOne() && _staffDetail().rating != 0,
                    child: Row(
                      children: [
                        SvgPicture.asset(Res.star,
                            color: MyColors.gold, height: 12.sp, width: 12.sp),
                        MyText(
                            title: " ${_staffDetail().rating}",
                            color: MyColors.white,
                            size: model.isTablet ? 5.sp : 8.sp)
                      ],
                    ),
                  ),
                ],
              ),
              if (!_isAnyOne())
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 8),
                  height: 35.h,
                  width: 1.w,
                  color: MyColors.white,
                ),
              if (!_isAnyOne())
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MyText(
                      title: tr("available_days"),
                      color: MyColors.white,
                      size: model.isTablet ? 5.sp : 8.sp,
                    ),
                    SizedBox(height: 5.sm),
                    Row(
                      children:
                          List.generate(_staffDetail().days.length, (dayIndex) {
                        return Container(
                          padding: EdgeInsets.all(4.sp),
                          margin: EdgeInsets.symmetric(horizontal: 2.w),
                          decoration: BoxDecoration(
                              color:
                                  _staffDetail().days[dayIndex].isDateAvailable
                                      ? MyColors.white
                                      : MyColors.white.withOpacity(.5),
                              shape: BoxShape.circle),
                          child: MyText(
                            title: getIt<Utilities>().convertNumToAr(
                             context:  context,
                             value:  _staffDetail().days[dayIndex].day,
                            ),
                            color: MyColors.primary,
                            size: model.isTablet ? 6.sp : 8.sp,
                          ),
                        );
                      }),
                    ),
                  ],
                ),
            ],
          ),
        ]),
      ),
    );
  }

  StaffModel _staffDetail() => serviceListModel.staffDetail[index];

  bool _isAnyOne() => _staffDetail().getStaffName() == tr("any_one");
}
