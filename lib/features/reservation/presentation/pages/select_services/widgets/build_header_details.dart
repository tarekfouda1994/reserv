part of 'select_services_widgets_imports.dart';

class SelectServicesDetails extends StatelessWidget {
  final DeviceModel model;
  final SelectServicesData servicesData;

  const SelectServicesDetails(
      {Key? key, required this.model, required this.servicesData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<ServiceDetailsModel?>,
        GenericState<ServiceDetailsModel?>>(
      bloc: servicesData.detailsCubit,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Container(
            color: MyColors.primary,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25).r,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(
                  Res.building,
                  height: 18.r,
                  width: 18.r,
                  color: MyColors.white,
                ),
                SizedBox(width: 10.sm),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: MyText(
                                fontFamily: CustomFonts.primarySemiBoldFont,
                                title: state.data?.getBusinessName() ?? "",
                                color: MyColors.white,
                                size: model.isTablet ? 7.sp : 11.sp),
                          ),
                          Offstage(
                            offstage: state.data?.rating == 0,
                            child: Row(
                              children: [
                                MyText(
                                    title: getIt<Utilities>().convertNumToAr(
                                        context: context, value: "${state.data?.rating} "),
                                    color: MyColors.amber,
                                    fontFamily: CustomFonts.primarySemiBoldFont,
                                    size: model.isTablet ? 5.5.sp : 9.sp),
                                SvgPicture.asset(
                                  Res.star,
                                  color: MyColors.amber,
                                  height: 12.sp,
                                  width: 12.sp,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 8.h),
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () => AutoRouter.of(context).push(
                                TrackingMapRoute(
                                  businessName: state.data!.getBusinessName(),
                                  address: state.data!.getAddressName(),
                                  lat: state.data!.latitude,
                                  lng: state.data!.longitude,
                                ),
                              ),
                              child: Row(children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 2.0),
                                  child: SvgPicture.asset(
                                      Res.location_arrow_icon,
                                      color: Color(0xffABDE54),
                                      width: model.isTablet ? 5.5.w : null),
                                ),
                                MyText(
                                    decoration: TextDecoration.underline,
                                    title: tr("Direction") + " ",
                                    color: Color(0xffABDE54),
                                    size: model.isTablet ? 5.5.sp : 9.sp),
                                Offstage(
                                  offstage:
                                      state.data?.serviceDistance.isEmpty ??
                                          true,
                                  child: MyText(
                                    title: state.data?.serviceDistance ?? "",
                                    color: MyColors.white,
                                    size: model.isTablet ? 5.5.sp : 9.sp,
                                  ),
                                ),
                              ]),
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsetsDirectional.only(end: 3, start: 25.w),
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: MyColors.white),
                          ),
                          MyText(
                            title: state.data!.getBusinessGender(),
                            color: MyColors.white,
                            size: model.isTablet ? 5.5.sp : 9.sp,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }
        return BuildShimmerView(
          height: 88.h,
          width: MediaQuery.of(context).size.width,
        );
      },
    );
  }
}
