part of 'select_services_widgets_imports.dart';

class BuildSwiperBottomSheet extends StatelessWidget {
  final DeviceModel model;
  final SelectServicesData servicesData;
  final List<String> pictures;

  const BuildSwiperBottomSheet(
      {Key? key,
      required this.servicesData,
      required this.model,
      required this.pictures})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: pictures.isNotEmpty,
      child: BlocBuilder<GenericBloc<int>, GenericState<int>>(
          bloc: servicesData.indicatorBloc,
          builder: (cxt, state) {
            return Column(
              children: [
                pictures.length != 0
                    ? CarouselSlider(
                        options: CarouselOptions(
                            enlargeCenterPage: true,
                            onPageChanged: (index, carousel) {
                              servicesData.indicatorBloc.onUpdateData(index);
                            },
                            autoPlay: pictures.length > 1,
                            height: model.isTablet
                                ? MediaQuery.of(context).size.height * .2.h
                                : MediaQuery.of(context).size.height * .25.h,
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 1000),
                            viewportFraction: 1),
                        items: List.generate(pictures.length, (index) {
                          return CachedImage(
                            url: pictures[index].replaceAll("\\", "/"),
                            borderRadius: BorderRadius.vertical(
                                top: Radius.circular(15.r)),
                            fit: BoxFit.cover,
                          );
                        }),
                      )
                    : CachedImage(
                        url: "",
                        height: model.isTablet
                            ? MediaQuery.of(context).size.height * .2.h
                            : MediaQuery.of(context).size.height * .25.h,
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(15.r),
                        ),
                        fit: BoxFit.cover,
                        placeHolder: ServicePlaceholder(),
                      ),
                SizedBox(height: 6.h),
                Visibility(
                  visible: pictures.length != 1,
                  child: CarouselIndicator(
                    color: MyColors.primary.withOpacity(.07),
                    activeColor: MyColors.primary,
                    count: pictures.length == 0 ? 1 : pictures.length,
                    index: state.data,
                  ),
                ),
              ],
            );
          }),
      replacement: Container(height: 40),
    );
  }
}
