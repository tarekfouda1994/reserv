import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_types_model/service_types_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/business_service_entity.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/select_services/widgets/select_services_widgets_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/product_details/widgets/product_details_widgets_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../core/helpers/custom_toast.dart';
import '../../../../../core/localization/localization_methods.dart';
import '../../../../search/data/models/service_details_model/service_details_model.dart';
import '../../../../search/domain/entites/service_details_entity.dart';
import '../../../../search/domain/use_cases/get_service_details.dart';
import '../../../domain/use_cases/get_company_services.dart';

part 'select_services.dart';
part 'select_services_data.dart';
