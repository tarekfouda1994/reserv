part of 'select_services_imports.dart';

class SelectServicesData {
  final GenericBloc<int> indicatorBloc = GenericBloc(0);
  final GenericBloc<List<ServiceTypesModel>> companyServicesBloc =
      GenericBloc([]);
  final GenericBloc<List<ServiceListModel>> servicesBloc = GenericBloc([]);
  final GenericBloc<num> totalBloc = GenericBloc(0);

  final GenericBloc<ServiceDetailsModel?> detailsCubit = GenericBloc(null);

  final ScrollController scrollController = ScrollController();
  final ScrollController scrollTabsController = ScrollController();

  void scrollToServiceType(GlobalKey key, int index) {
    scrollController.position
        .ensureVisible(key.currentContext!.findRenderObject()!,
            duration: const Duration(milliseconds: 300), alignment: 0.1)
        .then((value) {
      _onUpdateSelectedTab(index);
    });
  }

  void _onUpdateSelectedTab(int index) {
    int lastIndex = companyServicesBloc.state.data.indexWhere(
      (element) => element.selected,
    );
    companyServicesBloc.state.data[lastIndex].selected = false;
    companyServicesBloc.state.data[index].selected = true;
    companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
  }

  void scrollTabs() {
    scrollController.addListener(() {
      _selectCurrentCategory();
    });
  }

  void _selectCurrentCategory() {
    var items = companyServicesBloc.state.data
        .where((element) =>
            getCurrentPosition(element.key) >= 180 &&
            getCurrentPosition(element.key) <= 500)
        .toList();
    if (items.isNotEmpty) {
      int last = companyServicesBloc.state.data.indexOf(items.last);
      var element = items.first;
      if (last == (companyServicesBloc.state.data.length - 1)) {
        element = items.last;
      }
      var prevItem =
          companyServicesBloc.state.data.firstWhere((item) => item.selected);
      _updateTabColor(prevItem, element);
      scrollTabsController.position.ensureVisible(
        element.tabKey.currentContext!.findRenderObject()!,
        duration: const Duration(milliseconds: 400),
        alignment: 0.1,
      );
    }
  }

  void _updateTabColor(ServiceTypesModel prevItem, ServiceTypesModel element) {
    prevItem.selected = false;
    element.selected = true;
    companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
  }

  double getCurrentPosition(GlobalKey key) {
    RenderBox box = key.currentContext!.findRenderObject() as RenderBox;
    Offset position = box.localToGlobal(Offset.zero);
    return position.dy;
  }

  void onSelectService(ServiceListModel model) {
    if (servicesBloc.state.data.any(
        (element) => element.businessServiceID == model.businessServiceID)) {
      servicesBloc.state.data
          .removeWhere((e) => e.businessServiceID == model.businessServiceID);
    } else {
      model.staffId = "0";
      model.staffName = tr("any_one");
      servicesBloc.state.data.add(model);
    }
    servicesBloc.onUpdateData(servicesBloc.state.data);
    companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
    calculateTotal();
  }

  void calculateTotal() {
    var total =
        servicesBloc.state.data.fold(0, (num prev, e) => prev + e.price);
    totalBloc.onUpdateData(total);
  }

// ======>>>> test this
  void addStaff(ServiceListModel model, int index) {
    servicesBloc.state.data.removeWhere(
      (e) => e.businessServiceID == model.businessServiceID,
    );
    servicesBloc.state.data.add(model);
    var data = servicesBloc.state.data.map((e) {
      if (e.businessServiceID == model.businessServiceID) {
        e.staffId = model.staffDetail[index].staffID;
        e.staffName = model.staffDetail[index].getStaffName();
      }
      return e;
    }).toList();
    servicesBloc.onUpdateData(data);
  }

  removeStaff(ServiceListModel model) {
    if (model.staffDetail.length != 0)
      servicesBloc.state.data.map((e) {
        if (e == model) {
          e.staffId = "";
          e.staffName = "";
        }
        return e;
      }).toList();
    servicesBloc.onUpdateData(servicesBloc.state.data);
  }

  void nextPage(ReservationRootData rootData) {
    if (rootData.servicesData.servicesBloc.state.data.length == 0) {
      CustomToast.showSimpleToast(
        msg: tr("worn_to_select_service"),
        title: tr("Missing_reservation_service"),
        type: ToastType.info,
      );
      return;
    }
    for (var e in servicesBloc.state.data) {
      _checkServiceStaff(e);
      if (e.staffId.isEmpty) {
        CustomToast.showSimpleToast(
          msg: "${tr("select")} ${e.getServiceName()} ${tr("staff")}",
          title: tr("Warning_Toast"),
          type: ToastType.warning,
        );
        return;
      }
    }
    if (!servicesBloc.state.data
        .any((element) => element.staffDetail.isEmpty)) {
      rootData.dateData.applyTogetherFunc(servicesBloc.state.data);
      rootData.goNextPage();
    }
  }

  void _checkServiceStaff(ServiceListModel e) {
    if (e.staffDetail.isEmpty) {
      CustomToast.showSimpleToast(
        msg: "${e.getServiceName()} ${tr("staff")} ${tr("empty_staff_alert")}",
        title: tr("Missing_reservation_staff"),
        type: ToastType.info,
      );
      return;
    }
    if (e.staffId == "0" && e.staffDetail.isNotEmpty) {
      e.staffName = tr("any_one");
    }
  }

  void _bottomSheet(BuildContext context, Widget widget) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) => widget,
    );
  }

  void allServicesBottomSheet(
    BuildContext context,
    SelectServicesData servicesData,
  ) {
    Widget widget = BuildSelectedServicesSheet(
      bloc: servicesData.servicesBloc,
      onRemoveItem: (index) => _onRemoveItem(servicesData, index, context),
      onDeleteAll: () => _onDeleteAllSheetServices(servicesData, context),
    );
    _bottomSheet(context, widget);
  }

  void _onRemoveItem(
      SelectServicesData servicesData, int index, BuildContext context) {
    var cubit = servicesData.servicesBloc.state.data;
    servicesData.selectedItem(cubit[index], context);
    if (cubit.isEmpty) {
      AutoRouter.of(context).pop();
    }
  }

  void _onDeleteAllSheetServices(
      SelectServicesData servicesData, BuildContext context) {
    servicesData.servicesBloc.onUpdateData([]);
    servicesData.companyServicesBloc.onUpdateData(
      servicesData.companyServicesBloc.state.data,
    );
    AutoRouter.of(context).pop();
  }

  selectedItem(ServiceListModel model, BuildContext context) {
    if (servicesBloc.state.data.any((element) => element == model)) {
      servicesBloc.state.data.remove(model);
    } else {
      model.staffId = "0";
      servicesBloc.state.data.add(model);
    }
    servicesBloc.onUpdateData(servicesBloc.state.data);
    companyServicesBloc.onUpdateData(companyServicesBloc.state.data);
  }

  void DetailsBottomSheet(BuildContext context, SelectServicesData servicesData,
      ServiceListModel serviceListModel, void Function() updateWish) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) {
        return BuildDetailsBottomSheet(
          servicesData: servicesData,
          serviceListModel: serviceListModel,
          updateWish: updateWish,
        );
      },
    );
  }

  void getServiceDetails(BuildContext context, String id,
      {bool refresh = true}) async {
    var data = await GetServiceDetails()
        .call(ServiceDetailsPrams(id: id, refresh: refresh));
    if (data != null) {
      detailsCubit.onUpdateData(data);
    }
  }

  void getCompanyServices(
      BuildContext context, String id, String? serviceId) async {
    var data = await GetCompanyServices().call(_servicesPrams(id));
    if (data.isNotEmpty) {
      data.first.selected = true;
      for (var element in data) {
        for (var service in element.servicesList) {
          _setInitialSelectedStaffData(service);
          _addDefaultStaff(service);
          _initialSelectedService(element, serviceId ?? "");
        }
      }
    }
    companyServicesBloc.onUpdateData(data);
    _scrollToInitSelectedService(serviceId ?? "");
  }

  void _setInitialSelectedStaffData(ServiceListModel service) {
    final isContain = servicesBloc.state.data.any(
      (e) => e.serviceId == service.serviceId,
    );
    if (servicesBloc.state.data.isNotEmpty) {
      if (isContain) {
        final item = servicesBloc.state.data.singleWhere(
          (ele) => ele.serviceId == service.serviceId,
        );
        service.staffName = item.staffName;
        service.staffId = item.staffId;
      }
    }
  }

  BusinessServicePrams _servicesPrams(String id) =>
      BusinessServicePrams(id: id, loadImages: true, refresh: true);

  void _addDefaultStaff(ServiceListModel service) {
    if (service.staffDetail.isNotEmpty) {
      service.staffDetail = service.staffDetail.toSet().toList();
      service.staffDetail.sort((a, b) => b.days
          .where((day) => day.isDateAvailable)
          .length
          .compareTo(a.days.where((day) => day.isDateAvailable).length));
      service.staffId = servicesBloc.state.data
              .firstWhereOrNull(
                  (e) => e.businessServiceID == service.businessServiceID)
              ?.staffId ??
          "";
      service.staffDetail.insert(
        0,
        service.staffDetail.first.copyWith(
          staffNameArabic: tr("any_one"),
          staffNameEnglish: tr("any_one"),
          staffID: "0",
        ),
      );
    }
  }

  void _initialSelectedService(
    ServiceTypesModel serviceTypesModel,
    String serviceId,
  ) async {
    if (serviceId.isNotEmpty) {
      var initService = serviceTypesModel.servicesList
          .where((e) => e.serviceId == serviceId && e.staffDetail.isNotEmpty)
          .toList();
      servicesBloc.state.data.addAll(initService);
      for (var e in servicesBloc.state.data) {
        e.staffId = "0";
        e.staffName = tr("any_one");
      }
      servicesBloc.onUpdateData(servicesBloc.state.data.toSet().toList());
      calculateTotal();
    }
  }

  void _scrollToInitSelectedService(String serviceId) {
    Future.delayed(Duration(milliseconds: 500), () async {
      for (ServiceTypesModel e in companyServicesBloc.state.data) {
        if (e.servicesList.any((element) => element.serviceId == serviceId)) {
          int index = companyServicesBloc.state.data.indexOf(e);
          scrollToServiceType(e.key, index);
          scrollTabsController.position.ensureVisible(
              e.tabKey.currentContext!.findRenderObject()!,
              alignment: 0.1);
          await Future.delayed(Duration(milliseconds: 500));
          scrollTabs();
        }
      }
    });
  }
}
