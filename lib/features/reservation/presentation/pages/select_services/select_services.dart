part of 'select_services_imports.dart';

class SelectServices extends StatefulWidget {
  final ReservationRootData rootData;
  final String businessId;

  const SelectServices(
      {Key? key, required this.businessId, required this.rootData})
      : super(key: key);

  @override
  State<SelectServices> createState() => _SelectServicesState();
}

class _SelectServicesState extends State<SelectServices> {
  late SelectServicesData servicesData;
  late ReservationRootData rootData;

  @override
  void initState() {
    rootData = widget.rootData;
    servicesData = widget.rootData.servicesData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceTypesModel>>,
        GenericState<List<ServiceTypesModel>>>(
      bloc: servicesData.companyServicesBloc,
      builder: (context, state) {
        if (state is GenericUpdateState) {
          return Column(
            children: [
              SelectServicesDetails(model: device, servicesData: servicesData),
              BuildSelectedServiceTabBar(
                servicesData: servicesData,
                model: device,
                serviceTypes: state.data,
              ),
              BuildListItems(serviceTypes: state.data, rootData: rootData),
              BuildBottomScreen(
                rootData: rootData,
                servicesData: servicesData,
                model: device,
              )
            ],
          );
        }
        return BuildSelectServiceShimmer(servicesData: servicesData);
      },
    );
  }
}
