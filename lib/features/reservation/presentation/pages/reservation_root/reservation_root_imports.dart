import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/widgets/default_app_bar.dart';
import 'package:flutter_tdd/core/widgets/gradient_scaffold.dart';
import 'package:flutter_tdd/features/profile/domain/entities/wish_entity.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/remove_wish_service.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/personal_details/personal_details_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/select_services/select_services_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../core/helpers/loading_helper.dart';
import '../../../../profile/domain/use_cases/update_wishe_item.dart';
import '../../../domain/entities/personal_paroms.dart';
import '../reservation_date/reservation_date_imports.dart';
import '../reservation_details/reservation_details_imports.dart';
import 'widgets/reservation_root_widgets_imports.dart';

part 'reservation_root.dart';
part 'reservation_root_data.dart';
