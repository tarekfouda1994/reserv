part of 'reservation_root_imports.dart';

class ReservationRoot extends StatefulWidget {
  final String businessID;
  final List<ServiceListModel>? services;
  final String? serviceId;

  const ReservationRoot(
      {Key? key, required this.businessID, this.services, this.serviceId})
      : super(key: key);

  @override
  State<ReservationRoot> createState() => _ReservationRootState();
}

class _ReservationRootState extends State<ReservationRoot> {
  final ReservationRootData rootData = ReservationRootData();

  @override
  void initState() {
    rootData.dateData.businessId = widget.businessID;
    rootData.servicesData.servicesBloc.onUpdateData(widget.services ?? []);
    //This is the last update here change initial 1 to 0
    int initial = widget.services == null ? 0 : 0;
    // if (initial == 1) {
    //   rootData.servicesData.calculateTotal();
    //   rootData.dateData.applyTogetherFunc(widget.services!);
    // }
    rootData.controller = PageController(initialPage: initial);
    rootData.pagesCubit.onUpdateData(initial);
    rootData.servicesData
        .getCompanyServices(context, widget.businessID, widget.serviceId);
    rootData.servicesData.getServiceDetails(context, widget.businessID);
    rootData.personalDetailsData.expiryDate.text =
        "01/${DateTime.now().year + 1}";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return GradientScaffold(
      appBar: DefaultAppBar(
        titleWidget: BuildTitlesAppBar(
          rootData: rootData,
          model: device,
        ),
        onBack: () => rootData.goPreviousPage(context),
        actions: [
          BuildStepperView(
            rootData: rootData,
            model: device,
          ),
        ],
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: rootData.controller,
        onPageChanged: (index) => rootData.pagesCubit.onUpdateData(index),
        children: [
          SelectServices(rootData: rootData, businessId: widget.businessID),
          ReservationDate(rootData: rootData),
          if (!device.auth) PersonalDetails(rootData: rootData),
          ReservationDetails(rootData: rootData),
        ],
      ),
    );
  }
}
