part of 'reservation_root_widgets_imports.dart';

class BuildCancelReservationButton extends StatelessWidget {
  final ReservationRootData rootData;

  const BuildCancelReservationButton({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      children: [
        BuildIconContainer(
          onTap: () async =>
              await rootData.cancelDialog(device, context, rootData),
          borderColor: MyColors.grey,
          width: device.isTablet ? 30.r : 30.r,
          height: device.isTablet ? 30.r : 30.r,
          bgColor: MyColors.white,
          child: Icon(Icons.close,
              size: device.isTablet ? 10.sp : 20, color: MyColors.grey),
        ),
        MyText(
          title: tr("cancel"),
          color: MyColors.blackOpacity,
          size: device.isTablet ? 6.sp : 10.sp,
        )
      ],
    );
  }
}
