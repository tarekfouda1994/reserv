import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/widgets/build_icon_container.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/localization/localization_methods.dart';

part 'build_cancel_items_body.dart';
part 'build_cancel_reservation_button.dart';
part 'build_stepper_view.dart';
part 'build_titles_app_bar.dart';
