part of 'reservation_root_widgets_imports.dart';

class BuildCancelItemsBody extends StatelessWidget {
  final ReservationRootData rootData;

  const BuildCancelItemsBody({Key? key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: rootData.servicesData.servicesBloc,
        builder: (context, state) {
          return Container(
            height: _heightScroll(isTablet, state),
            child: ListView.separated(
                itemBuilder: (cxt, index) {
                  return Row(
                    children: [
                      CachedImage(
                        borderRadius: BorderRadius.circular(10.r),
                        url: state.data[index].picture.first
                            .replaceAll("\\", "/"),
                        height: isTablet ? 90.sm : 50.sm,
                        width: isTablet ? 90.sm : 50.sm,
                      ),
                      SizedBox(width: 8),
                      MyText(
                        title: state.data[index].getServiceName(),
                        color: MyColors.black,
                        size: isTablet ? 7.sp : 11.sp,
                      )
                    ],
                  );
                },
                separatorBuilder: (cxt, i) {
                  return SizedBox(height: 10);
                },
                itemCount: state.data.length),
          );
        });
  }

  double _heightScroll(
      bool isTablet, GenericState<List<ServiceListModel>> state) {
    return (isTablet ? 100.sm : 60.sm) *
        (state.data.length >= 5 ? 4 : state.data.length);
  }
}
