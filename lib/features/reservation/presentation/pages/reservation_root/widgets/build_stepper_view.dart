part of 'reservation_root_widgets_imports.dart';

class BuildStepperView extends StatelessWidget {
  final ReservationRootData rootData;
  final DeviceModel model;

  const BuildStepperView({Key? key, required this.rootData, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: rootData.pagesCubit,
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.only(top:model.isTablet? 6:0),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: model.isTablet ? 0 : 8.h),
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: model.isTablet ? 5.h : 6),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30.r),
                color: MyColors.grey.withOpacity(.02),
                border: Border.all(color: MyColors.grey.withOpacity(.5), width: .2)),
            child: Row(children: [
              MyText(
                  title:getIt<Utilities>().convertNumToAr(context: context, value: "${tr("step")} ${state.data + 1}/${model.auth ? "3" : "4"} "),
                  color: MyColors.primaryDark,
                  size: model.isTablet ? 6.sp : 9.sp,
                  fontWeight: FontWeight.bold),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 2),
                  width: 8.h,
                  height: 8.h,
                  decoration: BoxDecoration(
                      color: state.data == 0 ? MyColors.primary.withOpacity(.5) : MyColors.primary,
                      shape: BoxShape.circle)),
              Visibility(
                visible: state.data >= 1,
                child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 2),
                    width: 8.h,
                    height: 8.h,
                    decoration: BoxDecoration(
                        color: state.data == 1 ? MyColors.primary.withOpacity(.5) : MyColors.primary,
                        shape: BoxShape.circle)),
                replacement: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 2),
                    width: 8.h,
                    height: 8.h,
                    decoration: BoxDecoration(
                        color: MyColors.primary.withOpacity(.05), shape: BoxShape.circle)),
              ),
              if (!model.auth)
                Visibility(
                  visible: state.data >= 2,
                  child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 2),
                      width: 8.h,
                      height: 8.h,
                      decoration: BoxDecoration(
                          color:
                              state.data == 2 ? MyColors.primary.withOpacity(.5) : MyColors.primary,
                          shape: BoxShape.circle)),
                  replacement: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 2),
                      width: 8.h,
                      height: 8.h,
                      decoration: BoxDecoration(
                          color: MyColors.primary.withOpacity(.05), shape: BoxShape.circle)),
                ),
              Visibility(
                visible: state.data == (model.auth ? 2 : 3),
                child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 2),
                    width: 8.h,
                    height: 8.h,
                    decoration: BoxDecoration(
                        color: state.data == (model.auth ? 2 : 3)
                            ? MyColors.primary.withOpacity(.5)
                            : MyColors.primary,
                        shape: BoxShape.circle)),
                replacement: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 2),
                    width: 8.h,
                    height: 8.h,
                    decoration: BoxDecoration(
                        color: MyColors.primary.withOpacity(.05), shape: BoxShape.circle)),
              ),
            ]),
          ),
        );
      },
    );
  }
}
