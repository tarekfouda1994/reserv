part of 'reservation_root_widgets_imports.dart';

class BuildTitlesAppBar extends StatelessWidget {
  final ReservationRootData rootData;
  final DeviceModel model;

  const BuildTitlesAppBar({required this.rootData, required this.model});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<int>, GenericState<int>>(
      bloc: rootData.pagesCubit,
      builder: (context, state) {
        return MyText(
          title: rootData.appBarTitle(state.data, model.auth),
          color: MyColors.black,
          size: model.isTablet ? 8.sp : 12.sp,
          fontWeight: FontWeight.bold,
        );
      },
    );
  }
}
