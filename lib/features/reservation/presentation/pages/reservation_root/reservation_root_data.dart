part of 'reservation_root_imports.dart';

class ReservationRootData {
  final GenericBloc<int> pagesCubit = GenericBloc(0);
  late PageController controller;
  final ReservationDateData dateData = ReservationDateData();
  final SelectServicesData servicesData = SelectServicesData();
  final PersonalDetailsData personalDetailsData = PersonalDetailsData();
  final ReservationDetailsData reservationDetailsData =
      ReservationDetailsData();

  PersonalParams? personalParams;

  void goNextPage() {
    pagesCubit.onUpdateData(pagesCubit.state.data + 1);
    controller.nextPage(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );
  }

  void goPreviousPage(BuildContext context) {
    if (pagesCubit.state.data == 0) {
      AutoRouter.of(context).pop();
      return;
    } else {
      if (pagesCubit.state.data == 2) {
        _reseatSelectedDateTimes();
        if (dateData.selectedServiceCubit.state.data.length !=
            servicesData.servicesBloc.state.data.length) {
          dateData.applyTogetherFunc(servicesData.servicesBloc.state.data);
          // if (servicesData.servicesBloc.state.data.length == 1)
          // dateData.onSelectDate(
          //     false,
          //     dateData.availableDatesCubit.state.data.first.date,
          //     servicesData.servicesBloc.state.data);
        }
      }
      pagesCubit.onUpdateData(pagesCubit.state.data - 1);
      controller.previousPage(
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  void _reseatSelectedDateTimes() {
    if (pagesCubit.state.data == 1) {
      dateData.selectedServiceCubit.onUpdateData([]);
      servicesData.servicesBloc.state.data
          .map((e) => e.selectedTimeForService = "-")
          .toList();
      servicesData.servicesBloc
          .onUpdateData(servicesData.servicesBloc.state.data);
    }
  }

  void updateWish(
      BuildContext context, ServiceListModel model, bool fromDialog) async {
    if (checkUserAuth(context)) {
      var params = WishParams(
        serviceID: model.serviceId,
        businessServiceID: model.businessServiceID,
      );
      if (model.hasWishItem) {
        getIt<LoadingHelper>().showLoadingDialog();
        var update = await RemoveWishService()(params);
        if (update) {
          _updateCubitToWish(model);
          if (fromDialog) AutoRouter.of(context).pop();
          getIt<LoadingHelper>().dismissDialog();
        }
      } else {
        getIt<LoadingHelper>().showLoadingDialog();
        var update = await UpdateWishService()(params);
        if (update) {
          _updateCubitToWish(model);
          if (fromDialog) AutoRouter.of(context).pop();
        }
        getIt<LoadingHelper>().dismissDialog();
      }
    }
  }

  bool checkUserAuth(BuildContext context) {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (auth) {
      return true;
    } else {
      CustomToast.customAuthDialog(isSalon: false);
    }
    return false;
  }

  void _updateCubitToWish(ServiceListModel model) {
    model.hasWishItem = !model.hasWishItem;
    servicesData.servicesBloc
        .onUpdateData(servicesData.servicesBloc.state.data);
  }

  Future<void> cancelDialog(DeviceModel device, BuildContext context,
      ReservationRootData rootData) async {
    bool isTablet = device.isTablet;
    int lengthItems = servicesData.servicesBloc.state.data.length;
    CustomToast.customConfirmDialog(
        recordItem: BuildCancelItemsBody(rootData: rootData),
        heightBottom: 0,
        height: _heightCancelSheet(isTablet, lengthItems),
        onConfirm: () async {
          await AutoRouter.of(context).pop();
          AutoRouter.of(context).pop();
        },
        content: "",
        btnName: tr("cancel_reservation"),
        title: tr("cancel_reservation"));
  }

  double _heightCancelSheet(bool isTablet, int lengthItems) {
    return (isTablet ? 140.h : 130.h) +
        ((isTablet ? 90.sm : 55.sm) * (lengthItems >= 5 ? 4 : lengthItems));
  }

  String appBarTitle(int index, bool auth) {
    late String title;
    if (index == 0) {
      title = tr("select_services");
    } else if (index == 1) {
      title = tr("pick_datetime");
    } else if (index == 2 && !auth) {
      title = tr("personal_details");
    } else if (index == 3 && !auth) {
      title = tr("summery");
    } else {
      title = tr("summery");
    }
    return title;
  }
}
