part of 'reservation_details_widgets_imports.dart';

class BuildBottomSheetPromoCode extends StatelessWidget {
  final DeviceModel model;
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;

  const BuildBottomSheetPromoCode(
      {Key? key,
      required this.model,
      required this.reservationDetailsData,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<List<PromoModel>>,
        GenericState<List<PromoModel>>>(
      bloc: reservationDetailsData.promoCodesCubit,
      builder: (context, state) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(15.r),
              ),
            ),
            padding: EdgeInsets.all(15.r),
            child: Form(
              key: reservationDetailsData.promoFormKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BuildHeaderBottomSheet(
                    ladingIcon: SvgPicture.asset(
                      Res.label,
                      width: 13.h,
                      height: 13.h,
                    ),
                    margin: EdgeInsets.only(bottom: 15.h),
                    title: tr("add_promo"),
                  ),
                  CustomTextField(
                    label: tr("promo_code"),
                    controller: reservationDetailsData.promoCode,
                    margin: const EdgeInsets.symmetric(vertical: 12),
                    fieldTypes: FieldTypes.normal,
                    hint: tr("enter_promo"),
                    type: TextInputType.text,
                    onChange: (v) {
                      reservationDetailsData.promoCodesCubit.onUpdateData(
                        reservationDetailsData.promoCodesCubit.state.data,
                      );
                    },
                    action: TextInputAction.done,
                    suffixIcon: NotesIcon(),
                    validate: (value) => value?.validateEmpty(),
                  ),
                  ...List.generate(
                    state.data.length,
                    (index) => BuildPromoCodeItem(
                      model: model,
                      rootData: rootData,
                      promoModel: state.data[index],
                      reservationDetailsData: reservationDetailsData,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Expanded(
                        child: DefaultButton(
                          height: model.isTablet ? 60.sm : 50.sm,
                          borderRadius: BorderRadius.circular(50.r),
                          margin: EdgeInsets.only(bottom: 5.sm),
                          color: MyColors.primary.withOpacity(.05),
                          textColor: MyColors.primary,
                          fontSize: model.isTablet ? 7.sp : 11.sp,
                          title: tr("close"),
                          onTap: () => reservationDetailsData
                              .clearPromoCodeData(context),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: DisableWidget(
                          isDisable: _isDisableButton(),
                          child: DefaultButton(
                            height: model.isTablet ? 60.sm : 50.sm,
                            borderRadius: BorderRadius.circular(50.r),
                            margin: EdgeInsets.only(bottom: 5.sm),
                            color: MyColors.primary,
                            textColor: MyColors.white,
                            fontSize: model.isTablet ? 7.sp : 11.sp,
                            title: tr("apply"),
                            onTap: () => reservationDetailsData.makeReservation(
                              context,
                              rootData,
                              ReservationEnum.promo,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  bool _isDisableButton() {
    return (reservationDetailsData.promoCode.text ==
            reservationDetailsData.selectedPromoCode?.promoCode) ||
        reservationDetailsData.promoCode.text.isEmpty ||
        reservationDetailsData.promoCodeItemCubit.state.data != null;
  }
}
