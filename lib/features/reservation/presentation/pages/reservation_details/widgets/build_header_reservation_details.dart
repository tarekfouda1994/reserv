part of 'reservation_details_widgets_imports.dart';

class BuildHeaderReservationDetails extends StatelessWidget {
  final DeviceModel model;
  final ReservationRootData rootData;

  const BuildHeaderReservationDetails({Key? key, required this.model, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(13.r),
        border: Border.all(
          width: 2,
          color: Color(0xffD6ECEC),
        ),
      ),
      margin: EdgeInsets.only(left: 16.h, right: 16.h, bottom: 10.h, top: 16.h),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16).r,
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xffD6ECEC),
              ),
              borderRadius: BorderRadius.vertical(top: Radius.circular(11.r)),
              color: Color(0xffD6ECEC),
            ),
            width: MediaQuery.of(context).size.width,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Transform.scale(
                  scale: .9,
                  child: SvgPicture.asset(Res.appointmentActive),
                ),
                SizedBox(width: 7),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: _convertNumToAr(
                          context,
                          rootData.reservationDetailsData.formatDate(
                            rootData.dateData.dateCubit.state.data,
                            rootData.dateData.dateCubit.state.data.firstOrNull,
                            model.locale,
                          ),
                        ),
                        color: MyColors.black,
                        size: model.isTablet ? 11.sp : 14.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      const SizedBox(height: 8),
                      MyText(
                        title: _convertNumToAr(context, _getReservedTimes()),
                        fontFamily: CustomFonts.primarySemiBoldFont,
                        color: MyColors.primary,
                        size: model.isTablet ? 8.5.sp : 13.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 25).r,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(Res.building, height: 18.r, width: 18.r),
                SizedBox(width: 10.sm),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.r,
                    ),
                    Row(
                      children: [
                        MyText(
                          fontFamily: CustomFonts.primarySemiBoldFont,
                          title: rootData.servicesData.detailsCubit.state.data?.getBusinessName() ??
                              "",
                          color: MyColors.black,
                          size: model.isTablet ? 7.sp : 11.sp,
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0).r,
                      child: Row(
                        children: [
                          Offstage(
                            offstage: rootData.servicesData.detailsCubit.state.data?.rating == 0,
                            child: Row(
                              children: [
                                MyText(
                                    title: _convertNumToAr(
                                      context,
                                      "${rootData.servicesData.detailsCubit.state.data?.rating} ",
                                    ),
                                    color: MyColors.amber,
                                    size: model.isTablet ? 5.sp : 8.sp),
                                SvgPicture.asset(
                                  Res.star,
                                  color: MyColors.amber,
                                  height: 12.sp,
                                  width: 12.sp,
                                ),
                                Container(
                                  margin: EdgeInsetsDirectional.only(end: 3, start: 25.w),
                                  padding: EdgeInsets.all(4),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: MyColors.greyLight,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          MyText(
                            title:
                                rootData.servicesData.detailsCubit.state.data?.getAddressName() ==
                                        "Male"
                                    ? tr("men")
                                    : tr("women"),
                            color: MyColors.grey,
                            size: model.isTablet ? 5.5.sp : 9.sp,
                          ),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: () => AutoRouter.of(context).push(TrackingMapRoute(
                          businessName:
                              rootData.servicesData.detailsCubit.state.data!.getBusinessName(),
                          address: rootData.servicesData.detailsCubit.state.data!.getAddressName(),
                          lat: rootData.servicesData.detailsCubit.state.data!.latitude,
                          lng: rootData.servicesData.detailsCubit.state.data!.longitude)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 2.0),
                            child: SvgPicture.asset(Res.location_arrow_icon,
                                width: model.isTablet ? 5.5.w : null),
                          ),
                          Padding(
                            padding: const EdgeInsetsDirectional.only(end: 8.0, start: 5),
                            child: MyText(
                                title: tr("See_Direction"),
                                color: MyColors.blue,
                                size: model.isTablet ? 5.5.sp : 9.sp),
                          ),
                          Offstage(
                            offstage: rootData.servicesData.detailsCubit.state.data?.serviceDistance
                                    .isEmpty ??
                                true,
                            child: MyText(
                                title:
                                    rootData.servicesData.detailsCubit.state.data?.serviceDistance ??
                                        "",
                                color: MyColors.grey,
                                size: model.isTablet ? 5.5.sp : 9.sp),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _convertNumToAr(BuildContext context, String text) {
    return getIt<Utilities>().convertNumToAr(context: context, value: text);
  }

  String _getReservedTimes() {
    if (rootData.dateData.switchedCubit.state.data) {
      return _getReserveTogetherTimes();
    }
    return _getReserveManualTimes();
  }

  String _getReserveTogetherTimes() {
    var times = rootData.dateData.bestTimesCubit.state.data;
    var selected = rootData.dateData.bestTimesCubit.state.data.index;
    return times.times[selected];
  }

  String _getReserveManualTimes() {
    var times = rootData.dateData.selectedServiceCubit.state.data.map((e) => e.time).toList();
    return times.join(" - ");
  }
}
