part of 'reservation_details_widgets_imports.dart';

class BuildPromoCodeItem extends StatelessWidget {
  final DeviceModel model;
  final PromoModel promoModel;
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;

  const BuildPromoCodeItem(
      {Key? key,
      required this.model,
      required this.promoModel,
      required this.reservationDetailsData,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final percentage = promoModel.discountPercentage ?? 0;
    var discount = percentage > 0
        ? "${promoModel.discountPercentage} %"
        : promoModel.discount.toString();
    return Visibility(
      visible: rootData.servicesData.servicesBloc.state.data
          .any((element) => element.serviceId == promoModel.serviceId),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                  title: promoModel.promoCode,
                  color: MyColors.black,
                  size: model.isTablet ? 7.sp : 11.sp,
                  fontWeight: FontWeight.bold),
              SizedBox(width: 20),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      title: "$discount ${tr("discount")}",
                      color: MyColors.grey,
                      size: model.isTablet ? 6.sp : 10.sp),
                  MyText(
                    title:
                        "${promoModel.getCurrencyName()} ${promoModel.minimumSpendAmount}",
                    color: MyColors.black,
                    fontFamily: CustomFonts.primarySemiBoldFont,
                    size: model.isTablet ? 6.sp : 10.sp,
                  ),
                ],
              )),
              GestureDetector(
                onTap: () => reservationDetailsData.applyPromo(
                  context,
                  promoModel,
                  rootData,
                ),
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: MyColors.primary.withOpacity(.05),
                  ),
                  child: MyText(
                    alien: TextAlign.center,
                    size: model.isTablet ? 6.sp : 10.sp,
                    title: tr("apply"),
                    color: MyColors.primary,
                  ),
                ),
              ),
            ],
          ),
          Divider()
        ],
      ),
    );
  }
}
