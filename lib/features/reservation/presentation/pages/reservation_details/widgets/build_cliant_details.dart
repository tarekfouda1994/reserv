part of 'reservation_details_widgets_imports.dart';

class BuildClientDetails extends StatelessWidget {
  final DeviceModel model;
  final PersonalParams? personalParams;
  final ReservationDetailsData reservationDetailsData;

  const BuildClientDetails(
      {Key? key,
      required this.model,
      required this.reservationDetailsData,
      required this.personalParams})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = context.watch<UserCubit>().state.model;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: BuildHeaderTitle(title: tr("person_details"), count: 0),
        ),
        CustomCard(
          child: BlocBuilder<GenericBloc<bool>, GenericState<bool>>(
              bloc: reservationDetailsData.clientDataCubit,
              builder: (context, state) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        if (state.data == false ||
                            user?.photo == "" ||
                            personalParams?.email != "")
                          SvgPicture.asset(
                            Res.staff_default_img,
                            width: model.isTablet ? 25.w : 28.w,
                            height: model.isTablet ? 25.w : 28.w,
                          ),
                        SizedBox(
                          width: 10,
                        ),
                        MyText(
                            title: state.data
                                ? "${user?.firstName ?? personalParams?.firstName} ${user?.lastName ?? personalParams?.lastName}"
                                : "${reservationDetailsData.giftFirstName.text} ${reservationDetailsData.giftLsatName.text}",
                            color: MyColors.black,
                            size: model.isTablet ? 7.sp : 11.sp),
                      ],
                    ),
                    if ((user?.phoneNumber ?? personalParams?.phone)
                        .toString()
                        .isNotEmpty)
                      Column(
                        children: [
                          SizedBox(height: 5.h),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 9),
                                child: SvgPicture.asset(
                                  Res.smartphone,
                                  width: 16.h,
                                  height: 16.h,
                                ),
                              ),
                              Directionality(
                                textDirection:TextDirection.ltr,
                                child: MyText(
                                  title: state.data
                                      ? " ${user?.phoneNumber ?? personalParams?.phone}"
                                      : " ${reservationDetailsData.giftPhoneNumber.text}",
                                  color: MyColors.blackOpacity,
                                  size: model.isTablet ? 5.5.sp : 9.sp,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    SizedBox(height: 5.h),
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 9),
                          child: SvgPicture.asset(
                            Res.envelope,
                            width: 13.h,
                            height: 13.h,
                          ),
                        ),
                        MyText(
                          title: state.data
                              ? " ${user?.email ?? personalParams?.email}"
                              : " ${reservationDetailsData.giftEmailAddress.text}",
                          color: MyColors.blackOpacity,
                          size: model.isTablet ? 5.5.sp : 9.sp,
                        ),
                      ],
                    )
                  ],
                );
              }),
        ),
      ],
    );
  }
}
