part of 'reservation_details_widgets_imports.dart';

class BuildPaymentUserDetails extends StatelessWidget {
  final ReservationDetailsData controller;
  final ReservationRootData rootData;

  const BuildPaymentUserDetails({
    key,
    required this.controller,
    required this.rootData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<String?>, GenericState<String?>>(
        bloc: controller.finallySelectedPayTypeCubit,
        builder: (cxt, state) {
          return BuildPaymentMethodsView(
            subHeader: BuildHeaderCatReservationDetails(
              onTap: () => _onAddCardSheet(context),
              actionTitle: model.auth ? "+ ${tr("add_new_method")}" : "",
            ),
            bloc: controller.payType,
            callback: () {
              controller.finallySelectedPayTypeCubit.onUpdateData(null);
            },
            userCards: BlocBuilder<GenericBloc<List<PaymentModel>>,
                    GenericState<List<PaymentModel>>>(
                bloc: controller.paymentCubit,
                builder: (cxt, statePayment) {
                  if (statePayment is GenericUpdateState) {
                    return Visibility(
                      visible: statePayment.data.isNotEmpty,
                      child: Column(
                        children:
                            List.generate(statePayment.data.length, (index) {
                          return BuildVisaCardItem(
                            changeValue: state.data,
                            controller: controller,
                            rootData: rootData,
                            index: index,
                            paymentModel: statePayment.data[index],
                          );
                        }),
                      ),
                      replacement: Column(
                        children: [
                          BuildEmptyReservationOptionsItem(
                            title: tr("no_payment_methods_yet"),
                            image: Res.EmptyPaymentMethods,
                            subTitle: tr("methods_will_appear"),
                          ),
                          Divider(),
                        ],
                      ),
                    );
                  }
                  return Container();
                }),
          );
        });
  }

  void _onAddCardSheet(BuildContext context) {
    controller.bottomSheetAddCard(context, controller);
  }
}
