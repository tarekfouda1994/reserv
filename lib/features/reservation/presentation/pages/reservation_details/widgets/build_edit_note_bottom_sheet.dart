part of 'reservation_details_widgets_imports.dart';

class BuildEditNoteBottomSheet extends StatelessWidget {
  final DeviceModel deviceModel;
  final ReservationDetailsData reservationDetailsData;
  final String item;

  const BuildEditNoteBottomSheet(
      {Key? key,
      required this.deviceModel,
      required this.reservationDetailsData,
      required this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        padding: EdgeInsets.all(15.r),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              ladingIcon: SvgPicture.asset(
                Res.comment,
                width: 13.h,
                height: 13.h,
              ),
              margin: EdgeInsets.only(bottom: 15.h),
              title: "  ${tr("edit_note")}",
            ),
            MyText(
              title: " ${tr("notes")}",
              color: MyColors.black,
              size: 11.sp,
              fontWeight: FontWeight.bold,
            ),
            CustomTextField(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              max: 4,
              controller: reservationDetailsData.editNote,
              fieldTypes: FieldTypes.chat,
              contentPadding:
                  EdgeInsets.symmetric(vertical: 12, horizontal: 10),
              type: TextInputType.multiline,
              action: TextInputAction.done,
              validate: (value) => value?.validateEmpty(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: DefaultButton(
                    height: deviceModel.isTablet ? 60.sm : 50.sm,
                    borderRadius: BorderRadius.circular(50.r),
                    margin: EdgeInsets.only(bottom: 5.sm),
                    color: MyColors.primary.withOpacity(.05),
                    textColor: MyColors.primary,
                    fontSize: deviceModel.isTablet ? 7.sp : 11.sp,
                    title: tr("keep"),
                    onTap: () => AutoRouter.of(context).pop(),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: DefaultButton(
                    height: deviceModel.isTablet ? 60.sm : 50.sm,
                    borderRadius: BorderRadius.circular(50.r),
                    margin: EdgeInsets.only(bottom: 5.sm),
                    color: MyColors.primary,
                    textColor: MyColors.white,
                    fontSize: deviceModel.isTablet ? 7.sp : 11.sp,
                    title: tr("Edit_note"),
                    onTap: () =>
                        reservationDetailsData.editNoteItem(context, item),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
