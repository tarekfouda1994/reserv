part of 'reservation_details_widgets_imports.dart';

class BuildVisaCardItem extends StatelessWidget {
  final ReservationDetailsData controller;
  final ReservationRootData rootData;
  final PaymentModel paymentModel;
  final int index;
  final String? changeValue;

  const BuildVisaCardItem(
      {Key? key,
      required this.controller,
      required this.rootData,
      required this.paymentModel,
      required this.index,
      required this.changeValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isGuestUserCard = rootData.personalParams?.cardNumber != null;
    final value = _itemValue(isGuestUserCard);
    final model = context.watch<DeviceCubit>().state.model;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 15),
      margin: const EdgeInsets.symmetric(vertical: 3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
          color:
              value == changeValue ? MyColors.primary : MyColors.reserveDateBg,
          width: value == changeValue ? 1.5 : 1,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () => controller.selectVisaCard(paymentModel, isGuestUserCard),
            child: Row(
              children: [
                BuildRadioItem(
                  iconSize: model.isTablet ? 8.sp : 14.sp,
                  value: value,
                  margin: EdgeInsets.zero,
                  changeValue: changeValue,
                ),
                // MyText(
                //   title: tr("visa_card"),
                //   color: MyColors.black,
                //   size: model.isTablet ? 6.sp : 9.sp,
                // ),
                SvgPicture.asset(
                  paymentModel.cardNumber.startsWith("5")
                      ? Res.mastercardNew
                      : Res.visacardNew,
                ),
                SizedBox(width: 15.w),
                Row(
                  children: [
                    MyText(
                      // title: "${tr("ending_with")} ",
                      title: "****",
                      color: MyColors.grey,
                      size: model.isTablet ? 6.sp : 9.sp,
                    ),
                    MyText(
                      title: isGuestUserCard
                          ? "${rootData.personalParams?.cardNumber.substring(rootData.personalParams!.cardNumber.length - 4, rootData.personalParams?.cardNumber.length)}"
                          : " ${paymentModel.cardNumber.substring(paymentModel.cardNumber.length - 4, paymentModel.cardNumber.length)}",
                      color: MyColors.black,
                      size: model.isTablet ? 6.sp : 9.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Visibility(
            visible: model.auth,
            child: Row(
              children: [
                GestureDetector(
                  onTap: () =>
                      controller.deleteCard(context, paymentModel),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: SvgPicture.asset(Res.trash,
                        width: 15.h, height: 15.h, color: MyColors.black),
                  ),
                ),
                const SizedBox(width: 6),
                GestureDetector(
                  onTap: () => controller.bottomSheetEditCard(
                      context, model, controller, paymentModel),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: SvgPicture.asset(Res.edit,
                        width: 15.h, height: 15.h, color: MyColors.black),
                  ),
                ),
              ],
            ),
            replacement: SizedBox(width: 60.w),
          ),
        ],
      ),
    );
  }

  String _itemValue(bool isGuestUserCard) {
    return isGuestUserCard
        ? (rootData.personalParams?.cardNumber ?? "")
        : paymentModel.id;
  }
}
