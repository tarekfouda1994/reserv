part of 'reservation_details_widgets_imports.dart';

class BuildItem extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final DeviceModel model;
  final int index;
  final int length;
  final ServiceListModel serviceListModel;
  final ReservationRootData rootData;

  const BuildItem(
      {Key? key,
      required this.reservationDetailsData,
      required this.model,
      required this.serviceListModel,
      required this.rootData,
      required this.index,
      required this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
              EdgeInsets.only(left: 16.w, right: 16.w, bottom: 16.h, top: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () =>
                        reservationDetailsData.bottomSheetServiceOptions(
                      context,
                      reservationDetailsData,
                      model,
                      serviceListModel,
                      rootData,
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.w, right: 8.w, top: 8),
                      child: SvgPicture.asset(Res.dots),
                    ),
                  )
                ],
              ),
              Row(
                children: [
                    CachedImage(
                      url: serviceListModel.picture.firstOrNull?.replaceAll("\\", "/")??"",
                      width: model.isTablet ? 35.w : 50.w,
                      height: model.isTablet ? 35.w : 50.w,
                      fit: BoxFit.cover,
                      bgColor: MyColors.defaultImgBg,
                      placeHolder: ServicePlaceholder(),
                      borderRadius: BorderRadius.circular(12.r),
                    ),
                  SizedBox(width: 10.w),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                            title: serviceListModel.getServiceName(),
                            color: MyColors.primary,
                            size: model.isTablet ? 7.sp : 11.sp,
                            fontWeight: FontWeight.bold),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 5.h),
                          child: Row(
                            children: [
                              MyText(
                                title: _convertNumToAr(context,
                                    "${serviceListModel.getCurrencyName()} ${serviceListModel.price}"),
                                color: MyColors.black,
                                size: model.isTablet ? 5.5.sp : 9.sp,
                                fontFamily: CustomFonts.primarySemiBoldFont,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.access_time,
                                    color: MyColors.greyLight,
                                    size: model.isTablet ? 14.sp : 17.sp,
                                  ),
                                  SizedBox(width: 3),
                                  MyText(
                                    title: _convertNumToAr(context,
                                        "${tr("min")} ${serviceListModel.serviceDuration}"),
                                    color: MyColors.grey,
                                    size: model.isTablet ? 5.5.sp : 9.sp,
                                  ),
                                ],
                              ),
                              if (serviceListModel
                                  .getServiceGender()
                                  .isNotEmpty)
                                Expanded(
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsetsDirectional.only(
                                            end: 3, start: 25.w),
                                        padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: MyColors.greyLight,
                                        ),
                                      ),
                                      Expanded(
                                        child: MyText(
                                          title: serviceListModel
                                              .getServiceGender(),
                                          color: MyColors.grey,
                                          size: model.isTablet ? 5.5.sp : 9.sp,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            SvgPicture.asset(Res.user_icon,
                                color: MyColors.grey.withOpacity(.4),
                                width: 10.h,
                                height: 10.h),
                            MyText(
                              title: serviceListModel.staffName.isNotEmpty
                                  ? " ${serviceListModel.staffName}"
                                  : tr("any_one"),
                              color: MyColors.grey,
                              size: model.isTablet ? 5.5.sp : 9.sp,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Visibility(
          visible: (index + 1) != length,
          child: Divider(
            height: 0,
          ),
        )
      ],
    );
  }

  String _convertNumToAr(BuildContext context, String text) =>
      getIt<Utilities>().convertNumToAr(context: context, value: text);
}
