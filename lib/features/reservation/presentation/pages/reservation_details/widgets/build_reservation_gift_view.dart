part of 'reservation_details_widgets_imports.dart';

class BuildReservationGiftView extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;
  final DeviceModel deviceModel;

  const BuildReservationGiftView(
      {Key? key,
      required this.reservationDetailsData,
      required this.deviceModel,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<GiftEntity?>, GenericState<GiftEntity?>>(
        bloc: reservationDetailsData.giftCubit,
        builder: (context, state) {
          return Column(
            children: [
              BuildHeaderCatReservationDetails(
                title: tr("Send_As_Gift"),
                onTap: () {
                  if (state.data != null) {
                    reservationDetailsData.giftCubit.onUpdateData(null);
                  } else {
                    reservationDetailsData.bottomSheetSendAsGift(
                        context, deviceModel, reservationDetailsData, rootData);
                  }
                },
                actionTitle: state.data != null
                    ? "- ${tr("remove_beneficiary")}"
                    : "+ ${tr("Add_Beneficiary")}",
                actionTitleColor:
                    state.data != null ? MyColors.errorColor : MyColors.primary,
              ),
              CustomCard(
                child: Visibility(
                  visible: state.data != null,
                  child: InkWell(
                    onTap: (){
                      reservationDetailsData.bottomSheetSendAsGift(
                          context, deviceModel, reservationDetailsData, rootData);
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(
                              Res.defaultProfile,
                              width: deviceModel.isTablet ? 28.w : 35.w,
                              height: deviceModel.isTablet ? 28.w : 35.w,
                            ),
                            SizedBox(width: 10),
                            MyText(
                                title:
                                    "${state.data?.applicantFirstName} ${state.data?.applicantLastName}",
                                color: MyColors.black,
                                size: deviceModel.isTablet ? 7.sp : 11.sp),
                          ],
                        ),
                        SizedBox(height: 5.h),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.h),
                              child: SvgPicture.asset(
                                Res.smartphone,
                                width: 16.h,
                                height: 16.h,
                              ),
                            ),
                            MyText(
                                title: " ${state.data?.applicantMobile}",
                                color: MyColors.blackOpacity,
                                size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                          ],
                        ),
                        SizedBox(height: 5.h),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.h),
                              child: SvgPicture.asset(
                                Res.envelope,
                                width: 13.h,
                                height: 13.h,
                              ),
                            ),
                            MyText(
                                title: " ${state.data?.applicantEmail}",
                                color: MyColors.blackOpacity,
                                size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                          ],
                        )
                      ],
                    ),
                  ),
                  replacement: BuildEmptyReservationOptionsItem(
                    title: tr("surprise_your_loved_ones"),
                    image: Res.EmptyGift,
                    subTitle: tr("with_great_salon_experience"),
                  ),
                ),
              ),
            ],
          );
        });
  }
}
