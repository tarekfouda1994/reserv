part of 'reservation_details_widgets_imports.dart';

class BuildReservationDetailsBottom extends StatelessWidget {
  final DeviceModel model;
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData reservationRoot;

  final List<ServiceListModel> services;

  const BuildReservationDetailsBottom({
    Key? key,
    required this.model,
    required this.reservationDetailsData,
    required this.reservationRoot,
    required this.services,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: MyColors.white, boxShadow: [
        BoxShadow(
          color: MyColors.greyWhite,
          spreadRadius: 2,
          blurRadius: 2,
        )
      ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Divider(height: 0),
          Padding(
            padding: EdgeInsetsDirectional.only(
                bottom: 10, start: 25, top: 10, end: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BuildCancelReservationButton(rootData: reservationRoot),
                BlocBuilder<GenericBloc<num>, GenericState<num>>(
                  bloc: reservationRoot.servicesData.totalBloc,
                  builder: (context, state) {
                    return InkWell(
                      onTap: () => reservationDetailsData.makeReservation(
                          context,
                          reservationRoot,
                          ReservationEnum.makeReservation),
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        height: 40.h,
                        decoration: BoxDecoration(
                            color: MyColors.primary,
                            borderRadius: BorderRadius.circular(40.r)),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 14, vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            MyText(
                                title:
                                    "${tr("pay")} ${services.firstOrNull?.getCurrencyName()} ${getIt<Utilities>().convertNumToAr(context: context, value: state.data.toStringAsFixed(1))}  ",
                                color: MyColors.white,
                                size: model.isTablet ? 7.sp : 11.sp),
                            BuildMyArrowIcon(),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
