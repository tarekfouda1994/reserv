part of 'reservation_details_widgets_imports.dart';

class BuildReservationBookedServices extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;
  final DeviceModel deviceModel;
  final List<ServiceListModel> listServices;

  const BuildReservationBookedServices(
      {Key? key,
      required this.reservationDetailsData,
      required this.deviceModel,
      required this.listServices,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: BuildHeaderTitle(
              title: tr("Booked_Services"), count: listServices.length),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8.0),
          decoration: BoxDecoration(
            color: MyColors.white,
            borderRadius: BorderRadius.circular(13.r),
            border: Border.all(
              width: .1,
              color: MyColors.grey.withOpacity(.5),
            ),
          ),
          child: Column(
            children: List.generate(
              listServices.length,
              (index) => BuildItem(
                reservationDetailsData: reservationDetailsData,
                model: deviceModel,
                serviceListModel: listServices[index],
                rootData: rootData,
                length: listServices.length,
                index: index,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
