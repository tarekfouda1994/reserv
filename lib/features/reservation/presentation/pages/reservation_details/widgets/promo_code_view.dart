part of 'reservation_details_widgets_imports.dart';

class BuildPromoCodeView extends StatelessWidget {
  final DeviceModel deviceModel;
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;

  const BuildPromoCodeView(
      {Key? key,
      required this.deviceModel,
      required this.reservationDetailsData,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<PromoModel?>, GenericState<PromoModel?>>(
        bloc: reservationDetailsData.promoCodeItemCubit,
        builder: (cxt, state) {
          return Column(
            children: [
              BuildHeaderCatReservationDetails(
                title: tr("promo_code"),
                onTap: () => reservationDetailsData.bottomSheetPromoCode(
                    context, deviceModel, reservationDetailsData, rootData),
                actionTitle: reservationDetailsData.promoCode.text.isNotEmpty
                    ? tr("apply_different_code")
                    : "+ ${tr("add_promo_code")}",
              ),
              CustomCard(
                  child: Visibility(
                visible: state.data != null,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        MyText(
                            title: "${tr("Applied_Code")}:",
                            fontWeight: FontWeight.bold,
                            color: MyColors.black,
                            size: deviceModel.isTablet ? 7.sp : 11.sp),
                        SizedBox(width: 10),
                        MyText(
                            title: reservationDetailsData.promoCode.text,
                            fontWeight: FontWeight.bold,
                            color: MyColors.primary,
                            size: deviceModel.isTablet ? 5.sp : 9.sp),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 15),
                      padding: EdgeInsets.symmetric(
                          horizontal: deviceModel.isTablet ? 14.w : 16.w),
                      decoration: BoxDecoration(
                          color: Color(0xffD6ECEC),
                          borderRadius: BorderRadius.circular(10.r)),
                      child: Row(
                        children: [
                          Transform.scale(
                            scale: deviceModel.isTablet ? 1.7 : 1,
                            child: Checkbox(
                                splashRadius: 100,
                                activeColor: MyColors.primary,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(100)),
                                value: true,
                                onChanged: (value) {}),
                          ),
                          MyText(
                              title: tr("congrats_saved"),
                              color: MyColors.primary,
                              size: deviceModel.isTablet ? 6.sp : 9.sp),
                          MyText(
                              title:
                                  " AED ${rootData.servicesData.servicesBloc.state.data.fold(0, (num prev, e) => prev + e.price) - rootData.servicesData.totalBloc.state.data}",
                              color: MyColors.primary,
                              size: deviceModel.isTablet ? 6.sp : 9.sp,
                              fontWeight: FontWeight.bold),
                        ],
                      ),
                    ),
                  ],
                ),
                replacement: BuildEmptyReservationOptionsItem(
                  title: tr("promo_not_applied_yet"),
                  image: Res.EmptyPromoCode,
                  subTitle: tr("promo_appear_here"),
                ),
              )),
            ],
          );
        });
  }
}
