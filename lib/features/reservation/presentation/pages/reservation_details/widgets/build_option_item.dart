part of 'reservation_details_widgets_imports.dart';

class BuildOptionItem extends StatelessWidget {
  final String title;
  final String img;
  final DeviceModel model;
  final Function()? onTap;

  const BuildOptionItem(
      {Key? key, required this.title, this.onTap, required this.img, required this.model})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(children: [
              SvgPicture.asset(img, width: 16.h, height: 16.h),
              MyText(
                title: title,
                color: MyColors.black,
                size: model.isTablet ? 6.sp : 9.sp,
                fontWeight: FontWeight.bold,
              ),
              Spacer(),
              Icon(
                Icons.add,
                size: model.isTablet ? 12.sp : 18.sp,
                color: MyColors.grey.withOpacity(.5),
              )
            ]),
          ),
        ),
        Divider(height: 20.h),
      ],
    );
  }
}
