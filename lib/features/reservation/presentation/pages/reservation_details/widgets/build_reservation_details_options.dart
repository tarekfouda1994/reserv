part of 'reservation_details_widgets_imports.dart';

class BuildReservationDetailsOptions extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final DeviceModel model;
  final ReservationRootData rootData;

  const BuildReservationDetailsOptions(
      {Key? key, required this.reservationDetailsData, required this.model, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BuildOptionItem(
          img: Res.comment,
          title: "  ${tr("add_note_for_salon")}",
          onTap: () =>
              reservationDetailsData.bottomSheetNotes(context, model, reservationDetailsData),
          model: model,
        ),
        BuildOptionItem(
          img: Res.gift,
          title: "  ${tr("Send_As_Gift")}",
          onTap: () => reservationDetailsData.bottomSheetSendAsGift(
              context, model, reservationDetailsData, rootData),
          model: model,
        ),
      ],
    );
  }
}
