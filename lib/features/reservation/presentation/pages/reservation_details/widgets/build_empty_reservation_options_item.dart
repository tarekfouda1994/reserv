part of 'reservation_details_widgets_imports.dart';

class BuildEmptyReservationOptionsItem extends StatelessWidget {
  final String image;
  final String title;
  final String subTitle;
  const BuildEmptyReservationOptionsItem({Key? key, required this.image, required this.title, required this.subTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;

    return Row(
      children: [
        SvgPicture.asset(image),
        SizedBox(width: 6.w),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            MyText(
              title: title,
              color: MyColors.black,
              size: device.isTablet ? 6.sp : 9.sp,
            ),
            SizedBox(height: 6.h),
            MyText(
              title: subTitle,
              color: MyColors.grey,
              size: device.isTablet ? 6.sp : 9.sp,
            )
          ],
        )
      ],
    );
  }
}
