part of 'reservation_details_widgets_imports.dart';

class BuildPayDetails extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;
  final DeviceModel model;

  const BuildPayDetails(
      {Key? key,
      required this.reservationDetailsData,
      required this.model,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _paymentDetails(context);
  }

  Widget _paymentDetails(BuildContext context) {
    bool isAuth = context.watch<DeviceCubit>().state.model.auth;
    if (isAuth) {
      return BuildPaymentUserDetails(
        controller: reservationDetailsData,
        rootData: rootData,
      );
    } else {
      return BuildGustUserPaymentDetails(
        rootData: rootData,
      );
    }
  }
}
