part of 'reservation_details_widgets_imports.dart';

class BuildAddVisaCardBottomSheet extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;

  const BuildAddVisaCardBottomSheet(
      {Key? key, required this.reservationDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
final model = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15.r),
          ),
          color: MyColors.white,
        ),
        height: MediaQuery.of(context).size.height * .5.h,
        padding: EdgeInsets.all(15.r),
        child: Form(
          key: reservationDetailsData.addCardFormKey,
          child: Column(
            children: [
              BuildHeaderBottomSheet(
                margin: EdgeInsets.only(bottom: 15.h),
                title: tr("add_new_card"),
              ),
              Flexible(
                child: ListView(
                  children: [
                    const SizedBox(height: 20),
                    CustomTextField(
                      fillColor: Color(0xfff6f9f9),
                      label: tr("holder_name"),
                      controller: reservationDetailsData.newCardName,
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      fieldTypes: FieldTypes.normal,
                      type: TextInputType.name,
                      action: TextInputAction.next,
                      suffixIcon: UserIcon(),
                      validate: (value) => value?.validateEmpty(),
                    ),
                    CustomTextField(
                      label: tr("card_number"),
                      controller: reservationDetailsData.cardNumber,
                      fieldTypes: FieldTypes.normal,
                      type: TextInputType.number,
                      action: TextInputAction.next,
                      validate: (value) => value?.validateCard(context),
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(16),
                        if (model.locale.languageCode == "en")
                          CardNumberFormatter(),
                      ],
                      hint: 'XXXX XXXX XXXX XXXX',
                      suffixIcon: BuildSuffixIcon(asset: Res.credit_card),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: model.isTablet ? 10.sp : 8.sp,
                          horizontal: model.isTablet ? 8.sp : 12.w),
                      margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 4,
                          child: CustomTextField(
                            fillColor: Color(0xffF6F9F9),
                            controller: reservationDetailsData.expiryDate,
                            margin: EdgeInsets.only(top: 10, bottom: 15.h),
                            fieldTypes: FieldTypes.normal,
                            type: TextInputType.datetime,
                            hint:
                                "${tr("ex")} (${DateFormat("MM/yyyy").format(DateTime.now())})",
                            label: tr("expiry_date"),
                            action: TextInputAction.next,
                            inputFormatters: [
                              CardExpirationFormatter(),
                              LengthLimitingTextInputFormatter(7),
                            ],
                            validate: (value) => value?.validateExpiryDate(),
                            suffixIcon: CalendarIcon(),
                          ),
                        ),
                        SizedBox(width: model.isTablet ? 15.sp : 10),
                        Expanded(
                          flex: 3,
                          child: CustomTextField(
                            controller: reservationDetailsData.cvvController,
                            fieldTypes: FieldTypes.password,
                            type: TextInputType.number,
                            action: TextInputAction.done,
                            label: tr("CVV"),
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(3),
                            ],
                            validate: (value) => value?.validateCVV(),
                            hint: "xxx",
                            maxLength: 3,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: model.isTablet ? 10.sp : 8.sp,
                                horizontal: model.isTablet ? 8.sp : 12.w),
                            margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: DefaultButton(
                      height: model.isTablet ? 60.sm : 50.sm,
                      borderRadius: BorderRadius.circular(50.sp),
                      margin: EdgeInsets.only(bottom: 5.sm),
                      color: MyColors.primary.withOpacity(.05),
                      textColor: MyColors.primary,
                      fontSize: model.isTablet ? 7.sp : 11.sp,
                      title: tr("close"),
                      onTap: () =>
                          reservationDetailsData.clearCardData(context),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    child: DefaultButton(
                      height: model.isTablet ? 60.sm : 50.sm,
                      borderRadius: BorderRadius.circular(50.sp),
                      margin: EdgeInsets.only(bottom: 5.sm),
                      color: MyColors.primary,
                      textColor: MyColors.white,
                      fontSize: model.isTablet ? 7.sp : 11.sp,
                      title: '+ ${tr("add_card")}',
                      onTap: () => reservationDetailsData.addCard(context),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
