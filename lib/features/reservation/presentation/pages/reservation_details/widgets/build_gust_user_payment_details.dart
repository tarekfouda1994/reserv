part of 'reservation_details_widgets_imports.dart';

class BuildGustUserPaymentDetails extends StatelessWidget {
  final ReservationRootData rootData;

  const BuildGustUserPaymentDetails({key, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isCard = rootData.personalParams!.paymentType == PaymentType.creditCard;
    return Column(
      children: [
        BuildHeaderCatReservationDetails(
          title: tr("payment"),
        ),
        BlocBuilder<GenericBloc<List<PaymentModel>>,
                GenericState<List<PaymentModel>>>(
            bloc: rootData.reservationDetailsData.paymentCubit,
            builder: (context, state) {
              return CustomCard(
                padding: !isCard ? EdgeInsets.zero : null,
                child: _paymentDetails(state.data.first),
              );
            }),
      ],
    );
  }

  Widget _paymentDetails(PaymentModel paymentModel) {
    if (rootData.personalParams!.paymentType == PaymentType.creditCard) {
      return BuildVisaCardItem(
        changeValue: rootData.personalParams?.cardNumber ?? "",
        controller: rootData.reservationDetailsData,
        rootData: rootData,
        index: 0,
        paymentModel: paymentModel,
      );
    } else {
      return BuildPayTypeItem(
        margin: EdgeInsets.zero,
        title: rootData.personalDetailsData.getPaymentTitle(),
        image: rootData.personalDetailsData.getPaymentImage(),
        value: rootData.personalParams!.paymentType.index,
        changeValue: rootData.personalParams!.paymentType.index,
      );
    }
  }
}
