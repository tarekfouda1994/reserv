part of 'reservation_details_widgets_imports.dart';

class BuildNotesBottomSheet extends StatelessWidget {
  final DeviceModel model;
  final ReservationDetailsData reservationDetailsData;

  const BuildNotesBottomSheet(
      {Key? key, required this.model, required this.reservationDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.r),
        ),
      ),
      child: Container(
        padding: EdgeInsets.all(15.r),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BuildHeaderBottomSheet(
              ladingIcon: SvgPicture.asset(
                Res.comment,
                width: 13.h,
                height: 13.h,
              ),
              margin: EdgeInsets.only(bottom: 15.h),
              title: " ${tr("add_note_for_salon")}",
            ),
            MyText(
              title: tr("notes"),
              color: MyColors.black,
              size: model.isTablet ? 7.sp : 11.sp,
              fontWeight: FontWeight.bold,
            ),
            CustomTextField(
              controller: reservationDetailsData.noteText,
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              max: 3,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              fieldTypes: FieldTypes.rich,
              type: TextInputType.text,
              action: TextInputAction.done,
              validate: (value) {},
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Expanded(
                  child: DefaultButton(
                    height: model.isTablet ? 60.sm : 50.sm,
                    borderRadius: BorderRadius.circular(50.r),
                    margin: EdgeInsets.only(bottom: 5.sm),
                    color: MyColors.primary.withOpacity(.05),
                    textColor: MyColors.primary,
                    fontSize: model.isTablet ? 7.sp : 11.sp,
                    title: tr("close"),
                    onTap: () => reservationDetailsData.clearNote(context),
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: DefaultButton(
                    height: model.isTablet ? 60.sm : 50.sm,
                    borderRadius: BorderRadius.circular(50.r),
                    margin: EdgeInsets.only(bottom: 5.sm),
                    color: MyColors.primary,
                    textColor: MyColors.white,
                    fontSize: model.isTablet ? 7.sp : 11.sp,
                    title: tr("add_note"),
                    onTap: () => reservationDetailsData.addNotes(context),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
