part of 'reservation_details_widgets_imports.dart';

class BuildEditVisaCardBottomSett extends StatelessWidget {
  final PaymentModel paymentModel;
  final DeviceModel deviceModel;
  final ReservationDetailsData reservationDetailsData;

  const BuildEditVisaCardBottomSett(
      {Key? key,
      required this.paymentModel,
      required this.deviceModel,
      required this.reservationDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15.r),
          ),
        ),
        padding: EdgeInsets.all(15.r),
        child: Form(
          key: reservationDetailsData.updateFormKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BuildHeaderBottomSheet(
                margin: EdgeInsets.only(bottom: 15.h),
                title: tr("edit_card"),
              ),
              CustomTextField(
                label: tr("holder_name"),
                controller: reservationDetailsData.editHolderName,
                margin: const EdgeInsets.symmetric(vertical: 12),
                fieldTypes: FieldTypes.normal,
                hint: tr("holder_name"),
                type: TextInputType.name,
                action: TextInputAction.next,
                suffixIcon: UserIcon(),
                validate: (value) => value?.validateEmpty(),
              ),
              CustomTextField(
                label: tr("card_number"),
                controller: reservationDetailsData.editCardNumber,
                fieldTypes: FieldTypes.normal,
                type: TextInputType.number,
                action: TextInputAction.next,
                validate: (value) => value?.validateCard(context),
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(16),
                  if (deviceModel.locale.languageCode == "en")
                    CardNumberFormatter(),
                ],
                hint: 'XXXX XXXX XXXX XXXX',
                suffixIcon: BuildSuffixIcon(asset: Res.credit_card),
                contentPadding: EdgeInsets.symmetric(
                    vertical: deviceModel.isTablet ? 10.sp : 8.sp,
                    horizontal: deviceModel.isTablet ? 8.sp : 12.w),
                margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 4,
                    child: CustomTextField(
                      fillColor: Color(0xffF6F9F9),
                      controller: reservationDetailsData.editExpiryDate,
                      margin: EdgeInsets.only(top: 10, bottom: 15.h),
                      fieldTypes: FieldTypes.normal,
                      type: TextInputType.datetime,
                      hint:
                          "${tr("ex")} (${DateFormat("MM/yyyy").format(DateTime.now())})",
                      label: tr("expiry_date"),
                      action: TextInputAction.next,
                      inputFormatters: [
                        CardExpirationFormatter(),
                        LengthLimitingTextInputFormatter(7),
                      ],
                      validate: (value) => value?.validateExpiryDate(),
                      suffixIcon: CalendarIcon(),
                    ),
                  ),
                  SizedBox(width: deviceModel.isTablet ? 15.sp : 10),
                  Expanded(
                    flex: 3,
                    child: CustomTextField(
                      controller: reservationDetailsData.editCvv,
                      fieldTypes: FieldTypes.password,
                      type: TextInputType.number,
                      action: TextInputAction.done,
                      label: tr("cvv"),
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(3),
                      ],
                      validate: (value) => value?.validateCVV(),
                      hint: "xxx",
                      maxLength: 3,
                      contentPadding: EdgeInsets.symmetric(
                        vertical: deviceModel.isTablet ? 10.sp : 8.sp,
                        horizontal: deviceModel.isTablet ? 8.sp : 12.w,
                      ),
                      margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: DefaultButton(
                      height: deviceModel.isTablet ? 60.sm : 50.sm,
                      borderRadius: BorderRadius.circular(50.r),
                      margin: EdgeInsets.only(bottom: 5.sm),
                      color: MyColors.primary.withOpacity(.05),
                      textColor: MyColors.primary,
                      fontSize: deviceModel.isTablet ? 7.sp : 11.sp,
                      title: tr("cancel"),
                      onTap: () => AutoRouter.of(context).pop(),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    child: DefaultButton(
                      height: deviceModel.isTablet ? 60.sm : 50.sm,
                      borderRadius: BorderRadius.circular(50.r),
                      margin: EdgeInsets.only(bottom: 5.sm),
                      color: MyColors.primary,
                      textColor: MyColors.white,
                      fontSize: deviceModel.isTablet ? 7.sp : 11.sp,
                      title: tr("edit_card"),
                      onTap: () => reservationDetailsData.updateCard(
                          context, paymentModel),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
