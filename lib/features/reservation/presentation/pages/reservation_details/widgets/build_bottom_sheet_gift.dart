part of 'reservation_details_widgets_imports.dart';

class BuildAsGiftBottomSheet extends StatelessWidget {
  final DeviceModel model;
  final ReservationRootData rootData;
  final ReservationDetailsData reservationDetailsData;

  const BuildAsGiftBottomSheet(
      {Key? key,
      required this.model,
      required this.reservationDetailsData,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        height: MediaQuery.of(context).size.height * .61.h,
        decoration: BoxDecoration(
          color: MyColors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(15),
          ),
        ),
        padding: EdgeInsets.all(15.r),
        child: Form(
          key: reservationDetailsData.giftFormKey,
          child: Column(
            children: [
              BuildHeaderBottomSheet(
                margin: EdgeInsets.only(bottom: 15.h),
                title: tr("send_gift"),
              ),
              Flexible(
                  child: ListView(
                children: [
                  CustomTextField(
                    label: tr("reservation_firstname"),
                    controller: reservationDetailsData.giftFirstName,
                    margin: const EdgeInsets.only(top: 10, bottom: 16),
                    fieldTypes: FieldTypes.normal,
                    type: TextInputType.name,
                    action: TextInputAction.next,
                    suffixIcon: UserIcon(),
                    validate: (value) => value?.validateEmpty(),
                  ),
                  CustomTextField(
                    label: tr("reservation_lastname"),
                    controller: reservationDetailsData.giftLsatName,
                    margin: const EdgeInsets.only(top: 10, bottom: 16),
                    fieldTypes: FieldTypes.normal,
                    type: TextInputType.name,
                    action: TextInputAction.next,
                    suffixIcon: UserIcon(),
                    validate: (value) => value?.validateEmpty(),
                  ),
                  CustomTextField(
                    label: tr("email"),
                    controller: reservationDetailsData.giftEmailAddress,
                    margin: const EdgeInsets.only(top: 10, bottom: 16),
                    fieldTypes: FieldTypes.normal,
                    type: TextInputType.emailAddress,
                    action: TextInputAction.next,
                    suffixIcon: EmailIcon(),
                    validate: (value) => value?.validateEmail(),
                  ),
                  CustomTextField(
                    label: tr("phone"),
                    controller: reservationDetailsData.giftPhoneNumber,
                    margin: const EdgeInsets.only(top: 10, bottom: 16),
                    fieldTypes: FieldTypes.normal,
                    type: TextInputType.phone,
                    action: TextInputAction.next,
                    suffixIcon: PhoneIcon(),
                    validate: (value) => value?.validatePhone(),
                  ),
                  CustomTextField(
                    label: tr("wishes"),
                    max: 3,
                    controller: reservationDetailsData.wishes,
                    margin: const EdgeInsets.only(top: 10, bottom: 16),
                    fieldTypes: FieldTypes.rich,
                    type: TextInputType.text,
                    action: TextInputAction.next,
                    suffixIcon: NotesIcon(),
                    validate: (value) => value?.validateEmpty(),
                  ),
                ],
              )),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: DefaultButton(
                      height: model.isTablet ? 60.sm : 50.sm,
                      borderRadius: BorderRadius.circular(50.r),
                      margin: EdgeInsets.only(bottom: 5.sm),
                      color: MyColors.primary.withOpacity(.05),
                      textColor: MyColors.primary,
                      fontSize: model.isTablet ? 7.sp : 11.sp,
                      title: tr("keep"),
                      onTap: () => AutoRouter.of(context).pop(),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    child: DefaultButton(
                      height: model.isTablet ? 60.sm : 50.sm,
                      borderRadius: BorderRadius.circular(50.r),
                      margin: EdgeInsets.only(bottom: 5.sm),
                      color: MyColors.primary,
                      textColor: MyColors.white,
                      fontSize: model.isTablet ? 7.sp : 11.sp,
                      title: tr("send_gift"),
                      onTap: () => reservationDetailsData.addGift(context),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
