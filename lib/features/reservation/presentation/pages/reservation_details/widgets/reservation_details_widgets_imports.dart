import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/build_header_bottom_sheet.dart';
import 'package:flutter_tdd/core/widgets/build_my_arrow_icon.dart';
import 'package:flutter_tdd/core/widgets/build_radio_item.dart';
import 'package:flutter_tdd/core/widgets/card_expiration_formatter.dart';
import 'package:flutter_tdd/core/widgets/card_number_formatter.dart';
import 'package:flutter_tdd/core/widgets/custom_card.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/core/widgets/disable/disable_widget.dart';
import 'package:flutter_tdd/core/widgets/images_place_holders/service_placeholder.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/calendar_icon.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/email_icon.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/notes_icon.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/phone_icon.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/user_icon.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';
import 'package:flutter_tdd/features/reservation/data/enums/payment_enum.dart';
import 'package:flutter_tdd/features/reservation/data/models/promo_model/promo_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/gift_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/personal_paroms.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/personal_details/widgets/personal_details_widgets_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/widgets/reservation_root_widgets_imports.dart';
import 'package:flutter_tdd/features/search/presentation/pages/product_details/widgets/product_details_widgets_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:intl/src/intl/date_format.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/localization/localization_methods.dart';
import '../../../../../auth/presentation/manager/user_cubit/user_cubit.dart';
import '../../../../data/models/service_list_model/service_list_model.dart';
import '../../reservation_root/reservation_root_imports.dart';
import '../reservation_details_imports.dart';

part 'build_add_card_bottom_sheet.dart';
part 'build_bottom_sheet_gift.dart';
part 'build_bottom_sheet_promo_code.dart';
part 'build_bottom_sheet_service_options.dart';
part 'build_cash_item.dart';
part 'build_cliant_details.dart';
part 'build_edit_card_bottom_sheet.dart';
part 'build_edit_note_bottom_sheet.dart';
part 'build_empty_reservation_options_item.dart';
part 'build_gust_user_payment_details.dart';
part 'build_header_cat_reservation_details.dart';
part 'build_header_reservation_details.dart';
part 'build_item.dart';
part 'build_notes_bottom_sheet.dart';
part 'build_option_item.dart';
part 'build_pay_details.dart';
part 'build_payment_user_details.dart';
part 'build_promo_code_item.dart';
part 'build_reservation_booked_services.dart';
part 'build_reservation_details_bottom.dart';
part 'build_reservation_details_options.dart';
part 'build_reservation_gift_view.dart';
part 'build_reservation_note_view.dart';
part 'build_service_options_item.dart';
part 'build_visa_card_item.dart';
part 'promo_code_view.dart';
