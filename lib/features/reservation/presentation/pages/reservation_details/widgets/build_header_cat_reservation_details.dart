part of 'reservation_details_widgets_imports.dart';

class BuildHeaderCatReservationDetails extends StatelessWidget {
  final String? actionTitle;
  final String? title;
  final void Function()? onTap;
  final Color? actionTitleColor;

  const BuildHeaderCatReservationDetails(
      {Key? key,
      this.actionTitle,
      this.title,
      this.onTap,
      this.actionTitleColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: const EdgeInsetsDirectional.only(start: 5.0, end: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (title != null) BuildHeaderTitle(title: title!, count: 0),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: InkWell(
                onTap: onTap,
                child: MyText(
                  title: actionTitle ?? "",
                  color: actionTitleColor ?? MyColors.primary,
                  fontWeight: FontWeight.bold,
                  size: device.isTablet ? 5.sp : 9.sp,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
