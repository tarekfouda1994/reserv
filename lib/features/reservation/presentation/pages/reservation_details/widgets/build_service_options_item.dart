part of 'reservation_details_widgets_imports.dart';

class BuildServiceOptionsItem extends StatelessWidget {
  final String title;
  final String subTitle;
  final String? image;
  final Color? imageColor;
  final Widget? icon;
  final void Function()? onTap;

  const BuildServiceOptionsItem(
      {Key? key,
      required this.title,
      required this.subTitle,
      this.image,
      this.imageColor,
      this.icon,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return InkWell(
      onTap: onTap,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xffF2F6F6),
            ),
            child: icon ??
                SvgPicture.asset(
                  image ?? "",
                  color: imageColor,
                  width: 14.w,
                  height: 14.h,
                ),
          ),
          SizedBox(width: 5.w),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MyText(
                    title: title,
                    color: MyColors.black,
                    size: device.isTablet ? 6.sp : 10.sp),
                MyText(
                    title: subTitle,
                    color: MyColors.blackOpacity,
                    size: device.isTablet ? 6.sp : 10.sp),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
