part of 'reservation_details_widgets_imports.dart';

class BuildCashItem extends StatelessWidget {
  final ReservationDetailsData controller;
  final String? changeValue;

  const BuildCashItem(
      {Key? key,
      required this.controller,
      required this.changeValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = context.watch<DeviceCubit>().state.model;
    return Visibility(
      visible: model.auth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () => controller.finallySelectedPayTypeCubit.onUpdateData(null),
            child: Row(
              children: [
                BuildRadioItem(
                  iconSize: model.isTablet ? 8.sp : 14.sp,
                  value: null,
                  changeValue: changeValue,
                  // onTap: () =>
                  //     controller.checkBoxBloc.onUpdateData(0),
                ),
                MyText(
                  title: tr("cash"),
                  color: MyColors.black,
                  size: model.isTablet ? 6.sp : 9.sp,
                ),
                SizedBox(width: 40.w),
                MyText(
                  title: tr("book_now_pay_later"),
                  color: MyColors.grey,
                  size: model.isTablet ? 6.sp : 9.sp,
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 50,
          ),
        ],
      ),
    );
  }
}
