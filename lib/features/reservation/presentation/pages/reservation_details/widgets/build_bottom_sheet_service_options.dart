part of 'reservation_details_widgets_imports.dart';

class BuildBottomSheetServiceOptions extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final ReservationRootData rootData;
  final DeviceModel deviceModel;
  final ServiceListModel serviceListModel;

  const BuildBottomSheetServiceOptions(
      {Key? key,
      required this.reservationDetailsData,
      required this.deviceModel,
      required this.serviceListModel,
      required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyText(
                      title: serviceListModel.getServiceName(),
                      color: MyColors.primary,
                      size: deviceModel.isTablet ? 7.sp : 11.sp,
                      fontWeight: FontWeight.bold),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5.h),
                    child: Row(
                      children: [
                        Icon(
                          Icons.access_time,
                          color: MyColors.greyLight,
                          size: deviceModel.isTablet ? 14.sp : 17.sp,
                        ),
                        SizedBox(width: 3),
                        MyText(
                          title:
                              "${tr("min")} ${getIt<Utilities>().convertNumToAr(context: context, value: serviceListModel.serviceDuration.toString())}",
                          color: MyColors.grey,
                          size: deviceModel.isTablet ? 6.sp : 10.sp,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () => AutoRouter.of(context).pop(),
              child: Container(
                padding: EdgeInsets.all(6.r),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: MyColors.black,
                ),
                child: Icon(
                  Icons.clear,
                  size: deviceModel.isTablet ? 14.sp : 19.sp,
                  color: MyColors.white,
                ),
              ),
            ),
          ],
        ),
        BuildServiceOptionsItem(
          title: serviceListModel.hasWishItem
              ? tr("remove_favourite")
              : tr("move_favourite"),
          subTitle: serviceListModel.hasWishItem
              ? tr("remove_favorite_and_appointment")
              : tr("add_favorite_and_appointment"),
          onTap: () => rootData.updateWish(
            context,
            serviceListModel,
            true,
          ),
          image: serviceListModel.hasWishItem
              ? Res.favoritesActive
              : Res.Favorites,
          imageColor: MyColors.primary,
        ),
        Divider(height: 5),
        if (!rootData.dateData.switchedCubit.state.data)
          BuildServiceOptionsItem(
            title: tr("Remove"),
            subTitle: tr("definitely_remove_appointment"),
            onTap: () => reservationDetailsData.deleteService(
              context,
              rootData,
              serviceListModel,
            ),
            icon: Icon(Icons.clear,
                color: MyColors.errorColor,
                size: deviceModel.isTablet ? 12.sp : 17.sp),
            imageColor: MyColors.primary,
          ),
      ]),
    );
  }
}
