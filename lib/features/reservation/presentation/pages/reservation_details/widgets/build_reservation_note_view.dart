part of 'reservation_details_widgets_imports.dart';

class BuildReservationNoteView extends StatelessWidget {
  final ReservationDetailsData reservationDetailsData;
  final DeviceModel deviceModel;

  const BuildReservationNoteView(
      {Key? key,
      required this.reservationDetailsData,
      required this.deviceModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BuildHeaderCatReservationDetails(
          title: tr("notes"),
          onTap: () => reservationDetailsData.bottomSheetNotes(
              context, deviceModel, reservationDetailsData),
          actionTitle: " + ${tr("add_note_for_salon")}",
        ),
        BlocBuilder<GenericBloc<List<String>>, GenericState<List<String>>>(
            bloc: reservationDetailsData.notesCubit,
            builder: (context, state) {
              return CustomCard(
                child: Visibility(
                  visible: state.data.isNotEmpty,
                  child: Column(children: [
                    ...List.generate(state.data.length, (index) {
                      return Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 16),
                              child: MyText(
                                  title: state.data[index],
                                  color: MyColors.blackOpacity,
                                  size: deviceModel.isTablet ? 5.5.sp : 9.sp),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 25.w),
                            child: InkWell(
                                onTap: () {
                                  reservationDetailsData.notesCubit.state.data
                                      .removeAt(index);
                                  reservationDetailsData.notesCubit
                                      .onUpdateData(state.data);
                                },
                                child: SvgPicture.asset(Res.trash,
                                    color: MyColors.black)),
                          ),
                          InkWell(
                              onTap: () =>
                                  reservationDetailsData.bottomSheetEditNote(
                                      context,
                                      reservationDetailsData,
                                      state.data[index],
                                      deviceModel),
                              child: SvgPicture.asset(Res.edit,
                                  color: MyColors.black)),
                        ],
                      );
                    }),
                  ]),
                  replacement: BuildEmptyReservationOptionsItem(
                    title: tr("No_Salon_Notes_Yet"),
                    image: Res.EmptyNotes,
                    subTitle: tr("notes_appear_here"),
                  ),
                ),
              );
            }),
      ],
    );
  }
}
