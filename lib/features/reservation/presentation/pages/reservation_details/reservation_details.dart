part of 'reservation_details_imports.dart';

class ReservationDetails extends StatefulWidget {
  final ReservationRootData rootData;

  const ReservationDetails({Key? key, required this.rootData})
      : super(key: key);

  @override
  State<ReservationDetails> createState() => _ReservationDetailsState();
}

class _ReservationDetailsState extends State<ReservationDetails> {
  late ReservationDetailsData reservationDetailsData;
  late ReservationRootData rootData;

  @override
  void initState() {
    reservationDetailsData = widget.rootData.reservationDetailsData;
    rootData = widget.rootData;
    reservationDetailsData.fetchData(context, rootData);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<List<ServiceListModel>>,
            GenericState<List<ServiceListModel>>>(
        bloc: rootData.servicesData.servicesBloc,
        builder: (context, state) {
          return Column(
            children: [
              Flexible(
                child: ListView(
                  children: [
                    BuildHeaderReservationDetails(
                      model: device,
                      rootData: rootData,
                    ),
                    BuildReservationBookedServices(
                      reservationDetailsData: reservationDetailsData,
                      deviceModel: device,
                      rootData: rootData,
                      listServices: state.data,
                    ),
                    BuildClientDetails(
                      model: device,
                      reservationDetailsData: reservationDetailsData,
                      personalParams: rootData.personalParams,
                    ),
                    BuildPayDetails(
                      reservationDetailsData: reservationDetailsData,
                      model: device,
                      rootData: rootData,
                    ),
                    BuildPromoCodeView(
                      deviceModel: device,
                      reservationDetailsData: reservationDetailsData,
                      rootData: rootData,
                    ),
                    BuildReservationNoteView(
                      reservationDetailsData: reservationDetailsData,
                      deviceModel: device,
                    ),
                    BuildReservationGiftView(
                      reservationDetailsData: reservationDetailsData,
                      deviceModel: device,
                      rootData: rootData,
                    ),
                  ],
                ),
              ),
              BuildReservationDetailsBottom(
                model: device,
                reservationDetailsData: reservationDetailsData,
                reservationRoot: rootData,
                services: state.data,
              ),
            ],
          );
        });
  }
}
