part of 'reservation_details_imports.dart';

class ReservationDetailsData {
  final GlobalKey<FormState> promoFormKey = GlobalKey();
  final GlobalKey<FormState> giftFormKey = GlobalKey();
  final GlobalKey<FormState> updateFormKey = GlobalKey();
  final GlobalKey<FormState> addCardFormKey = GlobalKey();
  final GenericBloc<String?> finallySelectedPayTypeCubit = GenericBloc(null);
  final GenericBloc<bool> favoriteBloc = GenericBloc(false);
  final GenericBloc<PaymentType> payType = GenericBloc(PaymentType.creditCard);
  final GenericBloc<bool> clientDataCubit = GenericBloc(true);
  final TextEditingController giftFirstName = TextEditingController();
  final TextEditingController giftLsatName = TextEditingController();
  final TextEditingController giftPhoneNumber = TextEditingController();
  final TextEditingController cardNumber = TextEditingController();
  final TextEditingController expiryDate = TextEditingController();
  final TextEditingController cvvController = TextEditingController();
  final TextEditingController promoCode = TextEditingController();
  final TextEditingController giftEmailAddress = TextEditingController();
  final TextEditingController wishes = TextEditingController();
  final TextEditingController newCardName = TextEditingController();
  final TextEditingController noteText = TextEditingController();
  final GenericBloc<List<PaymentModel>> paymentCubit = GenericBloc([]);
  final GenericBloc<List<PromoModel>> promoCodesCubit = GenericBloc([]);
  final GenericBloc<List<String>> notesCubit = GenericBloc([]);
  final GenericBloc<PromoModel?> promoCodeItemCubit = GenericBloc(null);
  final GenericBloc<GiftEntity?> giftCubit = GenericBloc(null);
  TextEditingController editHolderName = TextEditingController();
  TextEditingController editCardNumber = TextEditingController();
  TextEditingController editExpiryDate = TextEditingController();
  TextEditingController editCvv = TextEditingController();
  TextEditingController editNote = TextEditingController();

  ReservationDetailsWithoutLoginModel? successReserveModel;
  PromoModel? selectedPromoCode;

  void bottomSheetNotes(
    BuildContext context,
    DeviceModel model,
    ReservationDetailsData reservationDetailsData,
  ) {
    Widget widget = BuildNotesBottomSheet(
      model: model,
      reservationDetailsData: reservationDetailsData,
    );
    _showBottomSheet(context, widget);
  }

  void bottomSheetEditNote(
      BuildContext context,
      ReservationDetailsData reservationDetailsData,
      String note,
      DeviceModel deviceModel) {
    editNote.text = note;
    Widget widget = BuildEditNoteBottomSheet(
      deviceModel: deviceModel,
      reservationDetailsData: reservationDetailsData,
      item: note,
    );
    _showBottomSheet(context, widget);
  }

  void bottomSheetServiceOptions(
    BuildContext context,
    ReservationDetailsData reservationDetailsData,
    DeviceModel deviceModel,
    ServiceListModel serviceListModel,
    ReservationRootData rootData,
  ) {
    Widget widget = BuildBottomSheetServiceOptions(
      deviceModel: deviceModel,
      reservationDetailsData: reservationDetailsData,
      serviceListModel: serviceListModel,
      rootData: rootData,
    );
    _showBottomSheet(context, widget);
  }

  void editNoteItem(BuildContext context, String note) {
    _updateListNote(note);
    AutoRouter.of(context).pop();
    CustomToast.showSimpleToast(
      msg: tr("Success_edit_note"),
      type: ToastType.success,
      title: tr("success_toast"),
    );
  }

  void _updateListNote(String notesItem) {
    int index = notesCubit.state.data.indexOf(notesItem);
    notesCubit.state.data[index] = editNote.text;
    notesCubit.onUpdateData(notesCubit.state.data);
  }

  void bottomSheetPromoCode(
    BuildContext context,
    DeviceModel model,
    ReservationDetailsData reservationDetailsData,
    ReservationRootData rootData,
  ) {
    Widget widget = BuildBottomSheetPromoCode(
      model: model,
      reservationDetailsData: reservationDetailsData,
      rootData: rootData,
    );
    _showBottomSheet(context, widget);
  }

  void bottomSheetSendAsGift(
    BuildContext context,
    DeviceModel model,
    ReservationDetailsData reservationDetailsData,
    ReservationRootData rootData,
  ) {
    Widget widget = BuildAsGiftBottomSheet(
      model: model,
      reservationDetailsData: reservationDetailsData,
      rootData: rootData,
    );
    _showBottomSheet(context, widget);
  }

  void bottomSheetAddCard(
    BuildContext context,
    ReservationDetailsData reservationDetailsData,
  ) {
    Widget widget = BuildAddVisaCardBottomSheet(
      reservationDetailsData: reservationDetailsData,
    );
    _showBottomSheet(context, widget);
  }

  void _showBottomSheet(BuildContext context, Widget widget) {
    showModalBottomSheet(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      context: context,
      isScrollControlled: true,
      builder: (cxt) {
        return widget;
      },
    );
  }

  void selectVisaCard(PaymentModel paymentModel, bool isGuestUserCard) {
    if (!isGuestUserCard) {
      int index = paymentCubit.state.data.indexOf(paymentModel);
      paymentCubit.state.data.map((e) => e.isDefault = false);
      paymentCubit.state.data[index].isDefault = true;
      paymentCubit.onUpdateData(paymentCubit.state.data);
      finallySelectedPayTypeCubit.onUpdateData(paymentModel.id);
      payType.onUpdateData(PaymentType.creditCard);
    }
  }

  void bottomSheetEditCard(
      BuildContext context,
      DeviceModel model,
      ReservationDetailsData reservationDetailsData,
      PaymentModel paymentModel) {
    _addDataToEditPaymentController(paymentModel);
    Widget widget = BuildEditVisaCardBottomSett(
      deviceModel: model,
      paymentModel: paymentModel,
      reservationDetailsData: reservationDetailsData,
    );
    _showBottomSheet(context, widget);
  }

  Future<void> getPayment({bool refresh = true}) async {
    var data = await GetAllPayment()(refresh);
    if (data.isNotEmpty) {
      var item =
          data.firstWhereOrNull((element) => element.isDefault) ?? data.first;
      _updatePaymentCubit(data);
      selectVisaCard(item, false);
    }
  }

  void _updatePaymentCubit(List<PaymentModel> data) {
    paymentCubit.onUpdateData(data);
  }

  void _addDataToEditPaymentController(PaymentModel paymentModel) {
    editCardNumber.text = paymentModel.cardNumber;
    editCvv.text = paymentModel.cvv;
    editExpiryDate.text = paymentModel.expiryDate;
    editHolderName.text = paymentModel.holderName;
  }

  void updateCard(BuildContext context, PaymentModel model) async {
    if (updateFormKey.currentState!.validate()) {
      bool result = await _createUpdatePayment(model);
      if (result) {
        getPayment();
        AutoRouter.of(context).pop();
        CustomToast.showSimpleToast(
            msg: tr("success_edit"),
            type: ToastType.success,
            title: tr("success_toast"));
      }
    }
  }

  Future<void> applyPromo(BuildContext context, PromoModel promoModel,
      ReservationRootData rootData) async {
    if (rootData.servicesData.totalBloc.state.data >=
        promoModel.minimumSpendAmount) {
      promoCode.text = promoModel.promoCode;
      selectedPromoCode = promoModel;
      await makeReservation(context, rootData, ReservationEnum.promo);
      promoCodeItemCubit.onUpdateData(promoModel);
    } else {
      CustomToast.showSimpleToast(
        msg: "${tr("promo_toast")} ${promoModel.minimumSpendAmount}",
        type: ToastType.info,
        title: tr("Warning Toast"),
      );
    }
  }

  Future<bool> _createUpdatePayment(PaymentModel model) async {
    getIt<LoadingHelper>().showLoadingDialog();
    PaymentPrams prams = paymentParams(model);
    var result = await CreateUpdatePayment()(prams);
    getIt<LoadingHelper>().dismissDialog();
    return result;
  }

  PaymentPrams paymentParams(PaymentModel model) {
    PaymentPrams prams = PaymentPrams(
      id: model.id,
      holderName: editHolderName.text,
      expiryDate: editExpiryDate.text,
      cardNumber: editCardNumber.text,
      cvv: editCvv.text,
      isDefault: model.isDefault,
    );
    return prams;
  }

  Future<void> deleteCard(BuildContext context, PaymentModel model) async {
    getIt<LoadingHelper>().showLoadingDialog();
    var result = await DeletePaymentCard()(model.id);
    getIt<LoadingHelper>().dismissDialog();
    if (result) {
      if (finallySelectedPayTypeCubit.state.data == model.id) {
        finallySelectedPayTypeCubit.onUpdateData(null);
      }
      paymentCubit.state.data.remove(model);
      paymentCubit.onUpdateData(paymentCubit.state.data);
      CustomToast.showSimpleToast(
        msg: tr("deleteCard"),
        type: ToastType.success,
        title: tr("success_toast"),
      );
    }
  }

  void addCard(BuildContext context) async {
    if (addCardFormKey.currentState!.validate()) {
      getIt<LoadingHelper>().showLoadingDialog();
      PaymentPrams prams = PaymentPrams(
        holderName: newCardName.text,
        expiryDate: expiryDate.text,
        cardNumber: cardNumber.text,
        cvv: cvvController.text,
        isDefault: true,
      );
      var result = await CreateUpdatePayment()(prams);
      _handleAddResult(context, result);
    }
  }

  Future<void> _handleAddResult(BuildContext context, bool result) async {
    if (result) {
      await getPayment();
      _clearAddInputs();
      getIt<LoadingHelper>().dismissDialog();
      AutoRouter.of(context).pop();
      CustomToast.showSimpleToast(
          msg: tr("success_edit"),
          type: ToastType.success,
          title: tr("success_toast"));
    }
  }

  void _clearAddInputs() {
    expiryDate.clear();
    newCardName.clear();
    cardNumber.clear();
    cvvController.clear();
  }

  void addNotes(BuildContext context) {
    if (noteText.text.isNotEmpty) {
      notesCubit.state.data.add(noteText.text);
      notesCubit.onUpdateData(notesCubit.state.data);
      noteText.clear();
      AutoRouter.of(context).pop();
    }
  }

  void addGift(BuildContext context) {
    if (giftFormKey.currentState!.validate()) {
      giftCubit.onUpdateData(GiftEntity(
        applicantMobile: giftPhoneNumber.text,
        applicantLastName: giftLsatName.text,
        applicantFirstName: giftFirstName.text,
        applicantEmail: giftEmailAddress.text,
        wishes: wishes.text,
      ));
      clientDataCubit.onUpdateData(false);
      AutoRouter.of(context).pop();
    }
  }

  void clearCardData(BuildContext context) {
    AutoRouter.of(context).pop();
  }

  void clearNote(BuildContext context) {
    noteText.clear();
    AutoRouter.of(context).pop();
  }

  void clearPromoCodeData(BuildContext context) {
    promoCode.clear();
    AutoRouter.of(context).pop();
  }

  void clearGiftData(BuildContext context) {
    giftCubit.onUpdateData(null);
    AutoRouter.of(context).pop();
  }

  Future<List<AppointmentOrdersModel>> getOrders(BuildContext context,
      {required ReservationRootData rootData,
      required String orderNumber}) async {
    var lastName = context.read<UserCubit>().state.model?.lastName;
    var params = AppointmentOrderParams(
        refresh: false,
        lastName: rootData.personalParams?.lastName ?? lastName,
        orderNumber: orderNumber);
    var data = await GetDashboardData()(params);
    return data;
  }

  Future<void> makeReservation(BuildContext context,
      ReservationRootData rootData, ReservationEnum reservationEnum) async {
    var user = context.read<UserCubit>().state.model;
    var device = context.read<DeviceCubit>().state.model;
    List<ReservationServiceEntity> reservationList =
        _getReservationList(rootData);
    if (finallySelectedPayTypeCubit.state.data != null &&
        paymentCubit.state.data == [] &&
        rootData.personalParams?.cardNumber == null) {
      CustomToast.showSimpleToast(
          msg: tr("add_card_to_continue"),
          type: ToastType.warning,
          title: tr("Warning_Toast"));
      return;
    }
    var reservationEntity = _reservationParams(user, rootData, reservationList);
    if (reservationEnum == ReservationEnum.makeReservation) {
      await _applyConfirmReservation(
          reservationEntity, context, rootData, device);
    } else {
      rootData.servicesData.calculateTotal();
      await _applyPromoCodeValidation(reservationEntity, rootData, context);
    }
  }

  Future<void> _applyConfirmReservation(
      ReservationEntity reservationEntity,
      BuildContext context,
      ReservationRootData rootData,
      DeviceModel device) async {
    var result = await MakeReservation().call(reservationEntity);
    if (result != null) {
      await successReservationData(
          context, rootData, result.orderNumber, device);
      AutoRouter.of(context).push(HomeRoute());
      Future.delayed(Duration(seconds: 1), () {
        _successReservationPopup();
      });
    }
  }

  Future<void> successReservationData(
      BuildContext context,
      ReservationRootData rootData,
      String orderNumber,
      DeviceModel device) async {
    getIt<LoadingHelper>().showLoadingDialog();
    List<AppointmentOrdersModel> data =
        await getOrders(context, rootData: rootData, orderNumber: orderNumber);
    getIt<LoadingHelper>().dismissDialog();
    successReserveModel = ReservationDetailsWithoutLoginModel(
      refNumber: orderNumber,
      businessName:
          rootData.servicesData.detailsCubit.state.data!.getBusinessName(),
      reservationTimes: rootData.dateData.selectedServiceCubit.state.data
          .map((e) => e.time)
          .toList(),
      date: formatDate(rootData.dateData.dateCubit.state.data,
          rootData.dateData.dateCubit.state.data.firstOrNull, device.locale),
      appointmentOrdersModel: data.first,
    );
    context.read<ReservationDetailsCubit>().onUpdateReservationData(
          successReserveModel,
        );
  }

  Future<void> _applyPromoCodeValidation(ReservationEntity reservationEntity,
      ReservationRootData rootData, BuildContext context) async {
    if (promoCode.text.isNotEmpty) {
      var result = await ApplyPromoCode()(reservationEntity);
      if (result?.isSuccess == true) {
        _applyPromoCodeToTotal(rootData);
        AutoRouter.of(context).pop();
      } else {
        CustomToast.showSimpleToast(
          msg: tr("ca_not_apply_promo"),
          title: tr("Info"),
          type: ToastType.info,
        );
      }
    }
  }

  void _applyPromoCodeToTotal(ReservationRootData rootData) {
    var total = rootData.servicesData.totalBloc.state.data;
    final percentage = selectedPromoCode?.discountPercentage ?? 0;
    if (percentage > 0) {
      var servPrice = servicesPercentagePrice(rootData);
      var discount = (selectedPromoCode!.discountPercentage! / 100) * servPrice;
      total = total - discount;
    } else {
      total = total - (selectedPromoCode!.discount ?? 0);
    }
    rootData.servicesData.totalBloc.onUpdateData(total);
  }

  num servicesPercentagePrice(ReservationRootData rootData) =>
      rootData.servicesData.servicesBloc.state.data
          .firstWhere(
              (element) => element.serviceId == selectedPromoCode!.serviceId)
          .price;

  ReservationEntity _reservationParams(
      UserModel? user,
      ReservationRootData rootData,
      List<ReservationServiceEntity> reservationList) {
    ReservationEntity reservationEntity = ReservationEntity(
      userId: user?.userId,
      applicantEmail: user?.email ?? rootData.personalParams?.email,
      applicantFirstName: user?.firstName ?? rootData.personalParams?.firstName,
      applicantLastName: user?.lastName ?? rootData.personalParams?.lastName,
      applicantMobile: user?.phoneNumber ?? rootData.personalParams?.phone,
      appointmentDetails: reservationList,
      promoCode: promoCode.text,
      notes: notesCubit.state.data,
      isReserveTogether: rootData.dateData.switchedCubit.state.data,
      giftDetail: giftCubit.state.data,
      cardDetails: _getPaymentCardDetails(rootData),
    );
    return reservationEntity;
  }

  PaymentModel? _getPaymentCardDetails(ReservationRootData rootData) {
    bool isGuestUserCard = rootData.personalParams?.cardNumber != null;
    if (finallySelectedPayTypeCubit.state.data != null && !isGuestUserCard) {
      return paymentCubit.state.data
          .where(
              (element) => element.id == finallySelectedPayTypeCubit.state.data)
          .firstOrNull;
    } else if (_checkGustPayment(rootData)) {
      return paymentCubit.state.data.first;
    } else {
      return null;
    }
  }

  bool _checkGustPayment(ReservationRootData rootData) {
    bool isGuestUserCard = rootData.personalParams?.cardNumber != null;
    return (isGuestUserCard &&
        rootData.personalParams!.paymentType == PaymentType.creditCard);
  }

  List<ReservationServiceEntity> _getReservationList(
      ReservationRootData rootData) {
    var reservationList = rootData.servicesData.servicesBloc.state.data
        .map(
          (e) => ReservationServiceEntity(
            businessId: rootData.dateData.businessId,
            businessServiceId: e.businessServiceID,
            staffID: e.staffId == "0" ? "" : e.staffId,
            businessServiceName: e.getServiceName(),
            fromTime: _getReservedTimes(rootData, e.businessServiceID, true),
            toTime: _getReservedTimes(rootData, e.businessServiceID, false),
            appointmentDate: getIt<Utilities>().formatDate(
              rootData.dateData.dateCubit.state.data.firstOrNull ??
                  DateTime.now(),
            ),
          ),
        )
        .toList();
    return reservationList;
  }

  String _getReservedTimes(
      ReservationRootData rootData, String businessServiceID, bool isFrom) {
    if (rootData.dateData.switchedCubit.state.data) {
      return _getReserveTogetherTimes(rootData, isFrom);
    }
    return _getReserveManualTimes(rootData, businessServiceID);
  }

  String _getReserveTogetherTimes(ReservationRootData rootData, bool isFrom) {
    var times = rootData.dateData.bestTimesCubit.state.data;
    var selected = rootData.dateData.bestTimesCubit.state.data.index;
    if (isFrom) {
      return times.times[selected].split("-").first.trim();
    } else {
      return times.times[selected].split("-").last.trim();
    }
  }

  String _getReserveManualTimes(
      ReservationRootData rootData, String businessServiceID) {
    return rootData.dateData.selectedServiceCubit.state.data
        .firstWhere((time) => time.id == businessServiceID)
        .time;
  }

  String formatDate(List listDate, DateTime? dateTime, Locale locale) {
    String date = "";
    var formatter = DateFormat.yMMMMEEEEd('ar_SA');
    if (listDate != []) {
      if (locale == Locale('en', 'US')) {
        date = DateFormat("EEEE, d MMM yyyy").format(dateTime ?? DateTime(0));
      } else {
        date = formatter.format(dateTime ?? DateTime(0));
      }
    }
    return date;
  }

  Future<void> deleteService(BuildContext context, ReservationRootData rootData,
      ServiceListModel serviceListModel) async {
    if (rootData.servicesData.servicesBloc.state.data.length != 1) {
      _updateServiceAndDateCu(context, rootData, serviceListModel);
    } else {
      await rootData.pagesCubit.onUpdateData(0);
      rootData.controller
          .animateToPage(0,
              duration: Duration(milliseconds: 400), curve: Curves.easeIn)
          .then((value) => null);
      rootData.dateData.availableDatesCubit.onUpdateData([]);
      rootData.servicesData.servicesBloc.onUpdateData([]);
      AutoRouter.of(context).pop();
    }
  }

  Future<void> _updateServiceAndDateCu(BuildContext context,
      ReservationRootData rootData, ServiceListModel serviceListModel) async {
    _removeServicesTimeItem(rootData, serviceListModel);
    _removeServicesItem(rootData, serviceListModel);
    rootData.servicesData.calculateTotal();
    promoCodeItemCubit.onUpdateData(null);
    promoCode.clear();
    AutoRouter.of(context).pop();
  }

  _removeServicesTimeItem(
      ReservationRootData rootData, ServiceListModel serviceListModel) {
    rootData.dateData.selectedServiceCubit.state.data
        .removeWhere((e) => e.time == serviceListModel.selectedTimeForService);
    rootData.dateData.selectedServiceCubit
        .onUpdateData(rootData.dateData.selectedServiceCubit.state.data);
  }

  _removeServicesItem(
      ReservationRootData rootData, ServiceListModel serviceListModel) {
    rootData.servicesData.servicesBloc.state.data.remove(serviceListModel);
    rootData.servicesData.servicesBloc
        .onUpdateData(rootData.servicesData.servicesBloc.state.data);
    if (rootData.servicesData.servicesBloc.state.data.length == 1) {
      rootData.servicesData.servicesBloc.state.data[0].selectedTimeForService =
          "";
      rootData.servicesData.servicesBloc
          .onUpdateData(rootData.servicesData.servicesBloc.state.data);
    }
  }

  Future<void> getPromos(ReservationRootData rootData,
      {bool? refresh = true}) async {
    List<String> listServiceID = rootData.servicesData.servicesBloc.state.data
        .map((e) => e.businessServiceID)
        .toList();
    var data =
        await GetPromo()(_getPromoParams(rootData, listServiceID, refresh));
    promoCodesCubit.onUpdateData(data);
  }

  GetPromoParams _getPromoParams(
      ReservationRootData rootData, List<String> listServiceID, bool? refresh) {
    return GetPromoParams(
      businessId: rootData.dateData.businessId,
      businessServiceIDs: listServiceID,
      pageNumber: 1,
      pageSize: 500,
      refresh: refresh,
      loadImages: true,
    );
  }

  _successReservationPopup() {
    late CancelFunc toastCancel;
    toastCancel = BotToast.showAttachedWidget(
      attachedBuilder: (_) {
        return BuildSuccessReservationBottomSheet(toastCancel: toastCancel);
      },
      duration: Duration(seconds: 5),
      target: Offset(520, 520),
      animationDuration: Duration(milliseconds: 210),
      wrapToastAnimation: (controller, cancel, Widget child) =>
          CustomAttachedAnimation(
        controller: controller,
        child: child,
      ),
    );
  }

  fetchData(BuildContext context, ReservationRootData rootData) {
    var auth = context.read<DeviceCubit>().state.model.auth;
    if (promoCodeItemCubit.state.data == null) {
      promoCode.clear();
    }
    getPromos(rootData, refresh: false);
    getPromos(rootData);
    if (auth) {
      getPayment();
    } else {
      finallySelectedPayTypeCubit.onUpdateData(
        rootData.personalParams?.cardNumber ?? "",
      );
    }
  }
}

enum ReservationEnum { promo, makeReservation }
