part of 'personal_details_imports.dart';

class PersonalDetailsData {
  final GlobalKey<FormState> frmKey = GlobalKey();
  final GlobalKey<FormState> cardFrmKey = GlobalKey();
  final GenericBloc<Country?> codeCubit = GenericBloc(null);
  final GenericBloc<PaymentType?> payType = GenericBloc(null);
  final TextEditingController giftPhoneNumber = TextEditingController();
  final TextEditingController firstName = TextEditingController();
  final TextEditingController lastName = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController cardName = TextEditingController();
  final TextEditingController cardNumber = TextEditingController();
  final TextEditingController expiryDate = TextEditingController();
  final TextEditingController cvv = TextEditingController();

  void showCountryDialog(BuildContext context) async {
    getIt<Utilities>().showCountryDialog(
      context,
      onSelect: (country) => codeCubit.onUpdateData(country),
    );
  }

  void goNext(ReservationRootData rootData) {
    bool userDataValidate = frmKey.currentState!.validate();
    if (payType.state.data == null) {
      CustomToast.showSimpleToast(
        title: tr("paymentError"),
        msg: tr("selectPaymentMethod"),
      );
      return;
    }
    if (payType.state.data == PaymentType.creditCard) {
      if (!userDataValidate && !cardFrmKey.currentState!.validate()) {
        return;
      }
      _routToSummery(rootData);
    } else {
      if (userDataValidate) {
        _routToSummery(rootData);
      }
    }
  }

  void _routToSummery(ReservationRootData rootData) {
    rootData.personalParams = _personalParams();
    rootData.reservationDetailsData.paymentCubit.onUpdateData([]);
    _updatePaymentCubit(rootData);
    rootData.goNextPage();
  }

  PersonalParams _personalParams() {
    return PersonalParams(
      lastName: lastName.text,
      email: email.text,
      phone: phone.text,
      firstName: firstName.text,
      cardNumber: cardNumber.text,
      cvv: cvv.text,
      expiryDate: expiryDate.text,
      paymentType: payType.state.data!,
    );
  }

  void _updatePaymentCubit(ReservationRootData rootData) {
    rootData.reservationDetailsData.paymentCubit.state.data.add(
      _paymentModel(),
    );
    rootData.reservationDetailsData.paymentCubit.onUpdateData(
      rootData.reservationDetailsData.paymentCubit.state.data,
    );
  }

  PaymentModel _paymentModel() {
    return PaymentModel(
      expiryDate: expiryDate.text,
      cvv: cvv.text,
      cardNumber: cardNumber.text,
      id: '',
      isDefault: true,
      holderName: firstName.text + lastName.text,
    );
  }

  String getPaymentImage() {
    switch (payType.state.data) {
      case PaymentType.creditCard:
        return Res.visaMastercard;
      case PaymentType.applePay:
        return Res.applePay;
      case PaymentType.paypal:
        return Res.paypal;
      case PaymentType.cash:
        return Res.cash;
      case PaymentType.tabby:
        return Res.tabby;
      case PaymentType.tamara:
        return Res.tamara;
      default:
        return Res.visaMastercard;
    }
  }

  String getPaymentTitle() {
    switch (payType.state.data) {
      case PaymentType.creditCard:
        return tr("deptOrCredit");
      case PaymentType.applePay:
        return tr("applyNow");
      case PaymentType.paypal:
        return tr("payPal");
      case PaymentType.cash:
        return tr("cash");
      case PaymentType.tabby:
        return tr("tabby");
      case PaymentType.tamara:
        return tr("tamara");
      default:
        return tr("deptOrCredit");
    }
  }
}
