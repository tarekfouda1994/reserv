part of 'personal_details_widgets_imports.dart';

class BuildCardInputsView extends StatelessWidget {
  final PersonalDetailsData controller;
  const BuildCardInputsView({key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return Form(
      key: controller.cardFrmKey,
      child: Padding(
        padding: const EdgeInsets.only(top: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildHeaderText(device, tr("nameOnCard")),
            GenericTextField(
              controller: controller.cardName,
              fieldTypes: FieldTypes.normal,
              type: TextInputType.name,
              fillColor: MyColors.white,
              enableBorderColor: MyColors.reserveDateBg,
              action: TextInputAction.next,
              validate: (value) => value?.validateName(),
              hint: "${tr("ex")} ${tr("ex_name")}",
              suffixIcon: SizedBox(),
              contentPadding: EdgeInsets.symmetric(
                horizontal: device.isTablet ? 8.sp : 12.w,
              ),
              margin: EdgeInsets.only(bottom: 15, top: 5),
            ),
            buildHeaderText(device, tr("card_number")),
            GenericTextField(
              controller: controller.cardNumber,
              fieldTypes: FieldTypes.normal,
              type: TextInputType.number,
              fillColor: MyColors.white,
              enableBorderColor: MyColors.reserveDateBg,
              action: TextInputAction.next,
              validate: (value) => value?.validateCard(context),
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(16),
                if (device.locale.languageCode == "en") CardNumberFormatter(),
              ],
              hint: '--- ---- ---- ----',
              suffixIcon: BuildSuffixIcon(asset: Res.credit_card),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: device.isTablet ? 8.sp : 12.w),
              margin: EdgeInsets.only(bottom: 15, top: 5),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      buildHeaderText(device, tr("expiry_date")),
                      GenericTextField(
                        controller: controller.expiryDate,
                        margin: EdgeInsets.only(top: 5),
                        fillColor: MyColors.white,
                        enableBorderColor: MyColors.reserveDateBg,
                        fieldTypes: FieldTypes.normal,
                        type: TextInputType.datetime,
                        hint:
                            "${tr("ex")} (${DateFormat("MM/yyyy").format(DateTime.now())})",
                        action: TextInputAction.next,
                        inputFormatters: [
                          CardExpirationFormatter(),
                          LengthLimitingTextInputFormatter(7),
                        ],
                        validate: (value) => value?.validateExpiryDate(),
                        suffixIcon: CalendarIcon(),
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: device.isTablet ? 8.sp : 12.w,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: device.isTablet ? 15 : 5),
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      buildHeaderText(device, "CVV"),
                      GenericTextField(
                        controller: controller.cvv,
                        fieldTypes: FieldTypes.normal,
                        type: TextInputType.number,
                        fillColor: MyColors.white,
                        enableBorderColor: MyColors.reserveDateBg,
                        action: TextInputAction.done,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          LengthLimitingTextInputFormatter(3),
                        ],
                        validate: (value) => value?.validateCVV(),
                        hint: "---",
                        suffixIcon: SizedBox(),
                        contentPadding: EdgeInsets.symmetric(
                          horizontal: device.isTablet ? 8.sp : 12.w,
                        ),
                        margin: EdgeInsets.only(top: 5),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  RichText buildHeaderText(DeviceModel device, String title) {
    return RichText(
      textScaleFactor: 1.2,
      textWidthBasis: TextWidthBasis.longestLine,
      text: TextSpan(
        text: title,
        style: TextStyle(
          color: MyColors.black,
          fontFamily: CustomFonts.primaryFont,
          fontSize: device.isTablet ? 7.sp : 10.sp,
        ),
        children: [
          TextSpan(
            text: " *",
            style: TextStyle(
              fontFamily: CustomFonts.primaryFont,
              color: MyColors.errorColor,
              fontSize: device.isTablet ? 7.sp : 11.sp,
            ),
          ),
        ],
      ),
    );
  }
}
