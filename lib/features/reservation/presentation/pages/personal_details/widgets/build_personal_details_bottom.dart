part of 'personal_details_widgets_imports.dart';

class BuildPersonalDetailsBottom extends StatelessWidget {
  final PersonalDetailsData personalDetailsData;
  final ReservationRootData rootData;

  const BuildPersonalDetailsBottom(
      {Key? key, required this.personalDetailsData, required this.rootData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      decoration: BoxDecoration(color: MyColors.white, boxShadow: [
        BoxShadow(color: MyColors.greyWhite, spreadRadius: 2, blurRadius: 2)
      ]),
      padding:
          EdgeInsetsDirectional.only(bottom: 10, start: 25, top: 10, end: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BuildCancelReservationButton(rootData: rootData),
          InkWell(
            onTap: () => personalDetailsData.goNext(rootData),
            child: Container(
              height: device.isTablet ? 50.h : 40.h,
              width: MediaQuery.of(context).size.width * .45,
              decoration: BoxDecoration(
                  color: MyColors.primary,
                  borderRadius: BorderRadius.circular(40.r)),
              padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    MyText(
                        title: tr("review"),
                        color: MyColors.white,
                        size: device.isTablet ? 7.sp : 11.sp),
                    SizedBox(width: 5.sp),
                    BuildMyArrowIcon(),
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
