part of 'personal_details_widgets_imports.dart';

class BuildPaymentMethodsView extends StatelessWidget {
  final GenericBloc<PaymentType?> bloc;
  final Widget? userCards;
  final VoidCallback? callback;
  final Widget? subHeader;
  final PersonalDetailsData? personalDetailsController;

  const BuildPaymentMethodsView({
    Key? key,
    required this.bloc,
    this.userCards,
    this.callback,
    this.subHeader, this.personalDetailsController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenericBloc<PaymentType?>, GenericState<PaymentType?>>(
        bloc: bloc,
        builder: (context, state) {
          return Container(
            padding: const EdgeInsets.symmetric(vertical: 35, horizontal: 18),
            color: userCards == null ? MyColors.reserveTogetherBg : null,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      Res.secure,
                      height: 28,
                      width: 23,
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: MyText(
                                  title: tr("payWith"),
                                  color: MyColors.black,
                                  size: 13,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              if (subHeader != null) subHeader!,
                            ],
                          ),
                          SizedBox(height: 2),
                          MyText(
                            title: tr("secureConnection"),
                            color: MyColors.black,
                            size: 10,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(height: 12),
                Padding(
                  padding: const EdgeInsetsDirectional.only(start: 33),
                  child: MyText(
                    title: tr("chargeInNextScreen"),
                    color: MyColors.primary,
                    size: 10,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                  decoration: BoxDecoration(
                    color: MyColors.white,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: MyColors.reserveDateBg),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (userCards != null) userCards!,
                      if (userCards == null)
                        BuildPayTypeItem(
                          controller: personalDetailsController,
                          onTap: () {
                            bloc.onUpdateData(PaymentType.creditCard);
                          },
                          title: tr("deptOrCredit"),
                          image: Res.visaMastercard,
                          value: 0,
                          changeValue: state.data?.index??-1,
                        ),
                      BuildPayTypeItem(
                        onTap: () {
                          bloc.onUpdateData(PaymentType.applePay);
                          if (callback != null) callback!();
                        },
                        title: tr("applyNow"),
                        image: Res.applePay,
                        value: 1,
                        changeValue: state.data?.index??-1,
                      ),
                      BuildPayTypeItem(
                        onTap: () {
                          bloc.onUpdateData(PaymentType.paypal);
                          if (callback != null) callback!();
                        },
                        title: tr("payPal"),
                        image: Res.paypal,
                        value: 2,
                        changeValue: state.data?.index??-1,
                      ),
                      BuildPayTypeItem(
                        onTap: () {
                          bloc.onUpdateData(PaymentType.cash);
                          if (callback != null) callback!();
                        },
                        title: tr("cash"),
                        sideText: tr("bookNowPayLater"),
                        image: Res.cash,
                        value: 3,
                        changeValue: state.data?.index??-1,
                      ),
                      SizedBox(height: 24),
                      MyText(
                        title: tr("easyInstallment"),
                        color: MyColors.black,
                        size: 11,
                        fontWeight: FontWeight.w600,
                      ),
                      SizedBox(height: 2),
                      BuildPayTypeItem(
                        onTap: () {
                          bloc.onUpdateData(PaymentType.tabby);
                          if (callback != null) callback!();
                        },
                        title: tr("tabby"),
                        image: Res.tabby,
                        subTitle: tr("splitIn3Payment"),
                        value: 4,
                        changeValue: state.data?.index??-1,
                      ),
                      BuildPayTypeItem(
                        onTap: () {
                          bloc.onUpdateData(PaymentType.tamara);
                          if (callback != null) callback!();
                        },
                        title: tr("tamara"),
                        image: Res.tamara,
                        subTitle: tr("splitIn3Payment"),
                        value: 5,
                        changeValue: state.data?.index??-1,
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}
