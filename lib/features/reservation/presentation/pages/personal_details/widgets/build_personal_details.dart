part of 'personal_details_widgets_imports.dart';

class BuildPersonalDetails extends StatelessWidget {
  final PersonalDetailsData personalDetailsData;

  const BuildPersonalDetails({Key? key, required this.personalDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10.h),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextField(
                    controller: personalDetailsData.firstName,
                    fieldTypes: FieldTypes.normal,
                    type: TextInputType.name,
                    action: TextInputAction.next,
                    label: tr("first_name"),
                    validate: (value) => value?.validateName(),
                    hint: "${tr("ex")} ${tr("ex_name")}",
                    contentPadding: EdgeInsets.symmetric(
                      vertical: device.isTablet ? 10.sp : 8.sp,
                      horizontal: device.isTablet ? 8.sp : 12.w,
                    ),
                    margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
                  ),
                ],
              ),
            ),
            SizedBox(width: device.isTablet ? 15.sp : 10),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextField(
                      controller: personalDetailsData.lastName,
                      fieldTypes: FieldTypes.normal,
                      type: TextInputType.name,
                      action: TextInputAction.next,
                      validate: (value) => value?.validateName(),
                      hint: "${tr("ex")} ${tr("ex_name")}",
                      label: tr("last_name"),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: device.isTablet ? 10.sp : 8.sp,
                          horizontal: device.isTablet ? 8.sp : 12.w),
                      margin: EdgeInsets.only(bottom: 15.h, top: 4.h)),
                ],
              ),
            )
          ],
        ),
        CustomTextField(
          controller: personalDetailsData.email,
          fieldTypes: FieldTypes.normal,
          type: TextInputType.emailAddress,
          action: TextInputAction.next,
          label: tr("email"),
          validate: (value) => value?.validateEmail(),
          suffixIcon: BuildSuffixIcon(asset: Res.envelope_input),
          hint: "${tr("ex")} ${tr("ex_email")}",
          contentPadding: EdgeInsets.symmetric(
              vertical: device.isTablet ? 10.sp : 8.sp,
              horizontal: device.isTablet ? 8.sp : 12.w),
          margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
        ),
        PhoneComponent(
          phone: personalDetailsData.phone,
          codeCubit: personalDetailsData.codeCubit,
          onViewCountries: () => personalDetailsData.showCountryDialog(context),
        ),
      ],
    );
  }
}
