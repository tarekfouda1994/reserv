part of 'personal_details_widgets_imports.dart';

class BuildPaymentMethods extends StatelessWidget {
  final PersonalDetailsData personalDetailsData;

  const BuildPaymentMethods({Key? key, required this.personalDetailsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BuildPersonalDetailsCatName(
            device: device, title: tr("payment_methods")),
        CustomTextField(
          label: tr("card_number"),
          controller: personalDetailsData.cardNumber,
          fieldTypes: FieldTypes.normal,
          type: TextInputType.number,
          action: TextInputAction.next,
          validate: (value) => value?.validateCard(context),
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
            LengthLimitingTextInputFormatter(16),
            if (device.locale.languageCode == "en") CardNumberFormatter(),
          ],
          hint: 'XXXX XXXX XXXX XXXX',
          suffixIcon: BuildSuffixIcon(asset: Res.credit_card),
          contentPadding: EdgeInsets.symmetric(
              vertical: device.isTablet ? 10.sp : 8.sp,
              horizontal: device.isTablet ? 8.sp : 12.w),
          margin: EdgeInsets.only(bottom: 15.h, top: 4.h),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 4,
              child: CustomTextField(
                fillColor: Color(0xffF6F9F9),
                controller: personalDetailsData.expiryDate,
                margin: EdgeInsets.only(top: 10, bottom: 15.h),
                fieldTypes: FieldTypes.normal,
                type: TextInputType.datetime,
                hint:
                    "${tr("ex")} (${DateFormat("MM/yyyy").format(DateTime.now())})",
                label: tr("expiry_date"),
                action: TextInputAction.next,
                inputFormatters: [
                  CardExpirationFormatter(),
                  LengthLimitingTextInputFormatter(7),
                ],
                validate: (value) => value?.validateExpiryDate(),
                suffixIcon: CalendarIcon(),
              ),
            ),
            SizedBox(width: device.isTablet ? 15.sp : 10),
            Expanded(
              flex: 3,
              child: CustomTextField(
                controller: personalDetailsData.cvv,
                fieldTypes: FieldTypes.password,
                type: TextInputType.number,
                action: TextInputAction.done,
                label: tr("cvv"),
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(3),
                ],
                validate: (value) => value?.validateCVV(),
                hint: "xxx",
                maxLength: 3,
                contentPadding: EdgeInsets.symmetric(
                  vertical: device.isTablet ? 10.sp : 8.sp,
                  horizontal: device.isTablet ? 8.sp : 12.w,
                ),
                margin: EdgeInsets.only(bottom: 15.h, top: 10),
              ),
            )
          ],
        ),
      ],
    );
  }
}
