part of 'personal_details_widgets_imports.dart';

class BuildPersonalDetailsCatName extends StatelessWidget {
  final DeviceModel device;
  final String title;

  const BuildPersonalDetailsCatName({Key? key, required this.device, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: MyText(
        title: title,
        color: MyColors.black,
        size: device.isTablet ? 7.sp : 11.5.sp,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
