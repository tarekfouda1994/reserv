part of 'personal_details_widgets_imports.dart';

class BuildPayTypeItem extends StatelessWidget {
  final String title;
  final String? subTitle;
  final String? sideText;
  final String image;
  final int value;
  final int changeValue;
  final EdgeInsetsGeometry? margin;
  final void Function()? onTap;
  final PersonalDetailsData? controller;

  const BuildPayTypeItem({
    key,
    required this.title,
    required this.image,
    required this.value,
    required this.changeValue,
    this.onTap,
    this.subTitle,
    this.sideText,
    this.controller,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isSelected = value == changeValue;
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 15),
        margin: margin ?? const EdgeInsets.symmetric(vertical: 3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: isSelected ? MyColors.primary : MyColors.reserveDateBg,
            width: isSelected ? 1.5 : 1,
          ),
        ),
        child: Column(
          children: [
            Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              BuildRadioItem(
                iconSize: 12,
                margin: EdgeInsets.zero,
                value: value,
                changeValue: changeValue,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        MyText(
                          title: title,
                          color: isSelected ? MyColors.primary : MyColors.black,
                          size: 10,
                          fontWeight:
                              isSelected ? FontWeight.bold : FontWeight.w500,
                        ),
                        if (sideText != null)
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsetsDirectional.only(start: 8),
                              child: MyText(
                                title: sideText ?? "",
                                color: MyColors.grey,
                                size: 10,
                                fontWeight: FontWeight.w500,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                      ],
                    ),
                    if (subTitle != null)
                      MyText(
                        title: subTitle!,
                        color: MyColors.grey,
                        size: 10,
                        fontWeight: FontWeight.w500,
                      ),
                  ],
                ),
              ),
              image.contains("jpg")
                  ? Image.asset(
                      image,
                      fit: BoxFit.fill,
                      height: value == 1 ? 16 : 17,
                      width: value == 1 ? 41 : 50,
                    )
                  : SvgPicture.asset(image),
            ]),
            if (changeValue == 0 && value == 0)
              BuildCardInputsView(controller: controller!),
          ],
        ),
      ),
    );
  }
}
