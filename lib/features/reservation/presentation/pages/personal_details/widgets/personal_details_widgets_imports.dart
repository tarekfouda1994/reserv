import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_my_arrow_icon.dart';
import 'package:flutter_tdd/core/widgets/build_radio_item.dart';
import 'package:flutter_tdd/core/widgets/card_expiration_formatter.dart';
import 'package:flutter_tdd/core/widgets/card_number_formatter.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/core/widgets/inputs_icons/calendar_icon.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/register/widgets/phone_component.dart';
import 'package:flutter_tdd/features/reservation/data/enums/payment_enum.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/widgets/reservation_root_widgets_imports.dart';
import 'package:flutter_tdd/res.dart';
import 'package:intl/src/intl/date_format.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../../core/models/device_model/device_model.dart';
import '../personal_details_imports.dart';

part 'build_card_inputs_view.dart';
part 'build_gust_payment_view.dart';
part 'build_pay_type_item.dart';
part 'build_payment_methods.dart';
part 'build_perosnal_details_cat_nam.dart';
part 'build_personal_details.dart';
part 'build_personal_details_bottom.dart';
