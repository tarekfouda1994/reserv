part of 'personal_details_imports.dart';

class PersonalDetails extends StatefulWidget {
  final ReservationRootData rootData;

  const PersonalDetails({Key? key, required this.rootData}) : super(key: key);

  @override
  State<PersonalDetails> createState() => _PersonalDetailsState();
}

class _PersonalDetailsState extends State<PersonalDetails> {
  late ReservationRootData rootData;
  late PersonalDetailsData personalDetailsData;

  @override
  void initState() {
    personalDetailsData = widget.rootData.personalDetailsData;
    rootData = widget.rootData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Container(
        color: MyColors.white,
        child: Form(
          key: personalDetailsData.frmKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                  child: ListView(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                    child: Column(
                      children: [
                        BuildPersonalDetails(
                          personalDetailsData: personalDetailsData,
                        ),
                        // BuildPaymentMethods(
                        //   personalDetailsData: personalDetailsData,
                        // ),
                      ],
                    ),
                  ),
                  BuildPaymentMethodsView(
                    bloc: personalDetailsData.payType,
                    personalDetailsController: personalDetailsData,
                  ),
                ],
              )),
              BuildPersonalDetailsBottom(
                personalDetailsData: personalDetailsData,
                rootData: widget.rootData,
              )
            ],
          ),
        ),
      ),
    );
  }
}
