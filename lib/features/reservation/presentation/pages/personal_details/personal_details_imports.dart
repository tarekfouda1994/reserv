import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/custom_toast.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/features/profile/data/model/payment_model/payment_model.dart';
import 'package:flutter_tdd/features/reservation/data/enums/payment_enum.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/personal_paroms.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/personal_details/widgets/personal_details_widgets_imports.dart';
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../../../../../res.dart';

part 'personal_details.dart';
part 'personal_details_data.dart';
