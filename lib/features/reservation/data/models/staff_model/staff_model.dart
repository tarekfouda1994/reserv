import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';
import '../staff_days_moel/staff_days_model.dart';

part 'staff_model.freezed.dart';
part 'staff_model.g.dart';

@freezed
class StaffModel with _$StaffModel {
  StaffModel._();

  @JsonSerializable(explicitToJson: true)
  factory StaffModel({
    required String staffID,
    required String staffNameEnglish,
    required String staffNameArabic,
    required String avatar,
    required List<StaffDaysModel> days,
    @JsonKey(defaultValue: 0, nullable: true) required double rating,
  }) = _StaffModel;

  factory StaffModel.fromJson(Map<String, dynamic> json) => _$StaffModelFromJson(json);

  String getStaffName() {
    return getIt<Utilities>().getLocalizedValue(staffNameArabic, staffNameEnglish);
  }
}
