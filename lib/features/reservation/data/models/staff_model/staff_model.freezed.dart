// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'staff_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StaffModel _$StaffModelFromJson(Map<String, dynamic> json) {
  return _StaffModel.fromJson(json);
}

/// @nodoc
mixin _$StaffModel {
  String get staffID => throw _privateConstructorUsedError;
  String get staffNameEnglish => throw _privateConstructorUsedError;
  String get staffNameArabic => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  List<StaffDaysModel> get days => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  double get rating => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StaffModelCopyWith<StaffModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StaffModelCopyWith<$Res> {
  factory $StaffModelCopyWith(
          StaffModel value, $Res Function(StaffModel) then) =
      _$StaffModelCopyWithImpl<$Res>;
  $Res call(
      {String staffID,
      String staffNameEnglish,
      String staffNameArabic,
      String avatar,
      List<StaffDaysModel> days,
      @JsonKey(defaultValue: 0, nullable: true) double rating});
}

/// @nodoc
class _$StaffModelCopyWithImpl<$Res> implements $StaffModelCopyWith<$Res> {
  _$StaffModelCopyWithImpl(this._value, this._then);

  final StaffModel _value;
  // ignore: unused_field
  final $Res Function(StaffModel) _then;

  @override
  $Res call({
    Object? staffID = freezed,
    Object? staffNameEnglish = freezed,
    Object? staffNameArabic = freezed,
    Object? avatar = freezed,
    Object? days = freezed,
    Object? rating = freezed,
  }) {
    return _then(_value.copyWith(
      staffID: staffID == freezed
          ? _value.staffID
          : staffID // ignore: cast_nullable_to_non_nullable
              as String,
      staffNameEnglish: staffNameEnglish == freezed
          ? _value.staffNameEnglish
          : staffNameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      staffNameArabic: staffNameArabic == freezed
          ? _value.staffNameArabic
          : staffNameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      days: days == freezed
          ? _value.days
          : days // ignore: cast_nullable_to_non_nullable
              as List<StaffDaysModel>,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$$_StaffModelCopyWith<$Res>
    implements $StaffModelCopyWith<$Res> {
  factory _$$_StaffModelCopyWith(
          _$_StaffModel value, $Res Function(_$_StaffModel) then) =
      __$$_StaffModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String staffID,
      String staffNameEnglish,
      String staffNameArabic,
      String avatar,
      List<StaffDaysModel> days,
      @JsonKey(defaultValue: 0, nullable: true) double rating});
}

/// @nodoc
class __$$_StaffModelCopyWithImpl<$Res> extends _$StaffModelCopyWithImpl<$Res>
    implements _$$_StaffModelCopyWith<$Res> {
  __$$_StaffModelCopyWithImpl(
      _$_StaffModel _value, $Res Function(_$_StaffModel) _then)
      : super(_value, (v) => _then(v as _$_StaffModel));

  @override
  _$_StaffModel get _value => super._value as _$_StaffModel;

  @override
  $Res call({
    Object? staffID = freezed,
    Object? staffNameEnglish = freezed,
    Object? staffNameArabic = freezed,
    Object? avatar = freezed,
    Object? days = freezed,
    Object? rating = freezed,
  }) {
    return _then(_$_StaffModel(
      staffID: staffID == freezed
          ? _value.staffID
          : staffID // ignore: cast_nullable_to_non_nullable
              as String,
      staffNameEnglish: staffNameEnglish == freezed
          ? _value.staffNameEnglish
          : staffNameEnglish // ignore: cast_nullable_to_non_nullable
              as String,
      staffNameArabic: staffNameArabic == freezed
          ? _value.staffNameArabic
          : staffNameArabic // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: avatar == freezed
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      days: days == freezed
          ? _value._days
          : days // ignore: cast_nullable_to_non_nullable
              as List<StaffDaysModel>,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_StaffModel extends _StaffModel {
  _$_StaffModel(
      {required this.staffID,
      required this.staffNameEnglish,
      required this.staffNameArabic,
      required this.avatar,
      required final List<StaffDaysModel> days,
      @JsonKey(defaultValue: 0, nullable: true) required this.rating})
      : _days = days,
        super._();

  factory _$_StaffModel.fromJson(Map<String, dynamic> json) =>
      _$$_StaffModelFromJson(json);

  @override
  final String staffID;
  @override
  final String staffNameEnglish;
  @override
  final String staffNameArabic;
  @override
  final String avatar;
  final List<StaffDaysModel> _days;
  @override
  List<StaffDaysModel> get days {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_days);
  }

  @override
  @JsonKey(defaultValue: 0, nullable: true)
  final double rating;

  @override
  String toString() {
    return 'StaffModel(staffID: $staffID, staffNameEnglish: $staffNameEnglish, staffNameArabic: $staffNameArabic, avatar: $avatar, days: $days, rating: $rating)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StaffModel &&
            const DeepCollectionEquality().equals(other.staffID, staffID) &&
            const DeepCollectionEquality()
                .equals(other.staffNameEnglish, staffNameEnglish) &&
            const DeepCollectionEquality()
                .equals(other.staffNameArabic, staffNameArabic) &&
            const DeepCollectionEquality().equals(other.avatar, avatar) &&
            const DeepCollectionEquality().equals(other._days, _days) &&
            const DeepCollectionEquality().equals(other.rating, rating));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(staffID),
      const DeepCollectionEquality().hash(staffNameEnglish),
      const DeepCollectionEquality().hash(staffNameArabic),
      const DeepCollectionEquality().hash(avatar),
      const DeepCollectionEquality().hash(_days),
      const DeepCollectionEquality().hash(rating));

  @JsonKey(ignore: true)
  @override
  _$$_StaffModelCopyWith<_$_StaffModel> get copyWith =>
      __$$_StaffModelCopyWithImpl<_$_StaffModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StaffModelToJson(
      this,
    );
  }
}

abstract class _StaffModel extends StaffModel {
  factory _StaffModel(
      {required final String staffID,
      required final String staffNameEnglish,
      required final String staffNameArabic,
      required final String avatar,
      required final List<StaffDaysModel> days,
      @JsonKey(defaultValue: 0, nullable: true)
          required final double rating}) = _$_StaffModel;
  _StaffModel._() : super._();

  factory _StaffModel.fromJson(Map<String, dynamic> json) =
      _$_StaffModel.fromJson;

  @override
  String get staffID;
  @override
  String get staffNameEnglish;
  @override
  String get staffNameArabic;
  @override
  String get avatar;
  @override
  List<StaffDaysModel> get days;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  double get rating;
  @override
  @JsonKey(ignore: true)
  _$$_StaffModelCopyWith<_$_StaffModel> get copyWith =>
      throw _privateConstructorUsedError;
}
