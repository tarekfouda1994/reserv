// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'staff_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StaffModel _$$_StaffModelFromJson(Map<String, dynamic> json) =>
    _$_StaffModel(
      staffID: json['staffID'] as String,
      staffNameEnglish: json['staffNameEnglish'] as String,
      staffNameArabic: json['staffNameArabic'] as String,
      avatar: json['avatar'] as String,
      days: (json['days'] as List<dynamic>)
          .map((e) => StaffDaysModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      rating: (json['rating'] as num?)?.toDouble() ?? 0,
    );

Map<String, dynamic> _$$_StaffModelToJson(_$_StaffModel instance) =>
    <String, dynamic>{
      'staffID': instance.staffID,
      'staffNameEnglish': instance.staffNameEnglish,
      'staffNameArabic': instance.staffNameArabic,
      'avatar': instance.avatar,
      'days': instance.days.map((e) => e.toJson()).toList(),
      'rating': instance.rating,
    };
