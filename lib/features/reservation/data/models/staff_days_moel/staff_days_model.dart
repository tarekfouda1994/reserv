import 'package:freezed_annotation/freezed_annotation.dart';

part 'staff_days_model.freezed.dart';
part 'staff_days_model.g.dart';

@freezed
class StaffDaysModel with _$StaffDaysModel {
  @JsonSerializable(explicitToJson: true)
  factory StaffDaysModel({
    required String day,
    required bool isDateAvailable,
  }) = _StaffDaysModel;

  factory StaffDaysModel.fromJson(Map<String, dynamic> json) => _$StaffDaysModelFromJson(json);
}
