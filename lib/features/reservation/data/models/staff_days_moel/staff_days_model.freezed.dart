// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'staff_days_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StaffDaysModel _$StaffDaysModelFromJson(Map<String, dynamic> json) {
  return _StaffDaysModel.fromJson(json);
}

/// @nodoc
mixin _$StaffDaysModel {
  String get day => throw _privateConstructorUsedError;
  bool get isDateAvailable => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StaffDaysModelCopyWith<StaffDaysModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StaffDaysModelCopyWith<$Res> {
  factory $StaffDaysModelCopyWith(
          StaffDaysModel value, $Res Function(StaffDaysModel) then) =
      _$StaffDaysModelCopyWithImpl<$Res>;
  $Res call({String day, bool isDateAvailable});
}

/// @nodoc
class _$StaffDaysModelCopyWithImpl<$Res>
    implements $StaffDaysModelCopyWith<$Res> {
  _$StaffDaysModelCopyWithImpl(this._value, this._then);

  final StaffDaysModel _value;
  // ignore: unused_field
  final $Res Function(StaffDaysModel) _then;

  @override
  $Res call({
    Object? day = freezed,
    Object? isDateAvailable = freezed,
  }) {
    return _then(_value.copyWith(
      day: day == freezed
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as String,
      isDateAvailable: isDateAvailable == freezed
          ? _value.isDateAvailable
          : isDateAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_StaffDaysModelCopyWith<$Res>
    implements $StaffDaysModelCopyWith<$Res> {
  factory _$$_StaffDaysModelCopyWith(
          _$_StaffDaysModel value, $Res Function(_$_StaffDaysModel) then) =
      __$$_StaffDaysModelCopyWithImpl<$Res>;
  @override
  $Res call({String day, bool isDateAvailable});
}

/// @nodoc
class __$$_StaffDaysModelCopyWithImpl<$Res>
    extends _$StaffDaysModelCopyWithImpl<$Res>
    implements _$$_StaffDaysModelCopyWith<$Res> {
  __$$_StaffDaysModelCopyWithImpl(
      _$_StaffDaysModel _value, $Res Function(_$_StaffDaysModel) _then)
      : super(_value, (v) => _then(v as _$_StaffDaysModel));

  @override
  _$_StaffDaysModel get _value => super._value as _$_StaffDaysModel;

  @override
  $Res call({
    Object? day = freezed,
    Object? isDateAvailable = freezed,
  }) {
    return _then(_$_StaffDaysModel(
      day: day == freezed
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as String,
      isDateAvailable: isDateAvailable == freezed
          ? _value.isDateAvailable
          : isDateAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_StaffDaysModel implements _StaffDaysModel {
  _$_StaffDaysModel({required this.day, required this.isDateAvailable});

  factory _$_StaffDaysModel.fromJson(Map<String, dynamic> json) =>
      _$$_StaffDaysModelFromJson(json);

  @override
  final String day;
  @override
  final bool isDateAvailable;

  @override
  String toString() {
    return 'StaffDaysModel(day: $day, isDateAvailable: $isDateAvailable)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StaffDaysModel &&
            const DeepCollectionEquality().equals(other.day, day) &&
            const DeepCollectionEquality()
                .equals(other.isDateAvailable, isDateAvailable));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(day),
      const DeepCollectionEquality().hash(isDateAvailable));

  @JsonKey(ignore: true)
  @override
  _$$_StaffDaysModelCopyWith<_$_StaffDaysModel> get copyWith =>
      __$$_StaffDaysModelCopyWithImpl<_$_StaffDaysModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StaffDaysModelToJson(
      this,
    );
  }
}

abstract class _StaffDaysModel implements StaffDaysModel {
  factory _StaffDaysModel(
      {required final String day,
      required final bool isDateAvailable}) = _$_StaffDaysModel;

  factory _StaffDaysModel.fromJson(Map<String, dynamic> json) =
      _$_StaffDaysModel.fromJson;

  @override
  String get day;
  @override
  bool get isDateAvailable;
  @override
  @JsonKey(ignore: true)
  _$$_StaffDaysModelCopyWith<_$_StaffDaysModel> get copyWith =>
      throw _privateConstructorUsedError;
}
