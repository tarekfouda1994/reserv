// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'staff_days_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StaffDaysModel _$$_StaffDaysModelFromJson(Map<String, dynamic> json) =>
    _$_StaffDaysModel(
      day: json['day'] as String,
      isDateAvailable: json['isDateAvailable'] as bool,
    );

Map<String, dynamic> _$$_StaffDaysModelToJson(_$_StaffDaysModel instance) =>
    <String, dynamic>{
      'day': instance.day,
      'isDateAvailable': instance.isDateAvailable,
    };
