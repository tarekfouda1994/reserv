// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'appointment_success_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppointmentSuccessModel _$AppointmentSuccessModelFromJson(
    Map<String, dynamic> json) {
  return _AppointmentSuccessModel.fromJson(json);
}

/// @nodoc
mixin _$AppointmentSuccessModel {
  String get referenceId => throw _privateConstructorUsedError;
  String get orderNumber => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppointmentSuccessModelCopyWith<AppointmentSuccessModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppointmentSuccessModelCopyWith<$Res> {
  factory $AppointmentSuccessModelCopyWith(AppointmentSuccessModel value,
          $Res Function(AppointmentSuccessModel) then) =
      _$AppointmentSuccessModelCopyWithImpl<$Res>;
  $Res call({String referenceId, String orderNumber});
}

/// @nodoc
class _$AppointmentSuccessModelCopyWithImpl<$Res>
    implements $AppointmentSuccessModelCopyWith<$Res> {
  _$AppointmentSuccessModelCopyWithImpl(this._value, this._then);

  final AppointmentSuccessModel _value;
  // ignore: unused_field
  final $Res Function(AppointmentSuccessModel) _then;

  @override
  $Res call({
    Object? referenceId = freezed,
    Object? orderNumber = freezed,
  }) {
    return _then(_value.copyWith(
      referenceId: referenceId == freezed
          ? _value.referenceId
          : referenceId // ignore: cast_nullable_to_non_nullable
              as String,
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_AppointmentSuccessModelCopyWith<$Res>
    implements $AppointmentSuccessModelCopyWith<$Res> {
  factory _$$_AppointmentSuccessModelCopyWith(_$_AppointmentSuccessModel value,
          $Res Function(_$_AppointmentSuccessModel) then) =
      __$$_AppointmentSuccessModelCopyWithImpl<$Res>;
  @override
  $Res call({String referenceId, String orderNumber});
}

/// @nodoc
class __$$_AppointmentSuccessModelCopyWithImpl<$Res>
    extends _$AppointmentSuccessModelCopyWithImpl<$Res>
    implements _$$_AppointmentSuccessModelCopyWith<$Res> {
  __$$_AppointmentSuccessModelCopyWithImpl(_$_AppointmentSuccessModel _value,
      $Res Function(_$_AppointmentSuccessModel) _then)
      : super(_value, (v) => _then(v as _$_AppointmentSuccessModel));

  @override
  _$_AppointmentSuccessModel get _value =>
      super._value as _$_AppointmentSuccessModel;

  @override
  $Res call({
    Object? referenceId = freezed,
    Object? orderNumber = freezed,
  }) {
    return _then(_$_AppointmentSuccessModel(
      referenceId: referenceId == freezed
          ? _value.referenceId
          : referenceId // ignore: cast_nullable_to_non_nullable
              as String,
      orderNumber: orderNumber == freezed
          ? _value.orderNumber
          : orderNumber // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_AppointmentSuccessModel implements _AppointmentSuccessModel {
  _$_AppointmentSuccessModel(
      {required this.referenceId, required this.orderNumber});

  factory _$_AppointmentSuccessModel.fromJson(Map<String, dynamic> json) =>
      _$$_AppointmentSuccessModelFromJson(json);

  @override
  final String referenceId;
  @override
  final String orderNumber;

  @override
  String toString() {
    return 'AppointmentSuccessModel(referenceId: $referenceId, orderNumber: $orderNumber)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AppointmentSuccessModel &&
            const DeepCollectionEquality()
                .equals(other.referenceId, referenceId) &&
            const DeepCollectionEquality()
                .equals(other.orderNumber, orderNumber));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(referenceId),
      const DeepCollectionEquality().hash(orderNumber));

  @JsonKey(ignore: true)
  @override
  _$$_AppointmentSuccessModelCopyWith<_$_AppointmentSuccessModel>
      get copyWith =>
          __$$_AppointmentSuccessModelCopyWithImpl<_$_AppointmentSuccessModel>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppointmentSuccessModelToJson(
      this,
    );
  }
}

abstract class _AppointmentSuccessModel implements AppointmentSuccessModel {
  factory _AppointmentSuccessModel(
      {required final String referenceId,
      required final String orderNumber}) = _$_AppointmentSuccessModel;

  factory _AppointmentSuccessModel.fromJson(Map<String, dynamic> json) =
      _$_AppointmentSuccessModel.fromJson;

  @override
  String get referenceId;
  @override
  String get orderNumber;
  @override
  @JsonKey(ignore: true)
  _$$_AppointmentSuccessModelCopyWith<_$_AppointmentSuccessModel>
      get copyWith => throw _privateConstructorUsedError;
}
