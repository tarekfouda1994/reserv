import 'package:freezed_annotation/freezed_annotation.dart';

part 'appointment_success_model.freezed.dart';
part 'appointment_success_model.g.dart';

@freezed
class AppointmentSuccessModel with _$AppointmentSuccessModel {
  @JsonSerializable(explicitToJson: true)
  factory AppointmentSuccessModel({
    required String referenceId,
    required String orderNumber,
  }) = _AppointmentSuccessModel;

  factory AppointmentSuccessModel.fromJson(Map<String, dynamic> json) =>
      _$AppointmentSuccessModelFromJson(json);
}
