// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_success_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppointmentSuccessModel _$$_AppointmentSuccessModelFromJson(
        Map<String, dynamic> json) =>
    _$_AppointmentSuccessModel(
      referenceId: json['referenceId'] as String,
      orderNumber: json['orderNumber'] as String,
    );

Map<String, dynamic> _$$_AppointmentSuccessModelToJson(
        _$_AppointmentSuccessModel instance) =>
    <String, dynamic>{
      'referenceId': instance.referenceId,
      'orderNumber': instance.orderNumber,
    };
