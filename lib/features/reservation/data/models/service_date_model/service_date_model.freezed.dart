// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'service_date_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceDateModel _$ServiceDateModelFromJson(Map<String, dynamic> json) {
  return _ServiceDateModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceDateModel {
  @JsonKey(name: "calanderMonthDay")
  int get monthDay => throw _privateConstructorUsedError;
  @JsonKey(name: "calanderDate")
  DateTime get date => throw _privateConstructorUsedError;
  bool get isServiceAvailable => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceDateModelCopyWith<ServiceDateModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceDateModelCopyWith<$Res> {
  factory $ServiceDateModelCopyWith(
          ServiceDateModel value, $Res Function(ServiceDateModel) then) =
      _$ServiceDateModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: "calanderMonthDay") int monthDay,
      @JsonKey(name: "calanderDate") DateTime date,
      bool isServiceAvailable});
}

/// @nodoc
class _$ServiceDateModelCopyWithImpl<$Res>
    implements $ServiceDateModelCopyWith<$Res> {
  _$ServiceDateModelCopyWithImpl(this._value, this._then);

  final ServiceDateModel _value;
  // ignore: unused_field
  final $Res Function(ServiceDateModel) _then;

  @override
  $Res call({
    Object? monthDay = freezed,
    Object? date = freezed,
    Object? isServiceAvailable = freezed,
  }) {
    return _then(_value.copyWith(
      monthDay: monthDay == freezed
          ? _value.monthDay
          : monthDay // ignore: cast_nullable_to_non_nullable
              as int,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      isServiceAvailable: isServiceAvailable == freezed
          ? _value.isServiceAvailable
          : isServiceAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceDateModelCopyWith<$Res>
    implements $ServiceDateModelCopyWith<$Res> {
  factory _$$_ServiceDateModelCopyWith(
          _$_ServiceDateModel value, $Res Function(_$_ServiceDateModel) then) =
      __$$_ServiceDateModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: "calanderMonthDay") int monthDay,
      @JsonKey(name: "calanderDate") DateTime date,
      bool isServiceAvailable});
}

/// @nodoc
class __$$_ServiceDateModelCopyWithImpl<$Res>
    extends _$ServiceDateModelCopyWithImpl<$Res>
    implements _$$_ServiceDateModelCopyWith<$Res> {
  __$$_ServiceDateModelCopyWithImpl(
      _$_ServiceDateModel _value, $Res Function(_$_ServiceDateModel) _then)
      : super(_value, (v) => _then(v as _$_ServiceDateModel));

  @override
  _$_ServiceDateModel get _value => super._value as _$_ServiceDateModel;

  @override
  $Res call({
    Object? monthDay = freezed,
    Object? date = freezed,
    Object? isServiceAvailable = freezed,
  }) {
    return _then(_$_ServiceDateModel(
      monthDay: monthDay == freezed
          ? _value.monthDay
          : monthDay // ignore: cast_nullable_to_non_nullable
              as int,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      isServiceAvailable: isServiceAvailable == freezed
          ? _value.isServiceAvailable
          : isServiceAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceDateModel implements _ServiceDateModel {
  _$_ServiceDateModel(
      {@JsonKey(name: "calanderMonthDay") required this.monthDay,
      @JsonKey(name: "calanderDate") required this.date,
      required this.isServiceAvailable});

  factory _$_ServiceDateModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceDateModelFromJson(json);

  @override
  @JsonKey(name: "calanderMonthDay")
  final int monthDay;
  @override
  @JsonKey(name: "calanderDate")
  final DateTime date;
  @override
  final bool isServiceAvailable;

  @override
  String toString() {
    return 'ServiceDateModel(monthDay: $monthDay, date: $date, isServiceAvailable: $isServiceAvailable)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServiceDateModel &&
            const DeepCollectionEquality().equals(other.monthDay, monthDay) &&
            const DeepCollectionEquality().equals(other.date, date) &&
            const DeepCollectionEquality()
                .equals(other.isServiceAvailable, isServiceAvailable));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(monthDay),
      const DeepCollectionEquality().hash(date),
      const DeepCollectionEquality().hash(isServiceAvailable));

  @JsonKey(ignore: true)
  @override
  _$$_ServiceDateModelCopyWith<_$_ServiceDateModel> get copyWith =>
      __$$_ServiceDateModelCopyWithImpl<_$_ServiceDateModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceDateModelToJson(
      this,
    );
  }
}

abstract class _ServiceDateModel implements ServiceDateModel {
  factory _ServiceDateModel(
      {@JsonKey(name: "calanderMonthDay") required final int monthDay,
      @JsonKey(name: "calanderDate") required final DateTime date,
      required final bool isServiceAvailable}) = _$_ServiceDateModel;

  factory _ServiceDateModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceDateModel.fromJson;

  @override
  @JsonKey(name: "calanderMonthDay")
  int get monthDay;
  @override
  @JsonKey(name: "calanderDate")
  DateTime get date;
  @override
  bool get isServiceAvailable;
  @override
  @JsonKey(ignore: true)
  _$$_ServiceDateModelCopyWith<_$_ServiceDateModel> get copyWith =>
      throw _privateConstructorUsedError;
}
