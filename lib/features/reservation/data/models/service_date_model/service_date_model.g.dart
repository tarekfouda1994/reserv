// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_date_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceDateModel _$$_ServiceDateModelFromJson(Map<String, dynamic> json) =>
    _$_ServiceDateModel(
      monthDay: json['calanderMonthDay'] as int,
      date: DateTime.parse(json['calanderDate'] as String),
      isServiceAvailable: json['isServiceAvailable'] as bool,
    );

Map<String, dynamic> _$$_ServiceDateModelToJson(_$_ServiceDateModel instance) =>
    <String, dynamic>{
      'calanderMonthDay': instance.monthDay,
      'calanderDate': instance.date.toIso8601String(),
      'isServiceAvailable': instance.isServiceAvailable,
    };
