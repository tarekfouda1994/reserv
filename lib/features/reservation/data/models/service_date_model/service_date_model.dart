import 'package:freezed_annotation/freezed_annotation.dart';

part 'service_date_model.freezed.dart';
part 'service_date_model.g.dart';

@freezed
class ServiceDateModel with _$ServiceDateModel {
  @JsonSerializable(explicitToJson: true)
  factory ServiceDateModel({
    @JsonKey(name: "calanderMonthDay") required int monthDay,
    @JsonKey(name: "calanderDate") required DateTime date,
    required bool isServiceAvailable,
  }) = _ServiceDateModel;

  factory ServiceDateModel.fromJson(Map<String, dynamic> json) => _$ServiceDateModelFromJson(json);
}
