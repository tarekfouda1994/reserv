// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'apply_promo_code_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ApplyPromoCodeModel _$$_ApplyPromoCodeModelFromJson(
        Map<String, dynamic> json) =>
    _$_ApplyPromoCodeModel(
      isSuccess: json['isSuccess'] as bool,
      offerList: (json['offerList'] as List<dynamic>)
          .map((e) => PromoOfferListModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ApplyPromoCodeModelToJson(
        _$_ApplyPromoCodeModel instance) =>
    <String, dynamic>{
      'isSuccess': instance.isSuccess,
      'offerList': instance.offerList.map((e) => e.toJson()).toList(),
    };

_$_PromoOfferListModel _$$_PromoOfferListModelFromJson(
        Map<String, dynamic> json) =>
    _$_PromoOfferListModel(
      discountPercentage: json['discountPercentage'] as num?,
      discountAmount: json['discountAmount'] as num?,
      netAmount: json['netAmount'] as num?,
      businessId: json['businessId'] as String,
      businessServiceId: json['businessServiceId'] as String,
      message: json['message'] as String,
    );

Map<String, dynamic> _$$_PromoOfferListModelToJson(
        _$_PromoOfferListModel instance) =>
    <String, dynamic>{
      'discountPercentage': instance.discountPercentage,
      'discountAmount': instance.discountAmount,
      'netAmount': instance.netAmount,
      'businessId': instance.businessId,
      'businessServiceId': instance.businessServiceId,
      'message': instance.message,
    };
