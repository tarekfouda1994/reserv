// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'apply_promo_code_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ApplyPromoCodeModel _$ApplyPromoCodeModelFromJson(Map<String, dynamic> json) {
  return _ApplyPromoCodeModel.fromJson(json);
}

/// @nodoc
mixin _$ApplyPromoCodeModel {
  bool get isSuccess => throw _privateConstructorUsedError;
  List<PromoOfferListModel> get offerList => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ApplyPromoCodeModelCopyWith<ApplyPromoCodeModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApplyPromoCodeModelCopyWith<$Res> {
  factory $ApplyPromoCodeModelCopyWith(
          ApplyPromoCodeModel value, $Res Function(ApplyPromoCodeModel) then) =
      _$ApplyPromoCodeModelCopyWithImpl<$Res>;
  $Res call({bool isSuccess, List<PromoOfferListModel> offerList});
}

/// @nodoc
class _$ApplyPromoCodeModelCopyWithImpl<$Res>
    implements $ApplyPromoCodeModelCopyWith<$Res> {
  _$ApplyPromoCodeModelCopyWithImpl(this._value, this._then);

  final ApplyPromoCodeModel _value;
  // ignore: unused_field
  final $Res Function(ApplyPromoCodeModel) _then;

  @override
  $Res call({
    Object? isSuccess = freezed,
    Object? offerList = freezed,
  }) {
    return _then(_value.copyWith(
      isSuccess: isSuccess == freezed
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      offerList: offerList == freezed
          ? _value.offerList
          : offerList // ignore: cast_nullable_to_non_nullable
              as List<PromoOfferListModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_ApplyPromoCodeModelCopyWith<$Res>
    implements $ApplyPromoCodeModelCopyWith<$Res> {
  factory _$$_ApplyPromoCodeModelCopyWith(_$_ApplyPromoCodeModel value,
          $Res Function(_$_ApplyPromoCodeModel) then) =
      __$$_ApplyPromoCodeModelCopyWithImpl<$Res>;
  @override
  $Res call({bool isSuccess, List<PromoOfferListModel> offerList});
}

/// @nodoc
class __$$_ApplyPromoCodeModelCopyWithImpl<$Res>
    extends _$ApplyPromoCodeModelCopyWithImpl<$Res>
    implements _$$_ApplyPromoCodeModelCopyWith<$Res> {
  __$$_ApplyPromoCodeModelCopyWithImpl(_$_ApplyPromoCodeModel _value,
      $Res Function(_$_ApplyPromoCodeModel) _then)
      : super(_value, (v) => _then(v as _$_ApplyPromoCodeModel));

  @override
  _$_ApplyPromoCodeModel get _value => super._value as _$_ApplyPromoCodeModel;

  @override
  $Res call({
    Object? isSuccess = freezed,
    Object? offerList = freezed,
  }) {
    return _then(_$_ApplyPromoCodeModel(
      isSuccess: isSuccess == freezed
          ? _value.isSuccess
          : isSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
      offerList: offerList == freezed
          ? _value._offerList
          : offerList // ignore: cast_nullable_to_non_nullable
              as List<PromoOfferListModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ApplyPromoCodeModel implements _ApplyPromoCodeModel {
  _$_ApplyPromoCodeModel(
      {required this.isSuccess,
      required final List<PromoOfferListModel> offerList})
      : _offerList = offerList;

  factory _$_ApplyPromoCodeModel.fromJson(Map<String, dynamic> json) =>
      _$$_ApplyPromoCodeModelFromJson(json);

  @override
  final bool isSuccess;
  final List<PromoOfferListModel> _offerList;
  @override
  List<PromoOfferListModel> get offerList {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_offerList);
  }

  @override
  String toString() {
    return 'ApplyPromoCodeModel(isSuccess: $isSuccess, offerList: $offerList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ApplyPromoCodeModel &&
            const DeepCollectionEquality().equals(other.isSuccess, isSuccess) &&
            const DeepCollectionEquality()
                .equals(other._offerList, _offerList));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isSuccess),
      const DeepCollectionEquality().hash(_offerList));

  @JsonKey(ignore: true)
  @override
  _$$_ApplyPromoCodeModelCopyWith<_$_ApplyPromoCodeModel> get copyWith =>
      __$$_ApplyPromoCodeModelCopyWithImpl<_$_ApplyPromoCodeModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ApplyPromoCodeModelToJson(
      this,
    );
  }
}

abstract class _ApplyPromoCodeModel implements ApplyPromoCodeModel {
  factory _ApplyPromoCodeModel(
          {required final bool isSuccess,
          required final List<PromoOfferListModel> offerList}) =
      _$_ApplyPromoCodeModel;

  factory _ApplyPromoCodeModel.fromJson(Map<String, dynamic> json) =
      _$_ApplyPromoCodeModel.fromJson;

  @override
  bool get isSuccess;
  @override
  List<PromoOfferListModel> get offerList;
  @override
  @JsonKey(ignore: true)
  _$$_ApplyPromoCodeModelCopyWith<_$_ApplyPromoCodeModel> get copyWith =>
      throw _privateConstructorUsedError;
}

PromoOfferListModel _$PromoOfferListModelFromJson(Map<String, dynamic> json) {
  return _PromoOfferListModel.fromJson(json);
}

/// @nodoc
mixin _$PromoOfferListModel {
  num? get discountPercentage => throw _privateConstructorUsedError;
  num? get discountAmount => throw _privateConstructorUsedError;
  num? get netAmount => throw _privateConstructorUsedError;
  String get businessId => throw _privateConstructorUsedError;
  String get businessServiceId => throw _privateConstructorUsedError;
  String get message => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PromoOfferListModelCopyWith<PromoOfferListModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PromoOfferListModelCopyWith<$Res> {
  factory $PromoOfferListModelCopyWith(
          PromoOfferListModel value, $Res Function(PromoOfferListModel) then) =
      _$PromoOfferListModelCopyWithImpl<$Res>;
  $Res call(
      {num? discountPercentage,
      num? discountAmount,
      num? netAmount,
      String businessId,
      String businessServiceId,
      String message});
}

/// @nodoc
class _$PromoOfferListModelCopyWithImpl<$Res>
    implements $PromoOfferListModelCopyWith<$Res> {
  _$PromoOfferListModelCopyWithImpl(this._value, this._then);

  final PromoOfferListModel _value;
  // ignore: unused_field
  final $Res Function(PromoOfferListModel) _then;

  @override
  $Res call({
    Object? discountPercentage = freezed,
    Object? discountAmount = freezed,
    Object? netAmount = freezed,
    Object? businessId = freezed,
    Object? businessServiceId = freezed,
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as num?,
      discountAmount: discountAmount == freezed
          ? _value.discountAmount
          : discountAmount // ignore: cast_nullable_to_non_nullable
              as num?,
      netAmount: netAmount == freezed
          ? _value.netAmount
          : netAmount // ignore: cast_nullable_to_non_nullable
              as num?,
      businessId: businessId == freezed
          ? _value.businessId
          : businessId // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceId: businessServiceId == freezed
          ? _value.businessServiceId
          : businessServiceId // ignore: cast_nullable_to_non_nullable
              as String,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_PromoOfferListModelCopyWith<$Res>
    implements $PromoOfferListModelCopyWith<$Res> {
  factory _$$_PromoOfferListModelCopyWith(_$_PromoOfferListModel value,
          $Res Function(_$_PromoOfferListModel) then) =
      __$$_PromoOfferListModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {num? discountPercentage,
      num? discountAmount,
      num? netAmount,
      String businessId,
      String businessServiceId,
      String message});
}

/// @nodoc
class __$$_PromoOfferListModelCopyWithImpl<$Res>
    extends _$PromoOfferListModelCopyWithImpl<$Res>
    implements _$$_PromoOfferListModelCopyWith<$Res> {
  __$$_PromoOfferListModelCopyWithImpl(_$_PromoOfferListModel _value,
      $Res Function(_$_PromoOfferListModel) _then)
      : super(_value, (v) => _then(v as _$_PromoOfferListModel));

  @override
  _$_PromoOfferListModel get _value => super._value as _$_PromoOfferListModel;

  @override
  $Res call({
    Object? discountPercentage = freezed,
    Object? discountAmount = freezed,
    Object? netAmount = freezed,
    Object? businessId = freezed,
    Object? businessServiceId = freezed,
    Object? message = freezed,
  }) {
    return _then(_$_PromoOfferListModel(
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as num?,
      discountAmount: discountAmount == freezed
          ? _value.discountAmount
          : discountAmount // ignore: cast_nullable_to_non_nullable
              as num?,
      netAmount: netAmount == freezed
          ? _value.netAmount
          : netAmount // ignore: cast_nullable_to_non_nullable
              as num?,
      businessId: businessId == freezed
          ? _value.businessId
          : businessId // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceId: businessServiceId == freezed
          ? _value.businessServiceId
          : businessServiceId // ignore: cast_nullable_to_non_nullable
              as String,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PromoOfferListModel implements _PromoOfferListModel {
  _$_PromoOfferListModel(
      {required this.discountPercentage,
      required this.discountAmount,
      required this.netAmount,
      required this.businessId,
      required this.businessServiceId,
      required this.message});

  factory _$_PromoOfferListModel.fromJson(Map<String, dynamic> json) =>
      _$$_PromoOfferListModelFromJson(json);

  @override
  final num? discountPercentage;
  @override
  final num? discountAmount;
  @override
  final num? netAmount;
  @override
  final String businessId;
  @override
  final String businessServiceId;
  @override
  final String message;

  @override
  String toString() {
    return 'PromoOfferListModel(discountPercentage: $discountPercentage, discountAmount: $discountAmount, netAmount: $netAmount, businessId: $businessId, businessServiceId: $businessServiceId, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PromoOfferListModel &&
            const DeepCollectionEquality()
                .equals(other.discountPercentage, discountPercentage) &&
            const DeepCollectionEquality()
                .equals(other.discountAmount, discountAmount) &&
            const DeepCollectionEquality().equals(other.netAmount, netAmount) &&
            const DeepCollectionEquality()
                .equals(other.businessId, businessId) &&
            const DeepCollectionEquality()
                .equals(other.businessServiceId, businessServiceId) &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(discountPercentage),
      const DeepCollectionEquality().hash(discountAmount),
      const DeepCollectionEquality().hash(netAmount),
      const DeepCollectionEquality().hash(businessId),
      const DeepCollectionEquality().hash(businessServiceId),
      const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_PromoOfferListModelCopyWith<_$_PromoOfferListModel> get copyWith =>
      __$$_PromoOfferListModelCopyWithImpl<_$_PromoOfferListModel>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PromoOfferListModelToJson(
      this,
    );
  }
}

abstract class _PromoOfferListModel implements PromoOfferListModel {
  factory _PromoOfferListModel(
      {required final num? discountPercentage,
      required final num? discountAmount,
      required final num? netAmount,
      required final String businessId,
      required final String businessServiceId,
      required final String message}) = _$_PromoOfferListModel;

  factory _PromoOfferListModel.fromJson(Map<String, dynamic> json) =
      _$_PromoOfferListModel.fromJson;

  @override
  num? get discountPercentage;
  @override
  num? get discountAmount;
  @override
  num? get netAmount;
  @override
  String get businessId;
  @override
  String get businessServiceId;
  @override
  String get message;
  @override
  @JsonKey(ignore: true)
  _$$_PromoOfferListModelCopyWith<_$_PromoOfferListModel> get copyWith =>
      throw _privateConstructorUsedError;
}
