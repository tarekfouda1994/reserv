import 'package:freezed_annotation/freezed_annotation.dart';

part 'apply_promo_code_model.freezed.dart';
part 'apply_promo_code_model.g.dart';

@freezed
class ApplyPromoCodeModel with _$ApplyPromoCodeModel {
  @JsonSerializable(explicitToJson: true)
  factory ApplyPromoCodeModel({
    required bool isSuccess,
    required List<PromoOfferListModel> offerList,
  }) = _ApplyPromoCodeModel;

  factory ApplyPromoCodeModel.fromJson(Map<String, dynamic> json) =>
      _$ApplyPromoCodeModelFromJson(json);
}

@freezed
class PromoOfferListModel with _$PromoOfferListModel {
  @JsonSerializable(explicitToJson: true)
  factory PromoOfferListModel({
    required num? discountPercentage,
    required num? discountAmount,
    required num? netAmount,
    required String businessId,
    required String businessServiceId,
    required String message,
  }) = _PromoOfferListModel;

  factory PromoOfferListModel.fromJson(Map<String, dynamic> json) =>
      _$PromoOfferListModelFromJson(json);
}
