import 'package:freezed_annotation/freezed_annotation.dart';

import '../service_types_model/service_types_model.dart';

part 'company_services.freezed.dart';
part 'company_services.g.dart';

@freezed
class CompanyServices with _$CompanyServices {
  @JsonSerializable(explicitToJson: true)
  factory CompanyServices({
    required int businessServiceCount,
    required List companyServiceDetails,
    required List<ServiceTypesModel> businessServiceTypes,
  }) = _CompanyServices;

  factory CompanyServices.fromJson(Map<String, dynamic> json) => _$CompanyServicesFromJson(json);
}
