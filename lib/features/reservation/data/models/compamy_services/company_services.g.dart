// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_services.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CompanyServices _$$_CompanyServicesFromJson(Map<String, dynamic> json) =>
    _$_CompanyServices(
      businessServiceCount: json['businessServiceCount'] as int,
      companyServiceDetails: json['companyServiceDetails'] as List<dynamic>,
      businessServiceTypes: (json['businessServiceTypes'] as List<dynamic>)
          .map((e) => ServiceTypesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_CompanyServicesToJson(_$_CompanyServices instance) =>
    <String, dynamic>{
      'businessServiceCount': instance.businessServiceCount,
      'companyServiceDetails': instance.companyServiceDetails,
      'businessServiceTypes':
          instance.businessServiceTypes.map((e) => e.toJson()).toList(),
    };
