// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'company_services.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CompanyServices _$CompanyServicesFromJson(Map<String, dynamic> json) {
  return _CompanyServices.fromJson(json);
}

/// @nodoc
mixin _$CompanyServices {
  int get businessServiceCount => throw _privateConstructorUsedError;
  List<dynamic> get companyServiceDetails => throw _privateConstructorUsedError;
  List<ServiceTypesModel> get businessServiceTypes =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CompanyServicesCopyWith<CompanyServices> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CompanyServicesCopyWith<$Res> {
  factory $CompanyServicesCopyWith(
          CompanyServices value, $Res Function(CompanyServices) then) =
      _$CompanyServicesCopyWithImpl<$Res>;
  $Res call(
      {int businessServiceCount,
      List<dynamic> companyServiceDetails,
      List<ServiceTypesModel> businessServiceTypes});
}

/// @nodoc
class _$CompanyServicesCopyWithImpl<$Res>
    implements $CompanyServicesCopyWith<$Res> {
  _$CompanyServicesCopyWithImpl(this._value, this._then);

  final CompanyServices _value;
  // ignore: unused_field
  final $Res Function(CompanyServices) _then;

  @override
  $Res call({
    Object? businessServiceCount = freezed,
    Object? companyServiceDetails = freezed,
    Object? businessServiceTypes = freezed,
  }) {
    return _then(_value.copyWith(
      businessServiceCount: businessServiceCount == freezed
          ? _value.businessServiceCount
          : businessServiceCount // ignore: cast_nullable_to_non_nullable
              as int,
      companyServiceDetails: companyServiceDetails == freezed
          ? _value.companyServiceDetails
          : companyServiceDetails // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      businessServiceTypes: businessServiceTypes == freezed
          ? _value.businessServiceTypes
          : businessServiceTypes // ignore: cast_nullable_to_non_nullable
              as List<ServiceTypesModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_CompanyServicesCopyWith<$Res>
    implements $CompanyServicesCopyWith<$Res> {
  factory _$$_CompanyServicesCopyWith(
          _$_CompanyServices value, $Res Function(_$_CompanyServices) then) =
      __$$_CompanyServicesCopyWithImpl<$Res>;
  @override
  $Res call(
      {int businessServiceCount,
      List<dynamic> companyServiceDetails,
      List<ServiceTypesModel> businessServiceTypes});
}

/// @nodoc
class __$$_CompanyServicesCopyWithImpl<$Res>
    extends _$CompanyServicesCopyWithImpl<$Res>
    implements _$$_CompanyServicesCopyWith<$Res> {
  __$$_CompanyServicesCopyWithImpl(
      _$_CompanyServices _value, $Res Function(_$_CompanyServices) _then)
      : super(_value, (v) => _then(v as _$_CompanyServices));

  @override
  _$_CompanyServices get _value => super._value as _$_CompanyServices;

  @override
  $Res call({
    Object? businessServiceCount = freezed,
    Object? companyServiceDetails = freezed,
    Object? businessServiceTypes = freezed,
  }) {
    return _then(_$_CompanyServices(
      businessServiceCount: businessServiceCount == freezed
          ? _value.businessServiceCount
          : businessServiceCount // ignore: cast_nullable_to_non_nullable
              as int,
      companyServiceDetails: companyServiceDetails == freezed
          ? _value._companyServiceDetails
          : companyServiceDetails // ignore: cast_nullable_to_non_nullable
              as List<dynamic>,
      businessServiceTypes: businessServiceTypes == freezed
          ? _value._businessServiceTypes
          : businessServiceTypes // ignore: cast_nullable_to_non_nullable
              as List<ServiceTypesModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_CompanyServices implements _CompanyServices {
  _$_CompanyServices(
      {required this.businessServiceCount,
      required final List<dynamic> companyServiceDetails,
      required final List<ServiceTypesModel> businessServiceTypes})
      : _companyServiceDetails = companyServiceDetails,
        _businessServiceTypes = businessServiceTypes;

  factory _$_CompanyServices.fromJson(Map<String, dynamic> json) =>
      _$$_CompanyServicesFromJson(json);

  @override
  final int businessServiceCount;
  final List<dynamic> _companyServiceDetails;
  @override
  List<dynamic> get companyServiceDetails {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_companyServiceDetails);
  }

  final List<ServiceTypesModel> _businessServiceTypes;
  @override
  List<ServiceTypesModel> get businessServiceTypes {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_businessServiceTypes);
  }

  @override
  String toString() {
    return 'CompanyServices(businessServiceCount: $businessServiceCount, companyServiceDetails: $companyServiceDetails, businessServiceTypes: $businessServiceTypes)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CompanyServices &&
            const DeepCollectionEquality()
                .equals(other.businessServiceCount, businessServiceCount) &&
            const DeepCollectionEquality()
                .equals(other._companyServiceDetails, _companyServiceDetails) &&
            const DeepCollectionEquality()
                .equals(other._businessServiceTypes, _businessServiceTypes));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(businessServiceCount),
      const DeepCollectionEquality().hash(_companyServiceDetails),
      const DeepCollectionEquality().hash(_businessServiceTypes));

  @JsonKey(ignore: true)
  @override
  _$$_CompanyServicesCopyWith<_$_CompanyServices> get copyWith =>
      __$$_CompanyServicesCopyWithImpl<_$_CompanyServices>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CompanyServicesToJson(
      this,
    );
  }
}

abstract class _CompanyServices implements CompanyServices {
  factory _CompanyServices(
          {required final int businessServiceCount,
          required final List<dynamic> companyServiceDetails,
          required final List<ServiceTypesModel> businessServiceTypes}) =
      _$_CompanyServices;

  factory _CompanyServices.fromJson(Map<String, dynamic> json) =
      _$_CompanyServices.fromJson;

  @override
  int get businessServiceCount;
  @override
  List<dynamic> get companyServiceDetails;
  @override
  List<ServiceTypesModel> get businessServiceTypes;
  @override
  @JsonKey(ignore: true)
  _$$_CompanyServicesCopyWith<_$_CompanyServices> get copyWith =>
      throw _privateConstructorUsedError;
}
