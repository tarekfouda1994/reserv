import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../staff_model/staff_model.dart';

part 'service_list_model.freezed.dart';
part 'service_list_model.g.dart';

@unfreezed
class ServiceListModel with _$ServiceListModel {
  ServiceListModel._();

  @JsonSerializable(explicitToJson: true)
  factory ServiceListModel({
    @JsonKey(defaultValue: "", nullable: true) required String serviceTypeId,
    required String businessServiceID,
    required double rating,
    @JsonKey(defaultValue: "", nullable: true) required String staffId,
    @JsonKey(defaultValue: 0, nullable: true) required int staffBufferTime,
    @JsonKey(defaultValue: "", nullable: true) required String staffName,
    required String serviceEnglishName,
    required String serviceArabicName,
    required String englishDescription,
    required String serviceGender,
    required String arabicDescription,
    @JsonKey(defaultValue: "-", nullable: true)
        required String selectedTimeForService,
    required int serviceDuration,
    required String serviceDurationText,
    @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
        required num price,
    required String discountPercentage,
    required String currencyNameAR,
    required String currencyNameEN,
    @JsonKey(defaultValue: "", nullable: true) required String actualFees,
    @JsonKey(defaultValue: false, nullable: true) required bool selected,
    required bool hasWishItem,
    @JsonKey(defaultValue: "", nullable: true) required String serviceId,
    required List<String> picture,
    required List<StaffModel> staffDetail,
  }) = _ServiceListModel;

  factory ServiceListModel.fromJson(Map<String, dynamic> json) =>
      _$ServiceListModelFromJson(json);

  String getServiceName() {
    return getIt<Utilities>()
        .getLocalizedValue(serviceArabicName, serviceEnglishName);
  }

  String getServiceDetails() {
    return getIt<Utilities>()
        .getLocalizedValue(arabicDescription, englishDescription);
  }

  String getCurrencyName() {
    return getIt<Utilities>().getLocalizedValue(currencyNameAR, currencyNameEN);
  }

  String getServiceDescription() {
    return getIt<Utilities>()
        .getLocalizedValue(arabicDescription, englishDescription);
  }

  int get getStaffBufferTime => staffBufferTime = 10;

  String getServiceGender() {
    switch (serviceGender) {
      case "Male":
        return tr("men");
      case "Female":
        return tr("women");
      default:
        return "";
    }
  }
}
