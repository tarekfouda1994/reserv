// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'service_list_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceListModel _$ServiceListModelFromJson(Map<String, dynamic> json) {
  return _ServiceListModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceListModel {
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceTypeId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceTypeId(String value) => throw _privateConstructorUsedError;
  String get businessServiceID => throw _privateConstructorUsedError;
  set businessServiceID(String value) => throw _privateConstructorUsedError;
  double get rating => throw _privateConstructorUsedError;
  set rating(double value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get staffId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set staffId(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  int get staffBufferTime => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set staffBufferTime(int value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get staffName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set staffName(String value) => throw _privateConstructorUsedError;
  String get serviceEnglishName => throw _privateConstructorUsedError;
  set serviceEnglishName(String value) => throw _privateConstructorUsedError;
  String get serviceArabicName => throw _privateConstructorUsedError;
  set serviceArabicName(String value) => throw _privateConstructorUsedError;
  String get englishDescription => throw _privateConstructorUsedError;
  set englishDescription(String value) => throw _privateConstructorUsedError;
  String get serviceGender => throw _privateConstructorUsedError;
  set serviceGender(String value) => throw _privateConstructorUsedError;
  String get arabicDescription => throw _privateConstructorUsedError;
  set arabicDescription(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "-", nullable: true)
  String get selectedTimeForService => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "-", nullable: true)
  set selectedTimeForService(String value) =>
      throw _privateConstructorUsedError;
  int get serviceDuration => throw _privateConstructorUsedError;
  set serviceDuration(int value) => throw _privateConstructorUsedError;
  String get serviceDurationText => throw _privateConstructorUsedError;
  set serviceDurationText(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
  num get price => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
  set price(num value) => throw _privateConstructorUsedError;
  String get discountPercentage => throw _privateConstructorUsedError;
  set discountPercentage(String value) => throw _privateConstructorUsedError;
  String get currencyNameAR => throw _privateConstructorUsedError;
  set currencyNameAR(String value) => throw _privateConstructorUsedError;
  String get currencyNameEN => throw _privateConstructorUsedError;
  set currencyNameEN(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get actualFees => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set actualFees(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  bool get selected => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: false, nullable: true)
  set selected(bool value) => throw _privateConstructorUsedError;
  bool get hasWishItem => throw _privateConstructorUsedError;
  set hasWishItem(bool value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceId(String value) => throw _privateConstructorUsedError;
  List<String> get picture => throw _privateConstructorUsedError;
  set picture(List<String> value) => throw _privateConstructorUsedError;
  List<StaffModel> get staffDetail => throw _privateConstructorUsedError;
  set staffDetail(List<StaffModel> value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceListModelCopyWith<ServiceListModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceListModelCopyWith<$Res> {
  factory $ServiceListModelCopyWith(
          ServiceListModel value, $Res Function(ServiceListModel) then) =
      _$ServiceListModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(defaultValue: "", nullable: true) String serviceTypeId,
      String businessServiceID,
      double rating,
      @JsonKey(defaultValue: "", nullable: true) String staffId,
      @JsonKey(defaultValue: 0, nullable: true) int staffBufferTime,
      @JsonKey(defaultValue: "", nullable: true) String staffName,
      String serviceEnglishName,
      String serviceArabicName,
      String englishDescription,
      String serviceGender,
      String arabicDescription,
      @JsonKey(defaultValue: "-", nullable: true) String selectedTimeForService,
      int serviceDuration,
      String serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice") num price,
      String discountPercentage,
      String currencyNameAR,
      String currencyNameEN,
      @JsonKey(defaultValue: "", nullable: true) String actualFees,
      @JsonKey(defaultValue: false, nullable: true) bool selected,
      bool hasWishItem,
      @JsonKey(defaultValue: "", nullable: true) String serviceId,
      List<String> picture,
      List<StaffModel> staffDetail});
}

/// @nodoc
class _$ServiceListModelCopyWithImpl<$Res>
    implements $ServiceListModelCopyWith<$Res> {
  _$ServiceListModelCopyWithImpl(this._value, this._then);

  final ServiceListModel _value;
  // ignore: unused_field
  final $Res Function(ServiceListModel) _then;

  @override
  $Res call({
    Object? serviceTypeId = freezed,
    Object? businessServiceID = freezed,
    Object? rating = freezed,
    Object? staffId = freezed,
    Object? staffBufferTime = freezed,
    Object? staffName = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? englishDescription = freezed,
    Object? serviceGender = freezed,
    Object? arabicDescription = freezed,
    Object? selectedTimeForService = freezed,
    Object? serviceDuration = freezed,
    Object? serviceDurationText = freezed,
    Object? price = freezed,
    Object? discountPercentage = freezed,
    Object? currencyNameAR = freezed,
    Object? currencyNameEN = freezed,
    Object? actualFees = freezed,
    Object? selected = freezed,
    Object? hasWishItem = freezed,
    Object? serviceId = freezed,
    Object? picture = freezed,
    Object? staffDetail = freezed,
  }) {
    return _then(_value.copyWith(
      serviceTypeId: serviceTypeId == freezed
          ? _value.serviceTypeId
          : serviceTypeId // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      staffId: staffId == freezed
          ? _value.staffId
          : staffId // ignore: cast_nullable_to_non_nullable
              as String,
      staffBufferTime: staffBufferTime == freezed
          ? _value.staffBufferTime
          : staffBufferTime // ignore: cast_nullable_to_non_nullable
              as int,
      staffName: staffName == freezed
          ? _value.staffName
          : staffName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishDescription: englishDescription == freezed
          ? _value.englishDescription
          : englishDescription // ignore: cast_nullable_to_non_nullable
              as String,
      serviceGender: serviceGender == freezed
          ? _value.serviceGender
          : serviceGender // ignore: cast_nullable_to_non_nullable
              as String,
      arabicDescription: arabicDescription == freezed
          ? _value.arabicDescription
          : arabicDescription // ignore: cast_nullable_to_non_nullable
              as String,
      selectedTimeForService: selectedTimeForService == freezed
          ? _value.selectedTimeForService
          : selectedTimeForService // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as num,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as String,
      currencyNameAR: currencyNameAR == freezed
          ? _value.currencyNameAR
          : currencyNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      currencyNameEN: currencyNameEN == freezed
          ? _value.currencyNameEN
          : currencyNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      actualFees: actualFees == freezed
          ? _value.actualFees
          : actualFees // ignore: cast_nullable_to_non_nullable
              as String,
      selected: selected == freezed
          ? _value.selected
          : selected // ignore: cast_nullable_to_non_nullable
              as bool,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      serviceId: serviceId == freezed
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
      picture: picture == freezed
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as List<String>,
      staffDetail: staffDetail == freezed
          ? _value.staffDetail
          : staffDetail // ignore: cast_nullable_to_non_nullable
              as List<StaffModel>,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceListModelCopyWith<$Res>
    implements $ServiceListModelCopyWith<$Res> {
  factory _$$_ServiceListModelCopyWith(
          _$_ServiceListModel value, $Res Function(_$_ServiceListModel) then) =
      __$$_ServiceListModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(defaultValue: "", nullable: true) String serviceTypeId,
      String businessServiceID,
      double rating,
      @JsonKey(defaultValue: "", nullable: true) String staffId,
      @JsonKey(defaultValue: 0, nullable: true) int staffBufferTime,
      @JsonKey(defaultValue: "", nullable: true) String staffName,
      String serviceEnglishName,
      String serviceArabicName,
      String englishDescription,
      String serviceGender,
      String arabicDescription,
      @JsonKey(defaultValue: "-", nullable: true) String selectedTimeForService,
      int serviceDuration,
      String serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice") num price,
      String discountPercentage,
      String currencyNameAR,
      String currencyNameEN,
      @JsonKey(defaultValue: "", nullable: true) String actualFees,
      @JsonKey(defaultValue: false, nullable: true) bool selected,
      bool hasWishItem,
      @JsonKey(defaultValue: "", nullable: true) String serviceId,
      List<String> picture,
      List<StaffModel> staffDetail});
}

/// @nodoc
class __$$_ServiceListModelCopyWithImpl<$Res>
    extends _$ServiceListModelCopyWithImpl<$Res>
    implements _$$_ServiceListModelCopyWith<$Res> {
  __$$_ServiceListModelCopyWithImpl(
      _$_ServiceListModel _value, $Res Function(_$_ServiceListModel) _then)
      : super(_value, (v) => _then(v as _$_ServiceListModel));

  @override
  _$_ServiceListModel get _value => super._value as _$_ServiceListModel;

  @override
  $Res call({
    Object? serviceTypeId = freezed,
    Object? businessServiceID = freezed,
    Object? rating = freezed,
    Object? staffId = freezed,
    Object? staffBufferTime = freezed,
    Object? staffName = freezed,
    Object? serviceEnglishName = freezed,
    Object? serviceArabicName = freezed,
    Object? englishDescription = freezed,
    Object? serviceGender = freezed,
    Object? arabicDescription = freezed,
    Object? selectedTimeForService = freezed,
    Object? serviceDuration = freezed,
    Object? serviceDurationText = freezed,
    Object? price = freezed,
    Object? discountPercentage = freezed,
    Object? currencyNameAR = freezed,
    Object? currencyNameEN = freezed,
    Object? actualFees = freezed,
    Object? selected = freezed,
    Object? hasWishItem = freezed,
    Object? serviceId = freezed,
    Object? picture = freezed,
    Object? staffDetail = freezed,
  }) {
    return _then(_$_ServiceListModel(
      serviceTypeId: serviceTypeId == freezed
          ? _value.serviceTypeId
          : serviceTypeId // ignore: cast_nullable_to_non_nullable
              as String,
      businessServiceID: businessServiceID == freezed
          ? _value.businessServiceID
          : businessServiceID // ignore: cast_nullable_to_non_nullable
              as String,
      rating: rating == freezed
          ? _value.rating
          : rating // ignore: cast_nullable_to_non_nullable
              as double,
      staffId: staffId == freezed
          ? _value.staffId
          : staffId // ignore: cast_nullable_to_non_nullable
              as String,
      staffBufferTime: staffBufferTime == freezed
          ? _value.staffBufferTime
          : staffBufferTime // ignore: cast_nullable_to_non_nullable
              as int,
      staffName: staffName == freezed
          ? _value.staffName
          : staffName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceEnglishName: serviceEnglishName == freezed
          ? _value.serviceEnglishName
          : serviceEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      serviceArabicName: serviceArabicName == freezed
          ? _value.serviceArabicName
          : serviceArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishDescription: englishDescription == freezed
          ? _value.englishDescription
          : englishDescription // ignore: cast_nullable_to_non_nullable
              as String,
      serviceGender: serviceGender == freezed
          ? _value.serviceGender
          : serviceGender // ignore: cast_nullable_to_non_nullable
              as String,
      arabicDescription: arabicDescription == freezed
          ? _value.arabicDescription
          : arabicDescription // ignore: cast_nullable_to_non_nullable
              as String,
      selectedTimeForService: selectedTimeForService == freezed
          ? _value.selectedTimeForService
          : selectedTimeForService // ignore: cast_nullable_to_non_nullable
              as String,
      serviceDuration: serviceDuration == freezed
          ? _value.serviceDuration
          : serviceDuration // ignore: cast_nullable_to_non_nullable
              as int,
      serviceDurationText: serviceDurationText == freezed
          ? _value.serviceDurationText
          : serviceDurationText // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as num,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as String,
      currencyNameAR: currencyNameAR == freezed
          ? _value.currencyNameAR
          : currencyNameAR // ignore: cast_nullable_to_non_nullable
              as String,
      currencyNameEN: currencyNameEN == freezed
          ? _value.currencyNameEN
          : currencyNameEN // ignore: cast_nullable_to_non_nullable
              as String,
      actualFees: actualFees == freezed
          ? _value.actualFees
          : actualFees // ignore: cast_nullable_to_non_nullable
              as String,
      selected: selected == freezed
          ? _value.selected
          : selected // ignore: cast_nullable_to_non_nullable
              as bool,
      hasWishItem: hasWishItem == freezed
          ? _value.hasWishItem
          : hasWishItem // ignore: cast_nullable_to_non_nullable
              as bool,
      serviceId: serviceId == freezed
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
      picture: picture == freezed
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as List<String>,
      staffDetail: staffDetail == freezed
          ? _value.staffDetail
          : staffDetail // ignore: cast_nullable_to_non_nullable
              as List<StaffModel>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceListModel extends _ServiceListModel {
  _$_ServiceListModel(
      {@JsonKey(defaultValue: "", nullable: true)
          required this.serviceTypeId,
      required this.businessServiceID,
      required this.rating,
      @JsonKey(defaultValue: "", nullable: true)
          required this.staffId,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.staffBufferTime,
      @JsonKey(defaultValue: "", nullable: true)
          required this.staffName,
      required this.serviceEnglishName,
      required this.serviceArabicName,
      required this.englishDescription,
      required this.serviceGender,
      required this.arabicDescription,
      @JsonKey(defaultValue: "-", nullable: true)
          required this.selectedTimeForService,
      required this.serviceDuration,
      required this.serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
          required this.price,
      required this.discountPercentage,
      required this.currencyNameAR,
      required this.currencyNameEN,
      @JsonKey(defaultValue: "", nullable: true)
          required this.actualFees,
      @JsonKey(defaultValue: false, nullable: true)
          required this.selected,
      required this.hasWishItem,
      @JsonKey(defaultValue: "", nullable: true)
          required this.serviceId,
      required this.picture,
      required this.staffDetail})
      : super._();

  factory _$_ServiceListModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceListModelFromJson(json);

  @override
  @JsonKey(defaultValue: "", nullable: true)
  String serviceTypeId;
  @override
  String businessServiceID;
  @override
  double rating;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String staffId;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int staffBufferTime;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String staffName;
  @override
  String serviceEnglishName;
  @override
  String serviceArabicName;
  @override
  String englishDescription;
  @override
  String serviceGender;
  @override
  String arabicDescription;
  @override
  @JsonKey(defaultValue: "-", nullable: true)
  String selectedTimeForService;
  @override
  int serviceDuration;
  @override
  String serviceDurationText;
  @override
  @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
  num price;
  @override
  String discountPercentage;
  @override
  String currencyNameAR;
  @override
  String currencyNameEN;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String actualFees;
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool selected;
  @override
  bool hasWishItem;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String serviceId;
  @override
  List<String> picture;
  @override
  List<StaffModel> staffDetail;

  @override
  String toString() {
    return 'ServiceListModel(serviceTypeId: $serviceTypeId, businessServiceID: $businessServiceID, rating: $rating, staffId: $staffId, staffBufferTime: $staffBufferTime, staffName: $staffName, serviceEnglishName: $serviceEnglishName, serviceArabicName: $serviceArabicName, englishDescription: $englishDescription, serviceGender: $serviceGender, arabicDescription: $arabicDescription, selectedTimeForService: $selectedTimeForService, serviceDuration: $serviceDuration, serviceDurationText: $serviceDurationText, price: $price, discountPercentage: $discountPercentage, currencyNameAR: $currencyNameAR, currencyNameEN: $currencyNameEN, actualFees: $actualFees, selected: $selected, hasWishItem: $hasWishItem, serviceId: $serviceId, picture: $picture, staffDetail: $staffDetail)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_ServiceListModelCopyWith<_$_ServiceListModel> get copyWith =>
      __$$_ServiceListModelCopyWithImpl<_$_ServiceListModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceListModelToJson(
      this,
    );
  }
}

abstract class _ServiceListModel extends ServiceListModel {
  factory _ServiceListModel(
      {@JsonKey(defaultValue: "", nullable: true)
          required String serviceTypeId,
      required String businessServiceID,
      required double rating,
      @JsonKey(defaultValue: "", nullable: true)
          required String staffId,
      @JsonKey(defaultValue: 0, nullable: true)
          required int staffBufferTime,
      @JsonKey(defaultValue: "", nullable: true)
          required String staffName,
      required String serviceEnglishName,
      required String serviceArabicName,
      required String englishDescription,
      required String serviceGender,
      required String arabicDescription,
      @JsonKey(defaultValue: "-", nullable: true)
          required String selectedTimeForService,
      required int serviceDuration,
      required String serviceDurationText,
      @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
          required num price,
      required String discountPercentage,
      required String currencyNameAR,
      required String currencyNameEN,
      @JsonKey(defaultValue: "", nullable: true)
          required String actualFees,
      @JsonKey(defaultValue: false, nullable: true)
          required bool selected,
      required bool hasWishItem,
      @JsonKey(defaultValue: "", nullable: true)
          required String serviceId,
      required List<String> picture,
      required List<StaffModel> staffDetail}) = _$_ServiceListModel;
  _ServiceListModel._() : super._();

  factory _ServiceListModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceListModel.fromJson;

  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceTypeId;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceTypeId(String value);
  @override
  String get businessServiceID;
  set businessServiceID(String value);
  @override
  double get rating;
  set rating(double value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get staffId;
  @JsonKey(defaultValue: "", nullable: true)
  set staffId(String value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int get staffBufferTime;
  @JsonKey(defaultValue: 0, nullable: true)
  set staffBufferTime(int value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get staffName;
  @JsonKey(defaultValue: "", nullable: true)
  set staffName(String value);
  @override
  String get serviceEnglishName;
  set serviceEnglishName(String value);
  @override
  String get serviceArabicName;
  set serviceArabicName(String value);
  @override
  String get englishDescription;
  set englishDescription(String value);
  @override
  String get serviceGender;
  set serviceGender(String value);
  @override
  String get arabicDescription;
  set arabicDescription(String value);
  @override
  @JsonKey(defaultValue: "-", nullable: true)
  String get selectedTimeForService;
  @JsonKey(defaultValue: "-", nullable: true)
  set selectedTimeForService(String value);
  @override
  int get serviceDuration;
  set serviceDuration(int value);
  @override
  String get serviceDurationText;
  set serviceDurationText(String value);
  @override
  @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
  num get price;
  @JsonKey(defaultValue: 0, nullable: true, name: "servicePrice")
  set price(num value);
  @override
  String get discountPercentage;
  set discountPercentage(String value);
  @override
  String get currencyNameAR;
  set currencyNameAR(String value);
  @override
  String get currencyNameEN;
  set currencyNameEN(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get actualFees;
  @JsonKey(defaultValue: "", nullable: true)
  set actualFees(String value);
  @override
  @JsonKey(defaultValue: false, nullable: true)
  bool get selected;
  @JsonKey(defaultValue: false, nullable: true)
  set selected(bool value);
  @override
  bool get hasWishItem;
  set hasWishItem(bool value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get serviceId;
  @JsonKey(defaultValue: "", nullable: true)
  set serviceId(String value);
  @override
  List<String> get picture;
  set picture(List<String> value);
  @override
  List<StaffModel> get staffDetail;
  set staffDetail(List<StaffModel> value);
  @override
  @JsonKey(ignore: true)
  _$$_ServiceListModelCopyWith<_$_ServiceListModel> get copyWith =>
      throw _privateConstructorUsedError;
}
