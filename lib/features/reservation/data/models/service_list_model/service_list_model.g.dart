// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceListModel _$$_ServiceListModelFromJson(Map<String, dynamic> json) =>
    _$_ServiceListModel(
      serviceTypeId: json['serviceTypeId'] as String? ?? '',
      businessServiceID: json['businessServiceID'] as String,
      rating: (json['rating'] as num).toDouble(),
      staffId: json['staffId'] as String? ?? '',
      staffBufferTime: json['staffBufferTime'] as int? ?? 0,
      staffName: json['staffName'] as String? ?? '',
      serviceEnglishName: json['serviceEnglishName'] as String,
      serviceArabicName: json['serviceArabicName'] as String,
      englishDescription: json['englishDescription'] as String,
      serviceGender: json['serviceGender'] as String,
      arabicDescription: json['arabicDescription'] as String,
      selectedTimeForService: json['selectedTimeForService'] as String? ?? '-',
      serviceDuration: json['serviceDuration'] as int,
      serviceDurationText: json['serviceDurationText'] as String,
      price: json['servicePrice'] as num? ?? 0,
      discountPercentage: json['discountPercentage'] as String,
      currencyNameAR: json['currencyNameAR'] as String,
      currencyNameEN: json['currencyNameEN'] as String,
      actualFees: json['actualFees'] as String? ?? '',
      selected: json['selected'] as bool? ?? false,
      hasWishItem: json['hasWishItem'] as bool,
      serviceId: json['serviceId'] as String? ?? '',
      picture:
          (json['picture'] as List<dynamic>).map((e) => e as String).toList(),
      staffDetail: (json['staffDetail'] as List<dynamic>)
          .map((e) => StaffModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ServiceListModelToJson(_$_ServiceListModel instance) =>
    <String, dynamic>{
      'serviceTypeId': instance.serviceTypeId,
      'businessServiceID': instance.businessServiceID,
      'rating': instance.rating,
      'staffId': instance.staffId,
      'staffBufferTime': instance.staffBufferTime,
      'staffName': instance.staffName,
      'serviceEnglishName': instance.serviceEnglishName,
      'serviceArabicName': instance.serviceArabicName,
      'englishDescription': instance.englishDescription,
      'serviceGender': instance.serviceGender,
      'arabicDescription': instance.arabicDescription,
      'selectedTimeForService': instance.selectedTimeForService,
      'serviceDuration': instance.serviceDuration,
      'serviceDurationText': instance.serviceDurationText,
      'servicePrice': instance.price,
      'discountPercentage': instance.discountPercentage,
      'currencyNameAR': instance.currencyNameAR,
      'currencyNameEN': instance.currencyNameEN,
      'actualFees': instance.actualFees,
      'selected': instance.selected,
      'hasWishItem': instance.hasWishItem,
      'serviceId': instance.serviceId,
      'picture': instance.picture,
      'staffDetail': instance.staffDetail.map((e) => e.toJson()).toList(),
    };
