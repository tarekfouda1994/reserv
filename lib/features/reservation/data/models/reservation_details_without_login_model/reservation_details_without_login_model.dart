import 'package:flutter_tdd/features/search/data/models/appointment_orders/appointment_orders.dart';

class ReservationDetailsWithoutLoginModel {
  String businessName;
  String date;
  List<String> reservationTimes;
  String refNumber;
  AppointmentOrdersModel appointmentOrdersModel;

  ReservationDetailsWithoutLoginModel(
      {required this.date,
      required this.businessName,
      required this.refNumber,
      required this.reservationTimes,
      required this.appointmentOrdersModel});
}
