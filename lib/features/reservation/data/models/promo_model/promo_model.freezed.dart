// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'promo_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PromoModel _$PromoModelFromJson(Map<String, dynamic> json) {
  return _PromoModel.fromJson(json);
}

/// @nodoc
mixin _$PromoModel {
  String get serviceId => throw _privateConstructorUsedError;
  num? get discount => throw _privateConstructorUsedError;
  num? get discountPercentage => throw _privateConstructorUsedError;
  bool get byValue => throw _privateConstructorUsedError;
  int? get currencyId => throw _privateConstructorUsedError;
  String get promoCode => throw _privateConstructorUsedError;
  String get validUntilDate => throw _privateConstructorUsedError;
  num get minimumSpendAmount => throw _privateConstructorUsedError;
  num get maximumDiscountAmount => throw _privateConstructorUsedError;
  String? get currencyArabicName => throw _privateConstructorUsedError;
  String? get currencyEnglishName => throw _privateConstructorUsedError;
  String get id => throw _privateConstructorUsedError;
  String get arabicName => throw _privateConstructorUsedError;
  String get englishName => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PromoModelCopyWith<PromoModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PromoModelCopyWith<$Res> {
  factory $PromoModelCopyWith(
          PromoModel value, $Res Function(PromoModel) then) =
      _$PromoModelCopyWithImpl<$Res>;
  $Res call(
      {String serviceId,
      num? discount,
      num? discountPercentage,
      bool byValue,
      int? currencyId,
      String promoCode,
      String validUntilDate,
      num minimumSpendAmount,
      num maximumDiscountAmount,
      String? currencyArabicName,
      String? currencyEnglishName,
      String id,
      String arabicName,
      String englishName,
      bool isActive});
}

/// @nodoc
class _$PromoModelCopyWithImpl<$Res> implements $PromoModelCopyWith<$Res> {
  _$PromoModelCopyWithImpl(this._value, this._then);

  final PromoModel _value;
  // ignore: unused_field
  final $Res Function(PromoModel) _then;

  @override
  $Res call({
    Object? serviceId = freezed,
    Object? discount = freezed,
    Object? discountPercentage = freezed,
    Object? byValue = freezed,
    Object? currencyId = freezed,
    Object? promoCode = freezed,
    Object? validUntilDate = freezed,
    Object? minimumSpendAmount = freezed,
    Object? maximumDiscountAmount = freezed,
    Object? currencyArabicName = freezed,
    Object? currencyEnglishName = freezed,
    Object? id = freezed,
    Object? arabicName = freezed,
    Object? englishName = freezed,
    Object? isActive = freezed,
  }) {
    return _then(_value.copyWith(
      serviceId: serviceId == freezed
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as num?,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as num?,
      byValue: byValue == freezed
          ? _value.byValue
          : byValue // ignore: cast_nullable_to_non_nullable
              as bool,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as int?,
      promoCode: promoCode == freezed
          ? _value.promoCode
          : promoCode // ignore: cast_nullable_to_non_nullable
              as String,
      validUntilDate: validUntilDate == freezed
          ? _value.validUntilDate
          : validUntilDate // ignore: cast_nullable_to_non_nullable
              as String,
      minimumSpendAmount: minimumSpendAmount == freezed
          ? _value.minimumSpendAmount
          : minimumSpendAmount // ignore: cast_nullable_to_non_nullable
              as num,
      maximumDiscountAmount: maximumDiscountAmount == freezed
          ? _value.maximumDiscountAmount
          : maximumDiscountAmount // ignore: cast_nullable_to_non_nullable
              as num,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String?,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_PromoModelCopyWith<$Res>
    implements $PromoModelCopyWith<$Res> {
  factory _$$_PromoModelCopyWith(
          _$_PromoModel value, $Res Function(_$_PromoModel) then) =
      __$$_PromoModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String serviceId,
      num? discount,
      num? discountPercentage,
      bool byValue,
      int? currencyId,
      String promoCode,
      String validUntilDate,
      num minimumSpendAmount,
      num maximumDiscountAmount,
      String? currencyArabicName,
      String? currencyEnglishName,
      String id,
      String arabicName,
      String englishName,
      bool isActive});
}

/// @nodoc
class __$$_PromoModelCopyWithImpl<$Res> extends _$PromoModelCopyWithImpl<$Res>
    implements _$$_PromoModelCopyWith<$Res> {
  __$$_PromoModelCopyWithImpl(
      _$_PromoModel _value, $Res Function(_$_PromoModel) _then)
      : super(_value, (v) => _then(v as _$_PromoModel));

  @override
  _$_PromoModel get _value => super._value as _$_PromoModel;

  @override
  $Res call({
    Object? serviceId = freezed,
    Object? discount = freezed,
    Object? discountPercentage = freezed,
    Object? byValue = freezed,
    Object? currencyId = freezed,
    Object? promoCode = freezed,
    Object? validUntilDate = freezed,
    Object? minimumSpendAmount = freezed,
    Object? maximumDiscountAmount = freezed,
    Object? currencyArabicName = freezed,
    Object? currencyEnglishName = freezed,
    Object? id = freezed,
    Object? arabicName = freezed,
    Object? englishName = freezed,
    Object? isActive = freezed,
  }) {
    return _then(_$_PromoModel(
      serviceId: serviceId == freezed
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
      discount: discount == freezed
          ? _value.discount
          : discount // ignore: cast_nullable_to_non_nullable
              as num?,
      discountPercentage: discountPercentage == freezed
          ? _value.discountPercentage
          : discountPercentage // ignore: cast_nullable_to_non_nullable
              as num?,
      byValue: byValue == freezed
          ? _value.byValue
          : byValue // ignore: cast_nullable_to_non_nullable
              as bool,
      currencyId: currencyId == freezed
          ? _value.currencyId
          : currencyId // ignore: cast_nullable_to_non_nullable
              as int?,
      promoCode: promoCode == freezed
          ? _value.promoCode
          : promoCode // ignore: cast_nullable_to_non_nullable
              as String,
      validUntilDate: validUntilDate == freezed
          ? _value.validUntilDate
          : validUntilDate // ignore: cast_nullable_to_non_nullable
              as String,
      minimumSpendAmount: minimumSpendAmount == freezed
          ? _value.minimumSpendAmount
          : minimumSpendAmount // ignore: cast_nullable_to_non_nullable
              as num,
      maximumDiscountAmount: maximumDiscountAmount == freezed
          ? _value.maximumDiscountAmount
          : maximumDiscountAmount // ignore: cast_nullable_to_non_nullable
              as num,
      currencyArabicName: currencyArabicName == freezed
          ? _value.currencyArabicName
          : currencyArabicName // ignore: cast_nullable_to_non_nullable
              as String?,
      currencyEnglishName: currencyEnglishName == freezed
          ? _value.currencyEnglishName
          : currencyEnglishName // ignore: cast_nullable_to_non_nullable
              as String?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      arabicName: arabicName == freezed
          ? _value.arabicName
          : arabicName // ignore: cast_nullable_to_non_nullable
              as String,
      englishName: englishName == freezed
          ? _value.englishName
          : englishName // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_PromoModel extends _PromoModel {
  _$_PromoModel(
      {required this.serviceId,
      required this.discount,
      required this.discountPercentage,
      required this.byValue,
      required this.currencyId,
      required this.promoCode,
      required this.validUntilDate,
      required this.minimumSpendAmount,
      required this.maximumDiscountAmount,
      required this.currencyArabicName,
      required this.currencyEnglishName,
      required this.id,
      required this.arabicName,
      required this.englishName,
      required this.isActive})
      : super._();

  factory _$_PromoModel.fromJson(Map<String, dynamic> json) =>
      _$$_PromoModelFromJson(json);

  @override
  final String serviceId;
  @override
  final num? discount;
  @override
  final num? discountPercentage;
  @override
  final bool byValue;
  @override
  final int? currencyId;
  @override
  final String promoCode;
  @override
  final String validUntilDate;
  @override
  final num minimumSpendAmount;
  @override
  final num maximumDiscountAmount;
  @override
  final String? currencyArabicName;
  @override
  final String? currencyEnglishName;
  @override
  final String id;
  @override
  final String arabicName;
  @override
  final String englishName;
  @override
  final bool isActive;

  @override
  String toString() {
    return 'PromoModel(serviceId: $serviceId, discount: $discount, discountPercentage: $discountPercentage, byValue: $byValue, currencyId: $currencyId, promoCode: $promoCode, validUntilDate: $validUntilDate, minimumSpendAmount: $minimumSpendAmount, maximumDiscountAmount: $maximumDiscountAmount, currencyArabicName: $currencyArabicName, currencyEnglishName: $currencyEnglishName, id: $id, arabicName: $arabicName, englishName: $englishName, isActive: $isActive)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PromoModel &&
            const DeepCollectionEquality().equals(other.serviceId, serviceId) &&
            const DeepCollectionEquality().equals(other.discount, discount) &&
            const DeepCollectionEquality()
                .equals(other.discountPercentage, discountPercentage) &&
            const DeepCollectionEquality().equals(other.byValue, byValue) &&
            const DeepCollectionEquality()
                .equals(other.currencyId, currencyId) &&
            const DeepCollectionEquality().equals(other.promoCode, promoCode) &&
            const DeepCollectionEquality()
                .equals(other.validUntilDate, validUntilDate) &&
            const DeepCollectionEquality()
                .equals(other.minimumSpendAmount, minimumSpendAmount) &&
            const DeepCollectionEquality()
                .equals(other.maximumDiscountAmount, maximumDiscountAmount) &&
            const DeepCollectionEquality()
                .equals(other.currencyArabicName, currencyArabicName) &&
            const DeepCollectionEquality()
                .equals(other.currencyEnglishName, currencyEnglishName) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality()
                .equals(other.arabicName, arabicName) &&
            const DeepCollectionEquality()
                .equals(other.englishName, englishName) &&
            const DeepCollectionEquality().equals(other.isActive, isActive));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(serviceId),
      const DeepCollectionEquality().hash(discount),
      const DeepCollectionEquality().hash(discountPercentage),
      const DeepCollectionEquality().hash(byValue),
      const DeepCollectionEquality().hash(currencyId),
      const DeepCollectionEquality().hash(promoCode),
      const DeepCollectionEquality().hash(validUntilDate),
      const DeepCollectionEquality().hash(minimumSpendAmount),
      const DeepCollectionEquality().hash(maximumDiscountAmount),
      const DeepCollectionEquality().hash(currencyArabicName),
      const DeepCollectionEquality().hash(currencyEnglishName),
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(arabicName),
      const DeepCollectionEquality().hash(englishName),
      const DeepCollectionEquality().hash(isActive));

  @JsonKey(ignore: true)
  @override
  _$$_PromoModelCopyWith<_$_PromoModel> get copyWith =>
      __$$_PromoModelCopyWithImpl<_$_PromoModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PromoModelToJson(
      this,
    );
  }
}

abstract class _PromoModel extends PromoModel {
  factory _PromoModel(
      {required final String serviceId,
      required final num? discount,
      required final num? discountPercentage,
      required final bool byValue,
      required final int? currencyId,
      required final String promoCode,
      required final String validUntilDate,
      required final num minimumSpendAmount,
      required final num maximumDiscountAmount,
      required final String? currencyArabicName,
      required final String? currencyEnglishName,
      required final String id,
      required final String arabicName,
      required final String englishName,
      required final bool isActive}) = _$_PromoModel;
  _PromoModel._() : super._();

  factory _PromoModel.fromJson(Map<String, dynamic> json) =
      _$_PromoModel.fromJson;

  @override
  String get serviceId;
  @override
  num? get discount;
  @override
  num? get discountPercentage;
  @override
  bool get byValue;
  @override
  int? get currencyId;
  @override
  String get promoCode;
  @override
  String get validUntilDate;
  @override
  num get minimumSpendAmount;
  @override
  num get maximumDiscountAmount;
  @override
  String? get currencyArabicName;
  @override
  String? get currencyEnglishName;
  @override
  String get id;
  @override
  String get arabicName;
  @override
  String get englishName;
  @override
  bool get isActive;
  @override
  @JsonKey(ignore: true)
  _$$_PromoModelCopyWith<_$_PromoModel> get copyWith =>
      throw _privateConstructorUsedError;
}
