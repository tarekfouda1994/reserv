// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'promo_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PromoModel _$$_PromoModelFromJson(Map<String, dynamic> json) =>
    _$_PromoModel(
      serviceId: json['serviceId'] as String,
      discount: json['discount'] as num?,
      discountPercentage: json['discountPercentage'] as num?,
      byValue: json['byValue'] as bool,
      currencyId: json['currencyId'] as int?,
      promoCode: json['promoCode'] as String,
      validUntilDate: json['validUntilDate'] as String,
      minimumSpendAmount: json['minimumSpendAmount'] as num,
      maximumDiscountAmount: json['maximumDiscountAmount'] as num,
      currencyArabicName: json['currencyArabicName'] as String?,
      currencyEnglishName: json['currencyEnglishName'] as String?,
      id: json['id'] as String,
      arabicName: json['arabicName'] as String,
      englishName: json['englishName'] as String,
      isActive: json['isActive'] as bool,
    );

Map<String, dynamic> _$$_PromoModelToJson(_$_PromoModel instance) =>
    <String, dynamic>{
      'serviceId': instance.serviceId,
      'discount': instance.discount,
      'discountPercentage': instance.discountPercentage,
      'byValue': instance.byValue,
      'currencyId': instance.currencyId,
      'promoCode': instance.promoCode,
      'validUntilDate': instance.validUntilDate,
      'minimumSpendAmount': instance.minimumSpendAmount,
      'maximumDiscountAmount': instance.maximumDiscountAmount,
      'currencyArabicName': instance.currencyArabicName,
      'currencyEnglishName': instance.currencyEnglishName,
      'id': instance.id,
      'arabicName': instance.arabicName,
      'englishName': instance.englishName,
      'isActive': instance.isActive,
    };
