import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../../core/helpers/di.dart';
import '../../../../../core/helpers/utilities.dart';

part 'promo_model.freezed.dart';
part 'promo_model.g.dart';

@freezed
class PromoModel with _$PromoModel {
  PromoModel._();

  @JsonSerializable(explicitToJson: true)
  factory PromoModel({
    required String serviceId,
    required num? discount,
    required num? discountPercentage,
    required bool byValue,
    required int? currencyId,
    required String promoCode,
    required String validUntilDate,
    required num minimumSpendAmount,
    required num maximumDiscountAmount,
    required String? currencyArabicName,
    required String? currencyEnglishName,
    required String id,
    required String arabicName,
    required String englishName,
    required bool isActive,
  }) = _PromoModel;

  factory PromoModel.fromJson(Map<String, dynamic> json) => _$PromoModelFromJson(json);

  String getCurrencyName() {
    return getIt<Utilities>().getLocalizedValue(currencyArabicName??"", currencyEnglishName??"");
  }

  String getName() {
    return getIt<Utilities>().getLocalizedValue(arabicName, englishName);
  }
}
