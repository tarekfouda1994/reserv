import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';

class ServiceTypesModel {
  String serviceTypeArabicName;
  String serviceTypeEnglishName;
  String serviceTypeId;
  String businessId;
  bool selected;
  List<ServiceListModel> servicesList;
  GlobalKey key;
  GlobalKey tabKey;

  ServiceTypesModel({
    required this.serviceTypeArabicName,
    required this.serviceTypeEnglishName,
    required this.serviceTypeId,
    required this.businessId,
    required this.servicesList,
    required this.selected,
    required this.key,
    required this.tabKey,
  });

  factory ServiceTypesModel.fromJson(Map<String, dynamic> json) => ServiceTypesModel(
        key: GlobalKey(),
        tabKey: GlobalKey(),
        selected: false,
        serviceTypeArabicName: json["serviceTypeArabicName"],
        serviceTypeEnglishName: json["serviceTypeEnglishName"],
        serviceTypeId: json["serviceTypeId"],
        businessId: json["businessId"],
        servicesList: List<ServiceListModel>.from(
            json["servicesList"].map((x) => ServiceListModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "serviceTypeArabicName": serviceTypeArabicName,
        "serviceTypeEnglishName": serviceTypeEnglishName,
        "serviceTypeId": serviceTypeId,
        "businessId": businessId,
        "servicesList": List<dynamic>.from(servicesList.map((x) => x.toJson())),
      };

  String getServiceTypeName() {
    return getIt<Utilities>().getLocalizedValue(serviceTypeArabicName, serviceTypeEnglishName);
  }
}
