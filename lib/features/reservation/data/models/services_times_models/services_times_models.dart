import 'package:freezed_annotation/freezed_annotation.dart';

part 'services_times_models.freezed.dart';
part 'services_times_models.g.dart';

@freezed
@immutable
class ServicesTimesModels with _$ServicesTimesModels{
  const ServicesTimesModels._();
  @JsonSerializable(explicitToJson: true)
  const factory ServicesTimesModels({
    required String businessID,
    required String reservationDate,
    required String? nearAvailableDate,
    required List<ServiceSlotsModels> servicesWithStaff,

  }) = _ServicesTimesModels;


  factory ServicesTimesModels.fromJson(Map<String, dynamic> json) =>
      _$ServicesTimesModelsFromJson(json);
}

@freezed
@immutable
class ServiceSlotsModels with _$ServiceSlotsModels{
  const ServiceSlotsModels._();
  @JsonSerializable(explicitToJson: true)
  const factory ServiceSlotsModels({
    required List<String> timeSlots,
  }) = _ServiceSlotsModels;


  factory ServiceSlotsModels.fromJson(Map<String, dynamic> json) =>
      _$ServiceSlotsModelsFromJson(json);
}