// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'services_times_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServicesTimesModels _$$_ServicesTimesModelsFromJson(
        Map<String, dynamic> json) =>
    _$_ServicesTimesModels(
      businessID: json['businessID'] as String,
      reservationDate: json['reservationDate'] as String,
      nearAvailableDate: json['nearAvailableDate'] as String?,
      servicesWithStaff: (json['servicesWithStaff'] as List<dynamic>)
          .map((e) => ServiceSlotsModels.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ServicesTimesModelsToJson(
        _$_ServicesTimesModels instance) =>
    <String, dynamic>{
      'businessID': instance.businessID,
      'reservationDate': instance.reservationDate,
      'nearAvailableDate': instance.nearAvailableDate,
      'servicesWithStaff':
          instance.servicesWithStaff.map((e) => e.toJson()).toList(),
    };

_$_ServiceSlotsModels _$$_ServiceSlotsModelsFromJson(
        Map<String, dynamic> json) =>
    _$_ServiceSlotsModels(
      timeSlots:
          (json['timeSlots'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$_ServiceSlotsModelsToJson(
        _$_ServiceSlotsModels instance) =>
    <String, dynamic>{
      'timeSlots': instance.timeSlots,
    };
