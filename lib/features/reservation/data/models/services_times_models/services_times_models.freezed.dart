// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'services_times_models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServicesTimesModels _$ServicesTimesModelsFromJson(Map<String, dynamic> json) {
  return _ServicesTimesModels.fromJson(json);
}

/// @nodoc
mixin _$ServicesTimesModels {
  String get businessID => throw _privateConstructorUsedError;
  String get reservationDate => throw _privateConstructorUsedError;
  String? get nearAvailableDate => throw _privateConstructorUsedError;
  List<ServiceSlotsModels> get servicesWithStaff =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServicesTimesModelsCopyWith<ServicesTimesModels> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServicesTimesModelsCopyWith<$Res> {
  factory $ServicesTimesModelsCopyWith(
          ServicesTimesModels value, $Res Function(ServicesTimesModels) then) =
      _$ServicesTimesModelsCopyWithImpl<$Res>;
  $Res call(
      {String businessID,
      String reservationDate,
      String? nearAvailableDate,
      List<ServiceSlotsModels> servicesWithStaff});
}

/// @nodoc
class _$ServicesTimesModelsCopyWithImpl<$Res>
    implements $ServicesTimesModelsCopyWith<$Res> {
  _$ServicesTimesModelsCopyWithImpl(this._value, this._then);

  final ServicesTimesModels _value;
  // ignore: unused_field
  final $Res Function(ServicesTimesModels) _then;

  @override
  $Res call({
    Object? businessID = freezed,
    Object? reservationDate = freezed,
    Object? nearAvailableDate = freezed,
    Object? servicesWithStaff = freezed,
  }) {
    return _then(_value.copyWith(
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      reservationDate: reservationDate == freezed
          ? _value.reservationDate
          : reservationDate // ignore: cast_nullable_to_non_nullable
              as String,
      nearAvailableDate: nearAvailableDate == freezed
          ? _value.nearAvailableDate
          : nearAvailableDate // ignore: cast_nullable_to_non_nullable
              as String?,
      servicesWithStaff: servicesWithStaff == freezed
          ? _value.servicesWithStaff
          : servicesWithStaff // ignore: cast_nullable_to_non_nullable
              as List<ServiceSlotsModels>,
    ));
  }
}

/// @nodoc
abstract class _$$_ServicesTimesModelsCopyWith<$Res>
    implements $ServicesTimesModelsCopyWith<$Res> {
  factory _$$_ServicesTimesModelsCopyWith(_$_ServicesTimesModels value,
          $Res Function(_$_ServicesTimesModels) then) =
      __$$_ServicesTimesModelsCopyWithImpl<$Res>;
  @override
  $Res call(
      {String businessID,
      String reservationDate,
      String? nearAvailableDate,
      List<ServiceSlotsModels> servicesWithStaff});
}

/// @nodoc
class __$$_ServicesTimesModelsCopyWithImpl<$Res>
    extends _$ServicesTimesModelsCopyWithImpl<$Res>
    implements _$$_ServicesTimesModelsCopyWith<$Res> {
  __$$_ServicesTimesModelsCopyWithImpl(_$_ServicesTimesModels _value,
      $Res Function(_$_ServicesTimesModels) _then)
      : super(_value, (v) => _then(v as _$_ServicesTimesModels));

  @override
  _$_ServicesTimesModels get _value => super._value as _$_ServicesTimesModels;

  @override
  $Res call({
    Object? businessID = freezed,
    Object? reservationDate = freezed,
    Object? nearAvailableDate = freezed,
    Object? servicesWithStaff = freezed,
  }) {
    return _then(_$_ServicesTimesModels(
      businessID: businessID == freezed
          ? _value.businessID
          : businessID // ignore: cast_nullable_to_non_nullable
              as String,
      reservationDate: reservationDate == freezed
          ? _value.reservationDate
          : reservationDate // ignore: cast_nullable_to_non_nullable
              as String,
      nearAvailableDate: nearAvailableDate == freezed
          ? _value.nearAvailableDate
          : nearAvailableDate // ignore: cast_nullable_to_non_nullable
              as String?,
      servicesWithStaff: servicesWithStaff == freezed
          ? _value._servicesWithStaff
          : servicesWithStaff // ignore: cast_nullable_to_non_nullable
              as List<ServiceSlotsModels>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServicesTimesModels extends _ServicesTimesModels {
  const _$_ServicesTimesModels(
      {required this.businessID,
      required this.reservationDate,
      required this.nearAvailableDate,
      required final List<ServiceSlotsModels> servicesWithStaff})
      : _servicesWithStaff = servicesWithStaff,
        super._();

  factory _$_ServicesTimesModels.fromJson(Map<String, dynamic> json) =>
      _$$_ServicesTimesModelsFromJson(json);

  @override
  final String businessID;
  @override
  final String reservationDate;
  @override
  final String? nearAvailableDate;
  final List<ServiceSlotsModels> _servicesWithStaff;
  @override
  List<ServiceSlotsModels> get servicesWithStaff {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_servicesWithStaff);
  }

  @override
  String toString() {
    return 'ServicesTimesModels(businessID: $businessID, reservationDate: $reservationDate, nearAvailableDate: $nearAvailableDate, servicesWithStaff: $servicesWithStaff)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServicesTimesModels &&
            const DeepCollectionEquality()
                .equals(other.businessID, businessID) &&
            const DeepCollectionEquality()
                .equals(other.reservationDate, reservationDate) &&
            const DeepCollectionEquality()
                .equals(other.nearAvailableDate, nearAvailableDate) &&
            const DeepCollectionEquality()
                .equals(other._servicesWithStaff, _servicesWithStaff));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(businessID),
      const DeepCollectionEquality().hash(reservationDate),
      const DeepCollectionEquality().hash(nearAvailableDate),
      const DeepCollectionEquality().hash(_servicesWithStaff));

  @JsonKey(ignore: true)
  @override
  _$$_ServicesTimesModelsCopyWith<_$_ServicesTimesModels> get copyWith =>
      __$$_ServicesTimesModelsCopyWithImpl<_$_ServicesTimesModels>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServicesTimesModelsToJson(
      this,
    );
  }
}

abstract class _ServicesTimesModels extends ServicesTimesModels {
  const factory _ServicesTimesModels(
          {required final String businessID,
          required final String reservationDate,
          required final String? nearAvailableDate,
          required final List<ServiceSlotsModels> servicesWithStaff}) =
      _$_ServicesTimesModels;
  const _ServicesTimesModels._() : super._();

  factory _ServicesTimesModels.fromJson(Map<String, dynamic> json) =
      _$_ServicesTimesModels.fromJson;

  @override
  String get businessID;
  @override
  String get reservationDate;
  @override
  String? get nearAvailableDate;
  @override
  List<ServiceSlotsModels> get servicesWithStaff;
  @override
  @JsonKey(ignore: true)
  _$$_ServicesTimesModelsCopyWith<_$_ServicesTimesModels> get copyWith =>
      throw _privateConstructorUsedError;
}

ServiceSlotsModels _$ServiceSlotsModelsFromJson(Map<String, dynamic> json) {
  return _ServiceSlotsModels.fromJson(json);
}

/// @nodoc
mixin _$ServiceSlotsModels {
  List<String> get timeSlots => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceSlotsModelsCopyWith<ServiceSlotsModels> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceSlotsModelsCopyWith<$Res> {
  factory $ServiceSlotsModelsCopyWith(
          ServiceSlotsModels value, $Res Function(ServiceSlotsModels) then) =
      _$ServiceSlotsModelsCopyWithImpl<$Res>;
  $Res call({List<String> timeSlots});
}

/// @nodoc
class _$ServiceSlotsModelsCopyWithImpl<$Res>
    implements $ServiceSlotsModelsCopyWith<$Res> {
  _$ServiceSlotsModelsCopyWithImpl(this._value, this._then);

  final ServiceSlotsModels _value;
  // ignore: unused_field
  final $Res Function(ServiceSlotsModels) _then;

  @override
  $Res call({
    Object? timeSlots = freezed,
  }) {
    return _then(_value.copyWith(
      timeSlots: timeSlots == freezed
          ? _value.timeSlots
          : timeSlots // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$$_ServiceSlotsModelsCopyWith<$Res>
    implements $ServiceSlotsModelsCopyWith<$Res> {
  factory _$$_ServiceSlotsModelsCopyWith(_$_ServiceSlotsModels value,
          $Res Function(_$_ServiceSlotsModels) then) =
      __$$_ServiceSlotsModelsCopyWithImpl<$Res>;
  @override
  $Res call({List<String> timeSlots});
}

/// @nodoc
class __$$_ServiceSlotsModelsCopyWithImpl<$Res>
    extends _$ServiceSlotsModelsCopyWithImpl<$Res>
    implements _$$_ServiceSlotsModelsCopyWith<$Res> {
  __$$_ServiceSlotsModelsCopyWithImpl(
      _$_ServiceSlotsModels _value, $Res Function(_$_ServiceSlotsModels) _then)
      : super(_value, (v) => _then(v as _$_ServiceSlotsModels));

  @override
  _$_ServiceSlotsModels get _value => super._value as _$_ServiceSlotsModels;

  @override
  $Res call({
    Object? timeSlots = freezed,
  }) {
    return _then(_$_ServiceSlotsModels(
      timeSlots: timeSlots == freezed
          ? _value._timeSlots
          : timeSlots // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_ServiceSlotsModels extends _ServiceSlotsModels {
  const _$_ServiceSlotsModels({required final List<String> timeSlots})
      : _timeSlots = timeSlots,
        super._();

  factory _$_ServiceSlotsModels.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceSlotsModelsFromJson(json);

  final List<String> _timeSlots;
  @override
  List<String> get timeSlots {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_timeSlots);
  }

  @override
  String toString() {
    return 'ServiceSlotsModels(timeSlots: $timeSlots)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServiceSlotsModels &&
            const DeepCollectionEquality()
                .equals(other._timeSlots, _timeSlots));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_timeSlots));

  @JsonKey(ignore: true)
  @override
  _$$_ServiceSlotsModelsCopyWith<_$_ServiceSlotsModels> get copyWith =>
      __$$_ServiceSlotsModelsCopyWithImpl<_$_ServiceSlotsModels>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceSlotsModelsToJson(
      this,
    );
  }
}

abstract class _ServiceSlotsModels extends ServiceSlotsModels {
  const factory _ServiceSlotsModels({required final List<String> timeSlots}) =
      _$_ServiceSlotsModels;
  const _ServiceSlotsModels._() : super._();

  factory _ServiceSlotsModels.fromJson(Map<String, dynamic> json) =
      _$_ServiceSlotsModels.fromJson;

  @override
  List<String> get timeSlots;
  @override
  @JsonKey(ignore: true)
  _$$_ServiceSlotsModelsCopyWith<_$_ServiceSlotsModels> get copyWith =>
      throw _privateConstructorUsedError;
}
