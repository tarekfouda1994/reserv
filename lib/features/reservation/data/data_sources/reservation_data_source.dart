import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/features/reservation/data/models/apply_promo_code_model/apply_promo_code_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/appointment_success_model/appointment_success_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/promo_model/promo_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_date_model/service_date_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_types_model/service_types_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/services_times_models/services_times_models.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/available_days_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/business_service_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/get_promo_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/reservation_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/time_slot_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/together_entity.dart';



abstract class ReservationDataSource {
  Future<Either<Failure, List<ServiceTypesModel>>> getCompanyService(BusinessServicePrams params);

  Future<Either<Failure, List<ServiceDateModel>>> getServiceAvailableDays(AvailableDaysEntity params);

  Future<Either<Failure, List<String>>> getTimesSlots(TimeSlotEntity params);

  Future<Either<Failure, AppointmentSuccessModel>> makeReservation(ReservationEntity params);

  Future<Either<Failure, ApplyPromoCodeModel>> applyPromoCode(ReservationEntity params);

  Future<Either<Failure, List<PromoModel>>> getPromos(GetPromoParams params);

  Future<Either<Failure, ServicesTimesModels>> getReserveTogetherTimes(TogetherEntity params);

}
