import 'package:dartz/dartz.dart';
import 'package:flutter_tdd/core/errors/failures.dart';
import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/http/generic_http/generic_http.dart';
import 'package:flutter_tdd/core/http/models/http_request_model.dart';
import 'package:flutter_tdd/features/reservation/data/data_sources/reservation_data_source.dart';
import 'package:flutter_tdd/features/reservation/data/models/apply_promo_code_model/apply_promo_code_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/appointment_success_model/appointment_success_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/promo_model/promo_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_date_model/service_date_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_types_model/service_types_model.dart';
import 'package:flutter_tdd/features/reservation/data/models/services_times_models/services_times_models.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/available_days_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/business_service_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/get_promo_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/reservation_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/time_slot_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/together_entity.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: ReservationDataSource)
class ReservationDataSourceImpl extends ReservationDataSource {
  @override
  Future<Either<Failure, List<ServiceTypesModel>>> getCompanyService(
      BusinessServicePrams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getCompanyServices,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: params.refresh,
      requestBody: params.toJson(),
      responseKey: (data) => data["businessServiceTypes"],
      toJsonFunc: (json) => List<ServiceTypesModel>.from(
          json.map((e) => ServiceTypesModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<ServiceTypesModel>>()(model);
  }

  @override
  Future<Either<Failure, List<ServiceDateModel>>> getServiceAvailableDays(
      AvailableDaysEntity params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getServiceAvailableDays,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      requestBody: params.toJson(),
      responseKey: (data) => data["listItems"],
      toJsonFunc: (json) => List<ServiceDateModel>.from(
          json.map((e) => ServiceDateModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<ServiceDateModel>>()(model);
  }

  @override
  Future<Either<Failure, List<String>>> getTimesSlots(
      TimeSlotEntity params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getTimesSlots,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      showError: false,
      requestBody: params.toJson(),
      toJsonFunc: (json) => List<String>.from(json.map((e) => e)),
    );
    return await GenericHttpImpl<List<String>>()(model);
  }

  @override
  Future<Either<Failure, AppointmentSuccessModel>> makeReservation(
      ReservationEntity params) async {
    HttpRequestModel model = HttpRequestModel(
        url: ApiNames.makeReservation,
        requestMethod: RequestMethod.post,
        responseType: ResType.model,
        requestBody: params.toJson(),
        showLoader: true,
        toJsonFunc: (json) => AppointmentSuccessModel.fromJson(json));
    return await GenericHttpImpl<AppointmentSuccessModel>()(model);
  }

  @override
  Future<Either<Failure, ApplyPromoCodeModel>> applyPromoCode(
      ReservationEntity params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.addPromo,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      requestBody: params.toJson(),
      responseKey: (data) => data,
      showLoader: true,
      toJsonFunc: (json) {
        return ApplyPromoCodeModel.fromJson(json);
      },
    );
    return await GenericHttpImpl<ApplyPromoCodeModel>()(model);
  }

  @override
  Future<Either<Failure, List<PromoModel>>> getPromos(
      GetPromoParams params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getPromo,
      requestMethod: RequestMethod.post,
      responseType: ResType.list,
      refresh: params.refresh ?? true,
      showError: false,
      responseKey: (data) => data["itemsList"],
      requestBody: params.toJson(),
      toJsonFunc: (json) =>
          List<PromoModel>.from(json.map((e) => PromoModel.fromJson(e))),
    );
    return await GenericHttpImpl<List<PromoModel>>()(model);
  }

  @override
  Future<Either<Failure, ServicesTimesModels>> getReserveTogetherTimes(
      TogetherEntity params) async {
    HttpRequestModel model = HttpRequestModel(
      url: ApiNames.getReserveTogetherData,
      requestMethod: RequestMethod.post,
      responseType: ResType.model,
      requestBody: params.toJson(),
      toJsonFunc: (json) => ServicesTimesModels.fromJson(json),
    );
    return await GenericHttpImpl<ServicesTimesModels>()(model);
  }
}
