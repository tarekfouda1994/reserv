
import 'package:flutter_tdd/core/helpers/enums.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';

extension HomeDepartmentsExtension on HomeDepartments {

  String getTitle(String filter){
    switch(this){
      case HomeDepartments.offers:
        return filter.isEmpty?tr("services_offer"):"$filter ${tr("Offer")}";
      case HomeDepartments.recommended:
        return filter.isEmpty?tr("recommended_services"):"${tr("Recommended")} $filter";
      case HomeDepartments.recently:
        return filter.isEmpty?tr("recently_services"):"${tr("Recently")} $filter";
      case HomeDepartments.popular:
        return filter.isEmpty?tr("popular_services"):"${tr("Popular")} $filter";
      case HomeDepartments.trending:
        return filter.isEmpty?tr("trending_services"):"${tr("Trending")} $filter";
      case HomeDepartments.favorite:
        return filter.isEmpty?tr("favorite_services"):"${tr("Favorite")} $filter";
      case HomeDepartments.onSearch:
        return "$filter ${tr("services")}";
    }
  }

}