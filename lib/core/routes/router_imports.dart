import 'package:auto_route/auto_route.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/active_account/active_account_imports.dart';
import 'package:flutter_tdd/features/auth/presentation/pages/splash/splash_imports.dart';
import 'package:flutter_tdd/features/general/domain/entities/location_entity.dart';
import 'package:flutter_tdd/features/general/presentation/pages/location_address/LocationAddressImports.dart';
import 'package:flutter_tdd/features/general/presentation/pages/repeated_questions/repeated_questions_imports.dart';
import 'package:flutter_tdd/features/profile/presentation/manager/routes/routes.dart';
import 'package:flutter_tdd/features/reschedule/presentation/manager/routes/routes.dart';
import 'package:flutter_tdd/features/reservation/presentation/manager/routes/routes.dart';
import 'package:flutter_tdd/features/search/presentation/manager/routes/routes.dart';

import '../../features/auth/presentation/manager/routes/routes.dart';
import '../../features/auth/presentation/pages/select_language/select_language_imports.dart';
import '../../features/general/presentation/pages/terms/terms_imports.dart';

part 'router.dart';
