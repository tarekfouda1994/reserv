// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i39;
import 'package:flutter/material.dart' as _i40;
import 'package:flutter_tdd/core/helpers/enums.dart' as _i43;
import 'package:flutter_tdd/features/auth/presentation/pages/active_account/active_account_imports.dart'
    as _i3;
import 'package:flutter_tdd/features/auth/presentation/pages/confirmation_forget_password/confirmation_forget_password_imports.dart'
    as _i37;
import 'package:flutter_tdd/features/auth/presentation/pages/login/login_imports.dart'
    as _i36;
import 'package:flutter_tdd/features/auth/presentation/pages/register/register_imports.dart'
    as _i35;
import 'package:flutter_tdd/features/auth/presentation/pages/reset_password/reset_password_imports.dart'
    as _i33;
import 'package:flutter_tdd/features/auth/presentation/pages/select_language/select_language_imports.dart'
    as _i2;
import 'package:flutter_tdd/features/auth/presentation/pages/sent_success/sent_success_imports.dart'
    as _i34;
import 'package:flutter_tdd/features/auth/presentation/pages/splash/splash_imports.dart'
    as _i1;
import 'package:flutter_tdd/features/general/domain/entities/location_entity.dart'
    as _i41;
import 'package:flutter_tdd/features/general/presentation/pages/location_address/LocationAddressImports.dart'
    as _i6;
import 'package:flutter_tdd/features/general/presentation/pages/repeated_questions/repeated_questions_imports.dart'
    as _i5;
import 'package:flutter_tdd/features/general/presentation/pages/terms/terms_imports.dart'
    as _i4;
import 'package:flutter_tdd/features/profile/presentation/pages/adresses/address_pages/add_address/add_address_imports.dart'
    as _i30;
import 'package:flutter_tdd/features/profile/presentation/pages/adresses/adresses_imports.dart'
    as _i23;
import 'package:flutter_tdd/features/profile/presentation/pages/change_password/change_password_imports.dart'
    as _i26;
import 'package:flutter_tdd/features/profile/presentation/pages/contact_us/contact_us_imports.dart'
    as _i29;
import 'package:flutter_tdd/features/profile/presentation/pages/edit_profile/edit_profile_imports.dart'
    as _i21;
import 'package:flutter_tdd/features/profile/presentation/pages/favourite/favourite_imports.dart'
    as _i27;
import 'package:flutter_tdd/features/profile/presentation/pages/frequently_asked_questions/frequently_asked_questions_Imports.dart'
    as _i28;
import 'package:flutter_tdd/features/profile/presentation/pages/payment/payment_imports.dart'
    as _i22;
import 'package:flutter_tdd/features/profile/presentation/pages/setting/setting_imports.dart'
    as _i25;
import 'package:flutter_tdd/features/profile/presentation/pages/vouchers/vouchers_imports.dart'
    as _i24;
import 'package:flutter_tdd/features/reschedule/domain/entities/reschedule_entity.dart'
    as _i47;
import 'package:flutter_tdd/features/reschedule/presentation/pages/reschedule_appointment/reschedule_appointment_imports.dart'
    as _i38;
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart'
    as _i45;
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_date/reservation_date_imports.dart'
    as _i32;
import 'package:flutter_tdd/features/reservation/presentation/pages/reservation_root/reservation_root_imports.dart'
    as _i31;
import 'package:flutter_tdd/features/search/data/models/appointment_orders/appointment_orders.dart'
    as _i42;
import 'package:flutter_tdd/features/search/domain/entites/filter_entity.dart'
    as _i44;
import 'package:flutter_tdd/features/search/presentation/pages/all_banners_page/all_banners_page_imports.dart'
    as _i20;
import 'package:flutter_tdd/features/search/presentation/pages/business_saerch/business_search_imports.dart'
    as _i15;
import 'package:flutter_tdd/features/search/presentation/pages/chat_room/chat_room_imports.dart'
    as _i17;
import 'package:flutter_tdd/features/search/presentation/pages/dashboard_details/dashboard_details_imports.dart'
    as _i8;
import 'package:flutter_tdd/features/search/presentation/pages/filter/filter_import.dart'
    as _i18;
import 'package:flutter_tdd/features/search/presentation/pages/home/home_imports.dart'
    as _i7;
import 'package:flutter_tdd/features/search/presentation/pages/home/tabs/profile/profile_imports.dart'
    as _i46;
import 'package:flutter_tdd/features/search/presentation/pages/hot_deals_page/hot_deals_page_imports.dart'
    as _i13;
import 'package:flutter_tdd/features/search/presentation/pages/product_details/product_details_imports.dart'
    as _i14;
import 'package:flutter_tdd/features/search/presentation/pages/search_screan/search_screen_imports.dart'
    as _i11;
import 'package:flutter_tdd/features/search/presentation/pages/select_date_and_time_page/date_time_page_Imports.dart'
    as _i10;
import 'package:flutter_tdd/features/search/presentation/pages/servicees_search/services_search_imports.dart'
    as _i16;
import 'package:flutter_tdd/features/search/presentation/pages/top_parters/top_parters_imports.dart'
    as _i19;
import 'package:flutter_tdd/features/search/presentation/pages/tracking_map/tracking_map_imports.dart'
    as _i9;
import 'package:flutter_tdd/features/search/presentation/pages/trending_services_page/trending_services_imports.dart'
    as _i12;

class AppRouter extends _i39.RootStackRouter {
  AppRouter([_i40.GlobalKey<_i40.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i39.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i1.Splash(),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    SelectLanguageRoute.name: (routeData) {
      return _i39.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i2.SelectLanguage());
    },
    ActiveAccountRoute.name: (routeData) {
      return _i39.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i3.ActiveAccount());
    },
    TermsRoute.name: (routeData) {
      return _i39.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i4.Terms());
    },
    RepeatedQuestionsRoute.name: (routeData) {
      return _i39.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i5.RepeatedQuestions());
    },
    LocationAddressRoute.name: (routeData) {
      final args = routeData.argsAs<LocationAddressRouteArgs>();
      return _i39.AdaptivePage<_i41.LocationEntity?>(
          routeData: routeData,
          child: _i6.LocationAddress(location: args.location));
    },
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i7.Home(key: args.key, index: args.index),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    DashboardDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<DashboardDetailsRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i8.DashboardDetails(
              key: args.key,
              appointmentOrdersModel: args.appointmentOrdersModel,
              status: args.status),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    TrackingMapRoute.name: (routeData) {
      final args = routeData.argsAs<TrackingMapRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i9.TrackingMap(
              key: args.key,
              businessName: args.businessName,
              address: args.address,
              lat: args.lat,
              lng: args.lng),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    DateTimePageRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i10.DateTimePage(),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    SearchScreenRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i11.SearchScreen(),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    TrendingServicesRoute.name: (routeData) {
      final args = routeData.argsAs<TrendingServicesRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i12.TrendingServices(
              key: args.key,
              homeApisType: args.homeApisType,
              titleAppBar: args.titleAppBar,
              filter: args.filter),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    HotDealsPageRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i13.HotDealsPage(),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    ProductDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<ProductDetailsRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i14.ProductDetails(key: args.key, id: args.id),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    BusinessSearchPageRoute.name: (routeData) {
      final args = routeData.argsAs<BusinessSearchPageRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i15.BusinessSearchPage(key: args.key, filter: args.filter),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    ServicesSearchPageRoute.name: (routeData) {
      final args = routeData.argsAs<ServicesSearchPageRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i16.ServicesSearchPage(key: args.key, filter: args.filter),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    ChatRoomRoute.name: (routeData) {
      final args = routeData.argsAs<ChatRoomRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i17.ChatRoom(
              key: args.key,
              appointmentOrdersModel: args.appointmentOrdersModel),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    FilterRoute.name: (routeData) {
      final args = routeData.argsAs<FilterRouteArgs>(
          orElse: () => const FilterRouteArgs());
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i18.Filter(key: args.key, filterEntity: args.filterEntity),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    TopPartnersRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i19.TopPartners(),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    AllBannerPageRoute.name: (routeData) {
      final args = routeData.argsAs<AllBannerPageRouteArgs>(
          orElse: () => const AllBannerPageRouteArgs());
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i20.AllBannerPage(key: args.key),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 100,
          opaque: true,
          barrierDismissible: false);
    },
    EditProfileRoute.name: (routeData) {
      final args = routeData.argsAs<EditProfileRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i21.EditProfile(
              key: args.key, notCompletedData: args.notCompletedData),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    PaymentRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i22.Payment(),
          opaque: true,
          barrierDismissible: false);
    },
    AddressesRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i23.Addresses(),
          opaque: true,
          barrierDismissible: false);
    },
    VouchersRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i24.Vouchers(),
          opaque: true,
          barrierDismissible: false);
    },
    SettingRoute.name: (routeData) {
      final args = routeData.argsAs<SettingRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i25.Setting(
              key: args.key, formIntroScreen: args.formIntroScreen),
          opaque: true,
          barrierDismissible: false);
    },
    ChangePasswordRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i26.ChangePassword(),
          opaque: true,
          barrierDismissible: false);
    },
    FavouriteRoute.name: (routeData) {
      final args = routeData.argsAs<FavouriteRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i27.Favourite(
              key: args.key, fromHome: args.fromHome, homeData: args.homeData),
          opaque: true,
          barrierDismissible: false);
    },
    FAQRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i28.FAQ(),
          opaque: true,
          barrierDismissible: false);
    },
    ContactUsRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i29.ContactUs(),
          opaque: true,
          barrierDismissible: false);
    },
    AddAddressRoute.name: (routeData) {
      final args = routeData.argsAs<AddAddressRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i30.AddAddress(
              key: args.key, locationEntity: args.locationEntity),
          opaque: true,
          barrierDismissible: false);
    },
    ReservationRootRoute.name: (routeData) {
      final args = routeData.argsAs<ReservationRootRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i31.ReservationRoot(
              key: args.key,
              businessID: args.businessID,
              services: args.services,
              serviceId: args.serviceId),
          transitionsBuilder: _i39.TransitionsBuilders.fadeIn,
          durationInMilliseconds: 800,
          opaque: true,
          barrierDismissible: false);
    },
    ReservationDateRoute.name: (routeData) {
      final args = routeData.argsAs<ReservationDateRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i32.ReservationDate(key: args.key, rootData: args.rootData),
          opaque: true,
          barrierDismissible: false);
    },
    ResetPasswordRoute.name: (routeData) {
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i33.ResetPassword(),
          opaque: true,
          barrierDismissible: false);
    },
    SentSuccessRoute.name: (routeData) {
      final args = routeData.argsAs<SentSuccessRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i34.SentSuccess(key: args.key, profileData: args.profileData),
          opaque: true,
          barrierDismissible: false);
    },
    RegisterRoute.name: (routeData) {
      final args = routeData.argsAs<RegisterRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i35.Register(key: args.key, profileData: args.profileData),
          opaque: true,
          barrierDismissible: false);
    },
    LoginRoute.name: (routeData) {
      final args = routeData.argsAs<LoginRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i36.Login(key: args.key, profileData: args.profileData),
          opaque: true,
          barrierDismissible: false);
    },
    ConfirmationForgetPasswordRoute.name: (routeData) {
      final args = routeData.argsAs<ConfirmationForgetPasswordRouteArgs>();
      return _i39.CustomPage<dynamic>(
          routeData: routeData,
          child: _i37.ConfirmationForgetPassword(
              key: args.key, profileData: args.profileData),
          opaque: true,
          barrierDismissible: false);
    },
    RescheduleAppointmentRoute.name: (routeData) {
      final args = routeData.argsAs<RescheduleAppointmentRouteArgs>();
      return _i39.CustomPage<bool?>(
          routeData: routeData,
          child: _i38.RescheduleAppointment(
              key: args.key,
              businessID: args.businessID,
              services: args.services,
              orderNumber: args.orderNumber,
              lastName: args.lastName),
          opaque: true,
          barrierDismissible: false);
    }
  };

  @override
  List<_i39.RouteConfig> get routes => [
        _i39.RouteConfig(SplashRoute.name, path: '/'),
        _i39.RouteConfig(SelectLanguageRoute.name, path: '/select-language'),
        _i39.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i39.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i39.RouteConfig(RepeatedQuestionsRoute.name,
            path: '/repeated-questions'),
        _i39.RouteConfig(LocationAddressRoute.name, path: '/location-address'),
        _i39.RouteConfig(HomeRoute.name, path: '/Home'),
        _i39.RouteConfig(DashboardDetailsRoute.name,
            path: '/dashboard-details'),
        _i39.RouteConfig(TrackingMapRoute.name, path: '/tracking-map'),
        _i39.RouteConfig(DateTimePageRoute.name, path: '/date-time-page'),
        _i39.RouteConfig(SearchScreenRoute.name, path: '/search-screen'),
        _i39.RouteConfig(TrendingServicesRoute.name,
            path: '/trending-services'),
        _i39.RouteConfig(HotDealsPageRoute.name, path: '/hot-deals-page'),
        _i39.RouteConfig(ProductDetailsRoute.name, path: '/product-details'),
        _i39.RouteConfig(BusinessSearchPageRoute.name,
            path: '/business-search-page'),
        _i39.RouteConfig(ServicesSearchPageRoute.name,
            path: '/services-search-page'),
        _i39.RouteConfig(ChatRoomRoute.name, path: '/chat-room'),
        _i39.RouteConfig(FilterRoute.name, path: '/Filter'),
        _i39.RouteConfig(TopPartnersRoute.name, path: '/top-partners'),
        _i39.RouteConfig(DashboardDetailsRoute.name,
            path: '/dashboard-details'),
        _i39.RouteConfig(AllBannerPageRoute.name, path: '/all-banner-page'),
        _i39.RouteConfig(EditProfileRoute.name, path: '/edit-profile'),
        _i39.RouteConfig(PaymentRoute.name, path: '/Payment'),
        _i39.RouteConfig(AddressesRoute.name, path: '/Addresses'),
        _i39.RouteConfig(VouchersRoute.name, path: '/Vouchers'),
        _i39.RouteConfig(SettingRoute.name, path: '/Setting'),
        _i39.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i39.RouteConfig(FavouriteRoute.name, path: '/Favourite'),
        _i39.RouteConfig(FAQRoute.name, path: '/f-aQ'),
        _i39.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i39.RouteConfig(AddAddressRoute.name, path: '/add-address'),
        _i39.RouteConfig(ReservationRootRoute.name, path: '/reservation-root'),
        _i39.RouteConfig(ReservationDateRoute.name, path: '/reservation-date'),
        _i39.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i39.RouteConfig(SentSuccessRoute.name, path: '/sent-success'),
        _i39.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i39.RouteConfig(LoginRoute.name, path: '/Login'),
        _i39.RouteConfig(RegisterRoute.name, path: '/Register'),
        _i39.RouteConfig(ConfirmationForgetPasswordRoute.name,
            path: '/confirmation-forget-password'),
        _i39.RouteConfig(RescheduleAppointmentRoute.name,
            path: '/reschedule-appointment')
      ];
}

/// generated route for
/// [_i1.Splash]
class SplashRoute extends _i39.PageRouteInfo<void> {
  const SplashRoute() : super(SplashRoute.name, path: '/');

  static const String name = 'SplashRoute';
}

/// generated route for
/// [_i2.SelectLanguage]
class SelectLanguageRoute extends _i39.PageRouteInfo<void> {
  const SelectLanguageRoute()
      : super(SelectLanguageRoute.name, path: '/select-language');

  static const String name = 'SelectLanguageRoute';
}

/// generated route for
/// [_i3.ActiveAccount]
class ActiveAccountRoute extends _i39.PageRouteInfo<void> {
  const ActiveAccountRoute()
      : super(ActiveAccountRoute.name, path: '/active-account');

  static const String name = 'ActiveAccountRoute';
}

/// generated route for
/// [_i4.Terms]
class TermsRoute extends _i39.PageRouteInfo<void> {
  const TermsRoute() : super(TermsRoute.name, path: '/Terms');

  static const String name = 'TermsRoute';
}

/// generated route for
/// [_i5.RepeatedQuestions]
class RepeatedQuestionsRoute extends _i39.PageRouteInfo<void> {
  const RepeatedQuestionsRoute()
      : super(RepeatedQuestionsRoute.name, path: '/repeated-questions');

  static const String name = 'RepeatedQuestionsRoute';
}

/// generated route for
/// [_i6.LocationAddress]
class LocationAddressRoute
    extends _i39.PageRouteInfo<LocationAddressRouteArgs> {
  LocationAddressRoute({required _i41.LocationEntity location})
      : super(LocationAddressRoute.name,
            path: '/location-address',
            args: LocationAddressRouteArgs(location: location));

  static const String name = 'LocationAddressRoute';
}

class LocationAddressRouteArgs {
  const LocationAddressRouteArgs({required this.location});

  final _i41.LocationEntity location;

  @override
  String toString() {
    return 'LocationAddressRouteArgs{location: $location}';
  }
}

/// generated route for
/// [_i7.Home]
class HomeRoute extends _i39.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({_i40.Key? key, int index = 0})
      : super(HomeRoute.name,
            path: '/Home', args: HomeRouteArgs(key: key, index: index));

  static const String name = 'HomeRoute';
}

class HomeRouteArgs {
  const HomeRouteArgs({this.key, this.index = 0});

  final _i40.Key? key;

  final int index;

  @override
  String toString() {
    return 'HomeRouteArgs{key: $key, index: $index}';
  }
}

/// generated route for
/// [_i8.DashboardDetails]
class DashboardDetailsRoute
    extends _i39.PageRouteInfo<DashboardDetailsRouteArgs> {
  DashboardDetailsRoute(
      {_i40.Key? key,
      required _i42.AppointmentOrdersModel appointmentOrdersModel,
      required int status})
      : super(DashboardDetailsRoute.name,
            path: '/dashboard-details',
            args: DashboardDetailsRouteArgs(
                key: key,
                appointmentOrdersModel: appointmentOrdersModel,
                status: status));

  static const String name = 'DashboardDetailsRoute';
}

class DashboardDetailsRouteArgs {
  const DashboardDetailsRouteArgs(
      {this.key, required this.appointmentOrdersModel, required this.status});

  final _i40.Key? key;

  final _i42.AppointmentOrdersModel appointmentOrdersModel;

  final int status;

  @override
  String toString() {
    return 'DashboardDetailsRouteArgs{key: $key, appointmentOrdersModel: $appointmentOrdersModel, status: $status}';
  }
}

/// generated route for
/// [_i9.TrackingMap]
class TrackingMapRoute extends _i39.PageRouteInfo<TrackingMapRouteArgs> {
  TrackingMapRoute(
      {_i40.Key? key,
      required String businessName,
      required String address,
      required double lat,
      required double lng})
      : super(TrackingMapRoute.name,
            path: '/tracking-map',
            args: TrackingMapRouteArgs(
                key: key,
                businessName: businessName,
                address: address,
                lat: lat,
                lng: lng));

  static const String name = 'TrackingMapRoute';
}

class TrackingMapRouteArgs {
  const TrackingMapRouteArgs(
      {this.key,
      required this.businessName,
      required this.address,
      required this.lat,
      required this.lng});

  final _i40.Key? key;

  final String businessName;

  final String address;

  final double lat;

  final double lng;

  @override
  String toString() {
    return 'TrackingMapRouteArgs{key: $key, businessName: $businessName, address: $address, lat: $lat, lng: $lng}';
  }
}

/// generated route for
/// [_i10.DateTimePage]
class DateTimePageRoute extends _i39.PageRouteInfo<void> {
  const DateTimePageRoute()
      : super(DateTimePageRoute.name, path: '/date-time-page');

  static const String name = 'DateTimePageRoute';
}

/// generated route for
/// [_i11.SearchScreen]
class SearchScreenRoute extends _i39.PageRouteInfo<void> {
  const SearchScreenRoute()
      : super(SearchScreenRoute.name, path: '/search-screen');

  static const String name = 'SearchScreenRoute';
}

/// generated route for
/// [_i12.TrendingServices]
class TrendingServicesRoute
    extends _i39.PageRouteInfo<TrendingServicesRouteArgs> {
  TrendingServicesRoute(
      {_i40.Key? key,
      required _i43.HomeApisType homeApisType,
      required String titleAppBar,
      String? filter})
      : super(TrendingServicesRoute.name,
            path: '/trending-services',
            args: TrendingServicesRouteArgs(
                key: key,
                homeApisType: homeApisType,
                titleAppBar: titleAppBar,
                filter: filter));

  static const String name = 'TrendingServicesRoute';
}

class TrendingServicesRouteArgs {
  const TrendingServicesRouteArgs(
      {this.key,
      required this.homeApisType,
      required this.titleAppBar,
      this.filter});

  final _i40.Key? key;

  final _i43.HomeApisType homeApisType;

  final String titleAppBar;

  final String? filter;

  @override
  String toString() {
    return 'TrendingServicesRouteArgs{key: $key, homeApisType: $homeApisType, titleAppBar: $titleAppBar, filter: $filter}';
  }
}

/// generated route for
/// [_i13.HotDealsPage]
class HotDealsPageRoute extends _i39.PageRouteInfo<void> {
  const HotDealsPageRoute()
      : super(HotDealsPageRoute.name, path: '/hot-deals-page');

  static const String name = 'HotDealsPageRoute';
}

/// generated route for
/// [_i14.ProductDetails]
class ProductDetailsRoute extends _i39.PageRouteInfo<ProductDetailsRouteArgs> {
  ProductDetailsRoute({_i40.Key? key, required String id})
      : super(ProductDetailsRoute.name,
            path: '/product-details',
            args: ProductDetailsRouteArgs(key: key, id: id));

  static const String name = 'ProductDetailsRoute';
}

class ProductDetailsRouteArgs {
  const ProductDetailsRouteArgs({this.key, required this.id});

  final _i40.Key? key;

  final String id;

  @override
  String toString() {
    return 'ProductDetailsRouteArgs{key: $key, id: $id}';
  }
}

/// generated route for
/// [_i15.BusinessSearchPage]
class BusinessSearchPageRoute
    extends _i39.PageRouteInfo<BusinessSearchPageRouteArgs> {
  BusinessSearchPageRoute({_i40.Key? key, required String filter})
      : super(BusinessSearchPageRoute.name,
            path: '/business-search-page',
            args: BusinessSearchPageRouteArgs(key: key, filter: filter));

  static const String name = 'BusinessSearchPageRoute';
}

class BusinessSearchPageRouteArgs {
  const BusinessSearchPageRouteArgs({this.key, required this.filter});

  final _i40.Key? key;

  final String filter;

  @override
  String toString() {
    return 'BusinessSearchPageRouteArgs{key: $key, filter: $filter}';
  }
}

/// generated route for
/// [_i16.ServicesSearchPage]
class ServicesSearchPageRoute
    extends _i39.PageRouteInfo<ServicesSearchPageRouteArgs> {
  ServicesSearchPageRoute({_i40.Key? key, required String filter})
      : super(ServicesSearchPageRoute.name,
            path: '/services-search-page',
            args: ServicesSearchPageRouteArgs(key: key, filter: filter));

  static const String name = 'ServicesSearchPageRoute';
}

class ServicesSearchPageRouteArgs {
  const ServicesSearchPageRouteArgs({this.key, required this.filter});

  final _i40.Key? key;

  final String filter;

  @override
  String toString() {
    return 'ServicesSearchPageRouteArgs{key: $key, filter: $filter}';
  }
}

/// generated route for
/// [_i17.ChatRoom]
class ChatRoomRoute extends _i39.PageRouteInfo<ChatRoomRouteArgs> {
  ChatRoomRoute(
      {_i40.Key? key,
      required _i42.AppointmentOrdersModel appointmentOrdersModel})
      : super(ChatRoomRoute.name,
            path: '/chat-room',
            args: ChatRoomRouteArgs(
                key: key, appointmentOrdersModel: appointmentOrdersModel));

  static const String name = 'ChatRoomRoute';
}

class ChatRoomRouteArgs {
  const ChatRoomRouteArgs({this.key, required this.appointmentOrdersModel});

  final _i40.Key? key;

  final _i42.AppointmentOrdersModel appointmentOrdersModel;

  @override
  String toString() {
    return 'ChatRoomRouteArgs{key: $key, appointmentOrdersModel: $appointmentOrdersModel}';
  }
}

/// generated route for
/// [_i18.Filter]
class FilterRoute extends _i39.PageRouteInfo<FilterRouteArgs> {
  FilterRoute({_i40.Key? key, _i44.FilterEntity? filterEntity})
      : super(FilterRoute.name,
            path: '/Filter',
            args: FilterRouteArgs(key: key, filterEntity: filterEntity));

  static const String name = 'FilterRoute';
}

class FilterRouteArgs {
  const FilterRouteArgs({this.key, this.filterEntity});

  final _i40.Key? key;

  final _i44.FilterEntity? filterEntity;

  @override
  String toString() {
    return 'FilterRouteArgs{key: $key, filterEntity: $filterEntity}';
  }
}

/// generated route for
/// [_i19.TopPartners]
class TopPartnersRoute extends _i39.PageRouteInfo<void> {
  const TopPartnersRoute()
      : super(TopPartnersRoute.name, path: '/top-partners');

  static const String name = 'TopPartnersRoute';
}

/// generated route for
/// [_i20.AllBannerPage]
class AllBannerPageRoute extends _i39.PageRouteInfo<AllBannerPageRouteArgs> {
  AllBannerPageRoute({dynamic key})
      : super(AllBannerPageRoute.name,
            path: '/all-banner-page', args: AllBannerPageRouteArgs(key: key));

  static const String name = 'AllBannerPageRoute';
}

class AllBannerPageRouteArgs {
  const AllBannerPageRouteArgs({this.key});

  final dynamic key;

  @override
  String toString() {
    return 'AllBannerPageRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i21.EditProfile]
class EditProfileRoute extends _i39.PageRouteInfo<EditProfileRouteArgs> {
  EditProfileRoute({_i40.Key? key, required bool notCompletedData})
      : super(EditProfileRoute.name,
            path: '/edit-profile',
            args: EditProfileRouteArgs(
                key: key, notCompletedData: notCompletedData));

  static const String name = 'EditProfileRoute';
}

class EditProfileRouteArgs {
  const EditProfileRouteArgs({this.key, required this.notCompletedData});

  final _i40.Key? key;

  final bool notCompletedData;

  @override
  String toString() {
    return 'EditProfileRouteArgs{key: $key, notCompletedData: $notCompletedData}';
  }
}

/// generated route for
/// [_i22.Payment]
class PaymentRoute extends _i39.PageRouteInfo<void> {
  const PaymentRoute() : super(PaymentRoute.name, path: '/Payment');

  static const String name = 'PaymentRoute';
}

/// generated route for
/// [_i23.Addresses]
class AddressesRoute extends _i39.PageRouteInfo<void> {
  const AddressesRoute() : super(AddressesRoute.name, path: '/Addresses');

  static const String name = 'AddressesRoute';
}

/// generated route for
/// [_i24.Vouchers]
class VouchersRoute extends _i39.PageRouteInfo<void> {
  const VouchersRoute() : super(VouchersRoute.name, path: '/Vouchers');

  static const String name = 'VouchersRoute';
}

/// generated route for
/// [_i25.Setting]
class SettingRoute extends _i39.PageRouteInfo<SettingRouteArgs> {
  SettingRoute({_i40.Key? key, required bool formIntroScreen})
      : super(SettingRoute.name,
            path: '/Setting',
            args: SettingRouteArgs(key: key, formIntroScreen: formIntroScreen));

  static const String name = 'SettingRoute';
}

class SettingRouteArgs {
  const SettingRouteArgs({this.key, required this.formIntroScreen});

  final _i40.Key? key;

  final bool formIntroScreen;

  @override
  String toString() {
    return 'SettingRouteArgs{key: $key, formIntroScreen: $formIntroScreen}';
  }
}

/// generated route for
/// [_i26.ChangePassword]
class ChangePasswordRoute extends _i39.PageRouteInfo<void> {
  const ChangePasswordRoute()
      : super(ChangePasswordRoute.name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

/// generated route for
/// [_i27.Favourite]
class FavouriteRoute extends _i39.PageRouteInfo<FavouriteRouteArgs> {
  FavouriteRoute(
      {_i40.Key? key, required bool fromHome, _i7.HomeData? homeData})
      : super(FavouriteRoute.name,
            path: '/Favourite',
            args: FavouriteRouteArgs(
                key: key, fromHome: fromHome, homeData: homeData));

  static const String name = 'FavouriteRoute';
}

class FavouriteRouteArgs {
  const FavouriteRouteArgs({this.key, required this.fromHome, this.homeData});

  final _i40.Key? key;

  final bool fromHome;

  final _i7.HomeData? homeData;

  @override
  String toString() {
    return 'FavouriteRouteArgs{key: $key, fromHome: $fromHome, homeData: $homeData}';
  }
}

/// generated route for
/// [_i28.FAQ]
class FAQRoute extends _i39.PageRouteInfo<void> {
  const FAQRoute() : super(FAQRoute.name, path: '/f-aQ');

  static const String name = 'FAQRoute';
}

/// generated route for
/// [_i29.ContactUs]
class ContactUsRoute extends _i39.PageRouteInfo<void> {
  const ContactUsRoute() : super(ContactUsRoute.name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

/// generated route for
/// [_i30.AddAddress]
class AddAddressRoute extends _i39.PageRouteInfo<AddAddressRouteArgs> {
  AddAddressRoute({_i40.Key? key, required _i41.LocationEntity locationEntity})
      : super(AddAddressRoute.name,
            path: '/add-address',
            args:
                AddAddressRouteArgs(key: key, locationEntity: locationEntity));

  static const String name = 'AddAddressRoute';
}

class AddAddressRouteArgs {
  const AddAddressRouteArgs({this.key, required this.locationEntity});

  final _i40.Key? key;

  final _i41.LocationEntity locationEntity;

  @override
  String toString() {
    return 'AddAddressRouteArgs{key: $key, locationEntity: $locationEntity}';
  }
}

/// generated route for
/// [_i31.ReservationRoot]
class ReservationRootRoute
    extends _i39.PageRouteInfo<ReservationRootRouteArgs> {
  ReservationRootRoute(
      {_i40.Key? key,
      required String businessID,
      List<_i45.ServiceListModel>? services,
      String? serviceId})
      : super(ReservationRootRoute.name,
            path: '/reservation-root',
            args: ReservationRootRouteArgs(
                key: key,
                businessID: businessID,
                services: services,
                serviceId: serviceId));

  static const String name = 'ReservationRootRoute';
}

class ReservationRootRouteArgs {
  const ReservationRootRouteArgs(
      {this.key, required this.businessID, this.services, this.serviceId});

  final _i40.Key? key;

  final String businessID;

  final List<_i45.ServiceListModel>? services;

  final String? serviceId;

  @override
  String toString() {
    return 'ReservationRootRouteArgs{key: $key, businessID: $businessID, services: $services, serviceId: $serviceId}';
  }
}

/// generated route for
/// [_i32.ReservationDate]
class ReservationDateRoute
    extends _i39.PageRouteInfo<ReservationDateRouteArgs> {
  ReservationDateRoute(
      {_i40.Key? key, required _i31.ReservationRootData rootData})
      : super(ReservationDateRoute.name,
            path: '/reservation-date',
            args: ReservationDateRouteArgs(key: key, rootData: rootData));

  static const String name = 'ReservationDateRoute';
}

class ReservationDateRouteArgs {
  const ReservationDateRouteArgs({this.key, required this.rootData});

  final _i40.Key? key;

  final _i31.ReservationRootData rootData;

  @override
  String toString() {
    return 'ReservationDateRouteArgs{key: $key, rootData: $rootData}';
  }
}

/// generated route for
/// [_i33.ResetPassword]
class ResetPasswordRoute extends _i39.PageRouteInfo<void> {
  const ResetPasswordRoute()
      : super(ResetPasswordRoute.name, path: '/reset-password');

  static const String name = 'ResetPasswordRoute';
}

/// generated route for
/// [_i34.SentSuccess]
class SentSuccessRoute extends _i39.PageRouteInfo<SentSuccessRouteArgs> {
  SentSuccessRoute({_i40.Key? key, required _i46.ProfileData profileData})
      : super(SentSuccessRoute.name,
            path: '/sent-success',
            args: SentSuccessRouteArgs(key: key, profileData: profileData));

  static const String name = 'SentSuccessRoute';
}

class SentSuccessRouteArgs {
  const SentSuccessRouteArgs({this.key, required this.profileData});

  final _i40.Key? key;

  final _i46.ProfileData profileData;

  @override
  String toString() {
    return 'SentSuccessRouteArgs{key: $key, profileData: $profileData}';
  }
}

/// generated route for
/// [_i35.Register]
class RegisterRoute extends _i39.PageRouteInfo<RegisterRouteArgs> {
  RegisterRoute({_i40.Key? key, required _i46.ProfileData profileData})
      : super(RegisterRoute.name,
            path: '/Register',
            args: RegisterRouteArgs(key: key, profileData: profileData));

  static const String name = 'RegisterRoute';
}

class RegisterRouteArgs {
  const RegisterRouteArgs({this.key, required this.profileData});

  final _i40.Key? key;

  final _i46.ProfileData profileData;

  @override
  String toString() {
    return 'RegisterRouteArgs{key: $key, profileData: $profileData}';
  }
}

/// generated route for
/// [_i36.Login]
class LoginRoute extends _i39.PageRouteInfo<LoginRouteArgs> {
  LoginRoute({_i40.Key? key, required _i46.ProfileData profileData})
      : super(LoginRoute.name,
            path: '/Login',
            args: LoginRouteArgs(key: key, profileData: profileData));

  static const String name = 'LoginRoute';
}

class LoginRouteArgs {
  const LoginRouteArgs({this.key, required this.profileData});

  final _i40.Key? key;

  final _i46.ProfileData profileData;

  @override
  String toString() {
    return 'LoginRouteArgs{key: $key, profileData: $profileData}';
  }
}

/// generated route for
/// [_i37.ConfirmationForgetPassword]
class ConfirmationForgetPasswordRoute
    extends _i39.PageRouteInfo<ConfirmationForgetPasswordRouteArgs> {
  ConfirmationForgetPasswordRoute(
      {_i40.Key? key, required _i46.ProfileData profileData})
      : super(ConfirmationForgetPasswordRoute.name,
            path: '/confirmation-forget-password',
            args: ConfirmationForgetPasswordRouteArgs(
                key: key, profileData: profileData));

  static const String name = 'ConfirmationForgetPasswordRoute';
}

class ConfirmationForgetPasswordRouteArgs {
  const ConfirmationForgetPasswordRouteArgs(
      {this.key, required this.profileData});

  final _i40.Key? key;

  final _i46.ProfileData profileData;

  @override
  String toString() {
    return 'ConfirmationForgetPasswordRouteArgs{key: $key, profileData: $profileData}';
  }
}

/// generated route for
/// [_i38.RescheduleAppointment]
class RescheduleAppointmentRoute
    extends _i39.PageRouteInfo<RescheduleAppointmentRouteArgs> {
  RescheduleAppointmentRoute(
      {_i40.Key? key,
      required String businessID,
      required List<_i47.RescheduleParams> services,
      required String orderNumber,
      required String lastName})
      : super(RescheduleAppointmentRoute.name,
            path: '/reschedule-appointment',
            args: RescheduleAppointmentRouteArgs(
                key: key,
                businessID: businessID,
                services: services,
                orderNumber: orderNumber,
                lastName: lastName));

  static const String name = 'RescheduleAppointmentRoute';
}

class RescheduleAppointmentRouteArgs {
  const RescheduleAppointmentRouteArgs(
      {this.key,
      required this.businessID,
      required this.services,
      required this.orderNumber,
      required this.lastName});

  final _i40.Key? key;

  final String businessID;

  final List<_i47.RescheduleParams> services;

  final String orderNumber;

  final String lastName;

  @override
  String toString() {
    return 'RescheduleAppointmentRouteArgs{key: $key, businessID: $businessID, services: $services, orderNumber: $orderNumber, lastName: $lastName}';
  }
}
