part of 'router_imports.dart';

@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //auth routes
    CustomRoute(
      page: Splash,
      initial: true,
      durationInMilliseconds: 800,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    AdaptiveRoute(page: SelectLanguage),
    AdaptiveRoute(page: ActiveAccount),
    AdaptiveRoute(page: Terms),
    AdaptiveRoute(page: RepeatedQuestions),
    AdaptiveRoute<LocationEntity?>(page: LocationAddress),
    ...baseRoute,
    ...profileRoutes,
    ...reservationRoutes,
    ...authRoutes,
    ...rescheduleRoutes
  ],
)
class $AppRouter {}
