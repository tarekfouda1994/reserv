part of 'search_cubit.dart';

@immutable
abstract class SearchState {
  final List<String> filters;
  final String filter;
  final bool fromCat;
  final bool noSearchResult;
  final SelectedDateTimeEntity dateTime;

  SearchState(this.filter, this.dateTime, this.filters,this.fromCat,this.noSearchResult);
}

class SearchInitialState extends SearchState {
  SearchInitialState()
      : super("", SelectedDateTimeEntity(dateList: [], timeList: []), [],false,false);
}

class SearchUpdateState extends SearchState {
  SearchUpdateState(
      List<String> filters, SelectedDateTimeEntity dateTime, String filter,bool fromCat,bool noSearchResult)
      : super(filter, dateTime, filters,fromCat,noSearchResult);
}
