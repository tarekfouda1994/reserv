import 'package:bloc/bloc.dart';
import 'package:flutter_tdd/features/search/domain/entites/selected_date_time_entity.dart';
import 'package:meta/meta.dart';

part 'search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  SearchCubit() : super(SearchInitialState());

  onUpdate({required String filter, SelectedDateTimeEntity? dateTime}) {
    emit(SearchUpdateState(state.filters, dateTime ?? SelectedDateTimeEntity(timeList: [], dateList: []), filter,state.fromCat,state.noSearchResult));
  }

  onUpdateFiltersList({required List<String> filters}) {
    emit(SearchUpdateState(filters, state.dateTime,state.filter,state.fromCat,state.noSearchResult));
  }

  onUpdateFilterType({required bool fromCat}) {
    emit(SearchUpdateState(state.filters, state.dateTime,state.filter,fromCat,state.noSearchResult));
  }


  updateResultStatus({required bool noSearchResult}) {
    emit(SearchUpdateState(state.filters, state.dateTime,state.filter,state.fromCat,noSearchResult));
  }

  updateToInitial() {
    emit(SearchInitialState());
  }
}
