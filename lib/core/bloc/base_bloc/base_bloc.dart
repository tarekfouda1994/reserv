import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../errors/base_error.dart';
import 'base_state.dart';

/// if T is list we recommend use with SuccessListState with BaseBloc
///
class BaseBloc<T> extends Cubit<BaseState<T>> {
  BaseBloc([T? data]) : super(data != null ? BaseState.success(data) : const BaseState.init());

  T? _data;

  T? get data => _data;

  bool get hasData => data != null;

  void loadingState() {
    emit(const BaseState.loading());
  }

  void successState([T? data]) {
    _data = data;
    emit(BaseState.success(_data));
  }

  void failedState(BaseError error, VoidCallback callback) {
    emit(BaseState.failure(error, callback));
  }

  void reset() {
    _data = null;
    emit(const BaseState.init());
  }
}
