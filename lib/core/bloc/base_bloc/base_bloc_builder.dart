import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/errors/base_error.dart';
import 'package:flutter_tdd/core/widgets/loading_widget.dart';
import 'package:flutter_tdd/core/widgets/show_error_widget.dart';

import 'base_bloc.dart';
import 'base_state.dart';

typedef WidgetFailureBuilder = Widget Function(
    BuildContext context, BaseError error, VoidCallback callback);

class BaseBlocBuilder<T> extends StatelessWidget {
  final BaseBloc<T> bloc;
  final Widget Function(T data) onSuccessWidget;
  final WidgetFailureBuilder? onFailedWidget;
  final WidgetBuilder? onLoadingWidget;

  const BaseBlocBuilder({
    Key? key,
    required this.bloc,
    required this.onSuccessWidget,
    this.onFailedWidget,
    this.onLoadingWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BaseBloc<T>, BaseState<T>>(
      bloc: bloc,
      builder: (_, state) {
        return state.maybeWhen(
          orElse: () => SizedBox.shrink(),
          success: (data) {
            return onSuccessWidget(data as T);
          },
          loading: () => onLoadingWidget?.call(context) ?? const LoadingWidget(),
          failure: (error, callback) =>
              onFailedWidget?.call(context, error, callback) ??
              ShowErrorWidget(error: error, callback: callback),
        );
      },
    );
  }
}
