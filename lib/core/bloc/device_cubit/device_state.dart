part of 'device_cubit.dart';

@immutable
abstract class DeviceState {
  final DeviceModel model;

  DeviceState(this.model);
}

class DeviceInitial extends DeviceState {
  DeviceInitial()
      : super(DeviceModel(
          auth: false,
          isTablet: false,
          locale: Locale('en', 'US'),
        ));
}

class DeviceUpdateState extends DeviceState {
  DeviceUpdateState(DeviceModel model) : super(model);
}
