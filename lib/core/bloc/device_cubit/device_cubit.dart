import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';

part 'device_state.dart';

class DeviceCubit extends Cubit<DeviceState> {
  DeviceCubit() : super(DeviceInitial());

  updateDeviceType(bool isTablet) {
    emit(DeviceUpdateState(state.model.copyWith(isTablet: isTablet)));
  }

  updateUserAuth(bool auth) {
    emit(DeviceUpdateState(state.model.copyWith(auth: auth)));
  }

  updateLanguage(Locale locale) {
    emit(DeviceUpdateState(state.model.copyWith(locale: locale)));
  }
}
