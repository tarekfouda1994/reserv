import 'package:injectable/injectable.dart';

import '../../../helpers/global_state.dart';

@lazySingleton
class DioHeader {
  Map<String, String> call() {
    String? token = GlobalState.instance.get("token");
    var data = {
      "Content-Type": "application/json",
      'Accept': 'application/json',
      'X-SC-APIKey': 'TempAPIKey',
    };
    if (token != null) {
      data.addAll({'Authorization': 'Bearer $token'});
    }

    return data;
  }
}
