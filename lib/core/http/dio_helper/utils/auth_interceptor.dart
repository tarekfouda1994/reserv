import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/helpers/app_error_reporter.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/helpers/global_state.dart';
import 'package:flutter_tdd/core/http/dio_helper/utils/dio_header.dart';
import 'package:flutter_tdd/core/http/generic_http/api_names.dart';
import 'package:flutter_tdd/core/models/user_model/user_model.dart';
import 'package:flutter_tdd/features/auth/presentation/manager/user_cubit/user_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/manager/location_cubit/location_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    if (err.response!.statusCode == 401) {
      var context = getIt<GlobalContext>().context();
      UserModel user = context.read<UserCubit>().state.model!;
      var dio = Dio();
      try {
        var response = await dio.post(ApiNames.REFRESH_TOKEN,
            data: {"refreshToken": user.refreshToken, "accessToken": user.token});
        if (response.statusCode == 200) {
          GlobalState.instance.set("token", response.data["token"]);
          user.token = response.data["token"];
          user.refreshToken = response.data["refreshToken"];
          GlobalState.instance.set("token", user.token);
          getIt<SharedPreferences>().setString("user", json.encode(user.toJson()));
          final RequestOptions request = err.response!.requestOptions;
          request.headers = getIt<DioHeader>().call();
          final Options options = Options(
            headers: request.headers,
            method: request.method,
          );
          final Response result = await dio.request(request.path,
              data: request.data,
              queryParameters: request.queryParameters,
              cancelToken: request.cancelToken,
              options: options);
          return handler.resolve(result);
        }else if(response.statusCode == 401){
          _onError(context);
        }
      } catch (e, s) {
        AppErrorReporter.recordError(e, reason: e.toString(), stackTrace: s, fatal: true);
        _onError(context);
      }
    }
    return super.onError(err, handler);
  }

  void _onError(BuildContext context) {
    getIt<SharedPreferences>().clear().then((value) {
      context.read<HomeLocationCubit>().onLocationUpdated(null);
      GlobalState.instance.set("token", "");
      context.read<DeviceCubit>().updateLanguage(Locale('en', 'US'));
      Future.delayed(Duration(milliseconds: 400), () {
        Phoenix.rebirth(context);
      });
    });
  }
}
