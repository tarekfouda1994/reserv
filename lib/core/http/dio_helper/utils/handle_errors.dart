import 'dart:convert';
import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../errors/failures.dart';
import '../../../helpers/custom_toast.dart';

@lazySingleton
class HandleErrors {
  void catchError({Response? response, required Function(dynamic) errorFunc}) {
    if (response == null) {
      log("failed response Check Server");
      CustomToast.showSimpleToast(msg: "Check Server",title: "Server Error");
    } else {
      log("failed response ${response.statusCode}");
      log("failed response ${response.data}");
      var data = response.data;
      try {
        if (data is String) data = json.decode(response.data);
        String message = "";
        if (response.statusCode != 422) {
          message = errorFunc(data).toString();
        }
        switch (response.statusCode) {
          case 503:
          case 404:
            CustomToast.showSnakeBar(message,tr("response_error"));
            if (message == "Not Authorized") {
              _tokenExpired();
            }
            break;
          case 500:
            if (kDebugMode) {
              CustomToast.showSnakeBar(tr("pleaseCheckYourServer") ,tr("response_error"));
            }
            // CustomToast.showSnakeBar(message.toString(),tr("response_error"));
            break;
          case 502:
            if (kDebugMode) {
              CustomToast.showSnakeBar(tr("pleaseCheckYourServer") ,tr("response_error"));
            }
            // CustomToast.showSnakeBar("check your request",tr("response_error"));
            break;
          case 422:
          case 400:
          if (data["message"] != null) {
              CustomToast.showSnakeBar(data["message"] ,tr("response_error"));
            } else{
              for (var value in data.values) {
                CustomToast.showSnakeBar(value.toString(),tr("response_error"));
                return;
              }
            }

            break;
          case 401:
          case 301:
          case 302:
            _tokenExpired();
            break;
        }
      } catch (e) {
        if (kDebugMode) {
          CustomToast.showSnakeBar(tr("pleaseCheckYourServer") ,tr("response_error"));
        }
        // CustomToast.showSnakeBar(e.toString(),tr("response_error"));
      }
    }
  }

  Either<ServerFailure, Response> statusError(Response response, Function(dynamic) errorFunc) {
    if (response.statusCode! >= 200 && response.statusCode! <= 205) {
      return Right(response);
    }
    if (response.data["message"] != null) {
      CustomToast.showSnakeBar(response.data["message"].toString(),tr("response_error"));
    }
    return Left(ServerFailure());
  }

  void _tokenExpired() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("user");
    CustomToast.showSnakeBar("You don't have permission","Response Error");
  }
}
