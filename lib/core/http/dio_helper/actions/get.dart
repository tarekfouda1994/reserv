import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter_tdd/core/helpers/app_error_reporter.dart';
import 'package:injectable/injectable.dart';

import '../../../errors/failures.dart';
import '../../../helpers/di.dart';
import '../../models/request_body_model.dart';
import '../source/dio_helper.dart';
import '../utils/dio_options.dart';
import '../utils/handle_errors.dart';

@lazySingleton
class Get extends DioHelper {
  @override
  Future<Either<ServerFailure, Response>> call(RequestBodyModel params) async {
    try {
      var response = await dio.get(params.url,
          queryParameters: params.body,
          options: getIt<DioOptions>()(forceRefresh: params.forceRefresh),
      );
      return getIt<HandleErrors>().statusError(response, params.errorFunc);
    } on DioError catch (e) {
      AppErrorReporter.recordError(e, reason: e.message, stackTrace: e.stackTrace, fatal: true);
      if (params.showError) {
        getIt<HandleErrors>().catchError(errorFunc: params.errorFunc, response: e.response);
      }
      return Left(ServerFailure());
    }catch(e, s){
      AppErrorReporter.recordError(e, reason: e.toString(), stackTrace: s, fatal: true);
      return Left(ServerFailure());
    }
  }
}
