class RequestBodyModel {
  final String url;
  final Function(dynamic) errorFunc;
  final Map<String, dynamic> body;
  final bool showLoader;
  final bool forceRefresh;
  final bool showError;
  final dynamic dynamicBody;

  RequestBodyModel({
    required this.url,
    this.body = const <String, dynamic>{},
    this.showLoader = true,
    this.forceRefresh = true,
    this.showError = true,
    this.dynamicBody,
    required this.errorFunc,
  });
}
