import 'package:flutter_tdd/core/helpers/enums.dart';

class ApiNames {
  //development
  // static const String BRMSBaseUrl =
  //     "https://reservabo.smartcube.co/BRMS.WebAPI/api/";
  // static const String ELSBaseUrl =
  //     "https://reservabo.smartcube.co/BRMS.ELS.API.QA/api/";

  //Production
  static const String BRMSBaseUrl =
      "https://backoffice.reserva.world/BRMS.API/api/";
  static const String ELSBaseUrl =
      "https://backoffice.reserva.world/BRMS.ELS.API/api/";

  // auth routes
  static const String login = BRMSBaseUrl + "v1/Account/Login";
  static const String savePublicKey = BRMSBaseUrl + "users/";
  static const String getEncrypted = BRMSBaseUrl + "Common/GetEnc";
  static const String register = BRMSBaseUrl + "v1/Account/RegisterUser";
  static const String activeAccount = BRMSBaseUrl + "otp/verify";
  static const String resendCode = BRMSBaseUrl + "ResendCode";
  static const String switchNotify = BRMSBaseUrl + "SwitchNotify";
  static const String forgetPassword =
      BRMSBaseUrl + "v1/Account/ForgetPassword";
  static const String resetPassword =
      "A2q4mG84Z9rE2B38In4HALSWTwt9eQ5Slzq56Cod";
  static const String getAllCountries =
      BRMSBaseUrl + "Lookups/Countries/GetAll";
  static const String getHotDeals =
      ELSBaseUrl + "BusinessPromoteAds/GetAllHotDeals";
  static const String getMainBanners =
      ELSBaseUrl + "BusinessPromoteAds/GetAllMainBanners";
  static const String getTrending = ELSBaseUrl + "Businesses/GetBestSelling";
  static const String getSearchTrending =
      BRMSBaseUrl + "BusinessService/FindYourFavoriteBusinessServices";
  static const String popularServices =
      BRMSBaseUrl + "Appointments/Dashboard/GetPopularServiceCategory";
  static const String popularSearch =
      ELSBaseUrl + "Appointments/Dashboard/PopularSearch/GetAll";
  static const String recentSearch =
      BRMSBaseUrl + "Appointments/Dashboard/RecentSearch/GetAll";
  static const String updateProfile =
      BRMSBaseUrl + "UserManagement/ManageUsers/UpdateProfile";
  static const String getUserData =
      BRMSBaseUrl + "UserManagement/ManageUsers/Get";
  static const String searchGetAll =
      ELSBaseUrl + "Appointments/Dashboard/GetAll";
  static const String getCompanyServices =
      BRMSBaseUrl + "Businesses/GetCompanyServicesById";
  static const String getServicesDetails =
      BRMSBaseUrl + "Businesses/GetCompanyDetailById";
  static const String getServicesReviews =
      BRMSBaseUrl + "Businesses/GetCompanyReviewById";
  static const String getServiceAvailableDays =
      BRMSBaseUrl + "Appointments/Calendar/GetStatsBWDates";
  static const String getTimesSlots =
      BRMSBaseUrl + "BusinessStaff/GetStaffTimeSlots";
  static const String makeReservation =
      BRMSBaseUrl + "Appointments/Appointment/Create";
  static const String getServiceFavourite =
      ELSBaseUrl + "Businesses/GetServiceWishListItem";
  static const String getBusinessesFavourite =
      BRMSBaseUrl + "Businesses/GetWishItemForBusiness";
  static const String updateWishItem =
      BRMSBaseUrl + "Businesses/SaveWishItemForService";
  static const String removeWishService =
      BRMSBaseUrl + "Businesses/RemoveWishItemForService";
  static const String updateWishBusiness =
      BRMSBaseUrl + "Businesses/SaveWishItemForBusiness";
  static const String removeWishBusiness =
      BRMSBaseUrl + "Businesses/RemoveWishItemForBusiness";
  static const String staffWorkingHour =
      BRMSBaseUrl + "BusinessStaff/GetAllStaffWorkingHour";
  static const String getAllPayment =
      BRMSBaseUrl + "UserManagement/ManageUsers/Payment/GetAll";
  static const String createUpdatePayment =
      BRMSBaseUrl + "UserManagement/ManageUsers/Payment/CreateUpdate";
  static const String deletePaymentCard =
      BRMSBaseUrl + "UserManagement/ManageUsers/Payment/Delete";
  static const String setPaymentDefault =
      BRMSBaseUrl + "UserManagement/ManageUsers/Payment/SetDefaultPayment";
  static const String searchServices =
      ELSBaseUrl + "BusinessService/FindYourFavoriteService";
  static const String getBusinessesServicesList =
  ELSBaseUrl + "BusinessService/GetBusinessesServicesList";
  static const String getAllOrders =
      BRMSBaseUrl + "Appointments/GetAllByOrderGrouped";
  static const String getAllOrdersStatus =
      BRMSBaseUrl + "Common/Appointment/GetStatuses";
  static const String getAppointmentDetails =
      BRMSBaseUrl + "Appointments/Visit/GetById";
  static const String getBusinessStaff = BRMSBaseUrl + "BusinessStaff/GetAll";
  static const String cancelOrder =
      BRMSBaseUrl + "Appointments/Visit/CancelOrder";
  static const String cancelOrderDetails =
      BRMSBaseUrl + "Appointments/Visit/CancelOrderDetailByID";
  static const String businessImage = BRMSBaseUrl + "Businesses/Media/Get";
  static const String addNote =
      BRMSBaseUrl + "Appointments/AddAppointmentNotes";
  static const String editNote =
      BRMSBaseUrl + "Appointments/EditAppointmentNote";
  static const String removeOneNote =
      BRMSBaseUrl + "Appointments/RemoveAppointmentNotes";
  static const String removeAllNotes =
      BRMSBaseUrl + "Appointments/RemoveAllAppointmentNotes";
  static const String getAllNotes =
      BRMSBaseUrl + "Appointments/GetAllAppointmentNotes";
  static const String externalLogin = BRMSBaseUrl + "v1/Account/ExternalLogin";
  static const String changePass =
      BRMSBaseUrl + "UserManagement/ManageUsers/ChangePassword";
  static const String getVouchers =
      BRMSBaseUrl + "BusinessVoucher/GetAllUserVoucher";
  static const String updateAddressCard =
      BRMSBaseUrl + "UserManagement/ManageUsers/Address/CreateUpdate";
  static const String getAllAddress =
      BRMSBaseUrl + "UserManagement/ManageUsers/Address/GetAll";
  static const String deleteAddress =
      BRMSBaseUrl + "UserManagement/ManageUsers/Address/Delete";
  static const String setDefaultAddress =
      BRMSBaseUrl + "UserManagement/ManageUsers/Address/SetDefaultAddress";
  static const String getFAQ =
      BRMSBaseUrl + "UserManagement/ManageUsers/FAQ/GetAll";
  static const String logMeOut = BRMSBaseUrl + "v1/Account/LogMeOut";
  static const String contactUs = BRMSBaseUrl + "v1/Account/ContactUs";
  static const String addPromo =
      BRMSBaseUrl + "Appointments/Appointment/ValidateOffers";
  static const String getPromo =
      BRMSBaseUrl + "Lookups/Services/GetAllGeneratePromoCode";
  static const String getReserveTogetherData =
      BRMSBaseUrl + "BusinessStaff/GetStaffDateTimeSlots";
  static const String getTopBusinesses =
      ELSBaseUrl + "Businesses/GetTopBusinesses";
  static const String getRecommendedServices =
      ELSBaseUrl + "BusinessService/GetRecommendedBusinessServices";
  static const String getRecentlyServices =
      BRMSBaseUrl + "BusinessService/GetRecentlyViewedBusinessServices";
  static const String getTrendingServices =
      ELSBaseUrl + "BusinessService/GetTrendingBusinessServices";
  static const String getOffersServices =
      ELSBaseUrl + "BusinessService/GetServiceOfferBusinessServices";
  static const String getPopularServices =
      ELSBaseUrl + "BusinessService/GetDashboardPopularServices";
  static const String createRate =
      BRMSBaseUrl + "Businesses/SaveBusinessRating";
  static const String createServicesRate =
      BRMSBaseUrl + "BusinessServiceRatings/Create";
  static const String updateRate =
      BRMSBaseUrl + "BusinessServiceRatings/Update";
  static const String addMsgToChat = BRMSBaseUrl + "Appointments/ClientRequest";
  static const String getAllMessages =
      BRMSBaseUrl + "Appointments/GetBookingOrderChatByOrderNumber";
  static const String getMsgCount =
      BRMSBaseUrl + "Appointments/GetAllBookingOrderChat";
  static const String getPopularCat =
      BRMSBaseUrl + "Lookups/ServiceTypes/GetAll";
  static const String getVoucherStats =
      BRMSBaseUrl + "BusinessVoucher/GetVoucherStats";
  static const String bulkReschedule =
      BRMSBaseUrl + "Appointments/Appointment/BulkReschedule";
  static const String getTermsConditions =
      BRMSBaseUrl + "v1/Account/TermsandConditions";
  static const String GEOCODE_URL = "https://geocode.xyz/";
  static const String geoCodeAuthKey = "307492059888177453631x47444";

  // Home Api
  static String apiNamesHome(HomeApisType type) {
    switch (type) {
      case HomeApisType.recommended:
        return getRecommendedServices;
      case HomeApisType.recently:
        return getRecentlyServices;
      case HomeApisType.trending:
        return getTrendingServices;
      case HomeApisType.offers:
        return getOffersServices;
      case HomeApisType.popular:
        return getPopularServices;
        case HomeApisType.onSearch:
        return getBusinessesServicesList;
    }
  }

  static const String REFRESH_TOKEN = BRMSBaseUrl + "Token/refresh";
}
