import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'device_model.freezed.dart';

@freezed
class DeviceModel with _$DeviceModel {
  factory DeviceModel({
    required Locale locale,
    required bool auth,
    required bool isTablet,
  }) = _DeviceModel;
}
