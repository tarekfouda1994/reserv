// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserModel _$$_UserModelFromJson(Map<String, dynamic> json) => _$_UserModel(
      firstName: json['englishFullName'] as String,
      lastName: json['arabicFullName'] as String,
      nationalityCountryId: json['nationalityCountryId'] as String? ?? '',
      birthDate: json['birthDate'] as String,
      workPhoneNumber: json['workPhoneNumber'] as String? ?? '',
      homePhoneNumber: json['homePhoneNumber'] as String? ?? '',
      address: json['address'] as String? ?? '',
      isActive: json['isActive'] as bool,
      insertDate: json['insertDate'] as String,
      id: json['id'] as int? ?? 0,
      email: json['userName'] as String,
      phoneNumber: json['phoneNumber'] as String? ?? '',
      phoneNumberCode: json['phoneNumberCode'] as String? ?? '',
      phoneNumberWithCode: json['phoneNumberWithCode'] as String? ?? '',
      photo: json['photo'] as String? ?? '',
      token: json['token'] as String? ?? '',
      refreshToken: json['refreshToken'] as String? ?? '',
      userId: json['userId'] as String? ?? '',
      genderArabicName: json['genderArabicName'] as String? ?? '',
      genderEnglishName: json['genderEnglishName'] as String? ?? '',
      nationalityArabicName: json['nationalityArabicName'] as String? ?? '',
      nationalityEnglishName: json['nationalityEnglishName'] as String? ?? '',
      countryArabicName: json['countryArabicName'] as String? ?? '',
      countryEnglishName: json['countryEnglishName'] as String? ?? '',
    );

Map<String, dynamic> _$$_UserModelToJson(_$_UserModel instance) =>
    <String, dynamic>{
      'englishFullName': instance.firstName,
      'arabicFullName': instance.lastName,
      'nationalityCountryId': instance.nationalityCountryId,
      'birthDate': instance.birthDate,
      'workPhoneNumber': instance.workPhoneNumber,
      'homePhoneNumber': instance.homePhoneNumber,
      'address': instance.address,
      'isActive': instance.isActive,
      'insertDate': instance.insertDate,
      'id': instance.id,
      'userName': instance.email,
      'phoneNumber': instance.phoneNumber,
      'phoneNumberCode': instance.phoneNumberCode,
      'phoneNumberWithCode': instance.phoneNumberWithCode,
      'photo': instance.photo,
      'token': instance.token,
      'refreshToken': instance.refreshToken,
      'userId': instance.userId,
      'genderArabicName': instance.genderArabicName,
      'genderEnglishName': instance.genderEnglishName,
      'nationalityArabicName': instance.nationalityArabicName,
      'nationalityEnglishName': instance.nationalityEnglishName,
      'countryArabicName': instance.countryArabicName,
      'countryEnglishName': instance.countryEnglishName,
    };
