import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@unfreezed
class UserModel with _$UserModel {
  @JsonSerializable(explicitToJson: true)
  factory UserModel({
    @JsonKey(name: "englishFullName") required String firstName,
    @JsonKey(name: "arabicFullName") required String lastName,
    @JsonKey(defaultValue: "", nullable: true) required String nationalityCountryId,
    required String birthDate,
    @JsonKey(defaultValue: "", nullable: true) required String workPhoneNumber,
    @JsonKey(defaultValue: "", nullable: true) required String homePhoneNumber,
    @JsonKey(defaultValue: "", nullable: true) required String address,
    required bool isActive,
    required String insertDate,
    @JsonKey(defaultValue: 0, nullable: true) required int id,
    @JsonKey(name: "userName") required String email,
    @JsonKey(defaultValue: "", nullable: true) required String phoneNumber,
    @JsonKey(defaultValue: "", nullable: true) required String phoneNumberCode,
    @JsonKey(defaultValue: "", nullable: true) required String phoneNumberWithCode,
    @JsonKey(defaultValue: "", nullable: true) required String photo,
    @JsonKey(defaultValue: "", nullable: true) required String token,
    @JsonKey(defaultValue: "", nullable: true) required String refreshToken,
    @JsonKey(defaultValue: "", nullable: true) required String userId,
    @JsonKey(defaultValue: "", nullable: true) required String genderArabicName,
    @JsonKey(defaultValue: "", nullable: true)
    @JsonKey(defaultValue: "", nullable: true)
        required String genderEnglishName,
    @JsonKey(defaultValue: "", nullable: true) required String nationalityArabicName,
    @JsonKey(defaultValue: "", nullable: true) required String nationalityEnglishName,
    @JsonKey(defaultValue: "", nullable: true) required String countryArabicName,
    @JsonKey(defaultValue: "", nullable: true) required String countryEnglishName,
  }) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
}
