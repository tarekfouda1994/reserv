// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return _UserModel.fromJson(json);
}

/// @nodoc
mixin _$UserModel {
  @JsonKey(name: "englishFullName")
  String get firstName => throw _privateConstructorUsedError;
  @JsonKey(name: "englishFullName")
  set firstName(String value) => throw _privateConstructorUsedError;
  @JsonKey(name: "arabicFullName")
  String get lastName => throw _privateConstructorUsedError;
  @JsonKey(name: "arabicFullName")
  set lastName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get nationalityCountryId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set nationalityCountryId(String value) => throw _privateConstructorUsedError;
  String get birthDate => throw _privateConstructorUsedError;
  set birthDate(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get workPhoneNumber => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set workPhoneNumber(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get homePhoneNumber => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set homePhoneNumber(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get address => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set address(String value) => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  set isActive(bool value) => throw _privateConstructorUsedError;
  String get insertDate => throw _privateConstructorUsedError;
  set insertDate(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  int get id => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: 0, nullable: true)
  set id(int value) => throw _privateConstructorUsedError;
  @JsonKey(name: "userName")
  String get email => throw _privateConstructorUsedError;
  @JsonKey(name: "userName")
  set email(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get phoneNumber => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set phoneNumber(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get phoneNumberCode => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set phoneNumberCode(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get phoneNumberWithCode => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set phoneNumberWithCode(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get photo => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set photo(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get token => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set token(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get refreshToken => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set refreshToken(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get userId => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set userId(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get genderArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set genderArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  @JsonKey(defaultValue: "", nullable: true)
  String get genderEnglishName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  @JsonKey(defaultValue: "", nullable: true)
  set genderEnglishName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get nationalityArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set nationalityArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get nationalityEnglishName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set nationalityEnglishName(String value) =>
      throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get countryArabicName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set countryArabicName(String value) => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  String get countryEnglishName => throw _privateConstructorUsedError;
  @JsonKey(defaultValue: "", nullable: true)
  set countryEnglishName(String value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserModelCopyWith<UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserModelCopyWith<$Res> {
  factory $UserModelCopyWith(UserModel value, $Res Function(UserModel) then) =
      _$UserModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: "englishFullName")
          String firstName,
      @JsonKey(name: "arabicFullName")
          String lastName,
      @JsonKey(defaultValue: "", nullable: true)
          String nationalityCountryId,
      String birthDate,
      @JsonKey(defaultValue: "", nullable: true)
          String workPhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          String homePhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          String address,
      bool isActive,
      String insertDate,
      @JsonKey(defaultValue: 0, nullable: true)
          int id,
      @JsonKey(name: "userName")
          String email,
      @JsonKey(defaultValue: "", nullable: true)
          String phoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          String phoneNumberCode,
      @JsonKey(defaultValue: "", nullable: true)
          String phoneNumberWithCode,
      @JsonKey(defaultValue: "", nullable: true)
          String photo,
      @JsonKey(defaultValue: "", nullable: true)
          String token,
      @JsonKey(defaultValue: "", nullable: true)
          String refreshToken,
      @JsonKey(defaultValue: "", nullable: true)
          String userId,
      @JsonKey(defaultValue: "", nullable: true)
          String genderArabicName,
      @JsonKey(defaultValue: "", nullable: true)
      @JsonKey(defaultValue: "", nullable: true)
          String genderEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String nationalityArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String countryArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String countryEnglishName});
}

/// @nodoc
class _$UserModelCopyWithImpl<$Res> implements $UserModelCopyWith<$Res> {
  _$UserModelCopyWithImpl(this._value, this._then);

  final UserModel _value;
  // ignore: unused_field
  final $Res Function(UserModel) _then;

  @override
  $Res call({
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? nationalityCountryId = freezed,
    Object? birthDate = freezed,
    Object? workPhoneNumber = freezed,
    Object? homePhoneNumber = freezed,
    Object? address = freezed,
    Object? isActive = freezed,
    Object? insertDate = freezed,
    Object? id = freezed,
    Object? email = freezed,
    Object? phoneNumber = freezed,
    Object? phoneNumberCode = freezed,
    Object? phoneNumberWithCode = freezed,
    Object? photo = freezed,
    Object? token = freezed,
    Object? refreshToken = freezed,
    Object? userId = freezed,
    Object? genderArabicName = freezed,
    Object? genderEnglishName = freezed,
    Object? nationalityArabicName = freezed,
    Object? nationalityEnglishName = freezed,
    Object? countryArabicName = freezed,
    Object? countryEnglishName = freezed,
  }) {
    return _then(_value.copyWith(
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityCountryId: nationalityCountryId == freezed
          ? _value.nationalityCountryId
          : nationalityCountryId // ignore: cast_nullable_to_non_nullable
              as String,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as String,
      workPhoneNumber: workPhoneNumber == freezed
          ? _value.workPhoneNumber
          : workPhoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      homePhoneNumber: homePhoneNumber == freezed
          ? _value.homePhoneNumber
          : homePhoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      insertDate: insertDate == freezed
          ? _value.insertDate
          : insertDate // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumberCode: phoneNumberCode == freezed
          ? _value.phoneNumberCode
          : phoneNumberCode // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumberWithCode: phoneNumberWithCode == freezed
          ? _value.phoneNumberWithCode
          : phoneNumberWithCode // ignore: cast_nullable_to_non_nullable
              as String,
      photo: photo == freezed
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as String,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: refreshToken == freezed
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      genderArabicName: genderArabicName == freezed
          ? _value.genderArabicName
          : genderArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      genderEnglishName: genderEnglishName == freezed
          ? _value.genderEnglishName
          : genderEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityArabicName: nationalityArabicName == freezed
          ? _value.nationalityArabicName
          : nationalityArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityEnglishName: nationalityEnglishName == freezed
          ? _value.nationalityEnglishName
          : nationalityEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      countryArabicName: countryArabicName == freezed
          ? _value.countryArabicName
          : countryArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      countryEnglishName: countryEnglishName == freezed
          ? _value.countryEnglishName
          : countryEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_UserModelCopyWith<$Res> implements $UserModelCopyWith<$Res> {
  factory _$$_UserModelCopyWith(
          _$_UserModel value, $Res Function(_$_UserModel) then) =
      __$$_UserModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: "englishFullName")
          String firstName,
      @JsonKey(name: "arabicFullName")
          String lastName,
      @JsonKey(defaultValue: "", nullable: true)
          String nationalityCountryId,
      String birthDate,
      @JsonKey(defaultValue: "", nullable: true)
          String workPhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          String homePhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          String address,
      bool isActive,
      String insertDate,
      @JsonKey(defaultValue: 0, nullable: true)
          int id,
      @JsonKey(name: "userName")
          String email,
      @JsonKey(defaultValue: "", nullable: true)
          String phoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          String phoneNumberCode,
      @JsonKey(defaultValue: "", nullable: true)
          String phoneNumberWithCode,
      @JsonKey(defaultValue: "", nullable: true)
          String photo,
      @JsonKey(defaultValue: "", nullable: true)
          String token,
      @JsonKey(defaultValue: "", nullable: true)
          String refreshToken,
      @JsonKey(defaultValue: "", nullable: true)
          String userId,
      @JsonKey(defaultValue: "", nullable: true)
          String genderArabicName,
      @JsonKey(defaultValue: "", nullable: true)
      @JsonKey(defaultValue: "", nullable: true)
          String genderEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String nationalityArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          String countryArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          String countryEnglishName});
}

/// @nodoc
class __$$_UserModelCopyWithImpl<$Res> extends _$UserModelCopyWithImpl<$Res>
    implements _$$_UserModelCopyWith<$Res> {
  __$$_UserModelCopyWithImpl(
      _$_UserModel _value, $Res Function(_$_UserModel) _then)
      : super(_value, (v) => _then(v as _$_UserModel));

  @override
  _$_UserModel get _value => super._value as _$_UserModel;

  @override
  $Res call({
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? nationalityCountryId = freezed,
    Object? birthDate = freezed,
    Object? workPhoneNumber = freezed,
    Object? homePhoneNumber = freezed,
    Object? address = freezed,
    Object? isActive = freezed,
    Object? insertDate = freezed,
    Object? id = freezed,
    Object? email = freezed,
    Object? phoneNumber = freezed,
    Object? phoneNumberCode = freezed,
    Object? phoneNumberWithCode = freezed,
    Object? photo = freezed,
    Object? token = freezed,
    Object? refreshToken = freezed,
    Object? userId = freezed,
    Object? genderArabicName = freezed,
    Object? genderEnglishName = freezed,
    Object? nationalityArabicName = freezed,
    Object? nationalityEnglishName = freezed,
    Object? countryArabicName = freezed,
    Object? countryEnglishName = freezed,
  }) {
    return _then(_$_UserModel(
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityCountryId: nationalityCountryId == freezed
          ? _value.nationalityCountryId
          : nationalityCountryId // ignore: cast_nullable_to_non_nullable
              as String,
      birthDate: birthDate == freezed
          ? _value.birthDate
          : birthDate // ignore: cast_nullable_to_non_nullable
              as String,
      workPhoneNumber: workPhoneNumber == freezed
          ? _value.workPhoneNumber
          : workPhoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      homePhoneNumber: homePhoneNumber == freezed
          ? _value.homePhoneNumber
          : homePhoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: isActive == freezed
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      insertDate: insertDate == freezed
          ? _value.insertDate
          : insertDate // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumberCode: phoneNumberCode == freezed
          ? _value.phoneNumberCode
          : phoneNumberCode // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumberWithCode: phoneNumberWithCode == freezed
          ? _value.phoneNumberWithCode
          : phoneNumberWithCode // ignore: cast_nullable_to_non_nullable
              as String,
      photo: photo == freezed
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as String,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: refreshToken == freezed
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      genderArabicName: genderArabicName == freezed
          ? _value.genderArabicName
          : genderArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      genderEnglishName: genderEnglishName == freezed
          ? _value.genderEnglishName
          : genderEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityArabicName: nationalityArabicName == freezed
          ? _value.nationalityArabicName
          : nationalityArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      nationalityEnglishName: nationalityEnglishName == freezed
          ? _value.nationalityEnglishName
          : nationalityEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
      countryArabicName: countryArabicName == freezed
          ? _value.countryArabicName
          : countryArabicName // ignore: cast_nullable_to_non_nullable
              as String,
      countryEnglishName: countryEnglishName == freezed
          ? _value.countryEnglishName
          : countryEnglishName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_UserModel implements _UserModel {
  _$_UserModel(
      {@JsonKey(name: "englishFullName")
          required this.firstName,
      @JsonKey(name: "arabicFullName")
          required this.lastName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.nationalityCountryId,
      required this.birthDate,
      @JsonKey(defaultValue: "", nullable: true)
          required this.workPhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          required this.homePhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          required this.address,
      required this.isActive,
      required this.insertDate,
      @JsonKey(defaultValue: 0, nullable: true)
          required this.id,
      @JsonKey(name: "userName")
          required this.email,
      @JsonKey(defaultValue: "", nullable: true)
          required this.phoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          required this.phoneNumberCode,
      @JsonKey(defaultValue: "", nullable: true)
          required this.phoneNumberWithCode,
      @JsonKey(defaultValue: "", nullable: true)
          required this.photo,
      @JsonKey(defaultValue: "", nullable: true)
          required this.token,
      @JsonKey(defaultValue: "", nullable: true)
          required this.refreshToken,
      @JsonKey(defaultValue: "", nullable: true)
          required this.userId,
      @JsonKey(defaultValue: "", nullable: true)
          required this.genderArabicName,
      @JsonKey(defaultValue: "", nullable: true)
      @JsonKey(defaultValue: "", nullable: true)
          required this.genderEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.nationalityArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.countryArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required this.countryEnglishName});

  factory _$_UserModel.fromJson(Map<String, dynamic> json) =>
      _$$_UserModelFromJson(json);

  @override
  @JsonKey(name: "englishFullName")
  String firstName;
  @override
  @JsonKey(name: "arabicFullName")
  String lastName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String nationalityCountryId;
  @override
  String birthDate;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String workPhoneNumber;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String homePhoneNumber;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String address;
  @override
  bool isActive;
  @override
  String insertDate;
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int id;
  @override
  @JsonKey(name: "userName")
  String email;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String phoneNumber;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String phoneNumberCode;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String phoneNumberWithCode;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String photo;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String token;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String refreshToken;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String userId;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String genderArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  @JsonKey(defaultValue: "", nullable: true)
  String genderEnglishName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String nationalityArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String nationalityEnglishName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String countryArabicName;
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String countryEnglishName;

  @override
  String toString() {
    return 'UserModel(firstName: $firstName, lastName: $lastName, nationalityCountryId: $nationalityCountryId, birthDate: $birthDate, workPhoneNumber: $workPhoneNumber, homePhoneNumber: $homePhoneNumber, address: $address, isActive: $isActive, insertDate: $insertDate, id: $id, email: $email, phoneNumber: $phoneNumber, phoneNumberCode: $phoneNumberCode, phoneNumberWithCode: $phoneNumberWithCode, photo: $photo, token: $token, refreshToken: $refreshToken, userId: $userId, genderArabicName: $genderArabicName, genderEnglishName: $genderEnglishName, nationalityArabicName: $nationalityArabicName, nationalityEnglishName: $nationalityEnglishName, countryArabicName: $countryArabicName, countryEnglishName: $countryEnglishName)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      __$$_UserModelCopyWithImpl<_$_UserModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserModelToJson(
      this,
    );
  }
}

abstract class _UserModel implements UserModel {
  factory _UserModel(
      {@JsonKey(name: "englishFullName")
          required String firstName,
      @JsonKey(name: "arabicFullName")
          required String lastName,
      @JsonKey(defaultValue: "", nullable: true)
          required String nationalityCountryId,
      required String birthDate,
      @JsonKey(defaultValue: "", nullable: true)
          required String workPhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          required String homePhoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          required String address,
      required bool isActive,
      required String insertDate,
      @JsonKey(defaultValue: 0, nullable: true)
          required int id,
      @JsonKey(name: "userName")
          required String email,
      @JsonKey(defaultValue: "", nullable: true)
          required String phoneNumber,
      @JsonKey(defaultValue: "", nullable: true)
          required String phoneNumberCode,
      @JsonKey(defaultValue: "", nullable: true)
          required String phoneNumberWithCode,
      @JsonKey(defaultValue: "", nullable: true)
          required String photo,
      @JsonKey(defaultValue: "", nullable: true)
          required String token,
      @JsonKey(defaultValue: "", nullable: true)
          required String refreshToken,
      @JsonKey(defaultValue: "", nullable: true)
          required String userId,
      @JsonKey(defaultValue: "", nullable: true)
          required String genderArabicName,
      @JsonKey(defaultValue: "", nullable: true)
      @JsonKey(defaultValue: "", nullable: true)
          required String genderEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required String nationalityArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required String nationalityEnglishName,
      @JsonKey(defaultValue: "", nullable: true)
          required String countryArabicName,
      @JsonKey(defaultValue: "", nullable: true)
          required String countryEnglishName}) = _$_UserModel;

  factory _UserModel.fromJson(Map<String, dynamic> json) =
      _$_UserModel.fromJson;

  @override
  @JsonKey(name: "englishFullName")
  String get firstName;
  @JsonKey(name: "englishFullName")
  set firstName(String value);
  @override
  @JsonKey(name: "arabicFullName")
  String get lastName;
  @JsonKey(name: "arabicFullName")
  set lastName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get nationalityCountryId;
  @JsonKey(defaultValue: "", nullable: true)
  set nationalityCountryId(String value);
  @override
  String get birthDate;
  set birthDate(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get workPhoneNumber;
  @JsonKey(defaultValue: "", nullable: true)
  set workPhoneNumber(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get homePhoneNumber;
  @JsonKey(defaultValue: "", nullable: true)
  set homePhoneNumber(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get address;
  @JsonKey(defaultValue: "", nullable: true)
  set address(String value);
  @override
  bool get isActive;
  set isActive(bool value);
  @override
  String get insertDate;
  set insertDate(String value);
  @override
  @JsonKey(defaultValue: 0, nullable: true)
  int get id;
  @JsonKey(defaultValue: 0, nullable: true)
  set id(int value);
  @override
  @JsonKey(name: "userName")
  String get email;
  @JsonKey(name: "userName")
  set email(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get phoneNumber;
  @JsonKey(defaultValue: "", nullable: true)
  set phoneNumber(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get phoneNumberCode;
  @JsonKey(defaultValue: "", nullable: true)
  set phoneNumberCode(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get phoneNumberWithCode;
  @JsonKey(defaultValue: "", nullable: true)
  set phoneNumberWithCode(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get photo;
  @JsonKey(defaultValue: "", nullable: true)
  set photo(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get token;
  @JsonKey(defaultValue: "", nullable: true)
  set token(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get refreshToken;
  @JsonKey(defaultValue: "", nullable: true)
  set refreshToken(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get userId;
  @JsonKey(defaultValue: "", nullable: true)
  set userId(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get genderArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set genderArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  @JsonKey(defaultValue: "", nullable: true)
  String get genderEnglishName;
  @JsonKey(defaultValue: "", nullable: true)
  @JsonKey(defaultValue: "", nullable: true)
  set genderEnglishName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get nationalityArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set nationalityArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get nationalityEnglishName;
  @JsonKey(defaultValue: "", nullable: true)
  set nationalityEnglishName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get countryArabicName;
  @JsonKey(defaultValue: "", nullable: true)
  set countryArabicName(String value);
  @override
  @JsonKey(defaultValue: "", nullable: true)
  String get countryEnglishName;
  @JsonKey(defaultValue: "", nullable: true)
  set countryEnglishName(String value);
  @override
  @JsonKey(ignore: true)
  _$$_UserModelCopyWith<_$_UserModel> get copyWith =>
      throw _privateConstructorUsedError;
}
