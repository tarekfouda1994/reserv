import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';

extension Validator on String {
  String? noValidate() {
    return null;
  }

  String? validateEmpty({String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    }
    return null;
  }

  String? validateExpiryDate({String? message}) {
    var dateParts = split("/");
    var month = int.tryParse(dateParts.first)??0;
    var year = int.tryParse(dateParts.last)??0;
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (month < 1 || month > 12 || dateParts.length != 2) {
      return message ?? tr("dateValidation");
    } else if (length != 7 || _checkExpDateValidate(trim()) || _validateExpiryDateBool()) {
      return message ?? tr("dateValidation");
    }
    return null;
  }

  bool _validateExpiryDateBool() {
    var year = int.parse(split("/").toList().last);
    var month = int.parse(split("/").toList().first);
    return DateTime(year, month).isBefore(DateTime.now());
  }

  String? validateCard(BuildContext context, {String? message}) {
    var languageCode = context.read<DeviceCubit>().state.model.locale.languageCode;
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (length != (languageCode == "en" ? 19 : 16)) {
      return message ?? tr("card_num");
    }
    return null;
  }

  String? validateCVV({String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (length != 3) {
      return message ?? tr("cvv_validation");
    }
    return null;
  }

  String? validateName({String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (RegExp(r'[!@#<>?.,":_`~;[\]\\|=+)(*&^%0-9-]').hasMatch(this)) {
      return message ?? tr("NameValidation");
    } else if (length > 15) {
      return message ?? tr("nameCher");
    }
    return null;
  }

  String? validateAddress({String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (length < 5 || length > 100) {
      return message ?? tr("validateName");
    }
    return null;
  }

  String? validatePassword({String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (!RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~])').hasMatch(this)) {
      return message ?? tr("passValidation");
    } else if (RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~])').hasMatch(this) &&
        trim().length < 8) {
      return message ?? tr("passwordValidation");
    }
    return null;
  }

  String? validateEmail({String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this)) {
      return message ?? tr("mailValidation");
    }
    return null;
  }

  String? validateEmailORNull({String? message}) {
    if (trim().isNotEmpty) {
      if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(this)) {
        return message ?? tr("mailValidation");
      }
    }
    return null;
  }

  String? validatePhone({String? message, int lengthNum = 10}) {
    var phone = replaceAll(" ", "");
    if (phone.trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (lengthNum != phone.length) {
      return message ?? tr("phoneValidation");
    } else if (!RegExp(r'(^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,4}$)')
        .hasMatch(phone)) {
      return message ?? tr("phoneValidation");
    }
    return null;
  }

  String? validatePhoneOrNull({String? message}) {
    if (trim().isEmpty) {
      return null;
    } else if (!RegExp(
                r'(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)')
            .hasMatch(this) ||
        length < 10) {
      return message ?? tr("phoneValidation");
    }
    return null;
  }

  String? validatePasswordConfirm({required String pass, String? message}) {
    if (trim().isEmpty) {
      return message ?? tr("fillField");
    } else if (this != pass) {
      return message ?? tr("confirmValidation");
    }
    return null;
  }
}

bool _checkExpDateValidate(String text) {
  if (text.contains("/")) {
    int first = int.parse(text.split("/").toList().first);
    int last = int.parse(text.split("/").toList().last);
    return (last < DateTime.now().year) && (first < 0 || first > 12) ||
        (last == DateTime.now().year && first < DateTime.now().month);
  } else {
    return false;
  }
}

String? validateDropDown(dynamic model, {String? message}) {
  if (model == null) {
    return message ?? tr("fillField");
  }
  return null;
}
