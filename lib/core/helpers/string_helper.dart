import 'package:collection/collection.dart';

abstract class StringHelper {
  static String? getNormalizedNameFromDottedString(String str) {
    return str.split('.').lastOrNull;
  }

  /// Get Firebase document's id based on BaseURL
  /// e.g
  /// BaseURL: "https://dev.osos.roaa.tech/"
  /// Then the doc id: "dev.osos.roaa.tech"
  static String getVersionFirebaseDocId(String baseURL) {
    final host = Uri.parse(baseURL).host;
    if (host.isNotEmpty) {
      return host;
    } else {
      return baseURL
          .replaceAll('https://', '')
          .replaceAll('http://', '')
          .replaceAll('/', '')
          .replaceAll('"', '')
          .replaceAll("'", '')
          .replaceAll(':', '')
          .replaceAll('"', '');
    }
  }
}
