import 'package:flutter_tdd/core/helpers/utilities.dart';
import 'package:flutter_tdd/features/reservation/data/models/service_list_model/service_list_model.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/service_time_entity/service_time_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/entities/time_slot_entity.dart';
import 'package:flutter_tdd/features/reservation/domain/use_cases/get_time_slots.dart';
import 'package:intl/intl.dart';

import 'di.dart';

class ServicesSlotsHelper {
  static Future<List<String>> fetchTimeSlots(
      String businessId, String serviceId, String staffId, DateTime dateTime) async {
    final params = TimeSlotEntity(
      businessId: businessId,
      serviceId: serviceId,
      staffId: staffId,
      dateTime: DateFormat("yyyy-MM-dd").format(dateTime),
    );
    var data = await GetTimeSlots()(params);
    return data.toSet().toList();
  }

  static List<List<ServiceTimeEntity>> getSelectedItems(
      List<ServiceTimeEntity> first, List<ServiceTimeEntity> second) {
    List<List<ServiceTimeEntity>> selectedTimes = [];
    for (var firstItem in first) {
      for (var secondItem in second) {
        int difference = secondItem.date.difference(firstItem.date).inMinutes;
        if (difference >= firstItem.duration) {
          var selectedItems = [firstItem, secondItem];
          selectedTimes.add(selectedItems);
          break;
        }
      }
    }

    return selectedTimes;
  }

  static Future<List<List<ServiceTimeEntity>>> manipulateTowServices(
      List<ServiceListModel> services, String businessId, DateTime selectedDate) async {
    List<List<ServiceTimeEntity>> availableTimes = [];
    for (var item in services) {
      var data = await fetchTimeSlots(businessId, item.businessServiceID, item.staffId, selectedDate);
      if (data.isNotEmpty) {
        var itemDuration = int.parse(item.serviceDuration.toString());
        var times = getIt<Utilities>().parseTimes(data).map((e) {
          return ServiceTimeEntity(date: e, duration: itemDuration, service: item.businessServiceID);
        }).toList();
        availableTimes.add(times);
      } else {
        return [];
      }
    }
    return getSelectedItems(availableTimes.first, availableTimes.last);
  }

  static Future<List<List<ServiceTimeEntity>>> manipulateThreeServices(
      List<ServiceListModel> services, String businessId, DateTime selectedDate) async {
    List<List<ServiceTimeEntity>> availableTimes = [];
    for (var item in services) {
      var data = await fetchTimeSlots(businessId, item.businessServiceID, item.staffId, selectedDate);
      if (data.isNotEmpty) {
        var itemDuration = int.parse(item.serviceDuration.toString());
        var times = getIt<Utilities>().parseTimes(data).map((e) {
          return ServiceTimeEntity(date: e, duration: itemDuration, service: item.businessServiceID);
        }).toList();
        availableTimes.add(times);
      } else {
        return [];
      }
    }

    var first = availableTimes[0];
    var second = availableTimes[1];
    var third = availableTimes[2];
    List<List<ServiceTimeEntity>> selectedTimes = getSelectedItems(first, second);
    var selectedSecond = selectedTimes
        .map((e) => e.firstWhere((element) => element.service == services[1].businessServiceID))
        .toSet()
        .toList();
    List<List<ServiceTimeEntity>> secondThirdTimes = getSelectedItems(selectedSecond, third);
    var selectedAll = secondThirdTimes.map((e) {
      DateTime selectedDate = e.firstWhere((element) => element.service == services[1].businessServiceID).date;
      var selectedFirst =
          selectedTimes.firstWhere((element) => e.any((item) => item.date == selectedDate)).first;
      e.insert(0,selectedFirst);
      return e;
    }).toList();

    return selectedAll;
  }
}
