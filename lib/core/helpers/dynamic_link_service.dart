import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tdd/features/search/presentation/pages/product_details/product_details_imports.dart';

class DynamicLinkService {
  Future<Uri> createDynamicLink(String id) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
        uriPrefix: 'https://smartcube.page.link',
        link: Uri.parse("https://smartcube.reserva.com/$id"),
        androidParameters: AndroidParameters(
            packageName: "co.smartcube.Reserva",
            minimumVersion: 1,
            fallbackUrl: Uri.parse("https://www.youtube.com/watch?v=r7qovpFAGrQ"),
        ),
        dynamicLinkParametersOptions: DynamicLinkParametersOptions(
            shortDynamicLinkPathLength: ShortDynamicLinkPathLength.unguessable),
        iosParameters: IosParameters(
            bundleId: "co.smartcube.Reserva",
            minimumVersion: '1',
            fallbackUrl: Uri.parse("https://www.youtube.com/watch?v=r7qovpFAGrQ")));
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    Uri url = shortLink.shortUrl;
    return url;
  }

  Future<void> retrieveDynamicLink(BuildContext context) async {
    try {
      FirebaseDynamicLinks.instance.onLink(onSuccess: (PendingDynamicLinkData? dynamicLink) async {
        final Uri? deepLink = dynamicLink?.link;
        if (deepLink != null) {
          var id = deepLink.path.split("/").last;
          // if (deepLink.path.contains("/details/")) {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return ProductDetails(id: id);
          }));
          return;
          // }
        }
      });
      final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
      final Uri? deepLink = data?.link;
      if (deepLink != null) {
        var id = deepLink.path.split("/").last;
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return ProductDetails(id: id);
        }));
        return;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
