// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/auth/data/data_sources/auth_data_source.dart' as _i3;
import '../../features/auth/data/data_sources/auth_data_source_impl.dart'
    as _i4;
import '../../features/auth/data/repositories/auth_repo_impl.dart' as _i6;
import '../../features/auth/domain/repositories/auth_repository.dart' as _i5;
import '../../features/profile/data/data_sources/profile_data_source.dart'
    as _i29;
import '../../features/profile/data/data_sources/profile_data_source_impl.dart'
    as _i30;
import '../../features/profile/data/repositories/profile_repo_imle.dart'
    as _i32;
import '../../features/profile/domain/repositories/profile_repository.dart'
    as _i31;
import '../../features/reschedule/data/data_sources/reschedule_data_source.dart'
    as _i34;
import '../../features/reschedule/data/data_sources/reschedule_data_source_impl.dart'
    as _i35;
import '../../features/reschedule/data/repositories/reschedule_repo_imp.dart'
    as _i37;
import '../../features/reschedule/domain/repositories/reschedule_repo.dart'
    as _i36;
import '../../features/reservation/data/data_sources/reservation_data_source.dart'
    as _i38;
import '../../features/reservation/data/data_sources/reservation_data_source_impl.dart'
    as _i39;
import '../../features/reservation/data/repositories/reservation_repo_imp.dart'
    as _i41;
import '../../features/reservation/domain/repositories/reservation_repo.dart'
    as _i40;
import '../../features/search/data/data_sources/impl_home_remote_data_source.dart'
    as _i24;
import '../../features/search/data/repositories/impl_base_repository.dart'
    as _i8;
import '../../features/search/domain/repositories/base_repository.dart' as _i7;
import '../../features/versioning/data/data_source/i_versioning_remote_data_source.dart'
    as _i20;
import '../../features/versioning/data/data_source/versioning_remote_data_source.dart'
    as _i21;
import '../../features/versioning/data/repository/i_versioning_repository.dart'
    as _i22;
import '../../features/versioning/domain/repository/versioning_repository.dart'
    as _i23;
import '../../features/versioning/presentation/controller/versioning_controller.dart'
    as _i43;
import '../http/dio_helper/actions/delete.dart' as _i9;
import '../http/dio_helper/actions/get.dart' as _i14;
import '../http/dio_helper/actions/patch.dart' as _i27;
import '../http/dio_helper/actions/post.dart' as _i28;
import '../http/dio_helper/actions/put.dart' as _i33;
import '../http/dio_helper/utils/dio_header.dart' as _i10;
import '../http/dio_helper/utils/dio_options.dart' as _i11;
import '../http/dio_helper/utils/handle_errors.dart' as _i17;
import '../http/dio_helper/utils/handle_json_response.dart' as _i18;
import '../http/dio_helper/utils/handle_request_body.dart' as _i19;
import '../http/generic_http/generic_http.dart' as _i13;
import '../network/network_info.dart' as _i26;
import 'firebase_analytics_helper.dart' as _i12;
import 'global_context.dart' as _i15;
import 'global_notification.dart' as _i16;
import 'loading_helper.dart' as _i25;
import 'utilities.dart' as _i42; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.AuthDataSource>(() => _i4.AuthDataSourceImpl());
  gh.factory<_i5.AuthRepository>(() => _i6.AuthRepositoryImpl());
  gh.factory<_i7.BaseRepository>(() => _i8.ImplBaseRepository());
  gh.lazySingleton<_i9.Delete>(() => _i9.Delete());
  gh.lazySingleton<_i10.DioHeader>(() => _i10.DioHeader());
  gh.lazySingleton<_i11.DioOptions>(() => _i11.DioOptions());
  gh.lazySingleton<_i12.FirebaseAnalyticsHelper>(
      () => _i12.FirebaseAnalyticsHelper());
  gh.lazySingleton<_i13.GenericHttpImpl<dynamic>>(
      () => _i13.GenericHttpImpl<dynamic>());
  gh.lazySingleton<_i14.Get>(() => _i14.Get());
  gh.lazySingleton<_i15.GlobalContext>(() => _i15.GlobalContext());
  gh.lazySingleton<_i16.GlobalNotification>(() => _i16.GlobalNotification());
  gh.lazySingleton<_i17.HandleErrors>(() => _i17.HandleErrors());
  gh.lazySingleton<_i18.HandleJsonResponse<dynamic>>(
      () => _i18.HandleJsonResponse<dynamic>());
  gh.lazySingleton<_i19.HandleRequestBody>(() => _i19.HandleRequestBody());
  gh.factory<_i20.IVersioningRemoteDataSource>(
      () => _i21.VersioningRemoteDataSource());
  gh.factory<_i22.IVersioningRepository>(() => _i23.VersioningRepository());
  gh.lazySingleton<_i24.ImplHomeRemoteDataSource>(
      () => _i24.ImplHomeRemoteDataSource());
  gh.lazySingleton<_i25.LoadingHelper>(() => _i25.LoadingHelper());
  gh.factory<_i26.NetworkInfo>(() => _i26.NetworkInfoImpl());
  gh.lazySingleton<_i27.Patch>(() => _i27.Patch());
  gh.lazySingleton<_i28.Post>(() => _i28.Post());
  gh.factory<_i29.ProfileDataSource>(() => _i30.AuthDataSourceImpl());
  gh.factory<_i31.ProfileRepository>(() => _i32.AuthRepositoryImpl());
  gh.lazySingleton<_i33.Put>(() => _i33.Put());
  gh.factory<_i34.RescheduleDataSource>(() => _i35.RescheduleDataSourceImpl());
  gh.factory<_i36.RescheduleRepo>(() => _i37.RescheduleRepoImpl());
  gh.factory<_i38.ReservationDataSource>(
      () => _i39.ReservationDataSourceImpl());
  gh.factory<_i40.ReservationRepository>(
      () => _i41.ReservationRepositoryImpl());
  gh.lazySingleton<_i42.Utilities>(() => _i42.Utilities());
  gh.lazySingleton<_i43.VersioningController>(
      () => _i43.VersioningController());
  return get;
}
