import 'package:auto_route/auto_route.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/constants/toast_slide_animation.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/build_header_bottom_sheet.dart';
import 'package:flutter_tdd/res.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:tf_custom_widgets/widgets/DefaultButton.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

enum ToastType { success, error, info, warning }

class CustomToast {
  static Map<ToastType, Color> toastBgColors = {
    ToastType.success: MyColors.successColor,
    ToastType.error: MyColors.errorColor,
    ToastType.info: MyColors.infoColor,
    ToastType.warning: MyColors.infoColor,
  };
  static Map<ToastType, Color> toastTextColors = {
    ToastType.success: MyColors.primaryDark,
    ToastType.error: MyColors.errorColor,
    ToastType.info: MyColors.infoColor,
    ToastType.warning: MyColors.infoColor,
  };

  static var toastCancel;

  static showSnakeBar(String msg, String title,
      {ToastType type = ToastType.error}) {
    _setVibrate(type);
    BuildContext context = getIt<GlobalContext>().context();
    var device = context.read<DeviceCubit>().state.model;
    toastCancel = BotToast.showAttachedWidget(
      attachedBuilder: (_) => _toastWidget(context, msg, title, type),
      duration: Duration(seconds: 5),
      target: Offset(device.isTablet ? 0 : 520, device.isTablet ? 0 : 520),
      animationDuration: Duration(milliseconds: 200),
      wrapToastAnimation: (controller, cancel, Widget child) =>
          CustomAttachedAnimation(
        controller: controller,
        child: child,
      ),
    );
  }

  static showSimpleToast(
      {required String msg,
      required String title,
      ToastType type = ToastType.error}) {
    showSnakeBar(msg, title, type: type);
  }

  static void _setVibrate(ToastType type) {
    switch (type) {
      case ToastType.error:
        Vibrate.feedback(FeedbackType.error);
        break;
      case ToastType.success:
        Vibrate.feedback(FeedbackType.success);
        break;
      case ToastType.info:
        Vibrate.feedback(FeedbackType.light);
        break;
      case ToastType.warning:
        Vibrate.feedback(FeedbackType.warning);
        break;
    }
  }

  static Widget _toastWidget(
      BuildContext context, String msg, String title, ToastType type) {
    var device = context.read<DeviceCubit>().state.model;
    Widget toast = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: 5),
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: MyColors.blackOpacity.withOpacity(.7),
                offset: Offset(0, 15),
                spreadRadius: -15,
                blurRadius: 20.0,
              ),
            ],
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(12.sp)),
            color: Colors.white,
          ),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10.0.w, vertical: 5),
            padding: EdgeInsets.symmetric(horizontal: 20.0.w, vertical: 12.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                  top: Radius.circular(12.sp), bottom: Radius.circular(12.sp)),
              color: toastBgColors[type]!.withOpacity(.2),
            ),
            child: Row(
              textDirection: device.locale == Locale('en', 'US')
                  ? TextDirection.rtl
                  : TextDirection.ltr,
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: title,
                        color: toastTextColors[type],
                        size: device.isTablet ? 7.sp : 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(height: 8.h),
                      MyText(
                        title: msg,
                        // overflow: TextOverflow.ellipsis,
                        color: toastTextColors[type],
                        size: device.isTablet ? 6.sp : 9.sp,
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10.w),
                GestureDetector(
                  onTap: () => toastCancel(),
                  child: CircleAvatar(
                    backgroundColor: toastTextColors[type],
                    radius: 15.sp,
                    child: Icon(
                      getIcon(type),
                      size: device.isTablet ? 15.sp : 20.sp,
                      color: MyColors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );

    return toast;
  }

  static IconData getIcon(ToastType type) {
    switch (type) {
      case ToastType.error:
        return Icons.close_rounded;
      case ToastType.success:
        return Icons.done_rounded;
      case ToastType.info:
        return Icons.info_outline_rounded;
      default:
        return Icons.warning_amber_rounded;
    }
  }

  static customAuthDialog({bool isSalon = false}) {
    BuildContext context = getIt<GlobalContext>().context();
    var device = context.read<DeviceCubit>().state.model;
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.sp),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
            height: device.isTablet ? 165.h : 160.h,
            padding: EdgeInsets.symmetric(
              horizontal: 12.w,
              vertical: 12.h,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BuildHeaderBottomSheet(
                  margin: EdgeInsets.only(bottom: 10.h),
                  title:
                      "${tr("add")} ${isSalon ? (tr("Salon")) : (tr("service"))} ${tr("toFavorites")}",
                  titleColor: MyColors.primary,
                  ladingIcon: Icon(
                    Icons.favorite_border_outlined,
                    size: device.isTablet ? 18.sp : 25.sp,
                    color: MyColors.primary,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsetsDirectional.only(start: 35.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        MyText(
                          title: tr("registeredUsersOnly"),
                          color: MyColors.grey,
                          size: device.isTablet ? 5.5.sp : 10.sp,
                        ),
                        SizedBox(height: 6.sp),
                        MyText(
                          title: tr("loginContinue"),
                          color: MyColors.black,
                          size: device.isTablet ? 5.5.sp : 10.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.sp, top: 5.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Expanded(
                        child: DefaultButton(
                          height: device.isTablet ? 60.sm : 50.sm,
                          borderRadius: BorderRadius.circular(50.r),
                          margin: EdgeInsets.zero,
                          color: Colors.transparent,
                          borderColor: MyColors.primaryDark,
                          textColor: MyColors.black,
                          fontSize: device.isTablet ? 7.sp : 11.sp,
                          title: tr("ignore"),
                          onTap: () => Navigator.of(context).pop(),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: DefaultButton(
                            height: device.isTablet ? 60.sm : 50.sm,
                            borderRadius: BorderRadius.circular(50.r),
                            margin: EdgeInsets.zero,
                            title: tr("login"),
                            fontSize: device.isTablet ? 7.sp : 11.sp,
                            onTap: () => AutoRouter.of(context)
                                .push(HomeRoute(index: 2))),
                      ),
                    ],
                  ),
                ),
              ],
            ));
      },
    );
  }

  static customConfirmDialog({
    required String title,
    required String content,
    Widget? recordItem,
    String? btnName,
    double? height,
    double? heightBottom,
    required Function() onConfirm,
  }) {
    BuildContext context = getIt<GlobalContext>().context();
    var device = context.read<DeviceCubit>().state.model;
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15.sp),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          height: height ?? 160.h,
          padding: EdgeInsets.symmetric(
            horizontal: 12.w,
            vertical: 12.h,
          ),
          decoration: BoxDecoration(
              color: MyColors.white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  SvgPicture.asset(
                    Res.trash,
                    width: device.isTablet ? 12.sp : null,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.sp),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MyText(
                            title: title,
                            color: MyColors.errorColor,
                            size: device.isTablet ? 7.sp : 11.sp,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(height: 2.h),
                          MyText(
                            title: tr("thisActionReversible"),
                            color: MyColors.grey,
                            size: device.isTablet ? 5.5.sp : 9.sp,
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () => Navigator.of(context).pop(),
                    child: CircleAvatar(
                      backgroundColor: MyColors.black,
                      radius: 15.sp,
                      child: Icon(
                        Icons.close,
                        size: device.isTablet ? 15.sp : 20.sp,
                        color: MyColors.white,
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsetsDirectional.only(start: 16.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if(content.isNotEmpty)
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          MyText(
                            title: content,
                            color: MyColors.black,
                            size: device.isTablet ? 5.5.sp : 9.sp,
                          ),
                        ],
                      ),
                      SizedBox(height: 5.sp),
                      recordItem ?? Container()
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: heightBottom ?? 8.sm),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: DefaultButton(
                        height: device.isTablet ? 60.sm : 50.sm,
                        borderRadius: BorderRadius.circular(50.r),
                        margin: EdgeInsets.zero,
                        color: MyColors.black,
                        textColor: MyColors.white,
                        fontSize: device.isTablet ? 7.sp : 11.sp,
                        title: tr("keep"),
                        onTap: () => Navigator.of(context).pop(),
                      ),
                    ),
                    SizedBox(width: 8.w),
                    Expanded(
                      child: DefaultButton(
                        margin: EdgeInsets.zero,
                        height: device.isTablet ? 60.sm : 50.sm,
                        borderRadius: BorderRadius.circular(50.r),
                        borderColor: MyColors.errorColor,
                        textColor: MyColors.errorColor,
                        color: MyColors.white,
                        title: btnName ?? tr("delete"),
                        fontSize: device.isTablet ? 7.sp : 11.sp,
                        onTap: onConfirm,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
