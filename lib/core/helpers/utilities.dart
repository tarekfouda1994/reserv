import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:country_picker/country_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/dynamic_link_service.dart';
import 'package:flutter_tdd/core/helpers/loading_helper.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/routes/router_imports.gr.dart';
import 'package:flutter_tdd/core/widgets/country_code_list/country_code_sheet.dart';
import 'package:flutter_tdd/features/profile/domain/entities/geo_code_address_entity.dart';
import 'package:flutter_tdd/features/profile/domain/use_cases/get_address.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart' as pm;
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../bloc/device_cubit/device_cubit.dart';
import '../models/user_model/user_model.dart';
import 'custom_toast.dart';
import 'di.dart';
import 'global_context.dart';

@lazySingleton
class Utilities {
  copyToClipBoard(String text) {
    Clipboard.setData(ClipboardData(text: text)).then((value) {
      CustomToast.showSnakeBar(
        tr("copied_successfully"),
        tr("success_toast"),
        type: ToastType.success,
      );
    });
  }

  void shareSinglePage(BuildContext context, String id, String businessOrServiceName) async {
    getIt<LoadingHelper>().showLoadingDialog();
    final Uri url = await DynamicLinkService().createDynamicLink(id);
    Share.share("${getShareHeaderName(businessOrServiceName, false)}\n${url.toString()}")
        .whenComplete(() {
      getIt<LoadingHelper>().dismissDialog();
    });
  }

  String getShareHeaderName(String businessOrServiceName, bool isService) {
    final BuildContext context = getIt<GlobalContext>().context();
    var lang = context.read<DeviceCubit>().state.model.locale.languageCode;
    if (lang == "ar") {
      return "  يمكنك زيارة ${isService ? "خدمه" : "اعمال"} $businessOrServiceName ";
    } else {
      return "You can visit this $businessOrServiceName ${isService ? "service" : "business"}";
    }
  }

  String customizePhoneNumber(String phone, String? code) {
    String phoneNumber = "";
    if (phone.startsWith("0")) {
      phoneNumber = phone.replaceFirst("0", code ?? "+20");
    } else {
      phoneNumber = "${(code ?? "+20")}$phone";
    }
    return phoneNumber;
  }

  Future<File?> getImage() async {
    await _imagesPermissions();
    if (await _isAllowed()) {
      final ImagePicker picker = ImagePicker();
      final XFile? result = await picker.pickImage(source: ImageSource.gallery);
      if (result != null) {
        File file = File(result.path);
        int sizeInBytes = file.lengthSync();
        double sizeInMb = sizeInBytes / (1024 * 1024);
        if (sizeInMb > 2) {
          CustomToast.showSimpleToast(
            msg: "Maximum limit 2MB,Choose another image",
            title: "Validation Image",
          );
          return null;
        }
        return file;
      } else {
        return null;
      }
    }
    return null;
  }

  Future<bool> _isAllowed() async {
    return await pm.Permission.photos.isGranted || await pm.Permission.storage.isGranted;
  }

  Future<void> _imagesPermissions() async {
    await pm.Permission.photos.request();
    await pm.Permission.storage.request();
  }

  String getLocalizedValue(String ar, String en, {BuildContext? ctx}) {
    final BuildContext context = ctx ?? getIt<GlobalContext>().context();
    var lang = context.read<DeviceCubit>().state.model.locale.languageCode;
    if (lang == "ar") {
      return ar;
    } else {
      return en;
    }
  }

  DateTime parseTime(String time) {
    if (time.toLowerCase().contains("am")) {
      time = time.replaceAll("AM", "").trim();
      return time.split(":").first == "12"
          ? DateTime.parse("2022-05-05 $time").add(Duration(hours: 12))
          : DateTime.parse("2022-05-05 $time");
    } else {
      time = time.replaceAll("PM", "").trim();
      return time.split(":").first == "12"
          ? DateTime.parse("2022-05-05 12:00")
          : DateTime.parse("2022-05-05 $time").add(Duration(hours: 12));
    }
  }

  List<DateTime> parseTimes(List<String> times) {
    List<DateTime> parsedTimes = [];
    for (var item in times) {
      if (item.toLowerCase().contains("am") || item.toLowerCase().contains("pm")) {
        parsedTimes.add(parseTime(item));
      } else {
        parsedTimes.add(DateTime.parse("2022-05-05 $item"));
      }
    }
    return parsedTimes;
  }

  String formatDate(DateTime? dateTime) {
    if (dateTime == null) {
      return "";
    }
    return DateFormat("yyyy-MM-dd 'T' hh:mm:ss").format(dateTime).replaceAll(" ", "");
  }

  Future<void> saveUserData(UserModel model) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user", json.encode(model.toJson()));
  }

  Future<void> saveToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", json.encode(token));
  }

  Future<File?> getAttachmentFile(FileType fileType) async {
    if (fileType == FileType.any) {
      return await getAPdfFile();
    }
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: fileType,
      allowMultiple: false,
    );
    if (result != null) {
      File imageFile = File(result.files.single.path!);
      return imageFile;
    }
    return null;
  }

  Future<File?> getAPdfFile() async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowMultiple: false, allowedExtensions: ['pdf']);
    if (result != null) {
      File imageFile = File(result.files.single.path!);
      return imageFile;
    }
    return null;
  }

  void callPhone({phone}) async {
    await launch("tel:$phone");
  }

  void sendMail(mail) async {
    await launch("mailto:$mail");
  }

  void launchWhatsApp(phone) async {
    String message = 'Hello';
    print(phone);
    var _whatsAppUrl = "whatsapp://send?phone=$phone&text=$message";
    await launch(_whatsAppUrl);
  }

  String localizedRangeNumber(BuildContext context, String number) {
    String local = context.read<DeviceCubit>().state.model.locale.languageCode;
    String format = DateFormat("hh:mm", local).format(DateTime.parse("2022-11-15 $number"));
    return format;
  }

  Future<LocationData?> getCurrentLocation(BuildContext context, {bool? showLoader = true}) async {
    if (showLoader == true) getIt<LoadingHelper>().showLoadingDialog();
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _loc;
    _serviceEnabled = await location.serviceEnabled();
    if (_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        if (showLoader == true) getIt<LoadingHelper>().dismissDialog();
        return null;
      }
    } else {
      getIt<LoadingHelper>().dismissDialog();
      CustomToast.showSimpleToast(
        type: ToastType.info,
        msg: "Turn on your device location to continue",
        title: "Location Permission",
      );
      return null;
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        getIt<LoadingHelper>().dismissDialog();
        return null;
      }
    }
    _loc = await location.getLocation();
    getIt<LoadingHelper>().dismissDialog();
    return _loc;
  }

  void trackingBusinessOnMap(double lat, double lng, String business, String address) {
    BuildContext context = getIt<GlobalContext>().context();
    AutoRouter.of(context).push(
      TrackingMapRoute(
        address: address,
        businessName: business,
        lat: lat,
        lng: lng,
      ),
    );
  }

  void navigateToMapWithDirection(
      {required double lat,
      required double lng,
      required String title,
      required BuildContext context}) async {
    if (lat == "0") return;
    try {
      final coords = Coords(lat, lng);
      // final title = "Destination";
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                        onTap: () => map.showMarker(
                          coords: coords,
                          title: title,
                        ),
                        title: Text(map.mapName),
                        leading: SvgPicture.asset(
                          map.icon,
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    } catch (e) {
      CustomToast.showSimpleToast(msg: "$e", title: "Error");
    }
  }

  /// used to get the current store path
  Future<String> getFilePath() async {
    String _sdPath = "";
    // if (Platform.isIOS) {
    //   Directory tempDir = await getTemporaryDirectory();
    //   _sdPath = tempDir.path;
    // } else {
    //   _sdPath = "/storage/emulated/0/new_record_sound";
    // }
    Directory tempDir = await getTemporaryDirectory();
    _sdPath = tempDir.path;
    var d = Directory(_sdPath);
    if (!d.existsSync()) {
      d.createSync(recursive: true);
    }
    String storagePath = _sdPath + "/" + DateTime.now().toIso8601String() + ".m4a";
    return storagePath;
  }

  Future<void> cropImage(BuildContext context, File? image,
      {required void Function(CroppedFile? croppedFile) callback}) async {
    if (image != null) {
      final croppedFile = await ImageCropper().cropImage(
        sourcePath: image.path,
        compressFormat: ImageCompressFormat.jpg,
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.ratio4x3
        ],
        compressQuality: 100,
        uiSettings: [
          _androidCropperSitting(),
          _iosCropperSitting(),
        ],
      );
      callback.call(croppedFile);
    }
  }

  _iosCropperSitting() => IOSUiSettings(title: 'Cropper');

  _androidCropperSitting() => AndroidUiSettings(
      activeControlsWidgetColor: MyColors.primary,
      toolbarTitle: 'Cropper',
      toolbarColor: MyColors.primary,
      toolbarWidgetColor: Colors.white,
      initAspectRatio: CropAspectRatioPreset.original,
      lockAspectRatio: false);

  Future<String> getAddress(LatLng latLng, BuildContext context) async {
    var deviceModel = context.read<DeviceCubit>().state.model;
    final params = GeoCodeAddressEntity(
      lat: latLng.latitude,
      lng: latLng.longitude,
      language: deviceModel.locale.languageCode,
    );
    var address = await GetGeoCodeAddress().call(params);
    return "${address?.country ?? ""}  ${address?.city ?? ""}  ${address?.region ?? ""}  ${address?.getStreet() ?? ""}";
  }

  void showCountryDialog(BuildContext context, {required void Function(Country) onSelect}) async {
    showCountryPickerSheet(
      context: context,
      showPhoneCode: true,
      countryListTheme: CountryListThemeData(
        flagSize: 25,
        backgroundColor: Colors.white,
        textStyle: TextStyle(fontSize: 16, color: Colors.black),
        // bottomSheetHeight: MediaQuery.of(context).size.height * .8,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
        inputDecoration: InputDecoration(
          labelText: 'Search',
          labelStyle: TextStyle(color: MyColors.primary),
          hintText: 'Start typing to search',
          focusColor: MyColors.primary,
          prefixIconColor: MyColors.primary,
          iconColor: MyColors.primary,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(color: MyColors.primary),
          ),
          contentPadding: const EdgeInsets.symmetric(vertical: 0),
          prefixIcon: const Icon(Icons.search),
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: const Color(0xFF8C98A8).withOpacity(0.2),
            ),
          ),
        ),
      ),
      onSelect: (Country country) {
        onSelect(country);
      },
    );
  }

  String convertNumToAr({required BuildContext context, required String value, bool isDistance = false}){
    var distanceNum = double.tryParse(value)?.round();
    String text = distanceNum?.toString()??value;
    var local = context.read<DeviceCubit>().state.model.locale.languageCode;
    if (distanceNum != null) {
      if (distanceNum >= 1000) {
        text = NumberFormat.compact(locale: local).format(distanceNum);
      }
    }
    if (local == "ar") {
      const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
      const farsi = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
      for (int i = 0; i < english.length; i++) {
        text = text.replaceAll(english[i], farsi[i]);
      }
    }
    return text;
  }

  String convertDigitsToLatin(String s) {
    var sb = StringBuffer();
    for (int i = 0; i < s.length; i++) {
      switch (s[i]) {
        //Arabic digits
        case '\u0660':
          sb.write('0');
          break;
        case '\u0661':
          sb.write('1');
          break;
        case '\u0662':
          sb.write('2');
          break;
        case '\u0663':
          sb.write('3');
          break;
        case '\u0664':
          sb.write('4');
          break;
        case '\u0665':
          sb.write('5');
          break;
        case '\u0666':
          sb.write('6');
          break;
        case '\u0667':
          sb.write('7');
          break;
        case '\u0668':
          sb.write('8');
          break;
        case '\u0669':
          sb.write('9');
          break;
        default:
          sb.write(s[i]);
          break;
      }
    }
    return sb.toString();
  }
}
