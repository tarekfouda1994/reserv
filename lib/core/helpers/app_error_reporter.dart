import 'dart:async';
import 'dart:developer';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter_tdd/core/models/user_model/user_model.dart';

class AppErrorReporter {
  const AppErrorReporter._();

  static Future<void> init({
    required FutureOr<void> Function() appRunner,
  }) async {
    appRunner();
    await AppErrorReporter._disableDebugReporting();
  }

  static Future<void> recordError(exception,
      {String? reason, StackTrace? stackTrace, bool fatal = false}) async {
    try {
      await FirebaseCrashlytics.instance.recordError(exception, stackTrace,
          reason: 'ADM>> ${reason ?? 'a non-fatal error>>'}', fatal: fatal, printDetails: true);
    } catch (e) {
      log(e.toString());
    }
  }

  static Future<void> logMessage(String message) async {
    try {
      FirebaseCrashlytics.instance.log(message);
    } catch (e) {
      log(e.toString());
    }
  }

  static Future<void> _disableDebugReporting() async {
    if (kDebugMode) {
      // Force disable Crashlytics collection while doing every day development.
      // Temporarily toggle this to true if you want to test crash reporting in your app.
      await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    } else {
      // Handle Crashlytics enabled status when not in Debug,
      // e.g. allow your users to opt-in to crash reporting.
    }
  }

  static Future<void> setUserInfo(UserModel user) async {
    try {
      await FirebaseCrashlytics.instance.setUserIdentifier(user.userId);
      await FirebaseCrashlytics.instance.setCustomKey('displayName', user.firstName);
    } catch (_) {}
  }
}