import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/input_field_style/custom_input_text_style.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:month_year_picker/month_year_picker.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class AdaptivePicker {
  static datePicker(
      {required BuildContext context,
      required Function(DateTime? date) onConfirm,
      required String title,
      DateTime? initial,
      DateTime? minDate,
      DateTime? maxDate,
      bool? isDateTime = false}) async {
    if (Platform.isIOS) {
      _iosDatePicker(
        context,
        onConfirm,
        title,
        initial: initial,
        minDate: minDate,
        maxDate: maxDate,
        isDateTime: isDateTime,
      );
    } else {
      _androidDatePicker(
        context,
        onConfirm,
        initial: initial,
        minDate: minDate,
        maxDate: maxDate,
        isDateTime: isDateTime,
      );
    }
  }

  static _androidDatePicker(
      BuildContext context, Function(DateTime? date) onConfirm,
      {DateTime? initial,
      DateTime? minDate,
      DateTime? maxDate,
      bool? isDateTime = false}) {
    showRoundedDatePicker(
      context: context,
      initialDate: initial ?? DateTime.now(),
      firstDate: minDate ?? DateTime.now().add(Duration(days: -1)),
      lastDate: maxDate ?? DateTime(2050),
      borderRadius: 16,
      height: 300,
      styleDatePicker: MaterialRoundedDatePickerStyle(
        textStyleDayHeader: TextStyle(fontFamily: CustomFonts.primaryFont),
        textStyleMonthYearHeader: TextStyle(
          fontFamily: CustomFonts.primaryFont,
        ),
        textStyleButtonAction: TextStyle(fontFamily: CustomFonts.primaryFont),
        textStyleDayOnCalendar: TextStyle(
          fontFamily: CustomFonts.primaryFont,
        ),
        textStyleDayOnCalendarSelected: TextStyle(
          fontFamily: CustomFonts.primaryFont,
          color: MyColors.white,
        ),
        textStyleCurrentDayOnCalendar: TextStyle(
          fontFamily: CustomFonts.primaryFont,
        ),
        textStyleButtonNegative: TextStyle(fontFamily: CustomFonts.primaryFont),
        textStyleButtonPositive: TextStyle(fontFamily: CustomFonts.primaryFont),
        textStyleDayButton: TextStyle(
          fontFamily: CustomFonts.primaryFont,
          color: MyColors.white,
        ),
        textStyleDayOnCalendarDisabled: TextStyle(
          fontFamily: CustomFonts.primaryFont,
        ),
        textStyleYearButton: TextStyle(
          fontFamily: CustomFonts.primaryFont,
          color: MyColors.white,
        ),
      ),
      theme: ThemeData.light().copyWith(
        accentColor: MyColors.primary,
        primaryColor: MyColors.primary,
        backgroundColor: MyColors.white,
        buttonTheme: ButtonThemeData(
          textTheme: ButtonTextTheme.primary,
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            foregroundColor: MyColors.primary, // button text color
          ),
        ),
      ),
    ).then(onConfirm);
  }

  static _iosDatePicker(
      BuildContext context, Function(DateTime? date) onConfirm, String title,
      {DateTime? initial,
      DateTime? minDate,
      DateTime? maxDate,
      bool? isDateTime = false}) {
    _bottomSheet(
      context: context,
      child: cupertinoDatePicker(
        context,
        onConfirm,
        title,
        initial: initial,
        minDate: minDate,
        maxDate: maxDate,
        isDateTime: isDateTime,
      ),
    );
  }

  static Widget cupertinoDatePicker(
      BuildContext context, Function(DateTime? date) onConfirm, String title,
      {DateTime? initial,
      DateTime? minDate,
      DateTime? maxDate,
      bool? isDateTime = false}) {
    DateTime _date = DateTime.now();
    var deviceModel = context.read<DeviceCubit>().state.model;
    return Container(
      height: deviceModel.isTablet ? 300 : 260,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(
                  title: title,
                  size: deviceModel.isTablet ? 10.sp : 12.sp,
                  color: Colors.black45,
                ),
                ElevatedButton(
                  onPressed: () {
                    onConfirm(_date);
                    Navigator.of(context).pop();
                  },
                  child: MyText(
                    title: "Done",
                    size: deviceModel.isTablet ? 8.sp : 12.sp,
                    color: MyColors.white,
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: MyColors.primary,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    elevation: 0,
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: CupertinoDatePicker(
              initialDateTime: initial ?? DateTime.now(),
              onDateTimeChanged: (date) {
                _date = date;
              },
              maximumDate: maxDate ?? DateTime(2050),
              minimumDate: minDate ?? DateTime.now().add(Duration(days: -1)),
              mode: isDateTime!
                  ? CupertinoDatePickerMode.dateAndTime
                  : CupertinoDatePickerMode.date,
            ),
          ),
        ],
      ),
    );
  }

  static timePicker(
      {required BuildContext context,
      required String title,
      required Function(DateTime? date) onConfirm}) async {
    if (Platform.isIOS) {
      _iosTimePicker(context, title, onConfirm);
    } else {
      _androidTimePicker(context, onConfirm);
    }
  }

  static _androidTimePicker(
      BuildContext context, Function(DateTime date) onConfirm) {
    var now = DateTime.now();
    showRoundedTimePicker(
      context: context,
      theme: ThemeData(
        primaryColor: MyColors.primary,
        backgroundColor: Colors.white,
        buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
      ),
      initialTime: TimeOfDay.now(),
    ).then((time) => onConfirm(
        DateTime(now.year, now.month, now.day, time!.hour, time.minute)));
  }

  static _iosTimePicker(
      BuildContext context, String title, Function(DateTime? date) onConfirm) {
    _bottomSheet(
      context: context,
      child: cupertinoTimePicker(context, title, onConfirm),
    );
  }

  static Widget cupertinoTimePicker(
      BuildContext context, String title, Function(DateTime? date) onConfirm) {
    DateTime _date = DateTime.now();
    return Container(
      height: 260,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MyText(title: title, size: 16.sp, color: Colors.black45),
                SizedBox(
                  height: 35,
                  child: ElevatedButton(
                    onPressed: () {
                      onConfirm(_date);
                      Navigator.of(context).pop();
                    },
                    child: MyText(
                        title: "Done", size: 16.sp, color: MyColors.primary),
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                    ),
                  ),
                )
              ],
            ),
          ),
          Flexible(
              child: CupertinoDatePicker(
            onDateTimeChanged: (date) {
              _date = date;
            },
            mode: CupertinoDatePickerMode.time,
          )),
        ],
      ),
    );
  }

  static _bottomSheet({required BuildContext context, required Widget child}) {
    var model = context.read<DeviceCubit>().state.model;
    return showModalBottomSheet(
      isScrollControlled: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(13),
          topRight: Radius.circular(13),
        ),
      ),
      backgroundColor: Colors.white,
      context: context,
      builder: (context) => Container(
        height: model.isTablet ? 400 : 320,
        child: child,
      ),
    );
  }

  static Future<DateTime?> expiryDatePicker(
      BuildContext context, Locale locale) async {
    // var locale = context.read<DeviceModel>().locale;
    final date = await showMonthYearPicker(
      context: context,
      locale: locale,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year - 6, 5),
      lastDate: DateTime(DateTime.now().year + 1, 9),
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: MyColors.primary,
              onPrimary: MyColors.white,
              onSurface: MyColors.black,
              secondary: MyColors.primary,
              onSecondary: MyColors.white,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                foregroundColor: Colors.blue,
                textStyle: CustomInputTextStyle(
                    lang: locale.languageCode, isTablet: false),
              ),
            ),
          ),
          child: child!,
        );
      },
    );
    return date;
  }
}
