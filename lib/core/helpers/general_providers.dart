import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tdd/features/auth/presentation/manager/user_cubit/user_cubit.dart';
import 'package:flutter_tdd/features/reservation/presentation/manager/reservation_details_cubit/reservation_details_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/manager/date_time_cubit/date_time_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/manager/enter_location_dialog_cubit/location_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/manager/filter_count_cubit/filter_count_cubit.dart';
import 'package:flutter_tdd/features/search/presentation/manager/location_cubit/location_cubit.dart';

import '../../features/general/presentation/pages/location_address/location_cubit/location_cubit.dart';
import '../bloc/search_cubit/search_cubit.dart';

class GeneralProviders {
  GeneralProviders._();

  static GeneralProviders instance = GeneralProviders._();

  List<BlocProvider> providers(BuildContext context) => [
        BlocProvider<SearchCubit>(
          create: (BuildContext context) => SearchCubit(),
        ),
        BlocProvider<UserCubit>(
          create: (BuildContext context) => UserCubit(),
        ),
        BlocProvider<DateTimeCubit>(
          create: (BuildContext context) => DateTimeCubit(),
        ),
        BlocProvider<HomeLocationCubit>(
          create: (BuildContext context) => HomeLocationCubit(),
        ),
        BlocProvider<LocationCubit>(
          create: (BuildContext context) => LocationCubit(),
        ),
        BlocProvider<ReservationDetailsCubit>(
          create: (BuildContext context) => ReservationDetailsCubit(),
        ),
        BlocProvider<SelectLocDialogCubit>(
          create: (BuildContext context) => SelectLocDialogCubit(),
        ),
        BlocProvider<FilterCountCubit>(
          create: (BuildContext context) => FilterCountCubit(),
        ),
      ];
}
