enum HomeDepartments {
  trending,
  recommended,
  favorite,
  recently,
  offers,
  onSearch,
  popular
}

enum HomeApisType { recommended, recently, trending, offers, popular,onSearch}

enum RatingEnum { create, update }
