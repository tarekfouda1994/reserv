import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:google_fonts/google_fonts.dart';

class AppThem {
  AppThem._();

  static AppThem instance = AppThem._();

  ThemeData  themeData({String? lang}) => ThemeData(
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: MyColors.primary,
          selectionColor: MyColors.primary.withOpacity(.4),
          selectionHandleColor: MyColors.primary,
        ),
        colorScheme: ColorScheme.fromSwatch(
          accentColor: MyColors.primary,
        ),
        focusColor: MyColors.primary,
        primaryColor: MyColors.primary,
        fontFamily: lang == "en" ? "workSans" : "readexProMedium",
        scaffoldBackgroundColor: MyColors.white,
        appBarTheme: const AppBarTheme(
            iconTheme: IconThemeData(color: Colors.white),
            systemOverlayStyle: SystemUiOverlayStyle(
                statusBarColor: MyColors.primary,
                statusBarBrightness: Brightness.light)),
        textTheme: TextTheme(
          subtitle1: GoogleFonts.cairo(
            fontSize: 14,
          ),
        ),
      );
}
