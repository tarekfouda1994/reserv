import 'firebase_error.dart';

class OperationNotAllowedError extends FirebaseError {
  OperationNotAllowedError() : super('Operation not allowed');
}
