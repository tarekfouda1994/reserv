import 'package:flutter_tdd/core/errors/firebase/firebase_error.dart';

class UnExpectedFirebaseError extends FirebaseError {
  UnExpectedFirebaseError() : super("error_unknown_happened");
}
