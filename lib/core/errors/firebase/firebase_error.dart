import 'package:equatable/equatable.dart';

import '../base_error.dart';

class FirebaseError extends BaseError with EquatableMixin {
  final String message;

  FirebaseError(this.message) : super();

  @override
  List<Object?> get props => [message];
}
