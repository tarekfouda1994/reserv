// ignore_for_file: constant_identifier_names

const ERROR_INVALID_EMAIL = 'invalid-email';

const ERROR_WRONG_PASSWORD = 'wrong-password';

const ERROR_USER_NOT_FOUND = 'user-not-found';

const ERROR_USER_DISABLED = 'ERROR_USER_DISABLED';

const ERROR_TOO_MANY_REQUESTS = 'ERROR_TOO_MANY_REQUESTS';

const ERROR_OPERATION_NOT_ALLOWED = 'ERROR_OPERATION_NOT_ALLOWED';

const ERROR_NETWORK_REQUEST_FAILED = 'ERROR_NETWORK_REQUEST_FAILED';

const ERROR_EMAIL_ALREADY_IN_USE = 'email-already-in-use';
