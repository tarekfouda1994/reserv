import 'firebase_error.dart';

class UserDisabledError extends FirebaseError {
  UserDisabledError() : super('User Disabled Error');
}
