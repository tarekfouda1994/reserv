// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'result.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MyResult<T> {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T? data) isSuccess,
    required TResult Function(BaseError error) isError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T? data)? isSuccess,
    TResult Function(BaseError error)? isError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T? data)? isSuccess,
    TResult Function(BaseError error)? isError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(IsSuccess<T> value) isSuccess,
    required TResult Function(IsError<T> value) isError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(IsSuccess<T> value)? isSuccess,
    TResult Function(IsError<T> value)? isError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(IsSuccess<T> value)? isSuccess,
    TResult Function(IsError<T> value)? isError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MyResultCopyWith<T, $Res> {
  factory $MyResultCopyWith(
          MyResult<T> value, $Res Function(MyResult<T>) then) =
      _$MyResultCopyWithImpl<T, $Res>;
}

/// @nodoc
class _$MyResultCopyWithImpl<T, $Res> implements $MyResultCopyWith<T, $Res> {
  _$MyResultCopyWithImpl(this._value, this._then);

  final MyResult<T> _value;
  // ignore: unused_field
  final $Res Function(MyResult<T>) _then;
}

/// @nodoc
abstract class _$$IsSuccessCopyWith<T, $Res> {
  factory _$$IsSuccessCopyWith(
          _$IsSuccess<T> value, $Res Function(_$IsSuccess<T>) then) =
      __$$IsSuccessCopyWithImpl<T, $Res>;
  $Res call({T? data});
}

/// @nodoc
class __$$IsSuccessCopyWithImpl<T, $Res> extends _$MyResultCopyWithImpl<T, $Res>
    implements _$$IsSuccessCopyWith<T, $Res> {
  __$$IsSuccessCopyWithImpl(
      _$IsSuccess<T> _value, $Res Function(_$IsSuccess<T>) _then)
      : super(_value, (v) => _then(v as _$IsSuccess<T>));

  @override
  _$IsSuccess<T> get _value => super._value as _$IsSuccess<T>;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$IsSuccess<T>(
      data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as T?,
    ));
  }
}

/// @nodoc

class _$IsSuccess<T> implements IsSuccess<T> {
  const _$IsSuccess([this.data]);

  @override
  final T? data;

  @override
  String toString() {
    return 'MyResult<$T>.isSuccess(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IsSuccess<T> &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  _$$IsSuccessCopyWith<T, _$IsSuccess<T>> get copyWith =>
      __$$IsSuccessCopyWithImpl<T, _$IsSuccess<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T? data) isSuccess,
    required TResult Function(BaseError error) isError,
  }) {
    return isSuccess(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T? data)? isSuccess,
    TResult Function(BaseError error)? isError,
  }) {
    return isSuccess?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T? data)? isSuccess,
    TResult Function(BaseError error)? isError,
    required TResult orElse(),
  }) {
    if (isSuccess != null) {
      return isSuccess(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(IsSuccess<T> value) isSuccess,
    required TResult Function(IsError<T> value) isError,
  }) {
    return isSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(IsSuccess<T> value)? isSuccess,
    TResult Function(IsError<T> value)? isError,
  }) {
    return isSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(IsSuccess<T> value)? isSuccess,
    TResult Function(IsError<T> value)? isError,
    required TResult orElse(),
  }) {
    if (isSuccess != null) {
      return isSuccess(this);
    }
    return orElse();
  }
}

abstract class IsSuccess<T> implements MyResult<T> {
  const factory IsSuccess([final T? data]) = _$IsSuccess<T>;

  T? get data;
  @JsonKey(ignore: true)
  _$$IsSuccessCopyWith<T, _$IsSuccess<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$IsErrorCopyWith<T, $Res> {
  factory _$$IsErrorCopyWith(
          _$IsError<T> value, $Res Function(_$IsError<T>) then) =
      __$$IsErrorCopyWithImpl<T, $Res>;
  $Res call({BaseError error});
}

/// @nodoc
class __$$IsErrorCopyWithImpl<T, $Res> extends _$MyResultCopyWithImpl<T, $Res>
    implements _$$IsErrorCopyWith<T, $Res> {
  __$$IsErrorCopyWithImpl(
      _$IsError<T> _value, $Res Function(_$IsError<T>) _then)
      : super(_value, (v) => _then(v as _$IsError<T>));

  @override
  _$IsError<T> get _value => super._value as _$IsError<T>;

  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$IsError<T>(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as BaseError,
    ));
  }
}

/// @nodoc

class _$IsError<T> implements IsError<T> {
  const _$IsError(this.error);

  @override
  final BaseError error;

  @override
  String toString() {
    return 'MyResult<$T>.isError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IsError<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  _$$IsErrorCopyWith<T, _$IsError<T>> get copyWith =>
      __$$IsErrorCopyWithImpl<T, _$IsError<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T? data) isSuccess,
    required TResult Function(BaseError error) isError,
  }) {
    return isError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(T? data)? isSuccess,
    TResult Function(BaseError error)? isError,
  }) {
    return isError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T? data)? isSuccess,
    TResult Function(BaseError error)? isError,
    required TResult orElse(),
  }) {
    if (isError != null) {
      return isError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(IsSuccess<T> value) isSuccess,
    required TResult Function(IsError<T> value) isError,
  }) {
    return isError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(IsSuccess<T> value)? isSuccess,
    TResult Function(IsError<T> value)? isError,
  }) {
    return isError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(IsSuccess<T> value)? isSuccess,
    TResult Function(IsError<T> value)? isError,
    required TResult orElse(),
  }) {
    if (isError != null) {
      return isError(this);
    }
    return orElse();
  }
}

abstract class IsError<T> implements MyResult<T> {
  const factory IsError(final BaseError error) = _$IsError<T>;

  BaseError get error;
  @JsonKey(ignore: true)
  _$$IsErrorCopyWith<T, _$IsError<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
