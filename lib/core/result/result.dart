import 'package:flutter_tdd/core/errors/base_error.dart';
import 'package:flutter_tdd/core/errors/unknown_error.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'result.freezed.dart';

@freezed
class MyResult<T> with _$MyResult<T> {
  const factory MyResult.isSuccess([T? data]) = IsSuccess<T>;

  const factory MyResult.isError(BaseError error) = IsError;

  factory MyResult.unknownError() {
    return const MyResult.isError(UnknownError());
  }
}
