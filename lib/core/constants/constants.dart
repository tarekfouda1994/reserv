import 'package:flutter/cupertino.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/helpers/utilities.dart';

class CustomFonts {
  final BuildContext context = getIt<GlobalContext>().context();
  static String primaryFont =
      getIt<Utilities>().getLocalizedValue("readexProMedium", "workSans");
  static String primarySemiBoldFont =
      getIt<Utilities>().getLocalizedValue("readexProMedium", "workSans");

  static String arabicFont = "readexProMedium";

  // static String secondaryFont = "PensumDisplay";
  // static String primaryRegularFont = getIt<Utilities>()
  //     .getLocalizedValue("readexProMedium", "workSans");
  // static String primaryBoldFont = getIt<Utilities>()
  //     .getLocalizedValue("readexProMedium", "workSans");

  // static String googleMapKey = "AIzaSyCs4U9JRZUbfjNvvmfDcKzSoRWS3x8SEUA";
  static String googleMapKey = "AIzaSyCGBF4VMQp8-gP2OcfnVylWA9w8sWIwcLs";
}
