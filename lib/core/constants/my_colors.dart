import 'package:flutter/material.dart';

class MyColors {
  static const Color primary = Color(0xFF005858);
  static const Color primaryOpacity = Color(0xFFF6F9F9);
  static const Color primaryDark = Color(0xFF0B6464);
  static const Color primaryLight = Color(0xFFD4F1F1);
  static Color primaryLightBg = Color(0xFFF6F9F9);
  static const Color secondary = Color(0xffE5B998);
  static const Color favouriteBg = Color(0xffC4C4C4);
  static const Color activeFavouriteBg = Color(0xffFF1010);
  static const Color bgRed = Color(0xffFFF3F9);
  static const Color borderCard = Color(0xffEAEAEA);
  static const Color offWhite = Color(0xffF2F2F2);
  static const Color gold = Color(0xffe4aa69);
  static const Color blueFaceBook = Color(0xff4267B2);
  static const Color blue = Color(0xff4278F6);
  static const Color amber = Color(0xffF1C800);
  static const Color gradientProfile = Color(0xffD4F1F1);
  static const Color darkGrey = Color(0xff797a7a);
  static const Color grey = Colors.grey;
  static Color greyLight = Color(0xffD7D7D7);
  static const Color greyBold = Color(0xffEEEEEE);
  static Color greyWhite = Colors.grey.withOpacity(.2);

  static Color indicatorBackgroundColor = Color(0xffF2F2F0);
  static const Color black = Color(0xff031626);
  static const Color blackOpacity = Colors.black54;
  static const Color white = Colors.white;
  static const Color successColor = Color(0xff03A89E);
  static const Color errorColor = Color(0xfff83245);
  static const Color redColor = Color(0xffED3A57);
  static const Color infoColor = Color(0xffFFBB00);
  static const Color headerBackGroundColor = Color(0xffF6F9F9);
  static const Color defaultImgBg = Color(0xffF2F6F6);
  static const Color reserveTogetherBg = Color(0xffD6ECEC);
  static const Color disableTogetherBg = Color(0xffE6E7E7);
  static const Color reserveDateBg = Color(0xffF3F3F3);
  static const Color fillColor = Color(0xffF6F9F9);
  static const Color fillErrorColor = Color(0xffFFF8F9);
}

