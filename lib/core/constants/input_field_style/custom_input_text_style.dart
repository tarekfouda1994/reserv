import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/constants.dart';

import '../my_colors.dart';

class CustomInputTextStyle extends TextStyle {
  final String lang;
  final Color? textColor;
  final bool isTablet;

  const CustomInputTextStyle({required this.lang, required this.isTablet, this.textColor});

  @override
  String get fontFamily => CustomFonts.primaryFont;

  @override
  // TODO: implement fontSize
  double get fontSize => isTablet ? 8.sp : 14.sp;

  @override
  // TODO: implement color
  Color get color => textColor ?? MyColors.black;
}
