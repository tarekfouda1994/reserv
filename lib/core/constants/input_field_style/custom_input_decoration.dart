import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';

class CustomInputDecoration extends InputDecoration {
  final String lang;
  final String? labelTxt;
  final String? hint;
  final Widget? prefIcon;
  final Widget? sufIcon;
  final Color? enableColor;
  final Color? focsColor;
  final Color? hintColor;
  final Color? customFillColor;
  final BorderRadius? borderRadius;
  final EdgeInsets? padding;
  final bool isTablet;
  final bool isCustomField;
  final double? hintTextStyle;
  const CustomInputDecoration(
      {required this.lang,
      required this.isTablet,
      this.isCustomField = false,
      this.hint,
      this.labelTxt,
      this.prefIcon,
      this.sufIcon,
      this.hintColor,
      this.customFillColor,
      this.enableColor,
      this.focsColor,
      this.borderRadius,
      this.hintTextStyle,
      this.padding})
      : super();

  @override
  InputBorder get enabledBorder => isCustomField? InputBorder.none : OutlineInputBorder(
        borderSide: BorderSide(color: enableColor ?? MyColors.greyWhite, width: .7),
        borderRadius: borderRadius ?? BorderRadius.circular(10),
      );

  @override
  InputBorder get focusedBorder => isCustomField? InputBorder.none :  OutlineInputBorder(
      borderRadius: borderRadius ?? BorderRadius.circular(10),
      borderSide: BorderSide(color: focusColor ?? MyColors.primary, width: 1));

  @override
  InputBorder get errorBorder => isCustomField? InputBorder.none :  OutlineInputBorder(
      borderSide: const BorderSide(color: Color(0xffED3A57), width: .5),
      borderRadius: borderRadius ?? BorderRadius.circular(10));

  @override
  InputBorder get focusedErrorBorder => isCustomField? InputBorder.none :  OutlineInputBorder(
      borderRadius: borderRadius ?? BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xffED3A57), width: 2));

  @override
  TextStyle get errorStyle =>
      TextStyle(fontSize: isTablet? 8.sp : 10.sp, fontFamily: CustomFonts.primaryFont, color: Color(0xffED3A57));

  @override
  String? get hintText => hint;

  @override
  Widget? get label => labelTxt == null
      ? super.label
      : Text(
          labelTxt ?? "",
        );

  @override
  TextStyle get labelStyle =>
      TextStyle(fontSize: isTablet ? 8.sp : 11.sp, color: hintColor ?? Colors.black54,fontFamily: CustomFonts.primaryFont);

  @override
  TextStyle? get floatingLabelStyle =>
      TextStyle(fontSize: isTablet ? 8.sp : 14.sp, color: hintColor ?? Colors.black54,fontFamily: CustomFonts.primaryFont);

  @override
  TextStyle? get hintStyle =>
      TextStyle(fontSize: hintTextStyle??(isTablet ? 8.sp : 11.sp), color: hintColor ?? Colors.black54,fontFamily: CustomFonts.primaryFont);

  @override
  EdgeInsetsGeometry get contentPadding => padding ?? const EdgeInsets.symmetric(horizontal: 10);

  @override
  bool get filled => true;

  @override
  Color get fillColor => customFillColor ?? Colors.transparent;

  @override
  bool get alignLabelWithHint => true;

  @override
  Widget? get suffixIcon => sufIcon ?? Icon(Icons.close, color: Colors.transparent);

  @override
  BoxConstraints? get suffixIconConstraints => sufIcon != null? super.suffixIconConstraints : BoxConstraints(
    minHeight: 60,
    maxWidth: 0,
  );

  @override
  Widget? get prefixIcon => prefIcon;

  @override
  Color? get focusColor => focsColor;
}
