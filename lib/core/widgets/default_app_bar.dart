import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final Widget? leading;
  final Widget? titleWidget;
  final PreferredSize? bottom;
  final List<Widget> actions;
  final double? size;
  final double? sizeFromHeight;
  final double? elevation;
  final double? titleSize;
  final bool? showBack;
  final bool? centerTitle;
  final Color? colorBackgroundBack;
  final Color? backgroundBack;
  final Function()? onBack;

  const DefaultAppBar(
      {Key? key,
      this.title,
      this.actions = const [],
      this.leading,
      this.size,
      this.onBack,
      this.showBack = true,
      this.centerTitle,
      this.titleWidget,
      this.titleSize,
      this.bottom,
      this.sizeFromHeight,
      this.backgroundBack,
      this.colorBackgroundBack,
      this.elevation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = context.watch<DeviceCubit>().state.model;
    return AppBar(
      title: titleWidget ??
          MyText(
            title: title ?? "",
            size: titleSize ?? (device.isTablet ? 10.sp : 13.sp),
            color: MyColors.black,
            fontWeight: FontWeight.bold,
          ),
      centerTitle: centerTitle ?? false,
      titleSpacing: 10,
      systemOverlayStyle:
          const SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
      backgroundColor: backgroundBack ?? Colors.white,
      elevation: elevation ?? 0,
      leadingWidth: showBack == true ? 55 : 10,
      leading: Padding(
        padding: EdgeInsets.only(top: device.isTablet ? 6 : 0),
        child: leading ??
            Visibility(
              visible: showBack ?? true,
              child: Container(
                margin: EdgeInsetsDirectional.only(
                  start: device.isTablet ? 3.r : 15.r,
                ),
                decoration: BoxDecoration(
                    color: colorBackgroundBack ??
                        MyColors.primary.withOpacity(.35),
                    shape: BoxShape.circle),
                child: IconButton(
                    icon: Icon(
                      device.locale == Locale('en', 'US')
                          ? Icons.west
                          : Icons.east,
                      color: MyColors.white,
                      size: device.isTablet ? 10.sp : 18.sp,
                    ),
                    onPressed: onBack ?? () => Navigator.of(context).pop()),
              ),
            ),
      ),
      actions: actions,
      bottom: bottom,
    );
  }

  @override
  Size get preferredSize {
    final cxt = getIt<GlobalContext>().context();
    final isTablet = cxt.read<DeviceCubit>().state.model.isTablet;
    return Size.fromHeight(size ?? (isTablet ? 40.h : 50.h));
  }
}
