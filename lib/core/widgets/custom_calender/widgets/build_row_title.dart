part of 'custom_calender_widgets_imports.dart';

class BuildRowTitle extends StatelessWidget {
  final DeviceModel model;

  const BuildRowTitle({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsetsDirectional.only(start: 16),
            child: SvgPicture.asset(Res.calendar_lines,
                color: MyColors.grey.withOpacity(.5), height: 15.h, width: 15.w),
          ),
          MyText(
            title: "  ${tr("select_date")}",
            color: MyColors.black,
            size: model.isTablet ? 5.5.sp : 9.sp,
            fontWeight: FontWeight.bold,
          ),
        ],
      ),
    );
  }
}
