part of 'custom_calender_widgets_imports.dart';

class BuildDayView extends StatelessWidget {
  final CustomCalenderData calenderData;
  final List<DateTime> selectedDates;
  final Function(bool, DateTime) onSelect;
  final List<DateTime> disableDates;
  final bool showIcons;

  final bool isLoading;
  final ScrollController? controller;

  const BuildDayView({
    Key? key,
    required this.calenderData,
    required this.selectedDates,
    required this.disableDates,
    required this.onSelect,
    required this.isLoading,
    this.showIcons = false,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    bool isEn = device.locale == Locale('en', 'US');
    return BlocBuilder<GenericBloc<List<CalenderDataEntity>>,
        GenericState<List<CalenderDataEntity>>>(
      bloc: calenderData.datesCubit,
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6).r,
          child: CustomScrollView(
            controller: controller,
            scrollDirection: Axis.horizontal,
            slivers: List.generate(state.data.length, (position) {
              return SliverStickyHeader(
                overlapsContent: true,
                header: InkWell(
                  onTap: () {
                    final e = state.data[position].dates.first;
                    bool disable = _checkDisabled(e);
                    onSelect(disable, e);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MyText(
                        title: state.data[position].year,
                        color: MyColors.black,
                        size: device.isTablet ? (7.2).sp : 11.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      MyText(
                        title: state.data[position].month,
                        color: MyColors.black,
                        size: device.isTablet ? (7.2).sp : 11.sp,
                      ),
                    ],
                  ),
                ),
                sliver: SliverPadding(
                  padding: EdgeInsets.only(top: 35).r,
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      final e = state.data[position].dates[index];
                      bool selected = _checkSelected(e);
                      bool disable = _checkDisabled(e);
                      return InkWell(
                        onTap: () => onSelect(disable, e),
                        // borderRadius: BorderRadius.circular(100),
                        child: Container(
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsetsDirectional.only(end: 8.sp),
                          height: device.isTablet ? 45.h : 50.h,
                          width: device.isTablet ? (isEn ? 30.w : 38.w) : (isEn ? 38.w : 55.w),
                          decoration: BoxDecoration(
                            color: selected ? MyColors.primary : MyColors.white,
                            borderRadius: BorderRadius.circular(40),
                            border: Border.all(color: MyColors.greyWhite),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              MyText(
                                title: DateFormat("dd", calenderData.local(context)).format(e),
                                color: selected ? MyColors.white : MyColors.black,
                                size: device.isTablet ? 8.sp : 10.sp,
                              ),
                              MyText(
                                title: DateFormat("EE", calenderData.local(context)).format(e),
                                color: selected ? MyColors.white : MyColors.blackOpacity,
                                size: device.isTablet ? 7.sp : 9.sp,
                              ),
                                Visibility(
                                  visible: isLoading,
                                  child: BuildShimmerView(
                                    height: 20,
                                    width: 20,
                                    boxShape: BoxShape.circle,
                                  ),
                                  replacement: Offstage(
                                    offstage: !showIcons,
                                    child: SvgPicture.asset(disable ? Res.unavailable : Res.available),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      );
                    }, childCount: state.data[position].dates.length),
                  ),
                ),
              );
            }),
          ),
        );
      },
    );
  }

  bool _checkDisabled(DateTime e) {
    final disable = disableDates
        .any((date) => date.day == e.day && date.month == e.month && date.year == e.year);
    return disable;
  }

  bool _checkSelected(DateTime e) {
    final selected = selectedDates
        .any((date) => date.day == e.day && date.month == e.month && date.year == e.year);
    return selected;
  }

//   void _onSelectDate(bool disable, DateTime e) {
//     if (disable) {
//       CustomToast.showSnakeBar("This day is not available", tr("Warning_Toast"),
//           type: ToastType.info);
//     } else {
//       onSelect(disable, e);
//     }
//   }
}
