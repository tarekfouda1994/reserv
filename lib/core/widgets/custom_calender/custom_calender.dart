part of 'custom_calender_imports.dart';

class CustomCalender extends StatefulWidget {
  final List<DateTime> selectedDates;
  final List<DateTime> disableDates;
  final Function(bool ,DateTime) onSelect;
  final bool showIcons;
  final bool showRowTitle;

  final bool isLoading;
  final ScrollController? controller;

  const CustomCalender({
    Key? key,
    required this.selectedDates,
    required this.onSelect,
    required this.disableDates,
    this.showIcons = false,
    this.isLoading = false,
    this.showRowTitle = true, this.controller,
  }) : super(key: key);

  @override
  _CustomCalenderState createState() => _CustomCalenderState();
}

class _CustomCalenderState extends State<CustomCalender> {
  final CustomCalenderData calenderData = CustomCalenderData();

  @override
  void initState() {
    calenderData.initDates(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Container(
      height: widget.showIcons? 105.h : 100.h,
      child: Row(
        children: [
          Visibility(
            visible: widget.showRowTitle == true,
            child: BuildRowTitle(model: device),
          ),
          Flexible(
            child: BuildDayView(
              controller: widget.controller,
              calenderData: calenderData,
              selectedDates: widget.selectedDates,
              disableDates: widget.disableDates,
              onSelect: widget.onSelect,
              showIcons: widget.showIcons,
              isLoading: widget.isLoading,
            ),
          ),
        ],
      ),
    );
  }
}
