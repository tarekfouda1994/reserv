part of 'custom_calender_imports.dart';

class CustomCalenderData {
  GenericBloc<List<CalenderDataEntity>> datesCubit = GenericBloc([]);

  initDates(BuildContext context) {
    final start = DateTime.now();
    List<String> months = [];
    var days = List.generate(60, (i) {
      var datetime = DateTime(start.year, start.month, start.day + (i));
      months.add(DateFormat("MMM", local(context)).format(datetime));
      return datetime;
    });
    months = months.toSet().toList();
    for (var element in months) {
      var dates = days
          .where((e) => DateFormat("MMM", local(context)).format(e) == element)
          .toList();
      var item = CalenderDataEntity(
          month: element,
          dates: dates,
          year: DateFormat("yyyy", local(context)).format(dates.first));
      datesCubit.state.data.add(item);
    }
    datesCubit.onUpdateData(datesCubit.state.data);
  }

  String local(BuildContext context) {
    Locale locale = context.read<DeviceCubit>().state.model.locale;
    if (locale == Locale('en', 'US')) {
      return "en_US";
    } else {
      return "ar_SA";
    }
  }
}

class CalenderDataEntity {
  String month;
  String year;
  List<DateTime> dates;

  CalenderDataEntity(
      {required this.month, required this.dates, required this.year});
}
