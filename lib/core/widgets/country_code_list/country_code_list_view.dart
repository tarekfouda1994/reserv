import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_header_sheet.dart';
import 'package:flutter_tdd/core/widgets/build_my_arrow_icon.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import 'contry_list_codes.dart';

class CountryListView extends StatefulWidget {
  /// Called when a country is select.
  ///
  /// The country picker passes the new value to the callback.
  final ValueChanged<Country> onSelect;

  /// An optional [showPhoneCode] argument can be used to show phone code.
  final bool showPhoneCode;

  /// An optional [exclude] argument can be used to exclude(remove) one ore more
  /// country from the countries list. It takes a list of country code(iso2).
  /// Note: Can't provide both [exclude] and [countryFilter]
  final List<String>? exclude;

  /// An optional [countryFilter] argument can be used to filter the
  /// list of countries. It takes a list of country code(iso2).
  /// Note: Can't provide both [countryFilter] and [exclude]
  final List<String>? countryFilter;

  /// An optional [favorite] argument can be used to show countries
  /// at the top of the list. It takes a list of country code(iso2).
  final List<String>? favorite;

  /// An optional argument for customizing the
  /// country list bottom sheet.
  final CountryListThemeData? countryListTheme;

  /// An optional argument for initially expanding virtual keyboard
  final bool searchAutofocus;

  /// An optional argument for showing "World Wide" option at the beginning of the list
  final bool showWorldWide;

  /// An optional argument for hiding the search bar
  final bool showSearch;

  const CountryListView({
    Key? key,
    required this.onSelect,
    this.exclude,
    this.favorite,
    this.countryFilter,
    this.showPhoneCode = false,
    this.countryListTheme,
    this.searchAutofocus = false,
    this.showWorldWide = false,
    this.showSearch = true,
  })  : assert(
          exclude == null || countryFilter == null,
          'Cannot provide both exclude and countryFilter',
        ),
        super(key: key);

  @override
  State<CountryListView> createState() => _CountryListViewState();
}

class _CountryListViewState extends State<CountryListView> {
  final CountryService _countryService = CountryService();

  late List<Country> _countryList;
  late List<Country> _filteredList;
  late TextEditingController _searchController;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();

    _countryList = _countryService.getAll();

    _countryList =
        countryCodes.map((country) => Country.from(json: country)).toList();

    //Remove duplicates country if not use phone code
    if (!widget.showPhoneCode) {
      final ids = _countryList.map((e) => e.countryCode).toSet();
      _countryList.retainWhere((country) => ids.remove(country.countryCode));
    }

    if (widget.exclude != null) {
      _countryList.removeWhere(
        (element) => widget.exclude!.contains(element.countryCode),
      );
    }

    if (widget.countryFilter != null) {
      _countryList.removeWhere(
        (element) => !widget.countryFilter!.contains(element.countryCode),
      );
    }

    _filteredList = <Country>[];
    if (widget.showWorldWide) {
      _filteredList.add(Country.worldWide);
    }
    _filteredList.addAll(_countryList);
  }

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return Column(
      children: <Widget>[
        BuildHeaderSheet(title: tr("select_country")),
        SizedBox(height: 10),
        Expanded(
          child: Visibility(
            visible: _filteredList.isNotEmpty,
            replacement: Center(
              child: MyText(
                title: tr("noResultsFound"),
                size: 14,
                color: MyColors.black,
              ),
            ),
            child: ListView(
              children: _filteredList
                  .map<Widget>((country) => _listRow(country))
                  .toList(),
            ),
          ),
        ),
        if (widget.showSearch)
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 10, 24, 20),
            child: CustomTextField(
              fieldTypes: FieldTypes.normal,
              controller: _searchController,
              type: TextInputType.text,
              action: TextInputAction.done,
              label: tr("country"),
              validate: (value) => value!.noValidate(),
              hint: tr("select_country_hint"),
              prefixIcon: Padding(
                padding: const EdgeInsets.all(13),
                child: SvgPicture.asset(
                  Res.Search,
                  width: deviceModel.isTablet ? 10.w : null,
                ),
              ),
              suffixIcon: Visibility(
                visible: _searchController.text.isNotEmpty,
                child: InkWell(
                  child: Icon(Icons.clear, color: MyColors.primary),
                  onTap: () => setState(() {
                    _searchController.clear();
                    _filteredList.clear();
                    _filteredList.addAll(_countryList);
                  }),
                ),
              ),
              onChange: _filterSearchResults,
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
            ),
          ),
      ],
    );
  }

  Widget _listRow(Country country) {
    final bool isRtl = Directionality.of(context) == TextDirection.rtl;

    return Material(
      // Add Material Widget with transparent color
      // so the ripple effect of InkWell will show on tap
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          country.nameLocalized = CountryLocalizations.of(context)
              ?.countryName(countryCode: country.countryCode)
              ?.replaceAll(RegExp(r"\s+"), " ");
          widget.onSelect(country);
          Navigator.pop(context);
        },
        child: Container(
          height: 48,
          margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 20),
          padding: const EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            border: Border.all(
              color: MyColors.reserveDateBg,
              width: 1,
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    _flagWidget(country),
                    SizedBox(width: 6),
                    Flexible(
                        child: MyText(
                      title: CountryLocalizations.of(context)
                              ?.countryName(countryCode: country.countryCode)
                              ?.replaceAll(RegExp(r"\s+"), " ") ??
                          country.name,
                      color: MyColors.blackOpacity.withOpacity(.45),
                      size: 10,
                      overflow: TextOverflow.ellipsis,
                    )),
                    if (widget.showPhoneCode && !country.iswWorldWide) ...[
                      const SizedBox(width: 6),
                      SizedBox(
                        width: 45,
                        child: MyText(
                          title:
                              '(${isRtl ? '' : '+'}${country.phoneCode}${isRtl ? '+' : ''})',
                          color: Color(0xff787a7a),
                          size: 10.2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ] else
                      const SizedBox(width: 15),
                  ],
                ),
              ),
              BuildMyArrowIcon(color: Color(0xffD9D9D9)),
            ],
          ),
        ),
      ),
    );
  }

  Widget _flagWidget(Country country) {
    final bool isRtl = Directionality.of(context) == TextDirection.rtl;
    return SizedBox(
      // the conditional 50 prevents irregularities caused by the flags in RTL mode
      width: isRtl ? 50 : null,
      child: Text(
        country.iswWorldWide ? '\uD83C\uDF0D' : getFlagImg(country.countryCode),
        style: TextStyle(
          fontSize: widget.countryListTheme?.flagSize ?? 22,
        ),
      ),
    );
  }

  String getFlagImg(String countryCode) {
    String flag = countryCode.toUpperCase().replaceAllMapped(RegExp(r'[A-Z]'),
        (match) => String.fromCharCode(match.group(0)!.codeUnitAt(0) + 127397));
    return flag;
  }

  void _filterSearchResults(String query) {
    List<Country> _searchResult = <Country>[];
    final CountryLocalizations? localizations =
        CountryLocalizations.of(context);

    if (query.isEmpty) {
      _searchResult.addAll(_countryList);
    } else {
      _searchResult = _countryList
          .where((c) => c.startsWith(query, localizations))
          .toList();
    }

    setState(() => _filteredList = _searchResult);
  }
}
