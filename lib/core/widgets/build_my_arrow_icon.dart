import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/res.dart';

class BuildMyArrowIcon extends StatelessWidget {
  final Color? color;
  final double? height;
  const BuildMyArrowIcon({
    Key? key, this.color, this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Transform.rotate(
      angle: device.locale == Locale('en', 'US') ? 0 : 3.16,
      child: SvgPicture.asset(
        Res.arrowForward,
        height:height?? (device.locale == Locale('en', 'US')
            ? null
            : device.isTablet
                ? 9.sp
                : 15.sp),
        color:color?? MyColors.white,
      ),
    );
  }
}
