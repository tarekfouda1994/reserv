import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/res.dart';

class StaffPlaceholder extends StatelessWidget {
  final double? width;
  final double? height;

  const StaffPlaceholder({Key? key, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgPicture.asset(
        Res.staff_default_img,
        width:width,
        height:height,
      ),
    );
  }
}
