import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/res.dart';

class CategoryPlaceholder extends StatelessWidget {
  const CategoryPlaceholder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgPicture.asset(
        Res.categorie_default_img,
      ),
    );
  }
}
