import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/res.dart';

class BusinessPlaceholder extends StatelessWidget {
  const BusinessPlaceholder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgPicture.asset(
        Res.business_default_img,
        width: 120.w,
        height: 120.h,
      ),
    );
  }
}
