import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/input_field_style/custom_input_decoration.dart';
import 'package:flutter_tdd/core/constants/input_field_style/custom_input_text_style.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/widgets/MyText.dart';

import 'custom_dropDown/CustomDropDown.dart';

class DropdownTextField<T> extends StatelessWidget {
  final dynamic data;
  final GlobalKey? dropKey;
  final String title;
  final String? label;
  final String? hint;
  final String? searchHint;
  final T? selectedItem;
  final Widget? prefixIcon;
  final bool showSelectedItem;
  final EdgeInsets? margin;
  final double? fontSize;
  final double? labelSize;
  final double? textSize;
  final String Function(T item) itemAsString;
  final String? Function(dynamic) validate;
  final ValueChanged<T?>? onChange;
  final Future<List<T>> Function(String text)? onFind;
  final EdgeInsets? arrowBtnPadding;
  final EdgeInsets? clearBtnPadding;
  final EdgeInsets? contentPadding;
  final bool useName;
  final bool showClearButton;
  final Color? enableColor;
  final Color? fillColor;
  final Color? hintColor;
  final Color? buttonsColor;
  final BorderRadius? radius;
  final Color? textColor;
  final DropdownSearchPopupItemBuilder<T>? itemBuilder;

  const DropdownTextField(
      {Key? key,
      this.label,
      this.hint,
      this.margin,
      required this.validate,
      required this.title,
      this.contentPadding,
      this.prefixIcon,
      this.clearBtnPadding,
      this.arrowBtnPadding,
      this.textColor,
      this.useName = true,
      this.showClearButton = true,
      this.searchHint,
      required this.itemAsString,
      this.onChange,
      this.fontSize,
      this.textSize,
      this.labelSize,
      this.hintColor,
      this.fillColor,
      this.buttonsColor,
      this.itemBuilder,
      this.onFind,
      this.dropKey,
      this.data,
      this.enableColor,
      this.selectedItem,
      this.radius,
      this.showSelectedItem = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = context.watch<DeviceCubit>().state.model;
    return FormField<T>(
      initialValue: selectedItem,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (T? value) {
        return validate(value);
      },
      builder: (fieldState) {
        return DropdownSearch<T>(
          key: dropKey,
          mode: Mode.BOTTOM_SHEET,
          isFilteredOnline: false,
          maxHeight: model.isTablet ? 500 : 350,
          label: label,
          items: data,
          onFind: onFind,
          validator: validate,
          onChanged: onChange,
          showSearchBox: true,
          showClearButton: showClearButton,
          popupItemBuilder: itemBuilder,
          clearButton: Padding(
            padding: clearBtnPadding ?? const EdgeInsets.symmetric(horizontal: 5),
            child: Icon(
              Icons.clear,
              size: model.isTablet ? 30 : 20,
              color: buttonsColor ?? Colors.black,
            ),
          ),
          dropDownButton: Padding(
            padding: arrowBtnPadding ?? const EdgeInsets.symmetric(horizontal: 5).r,
            child: Icon(
              Icons.arrow_drop_down,
              size: model.isTablet ? 40 : 24,
              color: buttonsColor ?? Colors.black,
            ),
          ),
          selectedItem: selectedItem,
          itemAsString: itemAsString,
          showSelectedItem: showSelectedItem,
          style: CustomInputTextStyle(
            textColor: textColor,
            isTablet: model.isTablet,
            lang: model.locale.languageCode,
          ),
          itemStyle: TextStyle(
            fontSize: textSize ?? fontSize ?? (model.isTablet ? 8.sp : 10.sp),
          ),
          searchBoxStyle: CustomInputTextStyle(
            textColor: textColor,
            isTablet: model.isTablet,
            lang: model.locale.languageCode,
          ),
          searchBoxDecoration: CustomInputDecoration(
            isTablet: model.isTablet,
            lang: model.locale.languageCode,
            // labelTxt: label,
            // hint: hint,
            prefIcon: prefixIcon,
            customFillColor: fillColor,
            hintColor: hintColor,
            borderRadius: radius,
            isCustomField: true,
          ),
          popupTitle: Container(
            height: 50,
            decoration: BoxDecoration(
              color: MyColors.primary,
            ),
            child: Center(
              child: MyText(
                title: label != null ? label! : hint ?? "",
                size: 16,
                color: Colors.white,
              ),
            ),
          ),
          dropdownSearchDecoration: CustomInputDecoration(
            isTablet: model.isTablet,
            lang: model.locale.languageCode,
            prefIcon: prefixIcon,
            customFillColor: fieldState.errorText != null
                ? MyColors.fillErrorColor
                : fillColor ?? MyColors.fillColor,
            hintColor: hintColor,
            borderRadius: radius,
            isCustomField: true,
          ),
        );
      },
    );

  }
}
