import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/res.dart';
import 'package:internet_connectivity_checker/internet_connectivity_checker.dart';
import 'package:lottie/lottie.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

import '../localization/localization_methods.dart';

class NoInternetScreen extends StatelessWidget {
  final Widget child;

  const NoInternetScreen({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return internetConnectivityBuilder(
      (ConnectivityStatus status) {
        if (status == ConnectivityStatus.offine) {
          return Container(
            color: MyColors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset(
                  Res.internetError,
                ),
                MyText(
                  title: tr("noInternet",context: context),
                  color: MyColors.blackOpacity,
                  size: 10,
                  fontWeight: FontWeight.w500,
                ),
                MyText(
                  title: tr("checkInternet",context: context),
                  color: MyColors.blackOpacity,
                  size: 10,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          );
        } else {
          return child;
        }
      },
    );
  }
}
