import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';

class InlineFieldValidationView extends StatelessWidget {
  final Widget child;
  final TextEditingController controller;
  final Function(String? value) validate;

  const InlineFieldValidationView({
    Key? key,
    required this.child,
    required this.controller,
    required this.validate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return FormField(
      initialValue: controller.text,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? value) {
        return validate(controller.text);
      },
      builder: (FormFieldState formFieldState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            child,
            if (formFieldState.errorText != null)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Text(
                  formFieldState.errorText ?? "",
                  style: TextStyle(
                    fontSize: deviceModel.isTablet ? 8.sp : 10.sp,
                    color: Color(0xffED3A57),
                    fontFamily: CustomFonts.primaryFont,
                  ),
                ),
              ),
          ],
        );
      },
    );
  }
}

