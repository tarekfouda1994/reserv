import 'package:flutter/material.dart';

class DisableWidget extends StatelessWidget {
  final bool isDisable;
  final bool ignorePointerWhenDisable;
  final Widget child;

  const DisableWidget(
      {Key? key,
      required this.isDisable,
      required this.child,
      this.ignorePointerWhenDisable = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: ignorePointerWhenDisable ? isDisable : false,
      child: Opacity(
        opacity: isDisable ? 0.3 : 1.0,
        child: child,
      ),
    );
  }
}
