import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/errors/base_error.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class ShowErrorWidget extends StatelessWidget {
  /// TT This should be BaseError instead of BaseState because it is always to show error :)
  final BaseError error;
  final VoidCallback? callback;

  const ShowErrorWidget({Key? key, required this.error, this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final error = this.error;
    return MyText(title: error.toString(), color: MyColors.errorColor, size: 16);
  }
}

