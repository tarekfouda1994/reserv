import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/helpers/validator.dart';
import 'package:flutter_tdd/core/localization/localization_methods.dart';
import 'package:flutter_tdd/core/widgets/build_header_sheet.dart';
import 'package:flutter_tdd/core/widgets/build_my_arrow_icon.dart';
import 'package:flutter_tdd/core/widgets/custom_text_field/custom_text_field.dart';
import 'package:flutter_tdd/features/auth/data/model/countries_item_model/countries_item_model.dart';
import 'package:flutter_tdd/features/auth/domain/entities/countries_params.dart';
import 'package:flutter_tdd/features/auth/domain/usercases/get_all_countries.dart';
import 'package:flutter_tdd/res.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildNationalitySheet extends StatefulWidget {
  final String? prevSelectedItem;
  final void Function(CountriesItemModel) onSelectCallBac;

  const BuildNationalitySheet({
    key,
    required this.onSelectCallBac,
    this.prevSelectedItem,
  }) : super(key: key);

  @override
  State<BuildNationalitySheet> createState() => _BuildNationalitySheetState();
}

class _BuildNationalitySheetState extends State<BuildNationalitySheet> {
  final TextEditingController _searchController = TextEditingController();
  final GenericBloc<List<CountriesItemModel>> _bloc = GenericBloc([]);
  List<CountriesItemModel> allList = [];

  Future<void> _fetchData({bool refresh = true}) async {
    await GetAllCounties().call(_countriesParams(refresh)).then(
      (data) {
        _setInitialValue(data);
        allList = data;
        _bloc.onUpdateData(data);
      },
    );
  }

  void _setInitialValue(List<CountriesItemModel> data) {
    if (widget.prevSelectedItem != null && widget.prevSelectedItem != "") {
      data
          .firstWhere(
            (element) => element.getNationName() == widget.prevSelectedItem,
          )
          .isSelected = true;
    }
  }

  CountriesParams _countriesParams(bool refresh) {
    return CountriesParams(
      isActive: true,
      pageNumber: 1,
      pageSize: 222,
      refresh: refresh,
    );
  }

  @override
  void initState() {
    _fetchData(refresh: false);
    _fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bool isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    final device = MediaQuery.of(context).size.height;
    final statusBarHeight = MediaQuery.of(context).padding.top;
    final height = device - (statusBarHeight + (150));
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      height: height,
      child: Column(
        children: [
          BuildHeaderSheet(title: tr("nationality")),
          SizedBox(height: 10),
          Expanded(
            child: BlocBuilder<GenericBloc<List<CountriesItemModel>>,
                    GenericState<List<CountriesItemModel>>>(
                bloc: _bloc,
                builder: (context, state) {
                  if (state is GenericUpdateState) {
                    return Column(
                      children: [
                        Expanded(
                          child: Visibility(
                            visible: state.data.isNotEmpty,
                            replacement: Center(
                              child: MyText(
                                title: tr("noResultsFound"),
                                size: 14,
                                color: MyColors.black,
                              ),
                            ),
                            child: ListView.builder(
                              itemCount: state.data.length,
                              itemBuilder: (context, index) {
                                final item = state.data[index];
                                return InkWell(
                                  onTap: () {
                                    _onSelect(item, context);
                                    widget.onSelectCallBac(item);
                                  },
                                  child: Container(
                                    height: 48,
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 6, horizontal: 20),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      border: Border.all(
                                        color: item.isSelected == true
                                            ? MyColors.primary
                                            : MyColors.reserveDateBg,
                                        width:
                                            item.isSelected == true ? 1.5 : 1,
                                      ),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: MyText(
                                            title: item.getNationName(),
                                            color: item.isSelected == true
                                                ? MyColors.primary
                                                : MyColors.blackOpacity
                                                    .withOpacity(.45),
                                            size: 10,
                                            fontWeight: item.isSelected == true
                                                ? FontWeight.w600
                                                : FontWeight.w500,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        BuildMyArrowIcon(
                                          color: item.isSelected == true
                                              ? MyColors.primary
                                              : Color(0xffD9D9D9),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                        if (allList.isNotEmpty)
                          Padding(
                            padding: const EdgeInsets.fromLTRB(24, 10, 24, 20),
                            child: CustomTextField(
                              fieldTypes: FieldTypes.normal,
                              controller: _searchController,
                              type: TextInputType.text,
                              action: TextInputAction.done,
                              label: tr("nationality"),
                              validate: (value) => value?.noValidate(),
                              hint: tr("select_nationality_hint"),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.all(13),
                                child: SvgPicture.asset(
                                  Res.Search,
                                  width: isTablet ? 10.w : null,
                                ),
                              ),
                              suffixIcon: Visibility(
                                visible: _searchController.text.isNotEmpty,
                                child: InkWell(
                                  child: Icon(Icons.clear,
                                      color: MyColors.primary),
                                  onTap: () => _onClear(),
                                ),
                              ),
                              onSubmit: () => _filterSearchResults(),
                              onChange: (v) => _filterSearchResults(),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 0, horizontal: 10),
                            ),
                          ),
                      ],
                    );
                  } else {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(
                          color: MyColors.primary,
                        ),
                      ],
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }

  void _onClear() {
    _searchController.clear();
    _bloc.onUpdateData(allList);
  }

  void _filterSearchResults() {
    if (_searchController.text.trim().isNotEmpty) {
      final filteredList = allList
          .where(
            (element) => element.getNationName().toLowerCase().contains(
                  _searchController.text.toLowerCase(),
                ),
          )
          .toList();
      _bloc.onUpdateData(filteredList);
    } else {
      _bloc.onUpdateData(allList);
    }
  }

  void _onSelect(CountriesItemModel item, BuildContext context) {
    _bloc.state.data.map((e) => e.isSelected = false).toList();
    item.isSelected = true;
    _bloc.onUpdateData(_bloc.state.data);
    Navigator.pop(context);
  }
}
