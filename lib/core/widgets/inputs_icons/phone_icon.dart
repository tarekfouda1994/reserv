import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/res.dart';

class PhoneIcon extends StatelessWidget {
  const PhoneIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildSuffixIcon(asset: Res.mobile_notch);
  }
}
