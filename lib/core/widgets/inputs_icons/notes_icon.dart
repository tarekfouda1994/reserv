import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/res.dart';

class NotesIcon extends StatelessWidget {
  const NotesIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildSuffixIcon(asset: Res.edit);
  }
}
