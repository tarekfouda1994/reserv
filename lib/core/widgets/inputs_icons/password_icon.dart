import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/res.dart';

class PasswordIcon extends StatelessWidget {
  final bool showPass;
  final Color? assetColor;
  const PasswordIcon({Key? key, required this.showPass, this.assetColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildSuffixIcon(
      asset: showPass ? Res.eye : Res.eye_crossed,
      assetColor: assetColor,
    );
  }
}
