import 'package:flutter/material.dart';
import 'package:flutter_tdd/core/widgets/suffix_icon.dart';
import 'package:flutter_tdd/res.dart';

class CalendarIcon extends StatelessWidget {
  const CalendarIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildSuffixIcon(asset: Res.calendar);
  }
}
