import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

import '../constants/my_colors.dart';

class BuildShimmerView extends StatelessWidget {
  final double? width;
  final double height;
  final double? borderRadius;
  final BoxShape? boxShape;
  final EdgeInsets? margin;

  const BuildShimmerView(
      {Key? key, this.width, this.boxShape, this.margin, required this.height, this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: MyColors.offWhite,
      highlightColor: MyColors.white,
      child: Container(
        width: width ?? MediaQuery.of(context).size.width,
        height: height,
        margin: margin ?? EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
          color: MyColors.offWhite,
          shape: boxShape ?? BoxShape.rectangle,
          borderRadius:
              boxShape != BoxShape.rectangle ? null : BorderRadius.circular(borderRadius ?? 8.r),
        ),
      ),
    );
  }
}
