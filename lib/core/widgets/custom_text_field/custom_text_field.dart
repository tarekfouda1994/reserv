import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/constants.dart';
import 'package:flutter_tdd/core/constants/input_field_style/custom_input_decoration.dart';
import 'package:flutter_tdd/core/constants/input_field_style/custom_input_text_style.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/models/device_model/device_model.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController? controller;
  final String? label;
  final String? hint;
  final bool autoFocus;
  final int? max;
  final EdgeInsets? margin;
  final EdgeInsets? contentPadding;
  final TextInputType type;
  final TextInputAction action;
  final BorderRadius? radius;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Widget? prefixWidget;
  final Widget? suffixWidget;
  final Function()? onTab;
  final Color? enableBorderColor;
  final Color? focusBorderColor;
  final Color? fillColor;
  final Color? hintColor;
  final Color? textColor;
  final int? maxLength;
  final Function(String? value) validate;
  final FieldTypes fieldTypes;
  final Function()? onSubmit;
  final Function(String)? onChange;
  final List<TextInputFormatter>? inputFormatters;
  final AutovalidateMode? autoValidateMode;
  final String? fontFamily;

  CustomTextField(
      {this.label,
      this.hint,
      required this.fieldTypes,
      this.controller,
      this.margin,
      this.autoFocus = false,
      this.contentPadding,
      this.inputFormatters,
      required this.type,
      this.onTab,
      this.radius,
      this.max,
      this.maxLength,
      this.suffixWidget,
      this.prefixWidget,
      this.textColor,
      this.fillColor,
      this.hintColor,
      this.prefixIcon,
      this.suffixIcon,
      this.onChange,
      this.fontFamily,
      this.autoValidateMode,
      this.onSubmit,
      required this.action,
      this.enableBorderColor,
      this.focusBorderColor,
      required this.validate});

  @override
  State<StatefulWidget> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  GenericBloc<FieldState> valueCubit = GenericBloc(FieldState());
  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    valueCubit.state.data.value = widget.controller?.text ?? "";
    valueCubit.onUpdateData(valueCubit.state.data);
    _focusNode.addListener(() {
      if (mounted) {
        valueCubit.state.data.hasFocus = _focusNode.hasFocus;
        valueCubit.onUpdateData(valueCubit.state.data);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var deviceModel = context.watch<DeviceCubit>().state.model;
    return BlocBuilder<GenericBloc<FieldState>, GenericState<FieldState>>(
      bloc: valueCubit,
      builder: (context, state) {
        return Padding(
          padding: widget.margin ?? EdgeInsets.all(0),
          child: FormField(
            initialValue: widget.controller?.text,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (String? value) {
              return widget.validate(state.data.value);
            },
            builder: (FormFieldState formFieldState) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding:
                        EdgeInsets.only(top: state.data.getPaddingValue()).r,
                    height: (deviceModel.isTablet ? 60.sm : 52.sm) +
                        (15.sm * (widget.max ?? 0)),
                    decoration: BoxDecoration(
                      color: formFieldState.errorText != null
                          ? MyColors.fillErrorColor
                          : widget.fillColor ?? MyColors.fillColor,
                      borderRadius: BorderRadius.circular(6).r,
                      border: Border.all(
                        color: state.data.hasFocus
                            ? MyColors.primary
                            : Colors.transparent,
                        width: 2,
                      ),
                    ),
                    child: Visibility(
                      visible: widget.fieldTypes == FieldTypes.clickable,
                      child: buildClickableView(deviceModel, formFieldState),
                      replacement: buildFormFiled(deviceModel, formFieldState),
                    ),
                  ),
                  if (formFieldState.errorText != null)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 5),
                      child: Text(
                        formFieldState.errorText ?? "",
                        style: TextStyle(
                          fontSize: deviceModel.isTablet ? 8.sp : 10.sp,
                          color: Color(0xffED3A57),
                          fontFamily: CustomFonts.primaryFont,
                        ),
                      ),
                    ),
                ],
              );
            },
          ),
        );
      },
    );
  }

  Widget buildClickableView(DeviceModel model, FormFieldState formFieldState) {
    return InkWell(
      onTap: widget.onTab,
      child: AbsorbPointer(
        absorbing: true,
        ignoringSemantics: false,
        child: buildFormFiled(model, formFieldState),
      ),
    );
  }

  Widget buildFormFiled(DeviceModel model, FormFieldState formFieldState) {
    return TextFormField(
      controller: widget.controller,
      keyboardType: widget.type,
      textInputAction: widget.action,
      enableSuggestions: false,
      autocorrect: false,
      autofocus: widget.autoFocus,
      focusNode: _focusNode,
      autovalidateMode:
          widget.autoValidateMode ?? AutovalidateMode.onUserInteraction,
      inputFormatters: widget.inputFormatters ??
          [
            if (widget.maxLength != null)
              LengthLimitingTextInputFormatter(widget.maxLength),
            //n is maximum number of characters you want in textfield
          ],
      enabled: widget.fieldTypes != FieldTypes.disable,
      autofillHints: getAutoFillHints(widget.type),
      maxLines: widget.fieldTypes == FieldTypes.chat
          ? null
          : widget.fieldTypes == FieldTypes.rich
              ? widget.max
              : 1,
      obscureText: widget.fieldTypes == FieldTypes.password,
      readOnly: widget.fieldTypes == FieldTypes.readonly,
      onEditingComplete: widget.onSubmit,
      onChanged: (value) {
        valueCubit.state.data.value = value;
        formFieldState.didChange(value);
        valueCubit.onUpdateData(valueCubit.state.data);
        if (widget.onChange != null) {
          widget.onChange!(value);
        }
      },
      style: CustomInputTextStyle(
        textColor: widget.textColor,
        isTablet: model.isTablet,
        lang: model.locale.languageCode,
      ),
      decoration: CustomInputDecoration(
        isTablet: model.isTablet,
        lang: model.locale.languageCode,
        labelTxt: widget.label,
        hint: widget.hint,
        prefIcon: widget.prefixIcon,
        sufIcon: widget.suffixIcon,
        enableColor: widget.enableBorderColor,
        focsColor: widget.focusBorderColor,
        hintColor: widget.hintColor,
        borderRadius: widget.radius,
        isCustomField: true,
      ),
    );
  }

  List<String> getAutoFillHints(TextInputType inputType) {
    if (inputType == TextInputType.emailAddress) {
      return [AutofillHints.email];
    } else if (inputType == TextInputType.datetime) {
      return [AutofillHints.birthday];
    } else if (inputType == TextInputType.phone) {
      return [AutofillHints.telephoneNumber];
    } else if (inputType == TextInputType.url) {
      return [AutofillHints.url];
    }
    return [AutofillHints.name, AutofillHints.username];
  }
}

class FieldState {
  String value;
  bool hasFocus;

  FieldState({this.value = "", this.hasFocus = false});

  double getPaddingValue() {
    if (value.isNotEmpty || hasFocus) {
      return 5;
    }
    return 0;
  }
}
