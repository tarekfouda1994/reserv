import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tf_custom_widgets/widgets/DefaultButton.dart';

import '../constants/my_colors.dart';
import '../localization/localization_methods.dart';

class BuildPageError extends StatelessWidget {
  final Function() onTap;

  const BuildPageError({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: DefaultButton(
        onTap: onTap,
        color: MyColors.grey,
        textColor: MyColors.white,
        title: tr("try_again"),
        width: MediaQuery.of(context).size.width * .2.w,
      ),
    );
  }
}
