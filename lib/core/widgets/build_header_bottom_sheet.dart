import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildHeaderBottomSheet extends StatelessWidget {
  final String title;
  final Color? titleColor;
  final Widget? ladingIcon;
  final EdgeInsetsGeometry? margin;

  const BuildHeaderBottomSheet({
    Key? key,
    required this.title,
    this.margin,
    this.ladingIcon, this.titleColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var device = context.watch<DeviceCubit>().state.model;
    return Padding(
      padding: margin ?? EdgeInsets.zero,
      child: Row(
        children: [
          Visibility(
            visible: ladingIcon != null,
            child: Row(
              children: [
                ladingIcon ?? SizedBox(),
                SizedBox(width: 10),
              ],
            ),
          ),
          Expanded(
            child: MyText(
              title: title,
              color:titleColor?? MyColors.black,
              size: device.isTablet ? 10.5.sp : 13.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
          InkWell(
            onTap: () => Navigator.pop(context),
            child: Container(
              padding: EdgeInsets.all(6.r),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: MyColors.black,
              ),
              child: Icon(
                Icons.clear,
                size: device.isTablet ? 14.sp : 19.sp,
                color: MyColors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
