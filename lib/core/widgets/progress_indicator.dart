import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_carousel_widget/indicators/slide_indicator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProgressSlideIndicator implements SlideIndicator {
  final double? itemSpacing;
  final double? width;
  final double? height;
  final Color? currentIndicatorColor;
  final Color? indicatorBackgroundColor;
  final EdgeInsetsGeometry? padding;

  const ProgressSlideIndicator({
    this.itemSpacing,
    this.currentIndicatorColor,
    this.indicatorBackgroundColor,
    this.width,
    this.height,
    this.padding,
  });

  @override
  Widget build(int currentPage, double pageDelta, int itemCount) {
    var activeColor = const Color(0xFFFFFFFF);
    var backgroundColor = const Color(0x66FFFFFF);

    if (SchedulerBinding.instance.window.platformBrightness ==
        Brightness.light) {
      activeColor = const Color(0xFF000000);
      backgroundColor = const Color(0xFF878484);
    }

    return Container(
      padding: padding,
      child: Wrap(
        spacing: itemSpacing ?? 8,
        runSpacing: 8,
        alignment: WrapAlignment.center,
        children: List.generate(itemCount, (index) {
          return Container(
            width: currentPage == index ? (width ?? 18.w) : (width ?? 8),
            height: currentPage == index ? 7 : 8,
            decoration: BoxDecoration(
                color: currentPage == index
                    ? currentIndicatorColor ?? activeColor
                    : indicatorBackgroundColor ?? backgroundColor,
                borderRadius: BorderRadius.circular(5).r),
          );
        }),
      ),
    );
  }
}
