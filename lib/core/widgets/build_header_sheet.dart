import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildHeaderSheet extends StatelessWidget {
  final String title;
  const BuildHeaderSheet({key, required this.title}):super(key: key);

  @override
  Widget build(BuildContext context) {
    final isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      child:
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        MyText(
          title: title,
          color: MyColors.black,
          size: isTablet ? 8.sp : 12.sp,
          fontWeight: FontWeight.bold,
        ),
        InkWell(
          onTap: () => AutoRouter.of(context).pop(),
          child: Container(
            padding: EdgeInsets.all(6.r),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: MyColors.black,
            ),
            child: Icon(
              Icons.clear,
              size: isTablet ? 14.sp : 19.sp,
              color: MyColors.white,
            ),
          ),
        ),
      ]),
    );
  }
}
