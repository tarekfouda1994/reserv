import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';

class CustomCard extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry? padding;
  final BoxBorder? border;
  const CustomCard({Key? key, required this.child, this.padding, this.border}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 10.h),
      padding:padding?? EdgeInsets.all(16.0.r),
      decoration: BoxDecoration(
        color: MyColors.white,
        borderRadius: BorderRadius.circular(13.r),
        border:border?? Border.all(width: .1, color: MyColors.grey.withOpacity(.5)),
      ),
      child: child,);
  }
}
