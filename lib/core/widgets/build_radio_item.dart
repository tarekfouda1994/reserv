import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/bloc/device_cubit/device_cubit.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:tf_custom_widgets/tf_custom_widgets.dart';

class BuildRadioItem extends StatelessWidget {
  final dynamic value;
  final dynamic changeValue;
  final String? title;
  final Function()? onTap;
  final double? iconSize;
  final String? fontFamily;
  final EdgeInsetsGeometry? margin;

  const BuildRadioItem({
    Key? key,
    required this.value,
    required this.changeValue,
    this.title,
     this.onTap,
    this.iconSize,
    this.fontFamily, this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isTablet = context.watch<DeviceCubit>().state.model.isTablet;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: margin??const EdgeInsets.symmetric(vertical: 6),
        child: Row(children: [
          Container(
            padding: EdgeInsets.all(2.sp),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: MyColors.primary),
              color: changeValue == value ? MyColors.primary : MyColors.white,
            ),
            child: Icon(
              Icons.done,
              color: MyColors.white,
              size: iconSize ?? (isTablet ? 8.sp : 16.sp),
            ),
          ),
          SizedBox(width: 10),
          if (title != null)
            MyText(
              title: title ?? "",
              color: changeValue == value ? MyColors.primary : MyColors.grey,
              size: isTablet ? 7.sp : 11.sp,
              fontFamily: fontFamily,
            ),
        ]),
      ),
    );
  }
}
