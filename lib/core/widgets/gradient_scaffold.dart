import 'package:flutter/material.dart';
import 'package:flutter_tdd/res.dart';

class GradientScaffold extends StatelessWidget {
  final PreferredSizeWidget? appBar;
  final Widget? body;
  final Widget? bottomSheet;
  final Widget? bottomNavigationBar;
  final Widget? floatingActionButton;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final FloatingActionButtonAnimator? floatingActionButtonAnimator;

  const GradientScaffold(
      {Key? key,
      this.appBar,
      this.body,
      // this.backgroundColor,
      this.bottomSheet,
      this.bottomNavigationBar,
      this.floatingActionButton,
      this.floatingActionButtonAnimator,
      this.floatingActionButtonLocation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Res.background),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Scaffold(
          appBar: appBar,
          body: body,
          backgroundColor: Colors.transparent,
          bottomSheet: bottomSheet,
          bottomNavigationBar: bottomNavigationBar,
          floatingActionButton: floatingActionButton,
          floatingActionButtonAnimator: floatingActionButtonAnimator,
          floatingActionButtonLocation: floatingActionButtonLocation,
        ),
      ],
    );
  }
}
