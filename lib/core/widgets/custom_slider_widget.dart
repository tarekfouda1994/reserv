import 'package:flutter/material.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/constants/my_colors.dart';
import 'package:flutter_tdd/core/widgets/progress_indicator.dart';

class CustomSliderWidget extends StatelessWidget {
  final List<Widget> items;
  final double? height;

  const CustomSliderWidget({Key? key, required this.items, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom:items.length>1? 35:25),
      child: FlutterCarousel(
          key: GlobalKey(),
          items: items,
          options: CarouselOptions(
            height: height ?? 200.0.h,
            aspectRatio: 16 / 9,
            viewportFraction: .9,
            enableInfiniteScroll: false,
            autoPlay: false,
            enlargeCenterPage: false,
            pageSnapping: true,
            scrollDirection: Axis.horizontal,
            pauseAutoPlayOnTouch: true,
            pauseAutoPlayOnManualNavigate: true,
            pauseAutoPlayInFiniteScroll: false,
            enlargeStrategy: CenterPageEnlargeStrategy.scale,
            disableCenter: false,
            showIndicator: true,
            slideIndicator: ProgressSlideIndicator(
              currentIndicatorColor: MyColors.primaryDark,
              indicatorBackgroundColor: MyColors.indicatorBackgroundColor,
            ),
          )),
    );
  }
}
