import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_tdd/core/errors/connection_error.dart';
import 'package:flutter_tdd/core/errors/firebase/auth_error_const.dart';
import 'package:flutter_tdd/core/errors/firebase/firebase_error.dart';
import 'package:flutter_tdd/core/errors/firebase/operation_not_allowed_error.dart';
import 'package:flutter_tdd/core/errors/firebase/too_many_requests_error.dart';
import 'package:flutter_tdd/core/errors/firebase/unexpected_error.dart';
import 'package:flutter_tdd/core/errors/firebase/user_disabled_error.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/network/network_info.dart';
import 'package:flutter_tdd/core/result/result.dart';


class FirebaseProvider {
  const FirebaseProvider._();

  static Future<MyResult<D>> requestDataList<D>({
    required Future<QuerySnapshot> Function() dataCallback,
    required D Function(QuerySnapshot snapshot) converter,
  }) async {
    try {
      final hasConnection = await _hasConnection();
      if (hasConnection != true) {
        return MyResult<D>.isError(const ConnectionError());
      }

      final dataSnapshot = await dataCallback();

      return MyResult<D>.isSuccess(converter(dataSnapshot));
    } catch (e, s) {
      _logException(e, s);
      return MyResult<D>.isError(_getFirebaseError(e));
    }
  }

  static Future<MyResult<D>> requestDataObject<D>({
    required Future<DocumentSnapshot<Map<String, dynamic>>> Function() callback,
    required D Function(DocumentSnapshot<Map<String, dynamic>> snapshot) converter,
    bool withConnectionCheck = true,
  }) async {
    try {
      if (withConnectionCheck) {
        final connection = await _hasConnection();
        if (connection != true) {
          return MyResult<D>.isError(const ConnectionError());
        }
      }
      final snapshot = await callback();
      return MyResult<D>.isSuccess(converter(snapshot));
    } catch (e, s) {
      _logException(e, s);
      return MyResult<D>.isError(_getFirebaseError(e));
    }
  }

  static Future<bool> _hasConnection() async {
    return await getIt.get<NetworkInfo>().isConnected;
  }

  static FirebaseError _getFirebaseError(dynamic error) {
    final errorAsString = error.toString();
    if (errorAsString.contains(ERROR_TOO_MANY_REQUESTS)) {
      return TooManyRequestError();
    } else if (errorAsString.contains(ERROR_OPERATION_NOT_ALLOWED)) {
      return OperationNotAllowedError();
    } else if (errorAsString.contains(ERROR_USER_DISABLED)) {
      return UserDisabledError();
    } else {
      return UnExpectedFirebaseError();
    }
  }

  static void _logException(Object e, StackTrace s) {
    log('_______________________ EXCEPTION ______________________\n$e\n$s');
  }
}
