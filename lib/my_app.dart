import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:country_picker/country_picker.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_tdd/core/helpers/di.dart';
import 'package:flutter_tdd/core/helpers/global_context.dart';
import 'package:flutter_tdd/core/widgets/no_internet_screen.dart';
import 'package:month_year_picker/month_year_picker.dart';

import 'core/bloc/device_cubit/device_cubit.dart';
import 'core/helpers/app_them.dart';
import 'core/helpers/dynamic_link_service.dart';
import 'core/helpers/firebase_analytics_helper.dart';
import 'core/helpers/general_providers.dart';
import 'core/localization/set_localization.dart';
import 'core/routes/router_imports.gr.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _appRouter = AppRouter(getIt<GlobalContext>().navigationKey);

  final DynamicLinkService dynamicLinkService = DynamicLinkService();

  @override
  void initState() {
    getIt<FirebaseAnalyticsHelper>().analytics.setConsent(
        adStorageConsentGranted: false, analyticsStorageConsentGranted: true);
    super.initState();
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Timer(Duration(milliseconds: 2000),
          () => dynamicLinkService.retrieveDynamicLink(context));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: GeneralProviders.instance.providers(context),
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        final bool isTablet = constraints.maxWidth >= 500;
        context.read<DeviceCubit>().updateDeviceType(isTablet);
        return BlocBuilder<DeviceCubit, DeviceState>(
          builder: (context, state) {
            return MaterialApp.router(
                debugShowCheckedModeBanner: false,
                theme: AppThem.instance.themeData(
                  lang: state.model.locale.languageCode,
                ),
                title: "Reserva",
                supportedLocales: const [
                  Locale('en', 'US'),
                  Locale('ar', 'EG')
                ],
                localizationsDelegates: const [
                  CountryLocalizations.delegate,
                  SetLocalization.localizationsDelegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                  MonthYearPickerLocalizations.delegate,
                ],
                locale: state.model.locale,
                routerDelegate: _appRouter.delegate(
                    initialRoutes: [const SplashRoute()],
                    navigatorObservers: () {
                      return [
                        BotToastNavigatorObserver(),
                        FirebaseAnalyticsObserver(
                            analytics:
                                getIt<FirebaseAnalyticsHelper>().analytics)
                      ];
                    }),
                routeInformationParser: _appRouter.defaultRouteParser(),
                builder: (ctx, child) {
                  final botToastBuilder = BotToastInit();
                  ScreenUtil.init(ctx);
                  child = FlutterEasyLoading(child: child);
                  child = botToastBuilder(context, child);
                  return NoInternetScreen(child: child);
                });
          },
        );
      }),
    );
  }
}
